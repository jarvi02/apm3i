//
//  APM4Tests.m
//  APM4Tests
//
//  Created by Fabian E. Pezet Vila on 24/01/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "APM4Tests.h"

@implementation APM4Tests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"Unit tests are not implemented yet in APM4Tests");
}

@end
