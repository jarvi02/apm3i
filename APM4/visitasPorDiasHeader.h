//
//  visitasPorDiasHeader.h
//  APM4
//
//  Created by Laura Busnahe on 9/11/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
@class visitasPorDiasHeader;

typedef enum
{
    fVisitasPorDiasFieldFecha,
    fVisitasPorDiasFieldFijos,
    fVisitasPorDiasFieldFluctuantes,
    fVisitasPorDiasFieldAcompaniadas,
    fVisitasPorDiasFieldTotal
} tVisitasPorDiasHeaderField;

@protocol MTvistasPorDiaHeaderDelegate <NSObject>
@required
// Sent when the user tap a field.
- (void)VisitasPorDiaHeader:(visitasPorDiasHeader*)header
                selectedField:(tVisitasPorDiasHeaderField)field
                        Order:(NSComparisonResult)order;
@end


@interface visitasPorDiasHeader : UIView

@property (nonatomic, assign) id <MTvistasPorDiaHeaderDelegate> delegate;

@property (retain, nonatomic) IBOutlet UIButton *lblFecha;
@property (retain, nonatomic) IBOutlet UIButton *lblFijos;
@property (retain, nonatomic) IBOutlet UIButton *lblFluctuantes;
@property (retain, nonatomic) IBOutlet UIButton *lblAcompaniadas;
@property (retain, nonatomic) IBOutlet UIButton *lblTotal;

- (void)orderBy:(tVisitasPorDiasHeaderField)field sender:(id)sender reset:(BOOL)reset;

- (IBAction)tapLabel:(id)sender;

@end
