//
//  ServiceEnviarNovedad.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Service.h"

@interface ServiceEnviarNovedad : Service

-(BOOL*) enviar:(NSString*)urlBase fileName:(NSString*)aFile tamanio:(NSInteger)tamanio;

@end
