//
//  PromocionadosViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "PromocionadosViewController.h"
#import "Promocionados.h"
#import "NSDate+extensions.h"

@interface PromocionadosViewController ()

@end

@implementation PromocionadosViewController

- (void)dealloc
{
    [_data release];
    
    [_tmpCell release];
	[_cellNib release];
    
    [_tableHeader release];
    [super dealloc];
}

- (id)initWithData:(NSArray*)aData
{
    self = [super init];
    if (self) {
        self.cellNib    = [UINib nibWithNibName:@"PromocionadosCell" bundle:nil];
        
        self.headerNib  = [UINib nibWithNibName:@"PromocionadosHeader" bundle:nil];
        [self.headerNib instantiateWithOwner:self options:nil];
        
        self.data       = aData;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    if ([self.data count] == 0) {
        return 1;
    }
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"PromocionadosCelldentifier";
	
    // Obtain the cell object.
	PromocionadosCell *cell = (PromocionadosCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	// If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
		cell = self.tmpCell;
		self.tmpCell = nil;
        
    }
    
    cell.lbFecha.text       = @"";
    cell.lbOrden.text       = @"";
    cell.lbProducto.text    = @"";
    cell.lbCantidad.text    = @"";
    cell.lbLit.text         = @"";
    cell.lbObs.text         = @"";
    
    if ([self.data count] == 0) {
        
        // si es 0 meto uno a la fuerza
        cell.lbFecha.text = @"Sin Productos";
        return cell;
    } 
    
    Promocionados *o = [self.data objectAtIndex:indexPath.row];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:o.fechaDisplay];
    date = date.dateAsDateWithoutTime;    
    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"dd/MM/yyyy"];
//    
//    cell.lbFecha.text = [formatter stringFromDate:date];
//    [formatter release];
    cell.lbFecha.text = [date formattedStringUsingFormat:@"dd/MM/yyyy"];
    
    
    
    //cell.lbFecha.text       = [date formattedStringUsingFormat:@"dd/MM/yyyy"];
    cell.lbOrden.text       = [NSString stringWithFormat:@"%d",o.orden];
    cell.lbProducto.text    = o.descripcion;
    cell.lbCantidad.text    = [NSString stringWithFormat:@"%d",o.muestras];
    cell.lbLit.text         = o.literatura;
    cell.lbObs.text         = o.obsequio;
    
	return cell;

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader;
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader.frame.size.height;
    else
        return 0;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    
//    UILabel *label;
//    CGRect frame;
//    
//    frame = CGRectMake(0, 0, 844, 52);
//    UILabel *header = [[UILabel alloc] initWithFrame:frame];
//    header.autoresizingMask = 34;
//    header.backgroundColor = [UIColor whiteColor];
//    
//    frame = CGRectMake(0, 30, 844, 20);
//    UILabel *background = [[[UILabel alloc] initWithFrame:frame] autorelease];
//    background.autoresizingMask = 34;
//    background.backgroundColor = [UIColor colorWithRed:0.09f green:0.3f blue:0.64f alpha:1.0f];
//    
//    [header addSubview:background];
//    
//    label = [[[UILabel alloc] init] autorelease];
//    label.frame = CGRectMake(20, 0, 400, 30);
//    label.autoresizingMask = 36;
//    label.text = @"Productos Promocionados";
//    label.textColor = [UIColor colorWithRed:0.09f green:0.3f blue:0.64f alpha:1.0f];
//    label.font = [UIFont boldSystemFontOfSize:26.0f];
//    label.backgroundColor = [UIColor clearColor];
//    [header addSubview:label];
//    
//    
//    label = [[[UILabel alloc] init] autorelease];
//    label.frame = CGRectMake(20, 0, 94, 20);
//    label.autoresizingMask = 36;
//    label.text = @"Fecha";
//    label.textColor = [UIColor whiteColor];
//    label.font = [UIFont boldSystemFontOfSize:14.0f];
//    label.backgroundColor = [UIColor clearColor];
//    [background addSubview:label];
//    
//    
//    label = [[[UILabel alloc] init] autorelease];
//    label.frame = CGRectMake(163, 0, 500, 20);
//    label.autoresizingMask = 36;
//    label.text = @"Productos";
//    label.textColor = [UIColor whiteColor];
//    label.font = [UIFont boldSystemFontOfSize:14.0f];
//    label.backgroundColor = [UIColor colorWithRed:0.09f green:0.3f blue:0.64f alpha:1.0f];
//    [background addSubview:label];
//    
//    label = [[[UILabel alloc] init] autorelease];
//    label.frame = CGRectMake(123, 0, 34, 20);
//    label.autoresizingMask = 36;
//    label.text = @"Ord.";
//    label.textColor = [UIColor whiteColor];
//    label.font = [UIFont boldSystemFontOfSize:14.0f];
//    label.backgroundColor = [UIColor colorWithRed:0.09f green:0.3f blue:0.64f alpha:1.0f];
//    [background addSubview:label];
//    
//    
//    label = [[[UILabel alloc] init] autorelease];
//    label.frame = CGRectMake(667, 0, 48, 20);
//    label.autoresizingMask = 37;
//    label.text = @"Cant.";
//    label.textColor = [UIColor whiteColor];
//    label.font = [UIFont boldSystemFontOfSize:14.0f];
//    label.backgroundColor = [UIColor colorWithRed:0.09f green:0.3f blue:0.64f alpha:1.0f];
//    [background addSubview:label];
//    
//    
//    label = [[[UILabel alloc] init] autorelease];
//    label.frame = CGRectMake(728, 0, 40, 20);
//    label.autoresizingMask = 33;
//    label.text = @"Lit.";
//    label.textColor = [UIColor whiteColor];
//    label.font = [UIFont boldSystemFontOfSize:14.0f];
//    label.backgroundColor = [UIColor colorWithRed:0.09f green:0.3f blue:0.64f alpha:1.0f];
//    [background addSubview:label];
//    
//    label = [[[UILabel alloc] init] autorelease];
//    label.frame = CGRectMake(782, 0, 42, 20);
//    label.autoresizingMask = 33;
//    label.text = @"Obs.";
//    label.textColor = [UIColor whiteColor];
//    label.font = [UIFont boldSystemFontOfSize:14.0f];
//    label.backgroundColor = [UIColor colorWithRed:0.09f green:0.3f blue:0.64f alpha:1.0f];
//    [background addSubview:label];
//    
//
//    return [header autorelease];
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 52.0;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
