//
//  MTMuestrasMedicas.m
//  APM4
//
//  Created by Laura Busnahe on 3/28/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTMuestrasMedicas.h"
#import "MTAjustes.h"
#import "DBExtended.h"
#import "Config.h"
#import "NSDateExtended.h"
#import "Novedad.h"
#import "GlobalDefines.h"

/// DB table column position into the arguments array.
#define valueID         0
#define valueNODO       1
#define valueCICLO      2
#define valuePRODUCTO   3
#define valueLOTE       4
#define valueAJUSTE     5
#define valueCANTIDAD   6
#define valueFECHA      7
#define valueSTOCK      8


@implementation MTMuestrasMedicas


- (id)init
{
    self = [super init];
    if (self) {
        
        self.descripcion = @"";
        self.codigo = @"";
        self.lote =@"";
        self.fecha_vto = nil;
        self.idNodo = 0;
        self.idLote = @"";
        self.vencido = NO;
        self.blocked = NO;
        
    }
    return self;
}


- (void)checkProducto
{
    //self.blocked = NO;
    self.vencido = NO;
    
    if ([[self.fecha_vto dateAsDateWithoutTime] timeIntervalSince1970] <= [[NSDate dateWithoutTime] timeIntervalSince1970])
    {
        self.blocked = YES;
        self.vencido = YES;
    }
    
    if ((self.stock == 0))
        self.blocked = YES;
}


- (NSArray*)dbGetArguments:(MTAjustes*)ajuste
{ // Esta función obtiene y devuelve los parámetros necesarios para el query de Add, Update y Delete
    // con el formato para realizarlo en la base de datos local.
    
    // stock = @stock WHERE (id = @id) AND (idlote = @idlote) AND (nodo = @nodo)"
    // (id, nodo, ciclo, producto, lote, ajuste, cantidad, fecha) VALUES(@id, @nodo, @ciclo, @producto, @lote, @ajuste, @cantidad, @fecha)
    
    NSMutableArray* result = [[NSMutableArray alloc] init];
//    NSInteger recID = [[DB getInstance] getNewTableID:@"gst_ajustes"
//                                           whereOrNil:[NSString stringWithFormat:@"nodo = %d", self.idNodo]
//                       ];

    // ID
    [result addObject:[NSString stringWithFormat:@"%i", self.recID]];
    
    // Nodo (Integer)
    [result addObject:[NSString stringWithFormat:@"%i", self.idNodo]];
    
    // Ciclo
    [result addObject:[[Config shareInstance] find:CICLO]];
    
    // Producto
    [result addObject:[NSString stringWithFormat:@"%i", self.recID]];
    
    // Lote
    [result addObject:self.idLote];
    
    // Ajuste
    [result addObject:[NSString stringWithFormat:@"%i", ajuste.recID]];
    
    // Cantidad
    [result addObject:[NSString stringWithFormat:@"%i", ajuste.cantidad]];
    
    // Fecha (Date)
    NSString *sDate = @"";
    sDate = [NSString stringWithFormat:@"%f",[ajuste.date datetimeForDB]];
    [result addObject:sDate];
    
    // Stock
    NSInteger newStock = self.stock + (ajuste.cantidad * ajuste.operacion);
    [result addObject:[NSString stringWithFormat:@"%i", newStock]];
    
 
    return result;
}

- (NSString *)getNovedadParameters:(MTAjustes*)ajuste gstID:(NSInteger)gstID;
{ // Esta función obtiene y devuelve los parámetros necesarios para el query de LOG_NOVEDADES
    // con el formato para realizarlo en dicha tabla.
    
    NSString *result = @"";
    NSArray *arguments = nil;
    NSString *sDate = nil;
    
    arguments = [self dbGetArguments:ajuste];
    sDate = [ajuste.date datetimeForNovedad];
        
    result = [NSString stringWithFormat:@"%d|%@|%@|%@|%@|%@|%@|%@",
              gstID,
              [arguments objectAtIndex:valueNODO],
              [arguments objectAtIndex:valueCICLO],
              [arguments objectAtIndex:valuePRODUCTO],
              [arguments objectAtIndex:valueLOTE],
              [arguments objectAtIndex:valueAJUSTE],
              [arguments objectAtIndex:valueCANTIDAD],
              sDate
              ];
    
    return result;
}



#pragma mark -
#pragma mark  General Purpose methods

- (NSString*)getDescription
{
    NSString *result = @"";
    
    result = [NSString stringWithString:self.descripcion];
    
    return result;
}


- (void)ajusteStock:(MTAjustes*)ajuste
{
    NSString *sql;
    NSString *query;
    NSArray *arguments = nil;
    NSString *lote = @"";
    
    // obtengo los argumentos para utilizar en la tabla de
    arguments = [self dbGetArguments:ajuste];
    
    if ([[arguments objectAtIndex:valueLOTE] length] > 0)
    {
        lote = [NSString stringWithFormat:@"= %@",[arguments objectAtIndex:valueLOTE]];
    } else
    {
        lote = @"is NULL";
    }
    
    // MTC_LOTES
    sql   = @"UPDATE mtc_lotes SET stock = %@ WHERE (id = %@) AND ((idlote %@)) AND (nodo = %@)";
    query = [NSString stringWithFormat:sql,
                    [arguments objectAtIndex:valueSTOCK],
                    [arguments objectAtIndex:valueID],
                    lote,
                    [arguments objectAtIndex:valueNODO]
                    ];
    [[DB getInstance] excecuteSQL:query];
#ifdef DEBUG_AJUSTEMM
    if (DEBUG_AJUSTEMM >= 1)
    {
        NSLog(@"--- ajusteStock ---");
        NSLog(@"Lotes: %@", query);
    }
#endif
    
    
    // GST_AJUSTES
    NSInteger newRecID = [[DB getInstance] getNewIDForTable:@"gst_ajustes"
                                                idNameOrNil:nil
                                                 whereOrNil:[NSString stringWithFormat:@"nodo = %d", self.idNodo]];
    
    sql   = @"INSERT INTO gst_ajustes (id, nodo, ciclo, producto, lote, ajuste, cantidad, fecha) "
             "VALUES(%d, %@, %@, %@, %@, %@, %@, %@)";
    query = [NSString stringWithFormat:sql,
                    newRecID,
                    [arguments objectAtIndex:valueNODO],
                    [arguments objectAtIndex:valueCICLO],
                    [NSString stringQuoted:[arguments objectAtIndex:valuePRODUCTO] ifNull:@"''"],
                    [NSString stringQuoted:[arguments objectAtIndex:valueLOTE] ifNull:@"''"],
                    [arguments objectAtIndex:valueAJUSTE],
                    [arguments objectAtIndex:valueCANTIDAD],
                    [arguments objectAtIndex:valueFECHA]
             ];
    [[DB getInstance] excecuteSQL:query];
#ifdef DEBUG_AJUSTEMM
    if (DEBUG_AJUSTEMM >= 1)
    {
        NSLog(@"Ajuste: %@", query);
    }
#endif
    
    
    // LOG_NOVEDADES
    NSString *novedadParam = [self getNovedadParameters:ajuste gstID:newRecID];
    [Novedad insert:@"gst_ajustes" novedad:novedadParam tipo:kInsert];
#ifdef DEBUG_AJUSTEMM
    if (DEBUG_AJUSTEMM >= 1)
    {
        NSLog(@"Novedad: %@", query);
    }
#endif

    
    
    // Actualizo el stock del produto.
    self.stock = [[arguments objectAtIndex:valueSTOCK] intValue];
}

+(NSArray*)GetAll {
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    NSString *sql = @"SELECT descripcion, stock, codigo, lote, "
                            "fecha_vto, "
                            //"strftime('%@', fecha_vto, 'unixepoch', 'localtime') fecha_vto_display, "
                            "id, nodo, idlote "
                    "FROM mtc_lotes "
                    "WHERE (mtc_lotes.nodo = %@) AND "
                          "(NOT (idlote is NULL)) AND "
                          "(idlote > 0) "  
                    "ORDER BY descripcion, mtc_lotes.codigo, mtc_lotes.fecha_vto, mtc_lotes.lote ;";
    
    NSString *nodo = SettingForKey(NODO, @"0");
    //NSString *query = [NSString stringWithFormat:sql, @"%s", nodo];
    NSString *query = [NSString stringWithFormat:sql, nodo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        MTMuestrasMedicas *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[MTMuestrasMedicas alloc] init];
            
            // Descripción.
            _c = (char *)sqlite3_column_text(statement, 0);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            // Stock.
            o.stock = sqlite3_column_int(statement, 1);
            
            // Código.
            _c = (char *)sqlite3_column_text(statement, 2);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.codigo = [NSString stringWithUTF8String:_c];
			}
            
            // Lote.
            _c = (char *)sqlite3_column_text(statement, 3);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.lote = [NSString stringWithUTF8String:_c];
			}
            
            // Vencimiento.
            int _fechaVto = sqlite3_column_int(statement, 4);
            o.fecha_vto = [[NSDate alloc] initWithTimeIntervalSince1970:_fechaVto];
            
            // ID.
            o.recID = sqlite3_column_int(statement, 5);
            
            // idNodo
            o.idNodo = sqlite3_column_int(statement, 6);
            
            // idLote
            _c = (char *)sqlite3_column_text(statement, 7);
            if((_c != nil) && (strlen(_c) > 0))
            {
                o.idLote = [NSString stringWithUTF8String:_c];
			}
            
            [o checkProducto];

            [array addObject:o];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla: %@", query);
	}
	sqlite3_finalize(statement);
    
    return array;
    
}

@end
