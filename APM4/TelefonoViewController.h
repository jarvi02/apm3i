//
//  TelefonoViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DomicilioViewController.h"
#import "Domicilio.h"

@interface TelefonoViewController : UIViewController <UITextFieldDelegate>

@property(nonatomic,assign) DomicilioViewController *domicilioViewControllerReference;
@property(nonatomic, assign) Domicilio *domicilioReference;
@property(nonatomic, assign) id<CloseModalViewControllerDelegate> delegate;

@property (retain, nonatomic) IBOutlet UITextField *txtPais;
@property (retain, nonatomic) IBOutlet UITextField *txtCodigoArea;
@property (retain, nonatomic) IBOutlet UITextField *txtTelefono;

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@property (retain, nonatomic) IBOutlet UIToolbar *tollBar;

-(void)guardar:(id)sender;
-(void)cancelar:(id)sender;
-(void)editar:(id)sender;
-(void)agregar:(id)sender;

@end
