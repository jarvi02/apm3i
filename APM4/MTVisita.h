//
//  MTVisita.h
//  APM4
//
//  Created by Laura Busnahe on 8/15/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTVisitaData.h"
#import "MTMedico.h"
#import "Promocion.h"


@interface MTVisita : MTVisitaData

@property(nonatomic, strong) MTVisitaData   *oldData;   // This is used by the method storeCurrentData

+ (BOOL)validarFechaDeVisista:(NSUInteger)idMedico fechaVisita:(NSDate *)fechaVisita fechaOriginal:(NSDate *)fechaOriginal;
+ (BOOL)validarVisitaExiste:(NSUInteger)idMedico fechaVisita:(NSDate *)fechaVisita fechaOriginal:(NSDate *)fechaOriginal;

+ (MTVisita*)getByID:(NSUInteger)idVisita idMedico:(NSUInteger)idMedico;


- (void)iniciarModificacion; // This method creates a copy into the object with the current data. It MUST to be used before to start
                             // to modify the visit.
- (void)cancelarModificacion;
- (void)guardarModificacion;
@end
