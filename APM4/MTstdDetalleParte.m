//
//  MTstdDetalleParte.m
//  APM4
//
//  Created by Laura Busnahe on 7/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTstdDetalleParte.h"
#import "Config.h"
#import "DB.h"
#import "Ciclo.h"

@implementation MTstdDetalleParte

- (id)init
{
    self = [super init];
    if (self)
    {
        self.fecha       = @"";
        self.actividad   = @"";
        self.nombre      = @"";
        self.descripcion = @"";
    }
    
    return self;
}

+ (NSArray*)getFechasParte
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql =
    @"SELECT DISTINCT "
            "fecha as fechadisplay, "
            "fecha, "
            "1 as _id "
    "FROM med_visitas "
    "WHERE nodo  = %@ AND fecha between %@ and %@ "
    
    "UNION "
    
    "SELECT DISTINCT "
        "fecha as fechadisplay, "
        "fecha, "
        "1 as _id "
    "FROM nodo_partediario "
    "WHERE nodo = %@ AND fecha between %@ AND %@ "
    "GROUP BY fechadisplay "
    "ORDER BY fecha";
    
    Ciclo *cicloActual = [Ciclo actual];
    
    NSString *sFechaD = [NSString floatToStr:[cicloActual.desde timeIntervalSince1970]];
    NSString *sFechaH = [NSString floatToStr:[cicloActual.hasta timeIntervalSince1970]];
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       [NSString string:sFechaD ifNull:@"0"],
                       [NSString string:sFechaH ifNull:@"100000000"],
                       nodo,
                       [NSString string:sFechaD ifNull:@"0"],
                       [NSString string:sFechaH ifNull:@"100000000"]];
    

#ifdef DEBUG_DETALLEPARTE
    NSLog(@"--- Detalle Parte Diario - Fechas ---");
    NSLog(@" - query: %@", query);
#endif
    
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            NSString *o = @"";
            NSString *d = @"";
            
            // fechadisplay
            _c = (char *)sqlite3_column_text(statement, 0);
            d = [NSString pCharToString:_c];
            
            o = [[NSDate datetimeFromDB:d] formattedStringUsingFormat:@"dd/MM/yyyy"];
            
            [result addObject:o];
        }
        
	} else {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);
    
    if ([result count] > 0)
    {
        [result insertObject:@"Todo" atIndex:0];
    }
    
    return result;
}

+ (NSArray*)getForFechaOrNil:(NSDate*)fecha
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSString       *sFecha = @"";
    
    
    
    Ciclo *cicloActual = [Ciclo actual];
    
    NSString *sFechaD = [NSString floatToStr:[cicloActual.desde timeIntervalSince1970]];
    NSString *sFechaH = [NSString floatToStr:[cicloActual.hasta timeIntervalSince1970]];;
    
    // Si es una fecha váída la convierto a string.
    if (fecha)
    {
        sFecha = [NSString stringWithFormat:@" AND fecha = %f ", [fecha timeIntervalSince1970]];
    } else
    {
        sFecha = [NSString stringWithFormat:@" AND fecha BETWEEN %@ AND %@ ", sFechaD, sFechaH];
    }
    
    NSString *sql =
    @"select 0 as _id, fecha as orderfecha, fecha as fechad, "
    "case m.actividad "
    "when 'T' then 'TARDE ' "
    "when 'M' then 'MAÑANA' "
    "end as actividad, "
    "m.apellido || \", \" || m.nombre as apenom, "
    "t.descripcion as descripcion "
    "from med_visitas v inner join "
    "med_medicos m on (m.id = v.medico and m.nodo = v.nodo) inner join 	"
    "mtc_tiposvisita t on (t.id = v.tipo)"
    "where v.nodo = %@ %@"
    
    "union "
    
    "SELECT 0 as _id, p.fecha as orderfecha, p.fecha as fechad, "
    "case "
    "when t.actividad = 'M' then 'MAÑANA' "
    "else 'TARDE' "
    "end as actividad, "
    "' ' as apenom, "
    "(mt.descripcion ||  ' (' || t.cantidad || ')' ) as descripcion "
    "FROM nodo_tareasParteDiario t "
    " inner join "
    "nodo_partediario p on p.id = t.partediario and p.nodo = t.nodo inner join "
    "mtc_tareas mt on mt.id = t.tarea "
    "where p.nodo = %@ and t.tarea <> 9999 %@ "
    "order by orderfecha";
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       [NSString string:sFecha ifNull:@""],
                       nodo,
                       [NSString string:sFecha ifNull:@""]];
    
#ifdef DEBUG_DETALLEPARTE
    NSLog(@"--- Detalle Parte Diario ---");
    NSLog(@" - query: %@", query);
#endif

    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTstdDetalleParte *o = [[MTstdDetalleParte alloc] init];
            
            // fecha
            _c = (char *)sqlite3_column_text(statement, 2);
            o.fecha = [[NSDate datetimeFromDB:[NSString pCharToString:_c]] formattedStringUsingFormat:@"dd/MM/yyyy"];
            
            // actividad
            _c = (char *)sqlite3_column_text(statement, 3);
            o.actividad = [NSString pCharToString:_c];
			
            // nombre
            _c = (char *)sqlite3_column_text(statement, 4);
            o.nombre = [NSString pCharToString:_c];
			         
            // descripcion
            _c = (char *)sqlite3_column_text(statement, 5);
            o.descripcion = [NSString pCharToString:_c];
			         
            [result addObject:o];
        }
        
	} else {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}

@end
