//
//  ServiceVerificarExisteNovedad.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Service.h"

@interface ServiceVerificarExisteNovedad : Service

-(NSInteger) verificar:(NSString*)urlBase;

@end
