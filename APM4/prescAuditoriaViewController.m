//
//  prescAuditoriaViewController.m
//  APM4
//
//  Created by Laura Busnahe on 3/4/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "prescAuditoriaViewController.h"
#import "popListView.h"
#import "MTAuditorias.h"
#import "MTMercados.h"


#pragma mark -
#pragma mark prescAuditoriaViewController implementation

@implementation prescAuditoriaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.idMedico = 0;
        self.delegate = nil;
        self.arrayAuditorias =  [MTAuditorias getAll];
        self.arrayMercados = nil;
        
        self.auditoriaSelection = nil;
    }
    return self;
}

// TODO: Seria bueno que todas las vistas implementen este método para que ella misma sepa que XIB le corresponde.
- (id)initViewDefaultNib
{
    self = [self initWithNibName:@"prescAuditoriaViewController" bundle:nil];
    return self;
}

- (id)initViewDefaultNibForSelection:(MTAuditorias*)selAuditoria mercadosArray:(NSArray*)mercados
{
    self = [self initViewDefaultNib];
    if (self)
    {
        self.auditoriaSelection = selAuditoria;
        self.arrayMercados      = mercados;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Chequeo si se inició con alguna selección.
    if (self.auditoriaSelection)
    {
        self.edtAuditoria.text = [self.auditoriaSelection getDescription];
        // Chequeo si estan cargados los mercados.
        if (self.arrayMercados)
        {
            // Actualizo los datos de la tabla.
            [self.tblMercados reloadData];
        } else
        {
            // Cargo los datos de los mercados.
            [self updateMercadosTable];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark UIAlertView related methods

- (void)showAlert:(NSString*)mensaje Titulo:(NSString*) titulo
{
    // Llama al AlertView de confirmación antes de eliminar el previsto seleccionado.
   	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titulo
                                                    message:mensaje
                                                   delegate:nil
                                          cancelButtonTitle:@"Aceptar"
                                          otherButtonTitles:nil];
	[alert show];
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // buttonIndex = 0 : No
    
    // buttonIndex = 1 : Si
}

#pragma mark -
#pragma mark Buttons methods

- (IBAction)btnTodos:(id)sender
{
    // Selecciono todos los mercados.
//    for (MTMercados* m in self.arrayMercados)
//        m.selected = true;

    // Deselecciono todos los mercados.
    [self tapLimpiar:sender];
    
    // llamo a la misma rutina que el botón seleccionar.
    [self btnSeleccion:sender];
}

- (IBAction)btnSeleccion:(id)sender
{
    // Chequeo que se haya seleccionado una auditoría.
    if (!self.auditoriaSelection)
    {
        [self showAlert:@"Debe seleccionar la auditoría y al menos un mercado de la lista."
                 Titulo:@"Advertencia"];
        return;
    }
    
    if ([self.arrayMercados count] == 0)
    {
        [self showAlert:@"Esta auditoría no posee datos del médico."
                 Titulo:@"Advertencia"];
        return;
    }
    
    // Chequeo que al menos se haya seleccionado un mercado.
    if (sender != self.oBtnTodos)
    {
        // Chequeo si hay selección.
        BOOL anySelected = NO;
        for (MTMercados* m in self.arrayMercados)
            if (m.selected) anySelected = YES;
        
        if (!anySelected)
        {
            [self showAlert:@"Debe seleccionar al menos un mercado de la lista."
                     Titulo:@"Advertencia"];
            return;
        }
    }
    
    // Si el delegate está asignado, llamo al método del protocolo.
    if (self.delegate)
    {
        [self.delegate prescAuditoriaView:self
                        selectedAuditoria:self.auditoriaSelection
                             mercadosList:self.arrayMercados
                                   action:kMTEAChange];
    }
    
    // Cierro la vista.
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)btnCancelar:(id)sender
{
    // Cierro la vista.
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)tapLimpiar:(id)sender
{
    for (MTMercados *o in self.arrayMercados)
    {
        o.selected = false;
    }
    
    [self.tblMercados reloadData];
}

- (IBAction)tapAuditoria:(id)sender
{
    // Obtengo la vista desde la cual se llama al popover.
    UIView* popRectView = (UIView*)sender;
    CGSize popOverSize = CGSizeMake(350, 250);
    
    // Creo el popover del menú.
    if (!self.popMenu)
    {
        self.popMenu = [[MTpopListView alloc] initWithArray:self.arrayAuditorias Rect:CGRectMake(0, 0, popOverSize.width, popOverSize.height)];
        self.popMenu.delegate = self;
    }
    [self.popMenu dismissPopoverAnimated:NO];
    
    [self.popMenu setTitleAndParameters:@"Auditorías" rowData:self.arrayAuditorias];
    [self.popMenu setSize:popOverSize];
    [self.popMenu useSearch:YES];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x,
                               popRectView.frame.origin.y,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    [self.popMenu presentPopoverFromRectWithSender:popSourceRect
                                            Sender:(id)self.edtAuditoria
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:YES];
}


#pragma mark -
#pragma mark Table methods

- (void)updateMercadosTable
{
    // Obtengo el listado de mercados según la auditoria seleccionada.
    if (self.auditoriaSelection)
    {
        // Obtengo el listado de mercados de la auditoría seleccionada.
        //self.arrayMercados =  [MTMercados  getByAuditoria:self.auditoriaSelection.RecID];
        NSInteger idAuditoria = self.auditoriaSelection.RecID;
        NSInteger idMedico    = self.idMedico;
        self.arrayMercados =  [MTMercados  getForMedico:idMedico Auditoria:idAuditoria];
       
        // Actualizo el tableView con el listado de mercados.
        [self.tblMercados reloadData];
    }
}

- (void)fillCell:(UITableViewCell*)cell atIndex:(NSIndexPath*)indexPath
{
    NSString* cellText = @"";
    MTMercados *o = [self.arrayMercados objectAtIndex:indexPath.row];
    cellText = o.descripcion;
    
    
	// Set up the cell content.
	cell.textLabel.text = [NSString stringWithFormat:@"%@", cellText];
    if (!o.selected)
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    else
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of time zone names.
    
    return self.arrayMercados.count;
//    return 4;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
// fixed font style. use custom view (UILabel) if you want something different
//{
//    return self.Title;
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UILabel *label = [[UILabel alloc] init];
//    label.text = self.popTitle;
//    label.textColor = [UIColor whiteColor];
//    label.font = [UIFont boldSystemFontOfSize:14.0f];
//    label.backgroundColor = [UIColor colorWithRed:0.09f green:0.3f blue:0.64f alpha:1.0f];
//    
//    return label;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MyIdentifier = @"MyIdentifier";
    
    // Obtain the cell object.
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        // Use the default cell style.
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    }
    
    // Relleno el contenido de la celada.
    [self fillCell:cell atIndex:indexPath];
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    MTMercados *o = [self.arrayMercados objectAtIndex:indexPath.row];
    
    o.selected = !o.selected;
    [self fillCell:cell atIndex:indexPath];
}


#pragma mark-
#pragma mark popListView related methods
- (void)popListView:(MTpopListView *)controller senderControl:(id)sender didSelectRow:(NSInteger *)MenuIndex selectedText:(NSString *)RowText
{
    // Guardo la selección
    self.auditoriaSelection = [self.arrayAuditorias objectAtIndex:MenuIndex];
    self.edtAuditoria.text = RowText;
    
    // Actualizo el listado de mercados.
    [self updateMercadosTable];
}

@end
