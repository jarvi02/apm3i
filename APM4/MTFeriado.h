//
//  MTFeriado.h
//  APM4
//
//  Created by Juan Pablo Garcia on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTFeriado : NSObject

@property(nonatomic, retain) NSDate *fecha;
@property(nonatomic, retain) NSString *descripcionFeriado;
@property(nonatomic) BOOL activo;

+(NSArray *)getAll;
+(BOOL)isFeriado:(NSDate *)date;
+(BOOL)isDate:(NSDate*)date intoArray:(NSArray*)array;

@end
