//
//  NSDateExtended.h
//  APM4
//
//  Created by Laura Busnahe on 3/27/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (extended)

- (id) initWithTimeString:(NSString*)sTime;
- (id) initWithString:(NSString*)sTime withFormat:(NSString*)dFormat;


-(NSDate *)     addDays:(NSUInteger) daysToAdd;
+(NSDate *)     dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;
-(BOOL)         isWeekEnd;
+(NSDate *)     dateWithoutTime;
-(NSDate *)     dateAsDateWithoutTime;
-(int)          differenceInDaysTo:(NSDate *)toDate;
-(NSString *)   formattedDateString;
-(NSString *)   formattedStringUsingFormat:(NSString *)dateFormat;

- (NSString*)   datetimeForNovedad;
- (double)      datetimeForDB;
- (NSString*)   datetimeForDBAsString;
- (NSString*)   timeToString;

- (NSDate*)     substractDays:(NSUInteger) dayAmount;



+ (NSDate*)     stringToTime:(NSString*)sTime;
+ (NSDate*)     stringToTime:(NSString*)sTime withFormat:(NSString*)dFormat;


- (NSString*)   datetimeAsSQLiteString;
+ (NSDate*)     datetimeFromSQLiteString:(NSString*)sDate;

+ (NSDate*)datetimeFromDB:(NSString*)sDate;
// A esta función se ingresa con el texto leido desde la base de datos. La función chequea si es una fecha en milisegundos o
// en formato de texto, y lo covierte automáticamente a NSDate.

+ (BOOL)uses24Hs;
@end


