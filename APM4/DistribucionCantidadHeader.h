//
//  DistribucionCantidadHeader.h
//  APM4
//
//  Created by Laura Busnahe on 9/9/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DistribucionCantidadHeader;

typedef enum
{
    fDistribucionCantidadFieldDescripcion,
    fDistribucionCantidadFieldManiana,
    fDistribucionCantidadFieldPorcManiana,
    fDistribucionCantidadFieldTarde,
    fDistribucionCantidadFieldPorcTarde,
    fDistribucionCantidadFieldTotal,
    fDistribucionCantidadFieldPorcTotal
} tDistribucionCantidadHeaderField;

@protocol MTdistribucionCantidadHeaderDelegate <NSObject>
@required
// Sent when the user tap a field.
- (void)DistribucionCantidadHeader:(DistribucionCantidadHeader*)header
                    selectedField:(tDistribucionCantidadHeaderField)field
                            Order:(NSComparisonResult)order;
@end


@interface DistribucionCantidadHeader : UIView

@property (nonatomic, assign) id <MTdistribucionCantidadHeaderDelegate> delegate;

@property (retain, nonatomic) IBOutlet UIButton *btnDescripcion;
@property (retain, nonatomic) IBOutlet UIButton *btnManiana;
@property (retain, nonatomic) IBOutlet UIButton *btnPorcManiana;
@property (retain, nonatomic) IBOutlet UIButton *btnTarde;
@property (retain, nonatomic) IBOutlet UIButton *btnPorcTarde;
@property (retain, nonatomic) IBOutlet UIButton *btnTotal;
@property (retain, nonatomic) IBOutlet UIButton *btnPorcTotal;


- (void)orderBy:(tDistribucionCantidadHeaderField)field sender:(id)sender reset:(BOOL)reset;

- (IBAction)tapLabel:(id)sender;

@end
