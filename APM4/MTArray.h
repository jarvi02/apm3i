//
//  MTArray.h
//  APM4
//
//  Created by Laura Busnahe on 3/20/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTArray : NSArray

@end
