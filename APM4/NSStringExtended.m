//
//  NSStringExtended.m
//  APM4
//
//  Created by Laura Busnahe on 4/18/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "NSStringExtended.h"

@implementation NSString (extensions)

-(BOOL)isValidIP {
    
    NSString *urlRegEx = @"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    NSPredicate *ipTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [ipTest evaluateWithObject:self];
    
}

-(BOOL)isNumber{
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return ([self rangeOfCharacterFromSet:notDigits].location == NSNotFound);
}

+ (NSString*)pCharToString:(char*)_c
{
    NSString *result = @"";
    
    if((_c != nil) && (strlen(_c) > 0)) {
        result  = [NSString stringWithUTF8String:_c];
    }

    return result;
}


+ (NSString*)stringQuoted:(NSString*)s ifNull:(NSString*)nullS
{
    NSString *result = nullS;
    
#ifdef USE_DOUBLEQUOTE
    if ((s) && ([s length] > 0))
    {
        if ([s length] > 1)
            result = [NSString stringWithFormat:@"\"%@\"", s];
        else
            result = [NSString stringWithFormat:@"'%@'", s];
    }
#else
    if ((s) && ([s length] > 0))
        result = [NSString stringWithFormat:@"'%@'", s];
#endif
    
    return result;
}


+ (NSString*)string:(NSString*)s
{
    NSString *result = @"";
    
    if ((s) && ([s length] > 0))
        result = s;
    
    return result;
}

+ (NSString*)string:(NSString*)s ifNull:(NSString*)nullS
{
    NSString *result = nullS;
    
    if ((s) && ([s length] > 0))
        result = s;
    
    return result;
}


+ (NSString*)intToStr:(NSInteger)intVal
{
    return [NSString stringWithFormat:@"%d", intVal];
}

+ (NSString*)intToStr:(NSInteger)intVal ifZero:(NSString*)zeroString
{
    if (intVal == 0)
        return zeroString;
    else
        return [NSString stringWithFormat:@"%d", intVal];
}

+ (NSString*)floatToStr:(double)val
{
    return [NSString stringWithFormat:@"%.02f", val];
}

@end
