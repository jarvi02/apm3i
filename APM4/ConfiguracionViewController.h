//
//  ConfiguracionViewController.h
//  APM4
//
//  Created by Juan Pablo Garcia on 14/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"

@interface ConfiguracionViewController : CustomViewController <UIAlertViewDelegate, UITextFieldDelegate>
{
    BOOL inicializar;
    BOOL inicializarChanged;
    BOOL modificar;

}

@property (retain, nonatomic) IBOutlet UIButton *btnInicializarOverall;
@property (retain, nonatomic) IBOutlet UIButton *btnModificarOverall;
@property (nonatomic, retain) IBOutlet UIButton *inicializarButton;
@property (nonatomic, retain) IBOutlet UIButton *modificarButton;

@property (nonatomic, retain) IBOutlet UITextField *servidor;
@property (nonatomic, retain) IBOutlet UITextField *puerto;
@property (nonatomic, retain) IBOutlet UITextField *entidad;
@property (nonatomic, retain) IBOutlet UITextField *ciclo;
@property (nonatomic, retain) IBOutlet UITextField *pais;
@property (retain, nonatomic) IBOutlet UITextField *servidor2;
@property (retain, nonatomic) IBOutlet UITextField *puerto2;

@property (nonatomic, retain) IBOutlet UILabel *servidorlbl;
@property (nonatomic, retain) IBOutlet UILabel *puertolbl;
@property (nonatomic, retain) IBOutlet UILabel *entidadlbl;
@property (nonatomic, retain) IBOutlet UILabel *ciclolbl;
@property (nonatomic, retain) IBOutlet UILabel *paislbl;
@property (retain, nonatomic) IBOutlet UILabel *servidor2lb;
@property (retain, nonatomic) IBOutlet UILabel *puerto2lb;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIButton *container;

@property (nonatomic, retain) NSString *claveConfig;

-(IBAction)pressedInicializar:(id)sender;
-(IBAction)pressedModificar:(id)sender;
-(IBAction)pressedGuardar:(id)sender;
-(IBAction)pressedCancelar:(id)sender;


@end
