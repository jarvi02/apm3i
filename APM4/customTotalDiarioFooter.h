//
//  customTotaldiarioFooter.h
//  APM4
//
//  Created by Laura Busnahe on 8/6/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customTotalDiarioFooter : UIView

@property (retain, nonatomic) IBOutlet UIButton *lblDescripcion;
@property (retain, nonatomic) IBOutlet UIButton *lblCantM;
@property (retain, nonatomic) IBOutlet UIButton *lblCantT;
@property (retain, nonatomic) IBOutlet UIButton *lblMedM;
@property (retain, nonatomic) IBOutlet UIButton *lblMedT;
@property (retain, nonatomic) IBOutlet UIButton *lblCantidad;
@property (retain, nonatomic) IBOutlet UIButton *lblTotal;
@end
