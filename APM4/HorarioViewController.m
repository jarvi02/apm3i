//
//  HorarioViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "HorarioViewController.h"
#import "Horario.h"
#import "NSStringExtended.h"

@interface HorarioViewController ()
@property(nonatomic, retain) NSMutableArray *horariosOriginales;
@property(nonatomic, retain) Horario *horarioAux;
-(void)crearToolBar;

@end

@implementation HorarioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)showAlert:(NSString*)mensaje Titulo:(NSString*) titulo
{
    // Llama al AlertView de confirmación antes de eliminar el previsto seleccionado.
   	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titulo
                                                    message:mensaje
                                                   delegate:nil
                                          cancelButtonTitle:@"Aceptar"
                                          otherButtonTitles:nil];
	[alert show];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.horarioAux = [[Horario alloc] init];

    self.horariosOriginales = [NSMutableArray array];
    for (Horario *o in self.domicilioReference.horarios) {
        [self.horariosOriginales addObject:[o copy]];
    }
    
    [self crearToolBar];

    
}

-(void)crearToolBar {
    
    // flex item used to separate the left groups items and right grouped items
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    // create a special tab bar item with a custom image and title
    UIBarButtonItem *guardarItem = [[UIBarButtonItem alloc] initWithTitle:@"Guardar"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(guardar:)];
    //guardarItem.tintColor = [UIColor grayColor];
    guardarItem.width = 70;
    
    UIBarButtonItem *editarItem = [[UIBarButtonItem alloc] initWithTitle:@"Editar"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(editar:)];
    //editarItem.tintColor = [UIColor grayColor];
    editarItem.width = 70;
    
    UIBarButtonItem *cancelarItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar"
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self
                                                                    action:@selector(cancelar:)];
    //cancelarItem.tintColor = [UIColor grayColor];
    cancelarItem.width = 70;

    UIBarButtonItem *agregarItem = [[UIBarButtonItem alloc] initWithTitle:@"Agregar"
                                                                    style:UIBarButtonItemStyleDone
                                                                    target:self
                                                                    action:@selector(agregar:)];

    //cancelarItem.tintColor = [UIColor grayColor];
    agregarItem.width  = 70;
    

    
    
    NSArray *items = [NSArray arrayWithObjects: guardarItem, editarItem, cancelarItem, flexItem, agregarItem, nil];
    [self.toolBar setItems:items animated:NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)lunes:(id)sender {
    
    if (self.horarioAux.lunes)
    {
        self.horarioAux.lunes = NO;
        [self.btnLunes setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    } else {
        self.horarioAux.lunes = YES;
        [self.btnLunes setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    }
    
}

- (IBAction)martes:(id)sender {

    if (self.horarioAux.martes) {
        self.horarioAux.martes = NO;
        [self.btnMartes setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    } else {
        self.horarioAux.martes = YES;
        [self.btnMartes setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    }

}

- (IBAction)miercoles:(id)sender {

    if (self.horarioAux.miercoles) {
        self.horarioAux.miercoles = NO;
        [self.btnMiercoles setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    } else {
        self.horarioAux.miercoles = YES;
        [self.btnMiercoles setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    }

}

- (IBAction)jueves:(id)sender {

    if (self.horarioAux.jueves) {
        self.horarioAux.jueves = NO;
        [self.btnJueves setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    } else {
        self.horarioAux.jueves = YES;
        [self.btnJueves setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    }

}

- (IBAction)viernes:(id)sender {

    if (self.horarioAux.viernes) {
        self.horarioAux.viernes = NO;
        [self.btnViernes setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    } else {
        self.horarioAux.viernes = YES;
        [self.btnViernes setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    }

}


-(void)guardar:(id)sender{
    
    
    // agrego la coleccion de la tabla a los medicos
    if ([self.domicilioReference.horarios count] >0) {
        Horario *o = [self.domicilioReference.horarios objectAtIndex:0];
        self.abmMedicoReference.txtHorario.text = [o descripcion];
        self.domicilioViewControllerReference.txtHorario.text =[o descripcion];
    } else {
        self.abmMedicoReference.txtHorario.text = @"";
    }
    
    [self.delegate closeModalView];
}
-(void)cancelar:(id)sender{
    
    [self.domicilioReference.horarios removeAllObjects];
    
    for (Horario *o in self.horariosOriginales) {
        [self.domicilioReference.horarios addObject:[o copy]];
    }
    
    [self.delegate closeModalView];
    
}

-(void)editar:(id)sender{
    
    
    if (self.tableview.editing == NO) {
        [self.tableview setEditing:YES animated:YES];
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:1]).tintColor = UIColorFromRGB(defaultEditModeButtonColor);
        
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:0]).enabled =NO;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:2]).enabled =NO;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:4]).enabled =NO;
        
        
    } else {
        [self.tableview setEditing:NO animated:YES];
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:1]).tintColor = ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:2]).tintColor;
        
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:0]).enabled =YES;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:2]).enabled =YES;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:4]).enabled =YES;
        
    }
    
    
}


-(void)agregar:(id)sender{
    
    if ([self.txtHoraDesde.text length] < 1 ){
        [self showAlert:@"Debe ingresar una hora para el Desde." Titulo:@"Atención"];
        return;
    }   
    
   if ([self.txtHoraHasta.text length] < 1 ){
        [self showAlert:@"Debe ingresar una hora para el Hasta." Titulo:@"Atención"];
        return;
    }
    
    self.horarioAux.iHrDesde = [self.txtHoraDesde.text integerValue];
    self.horarioAux.iHrHasta = [self.txtHoraHasta.text integerValue];
    
    self.horarioAux.iMinDesde = [self.txtMinutoDesde.text integerValue];
    self.horarioAux.iMinHasta = [self.txtMinutoHasta.text integerValue];
    
    self.txtMinutoDesde.text = [NSString stringWithFormat:@"%02d", self.horarioAux.iMinDesde];
    self.txtMinutoHasta.text = [NSString stringWithFormat:@"%02d", self.horarioAux.iMinHasta];
    
    self.horarioAux.comentarios = [NSString string:self.txtComentarios.text ifNull:@""];
    
    NSString *errMsg = [self.horarioAux isOkForSave];
    if ([errMsg length] > 0)
    {
        [self showAlert:errMsg Titulo:@"Atención"];
        return;
    }
    

    
    Horario *h = [self.horarioAux copy];
    
//    h.comentarios = [NSString stringWithFormat:@"%@", self.txtComentarios.text];
//    h.horaDesde = [NSString stringWithFormat:@"%@:%@", self.txtHoraDesde.text, self.txtMinutoDesde.text];
//    h.horaHasta = [NSString stringWithFormat:@"%@:%@", self.txtHoraHasta.text, self.txtMinutoHasta.text];
    
    [self.domicilioReference.horarios addObject:h];

    
    
    // Creo un nuevo objeto para el próximo horario a agregar.
    self.horarioAux = [[Horario alloc] init];
    
    self.txtComentarios.text = @"";
    self.txtHoraDesde.text = @"";
    self.txtMinutoDesde.text = @"";
    self.txtHoraHasta.text = @"";
    self.txtMinutoHasta.text = @"";
    
    [self.btnLunes      setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnMartes     setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnMiercoles  setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnJueves     setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnViernes    setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    
    
    [self.tableview reloadData];
}


#pragma - mark
#pragma UItableView Delegate and DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.domicilioReference.horarios count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    Horario *h = [self.domicilioReference.horarios objectAtIndex:indexPath.row];
    cell.textLabel.text = [h descripcion];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    cell.textLabel.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    cell.detailTextLabel.text = h.comentarios;
    cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.domicilioReference.horarios removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
    
    Horario *ingredient = [self.domicilioReference.horarios objectAtIndex:fromIndexPath.row];
    
    ingredient = [ingredient copy];
    
    [self.domicilioReference.horarios removeObjectAtIndex:fromIndexPath.row];
    
    [self.domicilioReference.horarios insertObject:ingredient atIndex:toIndexPath.row];
    
    
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Horario *o = [self.domicilioReference.horarios objectAtIndex:indexPath.row];
    
    self.horarioAux = [o copy];
    
    self.txtComentarios.text = o.comentarios;
    self.txtHoraDesde.text = [[o.horaDesde componentsSeparatedByString:@":"] objectAtIndex:0];
    self.txtMinutoDesde.text = [[o.horaDesde componentsSeparatedByString:@":"] objectAtIndex:1];
    self.txtHoraHasta.text = [[o.horaHasta componentsSeparatedByString:@":"] objectAtIndex:0];
    self.txtMinutoHasta.text = [[o.horaHasta componentsSeparatedByString:@":"] objectAtIndex:1];

    
    [self.btnLunes      setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnMartes     setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnMiercoles  setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnJueves     setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnViernes    setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    

    if (o.lunes)
        [self.btnLunes setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    
    if (o.martes)
        [self.btnMartes setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    
    if (o.miercoles)
        [self.btnMiercoles setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    
    if (o.jueves)
        [self.btnJueves setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    
    if (o.viernes)
        [self.btnViernes setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    
}

#pragma mark -
#pragma mark TextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

// En vistas que se presentan de forma modal es necesario agregar este override para que oculte el teclado
// con resignFirstResponder
- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

-(BOOL) textFieldShouldBeginEditing:(UITextField*)textField
{
    return YES;
}

- (BOOL)textFieldOLD:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //BOOL _isAllowed = YES;
    // return NO to not change text
    
 
    if ([string length] == 0 && range.length > 0)
    {
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return NO;
    }
    
    if ((textField == self.txtHoraDesde) ||
        (textField == self.txtHoraHasta) ||
        (textField == self.txtMinutoDesde) ||
        (textField == self.txtMinutoHasta))
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        if ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0)
            return YES;
        
        return NO;
    }

    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    if ([string stringByTrimmingCharactersInSet:notAllowedSet].length > 0)
        return YES;
    
    return NO;
    
//    NSString *oldString = [NSString string:textField.text ifNull:@""];
//    
//    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string] stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
//    
//    
//    if ([oldString isEqualToString:tempString])
//    {
//        _isAllowed =  NO;
//    }
//    
//    return   _isAllowed;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    NSString *oldString = [NSString string:textField.text ifNull:@""];
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    if ((textField == self.txtHoraDesde) ||
        (textField == self.txtHoraHasta) ||
        (textField == self.txtMinutoDesde) ||
        (textField == self.txtMinutoHasta))
    {
        notAllowedSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    }
    
    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
                            stringByTrimmingCharactersInSet:notAllowedSet];
    
    if ([oldString isEqualToString:tempString])
        return NO;
    
    return   YES;
}

@end
