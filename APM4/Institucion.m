//
//  Institucion.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Institucion.h"
#import "DB.h"

@implementation Institucion


- (id)init
{
    self = [super init];
    if (self) {
        self.descripcion = @"";
        self.calle = @"";
        self.localidad = @"";
        self.codigoPosta = @"";
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [_calle release];
    [_localidad release];
    [_codigoPosta release];
    
    [super dealloc];
}

- (NSString*)getDescription
{
    return [NSString stringWithFormat:@"%@", self.descripcion];
}

+(NSArray*)GetAllByTipo:(NSInteger)tipo{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"select descripcion, pais, provincia, calle, altura, tipodomicilio, desclocalidad, cp, id from mtc_instituciones where tipodomicilio = %d  order by descripcion;";
    
    NSString *query = [NSString stringWithFormat:sql, tipo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        Institucion *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[Institucion alloc] init];
            
            _c = (char *)sqlite3_column_text(statement, 0);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
    
            o.pais = sqlite3_column_int(statement, 1);
            o.provincia = sqlite3_column_int(statement, 2);
            
            _c = (char *)sqlite3_column_text(statement, 3);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.calle = [NSString stringWithUTF8String:_c];
			}
            
            o.altura = sqlite3_column_int(statement, 4);
            o.tipoDomicilio = sqlite3_column_int(statement, 5);
            
            _c = (char *)sqlite3_column_text(statement, 6);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.localidad = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 7);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.codigoPosta = [NSString stringWithUTF8String:_c];
			}
            
            o.recID = sqlite3_column_int(statement, 8);
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
}


-(id)copy{
    
    Institucion *o = [[Institucion alloc] init];

    o.descripcion = [NSString stringWithFormat:@"%@", self.descripcion];
    o.pais = self.pais;
    o.provincia = self.provincia;
    o.calle = [NSString stringWithFormat:@"%@", self.calle];
    o.altura = self.altura;
    o.tipoDomicilio = self.tipoDomicilio;
    o.localidad = [NSString stringWithFormat:@"%@", self.localidad];
    o.codigoPosta = [NSString stringWithFormat:@"%@", self.codigoPosta];
    o.recID = self.recID;
    
    return [o autorelease];
}

@end
