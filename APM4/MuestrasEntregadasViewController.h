//
//  MuestrasEntregadasViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomViewController.h"
#import "MuestrasEntregadasCell.h"
@interface MuestrasEntregadasViewController : CustomViewController


@property (nonatomic, retain) NSArray               *data;
@property (retain, nonatomic) IBOutlet UITableView              *table;
@property (retain, nonatomic) IBOutlet MuestrasEntregadasCell   *tmpCell;
@property (nonatomic, retain) UINib                             *cellNib;

@end
