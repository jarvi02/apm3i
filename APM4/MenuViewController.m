//
//  MenuViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MenuViewController.h"
#import "AppDelegate.h"
#import "UIViewController+PopOverSize.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

@synthesize parent;
@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
        self.parent = nil;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /*
    // Only for back
    if (self.forceResize)
    {
        [self forcePopoverSize];
        self.forceResize = NO;
    }*/
    
}

-(void)viewWillAppear:(BOOL)animated{
    CGSize popSize = CGSizeMake(400.0f, [self getEstimatedHeight]);
    self.contentSizeForViewInPopover = popSize;
    self.preferredContentSize = popSize;
    Delegate.popoverController.popoverContentSize = popSize;
    [super viewWillAppear:animated];
}


#pragma mark Public Methods

-(NSArray *)options
{
    return [NSArray array];
}

-(BOOL)hasSubMenus
{
    return NO;
}

-(BOOL)hasIcons
{
    return NO;
}

-(float)getEstimatedHeight{
    return (44 * [[self options] count]) + 44;
}

-(void)selectedOptionsAtIndex:(NSUInteger)index
{
    // only in child
}

-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index {
    return NO;
}

-(NSString *)title
{
    return [NSString string];
}

-(CGSize)size
{
    return CGSizeMake(0.0f, 0.0f);
}

-(void)navigateToSubMenu:(MenuViewController *)subMenu
{
    //subMenu.contentSizeForViewInPopover = [subMenu size];
    subMenu.parent = self;
    subMenu.delegate = self.delegate;
    [self.navigationController pushViewController:subMenu animated:YES];
}

-(NSString *)imageNameForIndexPath:(NSIndexPath *)indexPath
{
    return @"";
}

-(void)navigateToController:(UIViewController *)controller
{
    if (self.delegate != nil)
    {
        [self.delegate navigateToController:controller];
    }
}

-(void)showModalController:(UIViewController *)controller
{
    if (self.delegate != nil)
    {
        //controller.preferredContentSize = self.navigationController.topViewController.view.frame.size;
        [self.delegate showModalController:controller];
    }
}

-(void)showNotImplemented
{
    UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"Función disponible en próxima release" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    
    [av show];
}

#pragma mark Autorotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    // Return the number of rows in the section.
    return [[self options] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        //cell.backgroundView = [[[UIView alloc] init] autorelease];
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    
    // Configure the cell...
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    cell.textLabel.textColor    = UIColorFromRGB(defaultInteractiveCellFontColor);
    //cell.backgroundColor        = [UIColor whiteColor];
    cell.textLabel.text         = [[self options] objectAtIndex:indexPath.row];
    
    
    
    if ([self hasSubMenus])
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        //cell.accessoryView.tintColor = UIColorFromRGB(defaultAccesoryColor);
    }
    if ([self hasIcons])
    {
        cell.imageView.image = [UIImage imageNamed:[self imageNameForIndexPath:indexPath]];
    }
    
    // para deshabilitar las entradas que todavia no se hicieron
    if ( ![self isEnableOptionsAtIndex:indexPath.row] ) {
        cell.selectionStyle               = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor          = UIColorFromRGB(defaultDisabledButtonColor);
     }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if ( [self isEnableOptionsAtIndex:indexPath.row] ) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self selectedOptionsAtIndex:indexPath.row];
    }
}

#pragma mark -
#pragma mark Memory Management

-(void)dealloc
{
    [parent release], parent = nil;
    [super dealloc];
}

@end
