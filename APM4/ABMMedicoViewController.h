//
//  ABMMedicoViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomViewController.h"
#import "CarteraViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "AppDelegate.h"
#import "TableForComboPopoverViewController.h"
#import "EntryForCombo.h"
#import "DateSelectorViewController.h"
#import "MTMedico.h"
//#import "MTEditActions.h"
#import "CloseModalViewControllerDelegate.h"


@interface ABMMedicoViewController : CustomViewController<TableForComboPopoverViewControllerDelegate, DateSelectorDelegate, CloseModalViewControllerDelegate, UITextFieldDelegate, listViewDelegate>

@property(nonatomic, assign) CarteraViewController *carteraViewControllerReference;
@property(nonatomic, assign) MTEditActionsType modo;



@property (retain, nonatomic) IBOutlet UILabel *lblCUIT;
@property (retain, nonatomic) IBOutlet UILabel *lblObraSocial;
@property (retain, nonatomic) IBOutlet UILabel *lblMatNacional;
@property (retain, nonatomic) IBOutlet UILabel *lblMatProvincial;



@property(nonatomic, retain) MTMedico      *medico;

@property (retain, nonatomic) IBOutlet UITextField *txtApellido;
@property (retain, nonatomic) IBOutlet UITextField *txtNombre;


@property (retain, nonatomic) IBOutlet UIButton *btnActividadManana;
@property (retain, nonatomic) IBOutlet UIButton *btnActividadTarde;

- (IBAction)performActividadManana:(id)sender;
- (IBAction)performActividadTarde:(id)sender;

@property (retain, nonatomic) IBOutlet UITextField *txtPotencial;
- (IBAction)performPotencial:(id)sender;

@property (retain, nonatomic) IBOutlet UITextField *txtEspecialidad;
- (IBAction)performEspecialidad:(id)sender;


@property (retain, nonatomic) IBOutlet UITextField *txtDomicilio;
- (IBAction)performDomicilio:(id)sender;

@property (retain, nonatomic) IBOutlet UITextField *txtHorario;
- (IBAction)performHorario:(id)sender;

@property (retain, nonatomic) IBOutlet UITextField *txtObreSocial;
- (IBAction)performObraSocial:(id)sender;


@property (retain, nonatomic) IBOutlet UIButton *btnSexoFemenino;
@property (retain, nonatomic) IBOutlet UIButton *btnSexoMasculino;
- (IBAction)performSexoFemenino:(id)sender;
- (IBAction)performSexoMasculino:(id)sender;


@property (retain, nonatomic) IBOutlet UITextField *txtTratamiento;
- (IBAction)performTratamiento:(id)sender;

@property (retain, nonatomic) IBOutlet UITextField *txtFechaNacimiento;
- (IBAction)performFechaNacimiento:(id)sender;


@property (retain, nonatomic) IBOutlet UITextField  *txtAnoEgreso;
@property (retain, nonatomic) IBOutlet UITextField  *txtCUIT;
@property (retain, nonatomic) IBOutlet UITextField  *txtEmail;
@property (retain, nonatomic) IBOutlet UITextField  *txtRevisar;
@property (retain, nonatomic) IBOutlet UITextField  *txtMNacional;
@property (retain, nonatomic) IBOutlet UITextField  *txtMProvincial;
@property (retain, nonatomic) IBOutlet UIButton     *btnHorario;

@property (retain, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;


- (IBAction)performGuardar:(id)sender;
- (IBAction)performCancelar:(id)sender;



-(void) closeModalView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil idMedico:(NSInteger)idMedico andMode:(MTEditActionsType) modo;




@end
