//
//  customPrescripcionesHeader.m
//  APM4
//
//  Created by Ezequiel on 3/20/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "customPrescripcionesHeader.h"


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define colorASC     darkHeaderAscColor
#define colorDESC    darkHeaderDescColor
#define colorNormal  0x000000
//#define colorASC     0x4D6821
//#define colorDESC    0x980021
//#define colorNormal  0x000000

#pragma mark -
@implementation customPrescripcionesHeader
{
    MTHeaderField     orderedBy;
    MTHeaderOrder         order;
}

#pragma mark Private methods
- (id)init
{
    self = [super init];
    if (self)
    {
        self.delegate = nil;
        orderedBy = kMTFieldDescripcion;
        order     = kMTOrderAsc;
    }
    
    return self;
}

- (void)orderBy:(MTHeaderField)field reset:(BOOL)reset
{
    // Chequeo si el delegado fue asignado.
    if (!self.delegate)
        return;
    
    // Chequeo si se trata del mismo campo por el que ya estaba ordenado.
    if (orderedBy != field)
    {
        // Si no es igual, asigno el nuevo campo y cambio el orden a ascendete.
        orderedBy = field;
        order     = kMTOrderAsc;
    } else
    {
        // Si es el mismo campo, cambio el orden.
        if (order == kMTOrderAsc)
            order = kMTOrderDesc;
        else
            order = kMTOrderAsc;
    }
    
    if (reset)
        order = kMTOrderAsc;

    // Coloreo el label seleccionado.
    [self colorLabels:field];
    
    // Llamo el método del delegate.
    [self.delegate customPrescHeader:self selectedField:orderedBy Order:order];
}

- (void)colorLabels:(MTHeaderField)selectedField
{
    NSUInteger newColor = order == kMTOrderAsc ? colorASC : colorDESC;
    
    [self.lblProducto   setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblTrimestre1 setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblTrimestre2 setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblTrimestre3 setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblTrimestre4 setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblTotal      setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblMercado    setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    
    UIButton *o = nil;
    
    switch (selectedField)
    {
        case kMTFieldDescripcion:
            o = self.lblProducto;
            break;
            
        case kMTFieldTrimestre1:
            o = self.lblTrimestre1;
            break;
        
        case kMTFieldTrimestre2:
            o = self.lblTrimestre2;
            break;
        
        case kMTFieldTrimestre3:
            o = self.lblTrimestre3;
            break;
        
        case kMTFieldTrimestre4:
            o = self.lblTrimestre4;
            break;
        
        case kMTFieldTotal:
            o = self.lblTotal;
            break;
            
        case kMTFieldMercado:
            o = self.lblMercado;
            break;
            
        default:
            o = self.lblTrimestre1;
            break;
    }
    
    [o setTitleColor:UIColorFromRGB(newColor) forState:UIControlStateNormal];
    
}

#pragma mark IBAction methods
- (IBAction)tapProducto:(id)sender
{
    [self orderBy:kMTFieldDescripcion reset:NO];
}

- (IBAction)tapTrimestre1:(id)sender
{
    [self orderBy:kMTFieldTrimestre1 reset:NO];
}

- (IBAction)tapTrimestre2:(id)sender
{
    [self orderBy:kMTFieldTrimestre2 reset:NO];
}

- (IBAction)tapTrimestre3:(id)sender
{
    [self orderBy:kMTFieldTrimestre3 reset:NO];
}

- (IBAction)tapTrimestre4:(id)sender
{
    [self orderBy:kMTFieldTrimestre4 reset:NO];
}

- (IBAction)tapTotal:(id)sender
{
    [self orderBy:kMTFieldTotal reset:NO];
}

- (IBAction)tapMercado:(id)sender
{
    [self orderBy:kMTFieldMercado reset:NO];
}



#pragma mark -
@end

