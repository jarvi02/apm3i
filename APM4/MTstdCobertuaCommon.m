//
//  MTstdCobertuaCommon.m
//  APM4
//
//  Created by Laura Busnahe on 7/18/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTstdCobertuaCommon.h"
#import "Config.h"
#import "DB.h"

@implementation MTstdCobertuaCommon

- (id)init
{
    self = [super init];
    if (self)
    {
        self.descripcion = @"";
        self.programados = 0;
        self.fluctuantes = 0;
        self.vistos      = 0;
        self.revisitas   = 0;
        self.porcentaje  = 0.0f;
        self.total       = 0;
    }
    
    return self;
}


+ (NSArray*)getAll:(NSString*)query
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTstdCobertuaCommon *o = [[MTstdCobertuaCommon alloc] init];
            
            // descripción
            _c = (char *)sqlite3_column_text(statement, 1);
            o.descripcion = [NSString pCharToString:_c];
            
            // programados
            o.programados = sqlite3_column_int(statement, 2);
            
            // fluctuantes
            o.fluctuantes = sqlite3_column_int(statement, 3);
            
            // vistos
            o.vistos = sqlite3_column_int(statement, 4);
            
            // revisitas
            o.revisitas = sqlite3_column_int(statement, 5);
            
            // porcentaje
            o.porcentaje = sqlite3_column_double(statement, 6);
            
            // total
            o.total = sqlite3_column_int(statement, 7);
            
            [result addObject:o];
        }
        
	} else {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}
@end
