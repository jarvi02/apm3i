//
//  MTAjustes.m
//  APM4
//
//  Created by Laura Busnahe on 4/4/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTAjustes.h"
#import "GlobalDefines.h"
#import "DB.h"

@implementation MTAjustes

- (id)init
{
    self = [super init];    // Inicializo el objeto.
    
    if (self)               // Chequeo si se inicializó correctamente.
    {                       // Si es así asigno los valores por defecto a las propiedades.
        self.recID       = 0;
        self.descripcion = @"";
        self.operacion   = 0;
        self.activo      = NO;
        self.visibleAPM  = NO;
        
        self.cantidad    = 0;
    }
    
    return self;
}

- (NSString*)getDescription
{
    NSString* newDescription = [NSString stringWithFormat:@"%@",self.descripcion];
    
    return newDescription;
}


+ (NSArray*)getAll
{
    // Inicializo el objeto que devolverá el array de Ajustes.
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    //select id, descripcion from dpm_auditorias
    NSString *sql = @"SELECT id, descripcion, operacion, activo, visible_apm "
                     "FROM mtc_ajustes "
                     "WHERE (activo = 1) AND (visible_apm > 0) "
                     "ORDER BY id";
    
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    MTAjustes *o = nil;
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            o = [[MTAjustes alloc] init];
            
            // id
            o.recID = sqlite3_column_int(statement, 0);
            
            // Descripcion
            char *_descripcion = (char *)sqlite3_column_text(statement, 1);
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o.descripcion = [NSString stringWithUTF8String:_descripcion];
            
            // Operación
            o.operacion = sqlite3_column_int(statement, 2);

            // Activo.
            o.activo =  YES;
            
            // Operación
            o.visibleAPM = YES;
            
            [result addObject:o];
        }
        
	} else {
        NSAssert(0, @"ERROR: No se pudo ejecutar: %@", sql);
	}
	sqlite3_finalize(statement);
    
    return result;
}

@end
