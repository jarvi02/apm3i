//
//  FileMenuViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "FileMenuViewController.h"
#import "DoctorsMenuViewController.h"
#import "CarteraViewController.h"
#import "PrevistoMensualViewController.h"
#import "ParteDiarioViewController.h"
#import "GruposViewController.h"
#import "selectGrupoController.h"
#import "SeleccionZonaViewController.h"
#import "Config.h"

@interface FileMenuViewController ()

-(void)navigateToDoctors;
-(void)navigateToPrevistoMensual;
-(void)navigateToParteDiario;

@end

@implementation FileMenuViewController


-(NSArray *)options
{
    return [NSArray arrayWithObjects:
            @"Cartera de Médicos",
            @"Selección de Grupos",
            @"Grupos",
            @"Seleccionar Zona",
            @"Previsto Mensual",
            @"Parte Diario",
            @"Parte Diario (por fecha desde/hasta)",
            nil];
}

-(NSString *)title
{
    return @"Archivo";
}

-(CGSize)size
{
    if (iOSVersion >= 7)
    {
        return CGSizeMake(400.0f, 352.0f);
    } else
        return CGSizeMake(400.0f, 308.0f);
}

-(void)selectedOptionsAtIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
            // Cartera
            [self navigateToDoctors];
            break;
            
        case 4:
            // Previsto Mensual
            [self navigateToPrevistoMensual];
            break;
            
        case 5:
            // Parte Diario
            [self navigateToParteDiario];
            break;
            
        case 2:
            // Grupos
            [self navigateToGrupos];
            break;
            
        case 1:
            // Seleccion de Grupos
            [self navigateToSeleccionarGrupos];
            break;
            
        case 3:
            // Seleccionar Zona
            [self navigateToSeleccionarZona];
            break;
            
        case 6:
            // Parte Diario (por fecha desde y hasta)
            [self navigateToParteDiarioDesdeHasta];
            break;
            
        default:
            [self showNotImplemented];
    }
}

-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index {
    
    switch (index) {
        case 0:
            // Cartera
            return YES;
            break;
            
        case 4:
            // Previsto Mensual
            return YES;
            break;
            
        case 1:
            // Selección de Grupos
            return YES;
            break;
        case 2:
            // Grupos
            return YES;
            break;
        case 3:
            // Seleccionar Zona
            return YES;
            break;
        case 5:
            // Parte Diario
            return YES;
            break;
        case 6:
            // Parte Diario (por fecha desde y hasta)
            return YES;
            break;
        default:
            return NO;
    }
}


#pragma mark -
#pragma Private Members

-(void)navigateToDoctors
{
    CarteraViewController *cvc = [[[CarteraViewController alloc] initWithNibName:@"CarteraViewController" bundle:nil] autorelease];
    
    [self navigateToController:cvc];
}

-(void)navigateToPrevistoMensual
{
    PrevistoMensualViewController *pvc = [[[PrevistoMensualViewController alloc] initWithNibName:@"PrevistoMensualViewController" bundle:nil] autorelease];
    
    [self navigateToController:pvc];
}

-(void)navigateToParteDiario
{
    ParteDiarioViewController *pvc = [[[ParteDiarioViewController alloc] initWithNibName:@"ParteDiarioViewController" bundle:nil] autorelease];
    
    [self navigateToController:pvc];
}

-(void)navigateToParteDiarioDesdeHasta
{
    ParteDiarioViewController *pvc = [[[ParteDiarioViewController alloc] initWithNibName:@"ParteDiarioViewController" bundle:nil] autorelease];
    pvc.viewControllerMode = ParteDiarioDesdeHasta;
    
    [self navigateToController:pvc];
}


-(void)navigateToGrupos
{
    GruposViewController *gvc = [[[GruposViewController alloc] initWithDefaultNib] autorelease];
    
    //[Delegate dismissPopOver:YES];
    //[self.navigationController presentViewController:gvc animated:YES completion:NULL];
    [self showModalController:gvc];
}

-(void)navigateToSeleccionarGrupos
{
    selectGrupoController *gvc = [[[selectGrupoController alloc] initWithDefaultNib] autorelease];
    
    //[Delegate dismissPopOver:YES];
    //[self.navigationController presentViewController:gvc animated:YES completion:NULL];
    gvc.delegate = self.delegate;
    [self showModalController:gvc];
}

- (void)navigateToSeleccionarZona
{
    SeleccionZonaViewController *vc = [[[SeleccionZonaViewController alloc] initWithDefaultNib] autorelease];
    
    vc.delegate = self.delegate;
    
    //[Delegate dismissPopOver:YES];
    //[Delegate.navigationController presentViewController:vc animated:YES completion:NULL];
    [self showModalController:vc];
}

@end
