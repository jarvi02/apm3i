//
//  NSDate+extensions.h
//  APM4
//
//  Created by Juan Pablo Garcia on 08/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (extensions)

-(NSDate *) addDays:(NSUInteger) daysToAdd;
+(NSDate *) dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;
-(BOOL)isWeekEnd;
+(NSDate *)dateWithoutTime;
-(NSDate *)dateAsDateWithoutTime;
-(int)differenceInDaysTo:(NSDate *)toDate;
-(NSString *)formattedDateString;
-(NSString *)formattedStringUsingFormat:(NSString *)dateFormat;

@end
