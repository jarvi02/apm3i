//
//  TrazabilidadViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 21/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "CustomTrazabilidadCell.h"
#import "CustomTrazabilidadHeader.h"
#import "loadingViewController.h"

@protocol TrazabilidadDelegate <NSObject>

-(void)productosModificados:(NSArray *)productos;

@end

@interface TrazabilidadViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate,UISplitViewControllerDelegate>
{
    NSUInteger currentIndex;
    NSUInteger max;
    NSUInteger lastOrder;
}


@property (nonatomic, strong)loadingViewController       *loadingView;


@property(nonatomic, strong) IBOutlet UITableView *tableView;
@property(nonatomic, strong) UINib                              *headerNib;
@property(nonatomic, strong) IBOutlet CustomTrazabilidadHeader  *tmpHeader;
@property(nonatomic, strong) IBOutlet CustomTrazabilidadCell    *tmpCell;
@property (retain, nonatomic) IBOutlet UILabel *lblCantidad;

@property (nonatomic, strong) IBOutlet UIView            *backgroundView;
@property (nonatomic, strong) IBOutlet UIView            *popup;
@property (retain, nonatomic) IBOutlet UILabel           *lblPopDescProducto;
@property (retain, nonatomic) IBOutlet UILabel           *lblPopLote;
@property (retain, nonatomic) IBOutlet UILabel           *lblPopStock;
@property (nonatomic, strong) IBOutlet UITextField       *cantidad;



@property (nonatomic, strong) IBOutlet UITextField       *current;
@property (nonatomic, strong) IBOutlet UILabel           *lblPromocionado;
@property (nonatomic, strong) IBOutlet UISwitch          *promocionado;
@property (nonatomic, strong) IBOutlet UISwitch          *obsequio;
@property (nonatomic, strong) IBOutlet UISwitch          *literatura;
@property (retain, nonatomic) IBOutlet UILabel           *lblEvaluacion;
@property (retain, nonatomic) IBOutlet UITextField       *txtEvaluacion;
@property (retain, nonatomic) IBOutlet UILabel           *lblPlan;
@property (retain, nonatomic) IBOutlet UITextField       *txtPlan;
@property(nonatomic, assign) id<TrazabilidadDelegate>   delegate;
@property(nonatomic,retain) UISplitViewController* parentSplitVC;
@property (retain, nonatomic) IBOutlet UIView *emptyView;



-(IBAction)aceptar:(id)sender;
-(IBAction)cancelar:(id)sender;

// Popup Actions
-(IBAction)aceptarPopUp:(id)sender;
-(IBAction)cancelarPopUp:(id)sender;
-(void)showProductsForFamily:(int)family;
- (IBAction)swSinMuestraChanged:(UISwitch *)sender;
@property (retain, nonatomic) IBOutlet UILabel *orden;



@end
