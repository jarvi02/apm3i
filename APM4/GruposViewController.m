//
//  GruposViewController.m
//  APM4
//
//  Created by Laura Busnahe on 6/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "GruposViewController.h"
#import "MTGrupos.h"

#import "GlobalDefines.h"
#import "NSStringExtended.h"

@interface GruposViewController ()

@end

@implementation GruposViewController
{
    MTGrupos *selectedGroup;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.data     = nil;
        self.oldData  = nil;
        selectedGroup = nil;
    }
    return self;
}

- (id)initWithDefaultNib
{
    self = [self initWithNibName:@"GruposViewController" bundle:nil];
    self.modalPresentationStyle = UIModalPresentationFormSheet;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.data    = [NSMutableArray arrayWithArray:[MTGrupos getAll]];
    self.oldData = [self.data copy];
    
    
    //self.btnEditar.tintColor = [UIColor grayColor];
    self.btnAdd.titleLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark Sort methods

- (void)sortByProperty:(NSString*)key Ascending:(BOOL)ascending
{
    NSSortDescriptor *sortDescriptor;
    NSSortDescriptor *sortDescriptor2;
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key
                                                 ascending:ascending];
    
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"descripcion"
                                                  ascending:YES];
    
    NSArray *sortDescriptors;
    NSArray *sortedArray;
    
    
    // Chequeo si se ordena por
    if ([key compare:@"descripcion"] != NSOrderedSame)
    {
        sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, sortDescriptor2, nil];
    } else
    {
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    }
    
    sortedArray = [self.data sortedArrayUsingDescriptors:sortDescriptors];
    self.data = [NSMutableArray arrayWithArray:sortedArray];
    
}


#pragma mark -
#pragma mark Table Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of time zone names.
    
	return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MyIdentifier = @"CustomDoctorCellIdentifier";
	
    // Obtain the cell object.
	UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        // Use the default cell style.
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    
    NSString *cellText = @"";
    MTGrupos *_grp = nil;
    _grp = [self.data objectAtIndex:indexPath.row];
    cellText = [_grp getDescription];
    
    // Si el grupo está bloqueado, lo muestro en gris (si no puede modificarse).
    if (_grp.blocked)
        cell.textLabel.textColor = [UIColor grayColor];
    else
    {
        // Si está em modo de edición, cambio el color del texto al de un item interactivo.
        if (self.tblGrupos.editing == NO)
        {
            cell.textLabel.textColor = UIColorFromRGB(defaultCellFontColor);
        } else
        {
            cell.textLabel.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
        }
    }
    
	cell.textLabel.text = [NSString stringWithFormat:@"%@", cellText];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.tblGrupos.editing == YES)
    {
        MTGrupos *_grp = [self.data objectAtIndex:indexPath.row];
        // Chequeo si el grupo está bloqueado.
        if (_grp.blocked)
        {
            // Deselecciono el grupo y borro el contentido del textField con el nombre del grupo.
            selectedGroup = nil;
            self.txtNombreGrupo.text = @"";
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            
            // Muestro un mensaje de advertencia donde dice que el grupo no puede ser modificado.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                            message:@"Este grupo no puede ser modificado ni elimiando"
                                                           delegate:self
                                                  cancelButtonTitle:@"Aceptar"
                                                  otherButtonTitles:nil];
            [alert show];
        } else
        {
            // Selecciono el grupo y completo el textField con el nombre del mismo.
            selectedGroup = _grp;
            self.txtNombreGrupo.text = [NSString string:selectedGroup.descripcion ifNull:@""];
        }
    } else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{    
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Delete grupo
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        MTGrupos *o = [self.data objectAtIndex:indexPath.row];
        if (o.blocked)
        {
            // Muestro un mensaje de advertencia donde dice que el grupo no puede ser eliminado.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                            message:@"Este grupo no puede ser modificado ni elimiando"
                                                           delegate:self
                                                  cancelButtonTitle:@"Aceptar"
                                                  otherButtonTitles:nil];
            [alert show];
        } else
        {
            [o deleteAllData];
            [self.data removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
//        [self refreshTable];
    }
    
    // Edit Grupo
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        
    }
    
    
}
    
- (void)refreshTable
{
    [self sortByProperty:@"descripcion" Ascending:YES];
    [self.tblGrupos reloadData];
}


#pragma mark -
#pragma mark TextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}

// En vistas que se presentan de forma modal es necesario agregar este override para que oculte el teclado
// con resignFirstResponder
- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(BOOL) textFieldShouldBeginEditing:(UITextField*)textField
{
    return YES;
}

- (BOOL)textFieldOLD:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    
    
    BOOL _isAllowed = YES;
    
    NSString *oldString = [NSString string:textField.text ifNull:@""];
    
//    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
//                            stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];

    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
                            stringByTrimmingCharactersInSet:notAllowedSet];

    
    if ([oldString isEqualToString:tempString])
    {
        _isAllowed =  NO;
    }
    
    return   _isAllowed;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    NSString *oldString = [NSString string:textField.text ifNull:@""];
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
                            stringByTrimmingCharactersInSet:notAllowedSet];
    
    if ([oldString isEqualToString:tempString])
        return NO;
    
    return   YES;
}


#pragma mark -
#pragma mark Tap methods

- (IBAction)tapBtnAdd:(id)sender
{
    // Editar grupo
    if (self.tblGrupos.editing == YES)
    {
        NSString *oldDescription = selectedGroup.descripcion;
        
        if ([selectedGroup.descripcion isEqualToString:self.txtNombreGrupo.text])
            return;
            
        selectedGroup.descripcion = [NSString string:self.txtNombreGrupo.text ifNull:@""];
        
        NSString *_error = [selectedGroup isOkToSave];
        
        // Chequeo si existe algún error en el nombre del grupo ingresado.
        if ([_error length] > 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                            message:_error
                                                           delegate:self
                                                  cancelButtonTitle:@"Aceptar"
                                                  otherButtonTitles:nil];
            [alert show];
            selectedGroup.descripcion = oldDescription;
            return;
        }
        
        [selectedGroup updateAllData];
    } else
        
    // Agregar nuevo grupo
    {
        MTGrupos *_grp = [[MTGrupos alloc] init];
        _grp.descripcion = [NSString string:self.txtNombreGrupo.text ifNull:@""];
        //_grp.action      = kMTEAAdd;
        
        NSString *_error = [_grp isOkToSave];
        
        // Chequeo si existe algún error en el nombre del grupo ingresado.
        if ([_error length] > 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                            message:_error
                                                           delegate:self
                                                  cancelButtonTitle:@"Aceptar"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        [self.data addObject:_grp];
        [_grp saveAllData];
    }
    
    // Si no hay errores, agrego el nuevo grupo a la lista y la reordeno.
    self.txtNombreGrupo.text = @"";
    [self sortByProperty:@"descripcion" Ascending:YES];
    [self refreshTable];
}

- (IBAction)tapBtnEditar:(id)sender
{
    if ([self.data count] > 0)
    {
        if (self.tblGrupos.editing == NO)
        {
            self.txtNombreGrupo.text = @"";
            [self.tblGrupos setEditing:YES animated:YES];
            
            [self.btnAdd setHidden:YES];
            [self.btnOk setHidden:NO];
            
            [self.btnAceptar setEnabled:NO];
            self.btnEditar.title = @"Finalizar";
            //self.btnEditar.tintColor = [UIColor blueColor];
            
            selectedGroup = nil;
            [self.tblGrupos reloadData];
            
        } else
        {
            self.txtNombreGrupo.text = @"";
            [self.tblGrupos setEditing:NO animated:YES];
            
            [self.btnOk setHidden:YES];
            [self.btnAdd setHidden:NO];
            
            [self.btnAceptar setEnabled:YES];
            self.btnEditar.title = @"Editar";
            //self.btnEditar.tintColor = [UIColor grayColor];
            
            selectedGroup = nil;
            [self.tblGrupos reloadData];
        }
    }
}

- (IBAction)tapBtnAceptar:(id)sender
{
    // Chequeo si hay grupos y de ser así, si hay alguno nuevo.
//    if ([self.data count] > 0)
//    {
//        for (MTGrupos *_grp in self.data)
//        {
//            if (_grp.action == kMTEAAdd)
//               [_grp saveAllData];
//        }
//    }
    
    [self dismiss];
}

@end
