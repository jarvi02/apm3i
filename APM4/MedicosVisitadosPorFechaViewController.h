//
//  MedicosVisitadosPorFechaViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "CusromMedicosVisitadosPorFechaCell.h"
#import "TableForComboPopoverViewControllerDelegate.h"

@interface MedicosVisitadosPorFechaViewController : CustomViewController<UITabBarControllerDelegate, UITableViewDataSource,TableForComboPopoverViewControllerDelegate>


@property (nonatomic, retain) NSArray                   *dataCombo;
@property (nonatomic, retain) NSArray                   *data;
@property (retain, nonatomic) IBOutlet UITableView      *table;
@property (retain, nonatomic) IBOutlet CusromMedicosVisitadosPorFechaCell *tmpCell;
@property (nonatomic, retain) UINib                     *cellNib;

@property (retain, nonatomic) IBOutlet UITextField *txtFiltro;

- (IBAction)showComboDate:(id)sender;
@end
