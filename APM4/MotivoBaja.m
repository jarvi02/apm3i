//
//  MotivoBaja.m
//  APM4
//
//  Created by Juan Pablo Garcia on 27/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MotivoBaja.h"
#import "DB.h"

@implementation MotivoBaja

+(NSArray*) GetAll
{
    NSString *sql = @"select id, descripcion from mtc_motivosbaja ORDER BY descripcion";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    NSMutableArray *motivos = [NSMutableArray array];
    
	if ( statement )
    {
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            
            MotivoBaja *motivo = [[MotivoBaja alloc] init];
            
            // id
            motivo.motivoId = sqlite3_column_int(statement, 0);
            
            // motivo
            char *_descripcion = (char *)sqlite3_column_text(statement, 1);
            if((_descripcion != nil) && (strlen(_descripcion) > 0))
            {
                motivo.motivo = [NSString stringWithUTF8String:_descripcion];
			}else
            {
				motivo.motivo = @"";
			}
            
            [motivos addObject:motivo];
        }
        
	} else
    {
        NSAssert(0, @"No se pudo ejecutar: %@", sql);
	}
	sqlite3_finalize(statement);
    
    return motivos;
}

- (NSString*)getDescription
{
    return [NSString string:self.motivo];
}

+ (MotivoBaja*)getFirst
{
    MotivoBaja *result = nil;
    
    NSString *sql = @"select id, descripcion from mtc_motivosbaja ORDER BY descripcion LIMIT 1";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement )
    {
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            result = [[MotivoBaja alloc] init];
            
            // id
            result.motivoId = sqlite3_column_int(statement, 0);
            
            // motivo
            char *_descripcion = (char *)sqlite3_column_text(statement, 1);
            result.motivo = [NSString pCharToString:_descripcion];
        }
        
	} else
    {
        NSAssert(0, @"No se pudo ejecutar: %@", sql);
	}
	sqlite3_finalize(statement);
    
    return result;
}

@end
