//
//  Novedad.m
//  APM4
//
//  Created by Juan Pablo Garcia on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Novedad.h"
#import "DB.h"

@interface Novedad(Private)

+(NSString *)tipoStr:(NovedadTipo)tipo;

@end

@implementation Novedad

+(NSUInteger)lastId
{
    NSUInteger lastId = 0;
    NSString *sql = @"select max(secuencia) from log_novedades";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
        
            lastId = sqlite3_column_int(statement, 0);
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla log_novedades");
	}
	sqlite3_finalize(statement);
    
    return lastId;
}

+(NSString *)tipoStr:(NovedadTipo)tipo
{
    switch (tipo) {
        case kInsert:
            return @"I";
            break;
            
        case kUpdate:
            return @"U";
            break;
            
        case kDelete:
            return @"D";
            break;
            
        default:
            return @"";
            break;
    }
}


+(void)insert:(NSString *)tabla novedad:(NSString *)novedad tipo:(NovedadTipo)tipo
{
    NSString *sql = [NSString stringWithFormat:@"insert into log_novedades (secuencia, tabla, tipo, registro, tranid, transmitido) "
                                                "values (%d, %@, %@, %@, 0, 0);",
                     [self lastId]+1,
                     [NSString stringQuoted:[tabla lowercaseString] ifNull:@"''"],
                     [NSString stringQuoted:[Novedad tipoStr:tipo] ifNull:@"''"],
                     [NSString stringQuoted:novedad ifNull:@"''"]];
    
    [[DB getInstance] excecuteSQL:sql];
}
                     
                     

@end
