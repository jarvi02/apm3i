//
//  Config.h
//  RnR
//
//  Created by Fabian E. Pezet Vila on 10/12/12.
//  Copyright (c) 2012 Fabian E. Pezet Vila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTNodo.h"

#define SettingForKey(key, stringdefault) ([[Config shareInstance] isValidKey:key] ? [[Config shareInstance] find:key] : stringdefault)
//#define SettingSetValueForKey(key, value) if ([[Config shareInstance] isValidKey:key]) [[Config shareInstance] update:key value:value]
#define iOSVersion [[Config shareInstance] getiOSVersion]


#define CICLO               @"ciclo"
#define ENTIDAD             @"entidad"
#define REMOTE_PATH         @"remote_path"
#define REMOTE_HOST         @"remote_host"
#define REMOTE_PORT         @"remote_port"
#define INICIALIZAR         @"inicializar"
#define CERRAR_CICLO        @"cerrar_ciclo"
#define TRANID              @"tranid"
#define TRANID_OK           @"tranid_ok"
#define REINTENTOS          @"reintentos"
#define PAIS                @"pais"
#define VISITAS_PERMITIDAS  @"visitaspermitidas"
#define APP_ACTIVADA        @"app_activada"
#define DESCRIPCION_NODO    @"descripcion_nodo"
#define NODO                @"nodo"
#define REMOTE_HOST_2       @"remote_host_2"
#define REMOTE_PORT_2       @"remote_port_2"
#define USER                @"usr"
#define PASSWORD            @"pwd"

@interface Config : NSObject{
    
@private
    NSMutableDictionary *dictionary;
}

+ (Config*)shareInstance;

- (void) reloadData;
- (BOOL) isValidKey:(NSString*)key;
- (NSString*) find:(NSString*)aKey;
- (void) update:(NSString*)aKey value:(NSString*)aValue;
- (void) updateNodo:(MTNodo*)nodo;
- (NSUInteger)getiOSVersion;


@end
