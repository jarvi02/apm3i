//
//  TableForComboPopoverViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableForComboPopoverViewControllerDelegate.h"

@interface TableForComboPopoverViewController : UITableViewController


@property(nonatomic, retain) NSArray *data;
@property(nonatomic, retain) NSString *titulo;
@property(nonatomic, retain) NSString *caller;
@property(nonatomic, assign) id<TableForComboPopoverViewControllerDelegate> delegate;
@property(nonatomic, assign) NSTextAlignment aligment;

-(id)initWithData:(NSArray*) aData titulo:(NSString*) aTitulo size:(CGSize)aSize delegate:(id<TableForComboPopoverViewControllerDelegate>) aDelegate andCaller:(NSString*)caller;

@end
