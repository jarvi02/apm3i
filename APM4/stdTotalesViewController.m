//
//  stdTotalesViewController.m
//  APM4
//
//  Created by Laura Busnahe on 7/16/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "stdTotalesViewController.h"
#import "MTstdTotales.h"

@interface stdTotalesViewController ()

@property (nonatomic, strong)MTstdTotales   *valoresEstadistica;

@end

@implementation stdTotalesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Totales";
    }
    return self;
}

- (id)initViewDefaultNib
{
    self = [self initWithNibName:@"stdTotalesViewController" bundle:nil];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
    self.valoresEstadistica = [MTstdTotales getTotales];
    [self completarEstadisticas];
    
    [self.loadingView hide];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark CustomViewController methods

-(NSString *)getTitleForHeader{
    return @"Totales";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"cabecera_principal_32"];
}


#pragma mark -
#pragma mark CustomViewController methods

- (void)completarEstadisticas
{
    //[self.valoresEstadistica updateValues];
    
    // Períodos trabajados
    self.edtPeriodoM.text     = [NSString intToStr:self.valoresEstadistica.periodosTrabajados.maniana];
    self.edtPeriodoT.text     = [NSString intToStr:self.valoresEstadistica.periodosTrabajados.tarde];
    self.edtPeriodoTotal.text = [NSString intToStr:self.valoresEstadistica.periodosTrabajados.total];
    
    // Fijos
    self.txtFijosProgM.text     = [NSString intToStr:self.valoresEstadistica.fijos.programados.maniana];
    self.txtFijosProgT.text     = [NSString intToStr:self.valoresEstadistica.fijos.programados.tarde];
    self.txtFijosProgTotal.text = [NSString intToStr:self.valoresEstadistica.fijos.programados.total];
    
    self.txtFijosVisitM.text     = [NSString intToStr:self.valoresEstadistica.fijos.vistos.maniana];
    self.txtFijosVisitT.text     = [NSString intToStr:self.valoresEstadistica.fijos.vistos.tarde];
    self.txtFijosVisitTotal.text = [NSString intToStr:self.valoresEstadistica.fijos.vistos.total];
    
    self.txtFijosPorcM.text     = [NSString floatToStr:self.valoresEstadistica.fijos.porcenataje.maniana];
    self.txtFijosPorcT.text     = [NSString floatToStr:self.valoresEstadistica.fijos.porcenataje.tarde];
    self.txtFijosPorcTotal.text = [NSString floatToStr:self.valoresEstadistica.fijos.porcenataje.total];
    
    self.txtFijosMedM.text     = [NSString floatToStr:self.valoresEstadistica.fijos.media.maniana];
    self.txtFijosMedT.text     = [NSString floatToStr:self.valoresEstadistica.fijos.media.tarde];
    self.txtFijosMedTotal.text = [NSString floatToStr:self.valoresEstadistica.fijos.media.total];
    
    
    // Revisitas
    self.txtRevisProgM.text     = [NSString intToStr:self.valoresEstadistica.revisitas.programados.maniana];
    self.txtRevisProgT.text     = [NSString intToStr:self.valoresEstadistica.revisitas.programados.tarde];
    self.txtRevisProgTotal.text = [NSString intToStr:self.valoresEstadistica.revisitas.programados.total];
    
    self.txtRevisVisitM.text     = [NSString intToStr:self.valoresEstadistica.revisitas.vistos.maniana];
    self.txtRevisVisitT.text     = [NSString intToStr:self.valoresEstadistica.revisitas.vistos.tarde];
    self.txtRevisVisitTotal.text = [NSString intToStr:self.valoresEstadistica.revisitas.vistos.total];
    
    self.txtRevisPorcM.text     = [NSString floatToStr:self.valoresEstadistica.revisitas.porcenataje.maniana];
    self.txtRevisPorcT.text     = [NSString floatToStr:self.valoresEstadistica.revisitas.porcenataje.tarde];
    self.txtRevisPorcTotal.text = [NSString floatToStr:self.valoresEstadistica.revisitas.porcenataje.total];
    
    self.txtRevisMedM.text     = [NSString floatToStr:self.valoresEstadistica.revisitas.media.maniana];
    self.txtRevisMedT.text     = [NSString floatToStr:self.valoresEstadistica.revisitas.media.tarde];
    self.txtRevisMedTotal.text = [NSString floatToStr:self.valoresEstadistica.revisitas.media.total];
    
    // Fluctuantes
    self.txtFlucVisitM.text     = [NSString intToStr:self.valoresEstadistica.fluctuantes.visitados.maniana];
    self.txtFlucVisitT.text     = [NSString intToStr:self.valoresEstadistica.fluctuantes.visitados.tarde];
    self.txtFlucVisitTotal.text = [NSString intToStr:self.valoresEstadistica.fluctuantes.visitados.total];
    
    self.txtFlucRevisM.text     = [NSString intToStr:self.valoresEstadistica.fluctuantes.revisitas.maniana];
    self.txtFlucRevisT.text     = [NSString intToStr:self.valoresEstadistica.fluctuantes.revisitas.tarde];
    self.txtFlucRevisTotal.text = [NSString intToStr:self.valoresEstadistica.fluctuantes.revisitas.total];
    
    // Total
    self.txtTotalVisitM.text     = [NSString intToStr:self.valoresEstadistica.total.visitados.maniana];
    self.txtTotalVisitT.text     = [NSString intToStr:self.valoresEstadistica.total.visitados.tarde];
    self.txtTotalVisitTotal.text = [NSString intToStr:self.valoresEstadistica.total.visitados.total];
    
    self.txtTotalMedM.text     = [NSString floatToStr:self.valoresEstadistica.total.media.maniana];
    self.txtTotalMedT.text     = [NSString floatToStr:self.valoresEstadistica.total.media.tarde];
    self.txtTotalMedTotal.text = [NSString floatToStr:self.valoresEstadistica.total.media.total];
}

- (IBAction)edtPeriodoChanged:(id)sender
{
//    sTotalesValores s;
//    
//    s.maniana = [self.edtPeriodoM.text integerValue];
//    s.tarde  = [self.edtPeriodoT.text integerValue];
//    s.total  = [self.edtPeriodoTotal.text integerValue];
//    
//    [self.valoresEstadistica updateValues];
//    [self completarEstadisticas];
}

#pragma mark -
#pragma mark TextField methods

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField          // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
{
 
    sTotalesValores s;
    
    s.maniana = [self.edtPeriodoM.text integerValue];
    s.tarde  = [self.edtPeriodoT.text integerValue];
    s.total  = [self.edtPeriodoTotal.text integerValue];
    
    self.valoresEstadistica.periodosTrabajados = s;
    
    [self.valoresEstadistica updateValues];
    [self completarEstadisticas];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    [textField resignFirstResponder];
}

- (BOOL)textFieldOLD:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string   // return NO to not change text
{
    if ([string length] == 0 && range.length > 0)
    {
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return NO;
    }
    
    if ((textField == self.edtPeriodoM) ||
        (textField == self.edtPeriodoT) ||
        (textField == self.edtPeriodoTotal))
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        if ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0) return YES;
        
        return NO;
    } else
        return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    NSString *oldString = [NSString string:textField.text ifNull:@""];
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    if ((textField == self.edtPeriodoM) ||
        (textField == self.edtPeriodoT) ||
        (textField == self.edtPeriodoTotal))
    {
        notAllowedSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    }
    
    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
                            stringByTrimmingCharactersInSet:notAllowedSet];
    
    if ([oldString isEqualToString:tempString])
        return NO;
    
    return   YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}
@end
