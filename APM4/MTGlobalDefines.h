//
//  MTGlobalDefines.h
//  APM4
//
//  Created by Laura Busnahe on 4/3/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark -
#pragma mark Ajuste de Muestas Médicas 

#define INCLUDE_AJUSTEMM                    // Habilita la opción de Ajuste MM en el menú.
//#define DEBUG_AJUSTEMM      1               // Debug para módulo de Ajuste MM.


#pragma mark -
#pragma mark Prescripciones

#define INCLUDE_PRESCRICIONESTABLE          // Muestra el contenido de la tabla de prescripciones.
#define INCLUDE_PRESCRICIONES               // Habilita la opción Prescripciones en el menú.
//#define DEBUG_PRESCRIPCIONES    1           // Debug para módulo Prescripciones.


#pragma mark -
#pragma mark Previsto Mensual

//#define DEBUG_PREVISTOM    1                // Debug para módulo Previsto Mensual.


#pragma mark -

/*
@interface MTGlobalDefines : NSObject

@end
*/