//
//  EstadoVisita.h
//  APM4
//
//  Created by Juan Pablo Garcia on 11/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EstadoVisita : NSObject

@property(nonatomic, retain) NSString *descripcion;
@property(nonatomic) NSUInteger idEstado;

+(EstadoVisita *)getByID:(NSUInteger)idEstado;

@end
