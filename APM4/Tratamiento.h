//
//  Tratamiento.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tratamiento : NSObject

@property(nonatomic, assign) NSInteger  recID;
@property(nonatomic, retain) NSString   *descripcion;

+(NSArray*) GetAll;

@end
