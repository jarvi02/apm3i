//
//  coberturaCell.h
//  APM4
//
//  Created by Laura Busnahe on 7/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface coberturaCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblDescripcion;
@property (retain, nonatomic) IBOutlet UILabel *lblProgramados;
@property (retain, nonatomic) IBOutlet UILabel *lblVistos;
@property (retain, nonatomic) IBOutlet UILabel *lblPorcentaje;
@property (retain, nonatomic) IBOutlet UILabel *lblRevisitas;
@property (retain, nonatomic) IBOutlet UILabel *lblFuctuantes;
@property (retain, nonatomic) IBOutlet UILabel *lblTotal;

@end
