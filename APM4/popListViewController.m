//
//  popListViewController.m
//  APM4
//
//  Created by Laura Busnahe on 2/21/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "popListViewController.h"
#import "popListView.h"

@interface popListViewController ()

@end

@implementation popListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.withSearch = FALSE;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //self.searchBar.text = @"";
    
}

- (void)viewWillAppear:(BOOL)animated;
{
    [super viewWillAppear:animated];
    self.searchBar.text = @"";
    
    [self useSearch:self.withSearch];
    
    if (self.delegate)
        [self.delegate viewWillAppear];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (self.delegate)
        [self.delegate viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)dealloc {
//    [_TableView release];
//    [_searchBar release];
//    [super dealloc];
//}


- (void) useSearch:(BOOL) search
{
   self.withSearch = search;
   if (search)
   {
       // CON BUSQUEDA: muestro la barra de búsqueda y muevo la tabla hacia abajo.
       [self.TableView setFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
       [self.searchBar setHidden:FALSE];
       
       [self.TableView reloadData];
       
   } else
   {
       // SIN BUSQUEDA: oculto la barra de búsqueda, vacio el campo de busqueda y expando la tabla.
       [self.searchBar setHidden:TRUE];
       self.searchBar.text = @"";
       [self.TableView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
       
       [self.TableView reloadData];
   }
}

// En vistas que se presentan de forma modal es necesario agregar este override para que oculte el teclado
// con resignFirstResponder
- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar endEditing:YES];
    [searchBar resignFirstResponder];
}

@end
