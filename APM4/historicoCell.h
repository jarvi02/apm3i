//
//  hitoricoCell.h
//  APM4
//
//  Created by Laura Busnahe on 7/12/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface historicoCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *txtOrden01;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden02;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden03;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden04;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden05;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden06;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden07;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden08;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden09;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden10;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden11;
@property (retain, nonatomic) IBOutlet UILabel *txtOrden12;

@property (retain, nonatomic) IBOutlet UILabel *txtProducto01;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto02;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto03;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto04;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto05;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto06;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto07;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto08;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto09;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto10;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto11;
@property (retain, nonatomic) IBOutlet UILabel *txtProducto12;

@end
