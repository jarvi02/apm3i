//
//  StatisticsMenuViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "StatisticsMenuViewController.h"
#import "MedicosVisitadosPorFechaViewController.h"
#import "MuestrasEntregadasViewController.h"
#import "StockMMViewController.h"
#import "stdTotalesViewController.h"
#import "CoberturaViewController.h"
#import "DetalleParteViewController.h"
#import "TotalesDiariosViewController.h"
#import "DistribucionCantidadViewController.h"
#import "VisitasPorDiasViewController.h"
#import "NoVisitaPorMotivoViewController.h"
#import "Config.h"

@interface StatisticsMenuViewController ()
-(void)navigateToMedicosVisitadosFecha;
-(void)navigateToMuestrasEntregadas;
-(void)navigateToStockMM;
@end

@implementation StatisticsMenuViewController

-(NSArray *)options
{
    return [NSArray arrayWithObjects:
            @"Médicos visitados por Fecha",
            @"No visitas por motivos",
            @"Visitas por día",
            @"Totales",
            @"Detalle de parte diario",
            @"Cobertura por potencial",
            @"Cobertura por especialidad",
            @"Cobertura por lugar de visita",
            @"Totales de informe diario",
            @"Distribución de médicos por cantidad de visitas",
            @"Muestras entregadas",
            @"Stock MM",
            nil];
    
//    return [NSArray arrayWithObjects:
//            @"Médicos visitados por Fecha",
//            @"No visitas por motivos",
//            @"Visitas por día",
//            @"Totales",
//            @"Detalle de parte diario",
//            @"Cobertura por potencial",
//            @"Cobertura por especialidad",
//            @"Cobertura por lugar de visita",
//            @"Totales de informe diario",
//            @"Distribución de médicos por cantidad de visitas",
//            @"Muestras entregadas",
//            @"Stock MM",
//            nil];
}

-(NSString *)title
{
    return @"Estadísticas";
}



-(void)selectedOptionsAtIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
            // Medicos Visitados por Fecha
            [self navigateToMedicosVisitadosFecha];
            break;

        case 1:
            // No Visitas por Motivos
            [self navigateToNoVisitaPorMotivo];
            break;
            
        case 2:
            // Visitas por Dia
            [self navigateToVisitasPorDias];
            break;
            
        case 3:
            // Totales
            [self navigateToStdTotales];
            break;

            
        case 4:
            // Detalle de parte diario
            [self navigateToStdDetalleParte];
            break;
            
        case 5:
            // Cobertura por potencial
            [self navigateToCoberturaPotencial];
            break;
            
        case 6:
            // Cobertura por especialidad
            [self navigateToCoberturaEspecialidades];
            break;
            
        case 7:
            // Cobertura por lugar de visita
            [self navigateToCoberturaLugarVisita];
            break;
            
        case 8:
            // Totales de informe diario
            [self navigateToTotalesDiarios];
            break;

        case 9:
            // Distribución de médicos por cantidad de visitas
            [self navigateToDistribucionCantidad];
            break;
            
        case 10:
            // Muestras entregadas
            [self navigateToMuestrasEntregadas];
            break;
            
        case 11:
            // Stock MM
            [self navigateToStockMM];
            break;
            
        default:
            [self showNotImplemented];
    }

}

//-(void)selectedOptionsAtIndex:(NSUInteger)index
//{
//    switch (index) {
//        case 0:
//            // Medicos Visitados por Fecha
//            [self navigateToMedicosVisitadosFecha];
//            break;
//            
//        case 1:
//            // No Visitas por Motivos
//            
//        case 2:
//            // Visitas por Dia
//            
//        case 3:
//            // Totales
//            [self navigateToStdTotales];
//            break;
//            
//        case 4:
//            // Detalle de parte diario
//            
//        case 5:
//            // Cobertura por potencial
//            
//        case 6:
//            // Cobertura por especialidad
//            
//        case 7:
//            // Cobertura por lugar de visita
//            
//        case 8:
//            // Totales de informe diario
//            
//        case 9:
//            // Distribución de médicos por cantidad de visitas
//
//        case 10:
//            // Muestras entregadas
//            [self navigateToMuestrasEntregadas];
//            break;
//        case 11:
//            // Stock MM
//            [self navigateToStockMM];
//            break;
//            
//        default:
//            [self showNotImplemented];
//    }
//    
//}

-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index {
    
    switch (index) {
        case 0:
            // Medicos Visitados por Fecha
            return YES;
            break;
        case 1:
            // No Visitas por Motivos
            return YES;
            break;
        case 2:
            // Visitas por Dia
            return YES;
            break;
        case 3:
            // Totales
            return YES;
            break;
        case 4:
            // Detalle de parte diario
            return YES;
            break;
        case 5:
            // Cobertura por potencial
            return YES;
            break;
        case 6:
            // Cobertura por especialidad
            return YES;
            break;
        case 7:
            // Cobertura por lugar de visita
            return YES;
            break;
        case 8:
            // Totales de informe diario
            return YES;
            break;
        case 9:
            // Distribución de médicos por cantidad de visitas
            return YES;
            break;
        case 10:
            // Muestras entregadas
            return YES;
            break;
        case 11:
            // Stock MM
            return YES;
            break;
        default:
            return NO;
    }

}

//-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index {
//    
//    switch (index) {
//        case 0:
//            // Medicos Visitados por Fecha
//            return YES;
//            break;
//        case 1:
//            // No Visitas por Motivos
//            return NO;
//            break;
//        case 2:
//            // Visitas por Dia
//            return NO;
//            break;
//        case 3:
//            // Totales
//            return YES;
//            break;
//        case 4:
//            // Detalle de parte diario
//            return NO;
//            break;
//        case 5:
//            // Cobertura por potencial
//            return NO;
//            break;
//        case 6:
//            // Cobertura por especialidad
//            return NO;
//            break;
//        case 7:
//            // Cobertura por lugar de visita
//            return NO;
//            break;
//        case 8:
//            // Totales de informe diario
//            return NO;
//            break;
//        case 9:
//            // Distribución de médicos por cantidad de visitas
//            return NO;
//            break;
//        case 10:
//            // Muestras entregadas
//            return YES;
//            break;
//        case 11:
//            // Stock MM
//            return YES;
//            break;
//        default:
//            return NO;
//    }
//    
//}

-(void)navigateToMedicosVisitadosFecha
{
    MedicosVisitadosPorFechaViewController *o = [[[MedicosVisitadosPorFechaViewController alloc] initWithNibName:@"MedicosVisitadosPorFechaViewController" bundle:nil] autorelease];
    [self navigateToController:o];
}

-(void)navigateToTotalesDiarios
{
    TotalesDiariosViewController *o = [[[TotalesDiariosViewController alloc] initViewDefaultNib] autorelease];
    [self navigateToController:o];
}

-(void)navigateToCoberturaEspecialidades
{
    CoberturaViewController *o = [[[CoberturaViewController alloc] initViewDefaultNibFor:_kCoberturaEspecialidad] autorelease];
    o.type = _kCoberturaEspecialidad;
    [self navigateToController:o];
}

-(void)navigateToStdDetalleParte
{
    DetalleParteViewController *o = [[[DetalleParteViewController alloc] initViewDefaultNib] autorelease];
    [self navigateToController:o];
}

-(void)navigateToStdTotales
{
    stdTotalesViewController *o = [[[stdTotalesViewController alloc] initViewDefaultNib] autorelease];
    [self navigateToController:o];
}

-(void)navigateToCoberturaPotencial
{
    CoberturaViewController *o = [[[CoberturaViewController alloc] initViewDefaultNibFor:_kCoberturaPotencial] autorelease];
    o.type = _kCoberturaPotencial;
    [self navigateToController:o];
}

-(void)navigateToCoberturaLugarVisita
{
    CoberturaViewController *o = [[[CoberturaViewController alloc] initViewDefaultNibFor:_kCoberturaLugarVisita] autorelease];
    o.type = _kCoberturaLugarVisita;
    [self navigateToController:o];
}

-(void)navigateToMuestrasEntregadas {
    
    MuestrasEntregadasViewController *o = [[[MuestrasEntregadasViewController alloc] initWithNibName:@"MuestrasEntregadasViewController" bundle:nil] autorelease];
    [self navigateToController:o];
}

-(void)navigateToStockMM
{
    StockMMViewController *o = [[[StockMMViewController alloc] initWithNibName:@"StockMMViewController" bundle:nil] autorelease];
    [self navigateToController:o];
}

-(void)navigateToDistribucionCantidad
{
    DistribucionCantidadViewController *o = [[DistribucionCantidadViewController alloc] initViewDefaultNib];
    [self navigateToController:o];
}

-(void)navigateToVisitasPorDias
{
    VisitasPorDiasViewController *o = [[VisitasPorDiasViewController alloc] initViewDefaultNib];
    [self navigateToController:o];
}

-(void)navigateToNoVisitaPorMotivo
{
    NoVisitaPorMotivoViewController *o = [[NoVisitaPorMotivoViewController alloc] initViewDefaultNib];
    [self navigateToController:o];
}

@end

