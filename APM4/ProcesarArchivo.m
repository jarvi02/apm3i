//
//  ProcesarArchivo.m
//  borrar
//
//  Created by Fabian E. Pezet Vila on 24/03/13.
//  Copyright (c) 2013 Fabian E. Pezet Vila. All rights reserved.
//

#import "ProcesarArchivo.h"
#import "DDFileReader.h"
#import "DBExtended.h"
#import "NSString+Validaciones.h"

@interface ProcesarArchivo()
-(void) insert:(NSString*) registro onTable:(NSString*) tabla;
-(NSString*)convert:(NSString*)str;
-(void) status:(NSString*) mensaje;

@end

@implementation ProcesarArchivo
{
    NSMutableArray *rowList;
    
    NSMutableArray *columnsList;
    NSString       *columnsString;
    
    NSUInteger fileTotalLines;
}


-(id)init
{
    self = [super init];
    
    if (self)
    {
        rowList = [[NSMutableArray alloc] init];
        columnsList = [[NSMutableArray alloc] init];
    }
    
    return self;
}

-(BOOL) start:(NSString*)filePath
{
    
    /*
     
     El proceso se peude resumir en 3 pasos
     1 - busco inicio bloque <DATASET>
     2 - Borro Tabla
     3 - Inserto registros nuevos
     */
    
    NSLog(@" - Procesado de archivo iniciado");
    
    DDFileReader * reader = [[DDFileReader alloc] initWithFilePath:filePath];
    NSString * line = nil;
    
    NSUInteger lineNumber = 0;
    
    NSString *nombreTabla = nil;
    NSString *count = nil;
    NSInteger cnn = 0;
    
    BOOL nextNombreTabla = NO;
    BOOL nextDataSet = YES;
    BOOL nextBlockSize = NO;
    BOOL nextCount = NO;
    BOOL nextData = NO;
    
    UIProgressView* tempView = self.progressView;
    
    NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
    
//    NSError *error = nil;
//    NSString *xmlFileString = [NSString stringWithContentsOfFile:reader.filePath
//                                                        encoding:NSUTF8StringEncoding error:&error];
//    fileTotalLines = [xmlFileString componentsSeparatedByString:@"\n"].count;
//    
    double timeCheck = 0;//[[NSDate date] timeIntervalSince1970];
    while ((line = [reader readLine]))
    {
        double deltaTime;
        deltaTime = [[NSDate date] timeIntervalSince1970] - timeCheck;
        
        lineNumber++;
        
        if (deltaTime > 0.5f)
        {
            timeCheck = [[NSDate date] timeIntervalSince1970];
            [mainQueue addOperationWithBlock:
             ^{
                 if (DEBUG_QUEUE) NSLog(@"Queue Info: ProcesarArchivo start: - Update progressBar progress");
                 if (tempView)
                 {
                     tempView.progress = (CGFloat)[reader currentOffset] / (CGFloat)[reader totalFileLength];
                 }
             }];
        }

        
        line = [line stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
        
        if ( nextDataSet )
        {
            
            if ([nombreTabla length] > 0)
            {
                [self insertOnTable:nombreTabla];
                [rowList removeAllObjects];
            }
            
            if ([line isEqualToString:@"<DATASET>"]) {
                nextDataSet = NO;
                nextNombreTabla = YES;
                cnn = 0;
            }
        } else
            
        if (nextNombreTabla)
        {
            nombreTabla = [[line componentsSeparatedByString:@"="] objectAtIndex:1];
            
            NSString *sql = [NSString stringWithFormat:@"delete from %@;", nombreTabla];
            
            
            @try {
                [[DB getInstance] excecuteSQL:sql];
            }
            @catch (NSException *exception) {
                NSLog(@"DBError %@",exception.reason);        }
            @finally {
            }

            
            nextNombreTabla = NO;
            nextBlockSize = YES;
            
        } else
            
        if (nextBlockSize)
        {
            // hago lo que se necesite con el blocksize
            nextBlockSize = NO;
            nextCount = YES;
        } else
            
        if (nextCount)
        {
            count = [[line componentsSeparatedByString:@"="] objectAtIndex:1];
            
            nextCount = NO;
            nextData = YES;
            
        } else
        
        if (nextData)
        {
            if ([line isEqualToString:@"</DATASET>"])
            {
                [self insertOnTable:nombreTabla];
                [rowList removeAllObjects];
                nextData = NO;
                nextDataSet = YES;
            
            } else
            {
                cnn++;
//                NSString *msg = [NSString stringWithFormat:@"Procesando %d/%@ registros de la tabla %@ ",cnn, count, nombreTabla];
//                [self status:msg];
                
                //[self insert:line onTable:nombreTabla];
                if ([line length] > 2)
                    [rowList addObject:line];
                
                if ([rowList count] > 50)
                {
                    [self insertOnTable:nombreTabla];
                    [rowList removeAllObjects];
                }
            }
            
        } else
        {
            // jamas deveria entrar aca,
            return NO;
        }
    }
    
    if ([nombreTabla length] > 0)
    {
        [self insertOnTable:nombreTabla];
        [rowList removeAllObjects];
    }
    
    tempView = nil;
    [mainQueue cancelAllOperations];
    
    NSLog(@" - Procesado de archivo finalizado");
    
    return  YES;
}


-(BOOL) startOld:(NSString*)filePath
{
    
    
    /*
     
     El proceso se peude resumir en 3 pasos
     1 - busco inicio bloque <DATASET>
     2 - Borro Tabla
     3 - Inserto registros nuevos
     */
    
    DDFileReader * reader = [[DDFileReader alloc] initWithFilePath:filePath];
    NSString * line = nil;
    
    NSString *nombreTabla = nil;
    NSString *count = nil;
    NSInteger cnn = 0;
    
    BOOL nextNombreTabla = NO;
    BOOL nextDataSet = YES;
    BOOL nextBlockSize = NO;
    BOOL nextCount = NO;
    BOOL nextData = NO;
    
    while ((line = [reader readLine])) {
        
        line = [line stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
        
        if ( nextDataSet )
        {
            if ([line isEqualToString:@"<DATASET>"]) {
                nextDataSet = NO;
                nextNombreTabla = YES;
                cnn = 0;
            }
        }
        else if (nextNombreTabla)
        {
            nombreTabla = [[line componentsSeparatedByString:@"="] objectAtIndex:1];
            
            NSString *sql = [NSString stringWithFormat:@"delete from %@;", nombreTabla];
            
            
            @try {
                [[DB getInstance] excecuteSQL:sql];
            }
            @catch (NSException *exception) {
                NSLog(@"DBError %@",exception.reason);        }
            @finally {
            }
            
            nextNombreTabla = NO;
            nextBlockSize = YES;
            
        }
        else if (nextBlockSize)
        {
            // hago lo que se necesite con el blocksize
            nextBlockSize = NO;
            nextCount = YES;
        }
        else if (nextCount)
        {
            count = [[line componentsSeparatedByString:@"="] objectAtIndex:1];
            
            nextCount = NO;
            nextData = YES;
            
        }
        else if (nextData)
        {
            if ([line isEqualToString:@"</DATASET>"])
            {
                nextData = NO;
                nextDataSet = YES;
            }
            else
            {
                cnn++;
                NSString *msg = [NSString stringWithFormat:@"Procesando %d/%@ registros de la tabla %@ ",cnn, count, nombreTabla];
                [self status:msg];
                
                [self insert:line onTable:nombreTabla];
            }
            
        }
        else
        {
            // jamas deveria entrar aca,
            return NO;
        }
    }
    
    return  YES;
}


-(void) insert:(NSString*) registro onTable:(NSString*) tabla
{
    @autoreleasepool {
        NSString *str   = [registro substringToIndex:[registro length]-1];
        NSArray *values = [str componentsSeparatedByString:@"|"];
        
        NSString *sql = [NSString stringWithFormat:@"insert into %@ values(",tabla];
        
        // Tengo que controlar si el nombre de la tabla empieza con "std_" debo usar la rutina de conversión
        // que trata las fechas como dd/MM/yyyy
        if ([[[tabla substringToIndex:4] lowercaseString] isEqualToString:@"std_"])
        {
            for (NSString *o in values)
            {
                sql = [NSString stringWithFormat:@"%@%@,",sql, [self convertFechaInvertida:o]];
            }
        } else
        {
            for (NSString *o in values)
            {
                sql = [NSString stringWithFormat:@"%@%@,",sql, [self convert:o]];
            }
        }
        
//        if ([tabla isEqualToString:@"med_medicos"])
//        {
//            NSLog(@"procesararchivo: %@", registro);
//        }
        
        if ([tabla isEqualToString:@"mtc_estadosvisita"])
        {
            sql = [NSString stringWithFormat:@"%@1,",sql];
        }
        
        sql = [sql substringToIndex:[sql length]-1];
        sql = [NSString stringWithFormat:@"%@);",sql];
        
        //NSLog(@"%@", sql);
        
        @try {
            [[DB getInstance] excecuteSQL:sql];
        }
        @catch (NSException *exception) {
            NSLog(@"DBError %@",exception.reason);        }
        @finally {
        }

        
    }
}

-(void) insertOnTable:(NSString*) tabla
{
    
    if ([rowList count] < 1)
        return;
    
    @autoreleasepool
    {
        columnsList   = [[DB getInstance] getTableColumnsList:tabla];
        columnsString = [columnsList componentsJoinedByString:@","];
        
        NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (%@) ", tabla, columnsString];
        
        for (NSString *registro in rowList)
        {
            
            sql = [NSString stringWithFormat:@"%@ SELECT ",sql];
            NSString *str  = [registro substringToIndex:[registro length]-1];
            str = [str stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
            
            NSInteger columnCount = 1;
            NSArray *values = [str componentsSeparatedByString:@"|"];
        
            
       
            // Tengo que controlar si el nombre de la tabla empieza con "std_" debo usar la rutina de conversión
            // que trata las fechas como dd/MM/yyyy
            if ([[[tabla substringToIndex:4] lowercaseString] isEqualToString:@"std_"])
            {
                for (NSString *o in values)
                {
                    if (!(columnCount > [columnsList count]))
                        sql = [NSString stringWithFormat:@"%@%@,",sql, [self convertFechaInvertida:o]];
                    
                    columnCount++;
                }
            } else
            {
                for (NSString *o in values)
                {
                    if (!(columnCount > [columnsList count]))
                        sql = [NSString stringWithFormat:@"%@%@,",sql, [self convert:o]];
                    
                    columnCount++;
                }
            }
            
            
            
            if ([tabla isEqualToString:@"mtc_estadosvisita"])
            {
                sql = [NSString stringWithFormat:@"%@1,",sql];
                columnCount++;
            }
            
            
            // Completo con Null si faltan datos para la tabla.
            if ([columnsList count] > [values count])
            {
                while (!(columnCount > [columnsList count]))
                {
                    sql = [NSString stringWithFormat:@"%@NULL,",sql];
                    columnCount++;
                }
            }
            
            sql = [sql substringToIndex:[sql length]-1];
            sql = [NSString stringWithFormat:@"%@ UNION ",sql];
        }
        
        sql = [sql substringToIndex:[sql length]-6];
        sql = [NSString stringWithFormat:@"%@;",sql];
        
        NSLog(@"%@",sql);
        
        @try {
                    [[DB getInstance] excecuteSQL:sql];
        }
        @catch (NSException *exception) {
            NSLog(@"DBError %@",exception.reason);        }
        @finally {
        }

        
    }
}

-(NSString*)convert:(NSString*)str
{
    
    if ((str == nil) || ([str length] == 0))
    {
        return @"null";
    }
    
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    
    if (([str length] > 0) && ([str isNumber]))
    {
        return str;
    }
    
    
    NSArray *fecha = [str componentsSeparatedByString:@"/"];
    if ([fecha count] == 3) {
        NSArray *fecha  = [str componentsSeparatedByString:@" "];
        NSArray *dia    = [[fecha objectAtIndex:0] componentsSeparatedByString:@"/"];
        
        if ([dia count] == 3)
        {
            // Teniendo en cuenta que hay textos separados por / en la base de datos y que el formato de la fecha que se espera son números,
            // tengo que chequear que sean números para evitar que me convierta x...x/x...x/x...x en 0.
            if (([[dia objectAtIndex:0] isNumber]) &&
                ([[dia objectAtIndex:1] isNumber]) &&
                ([[dia objectAtIndex:2] isNumber]))
            {
//                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                [formatter setDateFormat:@"MM/dd/yyyy"];
//                NSDate *date = [formatter dateFromString:[fecha objectAtIndex:0]];
                NSDate *date = [[NSDate alloc] initWithString:[fecha objectAtIndex:0] withFormat:@"MM/dd/yyyy"];
                
                return [NSString stringWithFormat:@"%.0f",[date timeIntervalSince1970]];
            }
        }
    }
    
    return [NSString stringQuoted:str ifNull:@"NULL"];
    //return [NSString stringWithFormat:@"\"%@\"", str];
    
}

-(NSString*)convertFechaInvertida:(NSString*)str
{
    
    if ((str == nil) || ([str length] == 0))
    {
        return @"null";
    }
    
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    
    if (([str length] > 0) && ([str isNumber]))
    {
        return str;
    }
    
    
    NSArray *fecha = [str componentsSeparatedByString:@"/"];
    if ([fecha count] == 3) {
        NSArray *fecha  = [str componentsSeparatedByString:@" "];
        NSArray *dia    = [[fecha objectAtIndex:0] componentsSeparatedByString:@"/"];
        
        if ([dia count] == 3)
        {
            // Teniendo en cuenta que hay textos separados por / en la base de datos y que el formato de la fecha que se espera son números,
            // tengo que chequear que sean números para evitar que me convierta x...x/x...x/x...x en 0.
            if (([[dia objectAtIndex:0] isNumber]) &&
                ([[dia objectAtIndex:1] isNumber]) &&
                ([[dia objectAtIndex:2] isNumber]))
            {
//                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                [formatter setDateFormat:@"dd/MM/yyyy"];
//                NSDate *date = [formatter dateFromString:[fecha objectAtIndex:0]];
                NSDate *date = [[NSDate alloc] initWithString:[fecha objectAtIndex:0] withFormat:@"dd/MM/yyyy"];
                
                return [NSString stringWithFormat:@"%.0f",[date timeIntervalSince1970]];
            }
        }
    }
    
    return [NSString stringQuoted:str ifNull:@"NULL"];
    //return [NSString stringWithFormat:@"\"%@\"", str];
    
}


-(void) status:(NSString*) mensaje
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.lbStatusTransmisionViewController.text = mensaje;
    });
    
    
}

@end
