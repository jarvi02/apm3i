//
//  VisitasViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 08/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "VisitasViewController.h"
#import "FichaVisita.h"
#import "NSDate+extensions.m"
#import "VisitaViewController.h"
#import "NoVisitaViewController.h"

#import "DB.h"
#import "ConfigGlobal.h"
#import "Visita.h"
#import "MTVisita.h"
#import "Config.h"
#import "ParteDiario.h"
#import "ParteDiarioData.h"
#import "Novedad.h"
#import "Promocion.h"
#import "ProductoTrazabilidad.h"
#import "MTEditActions.h"

#import "NSStringExtended.h"

@interface VisitasViewController ()
-(BOOL)deleteVisita:(FichaVisita*)fichaVisita;
-(BOOL)visitaFueTransmitida:(FichaVisita*)fichaVisita;
@end

@implementation VisitasViewController

- (void)dealloc
{
    [_data release];
    
    [_tmpCell release];
	[_cellNib release];
    
    [super dealloc];
}

- (id)initWithData:(NSArray*)aData
{
    self = [super init];
    if (self) {
        self.cellNib    = [UINib nibWithNibName:@"FichaVisitaCell" bundle:nil];
        self.data       = [NSMutableArray arrayWithArray:aData];
        
        
        // tuve que hacer este hack para que me muestre Sin Visita cuando borre el ultimo registro
        if ([self.data count] == 0) {
            
            FichaVisita *a = [[FichaVisita alloc] init];
            a.fecha = @"-1";
            a.descripcion = @"Sin Visita";
            [self.data addObject:a];
            self.editing = NO;
            
        }
        
    }
    return self;
}



- (void)updateData:(NSArray*)aData
{
//    if (self.data)
//    {
//        [_data release];
//    }
    
    self.data = [NSMutableArray arrayWithArray:aData];
        
        
    // tuve que hacer este hack para que me muestre Sin Visita cuando borre el ultimo registro
    if ([self.data count] == 0)
    {
        FichaVisita *a = [[FichaVisita alloc] init];
        a.fecha = @"-1";
        a.descripcion = @"Sin Visita";
        [self.data addObject:a];
        self.editing = NO;
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    static NSString *MyIdentifier = @"FichaVisitaCelldentifier";
	
    // Obtain the cell object.
	FichaVisitaCell *cell = (FichaVisitaCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	// If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
		cell = self.tmpCell;
		self.tmpCell = nil;
        
    }
    
    FichaVisita *o = [self.data objectAtIndex:indexPath.row];
    
    if ([o.fecha isEqualToString:@"-1"]) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.lbFecha.text = o.descripcion;
        return cell;
    } else
    {
        Visita *visita = [[Visita getByID:o.idVisita idMedico:o.idMedico] retain];
        NSInteger bTransmitido = [self visitaFueTransmitida:o];
        
        cell.lbFecha.textColor  = UIColorFromRGB(defaultInteractiveCellFontColor);
        cell.lbVisita.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
        
        if ((bTransmitido == 1) || (visita.contacto == 0))
        {
            //cell.accessoryType      = UITableViewCellAccessoryNone;
            
            if (bTransmitido == 1)
            {
                //cell.accessoryType      = UITableViewCellAccessoryDetailDisclosureButton;
                
                cell.lbFecha.textColor  = UIColorFromRGB(defaultFixedInteractiveCellFontColor);
                cell.lbVisita.textColor = UIColorFromRGB(defaultFixedInteractiveCellFontColor);
            }
        }
    }
    
    cell.lbFecha.text       = [o.fechaAsDate formattedStringUsingFormat:@"dd/MM/yyyy"];
    cell.lbVisita.text      = o.descripcion;
    
	return cell;
    
    
    
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{

    FichaVisita *o = [self.data objectAtIndex:0];
    if ([o.fecha isEqualToString:@"-1"]) {
        return NO;
    }
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
       
        FichaVisita *o = [self.data objectAtIndex:indexPath.row];
        if ([self deleteVisita:o])
        {
            [self.data removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        
        // tuve que hacer este hack para que me muestre Sin Visita cuando borre el ultimo registro
        if ([self.data count] == 0) {
            
            FichaVisita *a = [[FichaVisita alloc] init];
            a.fecha = @"-1";
            a.descripcion = @"Sin Visita";
            [self.data addObject:a];
            [a release];
            self.editing = NO;
            [tableView reloadData];
        }

        
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {

    }
    
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UILabel *label = [[[UILabel alloc] init] autorelease];
    label.text = @"   Visitas";
    label.font = [UIFont systemFontOfSize:14.0f];
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.backgroundColor = UIColorFromRGB(defaultHeaderCellColor);
    
    return label;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // tuve que hacer este hack para que me muestre Sin Visita cuando borre el ultimo registro y no deje seleccionarlo
    FichaVisita *o = [self.data objectAtIndex:[indexPath row]];
    if ([o.fecha isEqualToString:@"-1"])
    {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        
        // NO VISITA
        
        // Chequeo si se trata de una no visita.
        Visita *visita = [[Visita getByID:o.idVisita idMedico:o.idMedico] retain];
        if (visita.contacto == 0)
        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"No es posible modificar una No Visita" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
//            [alert show];
//            [alert release];
            
            // Chequeo si la visita o no visita ya fue transmitida.
            if ( [self visitaFueTransmitida:o] == 1 )
            {
                NoVisitaViewController *nvc = [[NoVisitaViewController alloc] initWithDefaultNib];
                
                nvc.modo = NoVisitaModoVer;
                nvc.noVisita = [MTVisita getByID:o.idVisita idMedico:o.idMedico];
                nvc.ficha  = o;
                
                [self.navigationControllerReference pushViewController:nvc animated:YES];
                [nvc release];               
                
            } else
            {
                NoVisitaViewController *nvc = [[NoVisitaViewController alloc] initWithDefaultNib];
                
                nvc.modo = NoVisitaModoModificar;
                nvc.noVisita = [MTVisita getByID:o.idVisita idMedico:o.idMedico];
                nvc.ficha  = o;
                
                [self.navigationControllerReference pushViewController:nvc animated:YES];
                [nvc release];
            }
            
            return;
        }
        
        
        
        // VISITA
        
        // Chequeo si la visita o no visita ya fue transmitida.
        if ( [self visitaFueTransmitida:o] == 1 )
        {
            VisitaViewController *vc = [[VisitaViewController alloc] initWithNibName:@"VisitaViewController" bundle:nil];
            
            vc.modo = VisitaModoVer;
            vc.visita = [MTVisita getByID:o.idVisita idMedico:o.idMedico];
            vc.ficha  = o;
            
            [self.navigationControllerReference pushViewController:vc animated:YES];
            [vc release];

//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"No es posible modificar una visita transmitida" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
//            [alert show];
//            [alert release];
            
        } else
        {
            VisitaViewController *vc = [[VisitaViewController alloc] initWithNibName:@"VisitaViewController" bundle:nil];
            
            vc.modo = VisitaModoModificar;
            vc.visita = [MTVisita getByID:o.idVisita idMedico:o.idMedico];
            vc.ficha  = o;
            
            [self.navigationControllerReference pushViewController:vc animated:YES];
            [vc release];
        }
        
    }
 
    
}



-(BOOL)deleteVisita:(FichaVisita*)fichaVisita {
    
    MTEditActionsType actionParteDiario = kMTEANone;

    #ifdef DEBUG_VISITA
    NSLog(@"--- deleteVisita --");
    #endif
    
#ifndef DEBUG_VISITA
    // Chequeo si la visita fue transmitida.
    if ( [self visitaFueTransmitida:fichaVisita] == 1 )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"No es posible eliminar una visita transmitida" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
        [alert release];
        return NO;
    }
#endif
    
    // Chequeo si la visita corresponde a un médico fluctuante.
    if (self.carteraViewControllerReference.itemCarteraMedicoSelected.asignacion == 2)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"No es posible eliminar una visita fluctuante" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
        [alert release];
        return NO;
    }
    
    Visita *visita = [[Visita getByID:fichaVisita.idVisita idMedico:fichaVisita.idMedico] retain];
    NSArray *partesDiarios = [[ParteDiario getAllParteDiario:fichaVisita.fechaAsDate] retain];
    
    // Chequeo si graba la actividad del médico al cargar una visita.
    BOOL parte_sin_medico =  [[ConfigGlobal shareInstance] find:PARTE_SIN_MEDICOS];
    if ((!parte_sin_medico) && (visita.contacto == 1))
    {
        // Modificar el parte diario
        if ([partesDiarios count] == 0)
        {
            // Si el query no devuelve ningún registro   MENSAJE : "No existe el parte.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"No existe el parte" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        else
        {
            // TODO: Recalcular la cantidad de partes diarios segunlas visitas de todos los medicos.
            
            int cnn = 0;
            int idParteDiario;
            
            #ifdef DEBUG_VISITA
            NSLog(@"Actidad medico: %@", self.carteraViewControllerReference.itemCarteraMedicoSelected.actividad);
            #endif
            
            for (ParteDiarioData *o in partesDiarios)
            {
                //cnn = 0;
                idParteDiario = o.partediario;
                
                
                // Chequeo si la activiadad es 9999
                if (o.tarea == 9999)
                {
                    // Si la visita a eliminar tiene ACTIVIDAD : T
                    if ([self.carteraViewControllerReference.itemCarteraMedicoSelected.actividad isEqualToString:@"T"])
                    {
                        if ([o.actividad isEqualToString:@"T"])
                        {
                            if (o.cantidad > 0)
                            {
                                o.cantidad = o.cantidad -1;
                            
                                // UPDATE de nodo_tareaspartediario
                                actionParteDiario = kMTEAChange;
                                [ParteDiario restarTarea:o.id_nodotareaspartediario parteDiario:idParteDiario actividad:@"T" tarea:9999];
                                
                                // Novedad
                                [Novedad insert: @"NODO_TAREASPARTEDIARIO"
                                        novedad: [NSString stringWithFormat:@"%@|%@|%@|T|9999|%@",
                                                    [NSString intToStr:o.id_nodotareaspartediario],
                                                    SettingForKey(NODO, @""),
                                                    [NSString intToStr:idParteDiario],
                                                    [NSString intToStr:o.cantidad]
                                                    ]
                                           tipo:kUpdate];
                            }
                        }
                        
                    } else
                        
                    // Si la visita a eliminar tiene ACTIVIDAD : M
                    if ([self.carteraViewControllerReference.itemCarteraMedicoSelected.actividad isEqualToString:@"M"])
                    {
                       if ([o.actividad isEqualToString:@"M"])
                       {
                           if (o.cantidad > 0)
                           {
                               o.cantidad = o.cantidad -1;
                        
                               // UPDATE de nodo_tareaspartediario
                               actionParteDiario = kMTEAChange;
                               [ParteDiario restarTarea:o.id_nodotareaspartediario parteDiario:idParteDiario actividad:@"M" tarea:9999];
                               
                               // Novedad
                               [Novedad insert: @"NODO_TAREASPARTEDIARIO"
                                       novedad: [NSString stringWithFormat:@"%@|%@|%@|M|9999|%@",
                                                 [NSString intToStr:o.id_nodotareaspartediario],
                                                 SettingForKey(NODO, @""),
                                                 [NSString intToStr:idParteDiario],
                                                 [NSString intToStr:o.cantidad]
                                                 ]
                                          tipo:kUpdate];
                           }
                        }
                    }
                }
                
                if (o.cantidad == 0) { cnn++;}
                
            }
            
            // Si las tareas que devolvió ambas son del ID = 9999 y cantidades = 0 (después de haber restado 1 a la cantidad)
            // Hacer un delete de las tareas que se trajeron en el query y del partediario.
            if (cnn == [partesDiarios count])
            {  
                /* DELETE NODO_PARTEDIARIO */                
                actionParteDiario = kMTEADelete;
            }
            
            
            
        }     
    } 
    
    

    
    /* MED_MEDICOS ------------------------------- */
    
    NSString *sql;
    NSString *query;
    //NSString *logNovedad = nil;
    
    NSMutableArray *pipesNovedad = [[NSMutableArray alloc] init];
    
    int visitasRealizadasMenosUno = self.carteraViewControllerReference.itemCarteraMedicoSelected.visitasRealizadas;
    // Si es visita, decremento la cantiada the visitas realizadas
    if (visita.contacto == 1)   { visitasRealizadasMenosUno--; }
    
    MTMedico *medico = [[MTMedico GetById:fichaVisita.idMedico] retain];
    
    if (visitasRealizadasMenosUno <= 0)
    {
        medico.visitasRealizadas = 0;
    } else
    {
        medico.visitasRealizadas = visitasRealizadasMenosUno;
    }
    
    /* UPDATE MED_MEDICOS */
    medico.objetivos = [NSString string:visita.objetivos ifNull:@""];
    [medico updateVisitaData];

    CarteraMedico* cm = self.carteraViewControllerReference.itemCarteraMedicoSelected;
    cm.visitasRealizadas = medico.visitasRealizadas;
    cm.objetivos         = [NSString string:medico.objetivos];
    
    
    [medico release];
    
    
    
    sqlite3_stmt *statement = nil;
    // Si es una visita, elimino las promociones también.
    if (visita.contacto == 1)
    {
     
        // Actualizo MTC_LOTES
        
        NSUInteger idMedico = self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico;
        NSArray *current = [Promocion GetAllByMedico:idMedico visita:visita.idVisita];
        for (Promocion *p in current)
        {
            [ProductoTrazabilidad actualizarStock:p.producto lote:p.lote nuevoStock:(p.stock + p.muestras)];
        }
        
        
        
        /* MED_PROMOCIONES ------------------------------- */
        
        sql = @"select nodo || '|' || medico || '|' || visitador || '|' || visita || '|' || orden || '|' || producto || '|' || muestras || '|' || case when obsequio = 1 then 'S' else 'N' end || '|' || case when literatura = 1 then 'S' else 'N' end || '|' || case when evaluacion is null then '' else evaluacion end || '|' || lote as registro from med_promociones where nodo = %@ and medico = %d and visitador = %@ and visita = %d";
        
        query = [NSString stringWithFormat:sql, SettingForKey(NODO, @""), self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico, SettingForKey(NODO, @""), visita.idVisita];
        
        statement = [[DB getInstance] prepare:query];
        if ( statement )
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *str = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                [Novedad insert:@"MED_PROMOCIONES" novedad:str tipo:kDelete];
            }
            
        } else
        {
            NSAssert(0, @"No se encontre la tabla");
        }
        sqlite3_finalize(statement);
        
        /* DELETE MED_PROMOCIONES */
        sql = @"DELETE FROM MED_PROMOCIONES WHERE nodo = %@ and medico = %d and visitador = %@ and visita = %d;";
        query = [NSString stringWithFormat:sql, SettingForKey(NODO, @""), self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico, SettingForKey(NODO, @""), visita.idVisita];
        
        [[DB getInstance] excecuteSQL:query];
    }
    
    
    
    
    /* MED_VISITAS ------------------------------- */
    
    sql = @"Select nodo || '|' || medico || '|' || visitador || '|' || id || '|' || ifnull(strftime('%@', fecha,'unixepoch','localtime'), '') || '|' || tipo || '|' || domicilio || '|' || ifnull(objetivos, '') || '|' || ifnull(evaluacion, '') from med_visitas where nodo = %@ and medico =%d and  visitador = %@ and  id = %d;";
    
    query = [NSString stringWithFormat:sql, @"%m/%d/%Y %H:%M" ,SettingForKey(NODO, @""), self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico, SettingForKey(NODO, @""), visita.idVisita];
    
    #ifdef DEBUG_VISITA
    NSLog(@"- query med_visitas: %@", query);
    #endif
    
    statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            NSString *str = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            [Novedad insert:@"med_visitas" novedad:str tipo:kDelete];
            
            #ifdef DEBUG_VISITA
            NSLog(@"- novedad med_visitas: %@", str);
            #endif
        }
        
    } else
    {
        NSAssert(0, @"No se encontre la tabla");
    }
    sqlite3_finalize(statement);
    
    /* DELETE MED_VISITAS */
    sql = @"DELETE FROM MED_VISITAS WHERE nodo = %@ and medico = %d and visitador = %@ and id = %d;";
    
    query = [NSString stringWithFormat:sql, SettingForKey(NODO, @""), self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico, SettingForKey(NODO, @""), visita.idVisita];
    [[DB getInstance] excecuteSQL:query];
    
    //SI PARTE_SIN_MEDICOS=N, se decrementa la cantidad de visitas en el parte diario.
    if ((!parte_sin_medico) && (visita.contacto == 1))
    {
        //Las tarea visitas quedaron en cero, pero el parte tiene otras tareas
        /* Obtener el partediario para la visita */
        sql = @"select parte.id from nodo_partediario parte where parte.nodo = %@ and fecha = %0.f";
        query = [NSString stringWithFormat:sql, SettingForKey(NODO, @""), [visita.fecha dateAsDateWithoutTime].timeIntervalSince1970];
        
        int idParteDiario = 0;
        statement = [[DB getInstance] prepare:query];
        if ( statement ) {
            
            if (sqlite3_step(statement) == SQLITE_ROW) {
                idParteDiario = sqlite3_column_int(statement, 0);
            }
            
        } else {
            NSAssert(0, @"No se encontre la tabla");
        }
        sqlite3_finalize(statement);
        
        
        
        
        /* NODO_TAREASPARTEDIARIO ------------------------------- */
        
        [pipesNovedad removeAllObjects];
        if (actionParteDiario ==  kMTEADelete)
        {
            sql = @"select id || '|' || nodo || '|' || partediario || '|' || actividad || '|' || tarea || '|' || cantidad from NODO_TAREASPARTEDIARIO Where nodo = %@  and actividad in ('T','M') and partediario = %d";
            
            query = [NSString stringWithFormat:sql,SettingForKey(NODO, @""),idParteDiario];
            
            #ifdef DEBUG_PARTEDIARIO
            NSLog(@"- query nodo_partediario: %@", query);
            #endif
            
            statement = [[DB getInstance] prepare:query];
            if ( statement ) {
                
                while (sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSString *str = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                    [pipesNovedad addObject:str];
                    //                [Novedad insert:@"NODO_TAREASPARTEDIARIO" novedad:str tipo:kDelete];
                    
                    #ifdef DEBUG_PARTEDIARIO
                    NSLog(@"- novedad nodo_partediario: %@", str);
                    #endif
                }
                
            } else {
                NSAssert(0, @"No se encontre la tabla");
            }
            sqlite3_finalize(statement);

            
            for (NSString *str in pipesNovedad)
            {
                [Novedad insert:@"NODO_TAREASPARTEDIARIO" novedad:str tipo:kDelete];
            }
            
            
            /* DELETE NODO_TAREASPARTEDIARIO */
            sql = @"delete from NODO_TAREASPARTEDIARIO Where nodo = %@  and actividad in ('T','M') and partediario = %d";
            query = [NSString stringWithFormat:sql,SettingForKey(NODO, @""),idParteDiario];
            
            #ifdef DEBUG_PARTEDIARIO
            NSLog(@"- delete nodo_tareaspartediario: %@", query);
            #endif
            
            [[DB getInstance] excecuteSQL:query];
        }
        
        
        /* NODO_PARTEDIARIO ------------------------------- */
        
        [pipesNovedad removeAllObjects];
        
        if (actionParteDiario ==  kMTEADelete)
        {
            sql = @"select id|| '|' ||nodo|| '|' ||ifnull(strftime('%@', fecha,'unixepoch','localtime'), '') from NODO_PARTEDIARIO WHERE  id = %d and nodo = %@ and fecha =%0.f";
            
            query = [NSString stringWithFormat:sql,@"%m/%d/%Y %H:%M", idParteDiario, SettingForKey(NODO, @""), [visita.fecha dateAsDateWithoutTime].timeIntervalSince1970];
            
            #ifdef DEBUG_PARTEDIARIO
            NSLog(@"- query nodo_partediario: %@", query);
            #endif
            
            statement = [[DB getInstance] prepare:query];
            if ( statement )
            {
                while (sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSString *str = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                    
                    #ifdef DEBUG_PARTEDIARIO
                    NSLog(@"- novedad nodo_partediario: %@", str);
                    #endif
                    
                    [pipesNovedad addObject:str];
                    
                    //[Novedad insert:@"NODO_PARTEDIARIO" novedad:str tipo:kDelete];
                }
                
            } else {
                NSAssert(0, @"No se encontre la tabla");
            }
            sqlite3_finalize(statement);
            
            
            for (NSString *str in pipesNovedad)
            {
                [Novedad insert:@"NODO_PARTEDIARIO" novedad:str tipo:kDelete];
            }
                
            /* DELETE A LA NODO_PARTEDIARIO */
            sql = @"delete from NODO_PARTEDIARIO WHERE  id = %d and nodo = %@ and fecha =%0.f";
            query = [NSString stringWithFormat:sql,idParteDiario, SettingForKey(NODO, @""), [visita.fecha dateAsDateWithoutTime].timeIntervalSince1970];
            
            #ifdef DEBUG_PARTEDIARIO
            NSLog(@"- delete nodo_partediario: %@", query);
            #endif
            
            [[DB getInstance] excecuteSQL:query];
        }
        
        
        // TODO: Buscar la última visita del médico para cambiar el estado en la cartera.
    
    }
    
    // Limpia todos los partes de la base
    [ParteDiario cleanTareasParte];
    [ParteDiario cleanParte];

    [partesDiarios release];
    [pipesNovedad release];
    [visita release];
    return  YES;
}



-(BOOL)visitaFueTransmitida:(FichaVisita*)fichaVisita {
    
    NSString *query = [NSString stringWithFormat:@"select transmitido from log_novedades where registro = \"%@\"", fichaVisita.pipe];
    
    #ifdef DEBUG_VISITA
    NSLog(@"--- visitaFueTransmitida ---");
    NSLog(@"query: %@", query);
    #endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    NSInteger transmitido = 1;
    if ( statement ) {
        
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
            transmitido = sqlite3_column_int(statement, 0);
        }
        
    } else {
        NSAssert(0, @"No se puede ejecutar: %@", query);
    }
    sqlite3_finalize(statement);
    
    return transmitido;
}

@end
