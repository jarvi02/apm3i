//
//  DistribucionCantidadViewController.h
//  APM4
//
//  Created by Laura Busnahe on 9/9/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "DistribucionCantidadCell.h"
#import "DistribucionCantidadHeader.h"
#import "DistribucionCantidadFooter.h"

@interface DistribucionCantidadViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate,
                                                                      MTdistribucionCantidadHeaderDelegate>

@property (nonatomic, strong) NSMutableArray                      *data;
@property (nonatomic, strong) IBOutlet DistribucionCantidadCell   *tmpCell;
@property (nonatomic, strong) UINib                               *cellNib;
@property (nonatomic, strong) UINib                               *headerNib;

@property (strong, nonatomic) IBOutlet DistribucionCantidadHeader *tableHeader;
@property (retain, nonatomic) IBOutlet DistribucionCantidadFooter *tableFooter;
@property (retain, nonatomic) IBOutlet UITableView                *tableView;

- (id)initViewDefaultNib;

@end
