//
//  MTHistoricoProductos.h
//  APM4
//
//  Created by Laura Busnahe on 7/12/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTHistoricoProductos : NSObject

@property (nonatomic, strong)NSString *orden01;
@property (nonatomic, strong)NSString *orden02;
@property (nonatomic, strong)NSString *orden03;
@property (nonatomic, strong)NSString *orden04;
@property (nonatomic, strong)NSString *orden05;
@property (nonatomic, strong)NSString *orden06;
@property (nonatomic, strong)NSString *orden07;
@property (nonatomic, strong)NSString *orden08;
@property (nonatomic, strong)NSString *orden09;
@property (nonatomic, strong)NSString *orden10;
@property (nonatomic, strong)NSString *orden11;
@property (nonatomic, strong)NSString *orden12;

@property (nonatomic, strong)NSString *descripcion01;
@property (nonatomic, strong)NSString *descripcion02;
@property (nonatomic, strong)NSString *descripcion03;
@property (nonatomic, strong)NSString *descripcion04;
@property (nonatomic, strong)NSString *descripcion05;
@property (nonatomic, strong)NSString *descripcion06;
@property (nonatomic, strong)NSString *descripcion07;
@property (nonatomic, strong)NSString *descripcion08;
@property (nonatomic, strong)NSString *descripcion09;
@property (nonatomic, strong)NSString *descripcion10;
@property (nonatomic, strong)NSString *descripcion11;
@property (nonatomic, strong)NSString *descripcion12;


+(NSArray*)getAllForMedico:(NSUInteger)idMedico;

@end
