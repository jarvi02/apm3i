//
//  MotivosNoVisita.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 01/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MotivosNoVisita.h"
#import "DB.h"

@implementation MotivosNoVisita


+(NSArray*)GetAll{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"select id, descripcion, contacto, estado from mtc_tiposvisita where contacto = 0;";

    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
        MotivosNoVisita *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[MotivosNoVisita alloc] init];
            
            o.idMotivo = sqlite3_column_int(statement, 0);
            
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            o.estado = sqlite3_column_int(statement, 2);
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
    
    
}

@end
