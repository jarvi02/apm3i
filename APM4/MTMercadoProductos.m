//
//  MTMercadoProductos.m
//  APM4
//
//  Created by Laura Busnahe on 3/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#define DEBUG_MERCADOPRODUCTOS   1

#import "MTMercadoProductos.h"
#import "MTMercados.h"
#import "DB.h"



@implementation MTMercadoProductos



+ (NSArray*)getByAuditoria:(NSInteger)idAuditoria Mercados:(NSArray*)arrayMercados
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    //  select distinct producto
    //  from dpm_mercadoproductos
    //  where auditoria = :auditoria
    //        and mercado in (*lista de mercados*)
    //  into :producto
    
    // TODO: Podría agregarse todos los campos de la tabla, pero apara optimizar, solo cargo el producto.
    NSString *sql = @"select distinct producto "
    "from dpm_mercadoproductos "
    "where auditoria = %i "
    "and mercado in (%@) ";
    
    NSString *sMercados = [MTMercados getSelectedIdsString:arrayMercados];
    NSString *query = [NSString stringWithFormat:sql, idAuditoria, sMercados];
    
    
#ifdef DEBUG_MERCADOPRODUCTOS
    NSLog(@"--- MercadoProductos query ---");
    NSLog(@"%@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    MTMercadoProductos *o;
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
			
            o = [[MTMercadoProductos alloc] init];
            // id Producto
            int _producto = sqlite3_column_int(statement, 0);
            o.producto = _producto;

//            @property                    NSInteger  auditoria;
//            @property                    NSInteger  mercado;
//            @property                    NSInteger  producto;
//            @property                    NSInteger  total;
            
//            // id Auditoría
//            int _auditoria = sqlite3_column_int(statement, 0);
//            o.auditoria = _auditoria;
//            
//            // id Mercado
//            int _mercado = sqlite3_column_int(statement, 1);
//            o.mercado = _mercado;
//            
//            // id Producto
//            int _producto = sqlite3_column_int(statement, 2);
//            o.producto = _producto;
//            
//            // Total
//            int _total = sqlite3_column_int(statement, 3);
//            o.total = _total;
            
            [array addObject:o];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla dpm_mercadoproductos");
	}
	sqlite3_finalize(statement);
    
    
    return array;
    
}

@end
