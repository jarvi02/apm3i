//
//  MMEditViewController.m
//  APM4
//
//  Created by Laura Busnahe on 3/26/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MMEditViewController.h"

@interface MMEditViewController ()

@end

@implementation MMEditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initViewDefaultNib
{
    
}

//- (id)initViewDefaultNibForSelection:(MTProducto*)selProducto
//{
//    
//}

- (IBAction)btnAceptar:(id)sender
{
    
}

- (IBAction)btnCancelar:(id)sender
{
    
}

- (IBAction)tapMotivo:(id)sender
{
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
