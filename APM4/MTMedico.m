//
//  Medico.m
//  APM4
//
//  Created by Ezequiel Manacorda on 2/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTMedico.h"
#import "Horario.h"
#import "Especialidad.h"
#import "ObraSocial.h"
#import "Domicilio.h"
#import "Telefono.h"
#import "Potencial.h"

#import "Novedad.h"
#import "EstadoVisita.h"

#import "GlobalDefines.h"

#import "Config.h"
#import "ConfigGlobal.h"

#import "DBExtended.h"
#import "NSDateExtended.h"
#import "NSStringExtended.h"



/// DB table column position into the arguments array.
#define valueNODO           0
#define valueID             1
#define valueAPELLIDO       2
#define valueNOMBRE         3
#define valueSEXO           4
#define valueNACIMIENTO     5
#define valueCUIT           6
#define valueMATRICULANAC   7
#define valueMATRICULAPROV  8
#define valueEGRESO         9
#define valueEMAIL          10
#define valueCODINTERNO     11
#define valueASIGNACION     12
#define valueACTIVIDAD      13
#define valueTRATAMIENTO    14
#define valueVISITASPROG    15
#define valueVISITASREAL    16
#define valueESTADO         17
#define valueFECHABAJA      18
#define valueMOTIVOBAJA     19
#define valueIDPOOL         20
#define valueFECHAALTA      21
#define valueOBJETIVOS      22
#define valueMARCA          23
#define valueA              24
#define valueB              25
#define valueM              26
#define valueCP             27
#define valueLOCALIDAD      28
#define valueCALLE          29
#define valueALTURA         30
#define valuePISO           31
#define valueDPTO           32
#define valueESPECIALIDAD1  33
#define valueESPECIALIDAD2  34
#define valueDESDE          35
#define valueHASTA          36
#define valueCOMENTARIOS    37
#define valueLU             38
#define valueMA             39
#define valueMI             40
#define valueJU             41
#define valueVI             42
#define valueLUGAR          43
#define valueDESCESTADO     44
#define valueDESCABM        45
#define valuePOTENCIAL      46
#define valueDESCPOTENCIAL  47
#define valueDESCTRATAMIENTO 48
#define valueDESCMARCADO    49
#define valuePENALTA        50
#define valuePENBAJA        51

#define SettingForKey(key, stringdefault) ([[Config shareInstance] isValidKey:key] ? [[Config shareInstance] find:key] : stringdefault)

@implementation MTMedico

#pragma mark -
#pragma mark Private methods

- (id)init
{
    self = [super init];
    // Chequeo si el objeto 
    if (self)
    {
        self.modo = kMTEANone;
        
        self.apellido = @"";
        self.nombre = @"";
        self.actividad = @"";       // M/T
        self.potencial = nil;
        self.especialidades = nil;
        self.domicilios = nil;
        self.obrasSociales = nil;
        self.sexo = @"";            //M/F
        self.tratamiento = nil;
        self.fechaNacimiento = nil;
        self.anioEgreso = @"";
        self.CUIT = @"";
        self.email = @"";
        self.codigoInterno = @"";
        self.revisita = @"0";
        self.matriculaNacional = @"";
        self.matriculaProvincial = @"";
        self.lugar = @"";
        self.descEstado = @"NO VISTO";
        self.descABM = @"...";
        self.descPotencial = @"";
        self.descTratamiento = @"";
        self.descMarcado = @"";
        
        self.fechaBaja = nil;
        self.fechaAlta = nil;
        self.objetivos = @"";
        self.marca = @"";
        self.cp = @"";
        self.dpto = @"";
        self.especialidad1 = @"";
        self.especialidad2 = @"";
        self.comentarios = @"";
        self.idMotivoBaja = 0;
        self.asignacion = 0;
        self.idPool = 0;
        self.A = NO;
        self.B = NO;
        self.M = NO;
        self.piso = 0;
        self.desde = 0;
        self.hasta = 0;
        self.lu = NO;
        self.ma = NO;
        self.mi = NO;
        self.ju = NO;
        self.vi = NO;
        self.penAlta = NO;
        self.penBaja = NO;
        
        self.recID = @"0";
        self.idEstado = 0;
        self.visitasRealizadas = 0;
        self.visitasProgramadas = 0;
    }
    
    return self;
}




- (NSArray*)dbGetArguments
{ // Esta función obtiene y devuelve los parámetros necesarios para el query de Add, Update y Delete
    // con el formato para realizarlo en la base de datos local.
    
    NSMutableArray* result = [[NSMutableArray alloc] init];
    NSString *strNULL = @"";
    
//#define valueNODO           0
    [result insertObject:[[Config shareInstance] find:NODO] atIndex:valueNODO];
    
//#define valueID             1
    [result insertObject:self.recID atIndex:valueID];
    
//#define valueAPELLIDO       2
    [result insertObject:self.apellido atIndex:valueAPELLIDO];
    
//#define valueNOMBRE         3
    [result insertObject:self.nombre atIndex:valueNOMBRE];
    
//#define valueSEXO           4
    [result insertObject:self.sexo atIndex:valueSEXO];
    
//#define valueNACIMIENTO     5
    if (self.fechaNacimiento)
        [result insertObject:[self.fechaNacimiento datetimeForDBAsString] atIndex:valueNACIMIENTO];
    else
        [result insertObject:strNULL atIndex:valueNACIMIENTO];
    
//#define valueCUIT           6
    if ((self.CUIT) && ([self.CUIT length] > 0))
        [result insertObject:self.CUIT atIndex:valueCUIT];
    else
        [result insertObject:strNULL atIndex:valueCUIT];
    
//#define valueMATRICULANAC   7
    if ((self.matriculaNacional) && ([self.matriculaNacional length] > 0))
        [result insertObject:self.matriculaNacional atIndex:valueMATRICULANAC];
    else
        [result insertObject:@"0" atIndex:valueMATRICULANAC];
    
//#define valueMATRICULAPROV  8
    if ((self.matriculaProvincial) && ([self.matriculaProvincial length] > 0))
        [result insertObject:self.matriculaProvincial atIndex:valueMATRICULAPROV];
    else
        [result insertObject:@"0" atIndex:valueMATRICULAPROV];
    
//#define valueEGRESO         9
    if ((self.anioEgreso) && ([self.anioEgreso length] > 0))
        [result insertObject:self.anioEgreso atIndex:valueEGRESO];
    else
        [result insertObject:strNULL atIndex:valueEGRESO];

//#define valueEMAIL          10
    if ((self.email) && ([self.email length] > 0))
        [result insertObject:self.email atIndex:valueEMAIL];
    else
        [result insertObject:strNULL atIndex:valueEMAIL];
    
//#define valueCODINTERNO     11
    if ((self.codigoInterno) && ([self.codigoInterno length] > 0))
        [result insertObject:self.codigoInterno atIndex:valueCODINTERNO];
    else
        [result insertObject:strNULL atIndex:valueCODINTERNO];
    
//#define valueASIGNACION     12
    [result insertObject:[NSString stringWithFormat:@"%d", self.asignacion]
                 atIndex:valueASIGNACION];
    
//#define valueACTIVIDAD      13
    [result insertObject:self.actividad atIndex:valueACTIVIDAD];
    
//#define valueTRATAMIENTO    14
    [result insertObject:[NSString stringWithFormat:@"%d", self.tratamiento.recID]
                 atIndex:valueTRATAMIENTO];
    
//#define valueVISITASPROG    15
    [result insertObject:[NSString stringWithFormat:@"%d", self.visitasProgramadas]
                 atIndex:valueVISITASPROG];
    
//#define valueVISITASREAL    16
    [result insertObject:[NSString stringWithFormat:@"%d", self.visitasRealizadas]
                 atIndex:valueVISITASREAL];
    
//#define valueESTADO         17
    [result insertObject:[NSString stringWithFormat:@"%d", self.idEstado] atIndex:valueESTADO];
    
//#define valueFECHABAJA      18
//#define valueMOTIVOBAJA     19
    if ((self.fechaBaja) && ([self.fechaBaja timeIntervalSince1970] > 0))
    {
        [result insertObject:[self.fechaBaja datetimeForDBAsString]
                     atIndex:valueFECHABAJA];
        [result insertObject:[NSString stringWithFormat:@"%d", self.idMotivoBaja]
                     atIndex:valueMOTIVOBAJA];
    } else
    {
        [result insertObject:strNULL atIndex:valueFECHABAJA];
        [result insertObject:strNULL atIndex:valueMOTIVOBAJA];
    }
    
//#define valueIDPOOL         20
    if (self.idPool > 0)
        [result insertObject:[NSString stringWithFormat:@"%d", self.idPool]
                     atIndex:valueIDPOOL];
    else
        [result insertObject:strNULL atIndex:valueIDPOOL];
    
//#define valueFECHAALTA      21
    if (self.fechaAlta)
        [result insertObject:[self.fechaAlta datetimeForDBAsString]
                     atIndex:valueFECHAALTA];
    else
        [result insertObject:strNULL atIndex:valueFECHAALTA];
    
//#define valueOBJETIVOS      22
    [result insertObject:self.objetivos atIndex:valueOBJETIVOS];
    
//#define valueMARCA          23
    [result insertObject:self.marca atIndex:valueMARCA];
    
//#define valueA              24
    [result insertObject:self.A?@"1":@"0" atIndex:valueA];
    
//#define valueB              25
    [result insertObject:self.B?@"1":@"0" atIndex:valueB];
    
//#define valueM              26
    [result insertObject:self.M?@"1":@"0" atIndex:valueM];
    
//#define valueCP             27
    [result insertObject:self.cp atIndex:valueCP];
    
//#define valueLOCALIDAD      28
    if (self.localidad)
        [result insertObject:self.localidad atIndex:valueLOCALIDAD];
    else
        [result insertObject:strNULL atIndex:valueLOCALIDAD];
    
//#define valueCALLE          29
    [result insertObject:self.direccion atIndex:valueCALLE];
    
//#define valueALTURA         30
    [result insertObject:[NSString stringWithFormat:@"%d", self.altura] atIndex:valueALTURA];
    
//#define valuePISO           31
    if (self.piso > 0)
        [result insertObject:[NSString stringWithFormat:@"%d", self.piso] atIndex:valuePISO];
    else
        [result insertObject:@"0" atIndex:valuePISO];
    
//#define valueDPTO           32
    if ((self.dpto) &&([self.dpto length] > 0))
        [result insertObject:self.dpto atIndex:valueDPTO];
    else
        [result insertObject:strNULL atIndex:valueDPTO];
    
//#define valueESPECIALIDAD1  33
    [result insertObject:self.especialidad1 atIndex:valueESPECIALIDAD1];
    
//#define valueESPECIALIDAD2  34
    if ((self.especialidad2) && ([self.especialidad2 length] > 0))
        [result insertObject:self.especialidad2 atIndex:valueESPECIALIDAD2];
    else
        [result insertObject:strNULL atIndex:valueESPECIALIDAD2];
    
//#define valueDESDE          35
    [result insertObject:[NSString stringWithFormat:@"%0000d", self.desde]
                 atIndex:valueDESDE];
    
//#define valueHASTA          36
    [result insertObject:[NSString stringWithFormat:@"%0000d", self.hasta]
                 atIndex:valueHASTA];
    
//#define valueCOMENTARIOS    37
    if ((self.comentarios) && ([self.comentarios length] > 0))
        [result insertObject:self.comentarios atIndex:valueCOMENTARIOS];
    else
        [result insertObject:strNULL atIndex:valueCOMENTARIOS];
    
//#define valueLU             38
    [result insertObject:self.lu?@"1":@"0" atIndex:valueLU];
    
//#define valueMA             39
    [result insertObject:self.ma?@"1":@"0" atIndex:valueMA];
    
//#define valueMI             40
    [result insertObject:self.mi?@"1":@"0" atIndex:valueMI];
    
//#define valueJU             41
    [result insertObject:self.ju?@"1":@"0" atIndex:valueJU];
    
//#define valueVI             42
    [result insertObject:self.vi?@"1":@"0" atIndex:valueVI];
    
//#define valueLUGAR          43
    [result insertObject:self.lugar atIndex:valueLUGAR];
    
//#define valueDESCESTADO     44
    [result insertObject:self.descEstado atIndex:valueDESCESTADO];
    
//#define valueDESCABM        45
    if ((self.descABM) && ([self.descABM length] > 0))
        [result insertObject:self.descABM atIndex:valueDESCABM];
    else
        [result insertObject:@"..." atIndex:valueDESCABM];
    
//#define valuePOTENCIAL      46
    [result insertObject:[NSString stringWithFormat:@"%d", self.potencial.recID] atIndex:valuePOTENCIAL];
    
//#define valueDESCPOTENCIAL  47
    [result insertObject:self.potencial.descripcion atIndex:valueDESCPOTENCIAL];
    
//#define valueDESCTRATAMIENTO 48
    [result insertObject:self.tratamiento.descripcion atIndex:valueDESCTRATAMIENTO];
    
//#define valueDESCMARCADO    49
    if ((self.descMarcado) && ([self.descMarcado length] > 0))
        [result insertObject:self.descMarcado atIndex:valueDESCMARCADO];
    else
        [result insertObject:strNULL atIndex:valueDESCMARCADO];
    
//#define valuePENALTA        50
    [result insertObject:self.penAlta?@"1":@"0" atIndex:valuePENALTA];
    
//#define valuePENBAJA        51
    [result insertObject:self.penBaja?@"1":@"0" atIndex:valuePENBAJA];

    return result;
}

- (NSString *)getNovedadParameters
{ // Esta función obtiene y devuelve los parámetros necesarios para el query de LOG_NOVEDADES
    // con el formato para realizarlo en dicha tabla.
    
    NSString *result = @"";
    NSArray *arguments = nil;
    
    arguments = [self dbGetArguments];
    
    result = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@"
                                        "|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@"
                                        "|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@"
                                        "|%@",
              [arguments objectAtIndex:valueNODO],
              [arguments objectAtIndex:valueID],
              [arguments objectAtIndex:valueAPELLIDO],
              [arguments objectAtIndex:valueNOMBRE],
              [arguments objectAtIndex:valueDESCMARCADO],
              [arguments objectAtIndex:valueSEXO],
              [NSString string:[self.fechaNacimiento datetimeForNovedad] ifNull:@""],
              [arguments objectAtIndex:valueCUIT],
              [arguments objectAtIndex:valueMATRICULANAC],
              [arguments objectAtIndex:valueMATRICULAPROV],
              [arguments objectAtIndex:valueEGRESO],
              [arguments objectAtIndex:valueEMAIL],
              [arguments objectAtIndex:valueCODINTERNO],
              [arguments objectAtIndex:valueASIGNACION],
              [arguments objectAtIndex:valueACTIVIDAD],
              [arguments objectAtIndex:valuePOTENCIAL],
              [arguments objectAtIndex:valueTRATAMIENTO],
              [arguments objectAtIndex:valueVISITASPROG],
              [arguments objectAtIndex:valueVISITASREAL],
              [arguments objectAtIndex:valueESTADO],
              [NSString string:[self.fechaBaja datetimeForNovedad] ifNull:@""],
              [arguments objectAtIndex:valueMOTIVOBAJA],
              [arguments objectAtIndex:valueIDPOOL],
              [NSString string:[self.fechaAlta datetimeForNovedad] ifNull:@""],
              [arguments objectAtIndex:valueOBJETIVOS],
              [arguments objectAtIndex:valueMARCA],
              self.A?@"S":@"N",
              self.B?@"S":@"N",
              self.M?@"S":@"N",
              self.penAlta?@"S":@"N",
              self.penBaja?@"S":@"N"
              ];
    
    return result;
}

#pragma mark -
#pragma mark Public Medicos methods


- (MTMedico*)copyWithZone:(NSZone*)zone
{
    MTMedico* newCopy;
    
    newCopy = [[MTMedico alloc] init];
    
    // MTEntidad
    newCopy.recID      = [NSString string:self.recID       ifNull:@""];
    newCopy.descripcion= [NSString string:self.descripcion ifNull:@""];
    newCopy.direccion  = [NSString string:self.direccion   ifNull:@""];
    newCopy.altura     = self.altura;
    newCopy.localidad  = [NSString string:self.localidad   ifNull:@""];
    
    return newCopy;
}

-(NSString*)isOkForSave
{  /*
     Esta rutina chequea los datos obligatorios y que los ingresados sean válidos.
     
     Los datos obligatorios a ingresar son : 
        √ Domicilio,
        X  horario,
        √ Apellido,
        √ Nombre,
        √ Una de las 2 matriculas,
        √ actividad,
        √ sexo,
        √ potencial,
        X año de egreso
    */
    
    NSString *result = nil; // Resultado por defecto, esto quiere decir que no hubo error.
    
    Domicilio *medDomicilio = nil;
    
    // Busca el domicilio de Visita en lugar del primer domicilio y lo posiciona como primero.
    if ([self.domicilios count] > 0)
    {
        medDomicilio = [self.domicilios objectAtIndex:0];
        NSInteger cantDomicilios = [self.domicilios count];
        if (medDomicilio.utilidad.idUtilidad != 1)
        {
            if (cantDomicilios > 1)
            {
                for (NSInteger cDomicilio = 0; cDomicilio < cantDomicilios; cDomicilio++)
                {
                    Domicilio *d = [self.domicilios objectAtIndex:cDomicilio];
                    if (d.utilidad.idUtilidad == 1)
                    {
                        [self.domicilios removeObjectAtIndex:cDomicilio];
                        [self.domicilios insertObject:d atIndex:0];
                    }
                }
            }
        }
    }
    
#pragma mark    Nuevo ID
    // Si es un Alta, busco el nuevo ID
    if (self.modo == kMTEAAdd)
    {
        //self.recID = [NSString intToStr:[self cfgUltimoRead]];
        self.recID = [NSString intToStr:[self getNewID]];
    }
    
    
#pragma mark    Apellido y Nombre
    /* Apellido
     Mensaje : Debe ingresar el apellido del médico.*/
    if ((!self.apellido) || ([self.apellido length] < 1))
        return @"Debe ingresar el apellido del médico";
     
    /* Nombre
     Mensaje : Debe ingresar el nombre del médico.*/
    if ((!self.nombre) || ([self.nombre length] < 1))
        return @"Debe ingresar el nombre del médico";
    
#pragma mark    Actividad, Potencial y Especialidad
    /* Actividad
     MENSAJE :  Debe seleccionar la actividad.*/
    if ([self.actividad length] < 1)
        return @"Debe seleccionar la actividad del médico";
    
    /* Potencial
     MENSAJE : Debe seleccionar el potencial del médico.*/
    if ((!self.potencial) || (self.potencial.recID == 0))
        return @"Debe seleccionar el potencial del médico";
    
    /* Especialidad
     MENSAJE: Debe ingresar al menos una especialidad.*/
    if ([self.especialidades count] < 1)
        return @"Debe ingresar al menos una especialidad del médico";
    
#pragma mark    Domicilio
    /* Domicilio
     Mensaje : Debe ingresar un domicilio de visita. */
    if ([self.domicilios count] < 1)
        return @"Debe ingresar un domicilio de visita";
    
#pragma mark    Sexo, Tratamiento y Obra Social
    /* Sexo
     Debe seleccionar el sexo del médico.*/
    if ((!self.sexo) || ([self.sexo length] < 1))
        return @"Debe seleccionar el sexo del médico";
    
    /* Tratamiento
     MENSAJE : Debe seleccionar el tratamiento del médico*/
    if ((!self.tratamiento) || (self.tratamiento.recID == 0))
        return @"Debe seleccionar el tratamiento del médico";
    
    /* Obra Social
     Si CRT_OBRASOCIAL = ‘S’
     Si no cargo ninguna obra social para el medico
     MENSAJE : Debe ingresar al menos una obra social. */
    if ([[ConfigGlobal shareInstance] find:CRT_OBRASOCIAL])
        if ([self.obrasSociales count] < 1)
            return [[MTRegionalDictionary sharedInstance] valueForText:@"Debe ingresar al menos una obra social"];
    

#pragma mark    Matriculas    
    // Matricula.
    
    // Chequeo si se trata de Chile, que en lugar de la matrícula se utiliza el RUT (CUIT).
    if ([[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_CL"])
    {
        // Copio el RUT menus un caracter (dígito verificador) a la amtricula nacional.
        self.matriculaNacional   = @"0";
        self.matriculaProvincial = @"0";
        if ([self.CUIT length] > 1)
        {
            self.matriculaNacional = [self.CUIT substringToIndex:[self.CUIT length]-1];
        } else
        {
            self.CUIT = @"";
        }
    }
    
    self.matriculaNacional = [NSString intToStr:[self.matriculaNacional integerValue]];
    self.matriculaProvincial = [NSString intToStr:[self.matriculaProvincial integerValue]];
    
    if (([self.matriculaNacional integerValue] == 0) && ([self.matriculaProvincial integerValue] == 0))
    {
        if ([[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_CL"])
            self.CUIT = @"";
        
        return [[MTRegionalDictionary sharedInstance] valueForText:@"Debe ingresar al menos una matrícula del médico"];
    }
    
    NSInteger matriculaNacional     = 0;
    NSInteger matriculaProvincial   = 0;
    
     /*Se debe verificar que la/s matricula/s ingresada/s no exista/n :
     
     Para buscar matricula nacional :
     "select matriculanacional from med_medicos where id <> %d and matriculanacional = %d
     */
    NSString *sql = @"select matriculanacional from med_medicos where id <> %@ and matriculanacional = %d";
    NSString *query = [NSString stringWithFormat:sql,
                            [NSString string:self.recID ifNull:@"0"],
                            [self.matriculaNacional integerValue]];
    
    if (!([self.matriculaNacional integerValue] == 0))
    {
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- mat nacional : %@", query);
#endif
        sqlite3_stmt *statement = [[DB getInstance] prepare:query];
        if ( statement ) {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                matriculaNacional = sqlite3_column_int(statement, 0);
            }
            
        } else {
            NSAssert(0, @" - No se pudo ejecutar: %@", query);
        }
        sqlite3_finalize(statement);
    }
    
    
    /*
     Para buscar matricula provincial :
     
     select matriculaprovincial from med_medicos med
     left join med_domicilios dom
     on med.nodo = dom.nodo and med.id = dom.medico and dom.utilidad = 1
     where med.id <> %d and matriculaprovincial = %d and provincia = %d
     */
    
    if (!([self.matriculaProvincial integerValue] == 0))
    {
        Domicilio *d = [self.domicilios objectAtIndex:0];
        sql = @"select matriculaprovincial from med_medicos med "
        "left join med_domicilios dom "
        "on med.nodo = dom.nodo and med.id = dom.medico and dom.utilidad = 1 "
        "where med.id <> %@ and matriculaprovincial = %d and provincia = %d";
        query = [NSString stringWithFormat:sql,
                 [NSString string:self.recID ifNull:@"0"],
                 [self.matriculaProvincial integerValue],
                 d.provincia.idProvincia];
  
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- mat provincial : %@", query);
#endif
        
        sqlite3_stmt *statement2 = [[DB getInstance] prepare:query];
        if ( statement2 ) {
            while (sqlite3_step(statement2) == SQLITE_ROW)
            {
                matriculaProvincial = sqlite3_column_int(statement2, 0);
            }
            
        } else {
            NSAssert(0, @" - No se pudo ejecutar: %@", query);
        }
        sqlite3_finalize(statement2);
    }
        
    
    /* Si matricula nacional  es diferente de cero y existe
     MENSAJE : Ya existe el N° de Matrícula Nacional en otro médico.*/
    if ((matriculaNacional != 0) && ([self.matriculaNacional integerValue] != 0))
        return [[MTRegionalDictionary sharedInstance] valueForText:@"Ya existe el N° de Matrícula Nacional en otro médico"];
        
    
    /* Si matricula provincial es diferente de cero y  existe :
     MENSAJE : Ya existe el N° de Matrícula Provincial en otro médico.*/
    if ((matriculaProvincial != 0) && ([self.matriculaProvincial integerValue] != 0))
        return @"Ya existe el N° de Matrícula Provincial en otro médico";


    
    // TODO: Phoenix no utiliza la fecha de egreso, por lo que queda pendiente.
    
    /* Consultar el valor de la columna “VALOR” en la cfg_global por campo propiedad “CRT_EGRESO”.
     Si CRT_EGRESO = ‘S’
     Si anio egreso = 0 o a vacio
     MENSAJE : Debe ingresar el año de egreso.
     Sino
     Puede ingresar 0 o vacio.
     
     Si ingresa un valor
     Si Año de egreso es menor a 1900 o mayor o igual al año actual mostrar mensaje MENSAJE : Año de egreso inválido.
     
     Consultar el valor de la columna “VALOR” en la cfg_global por campo propiedad “CRT_OBRASOCIAL”.
    */
    
    
    
#pragma mark    Completa el resto de los datos
    self.visitasProgramadas = 1 + [self.revisita integerValue];

    self.fechaBaja    = nil;
    self.idMotivoBaja = 0;
    
    
    
    self.cp = medDomicilio.codigoPostal;
    self.localidad = medDomicilio.localidad.descripcion;
    self.direccion = medDomicilio.calle.descripcion;
    self.altura    = medDomicilio.numero;
    self.piso      = medDomicilio.piso;
    self.dpto      = medDomicilio.depto;
    self.lugar     = medDomicilio.tipo.descripcion;
       
    
    Horario *medHorario = [medDomicilio.horarios objectAtIndex:0];
    self.desde = [[[NSDate stringToTime:medHorario.horaDesde]
                  formattedStringUsingFormat:@"HHmm"] integerValue];
    self.hasta = [[[NSDate stringToTime:medHorario.horaHasta]
                  formattedStringUsingFormat:@"HHmm"] integerValue];
    self.comentarios = medHorario.comentarios;
    
    self.lu = medHorario.lunes;
    self.ma = medHorario.martes;
    self.mi = medHorario.miercoles;
    self.ju = medHorario.jueves;
    self.vi = medHorario.viernes;
    
    Especialidad *medEspecialidad = [self.especialidades objectAtIndex:0];
    self.especialidad1 = medEspecialidad.sigla;
    self.especialidad2 = @"";
    if ([self.especialidades count] > 1)
    {
        Especialidad *medEspecialidad = [self.especialidades objectAtIndex:1];
        self.especialidad2 = medEspecialidad.sigla;
    }
    
    self.descPotencial = self.potencial.descripcion;
    
    self.descTratamiento = self.tratamiento.descripcion;
    
    
    self.penBaja = NO;
    
    
    // Completo los datos restantes para el Alta o Modificación.
    if (self.modo == kMTEAAdd)
    {  // Completo los datos para el ALTA.
        self.descMarcado = @"";
        
        
        self.codigoInterno = @"";
        self.asignacion = 1;

        self.visitasRealizadas = 0;

        self.idEstado = 2;
        self.descEstado = @"NO VISTO";

        self.fechaAlta = [NSDate dateWithoutTime];
        
        self.objetivos = @"";
        self.marca     = @"0";
        
        self.A = YES;
        self.B = NO;
        self.M = NO;
        self.descABM = @"Alta";
        
        self.idPool = 0;
        
        // Chequeo si utiliza Autorización para marcarlo como pendiente de alta.
        if ([[ConfigGlobal shareInstance] find:USAAUTORIZACION])
        {
            self.penAlta = YES;
            self.descABM = @"Pend. Alta";
        }

    } else
    {  // Completo los datos para el MODIFICACION.

        // Estos valores quedan como estaban al leer el médico de la DB.
//        self.codigoInterno = ;
//        
//        self.idEstado = ;
//        self.descEstado = ;
//        
//        self.visitasRealizadas = ;
//
//        self.fechaAlta = ;
//        
//        self.objetivos = ;
//        self.marca     = ;
//        
//        self.A = ;
//        self.B = ;
        
        if ((!(self.penAlta)) && (!self.A))
        {
            self.descABM = @"Modificacion";
            self.M = YES;
        }

    }
    
    return result;
}



- (void)performEditAction: (MTEditActionsType)accion
{
    switch (self.modo)
    {
        case kMTEAAdd:                      // Alta.
            [self saveAllData];
            break;
            
        case kMTEAChange:                   // Modificación.
            
        default:
            [self updateAllData];
            break;
    }
}


-(void)saveAllData
{

#pragma mark    Insert MED_MEDICOS
    
    /*
     Se deberá:
     * Insertar el médico med_medicos
     * Insertar las especialidades med_especialidades
     * Insertar las obras sociales med_obrassociales
     * Insertar los domicilios med_domicilios
     * Por cada domicilio:
	 *    Insertar horarios med_horarios
	 *    Insertar teléfonos med_telefonos
     
     *en todos los insert de la log_novedades hay que grabar en el campo secuencia el max(secuencia) + 1
     
     * Si es un ALTA, se deberá:
     * Insertar el médico med_medicos
     * Insertar las especialidades med_especialidades
     * Insertar las obras sociales med_obrassociales
     * Insertar los domicilios med_domicilios
     * Por cada domicilio:
     *    Insertar horarios med_horarios
     *    Insertar teléfonos med_telefonos
     
     */
    
    
     /*
      Insert a la MED_MEDICOS :
     */
    NSString *sqlMedicos = @"INSERT INTO med_medicos "
    "(nodo, id, apellido, nombre, sexo, "
    "nacimiento, cuit, matriculaNacional, matriculaProvincial, egreso, "
    "email, codigoInterno, asignacion, actividad, tratamiento, "
    "visitasProgramadas, visitasRealizadas, estado, fechaBaja, motivoBaja, "
    "idPool, fechaAlta, objetivos, marca, A, "
    "B, M, cp, localidad, calle, "
    "altura, piso, dpto, especialidad1, especialidad2, "
    "desde, hasta, comentarios, lu, ma, "
    "mi, ju, vi, lugar, descEstado, "
    "descABM, potencial, descPotencial, descTratamiento, descMarcado, "
    "penAlta, penBaja) "
    "VALUES(%@, %@, %@, %@, %@, "
    "%@, %@, %@, %@, %@, "
    "%@, %@, %@, %@, %@, "
    "%@, %@, %@, %@, %@, "
    "%@, %@, %@, %@, %@, "
    "%@, %@, %@, %@, %@, "
    "%@, %@, %@, %@, %@, "
    "%@, %@, %@, %@, %@, "
    "%@, %@, %@, %@, %@, "
    "%@, %@, %@, %@, %@, "
    "%@, %@)";
    
    
    NSArray *argMedico = [self dbGetArguments];
    
    NSString *queryMedico = [NSString stringWithFormat:sqlMedicos,
                             [argMedico objectAtIndex:valueNODO           ],
                             [argMedico objectAtIndex:valueID             ],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueAPELLIDO       ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueNOMBRE         ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueSEXO           ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueNACIMIENTO     ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueCUIT           ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueMATRICULANAC   ] ifNull:@"0"],
                             [NSString string:[argMedico objectAtIndex:valueMATRICULAPROV  ] ifNull:@"0"],
                             [NSString string:[argMedico objectAtIndex:valueEGRESO         ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueEMAIL          ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueCODINTERNO     ] ifNull:@"''"],
                             [NSString string:[argMedico objectAtIndex:valueASIGNACION     ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueACTIVIDAD      ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueTRATAMIENTO    ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueVISITASPROG    ] ifNull:@"0"],
                             [NSString string:[argMedico objectAtIndex:valueVISITASREAL    ] ifNull:@"0"],
                             [NSString string:[argMedico objectAtIndex:valueESTADO         ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueFECHABAJA      ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueMOTIVOBAJA     ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueIDPOOL         ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueFECHAALTA      ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueOBJETIVOS      ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueMARCA          ] ifNull:@"NULL"],
                             [argMedico objectAtIndex:valueA              ],
                             [argMedico objectAtIndex:valueB              ],
                             [argMedico objectAtIndex:valueM              ],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueCP             ] ifNull:@"0"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueLOCALIDAD      ] ifNull:@"''"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueCALLE          ] ifNull:@"''"],
                             [NSString string:[argMedico objectAtIndex:valueALTURA         ] ifNull:@"0"],
                             [NSString string:[argMedico objectAtIndex:valuePISO           ] ifNull:@"0"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDPTO           ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueESPECIALIDAD1  ] ifNull:@"''"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueESPECIALIDAD2  ] ifNull:@"''"],
                             [NSString string:[argMedico objectAtIndex:valueDESDE          ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueHASTA          ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueCOMENTARIOS    ] ifNull:@"NULL"],
                             [argMedico objectAtIndex:valueLU             ],
                             [argMedico objectAtIndex:valueMA             ],
                             [argMedico objectAtIndex:valueMI             ],
                             [argMedico objectAtIndex:valueJU             ],
                             [argMedico objectAtIndex:valueVI             ],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueLUGAR          ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDESCESTADO     ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDESCABM        ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valuePOTENCIAL      ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDESCPOTENCIAL  ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDESCTRATAMIENTO] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDESCMARCADO    ] ifNull:@"NULL"],
                             [argMedico objectAtIndex:valuePENALTA        ],
                             [argMedico objectAtIndex:valuePENBAJA        ]];
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- query Alta Medico: %@", queryMedico);
#endif
    [[DB getInstance] excecuteSQL:queryMedico];
    
     /*
     Grabar novedad en la log_novedades para la MED_MEDICOS
     Tabla : MED_MEDICOS
     Tipo : ‘I’
     TranId : 0
     Transmitido : 0
     */
    NSString *novedadMedico = [self getNovedadParameters];
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- Novedad Alta Medico: %@", novedadMedico);
#endif
    [Novedad insert:@"med_medicos" novedad:novedadMedico tipo:kInsert];
    
    // Guardo el nuevo ID en CFG_ULTIMO
    [self cfgUltimoWrite:[self.recID integerValue]];
    
    
    
#pragma mark    Insert GST_GRUPOMEDICOS
     /*
     Insert a la GST_GRUPOMEDICOS:
     
     Grabar registro en la gst_grupoMedicos :
     
     "nodo" = nodo variable global
     "grupo" = 1.
     "medico" = id de medico
    */
    sqlMedicos = @"INSERT INTO gst_grupomedicos (nodo, grupo, medico) VALUES(%@,1,%@);";
    queryMedico = [NSString stringWithFormat:sqlMedicos,
                   [argMedico objectAtIndex:valueNODO           ],
                   [argMedico objectAtIndex:valueID             ]
                   ];
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- query Alta GrupoMedico: %@", queryMedico);
#endif
    [[DB getInstance] excecuteSQL:queryMedico];

    
    /*
     Grabar novedad en la log_novedades para la GST_GRUPOMEDICOS
     Tabla : GST_GRUPOMEDICOS
     Tipo : ‘I’
     Registro : Nodo|grupo|medico (es decir los valores grabados separados por pipes
     TranId : 0
     Transmitido : 0
     
     Si estoy trabajando en otro grupo que no sea el 1 que es la cartera de médicos se graba también otro registro en la gst_grupomedicos.
     (Breve reseña del uso de selección de grupo.
     Archivo – selección de grupo : Se muestra los médicos de ese grupo, lease que la opción de cartera de médicos es como un acceso directo seleccionando el grupo 1 )
     
     Si idGrupo > 1 entonces
     
     "nodo" = nodo variable global
     "grupo" = idGrupo
     "medico" = id de medico
     
     
     Grabar novedad en la log_novedades para la GST_GRUPOMEDICOS
     Tabla : GST_GRUPOMEDICOS
     Tipo : ‘I’
     Registro : Nodo|grupo|medico (es decir los valores grabados separados por pipes
     TranId : 0
     Transmitido : 0
     
     Fin si
     */
    novedadMedico = [NSString stringWithFormat:@"%@|1|%@",
                     [argMedico objectAtIndex:valueNODO           ],
                     [argMedico objectAtIndex:valueID             ]
                     ];
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- Novedad Alta GrupoMedico: %@", novedadMedico);
#endif
    [Novedad insert:@"gst_grupomedicos" novedad:novedadMedico tipo:kInsert];


    // Inserto el resto de las tablas.
    [self saveAllTheOtherShit:argMedico];
    
    
}

-(void)updateAllData
{
    
#pragma mark    Update MED_MEDICOS
    
    /*
     
     UPDATE A LA MED_MEDICOS DE TODO EL REGISTRO
     WHERE NODO = NODO VARIABLE GLOBAL  AND ID = ID MEDICO SELECCIONADO
     
     Grabar novedad en la log_novedades para la MED_MEDICOS
     
     Tabla : MED_MEDICOS
     Tipo : ‘U’
     Registro : nodo|id|apellido|nombre|descmarcado|sexo|nacimiento|cuit|matriculanacional|matriculaprovincial|egreso|
                email|codigointerno|asignacion|actividad|potencial|tratamiento|visitasprogramadas|visitasrealizadas|
                estado|fechabaja|motivobaja|idpool|fechaalta|objetivos|marca|A|B|M|penalta|penbaja
     TranId : 0
     Transmitido : 0
     
     
     Lo que hace es borrar los registros y volver a grabar:
     (Conviene traerse el registro de novedad antes de borrar)
     
     Grabar novedad en la log_novedades para la MED_MEDICOS
     */
    
    NSString *sqlMedicos = @"UPDATE med_medicos "
    "SET apellido=%@, nombre=%@, sexo=%@, "
    "nacimiento=%@, cuit=%@, matriculaNacional=%@, matriculaProvincial=%@, egreso=%@, "
    "email=%@, codigoInterno=%@, asignacion=%@, actividad=%@, tratamiento=%@, "
    "visitasProgramadas=%@, visitasRealizadas=%@, estado=%@, fechaBaja=%@, motivoBaja=%@, "
    "idPool=%@, fechaAlta=%@, objetivos=%@, marca=%@, A=%@, "
    "B=%@, M=%@, cp=%@, localidad=%@, calle=%@, "
    "altura=%@, piso=%@, dpto=%@, especialidad1=%@, especialidad2=%@, "
    "desde=%@, hasta=%@, comentarios=%@, lu=%@, ma=%@, "
    "mi=%@, ju=%@, vi=%@, lugar=%@, descEstado=%@, "
    "descABM=%@, potencial=%@, descPotencial=%@, descTratamiento=%@, descMarcado=%@, "
    "penAlta=%@, penBaja=%@ "
     "WHERE (nodo=%@) AND (id=%@);";
    
    
    NSArray *argMedico = [self dbGetArguments];
    
    NSString *queryMedico = [NSString stringWithFormat:sqlMedicos,
                             [NSString stringQuoted:[argMedico objectAtIndex:valueAPELLIDO       ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueNOMBRE         ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueSEXO           ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueNACIMIENTO     ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueCUIT           ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueMATRICULANAC   ] ifNull:@"0"],
                             [NSString string:[argMedico objectAtIndex:valueMATRICULAPROV  ] ifNull:@"0"],
                             [NSString string:[argMedico objectAtIndex:valueEGRESO         ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueEMAIL          ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueCODINTERNO     ] ifNull:@"''"],
                             [NSString string:[argMedico objectAtIndex:valueASIGNACION     ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueACTIVIDAD      ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueTRATAMIENTO    ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueVISITASPROG    ] ifNull:@"0"],
                             [NSString string:[argMedico objectAtIndex:valueVISITASREAL    ] ifNull:@"0"],
                             [NSString string:[argMedico objectAtIndex:valueESTADO         ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueFECHABAJA      ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueMOTIVOBAJA     ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueIDPOOL         ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueFECHAALTA      ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueOBJETIVOS      ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueMARCA          ] ifNull:@"NULL"],
                             [argMedico objectAtIndex:valueA              ],
                             [argMedico objectAtIndex:valueB              ],
                             [argMedico objectAtIndex:valueM              ],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueCP             ] ifNull:@"0"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueLOCALIDAD      ] ifNull:@"''"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueCALLE          ] ifNull:@"''"],
                             [NSString string:[argMedico objectAtIndex:valueALTURA         ] ifNull:@"0"],
                             [NSString string:[argMedico objectAtIndex:valuePISO           ] ifNull:@"0"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDPTO           ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueESPECIALIDAD1  ] ifNull:@"''"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueESPECIALIDAD2  ] ifNull:@"''"],
                             [NSString string:[argMedico objectAtIndex:valueDESDE          ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valueHASTA          ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueCOMENTARIOS    ] ifNull:@"NULL"],
                             [argMedico objectAtIndex:valueLU             ],
                             [argMedico objectAtIndex:valueMA             ],
                             [argMedico objectAtIndex:valueMI             ],
                             [argMedico objectAtIndex:valueJU             ],
                             [argMedico objectAtIndex:valueVI             ],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueLUGAR          ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDESCESTADO     ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDESCABM        ] ifNull:@"NULL"],
                             [NSString string:[argMedico objectAtIndex:valuePOTENCIAL      ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDESCPOTENCIAL  ] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDESCTRATAMIENTO] ifNull:@"NULL"],
                             [NSString stringQuoted:[argMedico objectAtIndex:valueDESCMARCADO    ] ifNull:@"NULL"],
                             [argMedico objectAtIndex:valuePENALTA        ],
                             [argMedico objectAtIndex:valuePENBAJA        ],
                             [argMedico objectAtIndex:valueNODO           ],
                             [argMedico objectAtIndex:valueID             ]];
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- query med_medicos: %@", queryMedico);
#endif
    [[DB getInstance] excecuteSQL:queryMedico];
    
    
    NSString *novedadMedico = [self getNovedadParameters];
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- Novedad med_medicos U: %@", novedadMedico);
#endif
    [Novedad insert:@"med_medicos" novedad:novedadMedico tipo:kUpdate];
    

    
#pragma mark    Delete MED_ESPECIALIDADES

    /*
     Tabla : MED_ESPECIALIDADES
     DELETE FROM MED_ESPECIALIDADES WHERE NODO = NODO VARIABLE GLOBAL  AND ID = ID MEDICO SELECCIONADO*/
    [Especialidad deleteAllForMedico:[[argMedico objectAtIndex:valueID] integerValue]];
    
    
    
#pragma mark    Delete MED_OBRASSOCIALES
    /*
     Tabla : MED_OBRASSOCIALES
        DELETE FROM MED_OBRASSOCIALES WHERE NODO = NODO VARIABLE GLOBAL  AND MEDICO = ID MEDICO SELECCIONADO*/
    [ObraSocial deleteAllForMedico:[[argMedico objectAtIndex:valueID] integerValue]];
    
    
    
#pragma mark    Delete MED_HORARIOS
    
    /*
    Tabla : MED_HORARIOS
        DELETE FROM MED_HORARIOS WHERE NODO = NODO VARIABLE GLOBAL  AND MEDICO = ID MEDICO SELECCIONADO AND DOMICILIO = ID TIPO DOMICILIO*/
    [Horario deleteAllForMedico:[[argMedico objectAtIndex:valueID] integerValue]];
    
    

#pragma mark    Delete MED_TELEFONOS
    
    /*
    Tabla : MED_TELEFONOS
        DELETE FROM MED_TELEFONOS WHERE NODO = NODO VARIABLE GLOBAL  AND MEDICO = ID MEDICO SELECCIONADO AND DOMICILIO = ID TIPO DOMICILIO*/
    [Telefono deleteAllForMedico:[[argMedico objectAtIndex:valueID] integerValue]];

    
    
#pragma mark    Delete MED_DOMICILIOS
    
    /*
    Tabla : MED_DOMICILIOS
        DELETE FROM MED_DOMICILIOS WHERE NODO = NODO VARIABLE GLOBAL  AND MEDICO = ID MEDICO SELECCIONADO*/
    [self deleteDomiciliosForMedico:[[argMedico objectAtIndex:valueID] integerValue]];
     
    
     /*
     Terminados los delete, se hacen los insert.*/
    [self saveAllTheOtherShit:argMedico];
     
    
}


-(void)saveAllTheOtherShit:(NSArray*)argMedico
{
    // Se crea esta función para no tener que reescribir todo el código de nuevo en el Update.
    
    NSString *sqlMedicos;
    NSString *queryMedico;
    NSString *novedadMedico;
    
    
#pragma mark    Insert MED_ESPECIALIDADES
    
    NSInteger orderCount = 1;
    for (Especialidad *o in self.especialidades)
    {
        /*
         Insert a la MED_ESPECIALIDADES: (tabla relación nodo, medico, especialidad)
         
         Iterar entre las especialidades cargadas
         
         "nodo"   = nodo variable global
         "medico" = id de medico
         "orden"  = Orden en el que se ubico la especialidad
         "especialidad" = idEspecialidad de la especialidad seleccionada obtenido de la mtc_especialidades
         */
        
        sqlMedicos = @"INSERT INTO med_especialidades "
        "(nodo, medico, orden, especialidad) "
        "VALUES(%@,%@,%@,%@);";
        queryMedico = [NSString stringWithFormat:sqlMedicos,
                       [argMedico objectAtIndex:valueNODO           ],
                       [argMedico objectAtIndex:valueID             ],
                       [NSString stringWithFormat:@"%d", orderCount],
                       [NSString stringWithFormat:@"%d", o.recID]
                       ];
        
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- query Alta EspecialidadMedico: %@", queryMedico);
#endif
        [[DB getInstance] excecuteSQL:queryMedico];
        
        /*
         Grabar novedad en la log_novedades para la GST_GRUPOMEDICOS
         Tabla : MED_ESPECIALIDADES
         Tipo : ‘I’
         Registro : Nodo|medico|orden|especialidad (es decir los valores grabados separados por pipes)
         TranId : 0
         Transmitido : 0
         */
        novedadMedico = [NSString stringWithFormat:@"%@|%@|%@|%@",
                         [argMedico objectAtIndex:valueNODO           ],
                         [argMedico objectAtIndex:valueID             ],
                         [NSString stringWithFormat:@"%d", orderCount],
                         [NSString stringWithFormat:@"%d", o.recID]
                         ];
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- Novedad Alta EspecialidadMedico: %@", novedadMedico);
#endif
        [Novedad insert:@"med_especialidades" novedad:novedadMedico tipo:kInsert];
        
        orderCount++;
    }
    
    
    
#pragma mark    Insert MED_OBRASSOCIALES
    
    orderCount = 1;
    if ([self.obrasSociales count] > 0)
        for (ObraSocial *o in self.obrasSociales)
        {
            /*
             Insert a la MED_OBRASSOCIALES (tabla relación nodo, medico, obra social)
             
             Iterar entre las obras sociales seleccionadas :
             
             "nodo"   = nodo variable global
             "medico" = id de medico
             "obrasocial" = idObraSocial de la obra social seleccionada obtenido de la mtc_obrassociales
             */
            
            sqlMedicos = @"INSERT INTO med_obrassociales "
            "(nodo, medico, obrasocial) "
            "VALUES(%@,%@,%@);";
            queryMedico = [NSString stringWithFormat:sqlMedicos,
                           [argMedico objectAtIndex:valueNODO           ],
                           [argMedico objectAtIndex:valueID             ],
                           [NSString intToStr:o.idObraSocial]
                           ];
            
#ifdef DEBUG_ABMMEDICO
            NSLog(@"- query Alta OSocialMedico: %@", queryMedico);
#endif
            [[DB getInstance] excecuteSQL:queryMedico];
            
            /*
             Grabar novedad en la log_novedades para la MED_OBRASSOCIALES
             Tabla : MED_OBRASSOCIALES
             Tipo : ‘I’
             Registro : Nodo|medico|orden|obrasocial (es decir los valores grabados separados por pipes)
             TranId : 0
             Transmitido : 0
             */
            novedadMedico = [NSString stringWithFormat:@"%@|%@|%@",
                             [argMedico objectAtIndex:valueNODO           ],
                             [argMedico objectAtIndex:valueID             ],
                             [NSString intToStr:o.idObraSocial]
                             ];
#ifdef DEBUG_ABMMEDICO
            NSLog(@"- Novedad Alta OSocialMedico: %@", novedadMedico);
#endif
            [Novedad insert:@"med_obrassociales" novedad:novedadMedico tipo:kInsert];
            
            orderCount++;
            
        }
    
    
#pragma mark    Insert MED_DOMICILIOS
    
    orderCount = 1;
    for (Domicilio *o in self.domicilios)
    {
        /*
         Insert a la MED_DOMICILIOS (tabla relación nodo, medico, domicilios)
         
         Iterar entre las direcciones cargadas :
         
         "nodo" = nodo variable global);
         "medico", id de medico
         "id",  = max id +1 para nodo y medico en la tabla med_domicilios
         "pais", país = id de país variable global
         "cp", = código postal cargado en la pantalla de domicilios
         "provincia" = provincia = id de provincia cargado en el domicilio
         "localidad" = descripción de la localidad seleccionada en carga domicilio
         "calle", = descripción de la calle cargada en la pantalla de domicilios
         "altura” =  altura de la calle cargada en la pantalla de domicilios
         
         Si piso <1
         "piso” = 0
         Sino
         "piso” = piso del depto cargado en la pantalla de domicilios
         
         
         Si departamento = vacio
         Departamento = null
         Sino
         Departamento = departamento cargado en la pantalla de domicilios
         
         "tipo” = id del tipo de domicilio seleccionado tomado de la mtc_tiposdomicilio
         
         "utilidad” = Id de la utilidad seleccionado tomado de la mtc_utilidadesdomicilio
         
         Si no se selecciono {
         "institucion" = null
         sino
         "institucion", id institución tomado de la mtc_instituciones
         }
         Si no se selecciono CARGO
         cargo= null
         sino
         "cargo" = id de cargo seleccionado
         
         Si tilde EXTERNO seleccionado
         "externo" =  1
         Sino
         “externo" = 0
         */
        
        NSInteger idDomicilio = [[DB getInstance] getNewIDForTable:@"med_domicilios"
                                                       idNameOrNil:nil
                                                        whereOrNil:
                                 [NSString stringWithFormat:@"(nodo = %@) AND (medico = %@)",
                                  [argMedico objectAtIndex:valueNODO           ],
                                  [argMedico objectAtIndex:valueID             ]
                                  ]
                                 ];
        
        sqlMedicos = @"INSERT INTO med_domicilios "
        "(nodo, medico, id, pais, "
        "cp, provincia, localidad, calle, altura, "
        "piso, departamento, tipo, utilidad, institucion, "
        "cargo, externo) "
        "VALUES(%@, %@, %@, %@, "
        "%@, %@, %@, %@, %@,"
        "%@, %@, %@, %@, %@,"
        "%@, %@);";
        queryMedico = [NSString stringWithFormat:sqlMedicos,
                       [argMedico objectAtIndex:valueNODO           ],
                       [argMedico objectAtIndex:valueID             ],
                       [NSString intToStr:idDomicilio],
                       SettingForKey(PAIS, @"54"),
                       [NSString stringQuoted:o.codigoPostal ifNull:@"'0'"],
                       [NSString intToStr:o.provincia.idProvincia],
                       [NSString stringQuoted:o.localidad.descripcion ifNull:@"''"],
                       [NSString stringQuoted:o.calle.descripcion ifNull:@"''"],
                       [NSString intToStr:o.numero],
                       [NSString intToStr:o.piso],
                       [NSString stringQuoted:o.depto ifNull:@"NULL"],
                       [NSString intToStr:o.tipo.idTipo],
                       [NSString intToStr:o.utilidad.idUtilidad],
                       [NSString intToStr:o.institucion.recID],
                       o.cargo?[NSString intToStr:o.cargo.idCargo]:@"NULL",
                       o.consultorioExterno?@"1":@"0"
                       ];
        
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- query Alta DomicilioMedico: %@", queryMedico);
#endif
        [[DB getInstance] excecuteSQL:queryMedico];
        
        /*
         Grabar novedad en la log_novedades para la MED_DOMICILIOS
         Tabla : MED_DOMICILIOS
         Tipo : ‘I’
         Registro : nodo|medico|id|pais|cp.trim()|provincia|localidad.trim()|calle.trim()|altura|
         (piso<1?"0":piso)|departamento.trim()|tipo|utilidad|(institucion==-1?"":institucion)|
         (cargo==-1||cargo==0?"":cargo)|externo==1?"1":"0"
         TranId : 0
         Transmitido : 0
         */
        
        novedadMedico = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",
                         [argMedico objectAtIndex:valueNODO           ],
                         [argMedico objectAtIndex:valueID             ],
                         [NSString intToStr:idDomicilio],
                         SettingForKey(PAIS, @"54"),
                         [NSString string:o.codigoPostal ifNull:@"0"],
                         [NSString intToStr:o.provincia.idProvincia],
                         [NSString string:o.localidad.descripcion ifNull:@""],
                         [NSString string:o.calle.descripcion ifNull:@""],
                         [NSString intToStr:o.numero],
                         [NSString intToStr:o.piso],
                         [NSString string:o.depto ifNull:@""],
                         [NSString intToStr:o.tipo.idTipo],
                         [NSString intToStr:o.utilidad.idUtilidad],
                         [NSString intToStr:o.institucion.recID],
                         o.cargo?[NSString intToStr:o.cargo.idCargo]:@"",
                         o.consultorioExterno?@"S":@"N"
                         ];
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- Novedad Alta DomicilioMedico: %@", novedadMedico);
#endif
        [Novedad insert:@"med_domicilios" novedad:novedadMedico tipo:kInsert];
    

        
#pragma mark    Insert MED_HORARIOS
        
        // Insert a la MED_HORARIOS (tabla relación nodo, medico, horario)
        if ([o.horarios count] > 0)
        {
            for (Horario *h in o.horarios)
            {
                [h saveForMedico:[self.recID integerValue] Domicilio:idDomicilio];
            }
        }
        
        
#pragma mark    Insert MED_TELEFONOS
        
        if ([o.telefonos count] > 0)
            for (Telefono *t in o.telefonos)
            {
                [t saveForMedico:[self.recID integerValue] Domicilio:idDomicilio];
            }
        orderCount++;
    }
    
}


+(NSArray*) GetAll{
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    
    NSString *sql = @"select 1 as orden, id as _id, apellido || ', ' || nombre as descripcion, calle, altura, localidad "
                     "from med_medicos "
                     "where ((nodo = %@) AND (B <> 1) AND (penalta <> 1)) "
                     "order by orden, descripcion";
    
    
//    med_medicos.B <> 1
//    med_medicos.penalta<>1
    
    NSString *query = [NSString stringWithFormat:sql, [[Config shareInstance] find:NODO]];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    MTMedico *o;
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
			
            o = [[MTMedico alloc] init];
            
            // No utilizo la columan 0.
            
            // id del médico
            int _recID = sqlite3_column_int(statement, 1);
            o.recID = [NSString stringWithFormat:@"%d",_recID];
            
            // Nombre del médico
            char *_nombreMedico = (char *)sqlite3_column_text(statement, 2);
            o.descripcion = @"";
            if ((_nombreMedico != nil) && (strlen(_nombreMedico) > 0))
                o.descripcion = [NSString stringWithUTF8String:_nombreMedico];
            
            // Direccion del médico
            char *_direccionMedico = (char *)sqlite3_column_text(statement, 3);
            o.direccion = @"";
            if ((_direccionMedico != nil) && (strlen(_direccionMedico) > 0))
                o.direccion = [NSString stringWithUTF8String:_direccionMedico];
            
            // Altura de dirección del médico
            int _alturaMedico = sqlite3_column_int(statement, 4);
            o.altura = _alturaMedico;
            
            // Localidad del médico
            char *_localidadMedico = (char *)sqlite3_column_text(statement, 5);
            o.localidad = @"";
            if ((_localidadMedico != nil) && (strlen(_localidadMedico) > 0))
                o.localidad = [NSString stringWithUTF8String:_localidadMedico];
            
            [array addObject:o];
//            [o release];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla med_medicos");
	}
	sqlite3_finalize(statement);
    
    
    return array;
}


+ (MTMedico*) Medico
{
    
    MTMedico *medico = [[MTMedico alloc] init];
    
    medico.modo = kMTEAAdd;
    medico.especialidades = [NSMutableArray array];
    medico.domicilios = [NSMutableArray array];
    medico.obrasSociales = [NSMutableArray array];

    return medico;
}

+ (MTMedico*) GetById:(NSUInteger) idMedico
{
    MTMedico *m = [MTMedico Medico];
    m.modo = kMTEANone;
    m.tratamiento = [[Tratamiento alloc] init];
    m.potencial = [[Potencial alloc] init];
    
    NSString *nodo = SettingForKey(NODO, @"");
//    NSString *sql = @"SELECT id, "
//    "estado, "
//    "visitasprogramadas, "
//    "visitasrealizadas, "
//    "apellido, "
//    "nombre, "
//    "actividad, "
//    "sexo, "
//    "descpotencial, "
//    "especialidad1, "
//    "desctratamiento, "
//    "nacimiento, "
//    "egreso, "
//    "cuit, "
//    "email, "    
//    "matriculanacional, "
//    "matriculaprovincial, "
//    "lu, "
//    "ma, "
//    "mi, "
//    "ju, "
//    "vi, "
//    "desde, "
//    "hasta "
//    "FROM med_medicos "
//    "WHERE id=%d and nodo = %@";
    NSString *sql = @"SELECT id, "
    "estado, "
    "visitasprogramadas, "
    "visitasrealizadas, "
    "apellido, "
    "nombre, "
    "actividad, "
    "sexo, "
    "descpotencial, "
    "especialidad1, "
    "desctratamiento, "
    "nacimiento, "
    "egreso, "
    "cuit, "
    "email, "
    "matriculanacional, "
    "matriculaprovincial, "
    "lu, "
    "ma, "
    "mi, "
    "ju, "
    "vi, "
    "desde, "
    "hasta, "
    
    "codigointerno, "
    "asignacion, "
    "tratamiento, "
    "idpool, "
    "fechaalta, "
    "objetivos, "
    "marca, "
    "cp, "
    "a, "
    "b, "
    "m, "
    "localidad, "
    "calle, "
    "altura, "
    "piso, "
    "dpto, "
    "especialidad2, "
    "comentarios, "
    "lugar, "
    "descestado, "
    "descabm, "
    "potencial, "
    "descpotencial, "
    "descmarcado, "
    "penalta, "
    "penbaja "
    "FROM med_medicos "
    "WHERE id=%d and nodo = %@";
    NSString *query = [NSString stringWithFormat:sql, idMedico, nodo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    m.recID = [NSString intToStr:idMedico];
    
    char *_c;
	if ( statement ) {
		while (sqlite3_step(statement) == SQLITE_ROW) {            
            
            m.recID = [NSString stringWithFormat:@"%d", sqlite3_column_int(statement, 0)];
            m.idEstado = sqlite3_column_int(statement, 1);
            m.visitasProgramadas = sqlite3_column_int(statement, 2);
            m.visitasRealizadas = sqlite3_column_int(statement, 3);
            
            _c = (char *)sqlite3_column_text(statement, 4);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.apellido = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 5);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.nombre = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 6);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.actividad = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 7);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.sexo  = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 8);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.descPotencial  = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 9);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.especialidad1  = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 10);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.descTratamiento  = [NSString stringWithUTF8String:_c];
			}
            
            
            //m.fechaNacimiento  = sqlite3_column_double(statement, 11);
            if (sqlite3_column_int(statement, 11) != 0) {
            m.fechaNacimiento = [[NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 11)] dateAsDateWithoutTime];
            }
            
            _c = (char *)sqlite3_column_text(statement, 12);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.anioEgreso  = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 13);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.CUIT  = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 14);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.email  = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 15);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.matriculaNacional  = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 16);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.matriculaProvincial  = [NSString stringWithUTF8String:_c];
			}
            
            m.lu = sqlite3_column_int(statement, 17);
            m.ma = sqlite3_column_int(statement, 18);
            m.mi = sqlite3_column_int(statement, 19);
            m.ju = sqlite3_column_int(statement, 20);
            m.vi = sqlite3_column_int(statement, 21);
            m.desde = sqlite3_column_int(statement, 22);
            m.hasta = sqlite3_column_int(statement, 23);
            
//            "codigointerno, "
            m.codigoInterno = [NSString pCharToString:(char *)sqlite3_column_text(statement, 24)];
//            "asignacion, "
            m.asignacion = sqlite3_column_int(statement, 25);
//            "tratamiento, "
            m.tratamiento.recID = sqlite3_column_int(statement, 26);
            m.tratamiento.descripcion = m.descTratamiento;
//            "idpool, "
            m.idPool = sqlite3_column_int(statement, 27);
//            "fechaalta, "
            m.fechaAlta = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(statement, 28)];
//            "objetivos, "
            m.objetivos = [NSString pCharToString:(char *)sqlite3_column_text(statement, 29)];
//            "marca, "
            m.marca = [NSString pCharToString:(char *)sqlite3_column_text(statement, 30)];
//            "cp, "
            m.cp = [NSString pCharToString:(char *)sqlite3_column_text(statement, 31)];
//            "a, "
            m.A = sqlite3_column_int(statement, 32);
//            "b, "
            m.B = sqlite3_column_int(statement, 33);
//            "m, "
            m.M = sqlite3_column_int(statement, 34);
//            "localidad, "
            m.localidad = [NSString pCharToString:(char *)sqlite3_column_text(statement, 35)];
//            "calle, "
            m.direccion = [NSString pCharToString:(char *)sqlite3_column_text(statement, 36)];
//            "altura, "
            m.altura = sqlite3_column_int(statement, 37);
//            "piso, "
            m.piso = sqlite3_column_int(statement, 38);
//            "dpto, "
            m.dpto= [NSString pCharToString:(char *)sqlite3_column_text(statement, 39)];
//            "especialidad2, "
            m.especialidad2 = [NSString pCharToString:(char *)sqlite3_column_text(statement, 40)];
//            "comentarios, "
            m.comentarios = [NSString pCharToString:(char *)sqlite3_column_text(statement, 41)];
//            "lugar, "
            m.lugar = [NSString pCharToString:(char *)sqlite3_column_text(statement, 42)];
//            "descestado, "
            m.descEstado = [NSString pCharToString:(char *)sqlite3_column_text(statement, 43)];
//            "descabm, "
            m.descABM = [NSString pCharToString:(char *)sqlite3_column_text(statement, 44)];
//            "potencial, "
            m.potencial.recID = sqlite3_column_int(statement, 45);
//            "descpotencial, "
            m.descPotencial= [NSString pCharToString:(char *)sqlite3_column_text(statement, 46)];
            m.potencial.descripcion = m.descPotencial;
//            "descmarcado, "
            m.descMarcado = [NSString pCharToString:(char *)sqlite3_column_text(statement, 47)];
//            "penalta, "
            m.penAlta = sqlite3_column_int(statement, 48);
//            "penbaja "
            m.penBaja = sqlite3_column_int(statement, 49);
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    // ESPECIALIDADES
    
    NSString *sqlEsp = @"SELECT orden, especialidad, descripcion, sigla FROM med_especialidades esp inner join mtc_especialidades mtc on esp.especialidad = mtc.id where nodo = %@ and medico = %d ";
    
    NSString *queryEsp = [NSString stringWithFormat:sqlEsp, nodo, idMedico ];
    //NSLog(@"%@",queryEsp);
    sqlite3_stmt *statementEsp = [[DB getInstance] prepare:queryEsp];
    
    Especialidad *o = nil;
    
	if ( statementEsp )
    {
        
        while (sqlite3_step(statementEsp) == SQLITE_ROW)
        {
            o = [[Especialidad alloc] init];
            
            // id
            o.orden = sqlite3_column_int(statementEsp, 0);
            o.recID = sqlite3_column_int(statementEsp, 1);
            
            // Descripcion
            char *_descripcion = (char *)sqlite3_column_text(statementEsp, 2);
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o.descripcion = [NSString stringWithUTF8String:_descripcion];
            
            char *_sigla = (char *)sqlite3_column_text(statementEsp, 3);
            if ((_sigla != nil) && (strlen(_sigla) > 0))
                o.sigla = [NSString stringWithUTF8String:_sigla];
            
                       								
            [m.especialidades addObject:o];
        }
    }
    
    sqlite3_finalize(statementEsp);
    
    // OBRAS SOCIALES
    
    NSString *sqlObsSoc = @"select med_os.obrasocial, os.descripcion as descripcion from med_obrassociales med_os inner join mtc_obrassociales os on med_os.obrasocial = os.id where nodo = %@ and medico = %d ";
    
      
    NSString *queryObsSoc = [NSString stringWithFormat:sqlObsSoc, nodo, idMedico ];
    //NSLog(@"%@",queryObsSoc);
    sqlite3_stmt *statementObsSoc = [[DB getInstance] prepare:queryObsSoc];
    
    ObraSocial 	*os = nil;
    
	if ( statementObsSoc )
    {
        
        while (sqlite3_step(statementObsSoc) == SQLITE_ROW)
        {
            os = [[ObraSocial alloc] init];
            
            // id
            os.idObraSocial = sqlite3_column_int(statementObsSoc, 0);
            
            // Descripcion
            char *_descripcion = (char *)sqlite3_column_text(statementObsSoc, 1);
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                os.descripcion = [NSString stringWithUTF8String:_descripcion];
            
                      
            
            [m.obrasSociales addObject:os];
        }
    }
    
    sqlite3_finalize(statementObsSoc);
    
    // DIRECCIONES
    
    NSString *sqlDirecciones = @"select "
    "dom.id, "
    "dom.pais, "
    "dom.cp, "
    "dom.provincia, "
    "dom.localidad, "
    "dom.calle, "
    "dom.altura, "
    "piso, "
    "case when departamento is null then \'\' else departamento end as departamento, "
    "tipo, "
    "utilidad, "
    "institucion, "
    "cargo, "
    "externo, "
    "util.descripcion as descutilidad, "
    "tipos.descripcion as desctipo, "
    "ci.descripcion as desccargo, "
    "gp.descripcion as descprovincia, "
    "ins.descripcion as descinstitucion "
    "from med_domicilios dom left outer join mtc_utilidadesdomicilio util on dom.utilidad = util.id left outer join mtc_tiposdomicilio tipos on dom.tipo = tipos.id left outer join mtc_cargosinstitucionales ci on ci.id = dom.cargo left outer join geo_provincias gp on gp.id = dom.provincia left outer join mtc_instituciones ins on ins.id = dom.institucion where nodo = %@ and medico = %d ";
    


    NSString *queryDirecciones = [NSString stringWithFormat:sqlDirecciones, nodo, idMedico ];
    //NSLog(@"%@",queryDirecciones );
    sqlite3_stmt *statementDirecciones = [[DB getInstance] prepare:queryDirecciones];

    Domicilio *dom = nil;
    
	if ( statementDirecciones )
    {
        
        while (sqlite3_step(statementDirecciones) == SQLITE_ROW)
        {
            dom = [[Domicilio alloc] init];
            dom.provincia   = [[Provincia alloc] init];
            dom.localidad   = [[Localidad alloc] init];
            dom.calle       = [[Calle alloc] init];
            dom.institucion = [[Institucion alloc] init];
            dom.cargo       = [[Cargo alloc] init];
            dom.tipo        = [[Tipo alloc] init];
            dom.utilidad    = [[Utilidad alloc] init];
            
            // id
            dom.idDomicilio = sqlite3_column_int(statementDirecciones, 0);
            
            
            // codigo postal
            char *_cp = (char *)sqlite3_column_text(statementDirecciones, 2);
            if ((_cp != nil) && (strlen(_cp) > 0))
                dom.codigoPostal = [NSString stringWithUTF8String:_cp];
            
            // provincia            
            
            dom.provincia.idProvincia = sqlite3_column_int(statementDirecciones, 3);   
                      
            char *_desclocalidad = (char *)sqlite3_column_text(statementDirecciones, 4);
            if ((_desclocalidad != nil) && (strlen(_desclocalidad) > 0))
                dom.localidad.descripcion = [NSString stringWithUTF8String:_desclocalidad];
            
            char *_desccalle = (char *)sqlite3_column_text(statementDirecciones, 5);
            if ((_desccalle != nil) && (strlen(_desccalle) > 0))
                dom.calle.descripcion = [NSString stringWithUTF8String:_desccalle];
            
            
                   
            dom.numero = sqlite3_column_int(statementDirecciones, 6); 
           // dom. = sqlite3_column_int(statementDirecciones, 5);
            dom.piso = sqlite3_column_int(statementDirecciones, 7);
            
            // departamento
            char *_depto = (char *)sqlite3_column_text(statementDirecciones, 8);
            if ((_depto != nil) && (strlen(_depto) > 0))
                dom.depto = [NSString stringWithUTF8String:_depto];
            
            dom.tipo.idTipo = sqlite3_column_int(statementDirecciones, 9);           
                       
            dom.utilidad.idUtilidad = sqlite3_column_int(statementDirecciones, 10);
            dom.institucion.recID =   sqlite3_column_int(statementDirecciones, 11);
           
            dom.cargo.idCargo = sqlite3_column_int(statementDirecciones, 12);
           // dom.utilidad.de
            
            // Consultorio Externo
            dom.consultorioExterno = sqlite3_column_int(statementDirecciones, 13);
            
            char *_descutilidad = (char *)sqlite3_column_text(statementDirecciones, 14);
            if ((_descutilidad != nil) && (strlen(_descutilidad) > 0))
                dom.utilidad.descripcion = [NSString stringWithUTF8String:_descutilidad];
            
            char *_desctipo = (char *)sqlite3_column_text(statementDirecciones, 15);
            if ((_desctipo != nil) && (strlen(_desctipo) > 0))
                dom.tipo.descripcion = [NSString stringWithUTF8String:_desctipo];
            
            char *_desccargo = (char *)sqlite3_column_text(statementDirecciones, 16);
            if ((_desccargo != nil) && (strlen(_desccargo) > 0))
                dom.cargo.descripcion = [NSString stringWithUTF8String:_desccargo ];
            
            char *_descprovincia = (char *)sqlite3_column_text(statementDirecciones, 17);
            if ((_descprovincia != nil) && (strlen(_descprovincia) > 0))
                dom.provincia.descripcion = [NSString stringWithUTF8String:_descprovincia ];
            
            char *_descinstitucion = (char *)sqlite3_column_text(statementDirecciones, 18);
            if ((_descinstitucion != nil) && (strlen(_descinstitucion) > 0))
                dom.institucion.descripcion = [NSString stringWithUTF8String:_descinstitucion];
            
            dom.telefonos = [NSMutableArray arrayWithArray:[Telefono getAllByMedico:[m.recID integerValue]
                                                                          Domicilio:dom.idDomicilio]];
            
           dom.horarios = [NSMutableArray arrayWithArray:[Horario getAllByMedico:[m.recID integerValue]
                                                                       Domicilio:dom.idDomicilio]];
            
            [dom fillDomicilioByCP];
            [m.domicilios addObject:dom];
        }
    }
    
    sqlite3_finalize(statementDirecciones);

    
    return m;
}

- (NSUInteger)getNewID
{
    NSUInteger result   = 0;
    
    NSUInteger newIDdb  = 0;
    NSUInteger newIDcfg = 0;
    
    // Obtengo la nueva ID según el último registro de la báse de datos.
    newIDdb = [[DB getInstance] getNewIDForTable:@"med_medicos"
                                     idNameOrNil:nil
                                      whereOrNil:nil];
    
    newIDcfg = [self cfgUltimoRead];
    
    result = newIDcfg;
    
    if (newIDdb > newIDcfg)
        result = newIDdb;
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"--- New ID Medico ---");
    NSLog(@"- idDB = %d     - idCFG = %d", newIDdb, newIDcfg);
    NSLog(@"- newID = %d", result);
#endif
    
    return result;
}


#pragma mark - 
#pragma mark Public Medicos flutuantes methods

-(BOOL)isFluctuante
{
    if (self.asignacion == 2)
        return YES;
    
    return NO;
}

- (NSString*)isOkForSaveFluctuante
{
    /*
     Esta rutina chequea los datos obligatorios y que los ingresados sean válidos.
     
     Los datos obligatorios a ingresar son :
     √ Domicilio,
     X  horario,
     √ Apellido,
     √ Nombre,
     √ Una de las 2 matriculas,
     √ actividad,
     √ sexo,
     √ potencial,
     X año de egreso
     */
    
    NSString *result = nil; // Resultado por defecto, esto quiere decir que no hubo error.
    
    Domicilio *medDomicilio = nil;
    
    // Busca el domicilio de Visita en lugar del primer domicilio y lo posiciona como primero.
    if ([self.domicilios count] > 0)
    {
        medDomicilio = [self.domicilios objectAtIndex:0];
    }
    
#pragma mark    Nuevo ID
    // Si es un Alta, busco el nuevo ID
    if (self.modo == kMTEAAdd)
    {
        //self.recID = [NSString intToStr:[self cfgUltimoRead]];
        self.recID = [NSString intToStr:[self getNewID]];
    }
    
    
#pragma mark    Apellido y Nombre
    /* Apellido
     Mensaje : Debe ingresar el apellido del médico.*/
    if ((!self.apellido) || ([self.apellido length] < 1))
        return @"Debe ingresar el apellido del médico";
    
    /* Nombre
     Mensaje : Debe ingresar el nombre del médico.*/
    if ((!self.nombre) || ([self.nombre length] < 1))
        return @"Debe ingresar el nombre del médico";
    
#pragma mark    Actividad, Potencial y Especialidad
    /* Actividad
     MENSAJE :  Debe seleccionar la actividad.*/
    if ([self.actividad length] < 1)
        return @"Debe seleccionar la actividad del médico";

    // Potencial
    NSArray *arrayP = [Potencial GetAll];
        
    if ([arrayP count] > 0)
    {
        self.potencial = [arrayP objectAtIndex:0];
    } else
    {
        // Nunca debería entrar acá ya que debería tener siempre al menos un potencial
        // en la base de datos. Pero en el supuesto caso que suceda, lo completo con el id 1
        self.potencial = [[Potencial alloc] init];
        self.potencial.recID = 1;
    }
    
    /* Especialidad
     MENSAJE: Debe ingresar al menos una especialidad.*/
    if ([self.especialidades count] < 1)
        return @"Debe ingresar la especialidad del médico";
    
#pragma mark    Domicilio
    /* Domicilio
     Mensaje : Debe ingresar un domicilio de visita. */
    if ([self.domicilios count] < 1)
        return @"Debe ingresar el lugar de visita del médico";
    
#pragma mark    Sexo, Tratamiento
    /* Sexo
     Debe seleccionar el sexo del médico.*/
    if ((!self.sexo) || ([self.sexo length] < 1))
        return @"Debe seleccionar el sexo del médico";
    
    /* Tratamiento
     MENSAJE : Debe seleccionar el tratamiento del médico*/
    if ((!self.tratamiento) || (self.tratamiento.recID == 0))
        return @"Debe seleccionar el tratamiento del médico";
   
#pragma mark    Matriculas
    // Matricula.
    
    // Chequeo si se trata de Chile, que en lugar de la matrícula se utiliza el RUT (CUIT).
    if ([[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_CL"])
    {
        // Copio el RUT menus un caracter (dígito verificador) a la amtricula nacional.
        self.matriculaNacional = @"";
        if ([self.CUIT length] > 1)
        {
            self.matriculaNacional = [self.CUIT substringToIndex:[self.CUIT length]-1];
        } else
            self.CUIT = @"";
    }
    
    if (([self.matriculaNacional integerValue] == 0) && ([self.matriculaProvincial integerValue] == 0))
        return [[MTRegionalDictionary sharedInstance] valueForText:@"Debe ingresar al menos una matrícula del médico"];
    
    NSInteger matriculaNacional     = 0;
    NSInteger matriculaProvincial   = 0;
    
    /*Se debe verificar que la/s matricula/s ingresada/s no exista/n :
     
     Para buscar matricula nacional :
     "select matriculanacional from med_medicos where id <> %d and matriculanacional = %d
     */
    NSString *sql = @"select matriculanacional from med_medicos where id <> %@ and matriculanacional = %d";
    NSString *query = [NSString stringWithFormat:sql,
                       [NSString string:self.recID ifNull:@"0"],
                       [self.matriculaNacional integerValue]];
    
    if (!([self.matriculaNacional integerValue] == 0))
    {
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- mat nacional : %@", query);
#endif
        sqlite3_stmt *statement = [[DB getInstance] prepare:query];
        if ( statement ) {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                matriculaNacional = sqlite3_column_int(statement, 0);
            }
            
        } else {
            NSAssert(0, @" - No se pudo ejecutar: %@", query);
        }
        sqlite3_finalize(statement);
    }
    
    
    /*
     Para buscar matricula provincial :
     
     select matriculaprovincial from med_medicos med
     left join med_domicilios dom
     on med.nodo = dom.nodo and med.id = dom.medico and dom.utilidad = 1
     where med.id <> %d and matriculaprovincial = %d and provincia = %d
     */
    
    if (!([self.matriculaProvincial integerValue] == 0))
    {
        Domicilio *d = [self.domicilios objectAtIndex:0];
        sql = @"select matriculaprovincial from med_medicos med "
        "left join med_domicilios dom "
        "on med.nodo = dom.nodo and med.id = dom.medico and dom.utilidad = 1 "
        "where med.id <> %@ and matriculaprovincial = %d and provincia = %d";
        query = [NSString stringWithFormat:sql,
                 [NSString string:self.recID ifNull:@"0"],
                 [self.matriculaProvincial integerValue],
                 d.provincia.idProvincia];
        
        
        sqlite3_stmt *statement2 = [[DB getInstance] prepare:query];
        if ( statement2 ) {
            while (sqlite3_step(statement2) == SQLITE_ROW)
            {
                matriculaProvincial = sqlite3_column_int(statement2, 0);
            }
            
        } else {
            NSAssert(0, @" - No se pudo ejecutar: %@", query);
        }
        sqlite3_finalize(statement2);
    }
    
    
    /* Si matricula nacional  es diferente de cero y existe
     MENSAJE : Ya existe el N° de Matrícula Nacional en otro médico.*/
    if ((matriculaNacional != 0) && ([self.matriculaNacional integerValue] != 0))
        return [[MTRegionalDictionary sharedInstance] valueForText:@"Ya existe el N° de Matrícula Nacional en otro médico"];
    
    
    /* Si matricula provincial es diferente de cero y  existe :
     MENSAJE : Ya existe el N° de Matrícula Provincial en otro médico.*/
    if ((matriculaProvincial != 0) && ([self.matriculaProvincial integerValue] != 0))
        return @"Ya existe el N° de Matrícula Provincial en otro médico";
    
    
#pragma mark    Completa el resto de los datos
    self.visitasProgramadas = 1;
    self.revisita = 0;
    
    self.fechaBaja    = nil;
    self.idMotivoBaja = 0;
    self.asignacion   = 2;
    
    self.cp        = medDomicilio.codigoPostal;
    self.localidad = medDomicilio.localidad.descripcion;
    self.direccion = medDomicilio.calle.descripcion;
    self.altura    = medDomicilio.numero;
    self.piso      = medDomicilio.piso;
    self.dpto      = medDomicilio.depto;
    self.lugar     = medDomicilio.tipo.descripcion;
    
    self.desde = 0;
    self.hasta = 0;
    self.comentarios = @"";
    
    self.lu = NO;
    self.ma = NO;
    self.mi = NO;
    self.ju = NO;
    self.vi = NO;
    
    Especialidad *medEspecialidad = [self.especialidades objectAtIndex:0];
    self.especialidad1 = medEspecialidad.sigla;
    self.especialidad2 = @"";
    
    self.descPotencial   = [NSString string:self.potencial.descripcion ifNull:@""];
    self.descTratamiento = self.tratamiento.descripcion;
    
    self.penBaja = NO;
    self.penAlta = NO;
    
    // Completo los datos restantes para el Alta o Modificación.
    if (self.modo == kMTEAAdd)
    {  // Completo los datos para el ALTA.
        self.descMarcado = @"";
        
        self.codigoInterno = @"";
        self.visitasRealizadas = 0;
        
        self.idEstado = 2;
        self.descEstado = @"NO VISTO";
        
        self.fechaAlta = [NSDate dateWithoutTime];
        
        //self.objetivos = @"";
        self.marca     = @"0";
        
        self.A = YES;
        self.B = NO;
        self.M = NO;
        self.descABM = @"Alta";
        
        self.idPool = 0;
        
    } else
    {  // Completo los datos para el MODIFICACION.
        
        // Estos valores quedan como estaban al leer el médico de la DB.
        //        self.codigoInterno = ;
        //
        //        self.idEstado = ;
        //        self.descEstado = ;
        //
        //        self.visitasRealizadas = ;
        //
        //        self.fechaAlta = ;
        //
        //        self.objetivos = ;
        //        self.marca     = ;
        //
        //        self.A = ;
        //        self.B = ;
        
        if ((!(self.penAlta)) && (!self.A))
        {
            self.descABM = @"Modificacion";
            self.M = YES;
        }
        
    }
    
    return result;
}


#pragma mark -
#pragma mark Private Delete methods

-(NSUInteger)cfgUltimoRead
{
    NSUInteger result = 0;
    NSUInteger valorinicial = 1;
    
    NSString *query = [NSString stringWithFormat:
                       @"SELECT valor, "
                               "valorinicializacion "
                        "FROM cfg_ultimo "
                        "WHERE nodo= %@ AND nombre='medicos';",
                             SettingForKey(NODO, @"0")];
    
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- Query cfg_ultimo Medico: %@", query);
#endif

    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        
		if (sqlite3_step(statement) == SQLITE_ROW)
        {
            // Valor Actual cfg_ultimo.
            result       = sqlite3_column_int(statement, 0);
            valorinicial = sqlite3_column_int(statement, 1);
        }
        
	} else
    {
        NSAssert(0, @"ERROR: No se pudo ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    self.id_valorinicial = valorinicial;
    result++;
    
    return result;
}

-(void)cfgUltimoWrite:(NSUInteger)value
{
    NSString  *query;
    NSInteger cantRegistros = 0;
    
    
    cantRegistros = [[DB getInstance] existsRowForTable:@"cfg_ultimo"
                                              condition:@"nombre = 'medicos'"];
    
//    // Si hay más de un registro en la base de de datos, los borro.
//    if (cantRegistros > 1)
//    {
//        query = [NSString stringWithFormat:
//                 @"DELETE FROM cfg_ultimo "
//                 "WHERE (nombre = 'medicos' AND nodo = %@);",
//                 SettingForKey(NODO, @"0")];
//        
//        [[DB getInstance] excecuteSQL:query];
//        
//        cantRegistros = 0;
//    }
    
    // Chequeo si ya existe el registro de cfg_ultimo
    if (cantRegistros > 0)
    {
        // Leo el valor actual de valorinicializacion.
        [self cfgUltimoRead];
        
        // UPDATE
        query = [NSString stringWithFormat:
                 @"UPDATE cfg_ultimo "
                    "set valor = %d "
                 "WHERE (nombre = 'medicos' AND nodo = %@);",
                 value,
                 SettingForKey(NODO, @"0")];
        
    } else
    {
        // INSERT
        query = [NSString stringWithFormat:
                 @"INSERT INTO cfg_ultimo "
                    "(nombre, nodo, valorinicializacion, valor) "
                 "VALUES ('medicos', %@, 1 , %d);",
                 SettingForKey(NODO, @"0"),
                 value];
        
    }
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- Query cfg_ultimo Medico: %@", query);
#endif
    [[DB getInstance] excecuteSQL:query];
}



-(void)deleteDomiciliosForMedico:(NSInteger)idMedico
{
    NSString *sqlMedicos = @"select nodo || '|' || "
    "medico || '|' || "
    "id  || '|' || "
    "case when pais is null then '0' else pais end  || '|' || "
    "cp || '|' || "
    "provincia || '|' || "
    "localidad || '|' || "
    "case when calle is null then '0' else calle end || '|' || "
    "case when altura is null then '' else altura end || '|' || "
    "case when piso is null then '' else piso end || '|' || "
    "case when departamento is null then '' else departamento end || '|' || "
    "tipo || '|' || "
    "utilidad || '|' || "
    "case when institucion is null then '' else institucion end || '|' || "
    "case when cargo is null then '' else cargo end || '|' || "
    "externo as registro "
    "from med_domicilios "
    "where nodo = %@ and medico = %@";
    
    NSString *queryMedico = [NSString stringWithFormat:sqlMedicos,
                             SettingForKey(NODO, @"0"),
                             [NSString intToStr:idMedico]];
    
    // Genero un array con los strings para luego agregar el registro en la log_novedades.
    
    NSMutableArray *arrayNovedad = [[NSMutableArray alloc] init];
    [arrayNovedad removeAllObjects];
	
    sqlite3_stmt *statement = [[DB getInstance] prepare:queryMedico];
    if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            NSString *o = @"";
            
            // Param. log_novedades
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o = [NSString stringWithUTF8String:_descripcion];
            
            [arrayNovedad addObject:[NSString stringWithFormat:@"%@",o]];
        }
        
	} else {
        NSAssert(0, @"ERROR: No se pudo ejecutar: %@", queryMedico);
	}
	sqlite3_finalize(statement);
    
    // Chequeo que haya algún dato para borrar.
    if ([arrayNovedad count] == 0)
        return;
    
    // Borro los registros de la base de datos
    sqlMedicos = @"DELETE FROM med_domicilios WHERE (nodo=%@) AND (medico=%@)";
    queryMedico = [NSString stringWithFormat:sqlMedicos,
                   SettingForKey(NODO, @"0"),
                   [NSString intToStr:idMedico]];
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- query D med_domicilios: %@", queryMedico);
#endif
    [[DB getInstance] excecuteSQL:queryMedico];
    
    
    // LOG_NOVEDADES
    for (NSString *str in arrayNovedad)
    {
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- Novedad D med_domicilios: %@", str);
#endif
        [Novedad insert:@"med_domicilios" novedad:str tipo:kDelete];
    }
    
}



#pragma mark -
#pragma mark Medico.h methods

- (NSString *)getForNovedad
{
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *FormatoFecha = @"%m/%d/%Y";
    
    NSString *query = [NSString stringWithFormat:
                       @"select nodo || \"|\" || "
                       "id || \"|\" || "
                       "ifnull(apellido, \"\") || \"|\" || "
                       "ifnull(nombre,\"\") || \"|\" || "
                       "ifnull(descmarcado,\"\") || \"|\" || "
                       "sexo || \"|\" || "
                       "ifnull(strftime('%@', nacimiento, 'unixepoch', 'localtime'), \"\") || \"|\" || "
                       //"ifnull(nacimiento, \"\") || \"|\" || "
                       "ifnull(cuit, \"\") || \"|\" || "
                       "ifnull(matriculanacional, \"\") || \"|\" || "
                       "ifnull(matriculaprovincial, \"\") || \"|\" || "
                       "ifnull(egreso,\"\") || \"|\" || "
                       "ifnull(email, \"\") || \"|\" || "
                       "codigointerno || \"|\" || "
                       "ifnull(asignacion,\"\") || \"|\" || "
                       "actividad || \"|\" || "
                       "potencial || \"|\" || "
                       "ifnull(tratamiento,\"\") || \"|\" || "
                       "visitasprogramadas || \"|\" || "
                       "visitasrealizadas || \"|\" || "
                       "ifnull(estado,\"\") || \"|\" || "
                       "ifnull(strftime('%@', fechabaja, 'unixepoch', 'localtime'), \"\") || \"|\" || "
                       "ifnull(motivobaja, \"\") || \"|\" || "
                       "ifnull(idpool,\"\") || \"|\" || "
                       "strftime('%@', fechaalta, 'unixepoch', 'localtime') || \"|\" || "
                       "ifnull(objetivos, \"\") || \"|\" || "
                       "ifnull(marca, \"\") || \"|\" || "
                       "case "
                       "when A IS 0 then 'N' "
                       "else 'S' end || \"|\" || "
                       "case "
                       "when B IS 0 then 'N' "
                       "else 'S' end || \"|\" || "
                       "case "
                       "when M IS 0 then 'N' "
                       "else 'S' end || \"|\" || "
                       "case "
                       "when penalta IS NULL then '' "
                       "when penalta is 0 then 'N' "
                       "when penalta is 1 then 'S' "
                       "else penalta end || \"|\" || "
                       "case "
                       "when penbaja IS NULL then '' "
                       "when penbaja is 0 then 'N' "
                       "when penbaja is 1 then 'S' "
                       "else penbaja end "
                       "as novedad "
                       "from med_medicos "
                       "where id = %@ and nodo = %@",
                       FormatoFecha, 
                       FormatoFecha,
                       FormatoFecha,
                       self.recID,
                       nodo];
    
    
    //NSLog(@"%@",query);
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    char *_c;
    NSString *result = @"";
	if ( statement ) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            _c = (char *)sqlite3_column_text(statement, 0);
            if((_c != nil) && (strlen(_c) > 0)) {
                result = [NSString stringWithUTF8String:_c];
            }
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
}

-(void)updateVisitaData
{
    EstadoVisita *estado = [EstadoVisita getByID:self.idEstado];
    
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *sql = @"update med_medicos set estado = %d, visitasprogramadas = %d, visitasrealizadas = %d, actividad = %@, descestado = %@, objetivos = %@ where id=%@ and nodo = %@";
    
    NSString *query = [NSString stringWithFormat:sql, self.idEstado, self.visitasProgramadas, self.visitasRealizadas,
                       [NSString stringQuoted:self.actividad ifNull:@"''"],
                       [NSString stringQuoted:estado.descripcion ifNull:@"''"],
                       [NSString stringQuoted:self.objetivos ifNull:@"''"],
                       self.recID, nodo];
    
    [[DB getInstance] excecuteSQL:query];
    
    /* UPDATE MED_MEDICOS */
   [Novedad insert:@"med_medicos" novedad:[self getForNovedad] tipo:kUpdate];
    
}



@end
