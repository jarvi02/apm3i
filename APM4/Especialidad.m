//
//  Especialidad.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Especialidad.h"
#import "Novedad.h"

#import "Config.h"
#import "DBExtended.h"
#import "NSStringExtended.h"

@implementation Especialidad

- (id)init
{
    self = [super init];
    if (self) {
        self.descripcion = @"";
        self.sigla = @"";
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [_sigla release];
    [super dealloc];
}

- (NSString*)getDescription
{
    return [NSString stringWithFormat:@"%@", self.descripcion];
}

+(NSArray*) GetAll{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"SELECT id, descripcion, sigla, 0 as orden "
                     "FROM mtc_especialidades "
                     "ORDER BY descripcion;";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
        Especialidad *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[Especialidad alloc] init];
            
            // id
            o.recID = sqlite3_column_int(statement, 0);
            
            // Descripción
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            // Siglas
            _c = (char *)sqlite3_column_text(statement, 2);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.sigla = [NSString stringWithUTF8String:_c];
			}
            
            //orden
            // id
            o.orden = sqlite3_column_int(statement, 3);
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
}


-(id)copy{
    
    Especialidad *o = [[Especialidad alloc] init];
    o.sigla = [NSString stringWithFormat:@"%@", self.sigla];
    o.descripcion = [NSString stringWithFormat:@"%@", self.descripcion];
    o.orden = self.orden;
    o.recID = self.recID;
    
    return [o autorelease];
}


+(void)deleteAllForMedico:(NSInteger)idMedico
{
    NSString *sqlMedicos = @"select nodo || '|' || medico || '|' || orden || '|' || especialidad as registro "
    "from med_especialidades where nodo = %@ and medico = %@";
    
    NSString *queryMedico = [NSString stringWithFormat:sqlMedicos,
                             SettingForKey(NODO, @"0"),
                             [NSString intToStr:idMedico]];
    
    // Genero un array con los strings para luego agregar el registro en la log_novedades.
    sqlite3_stmt *statement = [[DB getInstance] prepare:queryMedico];
    NSMutableArray *arrayNovedad = [[NSMutableArray alloc] init];
    [arrayNovedad removeAllObjects];
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            NSString *o = @"";
            
            // Param. log_novedades
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o = [NSString stringWithUTF8String:_descripcion];
            
            [arrayNovedad addObject:[NSString stringWithFormat:@"%@",o]];
        }
        
	} else {
        NSAssert(0, @"ERROR: No se pudo ejecutar: %@", queryMedico);
	}
	sqlite3_finalize(statement);
    
    // Chequeo que haya algún dato para borrar.
    if ([arrayNovedad count] == 0)
        return;
    
    // Borro los registros de la base de datos
    sqlMedicos = @"DELETE FROM med_especialidades WHERE (nodo=%@) AND (medico=%@)";
    queryMedico = [NSString stringWithFormat:sqlMedicos,
                   SettingForKey(NODO, @"0"),
                   [NSString intToStr:idMedico]];
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- query D med_especialidades: %@", queryMedico);
#endif
    [[DB getInstance] excecuteSQL:queryMedico];
    
    
    // LOG_NOVEDADES
    for (NSString *str in arrayNovedad)
    {
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- Novedad D med_especialidades: %@", str);
#endif
        [Novedad insert:@"med_especialidades" novedad:str tipo:kDelete];
    }

}
@end
