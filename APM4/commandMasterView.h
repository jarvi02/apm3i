//
//  commandMasterView.h
//  APM4
//
//  Created by Laura Busnahe on 7/31/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommandButton.h"

@protocol CommandMasterDelegate
@optional
- (void)didSelectMenuListItemAtIndex:(NSInteger)index ForButton:(CommandButton *)selectedButton;
- (void)didSelectButton:(CommandButton *)selectedButton;

@end

typedef NS_ENUM(NSUInteger, AppBarState) {
    AppBarMinimal,
    AppBarIcons,
    AppBarFullList
};

@interface commandMasterView : UIView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UINib *viewNib;

@property (nonatomic) CGRect      rightCircle;
@property (nonatomic) CGRect      middleCircle;
@property (nonatomic) CGRect      leftCircle;

@property (nonatomic, strong) UIView      *parentView;
@property (nonatomic, strong) NSString    *currentGroup;
@property (nonatomic, strong) NSArray     *menuListDataSource;
@property (nonatomic, strong) NSMutableDictionary *buttonContainer;
@property (nonatomic, strong) CommandButton       *selectedButton;

@property (retain, nonatomic) IBOutlet UIImageView *imgTop;
@property (retain, nonatomic) IBOutlet UIImageView *imgMiddle;
@property (retain, nonatomic) IBOutlet UIImageView *imgBottom;


+ (commandMasterView *)sharedInstance;

@property (retain, nonatomic) IBOutlet commandMasterView *viewReference;

- (void)addToView:(UIView *)parent;
- (void)loadGroup:(NSString *)group;

- (void)addToView:(UIView *)parent andLoadGroup:(NSString *)group;

- (void)addButton:(CommandButton *)button forGroup:(NSString *)group;
- (void)addButtons:(NSArray *)buttons forGroup:(NSString *)group;

- (void)showMinimalAppBar;
- (void)showIconsAppBar;
- (void)showFullList;

- (void)buttonForTitle:(NSString *)title setEnabled:(BOOL)enabled;

@property (nonatomic) bool                              showButtonTitles;
@property (nonatomic, readonly) AppBarState             currentState;
@property (nonatomic) bool                              autoHide;
@property (nonatomic, strong) id<CommandMasterDelegate> delegate;



@property (nonatomic) NSInteger minimalHeight;
@property (nonatomic) NSInteger fullHeight;
@property (nonatomic) NSInteger maxHeight;

@property (nonatomic) NSInteger buttonSpacing;
@property (nonatomic) NSInteger circleSize;
@property (nonatomic) NSInteger cellHeight;

@property (nonatomic) float     animationDuration;





@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UIButton    *shpLine;

@end
