//
//  StockMM.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StockMM : NSObject

@property(nonatomic, retain) NSString *descripcion;
@property(nonatomic, assign) NSInteger stock;
@property(nonatomic, retain) NSString *codigo;
@property(nonatomic, retain) NSString *lote;
@property(nonatomic, assign) NSInteger fecha_vto;

@property(nonatomic)BOOL    vencido;

- (void)checkProducto;

+(NSArray*)GetAll;

@end
