//
//  VisitaViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomViewController.h"
#import "CarteraViewController.h"
#import "DateSelectorViewController.h"
#import "TableForComboPopoverViewController.h"
#import "CustomProductoCell.h"
#import "CustomProductoHeader.h"
#import "CustomPromoCell.h"
#import "CustomPromoHeader.h"
#import "TrazabilidadViewController.h"
#import "FichaVisita.h"
#import "MTVisita.h"
#import "MTMedico.h"
#import "NSDate+extensions.h"

@class MTVisita;
@class TipoVisita;

typedef NS_ENUM(NSInteger, VisitaModo) {
    VisitaModoAlta,
    VisitaModoModificar,
    VisitaModoVer,
    VisitaModoAltaFluctuante
};



@interface VisitaViewController : CustomViewController <DateSelectorDelegate, TableForComboPopoverViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, TrazabilidadDelegate, UITextFieldDelegate>


@property(nonatomic, strong) NSDate *currentDate;
@property(nonatomic, strong) NSString *currentActividad;
@property(nonatomic, strong) TipoVisita *currentTipo;
@property(nonatomic, strong) NSArray *tiposVisita;
//@property(nonatomic, strong) MTMedico *currentMedico;

@property (nonatomic, strong) UIViewController  *trasitionStack;

//@property (nonatomic, strong) NSMutableArray  *productosList;
@property (nonatomic, strong) NSMutableArray    *entregadosList;
@property (nonatomic, retain) NSMutableArray    *promocionesList;

@property(nonatomic, assign) VisitaModo modo;
@property(nonatomic, strong) MTVisita *visita;
@property(nonatomic, assign) FichaVisita *ficha;


@property (retain, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrolledView;
@property(nonatomic, assign) CarteraViewController  *carteraViewControllerReference;
@property(nonatomic, strong) IBOutlet UITextField   *fecha;
@property(nonatomic, strong) IBOutlet UITextField   *tipo;
@property(nonatomic, strong) IBOutlet UITextField   *objetivos;
@property(nonatomic, strong) IBOutlet UITextField   *comentarios;
@property(nonatomic, strong) IBOutlet UITextField   *objetivosProx;
@property(nonatomic, strong) IBOutlet UIButton      *maniana;
@property(nonatomic, strong) IBOutlet UIButton      *tarde;
@property(nonatomic, strong) IBOutlet UIButton      *editar;

@property (nonatomic, strong) IBOutlet UIButton             *lblPromociones;
@property (nonatomic, strong) IBOutlet UISegmentedControl   *muestrasProductosSegment;
- (IBAction)tapMustrasProductosSegment:(id)sender;


// Tabla productos
@property (nonatomic, strong) IBOutlet UITableView          *productos;
@property (nonatomic, strong) UINib                         *headerNib;
@property (nonatomic, strong) IBOutlet CustomProductoHeader *tmpHeader;
@property (nonatomic, strong) IBOutlet CustomProductoCell   *tmpCell;



// Tabla promociones
@property (nonatomic, strong) IBOutlet UITableView          *promociones;
@property (nonatomic, strong) UINib                         *promoHeaderNib;
@property (nonatomic, strong) IBOutlet CustomPromoHeader    *promoHeader;
@property (nonatomic, strong) IBOutlet CustomPromoCell      *promoCell;


// Modificacion
@property (retain, nonatomic) IBOutlet UILabel  *lblVisitaTransmitida;
@property (retain, nonatomic) IBOutlet UIButton *btnManiana;
@property (retain, nonatomic) IBOutlet UIButton *btnTarde;
@property (retain, nonatomic) IBOutlet UIButton *btnFecha;
@property (retain, nonatomic) IBOutlet UIButton *btnTipo;
@property (retain, nonatomic) IBOutlet UIButton *btnAgregar;
@property (retain, nonatomic) IBOutlet UIButton *btnGuardar;
@property (retain, nonatomic) IBOutlet UIButton *btnCancelar;

// Actions
-(IBAction)selectFecha:(id)sender;
-(IBAction)selectTipo:(id)sender;
-(IBAction)selectManiana:(id)sender;
-(IBAction)selectTarde:(id)sender;
-(IBAction)guardar:(id)sender;
-(IBAction)cancelar:(id)sender;
-(IBAction)agregar:(id)sender;
-(IBAction)editar:(id)sender;

@end
