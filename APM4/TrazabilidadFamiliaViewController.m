//
//  TrazabilidadFamiliaViewController.m
//  APM4
//
//  Created by Lionel Fiszman on 31/7/15.
//  Copyright (c) 2015 Meditec. All rights reserved.
//

#import "TrazabilidadFamiliaViewController.h"
#import "ProductoTrazabilidad.h"
@interface TrazabilidadFamiliaViewController ()
@property (nonatomic,retain) NSArray* familias;
@property (nonatomic,retain) NSArray* filteredContentList;
@end

BOOL isSearching;
@implementation TrazabilidadFamiliaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.productosModificados   = [NSMutableArray array];
    self.productosPromocionados = [NSMutableArray array];
    //self.ordenCounter = 1;
    
    isSearching = false;
    self.familias = [ProductoTrazabilidad getFamiliasFromMedico:self.medicoId];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    [_searchBar release];
    [super dealloc];
}


#pragma mark - TableView


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isSearching)
        return [self.filteredContentList count];
    else
        return [self.familias count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
                             
    if(cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    if(!isSearching){
        cell.textLabel.text = [[self.familias objectAtIndex:indexPath.row] objectForKey:@"nombre"];
        cell.tag = [[[self.familias objectAtIndex:indexPath.row] objectForKey:@"id"] intValue];
    }
    else{
        cell.textLabel.text = [[self.filteredContentList objectAtIndex:indexPath.row] objectForKey:@"nombre"];
        cell.tag = [[[self.filteredContentList objectAtIndex:indexPath.row] objectForKey:@"id"] intValue];
    }
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    cell.textLabel.textColor = [UIColor blueColor];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.detailVC showProductsForFamily:[tableView cellForRowAtIndexPath:indexPath].tag];
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        
      
        NSIndexPath* ip = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
        
        [self.tableView selectRowAtIndexPath:ip animated:NO scrollPosition:UITableViewScrollPositionTop];
        
        int tag = [[[self.familias objectAtIndex:ip.row] objectForKey:@"id"] intValue];
        
        [self.detailVC showProductsForFamily:tag];
        //end of loading
        //for example [activityIndicator stopAnimating];
    }
}

#pragma mark - Search

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    isSearching = false;
    searchBar.text = @"";
    [self.tableView reloadData];
    [searchBar endEditing:true];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    isSearching = true;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    isSearching = false;
    [self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"Text change - %d",isSearching);
    
    //Remove all objects first.
    //[self.filteredContentList removeAllObjects];
    
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchTableList];
    }
    else {
        isSearching = NO;
        [self.tableView reloadData];
    }
    // [self.tblContentList reloadData];
}

- (void)searchTableList {
    NSString *searchString = self.searchBar.text;
    
    NSPredicate *p = [NSPredicate predicateWithFormat:@"nombre BEGINSWITH[c] %@", searchString];
    self.filteredContentList =[self.familias filteredArrayUsingPredicate:p];
    
    [self.tableView reloadData];
}
@end
