//
//  MTEntidad.h
//  APM4
//
//  Created by Ezequiel Manacorda on 2/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTEntidad : NSObject <NSCopying>

@property(nonatomic, retain) NSString*  recID;
@property(nonatomic, retain) NSString*  descripcion;
@property(nonatomic, retain) NSString*  direccion;
@property(nonatomic, assign) NSUInteger altura;
@property(nonatomic, retain) NSString*  localidad;

- (NSString*)getDescription;

- (MTEntidad*)copyWithZone:(NSZone*)zone;
@end
