//
//  PrevistoMensualViewController.h
//  APM4
//
//  Created by Juan Pablo Garcia on 08/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomViewController.h"
#import "PrevistoMSeleccionView.h"
#import "CustomPrevistoCell.h"
#import "TableForComboPopoverViewController.h"

@class PrevistoMensual;
@class Ciclo;

@interface PrevistoMensualWrapper : NSObject

@property (nonatomic, retain) PrevistoMensual *inner;
@property (nonatomic) BOOL selected;

@end

@interface PrevistoMensualViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate, PrevistoMEdicioDelegate, TableForComboPopoverViewControllerDelegate>
{
    Ciclo  *cicloSeleccionado;
    
    NSDate *dateFrom;
    NSDate *dateTo;
}

@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSArray     *previstos;
@property (retain, nonatomic) IBOutlet CustomPrevistoCell *tmpCell;
@property (nonatomic, retain) UINib       *cellNIB;

@property                     NSInteger   diasFuturosPrevisto;


@property (retain, nonatomic) IBOutlet UITextField *txtCiclo;
- (IBAction)ciclo:(id)sender;


-(IBAction)pressedSave:(id)sender;
-(IBAction)pressedCancel:(id)sender;

@end
