//
//  PrevistoMensual.h
//  APM4
//
//  Created by Juan Pablo Garcia on 08/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTEditActions.h"

@class MTPrevistoActividad;

typedef enum {
    kMorning,
    kAfternoon
} Type;

@interface PrevistoMensual : NSObject

@property(nonatomic, retain) NSDate *date;
@property BOOL                      dateInMS;
@property(nonatomic) Type type;
@property(nonatomic, retain) MTPrevistoActividad *activity;

@property(nonatomic)         MTEditActionsType   action;
@property(nonatomic, retain) MTPrevistoActividad *oldActivity;

// General purpose.
- (NSString *)description;
//- (NSString *)getNovedadParameters:(MTPrevistoActividad*)activity;

// Data base.
- (void)dbPerformAction;    // Este método realiza la acción definida en la propiedad action en la base de datos.


+ (PrevistoMensual*)getForFecha:(NSDate*)date Tipo:(Type)tipo;

@end
