//
//  MTInstitucion.m
//  APM4
//
//  Created by Ezequiel Manacorda on 2/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTInstitucion.h"
#import "Config.h"
#import "DB.h"

#import "NSStringExtended.h"

@implementation MTInstitucion

- (MTInstitucion*)copyWithZone:(NSZone*)zone
{
    MTInstitucion* newCopy;
    
    newCopy = [[MTInstitucion alloc] init];
    
    // MTEntidad
    newCopy.recID      = [NSString string:self.recID       ifNull:@""];
    newCopy.descripcion= [NSString string:self.descripcion ifNull:@""];
    newCopy.direccion  = [NSString string:self.direccion   ifNull:@""];
    newCopy.altura     = self.altura;
    newCopy.localidad  = [NSString string:self.localidad   ifNull:@""];
    
    return newCopy;
}

+(NSArray*) GetAll{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    NSString *sql = @"select id as _id, descripcion, calle, altura, desclocalidad from mtc_instituciones order by _id, descripcion";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    MTInstitucion *o;
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
			
            o = [[MTInstitucion alloc] init];
            
            // id de la institucion
            int _recID = sqlite3_column_int(statement, 0);
            o.recID = [NSString stringWithFormat:@"%d",_recID];
            
            // Nombre de la institución
            char *_nombre = (char *)sqlite3_column_text(statement, 1);
            o.descripcion = @"";
            if ((_nombre != nil) && (strlen(_nombre) > 0))
                o.descripcion = [NSString stringWithUTF8String:_nombre];
            
            // Direccion de la institución
            char *_direccion = (char *)sqlite3_column_text(statement, 2);
            o.direccion = @"";
            if ((_direccion != nil) && (strlen(_direccion) > 0))
                o.direccion = [NSString stringWithUTF8String:_direccion];
            
            // Altura de dirección de la institución
            int _alturaMedico = sqlite3_column_int(statement, 3);
            o.altura = _alturaMedico;
            
            // Localidad de la institución
            char * _localidad = (char *)sqlite3_column_text(statement, 4);
            o.localidad = @"";
            if ((_localidad != nil) && (strlen(_localidad) > 0))
                o.localidad = [NSString stringWithUTF8String:_localidad];
            
            [array addObject:o];
//            [o release];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla mtc_instituciones");
	}
	sqlite3_finalize(statement);
    
    
    return array;
}

@end
