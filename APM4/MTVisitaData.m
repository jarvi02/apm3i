//
//  MTVisitaData.m
//  APM4
//
//  Created by Laura Busnahe on 8/15/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTVisitaData.h"
#import "DBExtended.h"
#import "Config.h"
#import "EstadoVisita.h"
#import "Novedad.h"
#import "Promocion.h"
#import "ProductoTrazabilidad.h"
#import "ParteDiarioData.h"
#import "ParteDiario.h"


@implementation MTVisitaData

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.fecha = [NSDate dateWithoutTime];
        self.descripcionTipo = @"";
        self.domicilio       = @"";
        self.objetivos       = @"";
        self.evaluacion      = @"";
        
        self.idVisita    = 0;
        self.idTipo      = 0;
        self.idDomicilio = 0;
        self.idEstado    = 0;
        self.contacto    = 0;
        
        self.medico     = nil;
        self.productos  = [[NSMutableArray alloc] init];
        self.actividad  = @"";
    }
    
    return self;
}

- (MTVisitaData*)copyWithZone:(NSZone*)zone
{
    MTVisitaData* newCopy;
    
    newCopy = [[MTVisitaData alloc] init];
    
    newCopy.fecha           = [NSDate dateWithTimeIntervalSince1970:[self.fecha timeIntervalSince1970]];
    newCopy.descripcionTipo = [NSString string:self.descripcionTipo ifNull:@""];
    newCopy.domicilio       = [NSString string:self.domicilio ifNull:@""];
    newCopy.objetivos       = [NSString string:self.objetivos ifNull:@""];
    newCopy.evaluacion      = [NSString string:self.evaluacion ifNull:@""];
    newCopy.idVisita        = self.idVisita;
    newCopy.idTipo          = self.idTipo;
    newCopy.idDomicilio     = self.idDomicilio;
    newCopy.idEstado        = self.idEstado;
    newCopy.contacto        = self.contacto;
    
    newCopy.medico          = self.medico;
    newCopy.productos       = [NSMutableArray arrayWithArray:self.productos];
    newCopy.actividad       = [NSString string:self.actividad ifNull:@""];
    
    return newCopy;
}

-(NSString *)getForNovedad
{
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *FormatoFecha = @"%m/%d/%Y %H:%M";
    
    NSString *sql = @"select nodo || '|' || ifnull(medico,\"\") || '|' || ifnull(visitador,\"\") || '|' || id || '|' ||ifnull(strftime('%@', fecha,'unixepoch','localtime'), \"\") || '|' || ifnull(tipo,\"\") || '|' || ifnull(domicilio,\"\") || '|' || ifnull(objetivos, \"\") || '|' || ifnull(evaluacion, \"\")  from med_visitas where nodo  = %@ and id = %d and medico = %@";
    
    
    NSString *query = [NSString stringWithFormat:sql, FormatoFecha, nodo, self.idVisita, self.medico.recID];
    
#ifdef DEBUG_VISITA
    NSLog(@"- Visita: getForNovedad -");
    NSLog(@"%@",query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    char *_c;
    NSString *result = @"";
	if ( statement )
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
            _c = (char *)sqlite3_column_text(statement, 0);
            result = [NSString pCharToString:_c];
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
}

-(void)save
{
    NSString *nodo = SettingForKey(NODO, @"");
    
    self.idVisita = [self newRecID];
    
    NSString *sql = @"insert into med_visitas "
    "(nodo, medico, visitador, id, fecha, tipo, domicilio, objetivos, evaluacion) "
    "values (%@, %@, %@, %d, %.0f, %d, %d, %@, %@)";
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       self.medico.recID,
                       nodo,
                       self.idVisita,
                       [[self.fecha dateAsDateWithoutTime] timeIntervalSince1970 ] ,
                       self.idTipo,
                       self.idDomicilio,
                       [NSString stringQuoted:self.objetivos ifNull: @"''"],
                       [NSString stringQuoted:self.evaluacion ifNull: @"''"]];
    
#ifdef DEBUG_VISITA
    NSLog(@"- MTVisita: save -");
    NSLog(@"%@",query);
#endif
    
    [[DB getInstance] excecuteSQL:query];
    
}

-(void)update
{
    
    NSString *sql = @"UPDATE med_visitas "
    "SET fecha = %.0f, "
    "tipo = %d, "
    "objetivos = \"%@\", "
    "evaluacion = \"%@\" "
    "WHERE nodo = %@ and medico = %@ and visitador = %@ and id = %d";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql,
                       [[self.fecha dateAsDateWithoutTime] timeIntervalSince1970],
                       self.idTipo,
                       [NSString string:self.objetivos],
                       [NSString string:self.evaluacion],
                       nodo,
                       self.medico.recID,
                       nodo,
                       self.idVisita];
    
    [[DB getInstance] excecuteSQL:query];
    
}

- (void)guardarNuevaVisita
{
    // ALTA
    
    // Modificacion del estado y visitas del medico
    self.medico.idEstado          = [EstadoVisita getByID:self.idTipo].idEstado;
    self.medico.actividad         = self.actividad;
    
    // Si es una visita, incremento las visitas realizadas
    if (self.contacto)
    {
        self.medico.visitasRealizadas = self.medico.visitasRealizadas + 1;
        self.medico.objetivos         = [NSString string:self.medico.objetivos ifNull:@""];
    }
    [self.medico updateVisitaData];
    
    // Save the new visit into the DB
    [self save];
    
    // Novedad
    [Novedad insert:@"med_visitas" novedad:[self getForNovedad] tipo:kInsert];
    
    // Si es una visita, guardo los productos y actualizo el parte diario.
    if (self.contacto)
    {
        // Guardar Los productos
        [self guardarProductos];
    
        // Actualizar el parte Diario
        [self actualizarParteDiario];
    }
}

- (void)guardarProductos
{
#ifdef DEBUG_TRAZABILIDAD
    NSLog(@"--- guardarProductos ---");
#endif
    
    // Elimino todos generando las novedades
    NSArray *current = [Promocion GetAllByMedico:[self.medico.recID integerValue] visita:self.idVisita];
    for (Promocion *p in current)
    {
        NSString *novedad = [p getForNovedad];
        [Novedad insert:@"med_promociones" novedad:novedad tipo:kDelete];
        
#ifdef DEBUG_TRAZABILIDAD
        NSLog(@"- Novedad: med_promociones D %@", novedad);
#endif
        
        [Promocion deleteForMedico:[self.medico.recID integerValue] visita:self.idVisita];
        
        // Actualizo nuevamente todos los productos en caso de que se haya borrado alguno.
        // En el siguiente FOR vuelvo a decrementar los que aún se encuentran en la visita.
        [ProductoTrazabilidad actualizarStock:p.producto lote:p.lote nuevoStock:(p.stock + p.muestras)];
    }
    
    // Guardo los productos que actualemente contiene la visita
    for(Promocion *p in self.productos)
    {
        // Creo la promocion
        Promocion *promo = [[Promocion alloc] init];
        
        promo.medico        = [self.medico.recID integerValue];
        promo.visita        = self.idVisita;
        promo.orden         = p.orden;
        promo.ordenIngreso  = p.ordenIngreso;
        promo.producto      = p.producto;
        promo.literatura    = p.literatura;
        promo.obsequio      = p.obsequio;
        promo.lote          = p.lote;
        promo.muestras      = p.muestras;
        promo.evaluacion    = p.evaluacion;
        promo.objetivos     = p.objetivos;
        promo.promocionado  = p.promocionado;
        
        [promo save];
        
        
        // Novedad
        NSString *novedad = [promo getForNovedad];
        
#ifdef DEBUG_TRAZABILIDAD
        NSLog(@"- Novedad: med_promociones I %@", novedad);
#endif
        
        [Novedad insert:@"med_promociones" novedad:novedad tipo:kInsert];
        
        // Guardo A Promocionar
        BOOL existe = [Promocion existeRegistroAPromocionar:promo.medico producto:promo.producto lote:promo.lote];
        //if (existe && ![self.medico.objetivos isEqualToString:@""])
        if (existe)
        {
            [promo updateProductosAPromocionar];
            [Novedad insert:@"med_apromocionar" novedad:[promo getForNovedadAPromocionar] tipo:kUpdate];
        } 
        
        // Actualizo el stock
        [ProductoTrazabilidad actualizarStock:p.producto lote:p.lote nuevoStock:(p.stock - p.muestras)];
    }
}

-(void)actualizarParteDiario
{
    BOOL parteSinMedicos = [[ConfigGlobal shareInstance] find:PARTE_SIN_MEDICOS];
//    BOOL usaPorcentaje = [[ConfigGlobal shareInstance] find:PORCENTAJESENPARTEDIARIO];
    BOOL fluctuante = [self.medico isFluctuante];
    
    // Si no cumple esta condicion no hacer nada con el parte
    if (fluctuante || parteSinMedicos)
        return;
    
//    if (usaPorcentaje)
//        return;
    
    
    ParteDiarioData *parte = [ParteDiario getParteDiarioEx:9999
                                               todasTareas:1
                                                 actividad:@"'M', 'T'"
                                               todasFechas:0
                                                     fecha:self.fecha.dateAsDateWithoutTime
                                                     rango:1
                                                     desde:nil
                                                     hasta:nil];
    
    NSInteger parteDiario = [[self.fecha formattedStringUsingFormat:@"yyyyMMdd"] intValue];
    
    if ((!parte) || (parte.id_nodotareaspartediario == 0))
    {
        // No hay parte => Creo el parte
        
        // Creo el parte en NODO_PARTEDIARIO
        [ParteDiario insertCabecera:self.fecha.dateAsDateWithoutTime];
        [Novedad insert:NODO_PARTEDIARIO novedad:[ParteDiario getForNovedad:self.fecha.dateAsDateWithoutTime] tipo:kInsert];
        
        // Creo las tareas en NODO_TAREASPARTEDIARIO
        [ParteDiario insertTarea:1
                       actividad:self.actividad
                        cantidad:1
                     parteDiario:parteDiario
                           tarea:9999];
        
        [ParteDiario insertTarea:2
                       actividad:[self.actividad isEqualToString:@"M"] ? @"T" : @"M"
                        cantidad:0
                     parteDiario:parteDiario tarea:9999];
        
        [Novedad insert:NODO_TAREAS_PARTEDIARIO novedad:[ParteDiario getTareaForNovedad:1 parteDiario:parteDiario] tipo:kInsert];
        [Novedad insert:NODO_TAREAS_PARTEDIARIO novedad:[ParteDiario getTareaForNovedad:2 parteDiario:parteDiario] tipo:kInsert];
        
    } else
    {
        // Hay Parte, revisar si hay parte para la tarea 9999
        parte = [ParteDiario getParteDiario:9999
                                todasTareas:0
                                  actividad:self.actividad
                                todasFechas:0
                                      fecha:self.fecha.dateAsDateWithoutTime
                                      rango:1
                                      desde:nil
                                      hasta:nil];
        
        if ((!parte) || (parte.id_nodotareaspartediario == 0))
        {
            // NO hay tarea 9999
            NSUInteger lastId = [ParteDiario lastId:parteDiario];
            [ParteDiario insertTarea:lastId
                           actividad:self.actividad
                            cantidad:1
                         parteDiario:parteDiario
                               tarea:9999];
            
            [Novedad insert:NODO_TAREAS_PARTEDIARIO
                    novedad:[ParteDiario getTareaForNovedad:lastId
                                                parteDiario:parteDiario]
                       tipo:kInsert];
        } else
        {
            // Hay tarea 9999
            [ParteDiario addCantidad:parte.id_nodotareaspartediario
                         parteDiario:parte.partediario
                           actividad:parte.actividad
                               tarea:parte.tarea];
            
            [Novedad insert:NODO_TAREAS_PARTEDIARIO
                    novedad:[ParteDiario getTareaForNovedad:parte.id_nodotareaspartediario
                                                parteDiario:parte.partediario]
                       tipo:kUpdate];
        }
    }
    
}

- (NSUInteger)newRecID
{
    NSUInteger result = 0;
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *where = [NSString stringWithFormat:@"nodo = %@ and medico = %@ and visitador = %@",
                       nodo, self.medico.recID, nodo];
    
    result = [[DB getInstance] getNewIDForTable:@"med_visitas"
                                    idNameOrNil:@"id"
                                     whereOrNil:where];
    
    return result;
}



//#pragma mark -
//#pragma mark No Visita methods
//- (void)guardarNuevaNoVisita
//{
//    // ALTA
//    
//    // Modificacion del estado del medico
//    self.medico.idEstado          = [EstadoVisita getByID:self.idTipo].idEstado;
//    self.medico.actividad         = self.actividad;
//    [self.medico updateVistaData];
//    
//    
//    // Save the new visit into the DB
//    [self save];
//    
//    // Novedad
//    [Novedad insert:@"med_visitas" novedad:[self getForNovedad] tipo:kInsert];
//}
@end
