//
//  UIView+extensions.m
//  APM4
//
//  Created by Juan Pablo Garcia on 20/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "UIView+extensions.h"

@implementation UIView (extensions)

-(void)setHiddenAnimated:(BOOL)hide
{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^
     {
         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
         if (hide)
             self.alpha=0;
         else
         {
             self.hidden= NO;
             self.alpha=1;
         }
     }
                     completion:^(BOOL b)
     {
         if (hide)
             self.hidden= YES;
     }
     ];
}

@end
