//
//  prescDetailViewController.m
//  APM4
//
//  Created by Laura Busnahe on 2/28/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "prescDetailViewController.h"

@interface prescDetailViewController ()

@end

@implementation prescDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
