//
//  Cargo.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cargo : NSObject

@property(nonatomic, assign) NSInteger idCargo;
@property(nonatomic, retain) NSString *descripcion;

+(NSArray*) GetAll;


@end
