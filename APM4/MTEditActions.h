//
//  MTEditActions.h
//  APM4
//
//  Created by Laura Busnahe on 3/7/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kMTEAAdd,
    kMTEAChange,
    kMTEADelete,
    kMTEANone
} MTEditActionsType;


@interface MTEditActions : NSObject

@end
