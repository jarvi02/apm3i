//
//  NoVisitaPorMotivoHeader.h
//  APM4
//
//  Created by Laura Busnahe on 9/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NoVisitaPorMotivoHeader;

typedef enum
{
    fNoVisitaPorMotivoFieldMotivo,
    fNoVisitaPorMotivoFieldManiana,
    fNoVisitaPorMotivoFieldTarde,
    fNoVisitaPorMotivoFieldTotal
} tNoVisitaPorMotivoHeaderField;

@protocol MTNoVisitaPorMotivoHeaderDelegate <NSObject>
@required
// Sent when the user tap a field.
- (void)NoVisitaPorMotivoHeader:(NoVisitaPorMotivoHeader*)header
              selectedField:(tNoVisitaPorMotivoHeaderField)field
                      Order:(NSComparisonResult)order;
@end


@interface NoVisitaPorMotivoHeader : UIView

@property (nonatomic, assign) id <MTNoVisitaPorMotivoHeaderDelegate> delegate;

@property (retain, nonatomic) IBOutlet UIButton *lblMotivo;
@property (retain, nonatomic) IBOutlet UIButton *lblManiana;
@property (retain, nonatomic) IBOutlet UIButton *lblTarde;
@property (retain, nonatomic) IBOutlet UIButton *lblTotal;

- (void)orderBy:(tNoVisitaPorMotivoHeaderField)field sender:(id)sender reset:(BOOL)reset;

- (IBAction)tapLabel:(id)sender;

@end
