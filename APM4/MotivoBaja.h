//
//  MotivoBaja.h
//  APM4
//
//  Created by Juan Pablo Garcia on 27/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MotivoBaja : NSObject

@property(nonatomic, retain) NSString *motivo;
@property(nonatomic) NSInteger motivoId;

- (NSString*)getDescription;
+(NSArray*) GetAll;
+ (MotivoBaja*)getFirst;

@end
