//
//  ServiceDownloadData.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 01/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"

@interface ServiceDownloadData : Service

@property (nonatomic) unsigned long long        dataLenght;

@property(nonatomic, assign) UIProgressView *progressView;

- (NSString*) donwload:(NSString*)urlBase;

@end
