//
//  MTstdTotales.h
//  APM4
//
//  Created by Laura Busnahe on 7/16/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct {
    NSInteger   maniana;
    NSInteger   tarde;
    NSInteger   total;
} sTotalesValores;

typedef struct {
    double   maniana;
    double   tarde;
    double   total;
} sfTotalesValores;


typedef struct {
    sTotalesValores programados;
    sTotalesValores vistos;
    sfTotalesValores porcenataje;
    sfTotalesValores media;
} sTotalesFijosRevisitas;

typedef struct {
    sTotalesValores visitados;
    sTotalesValores revisitas;
} sTotalesFluctuantes;

typedef struct {
    sTotalesValores visitados;
    sfTotalesValores media;
} sTotalesTotal;


@interface MTstdTotales : NSObject

@property (nonatomic, assign)sTotalesValores        periodosTrabajados;
@property (nonatomic, assign)sTotalesFijosRevisitas fijos;
@property (nonatomic)sTotalesFijosRevisitas revisitas;
@property (nonatomic)sTotalesFluctuantes    fluctuantes;
@property (nonatomic)sTotalesTotal          total;

- (void)updateValues;


+ (MTstdTotales*)getTotales;
@end
