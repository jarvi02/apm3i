//
//  DetalleParteViewController.m
//  APM4
//
//  Created by Laura Busnahe on 7/22/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "DetalleParteViewController.h"
#import "MTstdDetalleParte.h"

@implementation DetalleParteViewController
{
    NSString *descTitulo;
    
    NSInteger fechaSeleccionada;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        descTitulo = @"Detalle de Parte Diario";
        self.title = descTitulo;
        
        self.cellNib    = [UINib nibWithNibName:@"detalleParteCell" bundle:nil];
        
        self.headerNib  = [UINib nibWithNibName:@"detalleParteHeader" bundle:nil];
        [self.headerNib instantiateWithOwner:self options:nil];
        
        self.data = nil;

        fechaSeleccionada = 0;

    }
    return self;
}

- (id)initViewDefaultNib
{
    self = [self initWithNibName:@"DetalleParteViewController" bundle:nil];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = descTitulo;

    self.lblFecha.text = @"";
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Do all the load shit here
    
    self.arrayCombo = [MTstdDetalleParte getFechasParte];
    self.lblFecha.text = @"Sin datos";
    if ([self.arrayCombo count] > 0)
    {
        self.lblFecha.text = (NSString*)[self.arrayCombo objectAtIndex:0];
        [self updateData];
    }
    
    // ------
    
    [self.loadingView hide];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Overrides

-(BOOL)hasFooter
{
    return YES;
}

-(NSString *)getTitleForHeader
{
    return descTitulo;
}

-(UIImage *)getIconoForHeader
{
    return [UIImage imageNamed:@"cabecera_principal_32"];
}

#pragma mark -
#pragma mark Tap methods
- (IBAction)tapBtnFecha:(id)sender
{
    UIView *popRectView = (UIView*)sender;
    
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(200, 350);
    
    // Creo el objeto, asigno el título, el listado de datos, y defino que utilizará búsqueda.
    //if (!self.popupMenu)
    //{
        self.popupMenu = [[MTpopListView alloc] initWithRect:rect];
    //}
    [self.popupMenu dismissPopoverAnimated:NO];
    
    self.popupMenu.delegate = self;
    
    [self.popupMenu setTitleAndParameters:@"Período" rowData:self.arrayCombo];
    [self.popupMenu setSize:rect.size];
    
    [self.popupMenu useSearch:NO];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x,
                               popRectView.frame.origin.y, // + 64,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:sender
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionUp
                                            animated:YES];
    
    
}


#pragma mark -
#pragma mark tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"detalleParteCelldentifier";
    

    // Obtain the cell object.
    detalleParteCell *cell = (detalleParteCell*)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];

    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
    	cell = self.tmpCell;
    	self.tmpCell = nil;
    }
    
    // Obtengo los datos para la celada.
    MTstdDetalleParte *o = [self.data objectAtIndex:indexPath.row];
    
    // Celdas de Orden
    cell.lblFecha.text =          [NSString string:o.fecha ifNull:@""];
    cell.lblActividad.text =      [NSString string:o.actividad ifNull:@""];
    cell.lblApellidoNombre.text = [NSString string:o.nombre ifNull:@""];
    cell.lblDescripcion.text =    [NSString string:o.descripcion ifNull:@""];

    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
    {
        return self.tableHeader;
    }
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader.frame.size.height;
    else
        return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

#pragma mark -
#pragma mark Actualización de datos
- (void)updateData
{
    
    NSDate *fecha = nil;
    
    if (fechaSeleccionada > 0)
    {
        NSString *o = [self.arrayCombo objectAtIndex:fechaSeleccionada];
        
        fecha = [NSDate stringToTime:o withFormat:@"dd/MM/yyyy"];
    }
    
    // Cargo los datos de la estadística.
    self.data = [MTstdDetalleParte getForFechaOrNil:fecha];
    
    // Actalizo los datos de la tabla.
    [self.tableView reloadData];
    [self.loadingView hide];
}


#pragma mark -
#pragma mark popListView delegate methods,

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender didSelectRow:(NSInteger *)MenuIndex selectedText:(NSString *)RowText
{
    if (sender == self.btnFecha)
    {
        fechaSeleccionada = MenuIndex;
        self.lblFecha.text = RowText;
        //[self.popupMenu dismissPopoverAnimated:YES];
        
        //fechaSeleccionada ++;
        [self updateData];
        return;
    }
}

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender willSelectObject:(id)selObject
{
    [self.loadingView show:self];
}

@end
