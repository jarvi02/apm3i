//
//  Medico.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Potencial.h"
#import "Tratamiento.h"

typedef NS_ENUM(NSInteger, ABMMedicoModo) {
    ABMMedicoModoAlta,
    ABMMedicoModoModificar
};

@interface Medico : NSObject

@property(nonatomic, assign) ABMMedicoModo  modo;

@property(nonatomic, retain) NSString       *apellido;
@property(nonatomic, retain) NSString       *nombre;
@property(nonatomic, retain) NSString       *actividad;       // M/T
@property(nonatomic, retain) Potencial      *potencial;
@property(nonatomic, retain) NSMutableArray *especialidades;
@property(nonatomic, retain) NSMutableArray *domicilios;
@property(nonatomic, retain) NSMutableArray *obrasSociales;
@property(nonatomic, retain) NSString       *sexo;            //M/F
@property(nonatomic, retain) Tratamiento    *tratamiento;
@property(nonatomic, retain) NSDate         *fechaNacimiento;
@property(nonatomic, retain) NSString       *anioEgreso;
@property(nonatomic, retain) NSString       *CUIT;
@property(nonatomic, retain) NSString       *email;
@property(nonatomic, retain) NSString       *revisita;
@property(nonatomic, retain) NSString       *matriculaNacional;
@property(nonatomic, retain) NSString       *matriculaProvincial;

@property(nonatomic) NSUInteger idMedico;
@property(nonatomic) NSUInteger idEstado;
@property(nonatomic) NSUInteger visitasRealizadas;
@property(nonatomic) NSUInteger visitasProgramadas;


+(Medico*) Medico;
+(Medico*) GetById:(NSUInteger) idMedico;
-(void)save:(NSString *)objetivos;
+(NSString *)getForNovedad:(NSUInteger) idMedico;
-(void)saveAllData;
-(NSString*)isOkForSave;
-(void)updateAllData;

@end
