//
//  StockMMViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "StockMMViewController.h"
#import "StockMMCell.h"
#import "StockMM.h"
#import "NSDate+extensions.h"

@interface StockMMViewController ()

@end

@implementation StockMMViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Stock MM";
    }
    return self;
}

- (void)dealloc {
    
    [_data      release];
    
    [_tmpCell   release];
	[_cellNib   release];
    
    [_table     release];
    
    [super dealloc];
}

-(NSString *)getTitleForHeader{
    return @"Stock MM";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"cartera_48"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.table.rowHeight = 44.0;
    

    self.data = [StockMM GetAll];
	
    
	self.cellNib = [UINib nibWithNibName:@"StockMMCell" bundle:nil];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Table Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of time zone names.
	return [self.data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MyIdentifier = @"StockMMdentifier";
	
    // Obtain the cell object.
	StockMMCell *cell = (StockMMCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	// If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
		cell = self.tmpCell;
		self.tmpCell = nil;
        
        
    }
    
    StockMM *o = [self.data objectAtIndex:indexPath.row];
    
   

    UIColor *textColor = [UIColor blackColor];
  
    // Chequeo si el producto está vencido.
    //[o checkProducto];
    if (o.vencido)
        textColor = [UIColor redColor];
    
    cell.lbProducto.text = o.descripcion;
    cell.lbProducto.textColor = textColor;
    cell.lbCantidad.text = [NSString stringWithFormat:@"%d", o.stock];
    cell.lbCantidad.textColor = textColor;
    cell.lbCondigo.text = o.codigo;
    cell.lbCondigo.textColor = textColor;
    cell.lbLote.text = o.lote;
    cell.lbLote.textColor = textColor;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:o.fecha_vto];
    cell.lbFecha.text = [date formattedStringUsingFormat:@"MM/yyyy"];
    cell.lbFecha.textColor = textColor;
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGRect frame = CGRectMake(0, 0, 768, 22);
    UILabel *background = [[UILabel alloc] initWithFrame:frame];
    background.autoresizingMask = 34;
    background.backgroundColor = UIColorFromRGB(defaultHeaderCellColor);
    
    UILabel *label;
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(20, 0, 400, 20);
    label.autoresizingMask = 38;
    label.text = @"Producto";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(421, 0, 69, 20);
    label.autoresizingMask = 37;
    label.text = @"Cantidad";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(491, 0, 93, 20);
    label.autoresizingMask = 37;
    label.text = @"Codigo";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(585, 0, 76, 20);
    label.autoresizingMask = 37;
    label.text = @"Lote";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(662, 0, 86, 20);
    label.autoresizingMask = 33;
    label.text = @"Vto.";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    return [background autorelease];
}


@end
