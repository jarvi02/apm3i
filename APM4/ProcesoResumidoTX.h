//
//  ProcesoResumidoTX.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 21/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ACCIONTX) {
    INICIALIZACION,
    CIERRE_CICLO,
    NOVEDADES
};

@interface ProcesoResumidoTX : NSObject

//-(void)start;

@end
