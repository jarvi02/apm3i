//
//  PrevistoMensualViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 08/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "PrevistoMensualViewController.h"
#import "NSDateExtended.h"
#import "PrevistoMensual.h"
#import "PrevistoMSeleccionView.h"
#import "MTEditActions.h"
#import "MTPrevistoActividad.h"
#import "CustomPrevistoCell.h"
#import "Ciclo.h"
#import "MTFeriado.h"
#import "ConfigGlobal.h"
#import "AppDelegate.h"
#import "EntryForCombo.h"

#import "GlobalDefines.h"

// TODO: Solo mostrar previsto mensual si está activado en la tabla de configuración.


@interface PrevistoMensualViewController ()

//-(NSArray *)createListFrom:(NSDate *)from To:(NSDate *)to;
-(NSArray *)fillSelected:(NSArray *)all;

@property(nonatomic, retain) NSArray *ciclos;

@end

@implementation PrevistoMensualWrapper

@synthesize inner;
@synthesize selected;

-(id)initWithObject:(PrevistoMensual *)previsto selected:(BOOL)sel
{
    self = [super init];
	if ( self ){
        self.inner = previsto;
        self.selected = sel;
	}
	return self;
}

@end

@implementation PrevistoMensualViewController

@synthesize tableView;
@synthesize previstos;

#pragma mark -
#pragma mark LyfeCicle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"Previsto Mensual";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    // Obtiene el listado de ciclos.
    self.ciclos = [Ciclo GetAll];
    
    // Selecciono el ciclo actual.
    cicloSeleccionado = [Ciclo actual];
    dateFrom = cicloSeleccionado.desde;
    dateTo   = cicloSeleccionado.hasta;
    self.txtCiclo.text = cicloSeleccionado.ciclo;
    
    // Cell
    self.cellNIB = [UINib nibWithNibName:@"CustomPrevistoCell" bundle:nil];
    
    // TableView
    CGRect frame = CGRectMake(0, 120, 1024.0f, 750.0f);
    self.tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.tableView];
    
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
    
//}
//
//- (void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
    // Load array
    self.previstos = nil;
    if (cicloSeleccionado > 0)
    {
        //NSArray *allDates = [self createListFrom:dateFrom To:dateTo];
        NSArray *allDates = [self listadoDiasDesde:dateFrom hasta:dateTo];
        self.previstos = [self fillSelected:allDates];
    }
    
   
    
    // Cantidad mínima de días futuros para la carga del previsto en horas.
    if ([[ConfigGlobal shareInstance] isValidKey:@"AGENDAMENSUAL_DIAS"])
        self.diasFuturosPrevisto = [[[ConfigGlobal shareInstance] findValue:@"AGENDAMENSUAL_DIAS"] integerValue];
    else
        self.diasFuturosPrevisto = 1;
    
    // Solo DEBUG.
//    NSLog(@"AGENDAMENSUAL_DIAS = %@",[[ConfigGlobal shareInstance] findValue:@"AGENDAMENSUAL_DIAS"]);
    
    [self.tableView reloadData];
    [self.loadingView hide];
}

#pragma mark -
#pragma mark Overrides

-(BOOL)hasFooter
{
    return YES;
}

-(NSString *)getTitleForHeader{
    return @"Previsto Mensual";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"user_add"];
}

#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger result = 1;
   
    if ([self.previstos count] >= 2)
        result = [self.previstos count] / 2;
    
    return result;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] init];
    label.text = @" Previsto";
    
   
    NSDateFormatter *frm = [[NSDateFormatter alloc] init];
    PrevistoMensualWrapper *obj = [self.previstos objectAtIndex:(section*2)];
    
    [frm setDateStyle:NSDateFormatterFullStyle];
    label.text = [[NSString stringWithFormat:@"  %@", [NSString string:[frm stringFromDate:obj.inner.date] ifNull:@""]] uppercaseString];
    
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont boldSystemFontOfSize:16.0f];
    label.backgroundColor = UIColorFromRGB(defaultHeaderCellColor);

    return label;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PrevistoMensualWrapper *obj = [self.previstos objectAtIndex:(indexPath.section*2 + indexPath.row)];
    
    NSDate *dateNow = [[NSDate date] addDays:self.diasFuturosPrevisto];
    NSComparisonResult dateCompare = [obj.inner.date  compare:dateNow];

    NSString *sDate = @"";
    NSDateFormatter* formatedDate = [[NSDateFormatter alloc] init];
       [formatedDate setDateFormat:@"d 'de' "];
    sDate = [sDate stringByAppendingString:[formatedDate stringFromDate:dateNow]];
    [formatedDate setDateFormat:@"MMMM "];
    sDate = [sDate stringByAppendingString:[[formatedDate stringFromDate:dateNow] capitalizedString]];
    [formatedDate setDateFormat:@"'de' yyyy"];
    sDate = [sDate stringByAppendingString:[formatedDate stringFromDate:dateNow]];
    
    
    if (dateCompare == NSOrderedDescending)
    {
        [self navigateToPrevistoMSeleccion:obj.inner objectIndex:(indexPath.section*2 + indexPath.row)];
       
    } else
    {
        [self showAlert:[NSString stringWithFormat:@"La fecha del previsto debe ser mayor al %@", sDate]
                 Titulo:@"Error"];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight;
    
    PrevistoMensualWrapper *obj = [self.previstos objectAtIndex:(indexPath.section*2 + indexPath.row)];
    
    cellHeight = 58;
    if ((obj.inner.activity.entidad.direccion == nil) ||
        ([obj.inner.activity.entidad.direccion length] == 0))
    {
        cellHeight = 44;
    }
    
    return (cellHeight);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    CustomPrevistoCell *cell = (CustomPrevistoCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	// If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNIB instantiateWithOwner:self options:nil];
		cell = self.tmpCell;
		self.tmpCell = nil;
    }
    
    
    
    
    // Configure the cell...
    PrevistoMensualWrapper *obj = [self.previstos objectAtIndex:(indexPath.section*2 + indexPath.row)];
    
    NSString *sDate = @"";
    NSDateFormatter* formatedDate = [[NSDateFormatter alloc] init];
    [formatedDate setDateFormat:@"d 'de' "];
    sDate = [sDate stringByAppendingString:[formatedDate stringFromDate:obj.inner.date]];
    [formatedDate setDateFormat:@"MMMM "];
    sDate = [sDate stringByAppendingString:[[formatedDate stringFromDate:obj.inner.date] capitalizedString]];
    [formatedDate setDateFormat:@"'de' yyyy"];
    sDate = [sDate stringByAppendingString:[formatedDate stringFromDate:obj.inner.date]];
    
    if (indexPath.row == 0)
        cell.lblFechaTurno.text = [NSString stringWithFormat: @"Mañana  %@" ,sDate] ;


    else
        cell.lblFechaTurno.text = [NSString stringWithFormat: @"Tarde      %@" , sDate] ;
    
    NSDate *dateNow = [[NSDate date] addDays:self.diasFuturosPrevisto];
    NSComparisonResult dateCompare = [obj.inner.date  compare:dateNow];
    if (dateCompare == NSOrderedDescending)
        cell.lblFechaTurno.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
    else
        cell.lblFechaTurno.textColor = UIColorFromRGB(defaultCellFontColor);
        
    
    cell.lblActividad.text  = @"";
    cell.lblDireccion.text  = @"";
    
    if (obj.inner.activity != nil)
    {
//        NSDateFormatter* formatedDate = [[NSDateFormatter alloc] init];
//        [formatedDate setDateFormat:@"HH:mm"];
        
        cell.imgCell.image = [UIImage imageNamed:@"ok_32.png"];
        cell.lblActividad.text = [obj.inner.activity getDescription];
        cell.lblDireccion.text = [obj.inner.activity getDireccion];
        
    } else
    {
        cell.imgCell.image = [UIImage imageNamed:@"ok_32_off.png"];
        [cell.lblFechaTurno setFrame:CGRectMake(cell.lblFechaTurno.frame.origin.x,
                                                11,
                                                cell.lblFechaTurno.frame.size.width,
                                                cell.lblFechaTurno.frame.size.height)];
    }
    
    // Chequeo si la actividad tiene dirección.
    if ((cell.lblDireccion.text == nil) ||
        ([cell.lblDireccion.text length] == 0))
    {
        [cell.imgCell setFrame:CGRectMake(cell.imgCell.frame.origin.x,
                                          6,
                                          32,
                                          32)];
    }

    
    return cell;
}


#pragma mark-
#pragma mark UIAlertView related methods

- (void)showAlert:(NSString*)mensaje Titulo:(NSString*) titulo
{
    // Llama al AlertView de confirmación antes de eliminar el previsto seleccionado.
   	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titulo
                                                    message:mensaje
                                                   delegate:nil
                                          cancelButtonTitle:@"Aceptar"
                                          otherButtonTitles:nil];
	[alert show];
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // buttonIndex = 0 : No
    
    // buttonIndex = 1 : Si
}


#pragma mark -
#pragma mark Private Methods


-(NSArray *)listadoDiasDesde:(NSDate *)from hasta:(NSDate *)to
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    // Obtengo todo el listado de feriados.
    NSArray *feriadosList = [MTFeriado getAll];
    
    while ([from compare:to] != NSOrderedDescending)
    {
        BOOL visitasFeriadoFinde = TRUE;
        
        // Busca la configuración para saber si el APM hace visitas los fines de semana.
        if ([[ConfigGlobal shareInstance] isValidKey:VIS_FINDESEMANA])
            visitasFeriadoFinde = [[ConfigGlobal shareInstance] find:VIS_FINDESEMANA];
        
        BOOL esSabadoDomingo = NO;
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"e"];
        
        NSString* strWeekDay = [from formattedStringUsingFormat:@"e"];
        
        if (([strWeekDay isEqual:@"1"]) ||         // Domingo
            ([strWeekDay isEqual:@"7"]))           // Sábado
            esSabadoDomingo = YES;
        
        // Chequeo que el día no sea feriado ni fin de semana y agrego la fecha al listado.
        if ((![MTFeriado isDate:from intoArray:feriadosList]) &&
            (!esSabadoDomingo))
        {
            PrevistoMensual *pm1 = [PrevistoMensual getForFecha:from Tipo:kMorning];
            PrevistoMensual *pm2 = [PrevistoMensual getForFecha:from Tipo:kAfternoon];
            [result addObject:pm1];
            [result addObject:pm2];
        } else
        // Chequeo si el APM visita los fines de semana y feriados, si es así también agrego el día al listado.
        if (visitasFeriadoFinde)
        {
            PrevistoMensual *pm1 = [PrevistoMensual getForFecha:from Tipo:kMorning];
            PrevistoMensual *pm2 = [PrevistoMensual getForFecha:from Tipo:kAfternoon];
            [result addObject:pm1];
            [result addObject:pm2];
        }
        from = [from addDays:1];
    }
    
    return result;
}


-(NSArray *)fillSelected:(NSArray *)all
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[all count]];
    for(PrevistoMensual *current in all)
    {
        PrevistoMensualWrapper *wrp = [[PrevistoMensualWrapper alloc] initWithObject:current selected:NO];
        
        [result addObject:wrp];
      
    }
    
    return result;
}

#pragma mark -
#pragma mark IBActions

- (IBAction)ciclo:(id)sender {
    

    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    NSMutableArray *entries = [[NSMutableArray alloc] initWithCapacity:0];
    EntryForCombo *e;
    
    for (int i=0; [self.ciclos count] > i; i++) {
        
        Ciclo *o = [self.ciclos objectAtIndex:i];
        
        e = [[EntryForCombo alloc] initWithValue:o.ciclo forIndex:i];
        
        [entries addObject:e];
    }
    
    
    CGSize size = CGSizeMake(200, 500);
    TableForComboPopoverViewController *t = [[TableForComboPopoverViewController alloc] initWithData:entries titulo:@"Ciclos" size:size delegate:self andCaller:@"Ciclos"];
    t.aligment = NSTextAlignmentLeft;
    
    
    Delegate.popoverController = [[UIPopoverController alloc] initWithContentViewController:t];
    
    [Delegate.popoverController presentPopoverFromRect: ((UIButton*)sender).frame inView:self.view permittedArrowDirections:YES animated:YES];
    
}

-(IBAction)pressedSave:(id)sender
{
    for (PrevistoMensualWrapper *o in self.previstos)
    {
        [o.inner dbPerformAction];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)pressedCancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Combo delegates methods

-(void)selectedIndex:(NSInteger)index caller:(NSString*)caller {
    
    [Delegate dismissPopOver:YES];
    
    if ([caller isEqualToString:@"Ciclos"]) {
        
        cicloSeleccionado = [self.ciclos objectAtIndex:index];;
        self.txtCiclo.text = cicloSeleccionado.ciclo;
        
    
        // aca hacer lo que haga falta con el ciclo
    
        
        dateFrom = cicloSeleccionado.desde;
        dateTo   = cicloSeleccionado.hasta;
        
        // Load array
        //NSArray *allDates = [self createListFrom:dateFrom To:dateTo];
        NSArray *allDates = [self listadoDiasDesde:dateFrom hasta:dateTo];
        self.previstos = [self fillSelected:allDates];

        [tableView reloadData];
        
    } else  {
        // jamas deberia entrar aca
    }
    
}

#pragma mark -
#pragma mark Previsto Mensual - Edit View

-(void)navigateToPrevistoMSeleccion:(PrevistoMensual*) previsto objectIndex:(NSInteger) oIndex;
{
    PrevistoMSeleccionView *pmsc = [[PrevistoMSeleccionView alloc] initViewWithPrevisto:previsto
                                                                                    Data:previsto.activity
                                                                             objectIndex:oIndex];

    pmsc.modalPresentationStyle = UIModalPresentationFormSheet;
    pmsc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    pmsc.delegate = self;
    [self.navigationController presentViewController:pmsc animated:YES completion:NULL];
   
}


#pragma mark -
#pragma mark Previsto Mensual - Edit Delegate           
-(void)pmEditPerformAction:(PrevistoMSeleccionView*)controller Action:(MTEditActionsType)action withData:(MTPrevistoActividad*)activity objectIndex:(NSInteger)selectionIndex
{
    PrevistoMensualWrapper* o = nil;
    if (selectionIndex >= 0)
    {
       o = [self.previstos objectAtIndex:selectionIndex];
    }
    
    // Chequeo que acción se tomó en la vista de edición.
    switch (action)
    {
        // Agreagr nueva actividad.
        case kMTEAAdd:
            if (o.inner.action == kMTEADelete)
            {
                o.inner.action = kMTEAChange;
                o.inner.activity = activity;
            } else
            {
                o.inner.action   = kMTEAAdd;               // Guardo la acción realizda.
                o.inner.activity = activity;
            }
            break;
            
        // Modificar la actividad existente.
        case kMTEAChange:
            if ((o.inner.action == kMTEANone) || (o.inner.action == kMTEADelete))
            {
                if (o.inner.action != kMTEADelete)
                    o.inner.oldActivity = o.inner.activity; // Guardo los datos viejos de la actividad
                
                o.inner.action      = kMTEAChange;          // Guardo la acción realizda.
            }
            o.inner.activity = activity;         // Asigno la nueva actividad.
            break;
            
        // Eliminar la actividad.
        case kMTEADelete:
            if (o.inner.action == kMTEAAdd)
            {
                o.inner.action      = kMTEANone;
            } else
            {
                o.inner.action      = kMTEADelete;
                o.inner.oldActivity = o.inner.activity;
            }
            
            o.inner.activity    = nil;
            break;
        
        // Solo DEBUG.
        // ---------------------------------------------------------            
//        case kMTEANone:
//            o.inner.action   = kMTEANone;
//            break;
        // ---------------------------------------------------------

        default:
            
            break;
    }

    
    
    if (action != kMTEANone)
        [tableView reloadData];
    
    
#ifdef DEBUG_PREVISTOM
    // Solo DEBUG.
    // ---------------------------------------------------------
    NSLog(@"--- PREVISTO EDIT ---");
    NSLog(@"action: %@",
                      action == kMTEAAdd? @"Add":
                      action == kMTEAChange? @"Modify":
                      action == kMTEADelete? @"Delete":
                      @"None");
    
    action = o.inner.action;
    NSLog(@"obj action: %@",
                      action == kMTEAAdd? @"Add":
                      action == kMTEAChange? @"Modify":
                      action == kMTEADelete? @"Delete":
                      @"None");
    
    NSLog(@"data: %@",
          activity == nil? @"-":
          activity.getDescription);
    
    NSLog(@"obj data: %@",
                      o.inner.activity == nil? @"-":
                      o.inner.activity.getDescription);
    
    NSLog(@"old data: %@",
          o.inner.oldActivity == nil? @"-":
          o.inner.oldActivity.getDescription);
    // ---------------------------------------------------------
#endif
}

@end
