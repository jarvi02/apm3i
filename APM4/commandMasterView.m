//
//  commandMasterView.m
//  APM4
//
//  Created by Laura Busnahe on 7/31/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "commandMasterView.h"
#import "CommandButton.h"


@interface commandMasterView ()
{
    
    
}

- (void)setButtonFrames;
- (void)animateButtonFramesToState:(AppBarState)appBarState;

@end

@implementation commandMasterView


static commandMasterView *_sharedInstance = nil;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        //_buttonContainer  = [[NSMutableDictionary alloc] init];
        
        
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    
    // Add function calls for buttons.
    for (CommandButton *button in [_buttonContainer objectForKey:_currentGroup])
    {
        if (button.containsMenuList)
        {
            [button addTarget:self action:@selector(openMenuList:) forControlEvents:UIControlEventTouchUpInside];
        } else
        {
            [button addTarget:self action:@selector(buttonWasSelected:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        [self addSubview:button];
    }
}
 
+ (commandMasterView *)sharedInstance
{
    if(!_sharedInstance) {
		static dispatch_once_t oncePredicate;
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[commandMasterView alloc] init];
        });
    }
    return _sharedInstance;
}

- (id)init
{
    NSLog(@" - Inicilizando Vista...");
    
    self = [super init];
    
    if (self)
    {
        self.viewNib = [UINib nibWithNibName:@"commandMasterVC" bundle:nil];
        [self.viewNib instantiateWithOwner:self options:nil];
        
        self = self.viewReference;
        
        //self.backgroundColor = [UIColor colorWithRed:0.129 green:0.125 blue:0.129 alpha:1.0];
        _buttonContainer = [[NSMutableDictionary alloc] init];
        _autoHide = YES;
        _showButtonTitles = YES;
        
        self.autoHide         = YES;
        self.showButtonTitles = YES;
        
        self.minimalHeight  = 30;
        //self.minimalHeight  = 2;
        self.fullHeight     = 70;
        self.maxHeight = 800;
        self.circleSize     = 3;
        
        self.animationDuration = 0.3f;
        
        self.cellHeight    = 55;
        self.buttonSpacing = 10;
        
        self.tableView.showsVerticalScrollIndicator = NO;
    }
    return self;
}

// Adds the bar to the view
- (void)addToView:(UIView *)parent
{
    // When first added, the current state is always Minimal.
    _currentState = AppBarMinimal;
    _parentView   = parent;
    
    [_parentView addSubview:_sharedInstance];
    
    // Add one UITableView, so that it can just be refreshed with different data.
    //self.tableView.backgroundColor              = [UIColor colorWithRed:0.129 green:0.125 blue:0.129 alpha:1.0];
    self.tableView.showsVerticalScrollIndicator = NO;
    //self.tableView.separatorStyle               = UITableViewCellSeparatorStyleNone;
    
    _menuListDataSource = [[NSArray alloc] init];
    
}

// Minimize code footprint, combine above 2 functions
- (void)addToView:(UIView *)parent andLoadGroup:(NSString *)group
{
    [self addToView:parent];
    [self loadGroup:group];
}


#pragma mark -
#pragma mark Group methods

// Loads up the group's icons
- (void)loadGroup:(NSString *)group
{
    _currentGroup = group;
    
    [self showMinimalAppBar];
    [self setNeedsDisplay];
    [self setButtonFrames];
}


#pragma mark -
#pragma mark Buttons methods

// Used for insertion of single button with error checking for group size greater than 4.
- (void)addButton:(CommandButton *)button forGroup:(NSString *)group
{
    NSMutableArray *tempButtons = [[NSMutableArray alloc] init];
    
    if ([_buttonContainer objectForKey:group])
    {
        tempButtons = [_buttonContainer objectForKey:group];
        @try
        {
            if ([tempButtons count] == 4)
            {
                @throw [NSException exceptionWithName:@"CMSizeException"
                                               reason:@"The array of buttons you are trying to pass must be less than 5."
                                             userInfo:nil];
            }
            button.showButtonTitle = _showButtonTitles;
            [tempButtons addObject:button];
        }
        
        @catch (NSException *exception)
        {
            NSLog(@"%@: %@", exception.name, exception.reason);
            return;
        }
        
    } else
    {
        button.showButtonTitle = _showButtonTitles;
        [tempButtons addObject:button];
    }
    
    [_buttonContainer setObject:tempButtons forKey:group];
    [self setButtonFrames];
    
    if (_currentGroup == nil)
    {
        [self loadGroup:group];
    }
}

// This is used so user can add an entire array of buttons (so long as <= 4), and associate it to a name. That way, we can store multiple "pages"
// of icons, and not have to re-init them when we navigate between pages
- (void)addButtons:(NSArray *)buttons forGroup:(NSString *)group
{
    @try
    {
        if ([buttons count] > 4)
        {
            @throw [NSException exceptionWithName:@"CMSizeException"
                                           reason:@"The array of buttons you are trying to pass must be less than 5."
                                         userInfo:nil];
        }
        
        if (group.length == 0 || group == nil) {
            @throw [NSException exceptionWithName:@"CMGroupNameException"
                                           reason:@"The Group Name must not be nil."
                                         userInfo:nil];
        }
        
        if (([[_buttonContainer objectForKey:group] count] + buttons.count) > 4)
        {
            @throw [NSException exceptionWithName:@"CMSizeException"
                                           reason:@"The array of buttons you are trying to add to the already exsiting group must be a total of less than 5"
                                         userInfo:nil];
        }
        
        [buttons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             if (![obj isKindOfClass:[CommandButton class]])
             {
                 @throw [NSException exceptionWithName:@"CMButtonArrayException"
                                                reason:@"The array can only contain CommandButton objects"
                                              userInfo:nil];
             }
             [self addButton:obj forGroup:group];
         }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@: %@", exception.name, exception.reason);
        return;
    }
}

// Set a button within the current group to be enabled/disabled
- (void)buttonForTitle:(NSString *)title setEnabled:(BOOL)enabled
{
    [[_buttonContainer objectForKey:_currentGroup] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
     {
         CommandButton *button = obj;
         if ([button.title isEqualToString:title])
         {
             [button setEnabled:enabled];
             *stop = YES;
         }
     }];
}

- (void)setShowButtonTitles:(bool)showButtonTitles {
    if (showButtonTitles == _showButtonTitles)
    {
        return;
    }
    _showButtonTitles = showButtonTitles;
    
    // Set all buttons within the container to showButtonTitles
    [[_buttonContainer objectForKey:_currentGroup] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
     {
         [obj setShowButtonTitle:_showButtonTitles];
     }];
}


#pragma mark -
#pragma mark State methods

- (void)showMinimalAppBar
{
    
    [UIView animateWithDuration:self.animationDuration animations:^
     {
         self.Frame = CGRectMake(self.frame.origin.x,
                                 self.frame.origin.y,
                                 self.frame.size.width,
                                 self.minimalHeight);
         
         // TODO: reacomodar y estirar imgMiddle
         
         [self animateButtonFramesToState:AppBarMinimal];
     }
                     completion:^(BOOL finished)
     {
         _currentState = AppBarMinimal;
         _currentGroup = nil;
         _selectedButton = nil;
         [self setHidden:YES];
     }];
    
    [self.shpLine setHidden:YES];
    
}

- (void)showIconsAppBar
{
    //NSInteger height = 0;
    
    //height = self.tableView.frame.origin.y + ([_selectedButton.menuListData count] * self.cellHeight);
    
    [self.shpLine setHidden:YES];
    
    [UIView animateWithDuration:self.animationDuration animations:
     ^{
         NSInteger height = self.fullHeight + 30;
         
         self.frame = CGRectMake(self.frame.origin.x,
                                 self.frame.origin.y,
                                 self.frame.size.width,
                                 height);
         
         // TODO: reacomodar y estirar imgMiddle
         
         [self animateButtonFramesToState:AppBarIcons];
         
         NSLog(@" - height = %d", height);
     }
                     completion:^(BOOL finished)
     {
         _currentState = AppBarIcons;
     }];
}

- (void)showFullList
{
    if (_selectedButton.menuListData == nil)
    {
        return;
    }
    
    [self.shpLine setHidden:NO];
    
    NSInteger height = 0;
    height = self.tableView.frame.origin.y + ([_selectedButton.menuListData count] * self.cellHeight);
    
    // No need to scroll if the list is less than 4
    if (height  > self.maxHeight)
    {
        self.tableView.scrollEnabled = YES;
    } else
    {
        self.tableView.scrollEnabled = NO;
    }
    
    if (self.tableView.scrollEnabled)
        height = self.maxHeight;

   
    [UIView animateWithDuration:self.animationDuration
                     animations:
     ^{
         self.frame = CGRectMake(self.frame.origin.x,
                                 self.frame.origin.y,
                                 self.frame.size.width,
                                 height+5);

         self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,
                                           self.tableView.frame.origin.y,
                                           self.tableView.frame.size.width,
                                           ([_selectedButton.menuListData count] * self.cellHeight));
      
         // TODO: reacomodar y estirar imgMiddle
         
         NSLog(@" - Full List height = %d", height);
         NSLog(@" - tableView height = %.0f", self.tableView.frame.size.height);
     }
                     completion:^(BOOL finished)
     {
         _currentState = AppBarFullList;
     }];
}


#pragma mark -
#pragma mark Private methods

- (void)setButtonFrames
{
    float width = 0;
    float height = 0;
    CommandButton *button = nil;
    
    int buttonsCount = [[_buttonContainer objectForKey:_currentGroup] count];
    
    if (buttonsCount > 0)
    {
        button = [_buttonContainer objectForKey:_currentGroup][0];
        
        height = button.frame.size.height;
        width  = button.frame.size.width;
    }
    
    float viewWidth = self.frame.size.width;
    //float viewHeight = self.view.frame.size.height;
    
    for (int i = 0; i < buttonsCount; i++)
    {
        button = [_buttonContainer objectForKey:_currentGroup][i];
        
        button.frame = CGRectMake(viewWidth/2 - ((width + self.buttonSpacing) * buttonsCount /2) + ((width + self.buttonSpacing) *i),
                                  self.frame.origin.y,
                                  width,
                                  self.fullHeight + 30);
    }
    
}

- (void)animateButtonFramesToState:(AppBarState)appBarState
{
    CommandButton *button = nil;
    float width = 0;
    float height = 0;
    float viewWidth = self.frame.size.width;
    
    
    int buttonsCount = [[_buttonContainer objectForKey:_currentGroup] count];
    
    if (buttonsCount > 0)
    {
        button = [_buttonContainer objectForKey:_currentGroup][0];
        
        height = [button frame].size.height;
        width = [button frame].size.width;
    }
    
    
    if (appBarState == AppBarIcons)
    {
        for (int i = 0; i < buttonsCount; i++)
        {
            button = [_buttonContainer objectForKey:_currentGroup][i];
            
            button.frame = CGRectMake(viewWidth/2 - ((width + self.buttonSpacing) * buttonsCount /2) + ((width + self.buttonSpacing) *i),
                                      self.frame.origin.y,
                                      width,
                                      height);
        }
    } else
        
    if (appBarState == AppBarMinimal)
    {
        for (int i = 0; i < buttonsCount; i++)
        {
            button = [_buttonContainer objectForKey:_currentGroup][i];
            button.frame = CGRectMake(viewWidth/2 - ((width + self.buttonSpacing) * buttonsCount /2) + ((width + self.buttonSpacing) *i),
                                          (- height - self.minimalHeight),
                                          width,
                                          height);
        }
    }
}


- (void)openMenuList:(CommandButton *)sender
{
    CommandButton *previousSelected = _selectedButton;
    
    _selectedButton = sender;
    
    switch (_currentState)
    {
        case AppBarMinimal:
        case AppBarIcons:
        {
            _menuListDataSource = _selectedButton.menuListData;
            [self.tableView reloadData];
            [self showFullList];
            
            break;
        }
            
        case AppBarFullList:
        {
            if (previousSelected != _selectedButton && previousSelected.containsMenuList)
            {
                NSInteger buttonsCount = [_selectedButton.menuListData count];
                float viewHeight;
                
                viewHeight = ((buttonsCount * self.cellHeight) + self.tableView.frame.origin.y + 5);
                
                if (viewHeight > self.maxHeight)
                    viewHeight = self.maxHeight;
                
                
                [UIView animateWithDuration:self.animationDuration
                                 animations:
                 ^{
                     _menuListDataSource = _selectedButton.menuListData;
                     [self.tableView reloadData];
                     
                     self.frame = CGRectMake(self.frame.origin.x,
                                             self.frame.origin.y,
                                             self.frame.size.width,
                                             viewHeight);
                     
                     // TODO: reacomodar y estirar imgMiddle
                     
                     [self animateButtonFramesToState:AppBarIcons];
                 }
                 
                                 completion:^(BOOL finished)
                 {
                     [self showFullList];
                 }];
                
            } else
                
                if (previousSelected == _selectedButton)
                {
                    [self showIconsAppBar];
                }
            break;
        }
    }
}

- (void)buttonWasSelected:(CommandButton *)sender {
    _selectedButton = sender;
    switch (_currentState) {
        case AppBarMinimal:
        case AppBarIcons:
        case AppBarFullList: {
            [self showMinimalAppBar];
            [_delegate didSelectButton:_selectedButton];
            break;
        }
    }
}


#pragma mark -
#pragma mark tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_menuListDataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"menuListReuseIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        //cell.contentView.backgroundColor = [UIColor colorWithRed:0.129 green:0.125 blue:0.129 alpha:1.0];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"Avenir-Light" size:20];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
    }
    
    cell.textLabel.text = [_menuListDataSource objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self showMinimalAppBar];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:NO];
    
    [_delegate didSelectMenuListItemAtIndex:indexPath.row ForButton:_selectedButton];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellHeight;
}


#pragma mark -
#pragma mark Event Delegates methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:self];
    
    // Add some buffer around the circles so it isn't such a small touch point.
    // Open if minimal
    if (CGRectContainsPoint(CGRectMake(0, 0, self.frame.size.width, self.minimalHeight), point) && _currentState == AppBarMinimal)
    {
        [self showIconsAppBar];
    } else
    {
        switch (_currentState)
        {
            case AppBarIcons:
            {
                [self showMinimalAppBar];
                break;
            }
                
            case AppBarFullList:
            {
                [self showMinimalAppBar];
                //[self showFullAppBar];
                break;
            }
                
            default:
                break;
        }
    }
}

// Handles bug that wouldn't let us touch the bottom 20px-ish of the menuList
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if (CGRectContainsPoint(self.tableView.frame, point))
    {
        return YES;
    }
    
    if (point.y < 0)
    {
        if (_currentState != AppBarMinimal && _autoHide)
        {
            [self showMinimalAppBar];
        }
        return NO;
    }
    
    return YES;
}


@end
