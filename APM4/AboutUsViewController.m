//
//  AboutUsViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 08/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "AboutUsViewController.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)hasFooter{
    return  NO;
}

-(BOOL)hasHeader{
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.image.image = [UIImage imageNamed:@"acerca_nuestro_3"];
    
    // para evitar que esta pantalla rote es un lio, ver: http://stackoverflow.com/questions/12713695/container-controllers-and-ios-6-rotations
    
    
//    if (self.interfaceOrientation != UIInterfaceOrientationPortrait)
//    {
//        [[UIDevice currentDevice] performSelector:NSSelectorFromString(@"setOrientation:") withObject:(id)UIInterfaceOrientationPortrait];
//    }
//    
//    CGRect frame = CGRectMake(0, 0, 768, 960);
//    UIImageView *image = [[UIImageView alloc]initWithFrame:frame];
//    image.image = [UIImage imageNamed:@"acerca_nuestro_2"];
//    image.autoresizingMask = 18;
//    [self.view addSubview:image];
//    [image release];
    
}

//-(BOOL)shouldAutorotate{
//    return YES;
//}
//
//-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
//    return UIInterfaceOrientationPortrait;
//}
//
//-(NSUInteger)supportedInterfaceOrientations{
//    
//    return UIInterfaceOrientationMaskPortrait;
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_image release];
    [super dealloc];
}
@end
