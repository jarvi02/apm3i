//
//  MotivosNoVisita.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 01/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MotivosNoVisita : NSObject

@property(nonatomic, assign) NSInteger idMotivo;
@property(nonatomic, retain) NSString *descripcion;
@property(nonatomic, assign) NSInteger estado;

+(NSArray*)GetAll;

@end
