//
//  EstadoVisita.m
//  APM4
//
//  Created by Juan Pablo Garcia on 11/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "EstadoVisita.h"
#import "Config.h"
#import "DB.h"

@implementation EstadoVisita

+(EstadoVisita *)getByID:(NSUInteger)idEstado
{
    NSString *sql = @"SELECT id, descripcion FROM mtc_estadosvisita WHERE id = %d";
    
    
    NSString *query = [NSString stringWithFormat:sql, idEstado];
    sqlite3_stmt *statement = [[DB getInstance] prepare:[NSString stringWithFormat:query, idEstado]];
    
    EstadoVisita *v = [[EstadoVisita alloc] init];
    
    char *_c;
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            v.idEstado = sqlite3_column_int(statement, 0);
            
                      
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                v.descripcion = [NSString stringWithUTF8String:_c];
			}
        }
    }
    
    sqlite3_finalize(statement);
    
    return [v autorelease];
    
}


-(void)dealloc
{
    [_descripcion release];
    [super dealloc];
}

@end
