//
//  ServiceLogin.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 01/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ServiceLogin.h"
#import "NSString+NSString_MD5.h"
#import "Config.h"


@implementation ServiceLogin


- (void)login:(NSString*) user andPassword:(NSString*) password
{
    
    NSString *urlFormated = [NSString stringWithFormat:@"%@weblogin.dll?CMD=LOGIN&USER=%@&PASS=%@", URL_BASE, user, password];
    
    NSURL *url = [NSURL URLWithString:urlFormated];
    self.request = [ASIHTTPRequest requestWithURL:url];
    [self.request setDelegate:self];
    [self.request startAsynchronous];
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    [super requestFinished:request];
    NSString *response =  [request responseString];
    
    NSArray *array = [response componentsSeparatedByString:@"|"];
    NSString *estado = [array objectAtIndex:0];
    
    if ( [estado isEqualToString:@"OK"] ) {
        
        [[Config shareInstance] update:REMOTE_HOST value:[array objectAtIndex:1]];
        [[Config shareInstance] update:REMOTE_PORT value:[array objectAtIndex:2]];
        [[Config shareInstance] update:REMOTE_HOST_2 value:[array objectAtIndex:3]];
        [[Config shareInstance] update:REMOTE_PORT_2 value:[array objectAtIndex:4]];
        [[Config shareInstance] update:REMOTE_PATH value:[array objectAtIndex:5]];
        [[Config shareInstance] update:ENTIDAD value:[array objectAtIndex:6]];
        [[Config shareInstance] update:TRANID value:[array objectAtIndex:7]];
        [[Config shareInstance] update:CICLO value:[array objectAtIndex:8]];
        [[Config shareInstance] update:PAIS value:[array objectAtIndex:9]];
        
        [[Config shareInstance] update:APP_ACTIVADA value:@"1"];
        
        [[Config shareInstance] reloadData];
        
        [self.delegate requestFinished];
        
    } else {
        if([response containsString:@"contraseña"]){
            NSString *msg = @"Usuario o contraseña inválida";  //[array objectAtIndex:1];
            [self.delegate requestFailed:msg];
        }else{
        NSString *msg = @"No se ha podido establecer la conección";  //[array objectAtIndex:1];
        [self.delegate requestFailed:msg];
        }
    }
    
}

- (NSString*)textoParaLog {
    return @"ServiceLogin";
}
@end
