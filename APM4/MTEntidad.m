//
//  MTEntidad.m
//  APM4
//
//  Created by Ezequiel Manacorda on 2/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTEntidad.h"

#import "NSStringExtended.h"



@implementation MTEntidad


- (NSString*) getDescription
{
    NSString* newDescription = [NSString string:self.descripcion ifNull:@""];
    
    return newDescription;
}

- (MTEntidad*)copyWithZone:(NSZone*)zone
{
    MTEntidad* newCopy; // = [[MTEntidad alloc] init];
    
    newCopy = [[MTEntidad alloc] init];
    
//    @property(nonatomic, retain) NSString*  recID;
//    @property(nonatomic, retain) NSString*  nombre;
//    @property(nonatomic, retain) NSString*  direccion;
//    @property(nonatomic, assign) NSInteger  altura;
//    @property(nonatomic, retain) NSString*  localidad;
    
    newCopy.recID      = [NSString string:self.recID       ifNull:@""];
    newCopy.descripcion= [NSString string:self.descripcion ifNull:@""];
    newCopy.direccion  = [NSString string:self.direccion   ifNull:@""];
    newCopy.altura     = self.altura;
    newCopy.localidad  = [NSString string:self.localidad   ifNull:@""];
    
    return newCopy;
}

@end
