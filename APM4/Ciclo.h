//
//  Ciclo.h
//  APM4
//
//  Created by Juan Pablo Garcia on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Ciclo : NSObject

@property(nonatomic) NSUInteger         recID;
@property(nonatomic, retain) NSString   *ciclo;
@property(nonatomic, retain) NSDate     *desde;
@property(nonatomic, retain) NSDate     *hasta;
@property(nonatomic) NSUInteger         orden;
@property(nonatomic) NSUInteger         proximo;

+ (NSArray*)GetAll;
+ (Ciclo *)getById:(NSUInteger)orden;
+ (Ciclo *)actual;

- (NSString *)description;

@end
