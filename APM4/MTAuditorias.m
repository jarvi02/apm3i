//
//  MTAuditorias.m
//  APM4
//
//  Created by Laura Busnahe on 2/28/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTAuditorias.h"
#import "NSDateExtended.h"
#import "DB.h"

@implementation MTAuditorias

- (id)init
{
    self = [super init];
    if (self)
    {
        self.RecID         = 0;
        self.descripcion   = @"";
        self.actualizacion = nil;
    }
    
    return self;
}

- (NSString*)getDescription
{
    NSString* newDescription = [NSString stringWithFormat:@"%@",self.descripcion];
    
    return newDescription;
}



+ (NSArray*)getAll
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    //select id, descripcion from dpm_auditorias
    NSString *sql = @"select auditoria, descripcion, actualizacion from dpm_auditorias";
    
    
    NSString *query = sql;
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    NSString *sDate = @"";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    MTAuditorias *o;
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
			
            o = [[MTAuditorias alloc] init];
            
            // id Auditoría
            int _auditoria = sqlite3_column_int(statement, 0);
            o.RecID = _auditoria;
            
            // Descripcion
            char *_descripcion = (char *)sqlite3_column_text(statement, 1);
            o.descripcion = @"";
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o.descripcion = [NSString stringWithUTF8String:_descripcion];
            
            // Fecha de Actualización
            sDate = @"";
            o.actualizacion = nil;
            char *_fActualizacion = (char *)sqlite3_column_text(statement, 2);
            if ((_fActualizacion != nil) && (strlen(_fActualizacion) > 0))
            {
//              [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//                o.actualizacion = [dateFormatter dateFromString:sDate];
                o.actualizacion = [[NSDate alloc] initWithString:sDate withFormat:@"yyyy-MM-dd"];
            }
            
            [array addObject:o];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla dpm_auditorias");
	}
	sqlite3_finalize(statement);
    
    
    return array;
}

@end
