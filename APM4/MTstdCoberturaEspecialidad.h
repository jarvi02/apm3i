//
//  MTstdCoberturaEspecialidad.h
//  APM4
//
//  Created by Laura Busnahe on 7/18/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTstdCobertuaCommon.h"

@interface MTstdCoberturaEspecialidad : MTstdCobertuaCommon

+ (NSArray*)getAllWhere:(NSString*)where;

@end
