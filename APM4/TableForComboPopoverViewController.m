//
//  TableForComboPopoverViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "TableForComboPopoverViewController.h"
#import "EntryForCombo.h"

@interface TableForComboPopoverViewController ()

@end

@implementation TableForComboPopoverViewController


-(id)initWithData:(NSArray*) aData titulo:(NSString*) aTitulo size:(CGSize)aSize delegate:(id<TableForComboPopoverViewControllerDelegate>) aDelegate andCaller:(NSString *)caller{
    
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        self.titulo                         = aTitulo;
        self.data                           = aData;
        self.delegate                       = aDelegate;
        self.caller                         = caller;
        self.contentSizeForViewInPopover    = aSize;
        self.aligment                       = NSTextAlignmentCenter;
    }
    return self;    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        NSAssert(0, @"usar : -(id)initWithData:(NSArray*) aData titulo:(NSString*) aTitulo size:(CGSize)aSize delegate:(id<TableForComboPopoverViewControllerDelegate>) aDelegate andCaller:(NSString *)caller { ... }");
    }
    return self;
}

- (void)dealloc
{
    [_caller release];
    [_data release];
    [_titulo release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.clearsSelectionOnViewWillAppear = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ComboCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }

    cell.textLabel.text = ((EntryForCombo*)[self.data objectAtIndex:indexPath.row]).description;
    cell.textLabel.textAlignment = self.aligment;
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate selectedIndex:indexPath.row caller:self.caller];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@"   %@", self.titulo];
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont boldSystemFontOfSize:14.0f];
    label.backgroundColor = UIColorFromRGB(defaultHeaderCellColor);
    
    return [label autorelease];
}


@end
