//
//  GenericSelectorViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 27/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "GenericSelectorViewController.h"
#import "AppDelegate.h"

@interface GenericSelectorViewController ()

-(NSInteger)indexForOption:(NSString *)option;
-(NSString *)optionForIndex:(NSInteger)index;

@end

@implementation GenericSelectorViewController

#pragma mark -
#pragma mark LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil options:(NSArray *)options
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.options = options;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.title = @"Seleccione";
    UIBarButtonItem *barButton = [[[UIBarButtonItem alloc] initWithTitle: @"OK" style:UIBarButtonItemStyleDone target:self action:@selector(optionDone:)] autorelease];
    barButton.tintColor = UIColorFromRGB(defaultButtonColor);
    
    self.navigationItem.rightBarButtonItem = barButton;
//    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle: @"OK" style:UIBarButtonItemStyleDone target:self action:@selector(optionDone:)] autorelease];
    
	[super viewWillAppear:animated];
	if (self.option)
	{
		[self.picker selectRow:[self indexForOption:self.option] inComponent:0 animated:YES];
        
	}
}

#pragma mark -
#pragma mark Private Members

-(NSInteger)indexForOption:(NSString *)option
{
    int i = 0;
    for(NSString *o in self.options)
    {
       if ([o isEqualToString:option])
       {
           return i;
       }
        i++;
    }
    
    return -1;
}

-(NSString *)optionForIndex:(NSInteger)index
{
    return (NSString *)[self.options objectAtIndex:index];
}

#pragma mark -
#pragma mark Actions

- (IBAction)optionChanged
{
    
}

- (void)optionDone:(id)sender
{
    [Delegate dismissPopOver:YES];
    if (self.delegate != nil)
    {
        [self.delegate optionSelected:self.option index:[self indexForOption:self.option]];
    }
}

#pragma mark -
#pragma mark UIPickerViewDelegate & UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [self.options count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self optionForIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.option = [self optionForIndex:row];
}

#pragma mark -
#pragma mark Memory Management


- (void)dealloc
{
    
    [_options release];
    [_picker release];
    [_option release];
    
    [super dealloc];
}



@end
