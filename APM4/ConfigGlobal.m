//
//  ConfigGlobal.m
//  APM4
//
//  Created by Juan Pablo Garcia on 20/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ConfigGlobal.h"
#import "DB.h"



@interface ConfigGlobal(private)
-(void)load;
-(void)updateDB:(NSString*)aKey value:(NSString*)aValue;
@end


@implementation ConfigGlobal

+(ConfigGlobal*)shareInstance{
	static ConfigGlobal *instance;
	
	if(instance == nil){
		instance = [ConfigGlobal new];
	}
	return instance;
}

-(id) init {
	
    self = [super init];
	if ( self ){
        dictionary = [[NSMutableDictionary alloc] init];
        [self load];
	}
	return self;
}

-(void)dealloc{
    
    [dictionary release];
    [super dealloc];
}

- (BOOL)isValidKey:(NSString *)key
{
    return [dictionary valueForKey:key]!=nil;
}


-(void)load{
    
    NSString *sql = @"select propiedad, valor from cfg_global;";
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
			
            
            char *_prop = (char *)sqlite3_column_text(statement, 0);
            NSString *prop;
            if((_prop != nil) && (strlen(_prop) > 0)) {
                prop = [NSString stringWithUTF8String:_prop];
			}else{
				prop = @"";
			}
            
            char *_val = (char *)sqlite3_column_text(statement, 1);
            NSString *val;
            if((_val != nil) && (strlen(_val) > 0)) {
                val = [NSString stringWithUTF8String:_val];
			}else{
				val = @"";
			}
            
            [dictionary setValue:val forKey:prop];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla cfg_global");
	}
	sqlite3_finalize(statement);
}

-(void) reloadData{
    
    [dictionary removeAllObjects];
    [self load];
    
}

-(void)updateDB:(NSString*)aKey value:(BOOL)aValue
{
    NSString *val = aValue ? @"S" : @"N";

    NSString *sql = @"UPDATE cfg_global set valor = %@ where propiedad = %@";
    
    NSString *query = [NSString stringWithFormat:sql,val, aKey];
    [[DB getInstance] excecuteSQL:query];
}


-(BOOL)find:(NSString*)aKey{
    
    NSString *value =[dictionary valueForKey:aKey];
    if (value == nil)
    {
//        NSString *msg = [NSString stringWithFormat:@"No se encontro key en diccionario Config ----> %@ <-----",aKey];
//        NSAssert(0, msg);
        return FALSE;
    }
    return [value isEqualToString:@"S"] ? YES : NO;
}

-(NSString *)findValue:(NSString*)aKey
{
    
    NSString *value =[dictionary valueForKey:aKey];
    if (value == nil) {
        NSString *msg = [NSString stringWithFormat:@"No se encontro key en diccionario Config ----> %@ <-----",aKey];
        NSAssert(0, msg);
    }
    return value;
}

-(void)update:(NSString*)aKey value:(NSString*)aValue{
    
    [self updateDB:aKey value:aValue];
    [dictionary removeObjectForKey:aKey];
    [dictionary setValue:aValue forKey:aKey];
    
}

- (BOOL)getVISITA_PROMOCION_SIN_DESCARGA
{
#ifdef FORCE_PROMOCION_SIN_DESCARGA
    return YES;
#endif
    
    if ([self isValidKey:PROMOCIONSINDESCARGA])
    {
        if ([[self findValue:PROMOCIONSINDESCARGA] isEqualToString:@"S"])
            return YES;
        
        return NO;
    }
    else
        return NO;
}


@end

