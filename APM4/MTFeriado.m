//
//  MTFeriado.m
//  APM4
//
//  Created by Juan Pablo Garcia on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTFeriado.h"
#import "DB.h"

@implementation MTFeriado


+(NSArray *)getAll
{
    NSMutableArray *feriados = [NSMutableArray array];
    
    // Cargo todas las tareas
    
    NSString *sql = @"select fecha, descripcion, activo from mtc_feriados where activo = 'S'";
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            MTFeriado *feriado = [[MTFeriado alloc] init];
            
            // fecha
            double val = sqlite3_column_double(statement, 0);
            feriado.fecha = [NSDate dateWithTimeIntervalSince1970:val];
            
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            [formatter setDateFormat:@"dd/MM/yyyy"];
//            NSLog(@"%@", [formatter stringFromDate:feriado.fecha]);
//            [formatter release];

            // Descripcion
            char *_descripcion = (char *)sqlite3_column_text(statement, 1);
            if((_descripcion != nil) && (strlen(_descripcion) > 0)) {
                feriado.descripcionFeriado = [NSString stringWithUTF8String:_descripcion];
			}else{
				feriado.descripcionFeriado = @"";
			}
            
            feriado.activo = YES;
            
            [feriados addObject:feriado];
//            [feriado release];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla mtc_tareas");
	}
	sqlite3_finalize(statement);
    
    return feriados;
}

+(BOOL)isFeriado:(NSDate *)date
{
//    COMENTADO POR EZEQUIEL: Esta rutina era ineficiente y demoraba mucho la busqueda de feriados.
//
//    NSArray *all = [MTFeriado getAll];
//    for(MTFeriado *feriado in all)
//    {
//        if ([date compare:[feriado fecha]] == NSOrderedSame)
//        {
//            return YES;
//        }
//    }
//    
//    return NO;
    
    BOOL result = NO;
    
    // Convierto la date a string con el formato para SQLite.
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"yyyy-MM-dd"];
//    NSString *strDate = [dateFormat stringFromDate:date];
    NSString* strDate = [date formattedStringUsingFormat:@"yyyy-MM-dd"];
    // Genero el SQL del query.
    NSString *sql = [NSString stringWithFormat:@"select fecha, activo from mtc_feriados where (activo = 'S') AND (fecha = %@)",
                                                [NSString stringQuoted:strDate ifNull:@"''"]];
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    if ( statement ) {
        // Si hay records para la fecha dada, cambio el valor del resultado de la función a YES
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            result = YES;
        }
        
	} else {
        NSAssert(0, @"No se encontró la tabla mtc_feriados");
	}
	sqlite3_finalize(statement);

    return result;
}

+(BOOL)isDate:(NSDate*)date intoArray:(NSArray*)array
{
    NSDate *dateNoTime = [date dateAsDateWithoutTime];
    
    for (NSDate* d in array)
    {
        NSDate *arrayDate = [d dateAsDateWithoutTime];
        if ([arrayDate compare:dateNoTime] == NSOrderedSame)
            return YES;
    }
    
    return NO;
}

//-(void) dealloc
//{
//    [_fecha release];
//    [_descripcionFeriado release];
//    
//    [super dealloc];
//}

@end
