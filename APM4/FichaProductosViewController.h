//
//  FichaProductosViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 19/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "CarteraViewController.h"

@interface FichaProductosViewController : CustomViewController

@property(nonatomic, strong) CarteraViewController *carteraViewControllerReference;

@property (strong, nonatomic) IBOutlet UISegmentedControl *btnSegment;
- (IBAction)tapSegments:(id)sender;

#pragma mark -
#pragma mark Planificado/Realizado view
@property (strong, nonatomic) IBOutlet UIView *planificadoView;
@property (strong, nonatomic) IBOutlet UITableView *programadosTableView;
@property (strong, nonatomic) IBOutlet UITableView *promocionadosTableView;

#pragma mark -
#pragma mark Histórico de productos view
@property (strong, nonatomic) IBOutlet UIScrollView *historicoView;
@property (strong, nonatomic) IBOutlet UITableView *historicoTable;

@end
