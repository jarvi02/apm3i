//
//  MuestrasEntregadas.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MuestrasEntregadas.h"
#import "DB.h"
#import "Config.h"

@implementation MuestrasEntregadas


- (id)init
{
    self = [super init];
    if (self) {

        self.apellidoNombre = @"";
        self.descripcion = @"";
        self.codigo = @"";
        self.lote = @"";
    
    }
    return self;
}

- (void)dealloc
{
    [_apellidoNombre release];
    [_descripcion release];
    [_codigo release];
    [_lote release];
    
    [super dealloc];
}

+(NSArray*)GetAll {
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    NSString *sql = @"select v.fecha as fecha_display, m.apellido || ', ' || m.nombre, l.descripcion, l.codigo, l.lote, p.muestras, l.fecha_vto as fecha_vto_display from med_promociones p join mtc_lotes l on (l.idlote = p.lote and l.nodo = p.nodo) join med_medicos m on (m.nodo = p.nodo and m.id = p.medico) join med_visitas v on (v.nodo = p.nodo and p.medico = v.medico and v.visitador = p.visitador and v.id = p.visita) where p.nodo = %@ Order by v.fecha, m.apellido, l.descripcion;";
    
    NSString *nodo = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql, nodo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        MuestrasEntregadas *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[MuestrasEntregadas alloc] init];
            
            o.fecha = sqlite3_column_int(statement, 0);
            
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.apellidoNombre = [NSString stringWithUTF8String:_c];
			}
           
            _c = (char *)sqlite3_column_text(statement, 2);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}

            _c = (char *)sqlite3_column_text(statement, 3);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.codigo = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 4);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.lote = [NSString stringWithUTF8String:_c];
			}
            
            o.muestras = sqlite3_column_int(statement, 5);
            
            o.fecha_vto = sqlite3_column_int(statement, 6);
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
    
}


@end
