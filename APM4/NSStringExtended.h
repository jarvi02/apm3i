//
//  NSStringExtended.h
//  APM4
//
//  Created by Laura Busnahe on 4/18/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (extensions)

-(BOOL)isValidIP;
-(BOOL)isNumber;

+ (NSString*)   stringQuoted:(NSString*)s ifNull:(NSString*)nullS;
+ (NSString*)   string:(NSString*)s;
+ (NSString*)   string:(NSString*)s ifNull:(NSString*)nullS;
+ (NSString*)   intToStr:(NSInteger)intVal;
+ (NSString*)   intToStr:(NSInteger)intVal ifZero:(NSString*)zeroString;
+ (NSString*)   floatToStr:(double)val;
+ (NSString*)   pCharToString:(char*)_c;

@end
