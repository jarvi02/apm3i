//
//  Horario.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 04/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Horario.h"
#import "Novedad.h"

#import "GlobalDefines.h"
#import "Config.h"

#import "DBExtended.h"
#import "NSDateExtended.h"
#import "NSStringExtended.h"

@implementation Horario


- (id)init
{
    self = [super init];
    if (self) {
        self.horaDesde      = @"00:00";
        self.horaHasta      = @"00:00";
        self.comentarios    = @"";
        
        self.iHrDesde  = 0;
        self.iMinDesde = 0;
        self.iHrHasta  = 0;
        self.iMinHasta = 0;
        
        self.recID = 0;
    }
    return self;
}

-(id)copy{
    
    Horario *horario = [[Horario alloc] init];
    
    horario.lunes       = self.lunes;
    horario.martes      = self.martes;
    horario.miercoles   = self.miercoles;
    horario.jueves      = self.jueves;
    horario.viernes     = self.viernes;
    
    horario.iHrDesde    = self.iHrDesde;
    horario.iMinDesde   = self.iMinDesde;
    horario.iHrHasta    = self.iHrHasta;
    horario.iMinHasta   = self.iMinHasta;
    
    horario.horaDesde   = [NSString stringWithFormat:@"%@", self.horaDesde];
    horario.horaHasta   = [NSString stringWithFormat:@"%@", self.horaHasta];
    horario.comentarios = [NSString stringWithFormat:@"%@", self.comentarios];
    
    horario.recID = self.recID;
    
    return  horario;
}

-(NSString *)descripcion{
    
    NSString *str= @"";
    
    if (self.lunes) {
        str = [NSString stringWithFormat:@"%@Lunes",str];
    }
    if (self.martes) {
        str = [NSString stringWithFormat:@"%@ Martes",str];
    }
    if (self.miercoles) {
        str = [NSString stringWithFormat:@"%@ Miercoles",str];
    }
    if (self.jueves) {
        str = [NSString stringWithFormat:@"%@ Jueves",str];
    }
    if (self.viernes) {
        str = [NSString stringWithFormat:@"%@ Viernes",str];
    }
    
    str = [NSString stringWithFormat:@"%@ desde %@ hasta %@",str,
                    [NSString string:self.horaDesde ifNull:@"0:00"],
                    [NSString string:self.horaHasta ifNull:@"0:00"]
           ];
    
    return str;
}

-(NSString*)isOkForSave
{
    
    
    if ((self.lunes == NO)      &&
        (self.martes == NO)     &&
        (self.miercoles == NO)  &&
        (self.jueves == NO)     &&
        (self.viernes == NO))
    {
        return @"Debe seleccionar al menos un día.";
    }
       
    
    // Valor del Horario Desde
    if (((self.iHrDesde < 0) || (self.iHrDesde > 23)) ||
        ((self.iMinDesde < 0) || (self.iMinDesde > 59)))
    {
        return @"Debe ingresar un horario válido para el Desde.";
    }
    
    
    // Valor del Horario Hasta
    if (((self.iHrHasta < 0) || (self.iHrHasta > 23)) ||
        ((self.iMinHasta < 0) || (self.iMinHasta > 59)))
    {
        return @"Debe ingresar un horario válido para el Hasta.";
    }
    
    
    // Chequeo que el horaro de Desde sea menor que el Hasta.
    NSInteger iHorarioDesde = 0;
    NSInteger iHorarioHasta = 0;
    iHorarioDesde = [[NSString stringWithFormat:@"%02d%02d", self.iHrDesde, self.iMinDesde] integerValue];
    iHorarioHasta = [[NSString stringWithFormat:@"%02d%02d", self.iHrHasta, self.iMinHasta] integerValue];
    if (iHorarioDesde >= iHorarioHasta)
    {
        return @"El horario Desde debe ser siempre menor que el Hasta.";
    }
    
    self.horaDesde = [NSString stringWithFormat:@"%02d:%02d", self.iHrDesde, self.iMinDesde];
    self.horaHasta = [NSString stringWithFormat:@"%02d:%02d", self.iHrHasta, self.iMinHasta];
    
    self.comentarios = [NSString string:self.comentarios ifNull:@""];
    
    return nil;
    
}

-(void)saveForMedico:(NSInteger)idMedico Domicilio:(NSInteger)idDomicilio
{
    NSInteger idHorario = [[DB getInstance] getNewIDForTable:@"med_horarios"
                                                 idNameOrNil:nil
                                                  whereOrNil:
                           [NSString stringWithFormat:@"(nodo = %@) AND (medico = %@) AND (domicilio = %@)",
                            SettingForKey(NODO, @"0"),
                            [NSString intToStr:idMedico],
                            [NSString intToStr:idDomicilio]
                            ]
                           ];
    
    NSString *sqlMedicos = @"INSERT INTO med_horarios "
    "(nodo, medico, domicilio, id, desde, "
    "hasta, comentarios, lu, ma, mi, "
    "ju, vi) "
    "VALUES(%@, %@, %@, %@, %@, "
    "%@, %@, %@, %@, %@,"
    "%@, %@);";
    NSString *queryMedico = [NSString stringWithFormat:sqlMedicos,
                   SettingForKey(NODO, @"0"),
                   [NSString intToStr:idMedico],
                   [NSString intToStr:idDomicilio],
                   [NSString intToStr:idHorario],
                   [[NSDate stringToTime:self.horaDesde] formattedStringUsingFormat:@"HHmm"],
                   [[NSDate stringToTime:self.horaHasta] formattedStringUsingFormat:@"HHmm"],
                   [NSString stringQuoted:self.comentarios ifNull:@"''"],
                   self.lunes?@"1":@"0",
                   self.martes?@"1":@"0",
                   self.miercoles?@"1":@"0",
                   self.jueves?@"1":@"0",
                   self.viernes?@"1":@"0"
                   ];
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- query Alta HorariosMedico: %@", queryMedico);
#endif
    [[DB getInstance] excecuteSQL:queryMedico];
    
    
    /*
     Grabar novedad en la log_novedades para la MED_HORARIOS
     Tabla : MED_HORARIOS
     Tipo : ‘I’
     Registro : nodo|medico|domicilio|id|desde|hasta|comentarios.trim()|(lunes==1?"S":"N")|
     (martes==1?"S":"N")|(miercoles==1?"S":"N")|(jueves==1?"S":"N")|(viernes==1?"S":"N")
     TranId : 0
     Transmitido : 0
     */
    
    NSString *novedadMedico = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",
                               SettingForKey(NODO, @"0"),
                               [NSString intToStr:idMedico],
                               [NSString intToStr:idDomicilio],
                               [NSString intToStr:idHorario],
                               [[NSDate stringToTime:self.horaDesde] formattedStringUsingFormat:@"HHmm"],
                               [[NSDate stringToTime:self.horaHasta] formattedStringUsingFormat:@"HHmm"],
                               [NSString string:self.comentarios ifNull:@""],
                               self.lunes?@"S":@"N",
                               self.martes?@"S":@"N",
                               self.miercoles?@"S":@"N",
                               self.jueves?@"S":@"N",
                               self.viernes?@"S":@"N"
                               ];
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- Novedad Alta HorariosMedico: %@", novedadMedico);
#endif
    [Novedad insert:@"med_horarios" novedad:novedadMedico tipo:kInsert];
    
    
}

+(void)deleteAllForMedico:(NSInteger)idMedico
{
    NSString *sqlMedicos = @"select nodo || '|' || medico || '|' || domicilio  || '|' || id  || '|' || desde  || '|' || hasta || '|' || "
    "case when comentarios is null then '' else comentarios end  || '|' || "
    "case when lu = 1 then 'S' else 'N' end  || '|' || "
    "case when ma = 1 then 'S' else 'N' end  || '|' || "
    "case when mi = 1 then 'S' else 'N' end  || '|' || "
    "case when ju = 1 then 'S' else 'N' end  || '|' || "
    "case when vi = 1 then 'S' else 'N' end as registro "
    "from med_horarios where nodo = %@ and medico = %@";
    
    NSString *queryMedico = [NSString stringWithFormat:sqlMedicos,
                             SettingForKey(NODO, @"0"),
                             [NSString intToStr:idMedico]];
    
    // Genero un array con los strings para luego agregar el registro en la log_novedades.
    NSMutableArray *arrayNovedad = [[NSMutableArray alloc] init];
    [arrayNovedad removeAllObjects];

    sqlite3_stmt *statement = [[DB getInstance] prepare:queryMedico];
    if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            NSString *o = @"";
            
            // Param. log_novedades
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o = [NSString stringWithUTF8String:_descripcion];
            
            [arrayNovedad addObject:[NSString stringWithFormat:@"%@",o]];
        }
        
	} else {
        NSAssert(0, @"ERROR: No se pudo ejecutar: %@", queryMedico);
	}
	sqlite3_finalize(statement);
    
    // Chequeo que haya algún dato para borrar.
    if ([arrayNovedad count] == 0)
        return;
    
    // Borro los registros de la base de datos
    sqlMedicos = @"DELETE FROM med_horarios WHERE (nodo=%@) AND (medico=%@)";
    queryMedico = [NSString stringWithFormat:sqlMedicos,
                   SettingForKey(NODO, @"0"),
                   [NSString intToStr:idMedico]];
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- query D med_horarios: %@", queryMedico);
#endif
    [[DB getInstance] excecuteSQL:queryMedico];
    
    
    // LOG_NOVEDADES
    for (NSString *str in arrayNovedad)
    {
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- Novedad D med_horarios: %@", str);
#endif
        [Novedad insert:@"med_horarios" novedad:str tipo:kDelete];
    }
}

+(NSArray*)getAllByMedico:(NSUInteger)idMedico Domicilio:(NSUInteger)idDomicilio
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql = @"SELECT id, desde, hasta, comentarios, lu, ma, mi, ju, vi "
    "FROM med_horarios "
    "WHERE domicilio = %d AND "
        "nodo = %@ AND "
        "medico = %d";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql, idDomicilio, nodo, idMedico];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
	if ( statement )
    {
        char *_c;
        NSString *str;
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
        
            Horario *h = [[Horario alloc] init];
            
            // recID
            h.recID = sqlite3_column_int(statement, 0);
          
            // Desde
            h.horaDesde = @"";
            NSInteger iHorarioDesde = sqlite3_column_int(statement, 1);
            str = [NSString stringWithFormat:@"%04d", iHorarioDesde];
            h.horaDesde = [[NSDate stringToTime:str withFormat:@"HHmm"] timeToString];
            
            // Hasta
            h.horaHasta = @"";
            NSInteger iHorarioHasta = sqlite3_column_int(statement, 2);
            str = [NSString stringWithFormat:@"%04d", iHorarioHasta];
            h.horaHasta = [[NSDate stringToTime:str withFormat:@"HHmm"] timeToString];
            
            // Comentarios
            _c = (char *)sqlite3_column_text(statement, 3);
            if((_c != nil) && (strlen(_c) > 0))
            {
                h.comentarios = [NSString stringWithUTF8String:_c];
			}
            
            // Lunes
            h.lunes = sqlite3_column_int(statement, 4)==0?NO:YES;
            
            // Martes
            h.martes = sqlite3_column_int(statement, 5)==0?NO:YES;
            
            // Miercoles
            h.miercoles = sqlite3_column_int(statement, 6)==0?NO:YES;
            
            // Jueves
            h.jueves = sqlite3_column_int(statement, 7)==0?NO:YES;
            
            // Viernes
            h.viernes = sqlite3_column_int(statement, 8)==0?NO:YES;
            
            [result addObject:h];
        }
        
    } else
        return result;
    
    sqlite3_finalize(statement);
    
    return result;
}

@end
