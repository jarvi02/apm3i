//
//  Telefono.m
//  APM4
//
//  Created by Laura Busnahe on 4/18/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Telefono.h"
#import "Novedad.h"

#import "GlobalDefines.h"
#import "Config.h"
#import "DBExtended.h"
#import "NSStringExtended.h"


@implementation Telefono
- (id)init
{
    self = [super init];
    if (self) {
        self.numero = @"";
        self.pais   = 0;
        self.area   = 0;
    }
    return self;
}



-(id)copy{
    
    Telefono *tel = [[Telefono alloc] init];
    
    tel.numero = [NSString stringWithFormat:@"%@", self.numero];
    tel.pais = self.pais;
    tel.area = self.area;

    return  tel;
}

-(NSString *)descripcion{
    
    NSString *str= @"";
    
    NSString *numero;
    NSString *pais;
    NSString *area;
    
    numero = [NSString string:self.numero ifNull:@""];
    pais   = self.pais==0?@"":[NSString stringWithFormat:@"+%d ", self.pais];
    area   = self.area==0?@"":[NSString stringWithFormat:@"%d ", self.area];
    
    str = [NSString stringWithFormat:@"%@%@%@",pais, area, numero];
    
    return str;
}


-(void)saveForMedico:(NSInteger)idMedico Domicilio:(NSInteger)idDomicilio
{
    /*
     Insert a la MED_TELEFONOS (tabla relación nodo, medico, telefonos)
     
     "nodo" = nodo variable global);
     "medico", id de medico
     "domicilio" =  iddomicilio (asignado en el paso anterior)
     "numero” = numero de teléfono ingresado
     "pais", país = id de país variable global
     "area", area = Nro de area ingresada
     */
    
    
    NSString *sqlMedicos = @"INSERT INTO med_telefonos "
    "(nodo, medico, domicilio, numero, pais, area) "
    "VALUES(%@, %@, %@, %@, %@, %@);";
    NSString *queryMedico = [NSString stringWithFormat:sqlMedicos,
                             SettingForKey(NODO, @"0"),
                             [NSString intToStr:idMedico],
                             [NSString intToStr:idDomicilio],
                             [NSString stringQuoted:self.numero ifNull:@"''"],
                             [NSString intToStr:self.pais],
                             [NSString intToStr:self.area]
                             ];
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- query Alta TelefonoMedico: %@", queryMedico);
#endif
    [[DB getInstance] excecuteSQL:queryMedico];
     
     /*
     Grabar novedad en la log_novedades para la MED_TELEFONOS
     
     Tabla : MED_TELEFONOS
     Tipo : ‘I’
     Registro : nodo|medico|domicilio|pais|area|numero
     TranId : 0
     Transmitido : 0
     
     //este caso podría obviarse según como se haga, en android lo hacemos para el siguiente caso : el usuario ingresa un domicilio, le asigna un horario todo dentro del alta ed domicilio.
     Luego vuelve a la pantalla ppal de alta de medico y desde esta asigna otro horario. El caso del insert este seria para dar de alta el horario sin modificar la med_domicilios.
     */
    
    NSString *novedadMedico = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@",
                               SettingForKey(NODO, @"0"),
                               [NSString intToStr:idMedico],
                               [NSString intToStr:idDomicilio],
                               [NSString intToStr:self.pais],
                               [NSString intToStr:self.area],
                               [NSString string:self.numero ifNull:@""]
                               ];
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- Novedad Alta TelefonoMedico: %@", novedadMedico);
#endif
    [Novedad insert:@"med_telefonos" novedad:novedadMedico tipo:kInsert];

}


+(void)deleteAllForMedico:(NSInteger)idMedico
{
    NSString *sqlMedicos = @"select nodo || '|' || medico || '|' || domicilio || '|' || pais || '|' || " 
    "area || '|' || numero as registro "
    "from med_telefonos where nodo = %@ and medico = %@";
    
    NSString *queryMedico = [NSString stringWithFormat:sqlMedicos,
                             SettingForKey(NODO, @"0"),
                             [NSString intToStr:idMedico]];
    
    // Genero un array con los strings para luego agregar el registro en la log_novedades.
    sqlite3_stmt *statement = [[DB getInstance] prepare:queryMedico];
    NSMutableArray *arrayNovedad = [[NSMutableArray alloc] init];
    [arrayNovedad removeAllObjects];
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            NSString *o = @"";
            
            // Param. log_novedades
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o = [NSString stringWithUTF8String:_descripcion];
            
            [arrayNovedad addObject:[NSString stringWithFormat:@"%@",o]];
        }
        
	} else {
        NSAssert(0, @"ERROR: No se pudo ejecutar: %@", queryMedico);
	}
	sqlite3_finalize(statement);
    
    // Chequeo que haya algún dato para borrar.
    if ([arrayNovedad count] == 0)
        return;
    
    // Borro los registros de la base de datos
    sqlMedicos = @"DELETE FROM med_telefonos WHERE (nodo=%@) AND (medico=%@)";
    queryMedico = [NSString stringWithFormat:sqlMedicos,
                   SettingForKey(NODO, @"0"),
                   [NSString intToStr:idMedico]];
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- query D med_telefonos: %@", queryMedico);
#endif
    [[DB getInstance] excecuteSQL:queryMedico];
    
    
    // LOG_NOVEDADES    
    for (NSString *str in arrayNovedad)
    {
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- Novedad D med_telefonos: %@", str);
#endif
        [Novedad insert:@"med_telefonos" novedad:str tipo:kDelete];
    }

}


+(NSArray*)getAllByMedico:(NSUInteger)idMedico Domicilio:(NSUInteger)idDomicilio
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql = @"SELECT numero, pais, area "
    "FROM med_telefonos "
    "WHERE domicilio = %d AND "
          "nodo = %@ AND "
          "medico = %d";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql, idDomicilio, nodo, idMedico];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
	if ( statement )
    {
        char *_c;
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            
            Telefono *h = [[Telefono alloc] init];
            
            // Numero
            _c = (char *)sqlite3_column_text(statement, 0);
            if((_c != nil) && (strlen(_c) > 0))
            {
                h.numero = [NSString stringWithUTF8String:_c];
			}
            
            // Pais
            h.pais = sqlite3_column_int(statement, 1);
            
            // Area
            h.area = sqlite3_column_int(statement, 2);
            
            [result addObject:h];
        }
        
    } else
        return result;
    
    sqlite3_finalize(statement);
    
    return result;
}

-(NSString*)isOkForSave
{
    
    
    // realmente con esto no queda muy claro que validaciones hay que hacer sobre el domicilio.
    
    /*De este pantalla si se selecciona “consultorio” los campos institución, cargo , piso, dpto. y teléfonos no son obligatorios.
     
     Si se selecciona institución los campos piso, dpto. y teléfonos no son obligatorios.
     
     Importante : Si es el primer domicilio que se carga, el campo UTILIDAD es siempre de VISITA y no se puede modificar.
     En los posteriores se puede modificar pero no se puede elegir visita
     
     SI PRIMER DOMICILIO
     CAMPO UTILIDAD = VISITA (no se peude tocar)
     SINO
     CAMPO UTILIDAD seleccionable menos la VISITA
     */
    
    
    return @"OK";
    
}

@end
