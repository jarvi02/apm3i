//
//  prescDetailViewController.h
//  APM4
//
//  Created by Laura Busnahe on 2/28/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface prescDetailViewController : UIViewController <UISplitViewControllerDelegate>

@end
