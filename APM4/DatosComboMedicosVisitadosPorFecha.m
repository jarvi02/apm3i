//
//  DatosComboMedicosVisitadosPorFecha.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "DatosComboMedicosVisitadosPorFecha.h"
#import "Config.h"
#import "DB.h"
#import "NSDate+extensions.h"

@implementation DatosComboMedicosVisitadosPorFecha

-(void)dealloc
{
    [_fecha  release];
    [super dealloc];
}

-(id)init{
    
    self = [super init];
    if (self) {
        
    }
    return self;
}

+(NSArray*)GetAll {
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"select 1 as _id, vis.fecha as fechadisplay from med_visitas vis inner join mtc_tiposvisita tipo on vis.tipo = tipo.id where tipo.contacto = 1 and vis.nodo = %@ group by fechadisplay order by fechadisplay;";
    
    NSString *nodo = [[Config shareInstance] find:NODO];
    NSString *query = [NSString stringWithFormat:sql, nodo];

    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        DatosComboMedicosVisitadosPorFecha *o;
        BOOL flag;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            flag = YES;
            NSDate *date = [[NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 1)] dateAsDateWithoutTime];
            for (DatosComboMedicosVisitadosPorFecha *dat in array)
            {
                if ([date isEqualToDate:dat.fecha])
                {
                    flag = NO;
                    break;
                }
            }
            
            if (flag) {
                o = [[DatosComboMedicosVisitadosPorFecha alloc] init];
                o.idDatosComboMedicosVisitadosPorFecha = sqlite3_column_int(statement, 0);
                o.fecha = date;
                [array addObject:o];
                [o release];
            }
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);

    return [array autorelease];
}


@end
