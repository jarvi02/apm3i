//
//  listViewController.h
//  APM4
//
//  Created by Laura Busnahe on 6/11/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "popListView.h"
@class listViewController;


#pragma mark -
#pragma mark Protocols

@protocol listViewDelegate <NSObject>
@required
// Sent when the user selects a row in the menu list.
- (void)listViewController:(listViewController *)controller referenceControl:(id)sender selectedObjects:(NSArray *)selectionArray;
@end



#pragma mark -
#pragma mark Classes

@interface listViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MTpopListViewDelegate>

@property (nonatomic, assign) id                delegate;
@property (nonatomic, assign) id                reference;                          // Qué control es la referencia del delegate.

@property (nonatomic, strong) NSMutableArray    *listContent;                       // Objetos listados en la tabla.
@property (nonatomic, strong) NSArray           *allObjects;                        // Listado de objetos que es posible agregar.
@property (nonatomic, strong) NSString          *mainKey;

@property (nonatomic, strong) MTpopListView     *popMenu;

// Outlets
@property (retain, nonatomic) IBOutlet UILabel          *lblTitulo;
@property (retain, nonatomic) IBOutlet UIBarButtonItem  *btnAceptar;
@property (retain, nonatomic) IBOutlet UIBarButtonItem  *btnCancelar;
@property (retain, nonatomic) IBOutlet UIBarButtonItem  *btnEdit;
@property (retain, nonatomic) IBOutlet UIButton         *btnAdd;
@property (retain, nonatomic) IBOutlet UITableView      *tableView;
@property (retain, nonatomic) IBOutlet UIToolbar        *toolbar;

//IBActions
- (IBAction)tapAceptar:(id)sender;
- (IBAction)tapCancelar:(id)sender;
- (IBAction)tapEdit:(id)sender;
- (IBAction)tapAdd:(id)sender;


// Methods
- (id)initViewDefaultNibWithReference:(id)reference;
- (id)initViewAsFechaUltimaVisita:(id)reference;
- (id)initViewAsEspecialidadesWithReference:(id)reference;
- (id)initViewAsPotencialesWithReference:(id)reference;
- (id)initViewAsObrasSocialesWithReference:(id)reference;

- (void)setObjectsList:(NSMutableArray*)oArray;

@end
