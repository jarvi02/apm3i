//
//  MTPrescripcionesTotales.h
//  APM4
//
//  Created by Laura Busnahe on 3/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface MTTotalesMedico : NSObject

@property (nonatomic, strong) NSString     *descripcion;
@property (nonatomic)         NSInteger    unidades;
@property (nonatomic)         float        porcentaje;

@end


@interface MTTotalesProducto : NSObject

@property (nonatomic, strong) NSString     *descripcion;
@property (nonatomic)         NSInteger    trimestre1;
@property (nonatomic)         NSInteger    trimestre2;
@property (nonatomic)         NSInteger    trimestre3;
@property (nonatomic)         NSInteger    trimestre4;
@property (nonatomic)         NSInteger    total;
@property (nonatomic)         BOOL         laboratorio;
@property (nonatomic, strong) NSString     *descMercado;

@end



@interface MTarrayTotalesProductos : NSMutableArray
{
    NSMutableArray  *_backArray;
}

- (void)sortByProperty:(NSString*)key Ascending:(BOOL)ascending;

@end



@interface MTarrayTotalesMedico : NSMutableArray
{
    NSMutableArray  *_backArray;
}

@end


@interface MTPrescripcionesTotales : NSObject

@property (nonatomic, strong) MTarrayTotalesMedico     *prescTotalesMedico;
@property (nonatomic, strong) MTarrayTotalesProductos  *prescProductos;


+ (MTPrescripcionesTotales*)getTotalesMedico:(NSInteger)idMedico
                                   Auditoria:(NSInteger)idAuditoria
                                    Mercados:(NSArray*)arrayMercados;


@end
