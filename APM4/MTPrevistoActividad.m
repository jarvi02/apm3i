//
//  MTPrevistoActividad.m
//  APM4
//
//  Created by Laura Busnahe on 2/14/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTPrevistoActividad.h"
#import "MTEntidad.h"
#import "Novedad.h"

@implementation MTPrevistoActividad

- (id)init
{
    self = [super init];
    if (self)
    {
        self.entidad     = nil;
        self.tipoEntidad = kTEUnknown;
        self.entrada     = nil;
        self.salida      = nil;
    }
    
    return self;
}


- (NSString*) getDescription
{
    NSString* newDescription = nil;

//    NSDateFormatter* formatedDate = [[NSDateFormatter alloc] init];
//    [formatedDate setDateFormat:@"HH:mm"];
    if (self.salida != nil)
    {
//        newDescription = [NSString stringWithFormat:@"De %@ a %@, %@",
//                                  [formatedDate stringFromDate:self.entrada],
//                                  [formatedDate stringFromDate:self.salida],
//                                  [self.entidad getDescription]
//                                  ];
        newDescription = [NSString stringWithFormat:@"De %@ a %@, %@",
                          [self.entrada formattedStringUsingFormat:@"HH:mm"],
                          [self.salida formattedStringUsingFormat:@"HH:mm"],
                          [self.entidad getDescription]
                          ];
    } else
    {
//        newDescription = [NSString stringWithFormat:@"Desde %@, %@",
//                                  [formatedDate stringFromDate:self.entrada],
//                                  [self.entidad getDescription]
//                                  ];
        newDescription = [NSString stringWithFormat:@"Desde %@, %@",
                          [self.entrada  formattedStringUsingFormat:@"HH:mm"],
                          [self.entidad getDescription]
                          ];
    }
    
    return newDescription;
}

- (NSString*) getDireccion
{
    NSString* newDescription = @"";
    
    if (self.tipoEntidad != kTEUnknown)
    {
        newDescription = [NSString stringWithFormat:@"%@ %i, %@",
                          self.entidad.direccion,
                          self.entidad.altura,
                          self.entidad.localidad
                          ];
    }
    
    return newDescription;
}


#pragma mark -
#pragma mark Data Base methods

- (NSUInteger)getDbEntityType
{
    NSUInteger result = TEDB_OTROS;
    
    switch (self.tipoEntidad)
    {
        case kTEMedico:
            result = TEDB_MEDICO;
            break;
            
        case kTEInstitucion:
            result = TEDB_INSTITUCION;
            break;
            
        default:
            break;
    }
    
    return result;
}

- (void)setEntityTypeFromDBvalue:(NSUInteger)dbEntityType
{
    MTEntityType tipo = kTEUnknown;
    
    switch (dbEntityType)
    {
        case TEDB_MEDICO:
            tipo = kTEMedico;
            break;
            
        case TEDB_INSTITUCION:
            tipo = kTEInstitucion;
            break;
            
        default:
            break;
    }
    
    self.tipoEntidad = tipo;
}


#pragma mark -
#pragma mark NSCopying Protocol methods
- (MTPrevistoActividad*)copyWithZone:(NSZone*)zone
{
    MTPrevistoActividad* newCopy;// = [[MTPrevistoActividad alloc] init];
    if ([super respondsToSelector:@selector(copyWithZone)])
        newCopy = [super copy];
    else
        newCopy = [[MTPrevistoActividad alloc] init];
    
//    newCopy = [super copy];
    
//    @property (nonatomic, retain) MTEntidad*    entidad;
//    @property (nonatomic, assign) MTEntityType  tipoEntidad;
//    @property (nonatomic, retain) NSDate*       entrada;
//    @property (nonatomic, retain) NSDate*       salida;

    newCopy.entidad     = [self.entidad copy];
    newCopy.tipoEntidad = self.tipoEntidad;
    newCopy.entrada     = [self.entrada copy];
    newCopy.salida      = [self.salida copy];
    
    return newCopy;
}

@end
