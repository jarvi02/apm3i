//
//  Service.m
//  WebGisMobile
//
//  Created by Develaris on 23/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Service.h"
#import "ASIHTTPRequest.h"

#define DEBUG   1

@implementation Service

@synthesize delegate=_delegate;

- (void)dealloc
{
    [_request clearDelegatesAndCancel];
    [_request release];

    [super dealloc];
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    [self logResponse:[request responseString]];
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    if (DEBUG) {
        if (DEBUG_TRANSMICIONX) NSLog(@"Request Error [ %@ ]",[[request error] localizedDescription]);
    }
    
    [self.delegate requestFailed:[NSString stringWithFormat:@"Request Error [ %@ ]",[[request error] localizedDescription]]];

}

- (void) cancel {
    
    [self.request clearDelegatesAndCancel];
}


#pragma mark -
#pragma LOGS Methods
- (void) logUrl:(NSString*)str {
    if (DEBUG) {
        if (DEBUG_TRANSMICIONX) NSLog(@"%@ - Service Call Url [ %@ ]",[self textoParaLog] ,str);
    }
}


- (NSString*)textoParaLog {
    
    return @"Service";
}

- (void) logResponse:(NSString*)str{
    if (DEBUG) {
        if (DEBUG_TRANSMICIONX) NSLog(@"%@ - Response [ %@ ]",[self textoParaLog] ,str);
    }
}

@end
