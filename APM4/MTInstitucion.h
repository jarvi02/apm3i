//
//  MTInstitucion.h
//  APM4
//
//  Created by Ezequiel Manacorda on 2/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTEntidad.h"

@interface MTInstitucion : MTEntidad <NSCopying>

- (MTInstitucion*)copyWithZone:(NSZone*)zone;

+(NSArray*) GetAll;

@end
