//
//  SeleccionZonaViewController.h
//  APM4
//
//  Created by Laura Busnahe on 8/27/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MTNodo.h"

@interface SeleccionZonaViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) IBOutlet UITableView      *tblZonas;
@property (nonatomic, strong) IBOutlet UIBarButtonItem  *btnAtras;
@property (nonatomic, strong) IBOutlet UISearchBar      *searchBar;

@property (nonatomic, assign) id delegate;

@property (nonatomic, strong) NSMutableArray    *data;
@property (nonatomic, strong) NSMutableArray    *filteredData;

- (IBAction)tapAtras:(id)sender;

- (id)initWithDefaultNib;

@end
