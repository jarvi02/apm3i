//
//  FichaVisitasViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 19/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "FichaVisitasViewController.h"
#import "NSDate+extensions.h"
#import "VisitasViewController.h"
#import "VisitasOtrosRepViewController.h"
#import "FichaVisita.h"
#import "FichaVisitaOtrosRep.h"
#import "MTHistoricoVisitas.h"
#import "historicoVisitasTableController.h"


@interface FichaVisitasViewController ()

@property(nonatomic, retain) VisitasViewController              *visitasViewController;
@property(nonatomic, retain) VisitasOtrosRepViewController      *visitasOtrosRepViewController;
@property(nonatomic, strong) historicoVisitasTableController    *historicoTableViewController;

-(void)crearToolBar;
@end

@implementation FichaVisitasViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Ficha de visita";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ((UILabel*)[self.view viewWithTag:TAG_TITULO]).text = self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre;

    ((UILabel*)[self.view viewWithTag:TAG_TITULO_HEADER]).text = [NSString stringWithFormat:@"%@ - %@",self.title, self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre];
    

    NSInteger idDoctor = self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico;
    NSArray *array;
    
    array = [FichaVisita GetAllByMedico:idDoctor];
    
//    self.visitasViewController = [[[VisitasViewController alloc] initWithData:array] autorelease];
    self.visitasViewController = [[VisitasViewController alloc] initWithData:array];
    self.visitasViewController.navigationControllerReference = self.navigationController;  // tuve que hacer esto para que
    self.visitasViewController.carteraViewControllerReference = self.carteraViewControllerReference;
    self.tablaVisitas.delegate = self.visitasViewController;
    self.tablaVisitas.dataSource = self.visitasViewController;
    
    
    array = [FichaVisitaOtrosRep GetAllByMedico:idDoctor];
//    self.visitasOtrosRepViewController = [[[VisitasOtrosRepViewController alloc] initWithData:array] autorelease];
    self.visitasOtrosRepViewController = [[VisitasOtrosRepViewController alloc] initWithData:array];
    self.tablaOtrosRepresentantes.delegate = self.visitasOtrosRepViewController;
    self.tablaOtrosRepresentantes.dataSource = self.visitasOtrosRepViewController;
    
    array = [MTHistoricoVisitas getAllForMedico:idDoctor];
    self.historicoTableViewController   = [[historicoVisitasTableController alloc] initWithData:array];
    self.historicoTable.delegate        = self.historicoTableViewController;
    self.historicoTable.dataSource      = self.historicoTableViewController;

    [self crearToolBar];
}

-(void)viewWillAppear:(BOOL)animated
{
    NSInteger idDoctor = self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico;
    NSArray *array;
    
    array = [FichaVisita GetAllByMedico:idDoctor];
    [self.visitasViewController updateData:array];
    
    array = [FichaVisitaOtrosRep GetAllByMedico:idDoctor];
    [self.visitasOtrosRepViewController updateData:array];
    
    [self.tablaVisitas              reloadData];
    [self.tablaOtrosRepresentantes  reloadData];
    
    
    // Le vuelvo a decir, qué tamaño tiene la tabla
    if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        // Portrait
        self.historicoView.contentSize = self.historicoTable.frame.size;
        
    } else if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        // Landscape
        self.historicoView.contentSize = self.historicoTable.frame.size;
    }
}

- (void)viewWillDisappear:(BOOL)animated;
{
    if (self.tablaVisitas.isEditing)
        [self.tablaVisitas setEditing:NO animated:NO];
    
    if (self.tablaOtrosRepresentantes.isEditing)
        [self.tablaOtrosRepresentantes setEditing:NO animated:NO];
    
    if (self.historicoTable.isEditing)
        [self.historicoTable setEditing:NO animated:NO];
    
    [super viewWillDisappear:animated];
}

-(void)crearToolBar {
    // flex item used to separate the left groups items and right grouped items
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    
    UIBarButtonItem *editarItem = [[UIBarButtonItem alloc] initWithTitle:@"Editar"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(editar:)];
    editarItem.tintColor = UIColorFromRGB(defaultButtonColor);
    
    
    NSArray *items = [NSArray arrayWithObjects:editarItem, flexItem, nil];
    [self.toolBar setItems:items animated:NO];
    
//    [flexItem release];
//    [editarItem release];
    
}

-(void)editar:(id)sender
{
    if (self.tablaVisitas.editing == NO)
    {
        [self.tablaVisitas setEditing:YES animated:YES];
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:0]).tintColor = [UIColor blueColor];
        
    } else
    {
        [self.tablaVisitas setEditing:NO animated:YES];
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:0]).tintColor = [UIColor grayColor];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)getTitleForHeader{
    return @"Ficha de visita";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"cartera_48"];
}

//- (void)dealloc {
//    
//    [_visitasViewController release];
//    [_visitasOtrosRepViewController release];
//    
//    [_tablaVisitas release];
//    [_tablaOtrosRepresentantes release];
//    [_toolBar release];
//    [_historicoView release];
//    [_visitaView release];
//    [super dealloc];
//}


#pragma mark -
#pragma mark Rotations methods

// Notifies when rotation begins, reaches halfway point and ends.
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
{
    // Tengo que deshabilitar la interacción con el usuario o al rotarlo descajeta todo.
    [self.historicoView setUserInteractionEnabled:NO];
    [self.historicoTable setUserInteractionEnabled:NO];
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    // Tengo que deshabilitar la interacción con el usuario o al rotarlo descajeta todo.
    [self.historicoView setUserInteractionEnabled:YES];
    [self.historicoTable setUserInteractionEnabled:YES];
    
    if(UIInterfaceOrientationIsPortrait(fromInterfaceOrientation))
    {
        // Landscape
        self.historicoView.contentSize = self.historicoTable.frame.size;
        
    } else if(UIInterfaceOrientationIsLandscape(fromInterfaceOrientation))
    {
        // Portrait
        self.historicoView.contentSize = self.historicoTable.frame.size;
    }
}


#pragma mark -
#pragma mark Tap methods

- (IBAction)tapSegments:(id)sender
{
    if (self.btnSegment.selectedSegmentIndex == 0)
    {
        // Muestro la vista de Visitas
        [self.visitaView setHidden:NO];
        [self.historicoView setHidden:YES];
    } else
    {
        // Muestro la vista de Histórico de visitas
        [self.visitaView setHidden:YES];
        [self.historicoView setHidden:NO];
    }
}

@end
