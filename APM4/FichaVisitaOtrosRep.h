//
//  FichaVisitaOtrosRep.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 09/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FichaVisitaOtrosRep : NSObject

@property(nonatomic, retain) NSString *fecha;
@property(nonatomic, retain) NSString *visitador;


+(NSArray*)GetAllByMedico:(NSInteger)idMedico;

@end
