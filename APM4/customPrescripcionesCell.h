//
//  customPrescripcionesCell.h
//  APM4
//
//  Created by Laura Busnahe on 3/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customPrescripcionesCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblProducto;
@property (retain, nonatomic) IBOutlet UILabel *lblTrimestre1;
@property (retain, nonatomic) IBOutlet UILabel *lblTrimestre2;
@property (retain, nonatomic) IBOutlet UILabel *lblTrimestre3;
@property (retain, nonatomic) IBOutlet UILabel *lblTrimestre4;
@property (retain, nonatomic) IBOutlet UILabel *lblTotal;
@property (retain, nonatomic) IBOutlet UILabel *lblMercado;
@end
