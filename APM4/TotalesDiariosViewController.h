//
//  TotalesDiariosViewController.h
//  APM4
//
//  Created by Laura Busnahe on 7/29/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "customTotalDiarioCell.h"
#import "customTotalDiarioHeader.h"
#import "customTotalDiarioFooter.h"

@interface TotalesDiariosViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray                       *data;

@property (retain, nonatomic) IBOutlet UITableView          *tableView;
@property (nonatomic, strong) UINib                         *cellNib;
@property (nonatomic, strong) IBOutlet customTotalDiarioCell    *tableCell;
@property (nonatomic, strong) IBOutlet customTotalDiarioHeader  *tableHeader;
@property (retain, nonatomic) IBOutlet customTotalDiarioFooter *tableFooter;


- (id)initViewDefaultNib;

@end
