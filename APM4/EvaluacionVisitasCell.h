//
//  EvaluacionVisitasCell.h
//  APM4
//
//  Created by Laura Busnahe on 9/4/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EvaluacionVisitasCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblFecha;
@property (retain, nonatomic) IBOutlet UILabel *lblEvaluacion;

@end
