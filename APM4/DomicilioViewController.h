//
//  DomicilioViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABMMedicoViewController.h"
#import "CustomViewController.h"
#import "CloseModalViewControllerDelegate.h"
//#import "TableForComboPopoverViewController.h"
#import "popListExtended.h"

@interface DomicilioViewController : CustomViewController <CloseModalViewControllerDelegate, //TableForComboPopoverViewControllerDelegate,
                                                           UITextFieldDelegate, UIAlertViewDelegate, MTpopListViewDelegate>


@property (retain, nonatomic) MTpopListView *popupMenu;
@property(nonatomic,assign) ABMMedicoViewController *abmMedicoReference;

@property (retain, nonatomic) IBOutlet UILabel *lblProvincia;
@property (retain, nonatomic) IBOutlet UILabel *lblLocalidad;



@property (retain, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (retain, nonatomic) IBOutlet UITextField *txtTipo;
- (IBAction)tipo:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *txtInstitucional;
- (IBAction)institucional:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *txtCargo;
- (IBAction)cargo:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *txtProvincia;
- (IBAction)provincia:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *txtLocalidad;
- (IBAction)localidad:(id)sender;


- (IBAction)calles:(id)sender;


@property (retain, nonatomic) IBOutlet UITextField *txtCalle;
@property (retain, nonatomic) IBOutlet UITextField *txtNumero;
@property (retain, nonatomic) IBOutlet UITextField *txtCP;
@property (retain, nonatomic) IBOutlet UITextField *txtPiso;
@property (retain, nonatomic) IBOutlet UITextField *txtDto;

@property (retain, nonatomic) IBOutlet UIButton *btnConsulrotioExterno;
- (IBAction)editTxtNumero:(id)sender;

@property (retain, nonatomic) IBOutlet UITextField *txtUtilidad;
- (IBAction)utilidad:(id)sender;

- (IBAction)consultorioExterno:(id)sender;



@property (retain, nonatomic) IBOutlet UITextField *txtTelefono;
- (IBAction)telefono:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *txtHorario;
- (IBAction)horario:(id)sender;


@property (retain, nonatomic) IBOutlet UIToolbar *toolBar;
@property (retain, nonatomic) IBOutlet UITableView *tableView;

-(void) closeModalView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andMode:(MTEditActionsType) modo;


-(void)guardar:(id)sender;
-(void)cancelar:(id)sender;
-(void)editar:(id)sender;
-(void)agregar:(id)sender;

@end
