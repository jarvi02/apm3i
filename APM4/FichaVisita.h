//
//  FichaVisita.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 09/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FichaVisita : NSObject


@property(nonatomic, assign) NSInteger idMedico;

@property(nonatomic, assign) NSInteger idVisita;
@property(nonatomic, retain) NSString  *fecha;
@property(nonatomic, retain) NSString  *descripcion;
@property(nonatomic, retain) NSDate    *fechaAsDate;
@property(nonatomic, assign) NSInteger idTipo;
@property(nonatomic, assign) NSInteger visitador;
@property(nonatomic, assign) NSInteger idDomicilio;
@property(nonatomic, retain) NSString *objetivos;
@property(nonatomic, retain) NSString *evaluacion;
@property(nonatomic, assign) NSInteger contacto;
@property(nonatomic, retain) NSString *pipe;
@property(nonatomic, retain) NSString *transmitido;
@property(nonatomic, retain) NSString *actividad;
@property(nonatomic, assign) NSInteger asignacion;
@property(nonatomic, retain) NSString *objetivosProximo;


+(NSArray*)GetAllByMedico:(NSInteger)idMedico;


@end
