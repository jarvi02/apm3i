//
//  MTGrupos.h
//  APM4
//
//  Created by Laura Busnahe on 6/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTEditActions.h"
@class MTGrupos;

@interface MTGrupos : NSObject

@property (nonatomic, assign) NSUInteger        recID;
@property (nonatomic, strong) NSString          *descripcion;
@property (nonatomic, assign) MTEditActionsType action;
@property (nonatomic) BOOL blocked;

- (NSString*)   getDescription;
- (MTGrupos*)copyWithZone:(NSZone*)zone;

// Métodos para GST_GRUPOS
- (BOOL)        existeGrupo:(NSString*)nombreGrupo;
- (NSString*)   isOkToSave;
- (void)        saveAllData;
- (void)        updateAllData;
- (void)        deleteAllData;
+ (NSArray*)    getAll;


// Métodos para GST_GRUPOMEDICOS
- (BOOL)        existeGrupoForMedico:(NSUInteger)idMedico;
- (BOOL)        saveGrupoForMedico:(NSUInteger)idMedico;
- (void)        deleteGrupoForMedico:(NSUInteger)idMedico;
- (void)        deleteGrupoForAllMedico;
+ (NSArray*)    getAllForMedico:(NSUInteger)idMedico;

@end
