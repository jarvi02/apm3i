//
//  customAjusteMMCell.h
//  APM4
//
//  Created by Laura Busnahe on 4/3/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customAjusteMMCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblProducto;
@property (retain, nonatomic) IBOutlet UILabel *lblCantidad;
@property (retain, nonatomic) IBOutlet UILabel *lblCodigo;
@property (retain, nonatomic) IBOutlet UILabel *lblLote;
@property (retain, nonatomic) IBOutlet UILabel *lblFecha;
@end
