//
//  Promocionados.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 25/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Promocionados : NSObject

@property(nonatomic, retain) NSString *descripcion;
@property(nonatomic, assign) NSInteger orden;
@property(nonatomic, assign) NSInteger muestras;
@property(nonatomic, retain) NSString *literatura;
@property(nonatomic, retain) NSString *obsequio;
@property(nonatomic, assign) NSInteger fechaDisplay;


+(NSArray*)GetAllForDoctor:(NSInteger)idDoctor;

@end
