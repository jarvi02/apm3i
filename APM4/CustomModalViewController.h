//
//  CustomModalViewController.h
//  RnR
//
//  Created by Fabian E. Pezet Vila on 27/11/12.
//  Copyright (c) 2012 Fabian E. Pezet Vila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloseModalViewControllerDelegate.h"

@interface CustomModalViewController : UIViewController

@property(nonatomic, assign) id<CloseModalViewControllerDelegate> delegate;

-(UIImage*)getBackgroundImage;

-(void) alertTitle:(NSString*) titulo mensaje:(NSString*) mensaje boton:(NSString*) boton;

@end
