//
//  MedicosVisitadosPorFechaViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MedicosVisitadosPorFechaViewController.h"
#import "DatosComboMedicosVisitadosPorFecha.h"
#import "MedicosVisitadosPorFecha.h"
#import "AppDelegate.h"
#import "TableForComboPopoverViewController.h"
#import "EntryForCombo.h"
#import "NSDate+extensions.h"


@interface MedicosVisitadosPorFechaViewController ()

@end

@implementation MedicosVisitadosPorFechaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Médicos visitados por Fecha";
    }
    return self;
}

- (void)dealloc {
    
    [_dataCombo release];
    [_data      release];
    
    [_tmpCell release];
	[_cellNib release];
    
    [_table release];
    
    [_txtFiltro release];
    [super dealloc];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.table.rowHeight = 44.0;
    
    self.txtFiltro.text = @"TODO";
    
	self.cellNib = [UINib nibWithNibName:@"CusromMedicosVisitadosPorFechaCell" bundle:nil];
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
}

- (void) viewDidAppear:(BOOL)animated
{
    self.dataCombo = [DatosComboMedicosVisitadosPorFecha GetAll];
    
    self.data = [MedicosVisitadosPorFecha GetAllWithFilter:1];

    [self.loadingView hide];
    
    [self.table reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)getTitleForHeader{
    return @"Médicos visitados por Fecha";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"cartera_48"];
}


#pragma mark -
#pragma mark Table Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of time zone names.
	return [self.data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MyIdentifier = @"CMIdentifier";
	
    // Obtain the cell object.
	CusromMedicosVisitadosPorFechaCell *cell = (CusromMedicosVisitadosPorFechaCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	// If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
		cell = self.tmpCell;
		self.tmpCell = nil;
        
        
    }
    
    MedicosVisitadosPorFecha *o = [self.data objectAtIndex:indexPath.row];
    
    NSDate *dateExpires = [NSDate  dateWithTimeIntervalSince1970:o.fechaDisplay ];
    
    dateExpires = dateExpires.dateAsDateWithoutTime;
    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"dd/MM/yyyy"];
//    
//    cell.fecha.text = [formatter stringFromDate:dateExpires];
//    [formatter release];
    cell.fecha.text = [dateExpires formattedStringUsingFormat:@"dd/MM/yyyy"];

    cell.nombre.text = o.apellidoNombre;
    cell.tipoVisita.text = o.tipoVisita;
    cell.actividad.text = o.actividad;
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
       
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGRect frame = CGRectMake(0, 0, 844, 22);
    UILabel *background = [[UILabel alloc] initWithFrame:frame];
    background.autoresizingMask = 34;
    background.backgroundColor = UIColorFromRGB(defaultHeaderCellColor);
 
    UILabel *label;
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(20, 0, 102, 20);
    label.autoresizingMask = 36;
    label.text = @"Fecha";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];

    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(113, 0, 474, 20);
    label.autoresizingMask = 36;
    label.text = @"Apellido y nombre";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];

    [background addSubview:label];

    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(669, 0, 99, 20);
    label.autoresizingMask = 37;
    label.text = @"Tipo visita";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];

    [background addSubview:label];

    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(756, 0, 105, 20);
    label.autoresizingMask = 33;
    label.text = @"Actividad";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    return [background autorelease];
}


- (IBAction)showComboDate:(id)sender
{
    
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:0];
    EntryForCombo *e;
    int i;
    
    e = [[EntryForCombo alloc] initWithValue:@"TODO"  forIndex:0];
    [arr addObject:e];
    [e release];
    
    for (i=0; [self.dataCombo count] > i; i++) {
        
        DatosComboMedicosVisitadosPorFecha *o = [self.dataCombo objectAtIndex:i];
    
        e = [[EntryForCombo alloc] initWithValue:[o.fecha formattedStringUsingFormat:@"dd/MM/yyyy"] forIndex:i+1];

        [arr addObject:e];
        [e release];
    }
    
    CGSize size = CGSizeMake(200, 400);
    TableForComboPopoverViewController *t = [[TableForComboPopoverViewController alloc] initWithData:arr titulo:@"Fecha" size:size delegate:self andCaller:@""];
    
    [arr release];
    
    Delegate.popoverController = [[[UIPopoverController alloc] initWithContentViewController:t] autorelease];
    [t release];
    
    [Delegate.popoverController presentPopoverFromRect: ((UIButton*)sender).frame inView:self.view permittedArrowDirections:YES animated:YES];

    
}

-(void)selectedIndex:(NSInteger)index caller:(NSString*)caller{
    
    [Delegate dismissPopOver:YES];
    
    NSInteger  filtro;
    if (index == 0) {
        filtro = 1;
        self.txtFiltro.text = @"TODO";
        
    } else {
        filtro = [((DatosComboMedicosVisitadosPorFecha*)[self.dataCombo objectAtIndex:index-1]).fecha timeIntervalSince1970];
        self.txtFiltro.text = [((DatosComboMedicosVisitadosPorFecha*)[self.dataCombo objectAtIndex:index-1]).fecha formattedStringUsingFormat:@"dd/MM/yyyy"];
    }
    
    self.data = [MedicosVisitadosPorFecha GetAllWithFilter:filtro];
	
    [self.table reloadData];
    
}

@end
