//
//  DB.h
//  CRUD
//
//  Created by Fabian Eduardo Pezet Vila on 12/06/12.
//  Copyright (c) 2012 Southopia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

/*
 * Esta clase implemeta un singleton, hacer un getInstance y usar 
 * En el metodo applicationWillTerminate del Delegate colocar  [[Database getInstance] close];
 */

@interface DB : NSObject {
    
@private	
	sqlite3 *dbh;
}

+(DB*)getInstance;

-(sqlite3_stmt *) prepare:(NSString *) sql;
-(void) excecuteSQL:(NSString*)_sql;
- (int)getLastInsertedId;
-(sqlite3 *) dbh;
-(void) close;

@end
