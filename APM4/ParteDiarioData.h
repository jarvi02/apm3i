//
//  ParteDiarioData.h
//  APM4
//
//  Created by Juan Pablo Garcia on 20/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParteDiarioData : NSObject

@property (nonatomic, retain) NSString *descripcion;
@property (nonatomic, retain) NSString *fecha;
@property (nonatomic) NSUInteger cantidad;
@property (nonatomic, retain) NSString *actividad;
@property (nonatomic) NSUInteger nodo;
@property (nonatomic) NSUInteger id_nodotareaspartediario;
@property (nonatomic) NSUInteger partediario;
@property (nonatomic) NSUInteger tarea;
@property (nonatomic, retain) NSString *pipe;
@property (nonatomic, retain) NSString *registro;

-(BOOL)isTarde;

@end
