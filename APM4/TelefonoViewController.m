//
//  TelefonoViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "TelefonoViewController.h"
#import "Telefono.h"

#import "Config.h"
#import "NSStringExtended.h"

@interface TelefonoViewController ()
@property(nonatomic, retain) NSMutableArray *telefonosOriginales;
-(void)crearToolBar;

@end

@implementation TelefonoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.txtPais.text = SettingForKey(PAIS, @"54");
    self.telefonosOriginales = [NSMutableArray array];
    for (Telefono *o in self.domicilioReference.telefonos) {
        [self.telefonosOriginales addObject:[o copy]];
    }
    
    [self crearToolBar];
}

-(void)crearToolBar {
    
    // flex item used to separate the left groups items and right grouped items
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    // create a special tab bar item with a custom image and title
    UIBarButtonItem *guardarItem = [[UIBarButtonItem alloc] initWithTitle:@"Guardar"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(guardar:)];
    //guardarItem.tintColor = [UIColor grayColor];
    guardarItem.width = 70;
    
    UIBarButtonItem *editarItem = [[UIBarButtonItem alloc] initWithTitle:@"Editar"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(editar:)];
    //editarItem.tintColor = [UIColor grayColor];
    editarItem.width = 70;
    
    UIBarButtonItem *cancelarItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar"
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self
                                                                    action:@selector(cancelar:)];
    //cancelarItem.tintColor = [UIColor grayColor];
    cancelarItem.width = 70;
    
    UIBarButtonItem *agregarItem = [[UIBarButtonItem alloc] initWithTitle:@"Agregar"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(agregar:)];
//    UIBarButtonItem *agregarItem = [[UIBarButtonItem alloc] initWithTitle:@"Agregar"
//                                                                    style:UIBarButtonSystemItemAdd
//                                                                   target:self
//                                                                   action:@selector(agregar:)];

    //agregarItem.tintColor = [UIColor darkGrayColor];
    agregarItem.width = 70;
    
    
    
    NSArray *items = [NSArray arrayWithObjects: guardarItem, editarItem, cancelarItem, flexItem, agregarItem, nil];
    [self.tollBar setItems:items animated:NO];
    
    
}

-(void)guardar:(id)sender{
    
    // agrego la coleccion de la tabla a los medicos
    if ([self.domicilioReference.telefonos count] >0)
    {
        Telefono *t = nil;
        t = [self.domicilioReference.telefonos objectAtIndex:0];
        self.domicilioViewControllerReference.txtTelefono.text = [t descripcion];
    } else {
        self.domicilioViewControllerReference.txtTelefono.text = @"";
    }
    
    [self.delegate closeModalView];
}

-(void)cancelar:(id)sender{
    
    [self.domicilioReference.telefonos removeAllObjects];
    
    for (Telefono *o in self.telefonosOriginales) {
        [self.domicilioReference.telefonos addObject:[o copy]];
    }
    
    [self.delegate closeModalView];
    
}

-(void)editar:(id)sender{
    
    
    if (self.tableView.editing == NO) {
        [self.tableView setEditing:YES animated:YES];
        ((UIBarButtonItem *)[self.tollBar.items objectAtIndex:1]).tintColor = UIColorFromRGB(defaultEditModeButtonColor);
        
        ((UIBarButtonItem *)[self.tollBar.items objectAtIndex:0]).enabled =NO;
        ((UIBarButtonItem *)[self.tollBar.items objectAtIndex:2]).enabled =NO;
        ((UIBarButtonItem *)[self.tollBar.items objectAtIndex:4]).enabled =NO;
        
        
    } else {
        [self.tableView setEditing:NO animated:YES];
        ((UIBarButtonItem *)[self.tollBar.items objectAtIndex:1]).tintColor = ((UIBarButtonItem *)[self.tollBar.items objectAtIndex:2]).tintColor;
        
        ((UIBarButtonItem *)[self.tollBar.items objectAtIndex:0]).enabled =YES;
        ((UIBarButtonItem *)[self.tollBar.items objectAtIndex:2]).enabled =YES;
        ((UIBarButtonItem *)[self.tollBar.items objectAtIndex:4]).enabled =YES;
        
    }
    
    
}


-(void)agregar:(id)sender{
    
    
    //NSString *str = [NSString stringWithFormat:@"%@ %@ %@", self.txtPais.text, self.txtCodigoArea.text, self.txtTelefono.text];
    
    Telefono *h = [[Telefono alloc] init];
    h.pais = [self.txtPais.text integerValue];
    h.area = [self.txtCodigoArea.text integerValue];
    h.numero = [NSString string:self.txtTelefono.text ifNull:@""];
    
    [self.domicilioReference.telefonos addObject:h];
    
    self.txtCodigoArea.text = @"";
    self.txtTelefono.text = @"";
    
    [self.tableView reloadData];
}


#pragma - mark
#pragma UItableView Delegate and DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.domicilioReference.telefonos count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    Telefono *h = [self.domicilioReference.telefonos objectAtIndex:indexPath.row];
    cell.textLabel.text = [h descripcion];
    cell.textLabel.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.domicilioReference.telefonos removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
    
    NSString *ingredient = [self.domicilioReference.telefonos objectAtIndex:fromIndexPath.row];
    
    [self.domicilioReference.telefonos removeObjectAtIndex:fromIndexPath.row];
    
    [self.domicilioReference.telefonos insertObject:ingredient atIndex:toIndexPath.row];
    
    
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    NSString *str = [self.domicilioReference.telefonos objectAtIndex:indexPath.row];
//    NSArray *arr = [str componentsSeparatedByString:@" "];

    Telefono *t = [self.domicilioReference.telefonos objectAtIndex:indexPath.row];
    
    self.txtCodigoArea.text = [NSString intToStr:t.area];
    self.txtTelefono.text = [NSString string:t.numero ifNull:@""];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark TextField methods
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}

// En vistas que se presentan de forma modal es necesario agregar este override para que oculte el teclado
// con resignFirstResponder
- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

@end
