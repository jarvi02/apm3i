//
//  FilterTableDelegate.m
//  APM4
//
//  Created by Juan Pablo Garcia on 25/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "FilterTableDelegate.h"

@implementation FilterTableDelegate

#pragma mark -
#pragma mark UITableViewDelegate & UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of time zone names.
	return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
    }
    
    switch([indexPath row])
    {
        case 0:
            cell.textLabel.text = @"Programados";
            break;
        case 1:
            cell.textLabel.text = @"Fluctuante";
            break;
        case 2:
            cell.textLabel.text = @"Mañana";
            break;
        case 3:
            cell.textLabel.text = @"Tarde";
            break;
        case 4:
            cell.textLabel.text = @"Visitados";
            break;
        case 5:
            cell.textLabel.text = @"No Visitados";
            break;
        case 6:
            cell.textLabel.text = @"Altas";
            break;
        case 7:
            cell.textLabel.text = @"Bajas";
            break;
        case 8:
            cell.textLabel.text = @"Parcialmente Visitados";
            break;
    }
    
    // Marcados
    NSNumber *current = [NSNumber numberWithInt:[indexPath row]];
    
    cell.textLabel.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
    
    if ([self.currentFilters containsObject:current])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSNumber *current = [NSNumber numberWithInt:[indexPath row]];
    if ([self.currentFilters containsObject:current])
    {
        // Lo deselecciono
        [self.currentFilters removeObject:current];
        if (self.delegate != nil)
        {
            [self.delegate filterSelected:[current integerValue] selected:NO];
        }
    }
    else
    {
        // Lo selecciono
        [self.currentFilters addObject:current];
        if (self.delegate != nil)
        {
            [self.delegate filterSelected:[current integerValue] selected:YES];
        }
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView reloadData];
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    
//    CGRect frame = CGRectMake(0, 0, 844, 22);
//    UILabel *background = [[UILabel alloc] initWithFrame:frame];
//    background.autoresizingMask = 34;
//    background.backgroundColor = [UIColor blackColor];
//    
//    UILabel *label;
//    
//    label = [[[UILabel alloc] init] autorelease];
//    label.frame = CGRectMake(5, 0, 102, 20);
//    label.autoresizingMask = 36;
//    label.text = @"Filtros";
//    label.textColor = [UIColor whiteColor];
//    label.font = [UIFont boldSystemFontOfSize:14.0f];
//    label.backgroundColor = [UIColor clearColor];
//    
//    [background addSubview:label];
//
//    
//    return [background autorelease];
//}

-(void)dealloc
{
    [_currentFilters release];
    
    [super dealloc];
}


@end
