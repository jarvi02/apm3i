//
//  MTMercadoProductos.h
//  APM4
//
//  Created by Laura Busnahe on 3/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTMercadoProductos : NSObject

@property                    NSInteger  auditoria;
@property                    NSInteger  mercado;
@property                    NSInteger  producto;
@property                    NSInteger  total;

+ (NSArray*)getByAuditoria:(NSInteger)idAuditoria Mercados:(NSArray*)arrayMercados;

@end
