//
//  NSDate+extensions.m
//  APM4
//
//  Created by Ezequiel Manacorda.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "NSDate+extensions.h"
#import "NSStringExtended.h"

@implementation NSDate (extended)

- (id) initWithTimeString:(NSString*)sTime
{
    return [self initWithString:sTime withFormat:@"HH:mm"];
}


- (id) initWithString:(NSString*)sTime withFormat:(NSString*)dFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale* enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:enUSPOSIXLocale];
    [formatter setDateFormat:dFormat];
    
    self = [formatter dateFromString:sTime];
    
   
    
    return self;
}

-(NSDate *) addDays:(NSUInteger) daysToAdd
{
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:daysToAdd];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    return [gregorian dateByAddingComponents:components toDate:self options:0];
}

+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    return [calendar dateFromComponents:components];
}

-(BOOL)isWeekEnd
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange weekdayRange = [calendar maximumRangeOfUnit:NSWeekdayCalendarUnit];
    NSDateComponents *components = [calendar components:NSWeekdayCalendarUnit fromDate:self];
    NSUInteger weekdayOfDate = [components weekday];
    
    if (weekdayOfDate == weekdayRange.location || weekdayOfDate == weekdayRange.length) {
        return YES;
    }
    else
        return NO;
}

+ (NSDate *)dateWithoutTime
{
    return [[NSDate date] dateAsDateWithoutTime];
}
- (NSDate *)dateAsDateWithoutTime
{
    NSString *formattedString = [self formattedDateString];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale* enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:enUSPOSIXLocale];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    NSDate *ret = [formatter dateFromString:formattedString];
    return ret;
}

- (int)differenceInDaysTo:(NSDate *)toDate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [gregorian components:NSDayCalendarUnit
                                                fromDate:self
                                                  toDate:toDate
                                                 options:0];
    NSInteger days = [components day];
    return days;
}

- (NSString *)formattedDateString
{
    return [self formattedStringUsingFormat:@"MMM dd, yyyy"];
}

- (NSString *)formattedStringUsingFormat:(NSString *)dateFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale* enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:enUSPOSIXLocale];
    [formatter setDateFormat:dateFormat];
    NSString *ret = [formatter stringFromDate:self];
    return ret;
}



- (NSString*)datetimeForNovedad
{
    NSString *result = @"";
    
    if (([self timeIntervalSince1970] > 0) || ([self timeIntervalSince1970] < 0))
        result = [self formattedStringUsingFormat:@"MM/dd/yyyy HH:mm"];
    
    return result;
}

- (double)datetimeForDB
{
    double result = 0;
    
    result = [self timeIntervalSince1970];
    
    return result;
}

- (NSString*)datetimeForDBAsString
{
    NSString *result = @"0";
    
    result = [NSString stringWithFormat:@"%f", [self timeIntervalSince1970]];
    
    return result;
}


- (NSString*)timeToString
{
    NSString *result= @"00:00";
    
    result = [self formattedStringUsingFormat:@"HH:mm"];
    
    return result;
}

+ (NSDate*)stringToTime:(NSString*)sTime
{
    NSDate *result = [NSDate stringToTime:sTime withFormat:@"HH:mm"];
    return result;
}

+ (NSDate*)stringToTime:(NSString*)sTime withFormat:(NSString*)dFormat
{
    NSDate *result = [[NSDate alloc] initWithString:sTime withFormat:dFormat];
    return result;
}

- (NSString*)datetimeAsSQLiteString;
{
    NSString *result = @"";
    
    result = [self formattedStringUsingFormat:@"yyyy-MM-dd HH:mm"];
    
    return result;
}

-(NSDate*) substractDays:(NSUInteger) dayAmount
{
    
    double dateInSeconds = [self timeIntervalSince1970];
    
    // Resto la cantidad de segundos correspondientes a los dias dayAmount
    dateInSeconds -= dayAmount * (3600 *24);
    
    return [NSDate dateWithTimeIntervalSince1970:dateInSeconds];
}

+ (NSDate*)datetimeFromSQLiteString:(NSString*)sDate
{
    NSDate *result = nil;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale* enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:enUSPOSIXLocale];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    result = [formatter dateFromString:sDate];
    
    return result;
}


+ (NSDate*)datetimeFromDB:(NSString*)sDate
{
    NSDate *result = nil;
    
    
    if ((!sDate) || ([sDate length] == 0))
        return result;
    
    
    // Chequeo si el String corresponde a un número.
    NSInteger miliSec = [sDate integerValue];
    if (miliSec > 0)
    {
        // Fecha en Milisegundos
        result = [NSDate dateWithTimeIntervalSince1970:miliSec];
        
    } else
    {
        // Fecha en formato texto yyyy-MM-dd HH:mm
        result = [NSDate stringToTime:sDate withFormat:@"yyyy-MM-dd HH:mm"];
    }
    
    return result;
    
}

+ (BOOL)uses24Hs
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    BOOL is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    
    return is24h;
}

@end
