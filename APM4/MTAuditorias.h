//
//  MTAuditorias.h
//  APM4
//
//  Created by Laura Busnahe on 2/28/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTAuditorias : NSObject

@property                    NSInteger  RecID;
@property(nonatomic, retain) NSString*  descripcion;
@property(nonatomic, retain) NSDate*    actualizacion;

- (NSString*)getDescription;


+ (NSArray*)getAll;

@end
