//
//  MasterViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 24/01/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuViewController.h"
#import "CustomViewController.h"
#include "ServiceLogin.h"
#include "commandMasterView.h"

@interface MasterViewController : CustomViewController <MenuContainerProtocol, ServiceDelegate, CommandMasterDelegate, UITextFieldDelegate>

@property(nonatomic, retain) ServiceLogin   *service;

@property (retain, nonatomic) IBOutlet UIView *loginView;
@property (retain, nonatomic) IBOutlet UITextField *txtUser;
@property (retain, nonatomic) IBOutlet UITextField *txtPassword;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
- (IBAction)performAccept:(id)sender;
- (IBAction)performCancel:(id)sender;

@property (retain, nonatomic) IBOutlet UILabel *lblVersion;
@end
