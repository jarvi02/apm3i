//
//  MTTiposDomicilios.m
//  APM4
//
//  Created by Laura Busnahe on 8/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTTiposDomicilios.h"
#import "DBExtended.h"

@implementation MTTiposDomicilios
- (id)init
{
    self = [super init];
    if (self)
    {
        self.recID       = 0;
        self.descripcion = @"";
    }
    return self;
}

- (NSString*)getDescription
{
    return [NSString string:self.descripcion ifNull:@""];
}

+ (NSArray*)getAll
{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql =
    @"SELECT id, descripcion "
    "FROM mtc_tiposdomicilio "
    "ORDER BY descripcion";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement )
    {
        MTTiposDomicilios *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            o = [[MTTiposDomicilios alloc] init];
            
            // id
            o.recID = sqlite3_column_int(statement, 0);
            
            // Descripción
            _c = (char *)sqlite3_column_text(statement, 1);
            o.descripcion = [NSString pCharToString:_c];
			
            [array addObject:o];
        }
        
	} else
    {
        NSAssert(0, @"No puede ejecutar: %@", sql);
	}
	sqlite3_finalize(statement);
    
    return array;
}


-(id)copy{
    
    MTTiposDomicilios *o = [[MTTiposDomicilios alloc] init];
    o.descripcion = [NSString string:self.descripcion ifNull:@""];
    o.recID = self.recID;
    
    return o;
}
@end
