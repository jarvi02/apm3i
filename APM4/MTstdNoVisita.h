//
//  MTstdNoVisita.h
//  APM4
//
//  Created by Laura Busnahe on 9/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTstdNoVisita : NSObject

@property (nonatomic, strong)NSString   *motivo;
@property (nonatomic, assign)NSInteger  maniana;
@property (nonatomic, assign)NSInteger  tarde;
@property (nonatomic, assign)NSInteger  total;

+ (NSArray*)getAll;

@end
