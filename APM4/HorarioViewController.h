//
//  HorarioViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABMMedicoViewController.h"
#import "CloseModalViewControllerDelegate.h"
#import "Domicilio.h"
#import "DomicilioViewController.h"

@interface HorarioViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UITextFieldDelegate>

@property(nonatomic,assign) ABMMedicoViewController *abmMedicoReference;
@property(nonatomic,assign) DomicilioViewController *domicilioViewControllerReference;
@property(nonatomic, assign) Domicilio *domicilioReference;
@property(nonatomic, assign) id<CloseModalViewControllerDelegate> delegate;

@property (retain, nonatomic) IBOutlet UIButton *btnLunes;
@property (retain, nonatomic) IBOutlet UIButton *btnMartes;
@property (retain, nonatomic) IBOutlet UIButton *btnMiercoles;
@property (retain, nonatomic) IBOutlet UIButton *btnJueves;
@property (retain, nonatomic) IBOutlet UIButton *btnViernes;



- (IBAction)lunes:(id)sender;
- (IBAction)martes:(id)sender;
- (IBAction)miercoles:(id)sender;
- (IBAction)jueves:(id)sender;
- (IBAction)viernes:(id)sender;


@property (retain, nonatomic) IBOutlet UITextField *txtHoraDesde;
@property (retain, nonatomic) IBOutlet UITextField *txtMinutoDesde;
@property (retain, nonatomic) IBOutlet UITextField *txtHoraHasta;
@property (retain, nonatomic) IBOutlet UITextField *txtMinutoHasta;
@property (retain, nonatomic) IBOutlet UITextView  *txtComentarios;

@property (retain, nonatomic) IBOutlet UITableView *tableview;
@property (retain, nonatomic) IBOutlet UIToolbar *toolBar;

-(void)guardar:(id)sender;
-(void)cancelar:(id)sender;
-(void)editar:(id)sender;
-(void)agregar:(id)sender;


@end
