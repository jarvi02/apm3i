//
//  MTstdVisitasPorDia.m
//  APM4
//
//  Created by Laura Busnahe on 9/11/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTstdVisitasPorDia.h"
#import "Config.h"
#import "DB.h"

@implementation MTstdVisitasPorDia

- (id)init
{
    self = [super init];
    if (self)
    {
        self.idVisita     = 0;
        self.fecha        = nil;
        self.fijos        = 0;
        self.fluctuantes  = 0;
        self.acompaniadas = 0;
        self.total        = 0;
    }
    
    return self;
}

+ (NSArray*)getAllForActivity:(MTActivitySelection) actividad
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql =
    @"SELECT "
        "med_visitas.id as _id, "
        "med_visitas.fecha as fecha, "
        "sum(case when med_medicos.asignacion = 1 then 1 else 0 end) as fijos, "
        "sum(case when med_medicos.asignacion = 2 then 1 else 0 end)as fluct, "
        "sum(case when med_visitas.tipo <> 1 then 1 else 0 end) as acompaniada, "
        "count(*) as Total "
    "FROM med_visitas "
        "INNER JOIN mtc_tiposvisita ON mtc_tiposvisita.id = med_visitas.tipo "
        "INNER JOIN med_medicos ON med_medicos.id = med_visitas.medico "
    "WHERE med_medicos.b = 0 AND "
        "med_medicos.nodo =  %@ AND "
        "med_visitas.nodo =  %@ AND "
        "mtc_tiposvisita.contacto = 1 AND "
        "med_medicos.actividad  in (%@)"
    "GROUP BY med_visitas.fecha";
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    
    // Genero el string para mostrar la activad seleccioanda únicamente.
    // Todas
    NSString *sActividad = @"'M', 'T'";
    // Mañana
    if (actividad == kMTActivityMorning)
        sActividad = @"'M'";
    // Tarde
    if (actividad == kMTActivityAfternon)
        sActividad = @"'T'";
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo, nodo, sActividad];
    
#ifdef DEBUG_VISITASPORDIAS
    NSLog(@"--- MTstdVisitasPorDia ---");
    NSLog(@" - query: %@", query);
    
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        NSInteger totalFijos = 0;
        NSInteger totalFluctuantes = 0;
        NSInteger totalAcompaniadas = 0;
        NSInteger totales = 0;
        
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTstdVisitasPorDia *o = [[MTstdVisitasPorDia alloc] init];
            
            // ID de visita
            o.idVisita = sqlite3_column_int(statement, 0);
            
            // Fecha
            _c = (char *)sqlite3_column_text(statement, 1);
            o.fecha = [[NSDate datetimeFromDB:[NSString pCharToString:_c]] dateAsDateWithoutTime];
            
            // Fijos
            o.fijos = sqlite3_column_int(statement, 2);
            
            // Fluctuantes
            o.fluctuantes = sqlite3_column_int(statement, 3);
            
            // Visitas Acompañadas
            o.acompaniadas = sqlite3_column_int(statement, 4);
            
            // Total
            o.total = sqlite3_column_double(statement, 5);
            
            // Sumo los valores para los totales.
            totalFijos        += o.fijos;
            totalFluctuantes  += o.fluctuantes;
            totalAcompaniadas += o.acompaniadas;
            totales           += o.total;
            
            [result addObject:o];
        }

        // Creo un objeto más con id 0 que contiene los totales
        MTstdVisitasPorDia *o = [[MTstdVisitasPorDia alloc] init];
        o.idVisita      = 0;
        o.fecha         = [NSDate distantFuture];
        o.fijos         = totalFijos;
        o.fluctuantes   = totalFluctuantes;
        o.acompaniadas  = totalAcompaniadas;
        o.total         = totales;

        // Agrego los totales al resultaod.
        [result addObject:o];
	} else
    {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}

@end
