//
//  popListView.m
//  APM3i
//
//  Created by Ezequiel Manacorda on 2/6/13.
//  Copyright (c) 2013 Ezequiel Manacorda. All rights reserved.
//

#import "popListView.h"
#import "popListViewController.h"
#import "MTEntidad.h"

@implementation MTpopListView
{
    NSIndexPath *selectedIndexPath;
}


- (id) initWithRect:(CGRect) rect
{
    self = [super init];
    
    if (self)
    {
        // Custom initialization
        self.rowData = nil;
        
        self.ViewController = [[popListViewController alloc] initWithNibName:@"popListView" bundle:nil];
        if (!self.popOver)
            self.popOver = [[UIPopoverController alloc] initWithContentViewController:self.ViewController];

        self.popOver.popoverContentSize = self.ViewController.view.frame.size;
        self.withSearch = FALSE;
        [self.ViewController useSearch:FALSE];
    }
    
    self.ViewController.TableView.delegate   = self;
    self.ViewController.TableView.dataSource = self;
    self.ViewController.searchBar.delegate   = self;
    self.ViewController.delegate = self;
    
    return self;
}

- (id) initWithArray:(NSArray*) list Rect:(CGRect) rect;
{
    self = [self initWithRect:rect];
    [self setTitleAndParameters:@"" rowData:list];
    
    return self;
}

- (void) setTitleAndParameters:(NSString*) title rowData:(NSArray*) data
{
    self.rowData = [NSArray arrayWithArray:data];
//    [rowData retain];
    self.rowDataFiltered = [NSMutableArray arrayWithArray:self.rowData];
    self.popTitle   = title;
    
    [self.ViewController.TableView reloadData];
}

- (void) setTitle:(NSString*)title
{
    self.popTitle   = title;
    self.rowDataFiltered = [NSMutableArray arrayWithArray:self.rowData];
    [self.ViewController.TableView reloadData];
}


- (void) setSize:(CGSize)size
{
    [self.ViewController.TableView  setFrame:CGRectMake(0, 0, size.width, size.height)];
    [self.ViewController.view       setFrame:CGRectMake(0, 0, size.width, size.height)];
    self.popOver.popoverContentSize = size;
    
    [self.ViewController.TableView reloadData];
}

- (void) useSearch:(BOOL) search
{
    self.withSearch = search;
    [self.ViewController useSearch:search];
}


- (void)presentPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated
{
    [self.popOver presentPopoverFromRect:rect inView:view permittedArrowDirections:arrowDirections animated:animated];
}

- (void)presentPopoverFromRectWithSender:(CGRect)rect
                                  Sender:(id)sender
                                  inView:(UIView *)view
                permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
                                animated:(BOOL)animated
{
    self.sender = sender;
    [self.popOver presentPopoverFromRect:rect inView:view permittedArrowDirections:arrowDirections animated:animated];
}


- (void)presentPopoverFromSender:(id)sender inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated
{
    // Obtengo el rect del sender
    UIView *senderView = (UIView *)sender;
    
    self.sender = sender;
    [self.popOver presentPopoverFromRect:senderView.frame inView:view permittedArrowDirections:arrowDirections animated:animated];
}

- (void)dismissPopoverAnimated:(BOOL)animated
{
    self.sender = nil;
    [self.popOver dismissPopoverAnimated:animated];
}

#pragma mark -
#pragma mark Lifecycle methods

- (void)viewDidLoad
{
    
	// create a filtered list that will contain products for the search results table.
	//self.rowDataFiltered = [NSMutableArray arrayWithCapacity:[rowData count]];
//    self.rowDataFiltered = [NSMutableArray arrayWithArray:rowData];
//		
//	[self.TableView reloadData];
}

- (void)viewDidUnload
{
	
}

- (void)viewWillAppear
{
    
}

- (void)viewWillDisappear
{
    if (selectedIndexPath)
    {
        NSString* cellText = @"";
        if ([self.delegate respondsToSelector:@selector(popListView:senderControl:didSelectRow:selectedText:)])
            [self.delegate popListView:self senderControl:self.sender didSelectRow:selectedIndexPath.row selectedText:cellText];
        
        if ([self.delegate respondsToSelector:@selector(popListView:senderControl:selObject:)])
            [self.delegate popListView:self senderControl:self.sender
                             selObject:[self.rowDataFiltered objectAtIndex:selectedIndexPath.row]];
    }

}


- (void)viewDidDisappear:(BOOL)animated
{
    if (selectedIndexPath)
    {
        NSString* cellText = @"";
        if (self.rowDataFiltered.count > 0)
        {
            if ([[self.rowDataFiltered objectAtIndex:selectedIndexPath.row] isKindOfClass:[MTEntidad class]])
            {
                MTEntidad* o = [self.rowDataFiltered objectAtIndex:selectedIndexPath.row];
                cellText = o.descripcion;
            } else
                if ([[self.rowDataFiltered objectAtIndex:selectedIndexPath.row] respondsToSelector:@selector(getDescription)])
                {
                    cellText = [[self.rowDataFiltered objectAtIndex:selectedIndexPath.row] getDescription];
                } else
                    cellText = [self.rowDataFiltered objectAtIndex:selectedIndexPath.row];
        }
        
        //[self.popOver dismissPopoverAnimated:YES];
        
        if ([self.delegate respondsToSelector:@selector(popListView:senderControl:didSelectRow:selectedText:)])
            [self.delegate popListView:self senderControl:self.sender didSelectRow:selectedIndexPath.row selectedText:cellText];
        
        if ([self.delegate respondsToSelector:@selector(popListView:senderControl:selObject:)])
            [self.delegate popListView:self senderControl:self.sender
                             selObject:[self.rowDataFiltered objectAtIndex:selectedIndexPath.row]];
    }
    selectedIndexPath = nil;
}

//- (void)dealloc
//{
//	[rowData          release];
//	[_rowDataFiltered release];
//    [_popOver         release];     // Objeto Popover
//    [_ViewController  release];     // View Controller que manejará la vista del Popover
//    [_popTitle        release];     // Título del popOver.
//
//	[super dealloc];
//}


#pragma mark -
#pragma mark Table methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of time zone names.
	//return self.rowData.count;
    
    return [self.rowDataFiltered count];
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
// fixed font style. use custom view (UILabel) if you want something different
//{
//    return self.Title;
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@"   %@", self.popTitle];
    label.font = [UIFont boldSystemFontOfSize:14.0f];
    
    label.backgroundColor   = UIColorFromRGB(defaultTitlesColor);
    label.textColor         = UIColorFromRGB(defaultTitlesFontColor);
    
//    label.textColor = [UIColor whiteColor];
//    label.backgroundColor = [UIColor colorWithRed:0.09f green:0.3f blue:0.64f alpha:1.0f];
    
    return label;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MyIdentifier = @"popListViewCell";
	
    NSString* cellText = @"";
       
    if ([self.rowDataFiltered count] > 0)
    {
        if ([[self.rowDataFiltered objectAtIndex:indexPath.row] isKindOfClass:[MTEntidad class]])
        {
            MTEntidad* o;
            o = [self.rowDataFiltered objectAtIndex:indexPath.row];
            
            cellText = o.descripcion;
        } else
        
        if ([[self.rowDataFiltered objectAtIndex:indexPath.row] respondsToSelector:@selector(getDescription)])
        {
            cellText = [[self.rowDataFiltered objectAtIndex:indexPath.row] getDescription];
        } else
            
        {
            NSString* o;
            o = [self.rowDataFiltered objectAtIndex:indexPath.row];
        
            cellText = o;
        }
    }
    
    // Obtain the cell object.
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        // Use the default cell style.
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
    }
    
	// Set up the cell content.
	cell.textLabel.text = [NSString stringWithFormat:@"%@", cellText];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    cell.textLabel.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedIndexPath = indexPath;
    
    if (selectedIndexPath)
    {
        if ([self.delegate respondsToSelector:@selector(popListView:senderControl:willSelectObject:)])
            [self.delegate popListView:self senderControl:self.sender
                             willSelectObject:[self.rowDataFiltered objectAtIndex:selectedIndexPath.row]];
    }
    
    [self.popOver dismissPopoverAnimated:YES];
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.rowDataFiltered removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    if ((searchText != nil) && ([searchText length] > 0))
    {
        if (self.rowData.count > 0)
        {
            // Chequeo si es una entidad.
            if ([[self.rowData objectAtIndex:0] isKindOfClass:[MTEntidad class]])
            {
                // Para los objetos de tipo MTEntidad.
                for (MTEntidad *item in self.rowData)
                {
                    NSRange range = [item.descripcion rangeOfString:searchText
                                                       options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                                 
                    if ( range.length > 0)
                    {
                        [self.rowDataFiltered addObject:item];
                    }
                }
            } else
                
            // En caso de no ser una entidad, compruebo si responde al selector getDescription
            if ([[self.rowData objectAtIndex:0] respondsToSelector:@selector(getDescription)])
            {
                // Para los objetos de tipo MTEntidad.
                for (id item in self.rowData)
                {
                    NSRange range = [[item getDescription] rangeOfString:searchText
                                                                 options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                
                    if ( range.length > 0)
                    {
                        [self.rowDataFiltered addObject:item];
                    }
                }
            } else
            {
                // Para los objetos de tipo NSString.
                for (NSString *item in self.rowData)
                {
                    NSRange range = [item rangeOfString:searchText
                                                options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                
                    if ( range.length > 0)
                    {
                        [self.rowDataFiltered addObject:item];
                    }
                }
            }
        }
    } else
    {
        // Si no hay texto de búsqueda, copio todos los elementos.
        for (id item in self.rowData)
            [self.rowDataFiltered addObject:item];
    }
    
    
    [self.ViewController.TableView reloadData];
}


#pragma mark -
#pragma mark Search bar delegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)aSearchBar
{

}


- (void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar {
    
    // If the user finishes editing text in the search bar by, for example:
    // tapping away rather than selecting from the recents list, then just dismiss the popover
    //
    
    // dismiss the popover, but only if it's confirm UIActionSheet is not open
    //  (UIActionSheets can take away first responder from the search bar when first opened)
    //
    // the popover's main view controller is a UINavigationController; so we need to inspect it's top view controller
    //
    [aSearchBar resignFirstResponder];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    // When the search string changes, filter the recents list accordingly.
    [self filterContentForSearchText:searchText scope:@""];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar endEditing:YES];
    [searchBar resignFirstResponder];
}

// En vistas que se presentan de forma modal es necesario agregar este override para que oculte el teclado
// con resignFirstResponder
- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

//#pragma mark -
//#pragma mark UISearchDisplayController Delegate Methods
//
//- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
//{
//    [self filterContentForSearchText:searchString scope:
//     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
//    
//    // Return YES to cause the search result table view to be reloaded.
//    return YES;
//}
//
//
//- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
//{
//    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
//     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
//    
//    // Return YES to cause the search result table view to be reloaded.
//    return YES;
//}

@end
