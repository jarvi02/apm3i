//
//  UIView+extensions.h
//  APM4
//
//  Created by Juan Pablo Garcia on 20/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (extensions)

-(void)setHiddenAnimated:(BOOL)hide;

@end
