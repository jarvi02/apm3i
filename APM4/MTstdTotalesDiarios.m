//
//  MTstdTotalesDiarios.m
//  APM4
//
//  Created by Laura Busnahe on 7/29/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTstdTotalesDiarios.h"
#import "Config.h"
#import "DBExtended.h"

@implementation MTstdTotalesDiarios


- (id)init
{
    self = [super init];
    if (self)
    {
        self.descripcion     = @"";
        self.cantidadManiana = 0;
        self.cantidadTarde   = 0;
        self.medicosManiana  = 0;
        self.medicosTarde    = 0;
        self.cantidad        = 0;
        self.medicos         = 0;
    }
    
    return self;
}

+ (NSArray*)getAll
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSString *FormatoFecha = @"%d/%m/%Y";
    
    NSString *sql =
    @"SELECT "
    " 0 as _id, "
    "strftime('%@', p.fecha, 'unixepoch', 'localtime') as fecha, "
    "sum(case "
    "when actividad='M' and tarea not in ('9999','10000') "
    "then  cantidad else 0 end) as CantMan, "
    "sum(case when actividad='T' and tarea not in ('9999','10000') "
    "then cantidad else 0 end) as CantTar, "
    "sum(case when actividad='M' and tarea in ('9999','10000') "
    "then cantidad else 0 end) as MedMan, "
    "sum(case when actividad='T' and tarea in ('9999','10000') "
    "then cantidad else 0 end) as MedTar, "
    "sum(case when tarea not in ('9999','10000') "
    "then cantidad else 0 end) as TotCant, "
    "sum(case when tarea  in ('9999','10000') "
    "then cantidad else 0 end) as TotMed "
    "from nodo_tareasParteDiario as t "
    "inner join nodo_partediario as p on p.id = t.partediario "
    "WHERE p.nodo = %@ "
    "group by p.fecha  "
    "union all "
    "select 1 as _id, 'Total: ', "
    "sum(case "
    "when actividad='M' and tarea not in ('9999','10000') "
    "     then  cantidad else 0 "
    "end )as CantMan, "
    " "
    "sum(case when actividad='T' and tarea not in ('9999','10000') "
    "     then  cantidad else 0 "
    "end) as CantTar, "
    " "
    "sum(case when actividad='M' and tarea in ('9999','10000') "
    "     then  cantidad else 0 "
    "end) as MedMan , "
    " "
    "sum(case when actividad='T' and tarea in ('9999','10000') "
    "     then  cantidad else 0 "
    "end) as MedTar, "
    " "
    "sum(case when tarea not in ('9999','10000') "
    "     then cantidad else 0 "
    "end) as TotCant, "
    " "
    "sum(case when tarea in ('9999','10000') "
    "      then cantidad else 0 "
    "end) as TotMed "
    " "
    " from nodo_tareasParteDiario as t "
    "inner join nodo_partediario as p on p.id = t.partediario "
    "WHERE p.nodo = %@ ";
    /*
    NSString *sql =
    @"SELECT "
        "0 as _id, "
        "strftime('%@', p.fecha, 'unixepoch', 'localtime') as fecha, "
        "sum(case when t.tarea<>'9999' and t.actividad = 'M' then "
            "t.cantidad else 0 end) as CantMan, "
        "sum(case when t.tarea<>'9999' and t.actividad = 'T' then "
            "t.cantidad else 0 end) as CantTar, "
        "sum(case when t.tarea='9999' and t.actividad = 'M' then "
            "t.cantidad else 0 end) as MedMan, "
        "sum(case when t.tarea='9999' and t.actividad = 'T' then "
            "t.cantidad else 0 end) as MedTar, "
        "sum(case when t.tarea<>'9999' then "
            "t.cantidad else 0 end) as TotCant, "
        "sum(case when t.tarea='9999' then "
            "t.cantidad else 0 end) as TotMed "
    "FROM nodo_partediario as p LEFT JOIN "
        "nodo_tareasParteDiario as t on (p.id = t.partediario AND p.nodo = t.nodo) "
    "WHERE p.nodo = %@ "
    "GROUP BY p.fecha "
    
    "UNION ALL "
    
    "SELECT "
        "1 as _id, "
        "'Total: ', "
        "sum(case when actividad='M' and tarea<>'9999' then "
                "cantidad else 0 end) as CantMan, "
        "sum(case when actividad='T' and tarea<>'9999' then "
                "cantidad else 0 end) as CantTar, "
        "sum(case when actividad='M' and tarea='9999' then "
                "cantidad else 0 end) as MedMan , "
        "sum(case when actividad='T' and tarea='9999' then "
                "cantidad else 0 end) as MedTar, "
        "sum(case when tarea<>'9999' then "
                "cantidad else 0 end) as TotCant, "
        "sum(case when tarea='9999' then "
                "cantidad else 0 end) as TotMed "
    "FROM nodo_tareasParteDiario as t INNER JOIN "
        "nodo_partediario as p on (p.id = t.partediario AND p.nodo = t.nodo) "
    "WHERE p.nodo = %@ ";*/
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql,
                                FormatoFecha,
                                nodo,
                                nodo];
    
#ifdef DEBUG_INFORMEDIARIO
    NSLog(@" - MTstdTotalesDiarios: getAll -");
    NSLog(@"- query: %@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTstdTotalesDiarios *o = [[MTstdTotalesDiarios alloc] init];
            
            // descripción
            _c = (char *)sqlite3_column_text(statement, 1);
            o.descripcion = [NSString pCharToString:_c];
            
            // Cantidad Mañana
            o.cantidadManiana = sqlite3_column_int(statement, 2);
            
            // Cantidad Tarde
            o.cantidadTarde   = sqlite3_column_int(statement, 3);
            
            // Médicos Mañana
            o.medicosManiana  = sqlite3_column_int(statement, 4);
            
            // Médicos Tarde
            o.medicosTarde    = sqlite3_column_int(statement, 5);
            
            // Total Cantidad
            o.cantidad        = sqlite3_column_int(statement, 6);
            
            // Total Medicos
            o.medicos         = sqlite3_column_int(statement, 7);
            
            [result addObject:o];
        }
        
	} else {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
    
}


@end
