//
//  TipoVisita.m
//  APM4
//
//  Created by Juan Pablo Garcia on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "TipoVisita.h"
#import "DB.h"

@implementation TipoVisita

+(NSArray*) GetAll
{
    NSString *sql = @"select vis.id, vis.descripcion, vis.contacto, vis.estado, estado.id as idestado, estado.descripcion as estadodesc, estado.contactado as contactado from mtc_tiposvisita vis inner join mtc_estadosvisita estado on vis.estado = estado.id where vis.contacto = 1";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    NSMutableArray *tipos = [NSMutableArray array];
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            TipoVisita *tipo = [[TipoVisita alloc] init];
            
            // id
            tipo.tipoId = sqlite3_column_int(statement, 0);
            
            // motivo
            char *_descripcion = (char *)sqlite3_column_text(statement, 1);
            if((_descripcion != nil) && (strlen(_descripcion) > 0)) {
                tipo.descripcion = [NSString stringWithUTF8String:_descripcion];
			}else{
				tipo.descripcion = @"";
			}
            
            tipo.estadoId = sqlite3_column_int(statement, 3);
            
            [tipos addObject:tipo];
            [tipo release];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla mtc_motivosbaja");
	}
	sqlite3_finalize(statement);
    
    return tipos;
}

-(void)dealloc
{
    [super dealloc];
    
}

@end
