//
//  AltaFluctuanteViewController.m
//  APM4
//
//  Created by Laura Busnahe on 8/9/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "AltaFluctuanteViewController.h"
#import "Especialidad.h"
#import "Provincia.h"
#import "Localidad.h"
#import "MTTiposDomicilios.h"
#import "Tratamiento.h"
#import "Utilidad.h"
#import "Cargo.h"
#import "Institucion.h"
#import "VisitaViewController.h"




@interface AltaFluctuanteViewController ()

@end

#define _xOffset 20
#define _yOffset 94

@implementation AltaFluctuanteViewController
{
    NSString *descTitulo;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        descTitulo = @"Alta de Médico Fluctuante";
        self.title = descTitulo;
    }
    return self;
}

- (id)initWithDefaultNib
{
    self = [self initWithNibName:@"AltaFluctuanteViewController" bundle:nil];
    
    return self;
}

- (id)initWithDefaultNib:(NSInteger)idMedico andMode:(MTEditActionsType) modo
{
    self = [self initWithDefaultNib];
    
    if (self)
    {
        descTitulo = @"Alta de Médico Fluctuante";
        self.modo = modo;
        self.visita = [[MTVisita alloc] init];
    
        if (modo == kMTEAAdd)
        {
            self.visita.fecha = [NSDate dateWithoutTime];
            self.medico = [MTMedico Medico];
        } else
        {
            descTitulo = @"Modificación de Médico Fluctuante";
            self.medico = [MTMedico GetById:idMedico];
        }
        
        self.medico.asignacion = 2;     // Medico fluctuante.
    }
    
    return  self;
}

-(NSString *)getTitleForHeader
{
    return descTitulo;
}

-(UIImage *)getIconoForHeader
{
    return [UIImage imageNamed:@"user_add"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = descTitulo;
    
    // Regionalización
    BOOL hide = NO;
   
    hide = [[MTRegionalDictionary sharedInstance] hideMatriculaProvincial];
    [self.lblMatProvincial setHidden:hide];
    [self.txtMProvincial   setHidden:hide];
    
    self.lblMatNacional.text = [[MTRegionalDictionary sharedInstance] valueForText:@"M. Nacional"];
    // ---
    
    
    [self.btnLocalidad setEnabled:NO];      // Habilitar cuando se haya seleccionado una provincia.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updateViewData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Tap methods

- (IBAction)tapBtnManiana:(id)sender
{
    [self.view endEditing:YES];
    self.medico.actividad = @"M";
    
    [self.btnManiana setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    [self.btnTarde setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    
}

- (IBAction)tapBtnTarde:(id)sender
{
    [self.view endEditing:YES];
    self.medico.actividad = @"T";
    
    [self.btnManiana setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnTarde setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    
    
}

- (IBAction)tapBtnMasculino:(id)sender
{
    [self.view endEditing:YES];
    self.medico.sexo = @"M";
    
    [self.btnFemenino  setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnMasculino setBackgroundImage:[UIImage imageNamed:@"ok_32"]      forState:UIControlStateNormal];
}

- (IBAction)tapBtnFemenino:(id)sender
{
    [self.view endEditing:YES];
    self.medico.sexo = @"F";
    
    [self.btnFemenino  setBackgroundImage:[UIImage imageNamed:@"ok_32"]      forState:UIControlStateNormal];
    [self.btnMasculino setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
}

- (IBAction)tapBtnProvincia:(id)sender
{
    [self.view endEditing:YES];
    
    UIView *popRectView = (UIView*)sender;
    
    //if (!self.popupMenu)
    self.popupMenu = [MTpopListView alloc];
    
    self.popupMenu = [self.popupMenu initForProvinciasWithDelegate:self];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x + _xOffset,
                               popRectView.frame.origin.y + _yOffset,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:sender
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionRight
                                            animated:YES];
}

- (IBAction)tapBtnLocalidad:(id)sender
{
    [self.view endEditing:YES];
    
    // Chequeo si se definió algún domicilio para el médico
    if ([self.medico.domicilios count] < 1)
        return;
        
    // si el médico tiene domicilio, chequeo si se seleccionó una provincia.
    Domicilio *domicilio = [self.medico.domicilios objectAtIndex:0];
    if (!domicilio.provincia)
    {
        // TODO: esto nunca debería pasar, pero si no tiene provincia...
        // mostrar un mensaje que debe seleccionar la provincia
        return;
    }
    
    UIView *popRectView = (UIView*)sender;
    
    //if (!self.popupMenu)
        self.popupMenu = [MTpopListView alloc];
    
    self.popupMenu = [self.popupMenu initForLocalidadesForProvincia:domicilio.provincia.idProvincia WithDelegate:self];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x + _xOffset,
                               popRectView.frame.origin.y + _yOffset,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:sender
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionRight
                                            animated:YES];
}

- (IBAction)tapBtnLugar:(id)sender
{
    [self.view endEditing:YES];
    
    UIView *popRectView = (UIView*)sender;
    
    //if (!self.popupMenu)
        self.popupMenu = [MTpopListView alloc];
    
    self.popupMenu = [self.popupMenu initForTiposDomiciliosWithDelegate:self];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x + _xOffset,
                               popRectView.frame.origin.y + _yOffset,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:sender
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionRight
                                            animated:YES];
}

- (IBAction)tapBtnTatamiento:(id)sender
{
    [self.view endEditing:YES];
    
    UIView *popRectView = (UIView*)sender;
    
    //if (!self.popupMenu)
        self.popupMenu = [MTpopListView alloc];
    
    self.popupMenu = [self.popupMenu initForTratamientosWithDelegate:self];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x + _xOffset,
                               popRectView.frame.origin.y + _yOffset,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:sender
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionRight
                                            animated:YES];
}

- (IBAction)tapBtnEspecialidad:(id)sender
{
    [self.view endEditing:YES];
    
    UIView *popRectView = (UIView*)sender;
    
    //if (!self.popupMenu)
    self.popupMenu = [MTpopListView alloc];
    
    self.popupMenu = [self.popupMenu initForEspecialidadesWithDelegate:self];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x + _xOffset,
                               popRectView.frame.origin.y + _yOffset,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:sender
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionRight
                                            animated:YES];
}

- (IBAction)tapBtnGuardar:(id)sender
{
//    insertaNuevoMedMedicos();
//    insertaMedEspecialidades();
//    insertaMedDomicilios();
//    regristrarVisita();
//    insertaGrupoMedicos();
//    GrabarIdCfgUltimo();
    
    [self.view endEditing:YES];
    
    MTMedico *o = self.medico;
    
    o.apellido  = [NSString string:self.txtApellido.text ifNull:@""];
    o.nombre    = [NSString string:self.txtNombre.text ifNull:@""];
    
    // Para la matricula nacional, en el caso de chile, la competo con el CUIT (RUT)
    if ([[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_CL"])
    {
        o.CUIT   = [NSString string:self.txtMNacional.text ifNull:@""];
    } else
    {
        o.matriculaNacional   = [NSString string:self.txtMNacional.text ifNull:@""];
    }

    o.matriculaProvincial = [NSString string:self.txtMProvincial.text ifNull:@""];
    
    NSString *msg = [self.medico isOkForSaveFluctuante];
    if ([msg length] == 0)
    {
        // Chequea que se haya realizado una visita
        if (self.visita)
        {
            if (self.visita.idVisita > 0)
            {
                // Médico
                self.medico.modo = kMTEAAdd;
                [self.medico performEditAction:self.medico.modo];
                
                // Visita.
                [self.visita guardarNuevaVisita];
            
                [self.navigationController popViewControllerAnimated:YES];
            } else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                                message:@"Debe realizarle una visita al médico"
                                                               delegate:self
                                                      cancelButtonTitle:@"Aceptar"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        } else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                            message:@"Debe realizarle una visita al médico"
                                                           delegate:self
                                                  cancelButtonTitle:@"Aceptar"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:msg delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)tapBtnCancelar:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)tapVisita:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *msg = [self.medico isOkForSaveFluctuante];
    if ([msg length] == 0)
    {
        self.visita.medico = self.medico;
        VisitaViewController *vc = [[VisitaViewController alloc] initWithNibName:@"VisitaViewController" bundle:nil];
        vc.visita = self.visita;
        //vc.currentMedico = self.medico;
        
        vc.modo = VisitaModoAltaFluctuante;
        
        // TODO: Ver porqué me toma la actividad como tarde cuando voy a la visita.
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:msg delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }
}


#pragma mark -
#pragma mark popListView delegate methods
- (void)popListView:(MTpopListView *)controller senderControl:(id)sender didSelectRow:(NSInteger *)MenuIndex selectedText:(NSString *)RowText
{
    
}

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender willSelectObject:(id)selObject
{
    // Especialidad
    if (sender == self.btnEspecialidad)
    {
        [self.medico.especialidades removeAllObjects];
        [self.medico.especialidades addObject:selObject];
        self.medico.especialidad1 = ((Especialidad*)selObject).descripcion;
    }
    
    // Provincia
    if (sender == self.btnProvincia)
    {
        Domicilio *d = nil;
        
        if ([self.medico.domicilios count] == 0)
        {
            d = [self crearNuevoDomicilio];
            
            [self.medico.domicilios addObject:d];
        } else
        {
            d = [self.medico.domicilios objectAtIndex:0];
        }
        
        d.provincia = selObject;
        d.localidad = nil;
        
        [self.btnLocalidad setEnabled:YES];
    }
    
    // Localidad
    if (sender == self.btnLocalidad)
    {
        Domicilio *d = nil;
        if ([self.medico.domicilios count] > 0)
        {
            d = [self.medico.domicilios objectAtIndex:0];
            d.localidad = selObject;
        }
    }
    
    // Lugar
    if (sender == self.btnLugar)
    {
        Domicilio *d = nil;
        if ([self.medico.domicilios count] > 0)
        {
            d = [self.medico.domicilios objectAtIndex:0];
            
            if (!d.tipo)
                d.tipo = [[Tipo alloc] init];
            
            d.tipo.idTipo = [(MTTiposDomicilios*)selObject recID];
            d.tipo.descripcion = [(MTTiposDomicilios*)selObject descripcion];
        }
    }
    
    // Tratamiento
    if (sender == self.btnTratamiento)
    {
        self.medico.tratamiento = selObject;
        self.medico.descTratamiento = self.medico.tratamiento.descripcion;
    }
    
    [self updateViewData];
}


#pragma mark -
#pragma mark UITextFieldDelegate

- (IBAction)txtChangedContent:(id)sender
{
    MTMedico *o = self.medico;
    UITextField *textField = sender;
    
    if (textField == self.txtApellido)
    {
        o.apellido  = [NSString string:textField.text ifNull:@""];
    }
    
    if (textField == self.txtNombre)
    {
        o.nombre    = [NSString string:textField.text ifNull:@""];
    }
    
    if (textField == self.txtMNacional)
    {
        // Para la matricula nacional, en el caso de chile, la competo con el CUIT (RUT)
        if ([[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_CL"])
        {
            o.CUIT = [NSString string:textField.text ifNull:@""];
        } else
        {
            o.matriculaNacional = [NSString string:textField.text ifNull:@""];
        }
    }
    
    if (textField == self.txtMProvincial)
    {
        o.matriculaProvincial = [NSString string:textField.text ifNull:@""];
    }
}

-(BOOL) textFieldShouldBeginEditing:(UITextField*)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [self txtChangedContent:textField];
}


- (BOOL)textFieldOLD:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    
    if ([string length] == 0 && range.length > 0)
    {
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return NO;
    }
    
    if (((textField == self.txtMNacional) && (![[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_CL"])) ||
        (textField == self.txtMProvincial))
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        if ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0)
            return YES;
        
        return NO;
    }
    
    
    
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    if ([string stringByTrimmingCharactersInSet:notAllowedSet].length > 0)
        return YES;
    
    return NO;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    NSString *oldString = [NSString string:textField.text ifNull:@""];
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    if (((textField == self.txtMNacional) && (![[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_CL"])) ||
        (textField == self.txtMProvincial))
    {
        notAllowedSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    }
    
    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
                            stringByTrimmingCharactersInSet:notAllowedSet];
    
    if ([oldString isEqualToString:tempString])
        return NO;
    
    return   YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

#pragma mark -
#pragma mark Private methods
- (void)updateViewData
{
    self.txtApellido.text = self.medico.apellido;
    self.txtNombre.text = self.medico.nombre;
    
    // Actividad
    if ([self.medico.actividad isEqualToString:@"T"])
    {
        [self tapBtnTarde:(self.btnTarde)];
    }
    else
        if ([self.medico.actividad isEqualToString:@"M"])
        {
            [self tapBtnManiana :(self.btnManiana)];
        }
    
    // Sexo
    if ([self.medico.sexo isEqualToString:@"M"])
    {
        [self tapBtnMasculino:(self.btnMasculino )];
    } else
        
        if ([self.medico.sexo isEqualToString:@"F"])
        {
            [self tapBtnFemenino:(self.btnFemenino )];
        }
    
    // Especialidad
    self.txtEspecialidad.text = @"";
    if ([self.medico.especialidades count] > 0)
    {
        Especialidad *e = [self.medico.especialidades objectAtIndex:0];
        self.txtEspecialidad.text = [NSString string:e.descripcion ifNull:@""];
    }
    
    // Provincia, Localidad, Lugar
    if ([self.medico.domicilios count] > 0)
    {
        Domicilio *d = [self.medico.domicilios objectAtIndex:0];
        
        self.txtProvincia.text = [NSString string:d.provincia.descripcion ifNull:@""];
        self.txtLocalidad.text = [NSString string:d.localidad.descripcion ifNull:@""];
        self.txtLugar.text = [NSString string:d.tipo.descripcion ifNull:@""];
        
        [self.btnLugar setEnabled:YES];
        
    } else
    {
        self.txtProvincia.text = @"";
        self.txtLocalidad.text = @"";
        [self.btnLocalidad setEnabled:NO];
        self.txtLugar.text = @"";
        [self.btnLugar setEnabled:NO];
    }
    
    // Tratamiento
    if (self.medico.descTratamiento != nil)
        self.txtTratamiento.text = self.medico.descTratamiento ;
    
    // Para la matricula nacional, en el caso de chile, la competo con el CUIT (RUT)
    if ([[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_CL"])
    {
        self.txtMNacional.text = [NSString string:self.medico.CUIT ifNull:@""];
    } else
    {
        if (self.medico.matriculaNacional != nil)
            self.txtMNacional.text = self.medico.matriculaNacional ;
    }
    
    // MatrículaProvincial
    if (self.medico.matriculaProvincial != nil)
        self.txtMProvincial.text = self.medico.matriculaProvincial;
    
    // Visita
    if ((self.visita) && (self.visita.idVisita > 0))
        self.txtVisita.text = @"Visita Completa";
    else
        self.txtVisita.text = @"Sin Visita";
    
}

- (Domicilio*)crearNuevoDomicilio
{
    Domicilio *d = [[Domicilio alloc] init];
    
    // Tipo
    d.tipo = [[Tipo alloc] init];
    
    // Institucion
    d.institucion = nil;
    
    //Cargo
    d.cargo = [[Cargo alloc] init];
    
    // Provincia
    d.provincia = nil;
    
    // Localidad
    d.localidad = nil;
    
    // Calle
    d.calle = [[Calle alloc] init];
    d.calle.descripcion = @"0";
    
    d.numero = 0;
    d.piso = 0;
    d.depto = @"";
    d.codigoPostal = @"";
    
    // Utilidad
    d.utilidad = [[Utilidad alloc] init];
    d.utilidad.idUtilidad = 1;              // Domicilio de VISITA
    
    d.consultorioExterno = NO;
    
    // Horarios
    // NO TIENE
    
    // Telefonos
    // NO TIENE
    
    return d;
}


@end
