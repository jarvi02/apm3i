//
//  MTEvaluacionVisitas.h
//  APM4
//
//  Created by Laura Busnahe on 9/6/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTEvaluacionVisitas : NSObject

@property(nonatomic)         NSUInteger     idVisita;
@property(nonatomic, strong) NSDate         *fecha;
@property(nonatomic, strong) NSString       *evaluacion;

+ (NSArray*)getAllForMedico:(NSUInteger)idMedico producto:(NSUInteger)idProducto;

@end
