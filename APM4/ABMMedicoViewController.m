//
//  ABMMedicoViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ABMMedicoViewController.h"
#import "ObrasSocialesViewController.h"
#import "HorarioViewController.h"
#import "TelefonoViewController.h"
#import "DomicilioViewController.h"
#import "EspecialidadViewController.h"
#import "Potencial.h"
#import "Tratamiento.h"
#import "ConfigGlobal.h"
#import "Especialidad.h"
#import "Domicilio.h"
#import "ObraSocial.h"
#import "Horario.h"

#import "NSDate+extensions.h"
#import "NSStringExtended.h"


@interface ABMMedicoViewController ()
@property(nonatomic, retain)NSArray     *potencial;
@property(nonatomic, retain)NSArray     *tratamientos;
-(void) guardar;
-(void) loadDatosMedico;
@end

@implementation ABMMedicoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil idMedico:(NSInteger)idMedico andMode:(MTEditActionsType) modo;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"ABM Medico";
        self.modo = modo;
        if (modo == kMTEAAdd) {
            self.medico = [MTMedico Medico];
        } else {
            self.medico = [MTMedico GetById:idMedico];
        }
    }
    return self;
}


-(NSString *)getTitleForHeader{

    if (self.modo == kMTEAAdd) {
        return @"Alta";
    }
    return @"Modificacion";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"user_add"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // Regionalización
    BOOL hide = NO;
    hide = [[MTRegionalDictionary sharedInstance] hideCUIT];
    [self.lblCUIT setHidden:hide];
    [self.txtCUIT setHidden:hide];
    
    hide = [[MTRegionalDictionary sharedInstance] hideMatriculaNacional];
    [self.lblMatNacional setHidden:hide];
    [self.txtMNacional   setHidden:hide];
    
    hide = [[MTRegionalDictionary sharedInstance] hideMatriculaProvincial];
    [self.lblMatProvincial setHidden:hide];
    [self.txtMProvincial   setHidden:hide];
    
    self.lblObraSocial.text  = [[MTRegionalDictionary sharedInstance] valueForText:@"Obra Social"];
    self.lblMatNacional.text = [[MTRegionalDictionary sharedInstance] valueForText:@"M. Nacional"];
    self.lblCUIT.text        = [[MTRegionalDictionary sharedInstance] valueForText:@"C.U.I.T."];
    // ---
    
    
    if (!(self.modo == kMTEAAdd))
    {
        [self loadDatosMedico];
        
         ((UILabel*)[self.view viewWithTag:TAG_TITULO_HEADER]).text = [NSString stringWithFormat:@"%@ - %@",self.title, self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre];
    }
        
    ((UILabel*)[self.view viewWithTag:TAG_TITULO]).text = [self getTitleForHeader];
   
    
    self.potencial = [Potencial GetAll];
    self.tratamientos = [Tratamiento GetAll];
    
//    if ([[ConfigGlobal shareInstance] find:VIS_REVISITA]) {
//        // habilito revisar
        self.txtRevisar.enabled = YES;
        self.txtRevisar.backgroundColor = [UIColor whiteColor];
//        self.txtRevisar.text = @"";
//    } else {
//        self.txtRevisar.text = @"0";
//    }

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    Domicilio *d = [self buscarDomicilioPrincial];
    
    if ((d) && ([d.horarios count] > 0))
    {
        self.btnHorario.hidden = NO;
    } else
    {
        self.btnHorario.hidden = YES;
    }

}


- (IBAction)performActividadManana:(id)sender {

    self.medico.actividad = @"M";
    
    [self.btnActividadManana setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    [self.btnActividadTarde setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    
}

- (IBAction)performActividadTarde:(id)sender {
    
    self.medico.actividad = @"T";
    
    [self.btnActividadManana setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnActividadTarde setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];

    
}

#define POTENCIAL @"Potencial"
- (IBAction)performPotencial:(id)sender
{

    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    NSMutableArray *entries = [[NSMutableArray alloc] initWithCapacity:0];
    EntryForCombo *e;
    
    for (int i=0; [self.potencial count] > i; i++)
    {
        
        Potencial *o = [self.potencial objectAtIndex:i];
        
        e = [[EntryForCombo alloc] initWithValue:o.descripcion forIndex:i];
        
        [entries addObject:e];
    }
    
    
    CGSize size = CGSizeMake(350, 300);
    TableForComboPopoverViewController *t = [[TableForComboPopoverViewController alloc] initWithData:entries titulo:POTENCIAL size:size delegate:self andCaller:POTENCIAL];
    t.aligment = NSTextAlignmentLeft;
    
    Delegate.popoverController = [[UIPopoverController alloc] initWithContentViewController:t];
    
    [Delegate.popoverController presentPopoverFromRect: ((UIButton*)sender).frame inView:self.scrollView permittedArrowDirections:YES animated:YES];
    
}

- (IBAction)performEspecialidad:(id)sender {

//    EspecialidadViewController *o = [[EspecialidadViewController alloc] initWithNibName:@"EspecialidadViewController" bundle:nil];
//    o.modalPresentationStyle = UIModalPresentationFormSheet;
//    o.abmMedicoReference = self;
//    [self presentViewController:o animated:YES completion:nil];

    listViewController *o = [[listViewController alloc] initViewAsEspecialidadesWithReference:sender];
    o.delegate = self;
    o.listContent = self.medico.especialidades;
    
    [self presentViewController:o animated:YES completion:nil];
}

- (IBAction)performDomicilio:(id)sender {

    DomicilioViewController *o = [[DomicilioViewController alloc] initWithNibName:@"DomicilioViewController" bundle:nil andMode:self.modo];
    
    o.abmMedicoReference = self;
    [self.navigationController pushViewController:o animated:YES];    
}

- (IBAction)performHorario:(id)sender {

    HorarioViewController *o = [[HorarioViewController alloc] initWithNibName:@"HorarioViewController" bundle:nil];
    o.modalPresentationStyle = UIModalPresentationFormSheet;
    o.abmMedicoReference = self;
    o.domicilioReference = [self buscarDomicilioPrincial];
    o.delegate = self;
    [self presentViewController:o animated:YES completion:nil];
    
}

- (IBAction)performObraSocial:(id)sender {

//    ObrasSocialesViewController *o = [[ObrasSocialesViewController alloc] initWithNibName:@"ObrasSocialesViewController" bundle:nil];
//    o.modalPresentationStyle = UIModalPresentationFormSheet;
//    o.abmMedicoReference = self;
//    [self presentViewController:o animated:YES completion:nil];
    
    listViewController *o = [[listViewController alloc] initViewAsObrasSocialesWithReference:sender];
    o.delegate = self;
    o.listContent = self.medico.obrasSociales;
    
    [self presentViewController:o animated:YES completion:nil];
}

- (IBAction)performSexoFemenino:(id)sender
{
    self.medico.sexo = @"F";
    
    [self.btnSexoFemenino setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    [self.btnSexoMasculino setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    
}

- (IBAction)performSexoMasculino:(id)sender
{

    self.medico.sexo = @"M";
    
    [self.btnSexoFemenino setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    [self.btnSexoMasculino setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    
}


#define TRATAMIENTO @"Tratamiento"

- (IBAction)performTratamiento:(id)sender {
    
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    NSMutableArray *entries = [[NSMutableArray alloc] initWithCapacity:0];
    EntryForCombo *e;
    
    for (int i=0; [self.tratamientos count] > i; i++) {
        
        Tratamiento *o = [self.tratamientos objectAtIndex:i];
        
        e = [[EntryForCombo alloc] initWithValue:o.descripcion forIndex:i];
        
        [entries addObject:e];
    }
    
    
    CGSize size = CGSizeMake(200, 250);
    TableForComboPopoverViewController *t = [[TableForComboPopoverViewController alloc] initWithData:entries titulo:TRATAMIENTO size:size delegate:self andCaller:TRATAMIENTO];
    t.aligment = NSTextAlignmentLeft;
    
    Delegate.popoverController = [[UIPopoverController alloc] initWithContentViewController:t];
    
    [Delegate.popoverController presentPopoverFromRect: ((UIButton*)sender).frame inView:self.scrollView permittedArrowDirections:YES animated:YES];
    

}
- (IBAction)performFechaNacimiento:(id)sender {

    if (!Delegate.popoverController.popoverVisible)
    {
        DateSelectorViewController *dvc = [[DateSelectorViewController alloc] initWithNibName:@"DateSelectorViewController" bundle:nil];
        
        dvc.contentSizeForViewInPopover = CGSizeMake(320.0f, 216.0f);
        dvc.date = [NSDate date];
        dvc.delegate = self;
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:dvc];
        
        Delegate.popoverController = [[UIPopoverController alloc] initWithContentViewController:nav];
        [Delegate.popoverController presentPopoverFromRect:((UIButton*)sender).frame inView:self.scrollView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }

    
}


-(void) closeModalView {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark ListView methods

- (void)listViewController:(listViewController *)controller referenceControl:(id)sender selectedObjects:(NSArray *)selectionArray
{
    [self loadDatosArrays];
}


#pragma mark -
#pragma mark Combo delegates methods

-(void)selectedIndex:(NSInteger)index caller:(NSString*)caller
{
    [Delegate dismissPopOver:YES];
    
    if ([caller isEqualToString:POTENCIAL])
    {
        self.medico.potencial = [self.potencial objectAtIndex:index];
        self.txtPotencial.text = self.medico.potencial.descripcion;
        
    } else if ([caller isEqualToString:TRATAMIENTO])
    {
        self.medico.tratamiento = [self.tratamientos objectAtIndex:index];
        self.txtTratamiento.text = self.medico.tratamiento.descripcion;
    
    } else
    {
        // jamas deberia entrar aca
    }
    
}
    

#pragma mark -
#pragma mark DateSelector

-(void)DateSelector:(DateSelectorViewController*)dateSelector dateSelected:(NSDate *)date
{
    self.medico.fechaNacimiento = date;
    self.txtFechaNacimiento.text = [self.medico.fechaNacimiento formattedStringUsingFormat:@"dd/MM/yyyy"];
}


#pragma mark -
#pragma mark UITextFieldDelegate

-(BOOL) textFieldShouldBeginEditing:(UITextField*)textField
{
    return YES;
}


- (BOOL)textFieldOLD:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text

    
    //BOOL _isAllowed = NO;
    
    if ([string length] == 0 && range.length > 0)
    {
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return NO;
    }
    
    if ((textField == self.txtMNacional) ||
        (textField == self.txtMProvincial) ||
        (textField == self.txtRevisar))
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        if ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0)
            return YES;
        
        return NO;
    } else
        
    if (textField == self.txtEmail)
    {
        return YES;
    }
        

    
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    if ([string stringByTrimmingCharactersInSet:notAllowedSet].length > 0)
        return YES;
    
    return NO;
    
    
//    NSString *oldString = [NSString string:textField.text ifNull:@""];
//    
//    
//    NSString *tempString = @"";
//    
//    tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
//                            stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
//    
//    
//    if (![oldString isEqualToString:tempString])
//    {
//        _isAllowed =  YES;
//    }
//    
//    
//    return   _isAllowed;
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    NSString *oldString = [NSString string:textField.text ifNull:@""];
    
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    if ((textField == self.txtMNacional) ||
        (textField == self.txtMProvincial) ||
        (textField == self.txtRevisar))
    {
        notAllowedSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    }
    
    
    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
                            stringByTrimmingCharactersInSet:notAllowedSet];
    
    if ([oldString isEqualToString:tempString])
        return NO;
    
    return   YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}


#pragma mark -
#pragma mark Guardar

- (IBAction)performGuardar:(id)sender {
    
    [self guardar];
    //[self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)performCancelar:(id)sender {

        [self.navigationController popViewControllerAnimated:YES];
}

-(void) guardar{

    //completo si me falta algun dato en la clase medico.
    
    MTMedico *o = self.medico;
    
    if ([o isFluctuante])
    {
        NSString *msg = @"Para convertir el médico fluctuante debe completar el domicilio de visita";
        Domicilio *d = [self buscarDomicilioPrincial];
        
        if (d)
        {
            if ([[d isOkForSave] isEqualToString:@"OK"])
                msg = @"";
        }
        
        
        if ([msg length] > 2)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:msg delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        // Lo asigno como médico normal.
        o.asignacion = 1;
        
        o.A = YES;
        o.B = NO;
        o.M = NO;
        o.descABM = @"Alta";
        
        
        // Chequeo si utiliza Autorización para marcarlo como pendiente de alta.
        if ([[ConfigGlobal shareInstance] find:USAAUTORIZACION])
        {
            o.penAlta = YES;
            o.descABM = @"Pend. Alta";
        }
    }
    
        
    o.apellido  = [NSString string:self.txtApellido.text ifNull:@""];
    o.nombre    = [NSString string:self.txtNombre.text ifNull:@""];
    o.CUIT      = [NSString string:self.txtCUIT.text ifNull:@""];
    o.email     = [NSString string:self.txtEmail.text ifNull:@""];
    o.revisita  = [NSString string:self.txtRevisar.text ifNull:@""];
    o.visitasProgramadas = 1 + [o.revisita integerValue];
    
    o.matriculaNacional   = [NSString string:self.txtMNacional.text ifNull:@""];
    o.matriculaProvincial = [NSString string:self.txtMProvincial.text ifNull:@""];
    
    NSString *msg = [self.medico isOkForSave];
    if ([msg length] == 0)
    {
        [self.medico performEditAction:self.medico.modo];
//        if (self.modo == kMTEAAdd)
//        {
//            [self.medico saveAllData];
//        }
//        else if (self.modo == kMTEAChange)
//        {
//            [self.medico updateAllData];
//        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:msg delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
    }
    
}

#pragma mark -
#pragma Cargar Datos en pantalla

-(void) loadDatosArrays
{
    if ([self.medico.especialidades count] > 0)
        self.txtEspecialidad.text = ((Especialidad*)[self.medico.especialidades objectAtIndex:0]).descripcion;
    
    if ([self.medico.obrasSociales count] > 0)
        self.txtObreSocial.text = ((ObraSocial*)[self.medico.obrasSociales objectAtIndex:0]).descripcion;
}

-(void) loadDatosMedico
{
    
    
    self.txtApellido.text = self.medico.apellido;
    self.txtNombre.text = self.medico.nombre;
   

    if ([self.medico.actividad isEqualToString:@"T"])
    {
        [self performActividadTarde:(_btnActividadTarde)];
    } else
    if ([self.medico.actividad isEqualToString:@"M"])
    {
        [self performActividadManana :(_btnActividadManana)];
        
    }
    
    if ([self.medico.sexo isEqualToString:@"M"])
    {
        [self performSexoMasculino:(_btnSexoMasculino )];
    } else
    if ([self.medico.sexo isEqualToString:@"F"])
    {
        [self performSexoFemenino:(_btnSexoFemenino )];
        
    }
    
    
    if (self.medico.descPotencial != nil)
        self.txtPotencial.text = self.medico.descPotencial ;
    
    if (self.medico.especialidad1 != nil)
        self.txtEspecialidad.text = self.medico.especialidad1 ;
    
    if (self.medico.descTratamiento != nil)
        self.txtTratamiento.text = self.medico.descTratamiento ;
    
    if (self.medico.fechaNacimiento != nil)
        self.txtFechaNacimiento.text = [self.medico.fechaNacimiento formattedStringUsingFormat:@"dd/MM/yyyy"];

    if (self.medico.anioEgreso != nil)
        self.txtAnoEgreso.text = self.medico.anioEgreso ;
    
    if (self.medico.CUIT  != nil)
        self.txtCUIT.text = self.medico.CUIT ;
    
    if (self.medico.email != nil)
        self.txtEmail.text = self.medico.email ;
    
    if (self.medico.visitasProgramadas > 0)
        self.txtRevisar.text = [NSString stringWithFormat:@"%d", self.medico.visitasProgramadas -1];
    else
        self.txtRevisar.text = @"0";
    
    if (self.medico.matriculaNacional != nil)
        self.txtMNacional.text = self.medico.matriculaNacional ;
    
    if (self.medico.matriculaProvincial != nil)
        self.txtMProvincial.text = self.medico.matriculaProvincial;  
    
    
//    if ([self.medico.especialidades count] > 0)
//        self.txtEspecialidad.text = ((Especialidad*)[self.medico.especialidades objectAtIndex:0]).descripcion;
//    
//    if ([self.medico.obrasSociales count] > 0)
//        self.txtObreSocial.text = ((ObraSocial*)[self.medico.obrasSociales objectAtIndex:0]).descripcion;
    [self loadDatosArrays];
    
    
    
    MTMedico *m = self.medico;
    NSArray *d = m.domicilios;
    
    self.txtHorario.text = @"";
    self.txtDomicilio.text = @"";
    
    if ([d count] > 0)
    {
//        Domicilio *o = [self.medico.domicilios objectAtIndex:0];
//        self.txtDomicilio.text = [NSString stringWithFormat:@"%@ - %@ %d %d %@, %@, %@, %@ - %@",
//                                  o.utilidad.descripcion, o.calle.descripcion, o.numero, o.piso, o.depto, o.codigoPostal,
//                                  o.localidad.descripcion, o.provincia.descripcion,
//                                  o.tipo.descripcion];
        Domicilio *o = [self buscarDomicilioPrincial];
        self.txtDomicilio.text = o==nil?@"":o.getDescripcion;
        
        if ([o.horarios count] > 0)
        {
            Horario *h = [o.horarios objectAtIndex:0];
            self.txtHorario.text = [h descripcion];
        }
    }
    
//    NSString *descHorario;
//    if (m.lu) {
//         descHorario = @"lu,";
//    }
//    
//    if (m.ma) {
//        descHorario = [NSString stringWithFormat:@"%@ %@",  descHorario, @"ma,"];
//    }
//    
//    if (m.mi) {
//        descHorario = [NSString stringWithFormat:@"%@ %@",  descHorario, @"mi,"];
//    }
//    
//    if (m.ju) {
//        descHorario = [NSString stringWithFormat:@"%@ %@",  descHorario, @"ju,"];
//    }
//    
//    if (m.vi) {
//        descHorario = [NSString stringWithFormat:@"%@ %@",  descHorario, @"vi"];
//    }
//    
//    
//     self.txtHorario.text = descHorario;
    
    
    
}

-(Domicilio*)buscarDomicilioPrincial
{
    if ([self.medico.domicilios count] > 0)
    {
        for (NSInteger oCount = 0; oCount < [self.medico.domicilios count]; oCount++)
        {
            Domicilio* d = [self.medico.domicilios objectAtIndex:oCount];
            if (d.utilidad.idUtilidad == 1)
                return d;
        }
    }
    
    return nil;
}

@end
