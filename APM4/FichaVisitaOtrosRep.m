//
//  FichaVisitaOtrosRep.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 09/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "FichaVisitaOtrosRep.h"
#import "Config.h"
#import "DB.h"

@implementation FichaVisitaOtrosRep


- (id)init
{
    self = [super init];
    if (self) {
        self.fecha = @"";
        self.visitador = @"";
    }
    return self;
}

- (void)dealloc
{
    [_fecha release];
    [_visitador release];
    [super dealloc];
}


+(NSArray*)GetAllByMedico:(NSInteger)idMedico {
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
//    NSString *sql = @"Select strftime('%@', fecha, 'unixepoch', 'localtime') as fecha, visitador from std_visitasexternas where nodo = %@ and medico = %d;";
//    
//    NSString *nodo = SettingForKey(NODO, @"");
//    NSString *FormatoFecha = @"%d/%m/%Y";
//    NSString *query = [NSString stringWithFormat:sql, FormatoFecha, nodo,idMedico];
    
    NSString *sql = @"Select fecha, visitador from std_visitasexternas where nodo = %@ and medico = %d;";
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *query = [NSString stringWithFormat:sql, nodo,idMedico];

#ifdef DEBUG_VISITA
    NSLog(@" - FichaVisitaOtrosRep: GetAllByMedico - ");
    NSLog(@"%@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        FichaVisitaOtrosRep *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[FichaVisitaOtrosRep alloc] init];
            
            _c = (char *)sqlite3_column_text(statement, 0);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.fecha = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.visitador = [NSString stringWithUTF8String:_c];
			}
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
    
}

@end
