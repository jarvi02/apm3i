//
//  Tratamiento.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Tratamiento.h"
#import "DB.h"

@implementation Tratamiento

- (id)init
{
    self = [super init];
    if (self) {
        self.recID = 0;
        self.descripcion = @"";
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [super dealloc];
}

- (NSString*)getDescription
{
    return [NSString string:self.descripcion ifNull:@""];
}

+(NSArray*) GetAll
{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"select id, descripcion from mtc_tratamientos order by id;"; // order by descripcion desc;";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
        Tratamiento *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            
            o = [[Tratamiento alloc] init];
            
            // recID
            o.recID = sqlite3_column_int(statement, 0);
            
            // Descripción
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
}

@end
