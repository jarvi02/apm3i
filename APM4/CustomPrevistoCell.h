//
//  PrevistoCustomCell.h
//  APM4
//
//  Created by Laura Busnahe on 2/26/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPrevistoCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *imgCell;
@property (retain, nonatomic) IBOutlet UILabel *lblFechaTurno;
@property (retain, nonatomic) IBOutlet UILabel *lblActividad;
@property (retain, nonatomic) IBOutlet UILabel *lblDireccion;
@end
