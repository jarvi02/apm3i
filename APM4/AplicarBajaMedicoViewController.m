//
//  AplicarBajaMedicoViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "AplicarBajaMedicoViewController.h"
#import "CarteraViewController.h"
#import "DateSelectorViewController.h"
#import "NSDate+extensions.h"
#import "AppDelegate.h"
#import "MotivoBaja.h"
#import "Ciclo.h"
#import "Novedad.h"
#import "Config.h"
#import "ConfigGlobal.h"
#import "CarteraMedico.h"

@interface AplicarBajaMedicoViewController ()

@end

//#define _yOffset 94

#define _xOffset 15
#define _yOffset 0


@implementation AplicarBajaMedicoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil cartera:(CarteraViewController *)cartera
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.carteraViewControllerReference = cartera;
        self.title = @"Aplicar Baja";
    }
    return self;
}

-(NSString *)getTitleForHeader
{
    return self.title;
}

-(UIImage *)getIconoForHeader
{
    return [UIImage imageNamed:@"user_del"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ((UILabel*)[self.view viewWithTag:TAG_TITULO_HEADER]).text = [NSString stringWithFormat:@"%@ - %@",self.title, self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre];
    ((UILabel*)[self.view viewWithTag:TAG_TITULO]).text = self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre;
    
    
    // Fecha Default
    self.currentDate = [NSDate dateWithoutTime];
    self.fecha.text = [self.currentDate formattedStringUsingFormat:@"dd/MM/yyyy"];
    
    // Motivo Default
    self.currentMotivo = [MotivoBaja getFirst];
    self.motivo.text = [NSString string:self.currentMotivo.motivo];
}

#pragma mark -
#pragma mark DateSelector

-(void)DateSelector:(DateSelectorViewController*)dateSelector dateSelected:(NSDate *)date
{
    self.currentDate = [date dateAsDateWithoutTime];
    self.fecha.text = [self.currentDate formattedStringUsingFormat:@"dd/MM/yyyy"];
}


#pragma mark -
#pragma mark popListView delegate methods
- (void)popListView:(MTpopListView *)controller senderControl:(id)sender didSelectRow:(NSInteger *)MenuIndex selectedText:(NSString *)RowText
{
    
}

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender willSelectObject:(id)selObject
{
    self.currentMotivo = (MotivoBaja*)selObject;
    self.motivo.text = [NSString stringWithFormat:@"  %@", self.currentMotivo.motivo];
    
    //[self updateViewData];
}


#pragma mark -
#pragma mark Actions

-(IBAction)pressedGuardar:(id)sender
{
    Ciclo *ciclo = [Ciclo actual];
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"Ciclo Actual: %@ - %@", [ciclo.desde formattedStringUsingFormat:@"dd/MM/yyyy"], [ciclo.hasta formattedStringUsingFormat:@"dd/MM/yyyy"]);
#endif
    
    if ([[self.currentDate dateAsDateWithoutTime] compare:[ciclo.desde dateAsDateWithoutTime]] == NSOrderedAscending || [[self.currentDate dateAsDateWithoutTime] compare:[ciclo.hasta dateAsDateWithoutTime]] == NSOrderedDescending)
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Fecha incorrecta . Ciclo comprendido entre fecha %@ y %@", [ciclo.desde formattedStringUsingFormat:@"dd/MM/yyyy"], [ciclo.hasta formattedStringUsingFormat:@"dd/MM/yyyy"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [av show];
        return;
    }
    
    BOOL *usaAutorizacion = [[ConfigGlobal shareInstance] find:USAAUTORIZACION];
    BOOL fluctuante = self.carteraViewControllerReference.itemCarteraMedicoSelected.asignacion == 2;
    BOOL baja;
    NSString *descripcion;
    BOOL pendiente;
    
    if (usaAutorizacion && !fluctuante)
    {
        baja = NO;
        descripcion = @"Pend. Baja";
        pendiente = YES;
    }
    else
    {
        baja = YES;
        descripcion = @"Baja";
        pendiente = NO;
    }
    double fechaMilsSegs = self.currentDate.timeIntervalSince1970;
    // Doy de baja al medico
    [self.carteraViewControllerReference.itemCarteraMedicoSelected darDeBaja:fechaMilsSegs motivo:self.currentMotivo.motivoId descr:descripcion pendiente:pendiente baja:baja];
    
    // Novedad
    NSString *n = [self.carteraViewControllerReference.itemCarteraMedicoSelected novedad];
    [Novedad insert:MED_MEDICOS novedad:n tipo:kUpdate];
    
    // Mensaje de confirmacion
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"" message:@"La baja se ha registrado con éxito" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [av show];
    
    // Vuelvo a la cartera
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)pressednCancelar:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)selectMotivo:(id)sender
{
    [self.view endEditing:YES];
    
    UIView *popRectView = (UIView*)sender;
    
    //if (!self.popupMenu)
    self.popupMenu = [MTpopListView alloc];
    
    self.popupMenu = [self.popupMenu initForMotivoBajaWithDelegate:self];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x + _xOffset,
                               popRectView.frame.origin.y + _yOffset,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:sender
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionRight
                                            animated:YES];
}

-(IBAction)selectFecha:(id)sender
{
    if (!Delegate.popoverController.popoverVisible)
    {
        DateSelectorViewController *dvc = [[DateSelectorViewController alloc] initWithNibName:@"DateSelectorViewController" bundle:nil];
        
        dvc.contentSizeForViewInPopover = CGSizeMake(320.0f, 216.0f);
        dvc.date = self.currentDate;
        dvc.delegate = self;
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:dvc];
        
        Delegate.popoverController = [[UIPopoverController alloc] initWithContentViewController:nav];
        [Delegate.popoverController presentPopoverFromRect:((UIButton*)sender).frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}


@end
