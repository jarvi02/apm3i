//
//  loadingViewController.h
//  APM4
//
//  Created by Laura Busnahe on 7/18/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loadingViewController : UIViewController

@property (nonatomic, strong) UINib     *Nib;

- (id)initWithDefaultNib;
- (void)show:(UIViewController*)owner;
- (void)hide;

@property (retain, nonatomic) IBOutlet UILabel *lblDescripcion;

@end
