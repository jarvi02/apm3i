//
//  ProcesoTransmicion.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransmitirViewController.h"

typedef NS_ENUM(NSInteger, ACCIONTX) {
    INICIALIZACION,
    CIERRE_CICLO,
    NOVEDADES
};


@interface ProcesoTransmicion : NSObject

@property(nonatomic, assign) NSInteger serverAvailable;
@property(nonatomic, assign) UILabel *lbStatusTransmisionViewController;
@property(nonatomic, assign) UIProgressView *progressView;


-(void)start;

@end
