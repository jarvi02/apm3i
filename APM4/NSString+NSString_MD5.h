//
//  NSString+NSString_MD5.h
//  WebGisMobile
//
//  Created by Develaris on 23/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_MD5)
- (NSString *) md5;
+ (NSString *)sha512:(NSString *)strData;
@end
