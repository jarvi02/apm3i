//
//  AltaFluctuanteViewController.h
//  APM4
//
//  Created by Laura Busnahe on 8/9/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomViewController.h"
#import "popListExtended.h"
#import "CarteraViewController.h"
#import "VisitaViewController.h"
#import "MTVisita.h"
#import "MTMedico.h"

@interface AltaFluctuanteViewController : CustomViewController<UITextFieldDelegate, MTpopListViewDelegate>

@property(nonatomic, assign) CarteraViewController *carteraViewControllerReference;

@property (strong, nonatomic) MTpopListView         *popupMenu;
@property(nonatomic, assign) MTEditActionsType      modo;

@property(nonatomic, retain) MTMedico               *medico;
@property(nonatomic, strong) MTVisita                 *visita;

// Controles que cambian en regionalización
@property (retain, nonatomic) IBOutlet UILabel *lblProvincia;
@property (retain, nonatomic) IBOutlet UILabel *lblLocalidad;
@property (retain, nonatomic) IBOutlet UILabel *lblMatNacional;
@property (retain, nonatomic) IBOutlet UILabel *lblMatProvincial;



// Outlets para datos del médico
@property (retain, nonatomic) IBOutlet UITextField *txtApellido;
@property (retain, nonatomic) IBOutlet UITextField *txtNombre;
@property (retain, nonatomic) IBOutlet UITextField *txtEspecialidad;
@property (retain, nonatomic) IBOutlet UITextField *txtProvincia;
@property (retain, nonatomic) IBOutlet UITextField *txtLocalidad;
@property (retain, nonatomic) IBOutlet UITextField *txtLugar;
@property (retain, nonatomic) IBOutlet UITextField *txtTratamiento;
@property (retain, nonatomic) IBOutlet UITextField *txtMNacional;
@property (retain, nonatomic) IBOutlet UITextField *txtMProvincial;

@property (retain, nonatomic) IBOutlet UITextField *txtVisita;

// Botones
@property (retain, nonatomic) IBOutlet UIButton *btnManiana;
- (IBAction)tapBtnManiana:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnTarde;
- (IBAction)tapBtnTarde:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnFemenino;
- (IBAction)tapBtnFemenino:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnMasculino;
- (IBAction)tapBtnMasculino:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnEspecialidad;
- (IBAction)tapBtnEspecialidad:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnProvincia;
- (IBAction)tapBtnProvincia:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnLocalidad;
- (IBAction)tapBtnLocalidad:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnLugar;
- (IBAction)tapBtnLugar:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnTratamiento;
- (IBAction)tapBtnTatamiento:(id)sender;


@property (retain, nonatomic) IBOutlet UIButton *btnGuardar;
- (IBAction)tapBtnGuardar:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnCancelar;
- (IBAction)tapBtnCancelar:(id)sender;

@property (retain, nonatomic) IBOutlet UIButton *btnVisita;
- (IBAction)tapVisita:(id)sender;

- (IBAction)txtChangedContent:(id)sender;

- (id)initWithDefaultNib;
- (id)initWithDefaultNib:(NSInteger)idMedico andMode:(MTEditActionsType) modo;

@end
