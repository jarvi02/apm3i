//
//  ServiceLongitudArchivoNovedad.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 29/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ServiceLongitudArchivoNovedad.h"
#import "Config.h"

@implementation ServiceLongitudArchivoNovedad


-(NSInteger) longitud:(NSString*)urlBase
{
    NSInteger tamanio = 0;
    @autoreleasepool {
        NSString *urlFormated = [NSString stringWithFormat:@"%@%@?USER=%@&CMD=SWR_INFO&NAME=%@",
                                 urlBase,
                                 SettingForKey(REMOTE_PATH, @""),
                                 SettingForKey(ENTIDAD, @""),
                                 SettingForKey(TRANID, @"")];
        
        NSURL *url = [NSURL URLWithString:urlFormated];
        self.request = [ASIHTTPRequest requestWithURL:url];
        [self.request startSynchronous];
        
        NSError *error = [self.request error];
        if (!error) {
            
            NSString *response = [self.request responseString];
            [self logResponse:response];
            NSArray *arr = [response componentsSeparatedByString:@"="];
            tamanio = [[arr objectAtIndex:2] integerValue];
        }
    }
    return tamanio;
}

- (NSString*)textoParaLog {
    return @"ServiceLongitudArchivoNovedad";
}

@end
