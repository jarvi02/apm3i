//
//  FichaVisita.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 09/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "FichaVisita.h"
#import "DB.h"
#import "Config.h"

@implementation FichaVisita


- (id)init
{
    self = [super init];
    if (self) {

        self.fecha = @"";
        self.descripcion =@"";
        self.objetivos =@"";
        self.evaluacion =@"";
        self.pipe =@"";
        self.transmitido =@"";
        self.actividad =@"";
        self.objetivosProximo =@"";
    }
    return self;
}

- (void)dealloc
{
    [_fecha release];
    [_descripcion release];
    [_pipe release];
    [_fechaAsDate release];
    [super dealloc];
}



+(NSArray*)GetAllByMedico:(NSInteger)idMedico {
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    //NSString *sql = @"SELECT DISTINCT strftime('%@', med_visitas.fecha) as fecha, mtc_tiposvisita.descripcion, med_medicos.asignacion as asignacion, med_visitas.nodo || '|' || med_visitas.medico || '|' || med_visitas.visitador || '|' || med_visitas.id || '|' || strftime('%@', med_visitas.fecha) || '|' || med_visitas.tipo || '|' || med_visitas.domicilio || '|' || case when med_visitas.objetivos is null then '' else med_visitas.objetivos end || '|' || case when med_visitas.evaluacion is null then '' else med_visitas.evaluacion end as pipe, med_visitas.id as id, med_visitas.medico as medico, med_visitas.fecha as fechasDate FROM med_visitas INNER JOIN mtc_tiposvisita ON mtc_tiposvisita.id = med_visitas.tipo  inner join med_medicos on med_visitas.nodo = med_medicos.nodo and med_visitas.medico = med_medicos.id left outer join log_novedades on pipe = log_novedades.registro and log_novedades.transmitido = 0 WHERE med_visitas.nodo = %@  AND med_visitas.medico =  %d AND  med_visitas.visitador = %@;";
    
    NSString *sql = @"SELECT DISTINCT "
                            "med_visitas.id as _id, "
                            "strftime('%@', med_visitas.fecha, 'unixepoch', 'localtime') as fecha, "
                            "mtc_tiposvisita.descripcion, "
                            "med_visitas.fecha as fechaLong, "
                            "med_visitas.tipo, "
                            "med_visitas.visitador, "
                            "med_visitas.domicilio, "
                            "case "
                                "when med_visitas.objetivos is null then '' "
                                "else med_visitas.objetivos end as objetivos,  "
                            "case "
                                "when med_visitas.evaluacion is null then '' "
                                "else med_visitas.evaluacion end as evaluacion, "
                            "mtc_tiposvisita.contacto, "
                            "med_visitas.nodo || '|' || "
                                "med_visitas.medico || '|' || "
                                "med_visitas.visitador || '|' || "
                                "med_visitas.id || '|' || "
                                "strftime('%@', med_visitas.fecha, 'unixepoch', 'localtime') || '|' || "
                                "med_visitas.tipo || '|' || "
                                "med_visitas.domicilio || '|' || "
                                "case "
                                    "when med_visitas.objetivos is null then '' "
                                    "else med_visitas.objetivos end || '|' || "
                                "case "
                                    "when med_visitas.evaluacion is null then '' "
                                    "else med_visitas.evaluacion end "
                            "as pipe, "
                            "log_novedades.registro as transmitido, "
                            "med_medicos.actividad as actividad, "
                            "med_medicos.asignacion as asignacion, "
                            "med_medicos.objetivos as objetivos_prox "
                    "FROM med_visitas INNER JOIN "
                         "mtc_tiposvisita ON (mtc_tiposvisita.id = med_visitas.tipo) INNER JOIN "
                         "med_medicos ON (med_visitas.nodo = med_medicos.nodo AND med_visitas.medico = med_medicos.id) LEFT OUTER JOIN "
                         "log_novedades ON (pipe = log_novedades.registro AND log_novedades.transmitido = 0) "
                    "WHERE med_visitas.nodo = %@ AND med_visitas.medico = %d AND med_visitas.visitador = %@ "
                    "ORDER BY med_visitas.fecha;";
    
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *FormatoFecha = @"%d/%m/%Y";
    NSString *FormatoFechaNovedad = @"%m/%d/%Y %H:%M";
    
    NSString *query = [NSString stringWithFormat:sql, FormatoFecha, FormatoFechaNovedad ,nodo,idMedico,nodo];
    
#ifdef DEBUG_VISITA
    NSLog(@" - FechaVisita: GetAllByMedico - ");
    NSLog(@"%@",query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        FichaVisita *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[FichaVisita alloc] init];

            o.idMedico = idMedico;
            
            o.idVisita = sqlite3_column_int(statement, 0);
            
            _c = (char *)sqlite3_column_text(statement, 1);
            o.fecha = [NSString pCharToString:_c];
            
            _c = (char *)sqlite3_column_text(statement, 2);
            o.descripcion = [NSString pCharToString:_c];

            o.fechaAsDate = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 3)];
            
            o.idTipo = sqlite3_column_int(statement, 4);
            
            o.visitador = sqlite3_column_int(statement, 5);
            
            o.idDomicilio = sqlite3_column_int(statement, 6);
            
            _c = (char *)sqlite3_column_text(statement, 7);
            o.objetivos = [NSString pCharToString:_c];
            
            _c = (char *)sqlite3_column_text(statement, 8);
            o.evaluacion = [NSString pCharToString:_c];
            
            o.contacto = sqlite3_column_int(statement, 9);
            
            _c = (char *)sqlite3_column_text(statement, 10);
            o.pipe = [NSString pCharToString:_c];
            
            _c = (char *)sqlite3_column_text(statement, 11);
            o.transmitido = [NSString pCharToString:_c];
            
            _c = (char *)sqlite3_column_text(statement, 12);
            o.actividad = [NSString pCharToString:_c];
        
            o.asignacion = sqlite3_column_int(statement, 13);
        
            _c = (char *)sqlite3_column_text(statement, 14);
            o.objetivosProximo = [NSString pCharToString:_c];
        
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se puede ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
    
}

@end
