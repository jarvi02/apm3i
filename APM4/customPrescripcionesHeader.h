//
//  customPrescripcionesHeader.h
//  APM4
//
//  Created by Ezequiel on 3/20/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
@class customPrescripcionesHeader;

typedef enum
{
    kMTFieldDescripcion,
    kMTFieldTrimestre1,
    kMTFieldTrimestre2,
    kMTFieldTrimestre3,
    kMTFieldTrimestre4,
    kMTFieldTotal,
    kMTFieldMercado
} MTHeaderField;

typedef enum
{
    kMTOrderAsc,
    kMTOrderDesc
} MTHeaderOrder;

@protocol MTHeaderDelegate <NSObject>
@required
// Sent when the user tap a field.
- (void)customPrescHeader:(customPrescripcionesHeader *)header
            selectedField:(MTHeaderField)field
                    Order:(MTHeaderOrder)order;
@end


@interface customPrescripcionesHeader : UIView

@property (nonatomic, assign) id <MTHeaderDelegate>        delegate;

@property (retain, nonatomic) IBOutlet UIButton *lblProducto;
@property (retain, nonatomic) IBOutlet UIButton *lblTrimestre1;
@property (retain, nonatomic) IBOutlet UIButton *lblTrimestre2;
@property (retain, nonatomic) IBOutlet UIButton *lblTrimestre3;
@property (retain, nonatomic) IBOutlet UIButton *lblTrimestre4;
@property (retain, nonatomic) IBOutlet UIButton *lblTotal;
@property (retain, nonatomic) IBOutlet UIButton *lblMercado;

- (void)orderBy:(MTHeaderField)field reset:(BOOL)reset;

- (IBAction)tapProducto:(id)sender;
- (IBAction)tapTrimestre1:(id)sender;
- (IBAction)tapTrimestre2:(id)sender;
- (IBAction)tapTrimestre3:(id)sender;
- (IBAction)tapTrimestre4:(id)sender;
- (IBAction)tapTotal:(id)sender;
- (IBAction)tapMercado:(id)sender;
@end
