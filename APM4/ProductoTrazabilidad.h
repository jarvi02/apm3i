//
//  ProductoTrazabilidad.h
//  APM4
//
//  Created by Juan Pablo Garcia on 04/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductoTrazabilidad : NSObject

@property(nonatomic, strong) NSString *descripcion;
@property(nonatomic, strong) NSString *lote;
@property(nonatomic, strong) NSString *codigo;
@property(nonatomic, strong) NSDate *fechaVto;
@property(nonatomic, strong) NSString *orden1;
@property(nonatomic, strong) NSString *orden2;

@property(nonatomic, strong) NSString *evaluacion;
@property(nonatomic, strong) NSString *objetivos;
@property(nonatomic, strong) NSString *familia;

@property(nonatomic)BOOL promocionado;
@property(nonatomic)BOOL obsequio;
@property(nonatomic)BOOL literatura;
@property(nonatomic)NSUInteger stock;
@property(nonatomic)NSUInteger cantidad;
@property(nonatomic)NSUInteger nodo;
@property(nonatomic)NSUInteger medico;
@property(nonatomic)NSUInteger prodId;
@property(nonatomic)NSUInteger loteId;
@property(nonatomic)NSUInteger orden3;
@property(nonatomic)NSUInteger orden;
@property(nonatomic)NSUInteger ordenfamilia;

@property(nonatomic) NSUInteger ordenIngreso;
@property(nonatomic) BOOL       blocked;
@property(nonatomic) BOOL       vencido;

@property(nonatomic) BOOL       aPromocionar;

- (void)checkProducto;
+ (void)checkProductoList:(NSArray*)lista;


-(NSInteger)anteriorProductoFromList:(NSArray*)lista;
-(NSInteger)proximoProductoFromList:(NSArray*)lista;

+(NSArray *)getAll:(NSUInteger)idMedico;
+(NSArray *)getForMedico:(NSUInteger)idMedico includePromociones:(NSArray*)promos;
+(void)actualizarStock:(NSUInteger) producto lote:(NSUInteger)lote cantidad:(NSUInteger)cantidad;
+(void)actualizarStock:(NSUInteger) producto lote:(NSUInteger)lote nuevoStock:(NSUInteger)newstock;
+(NSArray *)getForMedico:(NSUInteger)idMedico includePromociones:(NSArray*)promos andFamiliaId:(NSUInteger)familiaId;
+(NSArray*)getFamilias;
+(NSArray*)getFamiliasFromMedico:(NSUInteger)idMedico;
+(int)getFamiliaIdfromProductoId:(int)productoId;
@end
