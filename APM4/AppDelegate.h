//
//  AppDelegate.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 24/01/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
// 

#import <UIKit/UIKit.h>

#define Delegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (nonatomic, retain) UIPopoverController * popoverController;

-(void)dismissPopOver:(BOOL)animated;

@end
