//
//  PromocionadosViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PromocionadosCell.h"
#import "PromocionadosHeader.h"

@interface PromocionadosViewController : UITableViewController


@property (nonatomic, retain) NSArray                       *data;
@property (retain, nonatomic) IBOutlet PromocionadosHeader  *tableHeader;
@property (nonatomic, retain) UINib                         *headerNib;
@property (retain, nonatomic) IBOutlet PromocionadosCell    *tmpCell;
@property (nonatomic, retain) UINib                         *cellNib;

- (id)initWithData:(NSArray*)aData;

@end
