//
//  historicoTableController.h
//  APM4
//
//  Created by Laura Busnahe on 7/12/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "historicoHeaderView.h"
#import "historicoCell.h"

@interface historicoTableController : UITableViewController

@property (nonatomic, strong) NSArray                       *data;
@property (nonatomic, strong) IBOutlet historicoCell        *tmpCell;
@property (nonatomic, strong) UINib                         *cellNib;
@property (nonatomic, strong) UINib                         *headerNib;

@property (strong, nonatomic) IBOutlet historicoHeaderView  *tableHeader;

- (id)initWithData:(NSArray*)aData;

@end
