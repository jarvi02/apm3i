//
//  StockMMCell.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockMMCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lbProducto;
@property (retain, nonatomic) IBOutlet UILabel *lbCantidad;
@property (retain, nonatomic) IBOutlet UILabel *lbCondigo;
@property (retain, nonatomic) IBOutlet UILabel *lbLote;
@property (retain, nonatomic) IBOutlet UILabel *lbFecha;

@end
