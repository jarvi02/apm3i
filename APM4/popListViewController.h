//
//  popListViewController.h
//  APM4
//
//  Created by Laura Busnahe on 2/21/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MTpopListView;

@interface popListViewController : UIViewController

@property (retain, nonatomic) IBOutlet UITableView *TableView;
@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;

@property (assign, nonatomic) MTpopListView        *delegate;

@property (nonatomic) BOOL                          withSearch;

- (void) useSearch:(BOOL) search;

@end
