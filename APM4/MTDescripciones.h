//
//  MTDescripciones.h
//  APM4
//
//  Created by Laura Busnahe on 9/16/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTDescripciones : NSObject <NSCopying>

@property(nonatomic, assign) NSUInteger         recID;
@property(nonatomic, strong) NSString*  descripcion;

- (NSString*)getDescription;
- (MTDescripciones*)copyWithZone:(NSZone*)zone;

+ (NSArray*)getAllVisita;
+ (NSArray*)getAllPotencial;
+ (NSString*)getDescriptionById:(NSUInteger)descripcion fromArray:(NSArray*)array;

@end
