//
//  customTotalDiarioCell.h
//  APM4
//
//  Created by Laura Busnahe on 7/29/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customTotalDiarioCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblDescripciones;
@property (retain, nonatomic) IBOutlet UILabel *lblCantM;
@property (retain, nonatomic) IBOutlet UILabel *lblCantT;
@property (retain, nonatomic) IBOutlet UILabel *lblMedM;
@property (retain, nonatomic) IBOutlet UILabel *lblMedT;
@property (retain, nonatomic) IBOutlet UILabel *lblCantidad;
@property (retain, nonatomic) IBOutlet UILabel *lblMedicos;

@property (retain, nonatomic) IBOutlet UILabel *lblBackground;
@end
