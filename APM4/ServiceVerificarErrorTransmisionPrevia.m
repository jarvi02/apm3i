//
//  ServiceVerificarErrorTransmisionPrevia.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ServiceVerificarErrorTransmisionPrevia.h"
#import "Config.h"

@implementation ServiceVerificarErrorTransmisionPrevia


-(BOOL*) verificar:(NSString*)urlBase
{
    BOOL respuesta = NO;
    
    @autoreleasepool {
        NSString *urlFormated = [NSString stringWithFormat:@"%@%@?USER=%@&CMD=SVC_EXISTS_FILE&NAME=%@.ERR",
                                 urlBase,
                                 SettingForKey(REMOTE_PATH, @""),
                                 SettingForKey(ENTIDAD, @""),
                                 SettingForKey(TRANID, @"")];
        
        NSURL *url = [NSURL URLWithString:urlFormated];
        
#ifdef DEBUG_TRANSMICION
        NSLog(@" - verificar: %@", urlFormated);
#endif
        
        self.request = [ASIHTTPRequest requestWithURL:url];
        [self.request startSynchronous];
        
        NSError *error = [self.request error];
        if (!error) {
            
            NSString *response = [self.request responseString];
            [self logResponse:response];
            respuesta = ([response isEqualToString:@"1"]) ? YES:NO;
        }
    }
    return respuesta;
}

- (NSString*)textoParaLog {
    return @"ServiceVerificarErrorTransmisionPrevia";
}

@end
