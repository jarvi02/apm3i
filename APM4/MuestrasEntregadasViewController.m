//
//  MuestrasEntregadasViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MuestrasEntregadasViewController.h"
#import "MuestrasEntregadas.h"
#import "NSDate+extensions.h"

@interface MuestrasEntregadasViewController ()

@end

@implementation MuestrasEntregadasViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Muestras entregadas";
    }
    return self;
}

- (void)dealloc {
    
    [_data      release];
    
    [_tmpCell   release];
	[_cellNib   release];
    
    [_table     release];
    
    [super dealloc];
}

-(NSString *)getTitleForHeader{
    return @"Muestras entregadas";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"cartera_48"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.table.rowHeight = 44.0;
    self.cellNib = [UINib nibWithNibName:@"MuestrasEntregadasCell" bundle:nil];
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
}

- (void) viewDidAppear:(BOOL)animated
{
    self.data = [MuestrasEntregadas GetAll];

    [self.loadingView hide];
    
    [self.table reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Table Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of time zone names.
	return [self.data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MyIdentifier = @"MEIdentifier";
	
    // Obtain the cell object.
	MuestrasEntregadasCell *cell = (MuestrasEntregadasCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	// If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
		cell = self.tmpCell;
		self.tmpCell = nil;
        
        
    }
    
    MuestrasEntregadas *o = [self.data objectAtIndex:indexPath.row];
    
    NSDate *fecha = [NSDate dateWithTimeIntervalSince1970:o.fecha];
    fecha = fecha.dateAsDateWithoutTime;
    cell.lbFecha.text = [fecha formattedStringUsingFormat:@"dd/MM/yyyy"];
    
    cell.lbNombreMedico.text = o.apellidoNombre;
    cell.lbProducto.text = o.descripcion;
    cell.lbCodigo.text = o.codigo;
    cell.lbLote.text = o.lote;
    cell.lbMuestras.text = [NSString stringWithFormat:@"%d", o.muestras];
    
    NSDate *fechaVto = [NSDate dateWithTimeIntervalSince1970:o.fecha_vto];
    fechaVto = fechaVto.dateAsDateWithoutTime;
    cell.lbFechaVto.text = [fechaVto formattedStringUsingFormat:@"MM/yyyy"];
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGRect frame = CGRectMake(0, 0, 768, 22);
    UILabel *background = [[UILabel alloc] initWithFrame:frame];
    background.autoresizingMask = 34;
    background.backgroundColor = UIColorFromRGB(defaultHeaderCellColor);
    
    UILabel *label;
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(20, 0, 86, 20);
    label.autoresizingMask = 36;
    label.text = @"Fecha";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(108, 0, 215, 20);
    label.autoresizingMask = 38;
    label.text = @"Apellido y Nombre";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(331, 0, 151, 20);
    label.autoresizingMask = 35;
    label.text = @"Producto";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(518, 0, 72, 20);
    label.autoresizingMask = 33;
    label.text = @"Codigo";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(581, 0, 72, 20);
    label.autoresizingMask = 33;
    label.text = @"Lote";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];

    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(646, 0, 50, 20);
    label.autoresizingMask = 33;
    label.text = @"Cant.";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(686, 0, 80, 20);
    label.autoresizingMask = 33;
    label.text = @"Fecha Vto";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    return [background autorelease];
}


@end
