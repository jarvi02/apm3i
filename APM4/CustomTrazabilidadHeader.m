//
//  CustomTrazabilidadHeader.m
//  APM4
//
//  Created by Laura Busnahe on 7/25/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomTrazabilidadHeader.h"

@implementation CustomTrazabilidadHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
