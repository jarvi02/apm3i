//
//  NSString+Validaciones.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 14/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validaciones)

-(BOOL)isValidIP;
-(BOOL)isNumber;

@end
