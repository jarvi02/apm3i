//
//  ParteDiario.h
//  APM4
//
//  Created by Juan Pablo Garcia on 20/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ParteDiarioData;

@interface ParteDiario : NSObject

@property (nonatomic, retain) ParteDiarioData *maniana;
@property (nonatomic, retain) ParteDiarioData *tarde;
@property (nonatomic, retain) NSString *descripcion;
@property (nonatomic) NSUInteger identificador;
@property (nonatomic, retain) NSDate *fecha;
@property (nonatomic, assign) BOOL transmitido;

+ (NSArray *)  getAll:(NSDate *)fecha;
+ (NSUInteger) lastId:(NSUInteger)partediario;
+ (BOOL)       tieneTareasCargadas:(NSDate *)date;
+ (NSArray*)   tieneTareasCargadasEntreFecha:(NSDate *)date hasta:(NSDate*)dateHasta;

+ (void) remove:(NSDate *)fecha;
+ (void) save:(ParteDiarioData *)p;
+ (BOOL) existsCabecera:(NSDate *)date;
+ (void) insertCabecera:(NSDate *)date;
+ (void) insertTarea:(NSInteger) idTarea actividad:(NSString *)actividad cantidad:(NSInteger)cantidad parteDiario:(NSInteger)parteDiario tarea:(NSInteger)tarea;

- (NSUInteger) cantidadManiana;
- (NSUInteger) cantidadTarde;
- (NSUInteger) cantidadTotal;

+ (NSString *) getForNovedad:(NSDate *) fecha;
+ (NSString *) getTareaForNovedad:(NSInteger) idTarea parteDiario:(NSInteger)parteDiario;

+ (void) deleteTarea:(NSUInteger)parteDiario actividad:(NSString *)actividad;
+ (void) deleteTareaEx:(ParteDiarioData*)parteDiarioData;
+ (void) eliminarTareas:(NSDate*)date conservarVisitas:(BOOL)conservaVisitas;

+ (void) addCantidad:(NSUInteger)idTarea parteDiario:(NSUInteger)parteDiario actividad:(NSString *)actividad tarea:(NSUInteger)tarea;
+ (void) restarTarea:(NSUInteger)idTarea parteDiario:(NSUInteger)parteDiario actividad:(NSString *)actividad tarea:(NSUInteger)tarea;

- (void) setCantidadForManiana:(NSUInteger)maniana;
- (void) setCantidadForTarde:(NSUInteger)tarde;

+ (ParteDiarioData *) getParteDiario:(NSUInteger) tarea todasTareas:(NSInteger)todasTareas actividad:(NSString *)actividad todasFechas:(NSInteger)todasFechas fecha:(NSDate *)fecha rango:(NSInteger)rango desde:(NSDate *)desde hasta:(NSDate *)hasta;

+ (ParteDiarioData *) getParteDiarioEx:(NSUInteger) tarea todasTareas:(NSInteger)todasTareas actividad:(NSString *)actividad todasFechas:(NSInteger)todasFechas fecha:(NSDate *)fecha rango:(NSInteger)rango desde:(NSDate *)desde hasta:(NSDate *)hasta;


+ (NSArray *)getAllParteDiario:(NSDate *)fecha;


+ (NSInteger)getCountWithoutDoctorsForDate:(NSDate *)fecha;

+ (void)cleanTareasParte;
+ (void)cleanParte;

@end
