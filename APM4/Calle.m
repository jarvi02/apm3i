//
//  Calle.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 16/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Calle.h"
#import "DB.h"

@implementation Calle


- (id)init
{
    self = [super init];
    if (self) {
        self.descripcion = @"";
        self.idCalle = 0;
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [super dealloc];
    
}

- (NSString*)getDescription
{
    return [NSString stringWithFormat:@"%@", self.descripcion];
}


+(NSArray*) GetAllBy:(NSInteger)pais provincia:(NSInteger)provincia localidad:(NSInteger)localidad{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"SELECT id, descripcion FROM geo_calles where provincia = %d and localidad = %d and pais = %d;";
    
    NSString *query = [NSString stringWithFormat:sql, provincia, localidad, pais];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        Calle *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[Calle alloc] init];
            
            o.idCalle = sqlite3_column_int(statement, 0);
            
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
}


-(id)copy{
    
    Calle *o = [[Calle alloc] init];
    o.idCalle = self.idCalle;
    o.descripcion = [NSString stringWithFormat:@"%@", self.descripcion];
    
    return [o autorelease];
}


@end
