//
//  MTstdDistribucionCantidad.m
//  APM4
//
//  Created by Laura Busnahe on 9/9/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTstdDistribucionCantidad.h"
#import "Config.h"
#import "DB.h"

@implementation MTstdDistribucionCantidad

- (id)init
{
    self = [super init];
    if (self)
    {
        self.descripcion = @"";
        self.maniana     = 0;
        self.tarde       = 0;
        self.total       = 0;
        self.porcManiana = 0.0f;
        self.porcTarde   = 0.0f;
        self.porcTotal   = 0.0f;
    }
    
    return self;
}


+ (NSArray*)getAll
{
    NSMutableArray *result = [[NSMutableArray alloc] init];

    NSString *sql =
    @"SELECT "
        "0 as _id, "
        "mm.visitasrealizadas, "
        "SUM(case when  mm.actividad = 'M' then 1 else 0  end) as Ma, "
        "ROUND (CAST ( CAST (SUM(case when  mm.actividad = 'M' then 1 else 0  end) * 100 as float )  / "
               "CAST ((select count(*) from med_medicos where nodo = %@ and actividad = 'M') AS FLOAT) AS FLOAT ) ,2 ) "
            "as porc_m, "
        "SUM(case when  mm.actividad = 'T'  then  1 else 0 end) as Ta, "
        "ROUND (CAST ( CAST (SUM(case when  mm.actividad = 'T' then 1 else 0  end) * 100 as float )  / "
               "CAST ((select count(*) from med_medicos where nodo = %@ and actividad = 'T') AS FLOAT) AS FLOAT ) ,2 ) "
            "as porc_t, "
        "(SUM(case when  mm.actividad = 'M' then 1 else 0  end) + "
            "SUM(case when  mm.actividad = 'T'  then  1 else 0 end) ) "
            "as totalvisitas, "
        "ROUND (CAST ( CAST ((SUM(case when  mm.actividad = 'M' then 1 else 0  end) + "
                             "SUM(case when  mm.actividad = 'T'  then  1 else 0 end) ) * 100 as float )  / "
               "CAST ((select count(*) from med_medicos where nodo = %@ ) AS FLOAT) AS FLOAT ) ,2 ) "
            "as porc_total "
    "FROM med_medicos mm "
        "LEFT JOIN std_acumuladasanteriores aa on (mm.nodo = aa.nodo and mm.id = aa.medico) "
    "WHERE mm.nodo =  %@ "
    "GROUP BY visitasrealizadas "
    " "
    "UNION ALL"
    " "
    "SELECT "
        "0 as _id, "
        "'Totales', "
        "SUM(case when  mm.actividad = 'M' then 1 else 0  end) as Ma, "
        "ROUND (CAST ( CAST (SUM(case when  mm.actividad = 'M' then 1 else 0  end) * 100 as float )  / "
                      "CAST ((select count(*) from med_medicos where nodo = %@ ) AS FLOAT) AS FLOAT ) ,2 ) "
            "as porc_m, "
        "SUM(case when  mm.actividad = 'T'  then  1 else 0 end) as Ta, "
        "ROUND (CAST ( CAST (SUM(case when  mm.actividad = 'T' then 1 else 0  end) * 100 as float )  / "
               "CAST ((select count(*) from med_medicos where nodo = %@ ) AS FLOAT) AS FLOAT ) ,2 ) "
            "as porc_t, "
        "(SUM(case when  mm.actividad = 'M' then 1 else 0  end) +  "
            "SUM(case when  mm.actividad = 'T'  then  1 else 0 end) ) "
            "as totalvisitas, "
        "ROUND (CAST ( CAST ((SUM(case when  mm.actividad = 'M' then 1 else 0  end) + "
                             "SUM(case when  mm.actividad = 'T'  then  1 else 0 end) ) * 100 as float )  / "
               "CAST ((select count(*) from med_medicos where nodo = %@ ) AS FLOAT) AS FLOAT ) ,2 ) "
            "as porc_total "
    "FROM med_medicos mm "
        "LEFT JOIN std_acumuladasanteriores aa on (mm.nodo = aa.nodo and mm.id = aa.medico) "
    "WHERE mm.nodo =  %@";
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql,
                       nodo, nodo, nodo, nodo,
                       nodo, nodo, nodo, nodo];
    
#ifdef DEBUG_DISTRIBUCIOM
    NSLog(@"--- MTDistribucionCantidad ---");
    NSLog(@" - query: %@", query);
    
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTstdDistribucionCantidad *o = [[MTstdDistribucionCantidad alloc] init];
            
            // Descripción (Cantidad de visitas)
            _c = (char *)sqlite3_column_text(statement, 1);
            o.descripcion = [NSString pCharToString:_c];
            
            // Mañana
            o.maniana = sqlite3_column_int(statement, 2);
            
            // Porcentaje Mañana
            o.porcManiana = sqlite3_column_double(statement, 3);
            
            // Tarde
            o.tarde = sqlite3_column_int(statement, 4);
            
            // Porcentaje Tarde
            o.porcTarde = sqlite3_column_double(statement, 5);
            
            // Total
            o.total = sqlite3_column_int(statement, 6);
            
            // Porcentaje Total
            o.porcTotal = sqlite3_column_double(statement, 7);
            
            [result addObject:o];
        }
        
	} else
    {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}

@end
