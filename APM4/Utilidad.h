//
//  Utilidad.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 06/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilidad : NSObject

@property(nonatomic, assign) NSInteger idUtilidad;
@property(nonatomic, retain) NSString *descripcion;

+(NSArray*) GetAll;


@end
