//
//  TipoVisita.h
//  APM4
//
//  Created by Juan Pablo Garcia on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TipoVisita : NSObject

@property(nonatomic, retain) NSString *descripcion;
@property(nonatomic) NSInteger tipoId;
@property(nonatomic) NSInteger estadoId;

+(NSArray*) GetAll;

@end
