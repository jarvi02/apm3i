//
//  MMEditViewController.h
//  APM4
//
//  Created by Laura Busnahe on 3/26/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMEditViewController : UIViewController

// Methods
- (id)initViewDefaultNib;
//- (id)initViewDefaultNibForSelection:(MTProducto*)selProducto;

- (IBAction)btnAceptar:(id)sender;
- (IBAction)btnCancelar:(id)sender;

- (IBAction)tapMotivo:(id)sender;
@end
