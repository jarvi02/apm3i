//
//  GlobalDefines.h
//  APM4
//
//  Created by Laura Busnahe on 4/4/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#ifndef APM4_GlobalDefines_h
#define APM4_GlobalDefines_h


// Defininición de colores.

#define skyblue_iOS7Color   0x5AC8FA
#define blue_iOS7Color      0x007AFF
#define yellow_iOS7Color    0xFFCC00
#define green_iOS7Color     0x4CD964
#define orange_iOS7Color    0xFF9500
#define red_iOS7Color       0xFF3B30
#define magenta_iOS7Color   0xFF2D55
#define gray_iOS7Color      0x8E8E93
#define lightGrayTitles_iOS7Color 0xF7F7F7
#define grayTitles_iOS7Color      0xEFEFF4

#define defaultTitlesColor      lightGrayTitles_iOS7Color
#define defaultTitlesFontColor  0x000000

#define defaultFixedInteractiveCellFontColor  0x52A5FF
#define defaultInteractiveCellFontColor       blue_iOS7Color

#define darkHeaderAscColor     0x39A34B
#define darkHeaderDescColor    0xCF3027
#define lightHeaderAscColor    green_iOS7Color
#define lightHeaderDescColor   red_iOS7Color
#define defaultHeaderCellColor      grayTitles_iOS7Color
#define defaultHeaderCellFontColor        0x000000
#define defaultCellFontColor        0x000000

#define defaultButtonColor      blue_iOS7Color
#define defaultEditModeButtonColor blue_iOS7Color
#define defaultDisabledButtonColor gray_iOS7Color
#define defaultAccesoryColor       blue_iOS7Color

#define menuButtonColor         blue_iOS7Color
#define obligatiorioColor       orange_iOS7Color
#define eliminarButtonColor     red_iOS7Color
#define guardarButtonColor      green_iOS7Color
#define aceptarButtonColor      defaultButtonColor
#define editarButtonColor       defaultButtonColor
#define agregarButtonColor      defaultButtonColor
#define cancelarButtonColor     defaultButtonColor


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]






#pragma mark -
#pragma mark Ajuste de Muestas Médicas



#pragma mark -
#pragma mark Ajuste de Muestas Médicas

#define INCLUDE_AJUSTEMM                    // Habilita la opción de Ajuste MM en el menú.


#pragma mark -
#pragma mark Prescripciones

#define INCLUDE_PRESCRICIONESTABLE          // Muestra el contenido de la tabla de Prescripciones.
#define INCLUDE_PRESCRICIONES               // Habilita la opción Prescripciones en el menú.



#pragma mark -
#pragma mark Previsto Mensual
#define NEW_PREVISTOM



#pragma mark -
#pragma mark ABM Médico

#define USE_DOUBLEQUOTE                     // En los comandos SQL utiliza comilla doble para los strings.
                                            // TODO: Completar implementación en todo el código.
                                            // y resolver el problema que se presenta cuando la longitud de caracteres es 1 (lo
                                            // guarda como 0.

#define INCLUDE_ALTAMEDICO                  // Habilita el alta de médicos.
#define INCLUDE_MODIFICARMEDICO             // Habilita la modificación de médicos.
#define INCLUDE_BAJAMEDICO                  // Habilita aplica baja de médico.
#define INCLUDE_REVBAJAMEDICO //



#pragma mark -
#pragma mark Cartera


#pragma mark -
#pragma mark Nuevo menú
//#define USE_NEW_MENU                         // Define si utiliza o no el nuevo menú


#pragma mark -
#pragma mark Visita
#define FORCE_PROMOCION_SIN_DESCARGA        // Este valor fuerza las vistas de descarga sin promoción


#pragma mark -
#pragma mark Parte Diario




#pragma mark -
#pragma mark Debug

#define FORCE_PORCENTAJESENPARTEDIARIO      NO      // Fuerza el uso de procentajes en el Parte Diario.
#define DEBUG_QUEUE                         NO
#define DEBUG_TRANSMICIONX                  NO
#define DEBUG_VERIFICA_PARTE_AYER           NO

//#define DEBUG_ABMMEDICO                     // Debug para el módulo ABM de Médico.
//#define DEBUG_PARTEDIARIO                   // Debug para módulo de Parte Diario.

//#define DEBUG_AJUSTEMM      1               // Debug para módulo de Ajuste de Muestras Médicas.
//#define DEBUG_DBEXTENDED    1               // Debug para módulo de Base de Datos extendido.
//#define DEBUG_CARTERA                       // Debug para módulo Cartera de Médicos
//#define DEBUG_GRUPOS                        // Debug para módulo Grupos
//#define DEBUG_PREVISTOM    1                // Debug para módulo Previsto Mensual.
//#define DEBUG_PRESCRIPCIONES    1           // Debug para módulo Prescripciones.
//#define DEBUG_VISITA                          // Debug para ABM de visitas.
//#define DEBUG_TRAZABILIDAD                  // Debug para trazabilidad en Vistita y Ficha de visita.
//#define DEBUG_TRANSMICION                   // Debug para el proceso de transmición.
//#define DEBUG_HISTORICO                     // Debug para Histórico de Visitas y Productos.

// Estadísticas
//#define DEBUG_COBERTURA                     // Debug para estadísticas de Cobertura.
//#define DEBUG_DETALLEPARTE                  // Debug para estadísticas de Detalle de parte diario.
//#define DEBUG_INFORMEDIARIO                 // Debug para estadísticas de Totales de informa diario.
//#define DEBUG_DISTRIBUCIOM                  // Debug para estadísticas de Totales de informa diario.
//#define DEBUG_VISITASPORDIAS                // Debug para estadísticas de Visitas pro días.
//#define DEBUG_STDNOVISITA                   // Debug para estadítica de No Visitas por Motivo.
//#define DEBUG_FORMATOREGIONAL               // Debug para formato Regional.
//#define DEBUG_OBJETIVOS                     // Debug para Ficha -> Objetivos.
//#define DEBUG_EVALUACIONPRODUCTOS             // Debug para Ficha -> Evaluación de Productos.


#endif
