//
//  Tipo.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tipo : NSObject

@property(nonatomic, assign) NSInteger idTipo;
@property(nonatomic, retain) NSString *descripcion;

+(NSArray*) GetAll;

@end
