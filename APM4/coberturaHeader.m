//
//  coberturaHeader.m
//  APM4
//
//  Created by Laura Busnahe on 7/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "coberturaHeader.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define colorASC     darkHeaderAscColor
#define colorDESC    darkHeaderDescColor
#define colorNormal  0x000000

#pragma mark -
@implementation coberturaHeader

{
    MTCoberturaHeaderField     orderedBy;
    MTHeaderOrder              order;
}

#pragma mark Private methods
- (id)init
{
    self = [super init];
    if (self)
    {
        self.delegate = nil;
        orderedBy = kMTFieldDescripcion;
        order     = kMTOrderAsc;
    }
    
    return self;
}

- (void)orderBy:(MTCoberturaHeaderField)field sender:(id)sender reset:(BOOL)reset
{
    // Chequeo si el delegado fue asignado.
    if (!self.delegate)
        return;
    
    // Chequeo si se trata del mismo campo por el que ya estaba ordenado.
    if (orderedBy != field)
    {
        // Si no es igual, asigno el nuevo campo y cambio el orden a ascendete.
        orderedBy = field;
        order     = kMTOrderAsc;
    } else
    {
        // Si es el mismo campo, cambio el orden.
        if (order == kMTOrderAsc)
            order = kMTOrderDesc;
        else
            order = kMTOrderAsc;
    }
    
    if (reset)
        order = kMTOrderAsc;
    
    // Coloreo el label seleccionado.
    [self colorLabels:sender];
    
    // Llamo el método del delegate.
    if ([self.delegate respondsToSelector:@selector(customCoberturaHeader:selectedField:Order:)])
        [self.delegate customCoberturaHeader:self selectedField:orderedBy Order:order];
}

- (void)colorLabels:(id)sender
{
    NSUInteger newColor = order == kMTOrderAsc ? colorASC : colorDESC;
    
    [self.lblDescripcion    setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblFluctuantes    setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblPorcentaje     setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblProgramados    setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblRevisitas      setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblTotal          setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblVisitados      setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    
    UIButton *o = (UIButton*)sender;
    if ([o respondsToSelector:@selector(setTitleColor:forState:)])
        [o setTitleColor:UIColorFromRGB(newColor) forState:UIControlStateNormal];
    
}

#pragma mark -
#pragma mark IBAction methods
- (IBAction)tapLabel:(id)sender
{
    MTCoberturaHeaderField field = kMTFieldDescripcion;
    
    if (sender == self.lblDescripcion)
        field = kMTFieldDescripcion;
    
    if (sender == self.lblFluctuantes)
        field = kMTFieldFluctuantes;
    
    if (sender == self.lblPorcentaje)
        field = kMTFieldPorcentaje;
    
    if (sender == self.lblProgramados)
        field = kMTFieldProgramados;
    
    if (sender == self.lblRevisitas)
        field = kMTFieldRevisitas;
    
    if (sender == self.lblTotal)
        field = kMTFieldTotal;
    
    if (sender == self.lblVisitados)
        field = kMTFieldVisitados;
    
    
    [self orderBy:field sender:sender reset:NO];
}
#pragma mark -
@end
