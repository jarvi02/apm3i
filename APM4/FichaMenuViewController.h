//
//  FichaMenuViewController.h
//  APM4
//
//  Created by Juan Pablo Garcia on 07/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MenuViewController.h"
#import "CarteraViewController.h"

@interface FichaMenuViewController : MenuViewController

@property(nonatomic, assign) CarteraViewController *carteraViewControllerReference;

@end
