//
//  ParteDiarioViewController.h
//  APM4
//
//  Created by Juan Pablo Garcia on 19/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "popListExtended.h"
#import "CustomParteDiarioCell.h"
#import "DateSelectorViewController.h"
#import "Ciclo.h"

typedef NS_ENUM(NSInteger, ParteDiarioVCMode)
{
    ParteDiarioSimple,
    ParteDiarioDesdeHasta
};

@interface ParteDiarioViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate,
                                                             DateSelectorDelegate, UIAlertViewDelegate, MTpopListViewDelegate>
{
    NSInteger currentIndex;
    BOOL visitaFinDeSemana;
    BOOL parteObligatorio;
    BOOL usaPorcentaje;
    
    BOOL tieneTareasCargadas;
    BOOL huboModificaciones;
    
    NSUInteger nuevaCantidadManiana;
    NSUInteger nuevaCantidadTarde;

    
    id datePickerCaller;
}

@property (retain, nonatomic) NSMutableArray    *arrayComboManiana;
@property (retain, nonatomic) NSMutableArray    *arrayComboTarde;


@property (nonatomic, assign) ParteDiarioVCMode viewControllerMode;

@property (nonatomic, retain) NSDate *currentDate;
@property (nonatomic, retain) NSArray *partes;

@property (retain, nonatomic) MTpopListView                 *popupMenu;

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UITextField *dateTextField;
@property (nonatomic, retain) IBOutlet CustomParteDiarioCell *tmpCell;
@property (nonatomic, retain) IBOutlet UIButton *selectDateButton;
@property (retain, nonatomic) IBOutlet UILabel *lblYaTransmitido;

@property (nonatomic, retain) Ciclo *cicloActual;
@property (nonatomic, retain) Ciclo *cicloProximo;
@property (nonatomic) NSUInteger totalManiana;
@property (nonatomic) NSUInteger totalTarde;
@property (nonatomic) double total;
//@property (nonatomic) NSUInteger total;
@property (nonatomic, retain) NSMutableArray *inserted;
@property (nonatomic, retain) NSMutableArray *updated;
@property (nonatomic, retain) NSMutableArray *deleted;

// Parte diario Desde-Hasta
@property (nonatomic, retain) NSDate    *dateDesde;
@property (nonatomic, retain) NSDate    *dateHasta;
@property (retain, nonatomic) IBOutlet UILabel      *lblFecha;
@property (retain, nonatomic) IBOutlet UILabel      *lblFechaDesde;
@property (retain, nonatomic) IBOutlet UITextField  *txtFechaDesde;
@property (retain, nonatomic) IBOutlet UIButton     *btnFechaDesde;
@property (retain, nonatomic) IBOutlet UILabel      *lblFechaHasta;
@property (retain, nonatomic) IBOutlet UITextField  *txtFechaHasta;
@property (retain, nonatomic) IBOutlet UIButton     *btnFechaHasta;


// Popup
@property (retain, nonatomic) IBOutlet UIView *frameView;
@property (nonatomic, retain) IBOutlet UIView *popup;
@property (retain, nonatomic) IBOutlet UILabel *lblTitulo;
@property (nonatomic, retain) IBOutlet UITextField *manianaText;
@property (nonatomic, retain) IBOutlet UITextField *tardeText;
@property (nonatomic, retain) UITextField *current;

-(IBAction)pressedAceptarPop:(id)sender;
-(IBAction)pressedCancelarPop:(id)sender;

// Popup Porcentaje
@property (retain, nonatomic) IBOutlet UILabel *lblPorcTitulo;
@property (retain, nonatomic) IBOutlet UIView *popupPorcetaje;
@property (retain, nonatomic) IBOutlet UITextField *txtPorcManiana;
@property (retain, nonatomic) IBOutlet UITextField *txtPorcTarde;
@property (retain, nonatomic) IBOutlet UIButton *btnPorcManiana;
@property (retain, nonatomic) IBOutlet UIButton *btnPorcTarde;

- (IBAction)pressedPorcManiana:(id)sender;
- (IBAction)pressedPorcTarde:(id)sender;

// Actions

-(IBAction)pressedGuardar:(id)sender;
-(IBAction)pressedCancelar:(id)sender;

// Date Selector

-(IBAction)selectDate:(id)sender;

@end
