//
//  ServiceVerificarExisteNovedad.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ServiceVerificarExisteNovedad.h"
#import "Config.h"

#import "GlobalDefines.h"

@implementation ServiceVerificarExisteNovedad

-(NSInteger) verificar:(NSString*)urlBase
{
    NSInteger respuesta = -1;
    @autoreleasepool {
        NSString *urlFormated = [NSString stringWithFormat:@"%@%@?USER=%@&CMD=SVC_EXISTS_RPR&ID=%@",
                                 urlBase,
                                 SettingForKey(REMOTE_PATH, @""),
                                 SettingForKey(ENTIDAD, @""),
                                 SettingForKey(TRANID, @"")];
#ifdef DEBUG_TRANSMICION
        NSLog(@"VerificarNovedad: %@", urlFormated);
#endif
        
        NSURL *url = [NSURL URLWithString:urlFormated];
        self.request = [ASIHTTPRequest requestWithURL:url];
        [self.request startSynchronous];
        
        NSError *error = [self.request error];
        if (!error)
        {
            
            NSString *response = [self.request responseString];
            [self logResponse:response];
            
            respuesta =  ([response isEqualToString:@"1"]) ? 1:0;
        } else
        {
            NSString *str = [NSString stringWithFormat:@"TX0009 (sec. %@) En este momento no es posible obtener las novedades. Intentelo nuevamente en unos minutos.", SettingForKey(TRANID, @"")];
            [self performSelectorOnMainThread:@selector(alerta:) withObject:str waitUntilDone:YES];
        }
    }
    
    return respuesta;
}

- (NSString*)textoParaLog {
    return @"ServiceVerificarExisteNovedad";
}

@end
