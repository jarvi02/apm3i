//
//  ProductoTrazabilidad.m
//  APM4
//
//  Created by Juan Pablo Garcia on 04/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ProductoTrazabilidad.h"
#import "Promocion.h"
#import "DB.h"
#import "Config.h"

#import "GlobalDefines.h"
#import "NSStringExtended.h"
#import "NSDateExtended.h"

@implementation ProductoTrazabilidad

- (void)checkProducto
{
    // Ete método chequea si el producto está vencido.
    self.vencido = NO;
    
    if ([[self.fechaVto dateAsDateWithoutTime] timeIntervalSince1970] <= [[NSDate dateWithoutTime] timeIntervalSince1970])
    {
        self.blocked = YES;
        self.vencido = YES;
    }
    
    if ((self.stock == 0))
        self.blocked = YES;
}

+ (void)checkProductoList:(NSArray*)lista
{
    // Chequeo que la lista se un objeto válido.
    if (!lista)
        return;
    
    // Chequeo que la lista tenga elementos.
    if ([lista count] == 0)
        return;
    
    //  Chequeo que al menos el primer elemento de la lista sea de la clase ProductoTrazabilidad.
    if (![[lista objectAtIndex:0] isKindOfClass:[ProductoTrazabilidad class]])
        return;
    
    // Si alguna de las condiciones anteriores no se se cumple, salgo del método.
    
    NSString    *lastCodigo     = @"";
    NSUInteger  lastVencimiento = 0;
    NSInteger   lastStock       = 0;
    BOOL        lastVencido     = NO;
    
    
    for (ProductoTrazabilidad *p in lista)
    {
        p.blocked = NO;
        [p checkProducto];              // Chequeo si el producto está vencido
        
        // Chequeo si debo bloquear el producto.
        if ([p.codigo isEqualToString:lastCodigo])
        {
            
            if ((lastVencimiento < [p.fechaVto timeIntervalSince1970]) &&
                (lastStock) && (!lastVencido))
                p.blocked = YES;
            
#ifdef DEBUG_TRAZABILIDAD
            NSLog(@"--- Producto: %@ ---", lastCodigo);
            NSLog(@"- lastStrock: %d", lastStock);
            NSLog(@"- lastVencimiento: %d", lastVencimiento);
#endif
            
//            lastVencimiento = [p.fechaVto timeIntervalSince1970];
//            lastStock       = p.stock;
            
#ifdef DEBUG_TRAZABILIDAD
            NSLog(@"- Strock: %@", [NSString intToStr:p.stock-p.cantidad]);
            NSLog(@"- Vencimiento: %@", [NSString intToStr:[p.fechaVto timeIntervalSince1970]]);
#endif

            
        } else
        {
//            lastVencimiento = 0;
//            lastStock       = 0;
        }
        
        // Almaceno temporalmente los valor delproducto leido para compararlos con el siguiente.
        lastVencimiento = [p.fechaVto timeIntervalSince1970];
        lastCodigo      = [NSString string:p.codigo ifNull:@""];
        lastStock       = p.stock-p.cantidad;
        lastVencido     = p.vencido;
    }
}

-(NSInteger)proximoProductoFromList:(NSArray*)lista
{
    NSInteger result = NSNotFound;
    NSInteger indice = NSNotFound;
    
    if (!lista)
        return NSNotFound;
    
    if ([lista count] < 1)
        return NSNotFound;
    
    if (![[lista objectAtIndex:0] isKindOfClass:[ProductoTrazabilidad class]])
        return NSNotFound;
    
    
    // Obtengo el índice del objecto actual en la lista.
    indice = [lista indexOfObject:self];
    
    for (NSInteger i = (indice+1); i<[lista count]; i++)
    {
        ProductoTrazabilidad *p = [lista objectAtIndex:i];
        if ((p.prodId == self.prodId))
        {
            return i;
        }
    }
    
    return result;
}

-(NSInteger)anteriorProductoFromList:(NSArray*)lista
{
    NSInteger result = NSNotFound;
    NSInteger indice = NSNotFound;
    
    if (!lista)
        return NSNotFound;
    
    if ([lista count] < 1)
        return NSNotFound;
    
    if (![[lista objectAtIndex:0] isKindOfClass:[ProductoTrazabilidad class]])
        return NSNotFound;
    
    
    // Obtengo el índice del objecto actual en la lista.
    indice = [lista indexOfObject:self];
    if (indice == 0)
        return NSNotFound;
    
    for (NSInteger i = (indice-1); i>=0; i--)
    {
        ProductoTrazabilidad *p = [lista objectAtIndex:i];
        if ((p.prodId == self.prodId) && (p.loteId == self.loteId))
        {
            return i;
        }
    }
    
    return result;
}


+(NSArray *)getAll:(NSUInteger)idMedico
{
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *sql = [NSString stringWithFormat:
                     @"SELECT mtc_lotes.descripcion, "
                             "mtc_lotes.Stock, "
                             "mtc_lotes.lote, "
                             "case when Orden is null then "
                                 "0 "
                             "else "
                                 "cantidad "
                             "end cantidad, "
                             "mtc_lotes.codigo, "
                             "mtc_lotes.fecha_vto, "
                             "case when Orden is null then "
                                 "'S/O' "
                             "else "
                                 "Orden "
                             "end orden1, "
                     
                             "case when Orden is null then "
                                 "'No' "
                             "else "
                                 "case literatura when 1 then "
                                    "'Sí' "
                                 "else "
                                    "'No' "
                                 "end "
                             "end literatura, "
                     
                             "case when Orden is null then "
                                 "'No' "
                             "else "
                                 "case obsequio when 1 then "
                                    "'Sí' "
                                 "else "
                                    "'No' "
                                 "end "
                             "end obsequio, "
                     
                             "case when Orden is null then "
                                 "'' "
                             "else "
                                 "objetivos "
                             "end objetivos, "
                     
                             "case when Orden is null then "
                                 "'S/O' "
                             "else "
                                 "Orden "
                             "end orden2, "
                     
                             "mtc_lotes.id, "
                             "mtc_lotes.idlote, "
                     
                             "case when Orden is null then "
                                 "9999 "
                             "else "
                                 "Orden  "
                             "end orden3, "
                     
                             "case when Orden is null then "
                                "'No' "
                             "else "
                                "case promocionado when 1 then "
                                    "'Sí' "
                                "else "
                                    "'No' "
                                "end "
                             "end promocionado "
                     
                      "FROM mtc_lotes LEFT JOIN "
                            "med_apromocionar on (med_apromocionar.producto = mtc_lotes.id and "
                                                    "med_apromocionar.lote = mtc_lotes.idlote and   "
                                                    "med_apromocionar.nodo = %@ and  med_apromocionar.medico = %d) "
                      "WHERE mtc_lotes.stock > 0 and  mtc_lotes.nodo = %@  "
                      "ORDER BY orden3, descripcion, mtc_lotes.codigo, mtc_lotes.fecha_vto, mtc_lotes.lote ",
                            nodo, idMedico, nodo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    NSMutableArray *productos = [NSMutableArray array];
    
	if ( statement ) {
        
//        double      lastVencimiento = 0;
//        NSString    *lastCodigo = @"";
//        NSUInteger  lastStock = 0;
//        BOOL        lastVencido = NO;
        
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            ProductoTrazabilidad *p = [[ProductoTrazabilidad alloc] init];
            
            // descripcion
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if((_descripcion != nil) && (strlen(_descripcion) > 0)) {
                p.descripcion = [NSString stringWithUTF8String:_descripcion];
			}else{
				p.descripcion = @"";
			}
            
            
            // Stock
            p.stock = sqlite3_column_int(statement, 1);
            
            
            // Lote
            char *_lote = (char *)sqlite3_column_text(statement, 2);
            if((_lote != nil) && (strlen(_lote) > 0)) {
                p.lote = [NSString stringWithUTF8String:_lote];
			}else{
				p.lote = @"";
			}
            
            
            // Cantidad
            p.cantidad = sqlite3_column_int(statement, 3);
            
            
            // Codigo
            char *_codigo = (char *)sqlite3_column_text(statement, 4);
            if((_codigo != nil) && (strlen(_codigo) > 0)) {
                p.codigo = [NSString stringWithUTF8String:_codigo];
			}else{
				p.codigo = @"";
			}
            
            
            // Fecha Vto
            double val_vto = sqlite3_column_double(statement, 5);
            p.fechaVto = [NSDate dateWithTimeIntervalSince1970:val_vto];

            
            // Orden1
            char *_orden1 = (char *)sqlite3_column_text(statement, 6);
            if((_orden1 != nil) && (strlen(_orden1) > 0)) {
                p.orden1 = [NSString stringWithUTF8String:_orden1];
			}else{
				p.orden1 = @"";
			}
            
            
            // Literatura
            char *_lit = (char *)sqlite3_column_text(statement, 7);
            if((_lit != nil) && (strlen(_lit) > 0)) {
                p.literatura = ![[NSString stringWithUTF8String:_lit] isEqualToString:@"No"];
			}else{
				p.literatura = NO;
			}
            
            
            // Obsequio
            char *_obs = (char *)sqlite3_column_text(statement, 8);
            if((_obs != nil) && (strlen(_obs) > 0)) {
                p.obsequio = ![[NSString stringWithUTF8String:_obs] isEqualToString:@"No"];
			}else{
				p.obsequio = NO;
			}
            
            
            // objetivos
            char *_obj = (char *)sqlite3_column_text(statement, 9);
            if((_obj != nil) && (strlen(_obj) > 0)) {
                p.objetivos = [NSString stringWithUTF8String:_obj];
			}else{
				p.objetivos = @"";
			}
            
            
            // Orden2
            char *_orden2 = (char *)sqlite3_column_text(statement, 10);
            if((_orden2 != nil) && (strlen(_orden2) > 0)) {
                p.orden2 = [NSString stringWithUTF8String:_orden2];
			}else{
				p.orden2 = @"";
			}
            
            
            // Id
            p.prodId = sqlite3_column_int(statement, 11);
            
            
            // LoteId
            p.loteId = sqlite3_column_int(statement, 12);
            
            
            // Orden3
            p.orden3 = sqlite3_column_int(statement, 13);
            
            // Promocionado
            _obs = (char *)sqlite3_column_text(statement, 14);
            if((_obs != nil) && (strlen(_obs) > 0)) {
                p.promocionado = ![[NSString stringWithUTF8String:_obs] isEqualToString:@"No"];
			}else{
				p.promocionado = NO;
			}
            
            // Agrego el producto al array
            [productos addObject:p];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla mtc_lotes");
	}
	sqlite3_finalize(statement);

    // Controlo si alguno de los productos está vencido o debe ser bloqueado.
    [ProductoTrazabilidad checkProductoList:productos];
    
    return productos;
}

+(NSArray *)getForMedico:(NSUInteger)idMedico includePromociones:(NSArray*)promos
{
    BOOL tienePromociones = NO;
    NSString *incPromociones = @"";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    // Chequeo si hay un listado de promociones.
    if ((promos) && ([promos count] > 0))
    {
        incPromociones = @"OR (mtc_lotes.idlote IN (";
        
        // Agrego cada uno de los id de Lote de las promociones al string.
        for (Promocion *p in promos)
        {
            // Chequeo si es el primer producto que agrego al string.
            if (!tienePromociones)
            {
                tienePromociones = YES;
                incPromociones = [NSString stringWithFormat:@"%@%@ ",
                                        incPromociones,
                                        [NSString intToStr:p.lote]];
            } else
            {
                incPromociones = [NSString stringWithFormat:@"%@, %@ ",
                                  incPromociones,
                                  [NSString intToStr:p.lote]];
            }
        }
        
        incPromociones = [NSString stringWithFormat:@"%@)) ", incPromociones];
    }
    
    NSString *sql = [NSString stringWithFormat:
    @"SELECT mtc_lotes.descripcion,                                                                           "
    "                     case when mtc_lotes.Stock is null then                                              "
    "                        0 else mtc_lotes.Stock end Stock,                                                "
    "                     case when mtc_lotes.lote is null then                                               "
    "                        0 else mtc_lotes.lote end lote,                                                  "
    "                                                                                                         "
    "                     case when Orden is null then                                                        "
    "                        0 else cantidad                                                                  "
    "                     end cantidad,                                                                       "
    "                                                                                                         "
    "                     case when mtc_lotes.codigo is null then                                             "
    "                        0 else mtc_lotes.codigo end codigo,                                              "
    "                     case when mtc_lotes.fecha_vto is null then                                          "
    "                        0 else mtc_lotes.fecha_vto end fecha_vto,                                        "
    "                                                                                                         "
    "                     case when Orden is null then                                                        "
    "                        'S/O' else Orden                                                                 "
    "                     end orden1,                                                                         "
    "                                                                                                         "
    "                     case when Orden is null then                                                        "
    "                        'No' else                                                                        "
    "                        case literatura when 1 then                                                      "
    "                            'Sí' else 'No'                                                               "
    "                        end                                                                              "
    "                     end literatura,                                                                     "
    "                                                                                                         "
    "                     case when Orden is null then                                                        "
    "                        'No' else                                                                        "
    "                        case obsequio when 1 then                                                        "
    "                            'Sí' else 'No'                                                               "
    "                        end                                                                              "
    "                     end obsequio,                                                                       "
    "                                                                                                         "
    "                     case when Orden is null then                                                        "
    "                        '' else objetivos                                                                "
    "                     end objetivos,                                                                      "
    "                                                                                                         "
    "                     case when Orden is null then                                                        "
    "                        'S/O' else Orden                                                                 "
    "                     end orden2,                                                                         "
    "                                                                                                         "
    "                     case when mtc_lotes.id is null then                                                 "
    "                        0 else mtc_lotes.id end id,                                                      "
    "                     case when mtc_lotes.idlote is null then                                             "
    "                        0 else mtc_lotes.idlote end idlote,                                              "
    "                                                                                                         "
    "                     case when Orden is null then                                                        "
    "                        9999 else Orden                                                                  "
    "                     end orden3,mtc_lote_familia.nombre,mtc_lote_familia.id                                                                           "
    "                                                                                                         "
    "                     FROM mtc_lotes LEFT JOIN                                                            "
    "                     med_apromocionar on (med_apromocionar.producto = mtc_lotes.id and                   "
                     "med_apromocionar.nodo = %@ and  med_apromocionar.medico = %lu) "
                     "INNER JOIN mtc_lote_familia ON (mtc_lote_familia.id = mtc_lotes.familia) "
                     "WHERE (mtc_lotes.nodo = %@) %@"
                     "ORDER BY orden3, descripcion, mtc_lotes.codigo, mtc_lotes.fecha_vto, mtc_lotes.lote ",
                     nodo, (unsigned long)idMedico, nodo, incPromociones];
    
#ifdef DEBUG_TRAZABILIDAD
    NSLog(@" - ProductoTrazabilidad.getForMedico query: %@", sql);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    NSMutableArray *productos = [NSMutableArray array];
    
	if ( statement ) {
        
//        double      lastVencimiento = 0;
//        NSString    *lastCodigo = @"";
//        NSUInteger  lastStock = 0;
        
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            ProductoTrazabilidad *p = [[ProductoTrazabilidad alloc] init];
            
            // descripcion
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if((_descripcion != nil) && (strlen(_descripcion) > 0)) {
                p.descripcion = [NSString stringWithUTF8String:_descripcion];
			}else{
				p.descripcion = @"";
			}
            
            // Stock
            p.stock = sqlite3_column_int(statement, 1);
            
            
            // Lote
            char *_lote = (char *)sqlite3_column_text(statement, 2);
            if((_lote != nil) && (strlen(_lote) > 0)) {
                p.lote = [NSString stringWithUTF8String:_lote];
			}else{
				p.lote = @"";
			}
            
            
            // Cantidad
            p.cantidad = sqlite3_column_int(statement, 3);
            
            
            // Codigo
            char *_codigo = (char *)sqlite3_column_text(statement, 4);
            if((_codigo != nil) && (strlen(_codigo) > 0)) {
                p.codigo = [NSString stringWithUTF8String:_codigo];
			}else{
				p.codigo = @"";
			}
            
            
            // Fecha Vto
            double val_vto = sqlite3_column_double(statement, 5);
            p.fechaVto = [NSDate dateWithTimeIntervalSince1970:val_vto];
            
            
            // Orden1
//            char *_orden1 = (char *)sqlite3_column_text(statement, 6);
//            if((_orden1 != nil) && (strlen(_orden1) > 0)) {
//                p.orden1 = [NSString stringWithUTF8String:_orden1];
//			}else{
//				p.orden1 = @"";
//			}
            p.orden1 = @"";
            
            // Literatura
            char *_lit = (char *)sqlite3_column_text(statement, 7);
            if((_lit != nil) && (strlen(_lit) > 0)) {
                p.literatura = ![[NSString stringWithUTF8String:_lit] isEqualToString:@"No"];
			}else{
				p.literatura = NO;
			}
            
            
            // Obsequio
            char *_obs = (char *)sqlite3_column_text(statement, 8);
            if((_obs != nil) && (strlen(_obs) > 0)) {
                p.obsequio = ![[NSString stringWithUTF8String:_obs] isEqualToString:@"No"];
			}else{
				p.obsequio = NO;
			}
            
            
            // objetivos
            char *_obj = (char *)sqlite3_column_text(statement, 9);
            if((_obj != nil) && (strlen(_obj) > 0)) {
                p.objetivos = [NSString stringWithUTF8String:_obj];
			}else{
				p.objetivos = @"";
			}
            
            
            // Orden2
            char *_orden2 = (char *)sqlite3_column_text(statement, 10);
            if((_orden2 != nil) && (strlen(_orden2) > 0)) {
                p.orden2 = [NSString stringWithUTF8String:_orden2];
			}else{
				p.orden2 = @"";
			}
            
            
            // Id
            p.prodId = sqlite3_column_int(statement, 11);
            
            
            // LoteId
            p.loteId = sqlite3_column_int(statement, 12);
            
            
            // Orden3
            p.orden3 = sqlite3_column_int(statement, 13);
            
            
            // Promocionado
//            _obs = (char *)sqlite3_column_text(statement, 14);
//            if((_obs != nil) && (strlen(_obs) > 0)) {
//                p.promocionado = ![[NSString stringWithUTF8String:_obs] isEqualToString:@"No"];
//			}else{
				p.promocionado = NO;
//			}
            
       
            // Agrego el producto al array
            [productos addObject:p];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla mtc_lotes");
	}
	sqlite3_finalize(statement);

    
    [ProductoTrazabilidad checkProductoList:productos];
    
    return productos;
}

+(void)actualizarStock:(NSUInteger) producto lote:(NSUInteger)lote cantidad:(NSUInteger)cantidad
{
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    // Borro los registros realmente
    NSString *sql = @"UPDATE mtc_lotes SET stock = stock + %d WHERE id = %d and idlote = %d and nodo = %@";
    
    NSString *query = [NSString stringWithFormat:sql, cantidad, producto, lote, nodo];
    [[DB getInstance] excecuteSQL:query];
    
 
}


+(NSArray *)getForMedico:(NSUInteger)idMedico includePromociones:(NSArray*)promos andFamiliaId:(NSUInteger)familiaId
{
    BOOL tienePromociones = NO;
    NSString *incPromociones = @"";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    // Chequeo si hay un listado de promociones.
    /*
    if ((promos) && ([promos count] > 0))
    {
        incPromociones = @"OR (mtc_lotes.idlote IN (";
        
        // Agrego cada uno de los id de Lote de las promociones al string.
        for (Promocion *p in promos)
        {
            // Chequeo si es el primer producto que agrego al string.
            if (!tienePromociones)
            {
                tienePromociones = YES;
                incPromociones = [NSString stringWithFormat:@"%@%@ ",
                                  incPromociones,
                                  [NSString intToStr:p.lote]];
            } else
            {
                incPromociones = [NSString stringWithFormat:@"%@, %@ ",
                                  incPromociones,
                                  [NSString intToStr:p.lote]];
            }
        }
        
        incPromociones = [NSString stringWithFormat:@"%@)) ", incPromociones];
    }*/
    
    NSString *sql = [NSString stringWithFormat:
                     @"SELECT mtc_lotes.descripcion,                                                                           "
                     "                     case when mtc_lotes.Stock is null then                                              "
                     "                        0 else mtc_lotes.Stock end Stock,                                                "
                     "                     case when mtc_lotes.lote is null then                                               "
                     "                        0 else mtc_lotes.lote end lote,                                                  "
                     "                                                                                                         "
                     "                     case when Orden is null then                                                        "
                     "                        0 else cantidad                                                                  "
                     "                     end cantidad,                                                                       "
                     "                                                                                                         "
                     "                     case when mtc_lotes.codigo is null then                                             "
                     "                        0 else mtc_lotes.codigo end codigo,                                              "
                     "                     case when mtc_lotes.fecha_vto is null then                                          "
                     "                        0 else mtc_lotes.fecha_vto end fecha_vto,                                        "
                     "                                                                                                         "
                     "                     case when Orden is null then                                                        "
                     "                        'S/O' else Orden                                                                 "
                     "                     end orden1,                                                                         "
                     "                                                                                                         "
                     "                     case when Orden is null then                                                        "
                     "                        'No' else                                                                        "
                     "                        case literatura when 1 then                                                      "
                     "                            'Sí' else 'No'                                                               "
                     "                        end                                                                              "
                     "                     end literatura,                                                                     "
                     "                                                                                                         "
                     "                     case when Orden is null then                                                        "
                     "                        'No' else                                                                        "
                     "                        case obsequio when 1 then                                                        "
                     "                            'Sí' else 'No'                                                               "
                     "                        end                                                                              "
                     "                     end obsequio,                                                                       "
                     "                                                                                                         "
                     "                     case when Orden is null then                                                        "
                     "                        '' else objetivos                                                                "
                     "                     end objetivos,                                                                      "
                     "                                                                                                         "
                     "                     case when Orden is null then                                                        "
                     "                        'S/O' else Orden                                                                 "
                     "                     end orden2,                                                                         "
                     "                                                                                                         "
                     "                     case when mtc_lotes.id is null then                                                 "
                     "                        0 else mtc_lotes.id end id,                                                      "
                     "                     case when mtc_lotes.idlote is null then                                             "
                     "                        0 else mtc_lotes.idlote end idlote,                                              "
                     "                                                                                                         "
                     "                     case when Orden is null then                                                        "
                     "                        9999 else Orden                                                                  "
                     "                     end orden3,mtc_lote_familia.nombre,mtc_lote_familia.id                                                                           "
                     "                                                                                                         "
                     "                     FROM mtc_lotes LEFT JOIN                                                            "
                     "                     med_apromocionar on (med_apromocionar.producto = mtc_lotes.id and                   "
                     "med_apromocionar.nodo = %@ and  med_apromocionar.medico = %lu) "
                     "INNER JOIN mtc_lote_familia ON (mtc_lote_familia.id = mtc_lotes.familia) "
                     "WHERE (mtc_lotes.nodo = %@) %@ "
                     " AND mtc_lotes.familia = %lu"
                     " ORDER BY orden3, descripcion, mtc_lotes.codigo, mtc_lotes.fecha_vto, mtc_lotes.lote ",
                     nodo, (unsigned long)idMedico, nodo, incPromociones,(unsigned long)familiaId];
    
#ifdef DEBUG_TRAZABILIDAD
    NSLog(@" - ProductoTrazabilidad.getForMedico query: %@", sql);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    NSMutableArray *productos = [NSMutableArray array];
    
    if ( statement ) {
        
        //        double      lastVencimiento = 0;
        //        NSString    *lastCodigo = @"";
        //        NSUInteger  lastStock = 0;
        
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            ProductoTrazabilidad *p = [[ProductoTrazabilidad alloc] init];
            
            // descripcion
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if((_descripcion != nil) && (strlen(_descripcion) > 0)) {
                p.descripcion = [NSString stringWithUTF8String:_descripcion];
            }else{
                p.descripcion = @"";
            }
            
            // Stock
            p.stock = sqlite3_column_int(statement, 1);
            
            
            // Lote
            char *_lote = (char *)sqlite3_column_text(statement, 2);
            if((_lote != nil) && (strlen(_lote) > 0)) {
                p.lote = [NSString stringWithUTF8String:_lote];
            }else{
                p.lote = @"";
            }
            
            
            // Cantidad
            p.cantidad = sqlite3_column_int(statement, 3);
            
            
            // Codigo
            char *_codigo = (char *)sqlite3_column_text(statement, 4);
            if((_codigo != nil) && (strlen(_codigo) > 0)) {
                p.codigo = [NSString stringWithUTF8String:_codigo];
            }else{
                p.codigo = @"";
            }
            
            
            // Fecha Vto
            double val_vto = sqlite3_column_double(statement, 5);
            p.fechaVto = [NSDate dateWithTimeIntervalSince1970:val_vto];
            
            
            // Orden1
            //            char *_orden1 = (char *)sqlite3_column_text(statement, 6);
            //            if((_orden1 != nil) && (strlen(_orden1) > 0)) {
            //                p.orden1 = [NSString stringWithUTF8String:_orden1];
            //			}else{
            //				p.orden1 = @"";
            //			}
            p.orden1 = @"";
            
            // Literatura
            char *_lit = (char *)sqlite3_column_text(statement, 7);
            if((_lit != nil) && (strlen(_lit) > 0)) {
                p.literatura = ![[NSString stringWithUTF8String:_lit] isEqualToString:@"No"];
            }else{
                p.literatura = NO;
            }
            
            
            // Obsequio
            char *_obs = (char *)sqlite3_column_text(statement, 8);
            if((_obs != nil) && (strlen(_obs) > 0)) {
                p.obsequio = ![[NSString stringWithUTF8String:_obs] isEqualToString:@"No"];
            }else{
                p.obsequio = NO;
            }
            
            
            // objetivos
            char *_obj = (char *)sqlite3_column_text(statement, 9);
            if((_obj != nil) && (strlen(_obj) > 0)) {
                p.objetivos = [NSString stringWithUTF8String:_obj];
            }else{
                p.objetivos = @"";
            }
            
            // familia
            char *_fam = (char *)sqlite3_column_text(statement, 14);
            if((_fam != nil) && (strlen(_fam) > 0)) {
                p.familia = [NSString stringWithUTF8String:_fam];
            }else{
                p.familia = @"";
            }
            
            // Orden2
            char *_orden2 = (char *)sqlite3_column_text(statement, 10);
            if((_orden2 != nil) && (strlen(_orden2) > 0)) {
                p.orden2 = [NSString stringWithUTF8String:_orden2];
            }else{
                p.orden2 = @"";
            }
            
            
            // Id
            p.prodId = sqlite3_column_int(statement, 11);
            
            
            // LoteId
            p.loteId = sqlite3_column_int(statement, 12);
            
            
            // Orden3
            p.orden3 = sqlite3_column_int(statement, 13);
            
            
            // Promocionado
            //            _obs = (char *)sqlite3_column_text(statement, 14);
            //            if((_obs != nil) && (strlen(_obs) > 0)) {
            //                p.promocionado = ![[NSString stringWithUTF8String:_obs] isEqualToString:@"No"];
            //			}else{
            p.promocionado = NO;
            //			}
            
            
            // Agrego el producto al array
            [productos addObject:p];
        }
        
    } else {
        NSAssert(0, @"No se encontre la tabla mtc_lotes");
    }
    sqlite3_finalize(statement);
    
    
    [ProductoTrazabilidad checkProductoList:productos];
    
    return productos;
}


+(void)actualizarStock:(NSUInteger) producto lote:(NSUInteger)lote nuevoStock:(NSUInteger)newstock
{
    NSString *nodo = SettingForKey(NODO, @"");
    
    // Borro los registros realmente
    NSString *sql = @"UPDATE mtc_lotes SET stock = %d WHERE id = %d and idlote = %d and nodo = %@";
    
    NSString *query = [NSString stringWithFormat:sql, newstock, producto, lote, nodo];
    
    #ifdef DEBUG_TRAZABILIDAD
    NSLog(@"- Query: %@", query);
    #endif
    
    [[DB getInstance] excecuteSQL:query];
}


+(NSArray*)getFamiliasFromMedico:(NSUInteger)idMedico{
    
        NSString *nodo = SettingForKey(NODO, @"");
    
    NSString* sql = //@"SELECT * FROM mtc_lote_familia";
    [NSString stringWithFormat:@"SELECT DISTINCT mtc_lote_familia.id,mtc_lote_familia.nombre                                                                                                                                                                                                                                           FROM mtc_lotes LEFT JOIN                                                                                 med_apromocionar ON (med_apromocionar.producto = mtc_lotes.id AND                   med_apromocionar.nodo = %@ AND  med_apromocionar.medico = %lu) INNER JOIN mtc_lote_familia ON (mtc_lote_familia.id = mtc_lotes.familia) WHERE (mtc_lotes.nodo = %@) ORDER BY mtc_lote_familia.nombre ASC",nodo,(unsigned long)idMedico,nodo];
    
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    NSMutableArray *familias = [NSMutableArray array];
    
    if ( statement ) {
        
        //        double      lastVencimiento = 0;
        //        NSString    *lastCodigo = @"";
        //        NSUInteger  lastStock = 0;
        
       
        while (sqlite3_step(statement) == SQLITE_ROW) {
        
            NSMutableDictionary* d = [NSMutableDictionary dictionary];
            char *_nombre = (char *)sqlite3_column_text(statement, 0);
            if((_nombre != nil) && (strlen(_nombre) > 0)) {
               [d setObject:[NSString stringWithUTF8String:_nombre] forKey:@"id"];
            }else{
                [d setObject:@"" forKey:@"id"];
            }
            
            char *_id = (char *)sqlite3_column_text(statement, 1);
            if((_id != nil) && (strlen(_id) > 0)) {
                [d setObject:[NSString stringWithUTF8String:_id] forKey:@"nombre"];
            }else{
                [d setObject:@"" forKey:@"nombre"];
            }
            [familias addObject:d];
        }
        
        return familias;
    }else return NULL;
}

+(int)getFamiliaIdfromProductoId:(int)productoId{
    
    NSString* sql = //@"SELECT * FROM mtc_lote_familia";
    [NSString stringWithFormat:@"SELECT familia                                                                                                                                                                                                                                        FROM mtc_lotes WHERE id = %d",productoId];
    
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    NSMutableArray *familias = [NSMutableArray array];
    
    if ( statement ) {
        
        //        double      lastVencimiento = 0;
        //        NSString    *lastCodigo = @"";
        //        NSUInteger  lastStock = 0;
        
        int famId = -1;
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            
            char *_id = (char *)sqlite3_column_text(statement, 0);
            if((_id != nil) && (strlen(_id) > 0)) {
               famId = [[NSString stringWithUTF8String:_id] intValue];
            }else{
                famId = -1;
            }
        }
        
        return famId;
        
    }else return -1;
        
}
#pragma mark -
#pragma mark Memory Management

@end
