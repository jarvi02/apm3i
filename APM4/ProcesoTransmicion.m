//
//  ProcesoTransmicion.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ProcesoTransmicion.h"
#import "Config.h"
#import "DB.h"
#import "ServiceVerificarExisteNovedad.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "ServiceExisteNovedad.h"
#import "ServiceVerificarErrorTransmisionPrevia.h"
#import "Ciclo.h"
#import "ServiceEnviarNovedad.h"
#import "ServiceLongitudArchivoNovedad.h"
#import "ServiceDownloadData.h"
#import "ProcesarArchivo.h"
#import "ConfigGlobal.h"

#import "GlobalDefines.h"


@interface ProcesoTransmicion()
@property(nonatomic, retain) NSString *urlBase;
-(void) configurarUrlBase;
-(BOOL) recuperarErrorPrevio;
-(BOOL) recuperarNovedad;
-(BOOL) generarNovedad;
-(BOOL) enviarNovedad:(NSData*)novedad;
-(BOOL) obtenerNovedad;
-(NSData*) generarHeader:(ACCIONTX) accion;
-(NSData*) generarCuerpo:(ACCIONTX) accion;
-(NSData*) generarPie:(ACCIONTX) accion;
-(void) alerta:(NSString*) mensaje;
-(void) status:(NSString*) mensaje;
@end


@implementation ProcesoTransmicion


-(void)start {
    
    [self status:@"Iniciando Proceso Transmision"];
    
    [self configurarUrlBase];
    
    if ([SettingForKey(TRANID_OK, @"0") integerValue] == 0)
    {
        if (![self recuperarErrorPrevio])
        {
            return;
            // TODO: Agregar a log_excepciones
        }
    }
    
    if (![self generarNovedad])
        return;
    
    ServiceVerificarExisteNovedad *sven = [[ServiceVerificarExisteNovedad alloc] init];
    BOOL existeNovedad = [sven verificar:self.urlBase];
    
    if (!existeNovedad)
    {
        NSString *str = [NSString stringWithFormat:@"TX0008 (sec. %@) Nunca se genero la transacción anterior en el servidor.", SettingForKey(TRANID, @"")];
        [self performSelectorOnMainThread:@selector(alerta:) withObject:str waitUntilDone:YES];
        return;
    }
    
    [self obtenerNovedad];
    
}

-(void) configurarUrlBase {
    
    if (self.serverAvailable == 1)
    {
        self.urlBase =  [NSString stringWithFormat:@"http://%@:%@", SettingForKey(REMOTE_HOST, @""), SettingForKey(REMOTE_PORT, @"") ];
    }
    else if (self.serverAvailable == 2)
    {
        self.urlBase =  [NSString stringWithFormat:@"http://%@:%@", SettingForKey(REMOTE_HOST_2, @""), SettingForKey(REMOTE_PORT_2, @"") ];
    } else
    {
        return;
    }

}

#pragma - mark
#pragma Recuperar Error Previo

-(BOOL) recuperarErrorPrevio
{
    [self status:@"Recuperando error previo"];
    
    ServiceVerificarExisteNovedad *sven = [[ServiceVerificarExisteNovedad alloc] init];
    NSInteger existeNovedad = [sven verificar:self.urlBase];
    
#if DEBUG_TRANSMICION
    NSLog(@" - recuperarErrorPrevio: %@", existeNovedad? @"Existe Novedad": @"No Existe novedad");
#endif
    
    if (existeNovedad == 0)
    {
        [[DB getInstance] excecuteSQL:@"UPDATE log_novedades SET tranid = 0, transmitido = 0;"];
        [[Config shareInstance] update:TRANID_OK value:@"1"];

#ifdef DEBUG_TRANSMICION
        NSLog(@" - recuperarErrorPrevio: log_novedades marcada como no transmitido");
#endif

        return YES;
    } else
        
    if (existeNovedad == 1)
    {
        if ( [self recuperarNovedad] )
        {
            
        // TODO: Agregar a log_excepciones
            NSString *str = [NSString stringWithFormat:@"TX0003 (sec. %@) Había un error en la transacción previa, y este persiste.", SettingForKey(TRANID, @"")];
            [self performSelectorOnMainThread:@selector(alerta:) withObject:str waitUntilDone:YES];
            
            return NO;
            
        } else
        {
            if ([SettingForKey(CERRAR_CICLO, @"0") isEqualToString:@"1"])
            {
              
                NSString *proximo = [NSString stringWithFormat:@"%d", [Ciclo actual].proximo];
                [[Config shareInstance] update:CICLO value:proximo];
                [[Config shareInstance] update:CERRAR_CICLO value:@"0"];
                [[Config shareInstance] update:INICIALIZAR  value:@"1"];
            
            }
            return YES;
        }
        
    }
    return NO;
}



-(BOOL) recuperarNovedad {
    
    BOOL existeNovedad = NO;
    
    ServiceExisteNovedad *sen = [[ServiceExisteNovedad alloc] init];
    existeNovedad = [sen existeNovedad:self.urlBase];
    
#ifdef DEBUG_TRANSMICION
    NSLog(@" - recuperarNovedad: %@", existeNovedad? @"Existe Novedad": @"No Existe novedad");
#endif
    
    if (!existeNovedad)
    {
        NSInteger reintentos = [SettingForKey(REINTENTOS, @"0") integerValue];
        
#ifdef DEBUG_TRANSMICION
        NSLog(@" - recuperarNovedad: reintentos %d", reintentos);
#endif
        
        while (reintentos > 0)
        {
            reintentos--;
            
#ifdef DEBUG_TRANSMICION
            NSLog(@" - recuperarNovedad: reintentando .... (quedan %d reintentos)", reintentos);
#endif
            
            ServiceExisteNovedad *sen = [[ServiceExisteNovedad alloc] init];
            existeNovedad = [sen existeNovedad:self.urlBase];
            
            if (!existeNovedad)
            {
                
                ServiceVerificarErrorTransmisionPrevia *svetp = [[ServiceVerificarErrorTransmisionPrevia alloc] init];
                BOOL error = [svetp verificar:self.urlBase];
                
                existeNovedad = NO;
                
                if (error)
                {
                    reintentos = 0;
#ifdef DEBUG_TRANSMICION
                    NSLog(@" - recuperarNovedad: Error en transmición previa");
#endif
                }
                else
                {
                    [NSThread sleepForTimeInterval:10];
                }
            } else
            {
                reintentos = 0;
                existeNovedad = YES;
            }
            
        }
    }
    
#ifdef DEBUG_TRANSMICION
    NSLog(@" - recuperarNovedad: %@", existeNovedad? @"Se recuperó el error": @"El error persiste");
#endif
    
    return existeNovedad;
}


-(BOOL) generarNovedad
{
    BOOL result = YES;
    
    [self status:@"Generando Novedad para enviar al servidor"];
    
    NSInteger tranid = [SettingForKey(TRANID, @"0") integerValue]+1;
    NSString *strnaid = [NSString stringWithFormat:@"%d", tranid];
    
    [[Config shareInstance] update:TRANID value:strnaid];
    [[Config shareInstance] update:TRANID_OK value:@"0"];
    
       
    ACCIONTX accion;
    if ([SettingForKey(CERRAR_CICLO, @"0") isEqualToString:@"1"]) {
        accion = CIERRE_CICLO;
    } else if ([SettingForKey(INICIALIZAR, @"0") isEqualToString:@"1"]) {
        accion = INICIALIZACION;
    } else {
        accion = NOVEDADES;
    }
    
    NSMutableData *data = [[NSMutableData alloc] initWithCapacity:0];
    
    [data appendData:[self generarHeader:accion]];
    [data appendData:[self generarCuerpo:accion]];
    [data appendData:[self generarPie:accion]];
    
    NSString *sql = [NSString stringWithFormat:@"update log_novedades set tranid = %d, transmitido = 1  where tranid = 0 and transmitido = 0;", tranid];
    [[DB getInstance] excecuteSQL:sql];

#ifdef DEBUG_TRANSMICION
    NSLog(@" - generarNovedad - Query: %@", sql);
#endif
    
    result = [self enviarNovedad:data];
    if (!result)
    {
        NSString *str = [NSString stringWithFormat:@"TX0007 (sec. %@) En este momento no es posible enviar las novedades. Intentelo nuevamente en unos minutos.", SettingForKey(TRANID, @"")];
        [self performSelectorOnMainThread:@selector(alerta:) withObject:str waitUntilDone:YES];
    }
    
    
    return result;
}


-(NSData*) generarHeader:(ACCIONTX) accion {
    
    
    NSMutableData *data = [[NSMutableData alloc] initWithCapacity:0];
    @autoreleasepool {
        
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];
        
        NSString *target = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleInfoDictionaryVersionKey];
        
        NSString *device = [[UIDevice currentDevice] systemVersion];

        NSString *str = @"apm3i.i\r\n<HEADER>\r\nENTIDAD=%@\r\nCICLO=%@\r\nEXEVERSION=%@\r\nIPADTARGET=%@\r\nIPADVERSION=%@\r\nAPM3IVERSIONNAME=%@\r\nTRANSACTIONID=%@\r\n<FIN>\r\n<REQUERIMIENTOS>\r\n";
        
        
        str = [NSString stringWithFormat:str,SettingForKey(ENTIDAD, @""),SettingForKey(CICLO, @""),version,target,device,version,SettingForKey(TRANID, @"")];
        [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        if (accion == INICIALIZACION) {
            
            str = @"cfg_ultimo\r\nINICIALIZACION\r\ncfg_global\r\nmed_rechazados\r\nstd_visitasexternas\r\nstd_movimientofichero\r\n";
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
        } else if (accion == CIERRE_CICLO) {
            str = @"cfg_ultimo\r\nCERRARCICLO\r\ncfg_global\r\nmail_recibidos\r\nmed_rechazados\r\nstd_visitasexternas\r\nstd_movimientofichero\r\nmail_secuencia\r\n";
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
        } else if (accion == NOVEDADES) {
            
            str = @"cfg_ultimo\r\ncfg_global\r\nmed_rechazados\r\nstd_visitasexternas\r\nstd_movimientofichero\r\n";
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
            // Si usa Lotes debo enviar los requerimientos necesarios
            BOOL usaLotes = [[ConfigGlobal shareInstance] find:USALOTES];
            if (usaLotes == YES)
            {
                str=@"mtc_productos\r\nmtc_lotes\r\n";
                [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // si usa autorizacion o medicos inactivos debo enviar los requerimientos necesarios
            BOOL usaAutorizacion = [[ConfigGlobal shareInstance] find:USAAUTORIZACION];
            BOOL usaMedInactivo = [[ConfigGlobal shareInstance] find:USAMEDINACTIVO];
            if ((usaAutorizacion == YES) || (usaMedInactivo == YES))
            {
                str=@"med_medicos\r\nmed_especialidades\r\nmed_telefonos\r\nmed_domicilios\r\nmed_horarios\r\nmed_academias\r\nmed_obrassociales\r\n";
                [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // si no usa visita de fin de semana debo enviar los requerimientos necesarios
            BOOL usaVisFinSemana = [[ConfigGlobal shareInstance] find:VIS_FINDESEMANA];
            if (usaVisFinSemana == NO)
            {
                str=@"mtc_feriados\r\n";
                [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // si usa historico de visitas debo enviar los requerimientos necesarios
            BOOL usaHistorico = [[ConfigGlobal shareInstance] find:HISTORICOVISITA];
            if (usaHistorico == YES)
            {
                str=@"std_historicovisitas\r\n";
                [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // si usa orden de promocion debo enviar los requerimientos necesarios
            BOOL usaOrdenProm = [[ConfigGlobal shareInstance] find:ORDENPROMOCION];
            if (usaOrdenProm == YES)
            {
                str=@"std_ordenpromocion\r\n";
                [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // si usa frecuencia de visita debo enviar los requerimientos necesarios
            BOOL usaFrecuencia = [[ConfigGlobal shareInstance] find:FRECUENCIAVISITAS];
            if (usaFrecuencia == YES)
            {
                str=@"std_frecuenciavisitas\r\n";
                [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // si usa log de actividad debo enviar los requerimientos necesarios
            BOOL usaLogActividad = [[ConfigGlobal shareInstance] find:USALOGACTIVIDAD];
            if (usaLogActividad == YES)
            {
                str=@"fid_actividades\r\nfid_registros\r\n";
                [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
        } else {
            str = @"\r\n";
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        
        
        str = @"<FIN>\r\n";
        [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    return data;
    
}
-(NSData*) generarCuerpo:(ACCIONTX) accion {
    
    NSMutableData *data = [[NSMutableData alloc] initWithCapacity:0];
    @autoreleasepool {
        NSString *str;
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:10];
        
        NSString *QUERY_NOVEDADES = @"select nov.tabla, nov.secuencia || \"|\" || nov.tipo || \"|\" || nov.registro as registro, nov.tranid, nov.transmitido from log_novedades nov inner join cfg_tablastransmision cfg on nov.tabla = cfg.tabla where nov.transmitido = 0 order by cfg.orden, nov.secuencia";
        
        sqlite3_stmt *statement = [[DB getInstance] prepare:QUERY_NOVEDADES];
        if ( statement ) {
            
            char *_c;
            NSString *tabla;
            NSString *registro;
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                _c = (char *)sqlite3_column_text(statement, 0);
                tabla = [NSString stringWithUTF8String:_c];
                
                _c = (char *)sqlite3_column_text(statement, 1);
                registro = [NSString stringWithUTF8String:_c];
                
                
                if ( [dictionary valueForKey:tabla]!=nil ) {
                    
                    // ya existe la entrada pido el array
                    NSMutableArray *arr = [dictionary valueForKey:tabla];
                    
                    //agego registro
                    [arr addObject:registro];
                    
                } else {
                    
                    // creo array con registro
                    NSMutableArray *arr = [[NSMutableArray alloc] init];
                    [arr addObject:registro];
                    
                    // agrego entrada con array
                    [dictionary setValue:arr forKey:tabla];
                }
                
                
            }
            
        } else {
            NSAssert(0, @"No se encontre la tabla");
        }
        sqlite3_finalize(statement);
        
        
        NSArray *allKeys = [dictionary allKeys];
        
        for (NSString *tabla in allKeys) {
            
            str = @"\r\n\r\n<DATASET>\r\n";
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
            str = [NSString stringWithFormat:@"NOMBRE=%@\r\n",tabla];
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSArray *arr = [dictionary valueForKey:tabla];
            
            int cnn = 0;
            for (NSString *registro in arr) {
                cnn = cnn + [registro length] + 1;  //el +1 se lo pongo por el delimitadorque se agregar adelante  ~
            }
            str = [NSString stringWithFormat:@"BLOCKSIZE=%d\r\n",cnn];
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
            for (NSString *registro in arr) {
                
                str = [NSString stringWithFormat:@"%@~",registro];
                [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
                
            }
        }
        
        [data appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
    }
    
    return data;
    
}
-(NSData*) generarPie:(ACCIONTX) accion {
    
    NSMutableData *data = [[NSMutableData alloc] initWithCapacity:0];
    @autoreleasepool {
        
        NSString *str = @"\r\n=%@\r\nCMD=RTX\r\nNAME=%@\r\nSIZE=999\r\n";
        str = [NSString stringWithFormat:str,SettingForKey(ENTIDAD, @""),SettingForKey(TRANID, @"") ];
        [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        
#ifdef DEBUG_TRANSMICION
        NSLog(@" - generarPie: %@", str);
#endif
        
    }
    
    return data;
    
}

-(BOOL) enviarNovedad:(NSData*)novedad {
    
    [self status:@"Enviando novedad al servidor"];
    BOOL result = YES;
    
    @autoreleasepool {
        
        // generar archivo txt - apm3i.i
        NSString *cachesFolder = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
        NSString *file = [cachesFolder stringByAppendingPathComponent:@"apm3i.i"];
        
        BOOL fileSaved = [novedad writeToFile:file options:NSDataWritingAtomic error:nil];
        
        ServiceEnviarNovedad *sen = [[ServiceEnviarNovedad alloc] init];
        BOOL novedadEnvidad = [sen enviar:self.urlBase fileName:file tamanio:[novedad length]];
        
        [[NSFileManager defaultManager] removeItemAtPath:file error:nil];
        
        
        result = (novedadEnvidad) && (fileSaved);
//        if ((!novedadEnvidad) || (!fileSaved))
//            result = NO;
    }
    
    return result;
}


-(BOOL) obtenerNovedad{
    
    [self status:@"Obteniendo la novedad del servidor"];
    BOOL result = YES;
    
    if (![self recuperarNovedad])
    {
        NSString *str = [NSString stringWithFormat:@"TX0010 (sec. %@) Pasaron los reintentos pero el paquete no se genero", SettingForKey(TRANID, @"")];
        [self performSelectorOnMainThread:@selector(alerta:) withObject:str waitUntilDone:YES];
        result = NO;
    }
    else
    {
        ServiceLongitudArchivoNovedad *slan = [[ServiceLongitudArchivoNovedad alloc] init];
        NSInteger longitud = [slan longitud:self.urlBase];

        
        //NSString *msg = [NSString stringWithFormat:@"Descargando Archivo novedades: tamaño %d bytes",longitud];
        NSString *msg = [NSString stringWithFormat:@"Descargando archivo de novedades"];
        [self status:msg];
        
        ServiceDownloadData *sdd = [[ServiceDownloadData alloc] init];
        sdd.progressView = self.progressView;
        
        UIProgressView* tempView = self.progressView;
        NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
        [mainQueue addOperationWithBlock:
         ^{
             if (DEBUG_QUEUE) NSLog(@"Queue Info: ProcesoTransmicion obtenerNoverdad - Update progressBar visibility");
             if (tempView)
             {
                 [tempView setProgress:0.0];
                 [tempView setHidden:NO];
             }
         }];

        NSString *filePath = [sdd donwload:self.urlBase];


#ifdef DEBUG_TRANSMICION
        NSLog(@" - obtenerNovedad filePath: %@", filePath);
#endif
        
        [self.progressView setHidden:YES];
        
        NSData *data = [[NSData alloc] initWithContentsOfFile:filePath options:NSDataReadingUncached error:nil];
        NSInteger longitudFileDescargado = [data length];
        
        // TODO: Mostrar error cuando la longitud del archivo no es la esperada.
        
        if (longitudFileDescargado == longitud)
        {
            [self status:@"Procesando Archivo"];
            
            ProcesarArchivo *pro = [[ProcesarArchivo alloc] init];
            pro.lbStatusTransmisionViewController = self.lbStatusTransmisionViewController;
            pro.progressView = self.progressView;
            
            UIProgressView* tempView = self.progressView;
            NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
            [mainQueue addOperationWithBlock:
             ^{
                 if (DEBUG_QUEUE) NSLog(@"Queue Info: ProcesoTransmicion obtenerNoverdad - Update progressBar visibility");
                 if (tempView)
                 {
                     [tempView setProgress:0.0];
                     [tempView setHidden:NO];
                 }
             }];
            
            if ([pro start:filePath])
            {
                UIProgressView* tempView = self.progressView;
                NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
                [mainQueue addOperationWithBlock:
                 ^{
                     if (DEBUG_QUEUE) NSLog(@"Queue Info: ProcesoTransmicion obtenerNoverdad - Update progressBar progress");
                     if (tempView)
                         [tempView setProgress:1.0];
                 }];
                
                [[DB getInstance] excecuteSQL:@"delete from log_novedades;"];
                
                if ([SettingForKey(CERRAR_CICLO, @"0") isEqualToString:@"1"])
                {
                    NSString *proximo = [NSString stringWithFormat:@"%d", [Ciclo actual].proximo];
                    [[Config shareInstance] update:CICLO value:proximo];
                }
                
                [[Config shareInstance] update:CERRAR_CICLO value:@"0"];
                [[Config shareInstance] update:INICIALIZAR  value:@"0"];
                [[Config shareInstance] update:TRANID_OK  value:@"1"];
            }
            
        } else
        {
            NSString *str = [NSString stringWithFormat:@"TX0009 (sec. %@) En este momento no es posible obtener las novedades. Intentelo nuevamente en unos minutos.", SettingForKey(TRANID, @"")];
            [self performSelectorOnMainThread:@selector(alerta:) withObject:str waitUntilDone:YES];
            result = NO;
        }
        
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    
    UIProgressView* tempView = self.progressView;
    NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
    [mainQueue addOperationWithBlock:
     ^{
         if (DEBUG_QUEUE) NSLog(@"Queue Info: ProcesoTransmicion obtenerNoverdad - Update progressBar visibility");
         if (tempView)
         {
             [tempView setProgress:0.0];
             [tempView setHidden:YES];
         }
     }];
    
    return result;
}

-(void) alerta:(NSString*) mensaje
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:mensaje delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
    [alert show];

}

-(void) status:(NSString*) mensaje
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.lbStatusTransmisionViewController.text = mensaje;
    });
    
    
}

@end
