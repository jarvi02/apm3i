//
//  Provincia.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Provincia : NSObject

@property(nonatomic, assign) NSInteger idProvincia;
@property(nonatomic, retain) NSString *descripcion;

- (NSString*)getDescription;

+(NSArray*) GetAll;

@end
