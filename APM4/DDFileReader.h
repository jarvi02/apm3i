//
//  DDFileReader.h
//  borrar
//
//  Created by Fabian E. Pezet Vila on 24/03/13.
//  Copyright (c) 2013 Fabian E. Pezet Vila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDFileReader : NSObject {
    //NSString * filePath;
    
    NSFileHandle * fileHandle;
    
    
    NSString * lineDelimiter;
    NSUInteger chunkSize;
}

@property (nonatomic, retain) NSString * filePath;
@property (nonatomic) unsigned long long currentOffset;
@property (nonatomic) unsigned long long totalFileLength;

@property (nonatomic, copy) NSString * lineDelimiter;
@property (nonatomic) NSUInteger chunkSize;

- (id) initWithFilePath:(NSString *)aPath;

- (NSString *) readLine;
- (NSString *) readTrimmedLine;

#if NS_BLOCKS_AVAILABLE
- (void) enumerateLinesUsingBlock:(void(^)(NSString*, BOOL *))block;
#endif

@end