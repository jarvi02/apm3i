//
//  TotalesDiariosViewController.m
//  APM4
//
//  Created by Laura Busnahe on 7/29/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "TotalesDiariosViewController.h"
#import "MTstdTotalesDiarios.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface TotalesDiariosViewController ()

@end

@implementation TotalesDiariosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.cellNib    = [UINib nibWithNibName:@"customTotalDiarioCell" bundle:nil];
        [self.cellNib instantiateWithOwner:self options:nil];
        
        self.data = nil;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Do all the load shit here
    
    [self updateData];
    
    // ------
    
    [self.loadingView hide];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Custom view methods

- (id)initViewDefaultNib
{
    self = [self initWithNibName:@"TotalesDiariosViewController" bundle:nil];
    
    self.title = @"Totales de informe diario";
    
    return self;
}

-(BOOL)hasFooter
{
    return YES;
}

-(NSString *)getTitleForHeader{
    return self.title;
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"cabecera_principal_32"];
}


#pragma mark -
#pragma mark tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.data count] > 0)
        return [self.data count] -1;
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"coberturaCelldentifier";
    
    // Obtain the cell object.
    customTotalDiarioCell *cell = (customTotalDiarioCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        self.cellNib   = [UINib nibWithNibName:@"customTotalDiarioCell" bundle:nil];
        [self.cellNib instantiateWithOwner:self options:nil];
    	cell = self.tableCell;
    	//self.tableCell = nil;
    }
    
    // Obtengo los datos para la celada.
    MTstdTotalesDiarios *o = [self.data objectAtIndex:indexPath.row];
    
    // Celdas de Orden
    cell.lblDescripciones.text = [NSString string:o.descripcion ifNull:@""];
    cell.lblCantM.text =    [NSString intToStr:o.cantidadManiana ifZero:@"-"];
    cell.lblCantT.text =    [NSString intToStr:o.cantidadTarde ifZero:@"-"];
    cell.lblMedM.text =     [NSString intToStr:o.medicosManiana ifZero:@"-"];
    cell.lblMedT.text =     [NSString intToStr:o.medicosTarde ifZero:@"-"];
    cell.lblCantidad.text = [NSString intToStr:o.cantidad ifZero:@"-"];
    cell.lblMedicos.text =  [NSString intToStr:o.medicos ifZero:@"-"];
    
    // Sombreo el total (la última fila de la tabla)
    if (indexPath.row == ([self.data count] -1 ))
        cell.lblBackground.backgroundColor = UIColorFromRGB(0xC8DBFF);
    else
        cell.lblBackground.backgroundColor = UIColorFromRGB(0xFFFFFF);
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader.frame.size.height;
        
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
    {
        return self.tableHeader;
    }
    else
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.tableFooter)
    {
        return self.tableFooter.frame.size.height;
    }
    
    return 0;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (self.tableFooter)
    {
        MTstdTotalesDiarios *o = nil;
        
        if ([self.data count] > 0)
            o = [self.data objectAtIndex:[self.data count]-1];
        
        [self.tableFooter.lblCantM setTitle:[NSString intToStr:o.cantidadManiana ifZero:@"-"]
                                   forState:UIControlStateNormal];
        
        [self.tableFooter.lblCantT setTitle:[NSString intToStr:o.cantidadTarde ifZero:@"-"]
                                   forState:UIControlStateNormal];
        
        [self.tableFooter.lblMedM setTitle:[NSString intToStr:o.medicosManiana ifZero:@"-"]
                                  forState:UIControlStateNormal];
        
        [self.tableFooter.lblMedT setTitle:[NSString intToStr:o.medicosTarde ifZero:@"-"]
                                  forState:UIControlStateNormal];
        
        [self.tableFooter.lblCantidad setTitle:[NSString intToStr:o.cantidad ifZero:@"-"]
                                      forState:UIControlStateNormal];
        
        [self.tableFooter.lblTotal setTitle:[NSString intToStr:o.medicos ifZero:@"-"]
                                   forState:UIControlStateNormal];

        return self.tableFooter;
    }
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark -
#pragma mark Actualización de datos

- (void)updateData
{
    // Leo nuevamente los datos desde la base de datos.
    self.data = [MTstdTotalesDiarios getAll];
   
    // Actalizo los datos de la tabla.
    [self.tableView reloadData];
    [self.loadingView hide];
}

@end
