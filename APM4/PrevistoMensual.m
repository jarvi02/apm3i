//
//  PrevistoMensual.m
//  APM4
//
//  Created by Ezequiel Manacorda on 08/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "PrevistoMensual.h"
#import "MTPrevistoActividad.h"
#import "GlobalDefines.h"

#import "NSDateExtended.h"

#import "MTEntidad.h"
#import "MTMedico.h"
#import "MTInstitucion.h"

#import "Config.h"
#import "DB.h"
#import "Novedad.h"


// DB table column position into the arguments array.
//(nodo, fecha, he, hs, tipo, descripcion, id_relacion)
#define valueNODO     0
#define valueFECHA    1
#define valueHE       2
#define valueHS       3
#define valueTIPO     4
#define valueDESC     5
#define valueIDREL    6
#define valueFECHAMS  7


#pragma mark -
#pragma mark Previsto Mensual implementation
@implementation PrevistoMensual


- (NSArray*)dbGetArguments:(MTPrevistoActividad*)actividad
{ // Esta función obtiene y devuelve los parámetros necesarios para el query de Add, Update y Delete
  // con el formato para realizarlo en la base de datos local.
    
    NSMutableArray* result = [[NSMutableArray alloc] init];
    
    // Nodo (Integer)
    NSString *idNodo = [[Config shareInstance] find:NODO];
    if (!idNodo)
        idNodo = @"0";
    [result addObject:idNodo];
    
    // Fecha (Date)
    NSString *sDate = @"";
    if (self.date)
    {
        sDate = [[self.date dateAsDateWithoutTime] datetimeAsSQLiteString];
        //sDate = [[self.date dateAsDateWithoutTime] datetimeForDBAsString];

    }
    [result addObject:sDate];
    
    // HE (VarChar)
    if (actividad.entrada)
    {
        sDate = [actividad.entrada timeToString];
    } else
        sDate = @"";
    [result addObject:sDate];
    
    // HS (VarChar)
    if (actividad.salida)
    {
        sDate = [actividad.salida timeToString];
    } else
        sDate = @"";
    [result addObject:sDate];
    
    // Tipo (Integer)
    [result addObject:[NSString stringWithFormat:@"%i",[actividad getDbEntityType]]];
    
    // Descripcion (VarChar)
    NSString *descripcion = @"";
    if (actividad.entidad.descripcion)
        descripcion = [NSString stringWithFormat:@"%@", actividad.entidad.descripcion];
    [result addObject:descripcion];
    
    // ID_Relacion (Integer)
    NSString *idRelacion = actividad.entidad.recID;
    if (!idRelacion)
        idRelacion = @"0";
    [result addObject:idRelacion];
    
    // FechaMS (Date)
    sDate = @"";
    if (self.date)
    {
        //sDate = [[self.date dateAsDateWithoutTime] datetimeAsSQLiteString];
        sDate = [[self.date dateAsDateWithoutTime] datetimeForDBAsString];
    }
    [result addObject:sDate];

    
    return result;
}

- (NSString *)getNovedadParameters:(MTPrevistoActividad*)activity
{ // Esta función obtiene y devuelve los parámetros necesarios para el query de LOG_NOVEDADES
  // con el formato para realizarlo en dicha tabla.

    NSString *result = @"";
    NSArray *arguments = nil;
    NSString *sDate = nil;
    
    arguments = [self dbGetArguments:activity];
    sDate = [[self.date dateAsDateWithoutTime] datetimeForNovedad];
    
    
    result = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@",
              [arguments objectAtIndex:valueNODO],
              sDate,
              [arguments objectAtIndex:valueHE],
              [arguments objectAtIndex:valueHS],
              [arguments objectAtIndex:valueTIPO],
              [arguments objectAtIndex:valueDESC],
              [[arguments objectAtIndex:valueIDREL] isEqual:@"0"] ? @"" : [arguments objectAtIndex:valueIDREL]
              ];
   
    return result;
}


#pragma mark -
#pragma mark  General Purpose methods

-(NSString *)description
{
    NSDateFormatter *frm = [[NSDateFormatter alloc] init];
    [frm setDateFormat:@"EEE MMM dd HH:mm:ss ZZZ yyyy"];
    
    NSLocale *es_AR = [[NSLocale alloc] initWithLocaleIdentifier:@"es_AR"];
    [frm setLocale:es_AR];
    
    [frm setDateStyle:NSDateFormatterFullStyle];
    
    return [NSString stringWithFormat:@"%@: %@",
            self.type == kMorning ? @"Mañana" : @"Tarde", [[frm stringFromDate:self.date] stringByReplacingOccurrencesOfString:@"," withString:@""]];

}


#pragma mark -
#pragma mark Memory Management

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.activity    = nil;
        self.oldActivity = nil;
        self.action      = kMTEANone;
        self.dateInMS    = NO;
    }
    
    return self;
}


#pragma mark-
#pragma mark Data Base methods

- (void)dbPerformAction
{ // Este método realiza la acción definida en la propiedad action en la base de datos.
    
    NSString *sqlInsert;
    NSString *sqlUpdate;
    NSString *sqlDelete;
    
    
    sqlInsert = @"INSERT INTO gst_previsto_men "
                "(nodo, fecha, he, hs, tipo, descripcion, id_relacion) "
                "VALUES (%@, %@, %@, %@, %@, %@, %@)";
    
    sqlUpdate = @"UPDATE gst_previsto_men "
                "SET he=%@, hs=%@, tipo=%@, descripcion=%@, id_relacion=%@ "
                "WHERE (nodo=%@ AND fecha=%@ AND he=%@)";

    
    
    
    if (self.dateInMS)
    {
        sqlDelete = @"DELETE FROM gst_previsto_men "
        "WHERE (nodo=%@ AND fecha=%@ AND he=%@)";
    } else
    {
//        sqlInsert = @"INSERT INTO gst_previsto_men "
//                    "(nodo, fecha, he, hs, tipo, descripcion, id_relacion) "
//                    "VALUES (%@, '%@', '%@', '%@', %@, '%@', %@)";
//        
        sqlUpdate = @"UPDATE gst_previsto_men "
                    "SET he=%@, hs=%@, tipo=%@, descripcion=%@, id_relacion=%@ "
                    "WHERE (nodo=%@ AND fecha=%@ AND he=%@)";
        
        sqlDelete = @"DELETE FROM gst_previsto_men "
                    "WHERE (nodo=%@ AND fecha=%@ AND he=%@)";
    }

    NSString *query = @"";
    NSString *novedadParam = @"";
    NSArray *arguments = nil;
    NSArray *whereArguments = nil;
    
    switch (self.action)
    {
        // Agregar registro.
        case kMTEAAdd:
            arguments = [self dbGetArguments:self.activity];
            query = [NSString stringWithFormat:sqlInsert,
                     [arguments objectAtIndex:valueNODO],
                     //(self.dateInMS) ? [arguments objectAtIndex:valueFECHAMS] : [arguments objectAtIndex:valueFECHA],
                     [arguments objectAtIndex:valueFECHAMS],
                     [NSString stringQuoted:[arguments objectAtIndex:valueHE] ifNull:@"''"],
                     [NSString stringQuoted:[arguments objectAtIndex:valueHS] ifNull:@"''"],
                     [arguments objectAtIndex:valueTIPO],
                     [NSString stringQuoted:[arguments objectAtIndex:valueDESC] ifNull:@"''"],
                     [arguments objectAtIndex:valueIDREL]
                    ];
            [[DB getInstance] excecuteSQL:query];
#ifdef DEBUG_PREVISTOM
            if (DEBUG_PREVISTOM > 0)
            {
            NSLog(@"--- Perform Action ---");
            NSLog(@"Query: %@", query);
            }
#endif
            
            // Genero el registro en el Log de novedades.
            novedadParam = [self getNovedadParameters:self.activity];
            [Novedad insert:@"gst_previsto_men" novedad:novedadParam tipo:kInsert];
#ifdef DEBUG_PREVISTOM
            if (DEBUG_PREVISTOM > 0)
            {
            NSLog(@"Novedad: I %@", novedadParam);
            }
#endif
            break;

            // Modificar registro.
        case kMTEAChange:
            arguments      = [self dbGetArguments:self.activity];
            whereArguments = [self dbGetArguments:self.oldActivity];
            query = [NSString stringWithFormat:sqlUpdate,
                     [NSString stringQuoted:[arguments objectAtIndex:valueHE] ifNull:@"''"],
                     [NSString stringQuoted:[arguments objectAtIndex:valueHS] ifNull:@"''"],
                     [arguments objectAtIndex:valueTIPO],
                     [NSString stringQuoted:[arguments objectAtIndex:valueDESC] ifNull:@"''"],
                     [arguments objectAtIndex:valueIDREL],
                     [whereArguments objectAtIndex:valueNODO],
                     [NSString stringQuoted:((self.dateInMS) ? [whereArguments objectAtIndex:valueFECHAMS] : [whereArguments objectAtIndex:valueFECHA]) ifNull:@"''"],
                     [NSString stringQuoted:[whereArguments objectAtIndex:valueHE] ifNull:@"''"]
                     ];
            [[DB getInstance] excecuteSQL:query];
#ifdef DEBUG_PREVISTOM
            if (DEBUG_PREVISTOM > 0)
            {
            NSLog(@"--- Perform Action ---");
            NSLog(@"Query: %@", query);
            }
#endif
            
            // Genero el registro en el Log de novedades.
            novedadParam = [self getNovedadParameters:self.oldActivity];
            [Novedad insert:@"gst_previsto_men" novedad:novedadParam tipo:kDelete];
#ifdef DEBUG_PREVISTOM
            if (DEBUG_PREVISTOM > 0)
            {
            NSLog(@"Novedad: D %@", novedadParam);
            }
#endif
            novedadParam = [self getNovedadParameters:self.activity];
            [Novedad insert:@"gst_previsto_men" novedad:novedadParam tipo:kInsert];
#ifdef DEBUG_PREVISTOM
            if (DEBUG_PREVISTOM > 0)
            {
            NSLog(@"Novedad: I %@", novedadParam);
            }
#endif
            break;
            
        // Eliminar registro.
        case kMTEADelete:
            whereArguments = [self dbGetArguments:self.oldActivity];
            query = [NSString stringWithFormat:sqlDelete,
                     [whereArguments objectAtIndex:valueNODO],
                     [NSString stringQuoted:((self.dateInMS) ? [whereArguments objectAtIndex:valueFECHAMS] : [whereArguments objectAtIndex:valueFECHA]) ifNull:@"''"],
                     [NSString stringQuoted:[whereArguments objectAtIndex:valueHE] ifNull:@"''"]
                     ];
#ifdef DEBUG_PREVISTOM
            if (DEBUG_PREVISTOM > 0)
            {
            NSLog(@"--- Perform Action ---");
            NSLog(@"Query: %@", query);
            }
#endif
            
            [[DB getInstance] excecuteSQL:query];
            // Genero el registro en el Log de novedades.
            novedadParam = [self getNovedadParameters:self.oldActivity];
            [Novedad insert:@"gst_previsto_men" novedad:novedadParam tipo:kDelete];
#ifdef DEBUG_PREVISTOM
            if (DEBUG_PREVISTOM > 0)
            {
            NSLog(@"Novedad: D %@", novedadParam);
            }
#endif
            break;
            
        default:
            break;
    }
    
    self.action = kMTEANone;
}


#pragma mark-
#pragma mark Others

+ (PrevistoMensual*)getForFecha:(NSDate*)date Tipo:(Type)tipo
{ // Este método devuelve el previsto para la fecha y turnos pasados como parámetros.

    PrevistoMensual *o = nil;
    o = [[PrevistoMensual alloc] init];
    o.date = date;
    o.type = tipo;
    o.activity = nil;

    
    o = [self getForFechaMS:date Tipo:tipo];
    
    // Cheque si la consulta anterior trajo algún dato.
    if (o.activity)
    {
        o.dateInMS = YES;
        return o; // si lo trajo, devuelvo el previsto.
    }
    
    
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    NSString *sDate = @"";
    
    NSString *sql = @"select pre.nodo, "        // 0
                            "pre.fecha, "       // 1
                            "pre.he, "          // 2
                            "pre.hs, "          // 3
                            "pre.tipo, "        // 4
                            "pre.descripcion, " // 5
                            "pre.id_relacion, " // 6
                            "case when pre.he < '12:00' then 1 else 2 end as actividad, " // 7
                            "med.apellido || \',\' || med.nombre as apenom, " // 8
                            "med.calle as medcalle, "     // 9
                            "med.altura as medaltura, "   // 10
                            "med.localidad as medloca, "  // 11
                            "inst.descripcion as institucion, " // 12
                            "inst.calle as instcalle, "         // 13
                            "inst.altura as instaltura, "       // 14
                            "inst.desclocalidad as instloca "   // 15
                     "from gst_previsto_men pre "
                            "left outer join mtc_instituciones inst on pre.tipo = 3 and pre.id_relacion = inst.id "
                            "left outer join med_medicos med on pre.tipo = 2 and pre.nodo = med.nodo and pre.id_relacion = med.id "
                     "where pre.nodo = %@ and pre.fecha >= '%@ 00:00' and pre.fecha < '%@ 00:00' and pre.he %@ order by pre.fecha, pre.he";

    
    NSString *idNodo = [[Config shareInstance] find:NODO];
    NSString *sDate2 = @"";
    
    NSDate *date2 = [date addDays:1];
    
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    sDate = [dateFormatter stringFromDate:date];
//    sDate2 = [dateFormatter stringFromDate:date2];
    sDate  = [date  formattedStringUsingFormat:@"yyyy-MM-dd"];
    sDate2 = [date2 formattedStringUsingFormat:@"yyyy-MM-dd"];
    NSString *query = [NSString stringWithFormat:sql, idNodo, sDate, sDate2, tipo==kMorning ? @"< '12:00'" : @">= '12:00'"];
    
    //NSLog(@"Query: %@", query);
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            o.activity          = [[MTPrevistoActividad alloc] init];
            o.activity.entidad  = [[MTEntidad           alloc] init];
    
            // Horario Entrada
            char *_aHE = (char *)sqlite3_column_text(statement, 2);
            sDate = @"";
            sDate = [NSString pCharToString:_aHE];
//            [dateFormatter setDateFormat:@"HH:mm"];
//            o.activity.entrada = [[NSDate alloc] init];
//            o.activity.entrada = [dateFormatter dateFromString:sDate];
            o.activity.entrada = [[NSDate alloc] initWithTimeString:sDate];
            
            // Horario Salida
            char *_aHS = (char *)sqlite3_column_text(statement, 3);
            sDate = @"";
            sDate = [NSString pCharToString:_aHS];
//            [dateFormatter setDateFormat:@"HH:mm"];
//            o.activity.salida = [[NSDate alloc] init];
//            o.activity.salida = [dateFormatter dateFromString:sDate];
            o.activity.salida = [[NSDate alloc] initWithTimeString:sDate];
            
            // Tipo
            int _tipoEntidad = sqlite3_column_int(statement, 4);
            [o.activity setEntityTypeFromDBvalue:_tipoEntidad];
            
            switch (o.activity.tipoEntidad)
            {
                // OTROS
                case kTEUnknown:
                    // id Entidad
                    o.activity.entidad.recID = @"0";
                    
                    // Nombre
                    o.activity.entidad.descripcion = @"";
                    char *_eNombre = (char *)sqlite3_column_text(statement, 5);
                    if ((_eNombre != nil) && (strlen(_eNombre) > 0))
                        o.activity.entidad.descripcion = [NSString stringWithUTF8String:_eNombre];
                    
                    break;
                    
                // MEDICO
                case kTEMedico:
                    // id Entidad
                    o.activity.entidad.recID = @"";
                    char *_idEntidadM = (char *)sqlite3_column_text(statement, 6);
                    if ((_idEntidadM != nil) && (strlen(_idEntidadM) > 0))
                        o.activity.entidad.recID = [NSString stringWithUTF8String:_idEntidadM];
                    
                    // Nombre
                    o.activity.entidad.descripcion = @"";
                    char *_NombreM = (char *)sqlite3_column_text(statement, 8);
                    if ((_NombreM != nil) && (strlen(_NombreM) > 0))
                        o.activity.entidad.descripcion = [NSString stringWithUTF8String:_NombreM];
                    
                    // Direccion
                    o.activity.entidad.direccion = @"";
                    char *_DireccionM = (char *)sqlite3_column_text(statement, 9);
                    if ((_DireccionM != nil) && (strlen(_DireccionM) > 0))
                        o.activity.entidad.direccion = [NSString stringWithUTF8String:_DireccionM];
                    
                    // Altura
                    int _AlturaM = sqlite3_column_int(statement, 10);
                    o.activity.entidad.altura = _AlturaM;
                    
                    // Localidad
                    o.activity.entidad.localidad = @"";
                    char *_LocalidadM = (char *)sqlite3_column_text(statement, 11);
                    if ((_LocalidadM != nil) && (strlen(_LocalidadM) > 0))
                        o.activity.entidad.localidad = [NSString stringWithUTF8String:_LocalidadM];
                    break;
                    
                // INSTITUCION
                case kTEInstitucion:
                    // id Entidad
                    o.activity.entidad.recID = @"";
                    char *_idEntidadI = (char *)sqlite3_column_text(statement, 6);
                    if ((_idEntidadI != nil) && (strlen(_idEntidadI) > 0))
                        o.activity.entidad.recID = [NSString stringWithUTF8String:_idEntidadI];

                    // Nombre
                    o.activity.entidad.descripcion = @"";
                    char *_NombreI = (char *)sqlite3_column_text(statement, 12);
                    if ((_NombreI != nil) && (strlen(_NombreI) > 0))
                        o.activity.entidad.descripcion = [NSString stringWithUTF8String:_NombreI];
                    
                    // Direccion
                    o.activity.entidad.direccion = @"";
                    char *_DireccionI = (char *)sqlite3_column_text(statement, 13);
                    if ((_DireccionI != nil) && (strlen(_DireccionI) > 0))
                        o.activity.entidad.direccion = [NSString stringWithUTF8String:_DireccionI];
                    
                    // Altura
                    int _AlturaI = sqlite3_column_int(statement, 14);
                    o.activity.entidad.altura = _AlturaI;
                    
                    // Localidad
                    o.activity.entidad.localidad = @"";
                    char *_LocalidadI = (char *)sqlite3_column_text(statement, 15);
                    if ((_LocalidadI != nil) && (strlen(_LocalidadI) > 0))
                        o.activity.entidad.localidad = [NSString stringWithUTF8String:_LocalidadI];
                    break;
                    
                default:
                    o.activity.entidad.recID = @"0";
                    break;
            }
            
        }
        
	} else {
        NSAssert(0, @"No se encontre las tablas de previsto mensual.");
	}
	sqlite3_finalize(statement);
    
    
    
    o.dateInMS = NO;
    return o;
}


+ (PrevistoMensual*)getForFechaMS:(NSDate*)date Tipo:(Type)tipo
{ // Este método devuelve el previsto para la fecha en milisegundos y turnos pasados como parámetros.
    
    
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    NSString *sDate = @"";
    
    NSString *sql = @"select pre.nodo, "        // 0
    "pre.fecha, "       // 1
    "pre.he, "          // 2
    "pre.hs, "          // 3
    "pre.tipo, "        // 4
    "pre.descripcion, " // 5
    "pre.id_relacion, " // 6
    "case when pre.he < '12:00' then 1 else 2 end as actividad, " // 7
    "med.apellido || \',\' || med.nombre as apenom, " // 8
    "med.calle as medcalle, "     // 9
    "med.altura as medaltura, "   // 10
    "med.localidad as medloca, "  // 11
    "inst.descripcion as institucion, " // 12
    "inst.calle as instcalle, "         // 13
    "inst.altura as instaltura, "       // 14
    "inst.desclocalidad as instloca "   // 15
    "from gst_previsto_men pre "
    "left outer join mtc_instituciones inst on pre.tipo = 3 and pre.id_relacion = inst.id "
    "left outer join med_medicos med on pre.tipo = 2 and pre.nodo = med.nodo and pre.id_relacion = med.id "
    "where pre.nodo = %@ and pre.fecha >= %@ and pre.fecha < %@ and pre.he %@ order by pre.fecha, pre.he";
    
    PrevistoMensual *o = nil;
    o = [[PrevistoMensual alloc] init];
    o.date = date;
    o.type = tipo;
    o.activity = nil;
    
    NSString *idNodo = [[Config shareInstance] find:NODO];
    NSString *sDate2 = @"";
    
    NSDate *date2 = [date addDays:1];
    
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    sDate = [date datetimeForDBAsString];
    sDate2 = [date2 datetimeForDBAsString];
    NSString *query = [NSString stringWithFormat:sql,
                       idNodo,
                       sDate,
                       sDate2,
                       tipo==kMorning ? @"< '12:00'" : @">= '12:00'"];
    
    //NSLog(@"Query: %@", query);
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            o.activity          = [[MTPrevistoActividad alloc] init];
            o.activity.entidad  = [[MTEntidad           alloc] init];
            
            // Horario Entrada
            char *_aHE = (char *)sqlite3_column_text(statement, 2);
            sDate = @"";
            sDate = [NSString pCharToString:_aHE];
//            [dateFormatter setDateFormat:@"HH:mm"];
//            o.activity.entrada = [[NSDate alloc] init];
//            //NSLog(@"Time formated: %@", [dateFormatter dateFromString:sDate]);
//            o.activity.entrada = [dateFormatter dateFromString:sDate];
            o.activity.entrada = [[NSDate alloc] initWithTimeString:sDate];
            
            // Horario Salida
            char *_aHS = (char *)sqlite3_column_text(statement, 3);
            sDate = @"";
            sDate = [NSString pCharToString:_aHS];
//            [dateFormatter setDateFormat:@"HH:mm"];
//            o.activity.salida = [[NSDate alloc] init];
//            o.activity.salida = [dateFormatter dateFromString:sDate];
            o.activity.salida = [[NSDate alloc] initWithTimeString:sDate];
            
            // Tipo
            int _tipoEntidad = sqlite3_column_int(statement, 4);
            [o.activity setEntityTypeFromDBvalue:_tipoEntidad];
            
            switch (o.activity.tipoEntidad)
            {
                    // OTROS
                case kTEUnknown:
                    // id Entidad
                    o.activity.entidad.recID = @"0";
                    
                    // Nombre
                    o.activity.entidad.descripcion = @"";
                    char *_eNombre = (char *)sqlite3_column_text(statement, 5);
                    if ((_eNombre != nil) && (strlen(_eNombre) > 0))
                        o.activity.entidad.descripcion = [NSString stringWithUTF8String:_eNombre];
                    
                    break;
                    
                    // MEDICO
                case kTEMedico:
                    // id Entidad
                    o.activity.entidad.recID = @"";
                    char *_idEntidadM = (char *)sqlite3_column_text(statement, 6);
                    if ((_idEntidadM != nil) && (strlen(_idEntidadM) > 0))
                        o.activity.entidad.recID = [NSString stringWithUTF8String:_idEntidadM];
                    
                    // Nombre
                    o.activity.entidad.descripcion = @"";
                    char *_NombreM = (char *)sqlite3_column_text(statement, 8);
                    if ((_NombreM != nil) && (strlen(_NombreM) > 0))
                        o.activity.entidad.descripcion = [NSString stringWithUTF8String:_NombreM];
                    
                    // Direccion
                    o.activity.entidad.direccion = @"";
                    char *_DireccionM = (char *)sqlite3_column_text(statement, 9);
                    if ((_DireccionM != nil) && (strlen(_DireccionM) > 0))
                        o.activity.entidad.direccion = [NSString stringWithUTF8String:_DireccionM];
                    
                    // Altura
                    int _AlturaM = sqlite3_column_int(statement, 10);
                    o.activity.entidad.altura = _AlturaM;
                    
                    // Localidad
                    o.activity.entidad.localidad = @"";
                    char *_LocalidadM = (char *)sqlite3_column_text(statement, 11);
                    if ((_LocalidadM != nil) && (strlen(_LocalidadM) > 0))
                        o.activity.entidad.localidad = [NSString stringWithUTF8String:_LocalidadM];
                    break;
                    
                    // INSTITUCION
                case kTEInstitucion:
                    // id Entidad
                    o.activity.entidad.recID = @"";
                    char *_idEntidadI = (char *)sqlite3_column_text(statement, 6);
                    if ((_idEntidadI != nil) && (strlen(_idEntidadI) > 0))
                        o.activity.entidad.recID = [NSString stringWithUTF8String:_idEntidadI];
                    
                    // Nombre
                    o.activity.entidad.descripcion = @"";
                    char *_NombreI = (char *)sqlite3_column_text(statement, 12);
                    if ((_NombreI != nil) && (strlen(_NombreI) > 0))
                        o.activity.entidad.descripcion = [NSString stringWithUTF8String:_NombreI];
                    
                    // Direccion
                    o.activity.entidad.direccion = @"";
                    char *_DireccionI = (char *)sqlite3_column_text(statement, 13);
                    if ((_DireccionI != nil) && (strlen(_DireccionI) > 0))
                        o.activity.entidad.direccion = [NSString stringWithUTF8String:_DireccionI];
                    
                    // Altura
                    int _AlturaI = sqlite3_column_int(statement, 14);
                    o.activity.entidad.altura = _AlturaI;
                    
                    // Localidad
                    o.activity.entidad.localidad = @"";
                    char *_LocalidadI = (char *)sqlite3_column_text(statement, 15);
                    if ((_LocalidadI != nil) && (strlen(_LocalidadI) > 0))
                        o.activity.entidad.localidad = [NSString stringWithUTF8String:_LocalidadI];
                    break;
                    
                default:
                    o.activity.entidad.recID = @"0";
                    break;
            }
            
        }
        
	} else {
        NSAssert(0, @"No se encontre las tablas de previsto mensual.");
	}
	sqlite3_finalize(statement);
    
    
    
    
    return o;
}

@end
