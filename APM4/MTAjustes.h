//
//  MTAjustes.h
//  APM4
//
//  Created by Laura Busnahe on 4/4/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTAjustes : NSObject

@property (nonatomic, assign) NSInteger         recID;
@property (nonatomic, strong) NSString          *descripcion;
@property (nonatomic, assign) NSInteger         operacion;
@property (nonatomic, assign) BOOL              activo;
@property (nonatomic, assign) BOOL              visibleAPM;

@property (nonatomic, assign) NSInteger         cantidad;
//@property (nonatomic, assign) NSDate            *date;
@property(nonatomic, retain) NSDate *date;


- (NSString*)getDescription;


+ (NSArray*)getAll;

@end
