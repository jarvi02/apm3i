//
//  EvaluacionPromocionesViewController.m
//  APM4
//
//  Created by Laura Busnahe on 9/2/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "EvaluacionPromocionesViewController.h"

@interface EvaluacionPromocionesViewController ()

@end

@implementation EvaluacionPromocionesViewController
{
    NSString *descTitulo;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.dataProductos = nil;
        self.dataVisitas = nil;
        
        self.cellProductosNib   = [UINib nibWithNibName:@"EvaluacionProductosCell" bundle:nil];
        self.headerProductosNib = [UINib nibWithNibName:@"EvaluacionProductosHeader" bundle:nil];
        self.cellVisitasNib     = [UINib nibWithNibName:@"EvaluacionVisitasCell" bundle:nil];
        self.headerVisitasNib   = [UINib nibWithNibName:@"EvaluacionVisitasHeader" bundle:nil];
        [self.headerProductosNib instantiateWithOwner:self options:nil];
        [self.headerVisitasNib   instantiateWithOwner:self options:nil];
        
        descTitulo = @"Evaluación de productos";
        self.title = descTitulo;
    }
    return self;
}

- (id)initWithDefaultNib
{
    self = [self initWithNibName:@"EvaluacionPromocionesViewController" bundle:nil];
    
    return self;
}

-(NSString *)getTitleForHeader
{
    NSString *nombreMedico = @"";
    if (self.carteraViewControllerReference)
        nombreMedico = [NSString string:self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre ifNull:@"?"];
    
    descTitulo = [NSString stringWithFormat:@"%@ - %@",
                  descTitulo , nombreMedico];
    
    return descTitulo;
}

-(UIImage *)getIconoForHeader
{
    return [UIImage imageNamed:@"cartera_48"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = descTitulo;
    
//    // Colores
//    [self.lblProductos setTitleColor:UIColorFromRGB(defaultTitlesFontColor) forState:UIControlStateNormal];
//    self.lblProductos.backgroundColor = UIColorFromRGB(defaultTitlesColor);
//    
//    [self.lblVisitas setTitleColor:UIColorFromRGB(defaultTitlesFontColor) forState:UIControlStateNormal];
//    self.lblVisitas.backgroundColor = UIColorFromRGB(defaultTitlesColor);
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.dataProductos = [MTEvaluacionProductos getAllForMedico:self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico];
    
    [self.productosTableView reloadData];
    [self.loadingView hide];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Table methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Productos
    if (tableView == self.productosTableView)
        return [self.dataProductos count];
    
    // Visitas
    if (tableView == self.visitasTableView)
        return [self.dataVisitas count];
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Productos
    if (tableView == self.productosTableView)
        return [self cellForProductoAtIndexPath:indexPath];
    
    // Visitas
    if (tableView == self.visitasTableView)
        return [self cellForVisitaAtIndexPath:indexPath];
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.productosTableView)
    {
        if (self.productosHeader)
            return self.productosHeader;
    } else
        
    if (tableView == self.visitasTableView)
    {
        if (self.visitasHeader)
            return self.visitasHeader;
    }
        
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    // Productos
    if (tableView == self.productosTableView)
        return self.productosHeader.frame.size.height;
    else
    
    if (tableView == self.visitasTableView)
        return self.visitasHeader.frame.size.height;
    
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    
    // Productos
    if (tableView == self.productosTableView)
    {
        MTEvaluacionProductos *o = [self.dataProductos objectAtIndex:indexPath.row];
        self.dataVisitas = [MTEvaluacionVisitas getAllForMedico:self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico
                                                       producto:o.idProducto];
        
        [self.visitasTableView reloadData];
    }
}

- (UITableViewCell *)cellForProductoAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"EvaluacionProductoCelldentifier";
    
    // Obtain the cell object.
    EvaluacionProductosCell *cell = (EvaluacionProductosCell *)[self.productosTableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellProductosNib instantiateWithOwner:self options:nil];
    	cell = self.productosCell;
    	self.productosCell = nil;
    }
    
    // Obtengo los datos para la celada.
    MTEvaluacionProductos *o = [self.dataProductos objectAtIndex:indexPath.row];
    
    // Datos de las celdas
    cell.lblProducto.text   = [NSString string:o.descripcion];
    cell.lblObjetivo.text   = [NSString string:o.objetivo ifNull:@""];
    
    return cell;
}

- (UITableViewCell *)cellForVisitaAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"EvaluacionVisitaCelldentifier";
    
    // Obtain the cell object.
    EvaluacionVisitasCell *cell = (EvaluacionVisitasCell *)[self.visitasTableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellVisitasNib instantiateWithOwner:self options:nil];
    	cell = self.visitasCell;
    	self.visitasCell = nil;
    }
    
    // Obtengo los datos para la celada.
    MTEvaluacionVisitas *o = [self.dataVisitas objectAtIndex:indexPath.row];
    
    // Datos de las celdas
    cell.lblFecha.text      = [o.fecha formattedStringUsingFormat:@"dd/MM/yyyy"];
    cell.lblEvaluacion.text = [NSString string:o.evaluacion ifNull:@"-"];
    
    return cell;
}


@end
