//
//  Domicilio.h
//  APM4
//
//  Created by Juan Pablo Garcia on 11/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tipo.h"
#import "Institucion.h"
#import "Cargo.h"
#import "Provincia.h"
#import "Localidad.h"
#import "Utilidad.h"
#import "Calle.h"

@interface Domicilio : NSObject

@property(nonatomic)NSUInteger idDomicilio;

@property(nonatomic, retain) Tipo *tipo;
@property(nonatomic, retain) Institucion *institucion;
@property(nonatomic, retain) Cargo *cargo;
@property(nonatomic, retain) Provincia *provincia;
@property(nonatomic, retain) Localidad *localidad;
@property(nonatomic, retain) Calle *calle;
@property(nonatomic, assign) NSInteger numero;
@property(nonatomic, assign) NSInteger piso;
@property(nonatomic, retain) NSString *depto;
@property(nonatomic, retain) NSString *codigoPostal;
@property(nonatomic, retain) Utilidad *utilidad;
@property(nonatomic, assign) BOOL consultorioExterno;
@property(nonatomic, retain) NSMutableArray *horarios;
@property(nonatomic, retain) NSMutableArray *telefonos;


+(Domicilio *)getByMedico:(NSUInteger)idMedico;

-(NSString*)getDescripcion;
-(NSString*)getCpDescripcion;
-(NSString*)isOkForSave;
//-(NSString*)buscarCPforCalle:(NSInteger)idCalle;

- (void)fillDomicilioByCP;
@end
