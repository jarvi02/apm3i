//
//  Programados.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 25/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Programados.h"
#import "Config.h"
#import "DB.h"

@implementation Programados

- (id)init
{
    self = [super init];
    if (self) {
        self.descripcion = @"";
        self.literatura  = @"";
        self.obsequio    = @"";
        self.objetivos   = @"";
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [_literatura release];
    [_obsequio release];
    [_objetivos release];
    [super dealloc];
}


+(NSArray*)GetAllForDoctor:(NSInteger)idDoctor {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    NSString *sql = @"select orden, producto, mtc.descripcion, cantidad, case when literatura = 0 then 'No' else 'Si' end as literatura, case when obsequio = 0 then 'No' else 'Si' end as obsequio, objetivos from med_apromocionar med inner join mtc_productos mtc ON mtc.id = med.producto  where med.nodo = %@ AND med.medico = %d order by orden;";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql, nodo, idDoctor];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        Programados *o;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[Programados alloc] init];
            
            o.orden = sqlite3_column_int(statement, 0);
            o.producto = sqlite3_column_int(statement, 1);
            
            char *_d = (char *)sqlite3_column_text(statement, 2);
            if((_d != nil) && (strlen(_d) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_d];
			}
            
            o.cantidad = sqlite3_column_int(statement, 3);
            
            char *_an = (char *)sqlite3_column_text(statement, 4);
            if((_an != nil) && (strlen(_an) > 0)) {
                o.literatura = [NSString stringWithUTF8String:_an];
			}
            
            char *_obs = (char *)sqlite3_column_text(statement, 5);
            if((_obs != nil) && (strlen(_obs) > 0)) {
                o.obsequio = [NSString stringWithUTF8String:_obs];
			}
            
            char *_obj = (char *)sqlite3_column_text(statement, 6);
            if((_obj != nil) && (strlen(_obj) > 0)) {
                o.objetivos = [NSString stringWithUTF8String:_obj];
			}
            
            [array addObject:o];
            [o release];
            
        }
        
        
	} else {
        NSAssert(0, @"No se encontre la tabla configuracion");
	}
	sqlite3_finalize(statement);

    return [array autorelease];
}


@end
