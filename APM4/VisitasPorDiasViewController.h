//
//  VisitasPorDiasViewController.h
//  APM4
//
//  Created by Laura Busnahe on 9/11/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "popListExtended.h"
#import "visitasPorDiasCell.h"
#import "visitasPorDiasHeader.h"
#import "visitasPorDiasFooter.h"

@interface VisitasPorDiasViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate,
                                                                MTpopListViewDelegate, MTvistasPorDiaHeaderDelegate>

@property (retain, nonatomic) IBOutlet UITextField  *lblPeriodo;
@property (retain, nonatomic) IBOutlet UIButton     *btnPeriodo;

@property (nonatomic, strong) NSMutableArray                *data;
@property (nonatomic, strong) IBOutlet visitasPorDiasCell   *tmpCell;
@property (nonatomic, strong) UINib                         *cellNib;
@property (nonatomic, strong) UINib                         *headerNib;

@property (strong, nonatomic) IBOutlet visitasPorDiasHeader *tableHeader;
@property (strong, nonatomic) IBOutlet visitasPorDiasFooter *tableFooter;
@property (strong, nonatomic) IBOutlet UITableView          *tableView;

@property (strong, nonatomic) NSArray                       *arrayCombo;
@property (strong, nonatomic) MTpopListView                 *popupMenu;


- (id)initViewDefaultNib;

- (IBAction)tapBtnPeriodo:(id)sender;




@end
