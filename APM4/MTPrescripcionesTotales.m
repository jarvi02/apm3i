//
//  MTPrescripcionesTotales.m
//  APM4
//
//  Created by Laura Busnahe on 3/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTPrescripcionesTotales.h"
#import "MTMercados.h"
#import "DB.h"

#import "GlobalDefines.h"

#pragma mark -
@implementation MTTotalesMedico

- (id)init
{
    self = [super init];
    if (self)
    {
        self.descripcion = @"";
        self.unidades = 0;
        self.porcentaje = 0.0f;
    }
    
    return self;
}

@end

#pragma mark -
@implementation MTTotalesProducto

- (id)init
{
    self = [super init];
    if (self)
    {
        self.descripcion = @"";
        self.trimestre1  = 0;
        self.trimestre2  = 0;
        self.trimestre3  = 0;
        self.trimestre4  = 0;
        self.total       = 0;
        self.laboratorio = false;
        self.descMercado = @"";
       
    }
    
    return self;
}

@end


#pragma mark -
@implementation MTarrayTotalesProductos

- (id)init
{
    self = [super init];
    if (self)
    {
        _backArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

#pragma mark NSArray overrides

- (NSUInteger)count
{
    return [_backArray count];
}

- (MTTotalesProducto*)objectAtIndex:(NSUInteger)index
{
    return [_backArray objectAtIndex:index];
}


#pragma mark NSMutableArray overrides

- (void)addObject:(MTTotalesProducto*)objeto
{
    [_backArray addObject:objeto];
}

- (void)insertObject:(MTTotalesProducto*)anObject atIndex:(NSUInteger)index
{
    [_backArray insertObject:anObject atIndex:index];
}

- (void)removeLastObject;
{
    [_backArray removeLastObject];
}

- (void)removeObjectAtIndex:(NSUInteger)index;
{
    [_backArray removeObjectAtIndex:index];
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(MTTotalesProducto*)anObject;
{
    [_backArray replaceObjectAtIndex:index withObject:anObject];
}

#pragma mark Sort methods
- (void)sortByProperty:(NSString*)key Ascending:(BOOL)ascending
{
    NSSortDescriptor *sortDescriptor;
    NSSortDescriptor *sortDescriptor2;
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key
                                                  ascending:ascending];
    
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"descripcion"
                                                 ascending:YES];
    
    NSArray *sortDescriptors;
    NSArray *sortedArray;
    
    
    // Chequeo si se ordena por
    if ([key compare:@"descripcion"] != NSOrderedSame)
    {
        sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, sortDescriptor2, nil];
    } else
    {
        sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    }
    
    sortedArray = [_backArray sortedArrayUsingDescriptors:sortDescriptors];
    _backArray = [NSMutableArray arrayWithArray:sortedArray];
    
}
@end


#pragma mark -
@implementation MTarrayTotalesMedico

- (id)init
{
    self = [super init];
    if (self)
    {
        _backArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

#pragma mark NSArray overrides

- (NSUInteger)count
{
    return [_backArray count];
}

- (MTTotalesMedico*)objectAtIndex:(NSUInteger)index
{
    return [_backArray objectAtIndex:index];
}


#pragma mark NSMutableArray overrides

- (void)addObject:(MTTotalesMedico*)objeto
{
    [_backArray addObject:objeto];
}

- (void)insertObject:(MTTotalesMedico*)anObject atIndex:(NSUInteger)index
{
    [_backArray insertObject:anObject atIndex:index];
}

- (void)removeLastObject;
{
    [_backArray removeLastObject];
}

- (void)removeObjectAtIndex:(NSUInteger)index;
{
    [_backArray removeObjectAtIndex:index];
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(MTTotalesMedico*)anObject;
{
    [_backArray replaceObjectAtIndex:index withObject:anObject];
}

@end


#pragma mark -
@implementation MTPrescripcionesTotales

- (id)init
{
    self = [super init];
    if (self)
    {
        self.prescTotalesMedico = nil;
        self.prescProductos     = nil;
    }
    
    return self;
}


+ (MTarrayTotalesProductos*)getPrescripcionesForMercados:(NSArray*)arrayMercados
                                               Auditoria:(NSInteger)idAuditoria
                                                  Medico:(NSInteger)idMedico
{
    MTarrayTotalesProductos *array = [[MTarrayTotalesProductos alloc] init];
        
    NSString *sql   = @"";
    NSString *query = @"";
    
    // Chequeo si la lista de mercados está populada, o vacia, o no tiene ningún datos seleccionado.
    NSString *sMercados = [MTMercados getSelectedIdsString:arrayMercados];
    if ((sMercados == nil) || ([sMercados length] == 0))
    {
        // Si está vacía, obtengo los productos de todos los mercados
        sql = @"SELECT distinct dpm_productos.descripcion, t1, t2, t3, t4, (t1 + t2 + t3 + t4), laboratorio, dpm_mercados.descripcion as descMercado "
        "FROM dpm_prescripciones "
        "JOIN  dpm_productos ON (dpm_productos.auditoria = dpm_prescripciones.auditoria and dpm_productos.id = dpm_prescripciones.producto) "
        "JOIN dpm_mercados ON (dpm_mercados.mercado = dpm_prescripciones.mercado AND dpm_mercados.auditoria = dpm_prescripciones.auditoria) "
        "WHERE dpm_prescripciones.auditoria = %i and "
        "dpm_prescripciones.medico = %i";
        
        query = [NSString stringWithFormat:sql, idAuditoria, idMedico];
    } else
    {
        // En caso de tener datos, selecciono únicamente los mercados de la lista.
        sql = @"SELECT distinct dpm_productos.descripcion, t1, t2, t3, t4, (t1 + t2 + t3 + t4), laboratorio, dpm_mercados.descripcion as descMercado "
        "FROM dpm_prescripciones "
        "JOIN  dpm_productos ON (dpm_productos.auditoria = dpm_prescripciones.auditoria and dpm_productos.id = dpm_prescripciones.producto) "
        "JOIN dpm_mercados ON (dpm_mercados.mercado = dpm_prescripciones.mercado AND dpm_mercados.auditoria = dpm_prescripciones.auditoria) "
        "WHERE dpm_prescripciones.auditoria = %i and "
        "dpm_prescripciones.medico = %i and "
        "dpm_prescripciones.mercado IN (%@)";
        
        query = [NSString stringWithFormat:sql, idAuditoria, idMedico, sMercados];
    }
    
    
    
    
#ifdef DEBUG_PRESCRIPCIONES
    NSLog(@"--- Presc for Mercados query ---");
    NSLog(@"%@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement )
    {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTTotalesProducto *o = [[MTTotalesProducto alloc] init];
            
            // Descripción
            char* _descripcion = (char*)sqlite3_column_text(statement, 0);
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o.descripcion = [NSString stringWithUTF8String:_descripcion];
            
            // Trimestre 1
            int _trimestre1 = sqlite3_column_int(statement, 1);
            o.trimestre1 = _trimestre1;
            
            // Trimestre 2
            int _trimestre2 = sqlite3_column_int(statement, 2);
            o.trimestre2 = _trimestre2;
            
            // Trimestre 3
            int _trimestre3 = sqlite3_column_int(statement, 3);
            o.trimestre3 = _trimestre3;
            
            // Trimestre 4
            int _trimestre4 = sqlite3_column_int(statement, 4);
            o.trimestre4 = _trimestre4;
            
            // Total
            int _total = sqlite3_column_int(statement, 5);
            o.total = _total;
            
            // Laboratorio
            o.laboratorio = false;
            char* _laboratorio = (char*)sqlite3_column_text(statement, 6);
            if ((_laboratorio != nil) && (strlen(_laboratorio) > 0))
            {
                NSString *sValor = [NSString stringWithUTF8String:_laboratorio];
                o.laboratorio = [sValor compare:@"S"] == NSOrderedSame ? TRUE: FALSE;
            }
            
            // Descripción Mercado
            char* _descMercado = (char*)sqlite3_column_text(statement, 7);
            if ((_descMercado != nil) && (strlen(_descMercado) > 0))
                o.descMercado = [NSString stringWithUTF8String:_descMercado];
            
            [array addObject:o];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla dpm_prescripciones");
	}
	sqlite3_finalize(statement);
    
    return array;
}


+ (MTPrescripcionesTotales*)getTotalesMedico:(NSInteger)idMedico
                   Auditoria:(NSInteger)idAuditoria
                    Mercados:(NSArray*)arrayMercados
{
    MTPrescripcionesTotales *result = [[MTPrescripcionesTotales alloc] init];
    MTarrayTotalesMedico *arrayPrescTotales    = [[MTarrayTotalesMedico alloc] init];
    
    MTTotalesMedico *totalMedico      = [[MTTotalesMedico alloc] init];
    MTTotalesMedico *totalMercado     = [[MTTotalesMedico alloc] init];
    MTTotalesMedico *totalLaboratorio = [[MTTotalesMedico alloc] init];
    totalMedico.descripcion      = @"Total médico";
    totalMercado.descripcion     = @"Total mercados";
    totalLaboratorio.descripcion = @"Total laboratorio";

    
    NSString *sql = @"select total "
    "from dpm_medicos "
    "where auditoria = %i and medico = %i";
    NSString *query = [NSString stringWithFormat:sql, idAuditoria, idMedico];
    
    
#ifdef DEBUG_PRESCRIPCIONES
    NSLog(@"--- Presc for Médico query ---");
    NSLog(@"%@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            // Laboratorio
            int *_total = sqlite3_column_int(statement, 0);
            totalMedico.unidades = _total;
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla dpm_medicos");
	}
	sqlite3_finalize(statement);

    // Obtengo las prescripciones de cada uno de los productos del mercado para el médico.
    MTarrayTotalesProductos *arrayProductos = [MTPrescripcionesTotales getPrescripcionesForMercados:arrayMercados
                                                                                          Auditoria:idAuditoria
                                                                                             Medico:idMedico];
    
    if ([arrayProductos count] > 0)
    {
        for (MTTotalesProducto *o in arrayProductos)
        {
            NSInteger total = o.total;
            
            totalMercado.unidades = totalMercado.unidades + total;
            
            if (o.laboratorio)
                totalLaboratorio.unidades = totalLaboratorio.unidades + total;
            
        }
    }
    
    if (totalMedico.unidades > 0)
    {
        totalMedico.porcentaje      = 100;
        totalMercado.porcentaje     = ((float)totalMercado.unidades / (float)totalMedico.unidades) * 100.0f;
        totalLaboratorio.porcentaje = ((float)totalLaboratorio.unidades / (float)totalMedico.unidades) * 100.0f;
    }
    
    [arrayPrescTotales addObject:totalMedico];
    [arrayPrescTotales addObject:totalMercado];
    [arrayPrescTotales addObject:totalLaboratorio];
    
    result.prescTotalesMedico = arrayPrescTotales;
    result.prescProductos     = arrayProductos;
    
    return result;
}

@end
