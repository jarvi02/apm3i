//
//  NoVisitaViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "NoVisitaViewController.h"
#import "AppDelegate.h"
#import "DateSelectorViewController.h"
#import "EntryForCombo.h"
#import "TableForComboPopoverViewController.h"
#import "NSDateExtended.h"
#import "MTFeriado.h"
#import "Ciclo.h"
#import "DB.h"
#import "Config.h"

@interface NoVisitaViewController ()
@property(nonatomic, retain) NSArray            *motivosNoVisita;
@property(nonatomic, retain) MotivosNoVisita    *motivoSeleccionado;
@property(nonatomic, retain) NSString           *actividadSeleccionada;
@property(nonatomic, retain) NSDate             *currentDate;
@end

@implementation NoVisitaViewController
{
    NSString *descTitulo;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        descTitulo = @"No Visita";
        self.title = descTitulo;
        
        self.ficha = nil;
        self.noVisita = [[MTVisita alloc] init];
    }
    return self;
}

- (id)initWithDefaultNib
{
    self = [self initWithNibName:@"NoVisitaViewController" bundle:nil];
    
    return self;
}

-(NSString *)getTitleForHeader
{
    return self.title;
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"user_novisita"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentDate = [[NSDate date] dateAsDateWithoutTime];
    self.txtFecha.text = [self.currentDate formattedStringUsingFormat:@"dd/MM/yyyy"];
    
    // Motivos de No visita.
    self.motivosNoVisita = [MotivosNoVisita GetAll];
    
    
    
    // --- ALTA --------
    if (self.modo == NoVisitaModoAlta)
    {
        [self setDate:[NSDate date]];
        
        // Carga los datos del médico.
        if ([self.noVisita.medico.recID integerValue] == 0)
            self.noVisita.medico = [MTMedico GetById:self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico];
    }
    else
        
    // --- MODIFICACION ---------
    if ((self.modo == NoVisitaModoModificar) ||
        (self.modo == NoVisitaModoVer))
    {
                
        NSAssert(self.noVisita != nil, @"Debe setear la no visita en caso de modificacion");
        
        // Cargo los datos del médico.
        if ([self.noVisita.medico.recID integerValue] == 0)
        self.noVisita.medico = [MTMedico GetById:self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico];
                
        // Ver formato
        [self.noVisita iniciarModificacion];         // This must to be done before to make any modification so will keep a copy of old data which
                                                   // will be necesary into the DB updates.
        [self setDate:self.noVisita.fecha];
                
        // Tipo de Visita
        int i = 0;
        for (MotivosNoVisita * t in self.motivosNoVisita)
        {
            if ([t.descripcion isEqualToString:self.noVisita.descripcionTipo])
            {
                [self setTipoVisita:i];
            }
            i++;
        }
                
        
    }
    
    
    descTitulo = [NSString stringWithFormat:@"No Visita - %@, %@",
                  [NSString string:self.noVisita.medico.apellido ifNull:@"" ],
                  [NSString string:self.noVisita.medico.nombre ifNull:@"" ]];
    
    self.title = descTitulo;
    
    [super viewDidLoad];
    
    NSString *tActividad = self.noVisita.medico.actividad;
    [self setActividad:tActividad];
    
    // Chequeo si es modo solo de lectura (para las visitas ya transmitidas).
    if (self.modo == NoVisitaModoVer)
    {
        [self.btnGuardar  setHidden:YES];
        [self.btnCancelar setHidden:YES];
        [self.btnManiana  setEnabled:NO];
        [self.btnTarde    setEnabled:NO];
        [self.btnManianaFrame  setEnabled:NO];
        [self.btnTardeFrame    setEnabled:NO];
        [self.btnFecha    setEnabled:NO];
        [self.btnTipo     setEnabled:NO];
        [self.lblNoVisitaTransmitida setHidden:NO];
        
    } else
    {
        [self.btnGuardar  setHidden:NO];
        [self.btnCancelar setHidden:NO];
        [self.btnManiana  setEnabled:YES];
        [self.btnTarde    setEnabled:YES];
        [self.btnManianaFrame  setEnabled:YES];
        [self.btnTardeFrame    setEnabled:YES];
        [self.btnFecha    setEnabled:YES];
        [self.btnTipo     setEnabled:YES];
        [self.lblNoVisitaTransmitida setHidden:YES];
    }
    
//    ((UILabel*)[self.view viewWithTag:TAG_TITULO_HEADER]).text = [NSString stringWithFormat:@"%@ - %@",self.title, self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre];
//    
//    ((UILabel*)[self.view viewWithTag:TAG_TITULO]).text = self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Tap methods

-(IBAction)selectManiana:(id)sender
{
    [self setActividad:@"M"];
}

-(IBAction)selectTarde:(id)sender
{
    [self setActividad:@"T"];
}

-(IBAction)selectFecha:(id)sender
{
    if (!Delegate.popoverController.popoverVisible)
    {
        DateSelectorViewController *dvc = [[DateSelectorViewController alloc] initWithNibName:@"DateSelectorViewController" bundle:nil];
        
        dvc.contentSizeForViewInPopover = CGSizeMake(320.0f, 216.0f);
        dvc.date = self.currentDate;
        dvc.delegate = self;
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:dvc];
        
        Delegate.popoverController = [[UIPopoverController alloc] initWithContentViewController:nav];
        [Delegate.popoverController presentPopoverFromRect:((UIButton*)sender).frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

-(IBAction)selectTipo:(id)sender
{
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    NSMutableArray *entries = [[NSMutableArray alloc] initWithCapacity:0];
    EntryForCombo *e;
    
    for (int i=0; [self.motivosNoVisita count] > i; i++) {
        
        MotivosNoVisita *o = [self.motivosNoVisita objectAtIndex:i];
        
        e = [[EntryForCombo alloc] initWithValue:o.descripcion forIndex:i];
        
        [entries addObject:e];
    }
    
    
    CGSize size = CGSizeMake(440, 400);
    TableForComboPopoverViewController *t = [[TableForComboPopoverViewController alloc] initWithData:entries
                                                                                              titulo:@"Motivos de No Visita"
                                                                                                size:size
                                                                                            delegate:self
                                                                                           andCaller:@"NoVisita"];
    t.aligment = NSTextAlignmentLeft;
    
    Delegate.popoverController = [[UIPopoverController alloc] initWithContentViewController:t];
    
    [Delegate.popoverController presentPopoverFromRect:((UIButton*)sender).frame inView:self.view permittedArrowDirections:YES animated:YES];
}

-(IBAction)guardar:(id)sender
{
    if ([self validarFecha])
    {
        MTMedico *medico = self.noVisita.medico;
        
        // Busco el domicilio
        Domicilio *d = [Domicilio getByMedico:[medico.recID integerValue]];
        if (d == nil)
        {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Debe indicar el domicilio del médico, No se encontró el domicilio principal"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [av show];
            
            return;
        }
        
        self.noVisita.medico       = medico;
        self.noVisita.actividad    = self.actividadSeleccionada;
        self.noVisita.fecha        = [self.currentDate dateAsDateWithoutTime];
        self.noVisita.idTipo       = self.motivoSeleccionado.idMotivo;
        self.noVisita.objetivos    = @"";
        self.noVisita.evaluacion   = @"";
        self.noVisita.idDomicilio  = d.idDomicilio;
        self.noVisita.contacto       = 0;
        
        // ALTA
        if (self.modo == NoVisitaModoAlta)
        {
            [self.noVisita guardarNuevaVisita];
            
        } else
            
        // MODIFICACION
        if (self.modo == NoVisitaModoModificar)
        {
            [self.noVisita guardarModificacion];
                    
            // Actualizo el objecto de ficha de visita
            if (self.ficha)
            {
                self.ficha.fechaAsDate = self.noVisita.fecha;
                self.ficha.descripcion = self.noVisita.descripcionTipo;
            }
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)cancelar:(id)sender
{
    if (self.modo == NoVisitaModoModificar)
    {
        [self.noVisita cancelarModificacion];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark TableForComboPopoverViewControllerDelegate

-(void)selectedIndex:(NSInteger)index caller:(NSString*)caller
{
    [Delegate dismissPopOver:YES];
    [self setTipoVisita:index];
}


#pragma mark -
#pragma mark DateSelector Delegate

-(void)DateSelector:(DateSelectorViewController*)dateSelector dateSelected:(NSDate *)date
{
    [self setDate:date];
}


#pragma mark -
#pragma mark Private methods

-(void)setDate:(NSDate *)date
{
    self.currentDate = date;
    self.txtFecha.text = [self.currentDate formattedStringUsingFormat:@"dd/MM/yyyy"];
}

-(void)setTipoVisita:(NSUInteger)index
{
    MotivosNoVisita *tipo = (MotivosNoVisita*)[self.motivosNoVisita objectAtIndex:index];
    self.txtTipo.text = tipo.descripcion;
    self.motivoSeleccionado = tipo;
}

-(void)setActividad:(NSString *)actividad
{
    if ([actividad isEqualToString:@"M"])
    {
        self.actividadSeleccionada = @"M";
        [self.btnManiana setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
        [self.btnTarde setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    }
    else
    {
        self.actividadSeleccionada = @"T";
        [self.btnManiana setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
        [self.btnTarde setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    }
}

-(BOOL)validarFecha
{
    NSInteger idMedico = 0;
    if (self.modo == NoVisitaModoAlta)
    {
        idMedico = self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico;
    } else
    {
        idMedico = [self.noVisita.medico.recID integerValue];
    }
    
    // esta validacion la hago por las dudas para no generar inconsistencia, pero nunca deberia de pasar que el id del medico quede en cero MFS.
    if (idMedico == 0)
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Debe seleccionar un médico"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        
        [av show];
        return NO;
    }
    
    // Si estoy modificando y no cambio la fecha entonces no hace falta hacer las validaciones de fechas
    if (self.modo == NoVisitaModoModificar && [self.noVisita.fecha compare:self.currentDate] == NSOrderedSame)
    {
        return YES;
    }
    
    // Chequeo si la fecha corresponde a días futuros.
    if ([self.currentDate compare:[NSDate date]] == NSOrderedDescending)
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Fecha superior al día en curso"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        
        [av show];
        
        return NO;
    }
    
    // Chequeo si la fecha está dentro del ciclo actual.
    Ciclo *ciclo = [Ciclo actual];
    if (([[self.currentDate dateAsDateWithoutTime] compare:[ciclo.desde dateAsDateWithoutTime]] == NSOrderedAscending) ||
        ([[self.currentDate dateAsDateWithoutTime] compare:[ciclo.hasta dateAsDateWithoutTime]] == NSOrderedDescending))
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                     message:[NSString stringWithFormat:@"Fecha incorrecta . Ciclo comprendido entre fecha %@ y %@",
                                                              [ciclo.desde formattedStringUsingFormat:@"dd/MM/yyyy"],
                                                              [ciclo.hasta formattedStringUsingFormat:@"dd/MM/yyyy"]]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        
        [av show];
        
        return NO;
    }
    
    
    if (![MTVisita validarVisitaExiste:idMedico
                           fechaVisita:[self.currentDate dateAsDateWithoutTime]
                         fechaOriginal:(self.noVisita != nil ? [self.noVisita.fecha dateAsDateWithoutTime] : nil)])
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"El médico ya fue visitado en la fecha seleccionada."
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        
        [av show];
        
        return NO;
    }
    
    
    //BOOL cargarConFechaAnterior = NO;
    BOOL *cargaConFechaAnterior = [[ConfigGlobal shareInstance] find:CARGAVISITASCONFECHAANTERIOR];
    if (cargaConFechaAnterior == NO)
    {
        if (![MTVisita validarFechaDeVisista:idMedico fechaVisita:self.currentDate fechaOriginal:self.noVisita != nil ? self.noVisita.fecha : nil ])
        {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Hay visitas con fecha posterior en el ciclo."
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            
            [av show];
            
            return NO;
        }
        
    }
    
    BOOL visitaFinDeSemana = [[ConfigGlobal shareInstance] find:VIS_FINDESEMANA];
    
    // Validar por fines de semana y feriados
    if (visitaFinDeSemana == NO)
    {
        if ([self.currentDate isWeekEnd] || [MTFeriado isFeriado:self.currentDate])
        {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"la fecha seleccionada es fin de semana o feriado."
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            
            [av show];
            
            return NO;
        }
    }
    
    
    return YES;
}

@end
