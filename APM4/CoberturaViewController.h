//
//  CoberturaViewController.h
//  APM4
//
//  Created by Laura Busnahe on 7/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "coberturaHeader.h"
#import "coberturaCell.h"
#import "coberturaFooter.h"
#import "popListView.h"

#define _kCoberturaEspecialidad 1
#define _kCoberturaPotencial    2
#define _kCoberturaLugarVisita  3

@interface CoberturaViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate, MTpopListViewDelegate, MTCoberturaHeaderDelegate>

@property (retain, nonatomic) IBOutlet UITextField  *lblPeriodo;
@property (retain, nonatomic) IBOutlet UIButton     *btnPeriodo;

@property (nonatomic, strong) NSMutableArray                *data;
@property (nonatomic, strong) IBOutlet coberturaCell        *tmpCell;
@property (nonatomic, strong) UINib                         *cellNib;
@property (nonatomic, strong) UINib                         *headerNib;

@property (strong, nonatomic) IBOutlet coberturaHeader      *tableHeader;
@property (retain, nonatomic) IBOutlet coberturaFooter      *tableFooter;
@property (retain, nonatomic) IBOutlet UITableView          *tableView;

@property (strong, nonatomic) NSArray                       *arrayCombo;
@property (strong, nonatomic) MTpopListView                 *popupMenu;

@property (assign, nonatomic) NSInteger                     type;

- (id)initViewDefaultNibFor:(NSInteger)type;

- (IBAction)tapBtnPeriodo:(id)sender;

@end
