//
//  VisitaViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "VisitaViewController.h"
#import "AppDelegate.h"
#import "NSDate+extensions.h"
#import "EntryForCombo.h"
#import "TipoVisita.h"
#import "TrazabilidadViewController.h"
#import "CustomProductoCell.h"
#import "ProductoTrazabilidad.h"
#import "Ciclo.h"
#import "Config.h"
#import "ConfigGlobal.h"
#import "MTFeriado.h"
#import "MTVisita.h"
#import "MTMedico.h"
#import "EstadoVisita.h"
#import "Domicilio.h"
#import "Promocion.h"
#import "Novedad.h"
#import "ParteDiario.h"
#import "ParteDiarioData.h"
#import "PrescripcionesViewController.h"
#import "TrazabilidadFamiliaViewController.h"
#import "GlobalDefines.h"
#import "NSDateExtended.h"
#import "NSStringExtended.h"
#import "Visita.h"

@interface VisitaViewController ()

-(void)setDate:(NSDate *)date;
-(void)setActividad:(NSString *)actividad;
-(void)setTipoVisita:(NSUInteger)index;
-(CarteraMedico *)cartera;
-(BOOL)validarFecha;

@end

@implementation VisitaViewController
{
    NSString *descTitulo;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        descTitulo = @"Visita";
        self.title = descTitulo;
        
        self.trasitionStack = nil;
        self.ficha = nil;
        self.visita = [[MTVisita alloc] init];
        
        self.entregadosList = [[NSMutableArray alloc] init];
        self.promocionesList = [[NSMutableArray alloc] init];
        
        self.headerNib  = [UINib nibWithNibName:@"CustomProductoHeader" bundle:nil];
        [self.headerNib instantiateWithOwner:self options:nil];
        
        self.promoHeaderNib  = [UINib nibWithNibName:@"CustomPromocionHeader" bundle:nil];
        [self.promoHeaderNib instantiateWithOwner:self options:nil];
    }
    return self;
}

-(NSString *)getTitleForHeader
{
    return self.title;
}

-(UIImage *)getIconoForHeader
{
    return [UIImage imageNamed:@"cabecera_principal_32"];
}

-(void)viewDidLoad
{
    //[super viewDidLoad];
    
    self.tiposVisita = [TipoVisita GetAll];
    [self setTipoVisita:0];
    
    
    // --- ALTA --------
    if (self.modo == VisitaModoAlta)
    {
        [self setDate:[NSDate date]];
        
        // Carga los datos del médico.
        if ([self.visita.medico.recID integerValue] == 0)
            self.visita.medico = [MTMedico GetById:self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico];
        
#ifdef INCLUDE_PRESCRICIONES
        // Chequeo si al mostrarse debe ir a la vista de Prescripciones.
        if (([[ConfigGlobal shareInstance] isValidKey:VISITADPM]))
        {
            if ([[ConfigGlobal shareInstance] find:VISITADPM])
            {
                PrescripcionesViewController *p;
                p = [[PrescripcionesViewController alloc] initWithNibName:@"PrescripcionesViewController" bundle:nil];
                p.carteraViewControllerReference = self.carteraViewControllerReference;
                
                self.trasitionStack = p;
            }
        }
#endif
       
        
        self.comentarios.text = self.visita.evaluacion;
        self.objetivos.text = [NSString string:[self cartera].objetivos ifNull:@""];
    }
    else
    
    // --- ALTA FLUCTUANTE -------
    if ((self.modo == VisitaModoAltaFluctuante))
    {
        [self setDate:[self.visita.fecha dateAsDateWithoutTime]];
        self.objetivos.text     = [NSString string:self.visita.objetivos ifNull:@""];
        self.comentarios.text   = [NSString string:self.visita.evaluacion ifNull:@""];
        self.objetivosProx.text = [NSString string:self.visita.medico.objetivos ifNull:@""];
        
        if ([self.visita.medico.recID integerValue] == 0)
            self.visita.medico = [MTMedico GetById:self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico];
        
    } else
     
    // --- MODIFICACION ---------
    if ((self.modo == VisitaModoModificar) ||
        (self.modo == VisitaModoVer))
    {
        
        NSAssert(self.visita != nil, @"Debe setear la visita en caso de modificacion");
        
        // Cargo los datos del médico.
        if ([self.visita.medico.recID integerValue] == 0)
            self.visita.medico = [MTMedico GetById:self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico];
        
        // Cargo los productos de la visita
        [self loadProductosFromVisita];
        
        // Ver formato
        [self.visita iniciarModificacion];         // This must to be done before to make any modification so will keep a copy of old data which
                                                // will be necesary into the DB updates.
        [self setDate:self.visita.fecha];
        
        // Tipo de Visita
        int i = 0;
        for(TipoVisita * t in self.tiposVisita)
        {
            if ([t.descripcion isEqualToString:self.visita.descripcionTipo])
            {
                [self setTipoVisita:i];
            }
            i++;
        }
        
        // Actualizar el stock de productos, volver al valor original para poder hacer el update
        for(Promocion *p in self.visita.productos)
        {
            //[ProductoTrazabilidad actualizarStock:p.producto lote:p.lote cantidad:p.muestras];
            p.stock = p.stock + p.muestras;
        }
        
        self.objetivos.text     = [NSString string:self.visita.objetivos ifNull:@""];
        self.comentarios.text   = [NSString string:self.visita.evaluacion ifNull:@""];
        self.objetivosProx.text = [NSString string:self.visita.medico.objetivos ifNull:@""];        
    }
    
    
    descTitulo = [NSString stringWithFormat:@"Visita - %@, %@",
                  [NSString string:self.visita.medico.apellido ifNull:@"" ],
                  [NSString string:self.visita.medico.nombre ifNull:@"" ]];
    
    self.title = descTitulo;
    
    [super viewDidLoad];
    
    NSString *tActividad = self.visita.medico.actividad;
    [self setActividad:tActividad];
    
    
    [self updateListados];
    
    #ifdef DEBUG_VISITA
    NSLog(@"%@ - %@ - %@", self.currentActividad, [self cartera].actividad, self.visita.actividad);
    #endif
    
    if (VISITA_PROMOCION_SIN_DESCARGA)
    {
        [self.lblPromociones           setHidden:YES];
        [self.muestrasProductosSegment setHidden:NO];
        //self.muestrasProductosSegment.selectedSegmentIndex = 0;
    } else
    {
        [self.lblPromociones           setHidden:NO];
        [self.muestrasProductosSegment setHidden:YES];
        self.muestrasProductosSegment.selectedSegmentIndex = 0;
    }
    
    // Chequeo si es modo solo de lectura (para las visitas ya transmitidas).
    if (self.modo == VisitaModoVer)
    {
        [self.editar      setHidden:YES];
        [self.btnAgregar  setHidden:YES];
        [self.btnGuardar  setHidden:YES];
        [self.btnCancelar setHidden:YES];
        [self.tarde       setEnabled:NO];
        [self.maniana     setEnabled:NO];
        [self.btnManiana  setEnabled:NO];
        [self.btnTarde    setEnabled:NO];
        [self.btnFecha    setEnabled:NO];
        [self.btnTipo     setEnabled:NO];
        [self.lblVisitaTransmitida setHidden:NO];
        [self.comentarios   setEnabled:NO];
        [self.objetivosProx setEnabled:NO];
        
    } else
    {
        [self.editar      setHidden:NO];
        [self.btnAgregar  setHidden:NO];
        [self.btnGuardar  setHidden:NO];
        [self.btnCancelar setHidden:NO];
        [self.tarde       setEnabled:YES];
        [self.maniana     setEnabled:YES];
        [self.btnManiana  setEnabled:YES];
        [self.btnTarde    setEnabled:YES];
        [self.btnFecha    setEnabled:YES];
        [self.btnTipo     setEnabled:YES];
        [self.lblVisitaTransmitida setHidden:YES];
        [self.comentarios   setEnabled:YES];
        [self.objetivosProx setEnabled:YES];
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.scrolledView.contentSize = self.view.frame.size;
    
    // Chequeo si tenia alguna transición pendiente.
    if (_trasitionStack != nil)
    {
        [self navigateToController:_trasitionStack];
        _trasitionStack = nil;
    }

}


#pragma mark -
#pragma mark Private Members

- (void)loadProductosFromVisita
{
    self.visita.productos = [NSMutableArray arrayWithArray:
                         [Promocion GetAllByMedico:[self.visita.medico.recID integerValue]
                                            visita:self.visita.idVisita]];
}

-(void)setDate:(NSDate *)date
{
    self.currentDate = date;
    self.fecha.text = [self.currentDate formattedStringUsingFormat:@"dd/MM/yyyy"];
}

-(void)setTipoVisita:(NSUInteger)index
{
    TipoVisita *tipo = (TipoVisita*)[self.tiposVisita objectAtIndex:index];
    self.tipo.text = tipo.descripcion;
    self.currentTipo = tipo;
}

-(void)setActividad:(NSString *)actividad
{
    if ([actividad isEqualToString:@"M"])
    {
        self.currentActividad = @"M";
        [self.maniana setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
        [self.tarde setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    }
    else
    {
        self.currentActividad = @"T";
        [self.maniana setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
        [self.tarde setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
    }
}


-(CarteraMedico *)cartera
{
    return self.carteraViewControllerReference.itemCarteraMedicoSelected;
}

-(void)navigateToController:(UIViewController *)controller
{
    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark -
#pragma mark TableForComboPopoverViewControllerDelegate

-(void)selectedIndex:(NSInteger)index caller:(NSString*)caller
{
    [Delegate dismissPopOver:YES];
    [self setTipoVisita:index];
}


#pragma mark -
#pragma mark DateSelector Delegate

-(void)DateSelector:(DateSelectorViewController*)dateSelector dateSelected:(NSDate *)date
{
    [self setDate:date];
}


#pragma mark -
#pragma mark Tap methods

-(IBAction)selectManiana:(id)sender
{
    [self setActividad:@"M"];
}

-(IBAction)selectTarde:(id)sender
{
    [self setActividad:@"T"];
}

-(IBAction)selectFecha:(id)sender
{
    if (!Delegate.popoverController.popoverVisible)
    {
        DateSelectorViewController *dvc = [[DateSelectorViewController alloc] initWithNibName:@"DateSelectorViewController" bundle:nil];
        
        dvc.contentSizeForViewInPopover = CGSizeMake(320.0f, 216.0f);
        dvc.date = self.currentDate;
        dvc.delegate = self;
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:dvc];
        
        Delegate.popoverController = [[UIPopoverController alloc] initWithContentViewController:nav];
        [Delegate.popoverController presentPopoverFromRect:((UIButton*)sender).frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

-(IBAction)selectTipo:(id)sender
{
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    NSMutableArray *entries = [[NSMutableArray alloc] initWithCapacity:0];
    EntryForCombo *e;
    
    for (int i=0; [self.tiposVisita count] > i; i++) {
        
        TipoVisita *o = [self.tiposVisita objectAtIndex:i];
        
        e = [[EntryForCombo alloc] initWithValue:o.descripcion forIndex:i];
        
        [entries addObject:e];
    }
    
    
    CGSize size = CGSizeMake(440, 400);
    TableForComboPopoverViewController *t = [[TableForComboPopoverViewController alloc] initWithData:entries
                                                                                              titulo:@"Tipos Visita"
                                                                                                size:size
                                                                                            delegate:self
                                                                                           andCaller:@"Visita"];
    t.aligment = NSTextAlignmentLeft;
    
    Delegate.popoverController = [[UIPopoverController alloc] initWithContentViewController:t];
    
    [Delegate.popoverController presentPopoverFromRect:((UIButton*)sender).frame inView:self.view permittedArrowDirections:YES animated:YES];
}

-(IBAction)guardar:(id)sender
{
    if ([self validarFecha])
    {
        MTMedico *medico = self.visita.medico;
    
        // Busco el domicilio
        Domicilio *d = [Domicilio getByMedico:[medico.recID integerValue]];
        if ((d == nil) &&
            (self.modo != VisitaModoAltaFluctuante))
        {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Debe indicar el domicilio del médico, No se encontró el domicilio principal"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            [av show];
            
            return;
        }
        
        medico.objetivos         = [NSString string:self.objetivosProx.text ifNull:@""];
        self.visita.medico       = medico;
        self.visita.actividad    = self.currentActividad;
        self.visita.fecha        = [self.currentDate dateAsDateWithoutTime];
        self.visita.idTipo       = self.currentTipo.tipoId;
        self.visita.objetivos    = [NSString string:self.objetivos.text ifNull:@""];
        self.visita.evaluacion   = [NSString string:self.comentarios.text ifNull:@""];
        self.visita.idDomicilio  = d.idDomicilio;
        self.visita.contacto     = 1;
        
        /*
        for(int i = 0; i < self.visita.productos.count; i++){

            if(((Promocion*)self.visita.productos[i]).promocionado)
                ((Promocion*)self.visita.productos[i]).orden = i + 1;
        }*/
        
        
        
        // ALTA
        if (self.modo == VisitaModoAlta)
        {
            [self.visita guardarNuevaVisita];
            
        } else
        
        // ALTA para FLUCTUANTE
        if (self.modo == VisitaModoAltaFluctuante)
        {
            self.visita.idVisita = [self.visita newRecID];
            
        } else
         
        // MODIFICACION
        if (self.modo == VisitaModoModificar)
        {
            [self.visita guardarModificacion];
            
            // Actualizo el objecto de ficha de visita
            if (self.ficha)
            {
                self.ficha.fechaAsDate = self.visita.fecha;
                self.ficha.descripcion = self.visita.descripcionTipo;
            }
        }
        
        for(Promocion* p in self.visita.productos){
           
            int famId = [ProductoTrazabilidad getFamiliaIdfromProductoId:p.producto];
            
            NSString *nodo = SettingForKey(NODO, @"");

            [Visita addVisitaFamilia:famId Visita:self.visita.idVisita nodoId:[nodo intValue] medicoId:medico.recID andOrden:p.orden];
          
        }
    
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)cancelar:(id)sender
{
    if (self.modo == VisitaModoModificar)
    {
        [self.visita cancelarModificacion];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)agregar:(id)sender
{
    
    // Reordeno los productos según el OrdenIngreso.
    NSArray *sorted = [self.visita.productos sortedArrayUsingComparator:^NSComparisonResult(id a, id b)
                       {
                           NSUInteger first = ((Promocion*)a).ordenIngreso;
                           NSUInteger second = ((Promocion*)b).ordenIngreso;
                           
                           return [[NSNumber numberWithInt:first] compare:[NSNumber numberWithInt:second]];
                       }];
    [self.visita.productos removeAllObjects];
    [self.visita.productos addObjectsFromArray:sorted];
    
    // Inicializo y cargo la ventana de Trazabilidad.
    TrazabilidadViewController *tvc = [[TrazabilidadViewController alloc] initWithNibName:@"TrazabilidadViewController" bundle:nil];
    
    tvc.modalPresentationStyle = UIModalPresentationFullScreen;
    tvc.delegate = self;
    
   // [self.navigationController presentViewController:tvc animated:YES completion:nil];
    
    UISplitViewController* splitViewController = [[UISplitViewController alloc] init];
    tvc.parentSplitVC = splitViewController;
 
    TrazabilidadFamiliaViewController* tmvc = [[TrazabilidadFamiliaViewController alloc] initWithNibName:@"TrazabilidadFamiliaViewController" bundle:nil];
    
    tmvc.detailVC = tvc;
    tmvc.medicoId = [self.visita.medico.recID integerValue];
    if(self.promocionesList.count == 0)
        tmvc.ordenCounter = 1;
    else
        tmvc.ordenCounter = self.promocionesList.count;
    
    // Envío los productos.
    tmvc.productosActuales = [NSArray arrayWithArray:self.visita.productos];
    
    UINavigationController *rootNav = [[UINavigationController alloc] initWithRootViewController:tmvc];
    
    UINavigationController *detailNav = [[UINavigationController alloc] initWithRootViewController:tvc];
    
    splitViewController.viewControllers = [NSArray arrayWithObjects:rootNav, detailNav, nil];
    splitViewController.delegate = tvc;
    
    [self presentViewController:splitViewController animated:YES completion:nil];
    
}

- (IBAction)editar:(id)sender
{
    // Muestras entregadas
    if (![self.productos isHidden])
    {
        if (self.productos.editing == NO)
        {
            [self.productos setEditing:YES animated:YES];
            [self.editar setTitle:@"Finalizar" forState:UIControlStateNormal];
            
            // Deshabilito el segmento de selección de tablas para que no se pueda cambiar de tabla mientras se edita.
            [self.muestrasProductosSegment setUserInteractionEnabled:NO];
        } else
        {
            [self.productos setEditing:NO animated:YES];
            [self.editar setTitle:@"Editar" forState:UIControlStateNormal];
            
            [self updateListados];
            [self.productos reloadData];
            
            // Vuevlo a habilitar el segmento de selección de tablas.
            [self.muestrasProductosSegment setUserInteractionEnabled:YES];
        }
    } else
        
    // Productos Promocionados
    if (![self.promociones isHidden])
    {
        [self promocionesEditTable];
    }
}

- (IBAction)tapMustrasProductosSegment:(id)sender
{
    UISegmentedControl *o = sender;
    
    
    switch (o.selectedSegmentIndex)
    {
            // Muestras Entregadas.
        case 0:
            [self.productos setHidden:NO];
            [self.promociones setHidden:YES];
            
            [self updateListados];
            [self.productos reloadData];
            break;
            
            // Productos Promocionados
        case 1:
            [self.promociones setHidden:NO];
            [self.productos setHidden:YES];
            
            [self updateListados];
            [self.promociones reloadData];
            break;
            
        default:
            break;
    }
}



#pragma mark -
#pragma mark TrazabilidadDelegate

-(void)productosModificados:(NSArray *)productos
{
    // Paso de la coleccion de ProductoTrzabilidad a Promocion
    NSMutableArray *list = [NSMutableArray array];
    
    for(ProductoTrazabilidad *p in productos)
    {
        Promocion *pr = [[Promocion alloc] init];
        
        pr.medico = p.medico;
        //pr.orden = p.ordenIngreso;
        pr.orden = [p.orden2 integerValue];
        pr.ordenIngreso = p.ordenIngreso;
        pr.descripcionProducto = p.descripcion;
        pr.literatura = p.literatura == YES ? 1 : 0;
        pr.obsequio = p.obsequio == YES ? 1 : 0;
        
        pr.promocionado = p.promocionado == YES ? 1 : 0;
        
        pr.lote = p.loteId;
        pr.muestras = p.cantidad;
        pr.codigoLote = p.codigo;
        pr.stock = p.stock;
        pr.descripcionLote = p.lote;
        pr.producto = [p.codigo integerValue];
        
        pr.objetivos = [NSString string:p.objetivos];
        pr.evaluacion = [NSString string:p.evaluacion];
        
        /*
         
         cell.nro.text = [NSString stringWithFormat:@"%d", p.ordenIngreso+1];
         cell.producto.text = p.descripcionLote;
         cell.cantidad.text = [NSString stringWithFormat:@"%d", p.muestras];
         cell.codigo.text = p.codigoLote;
         //cell.lote.text = p;
         cell.stock.text = [NSString stringWithFormat:@"%d", p.stock];
         cell.literatura.text = p.literatura ? @"Si" : @"No";
         cell.obsequio.text = p.obsequio ? @"Si" : @"No";
         cell.objetivos.text = p.evaluacion;
         
         */
        
        [list addObject:pr];
    }
    
    self.visita.productos = [NSMutableArray arrayWithArray:list];
    
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc]
                                 initWithKey:@"ordenIngreso"
                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject: sorter];
    [self.visita.productos sortUsingDescriptors:sortDescriptors];
    
    // Actualizo las tablas
    [self updateListados];
    if (![self.productos isHidden])
        [self.productos reloadData];
    
    if (![self.promociones isHidden])
        [self.promociones reloadData];
}

#pragma mark -
#pragma mark UITableViewDelegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of time zone names.
    
    // Muestras entregadas
    if ((tableView == self.productos) && (VISITA_PROMOCION_SIN_DESCARGA))
        return [self.entregadosList count];
    else
      
    // Productos promocionados
    if ((tableView == self.promociones) || (!VISITA_PROMOCION_SIN_DESCARGA))
        return [self.promocionesList count];
    
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    // Muestras Entregadas
    if (tableView == self.productos)
    {
        static NSString *MyIdentifier = @"ProductosCellIdentifier";
        
        // Obtain the cell object.
        CustomProductoCell *cell = (CustomProductoCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        // If the cell is void, create the cell object.
        if (cell == nil)
        {
            cell = [[CustomProductoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
            [[NSBundle mainBundle] loadNibNamed:@"CustomProductoCell" owner:self options:nil];
            cell = self.tmpCell;
        }
        
        Promocion *p = nil;
        
        if (VISITA_PROMOCION_SIN_DESCARGA)
        {
            p = [self.entregadosList objectAtIndex:indexPath.row];
            [cell.promocionado setHidden:NO];
            
        } else
        {
            p = [self.promocionesList objectAtIndex:indexPath.row];
            [cell.promocionado setHidden:YES];
        }
        
        
        if (p.ordenIngreso > 0)
            cell.nro.text = [NSString stringWithFormat:@"%d", p.ordenIngreso];
        else
            cell.nro.text = @"-";
        
        cell.producto.text = p.descripcionProducto;
        cell.cantidad.text = [NSString stringWithFormat:@"%d", p.muestras];
        
        cell.codigo.text = [NSString string:p.codigoLote ifNull:@""];
        cell.lote.text = [NSString string:p.descripcionLote ifNull:@""];
        
        cell.stock.text = [NSString stringWithFormat:@"%d", p.stock-p.muestras];
        
        
        cell.promocionado.text = p.promocionado ? @"Si" : @"-";
        cell.literatura.text = p.literatura ? @"Si" : @"-";
        cell.obsequio.text = p.obsequio ? @"Si" : @"-";
        cell.objetivos.text = p.evaluacion;
        
        return cell;
    } else
    
    // Productos Promocionados
    if (tableView == self.promociones)
    {
        return [self promocionesCellForRowAtIndexPath:indexPath];
    }
    
    else
        return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Muestras entregadas
    if (tableView == self.productos)
    {
        if (VISITA_PROMOCION_SIN_DESCARGA)
        {
            [self.tmpHeader.lblPromocionado setHidden:NO];
            //[self.tmpHeader.lblPromocionado setTitle:@"Promo." forState:UIControlStateNormal];
        } else
        {
            [self.tmpHeader.lblPromocionado setHidden:YES];
            //[self.tmpHeader.lblPromocionado setTitle:@"O.Obj." forState:UIControlStateNormal];
        }
        return self.tmpHeader;
    }
    else
 
    // Productos promocionados
    if (tableView == self.promociones)
        return self.promoHeader;
    
    else
        return nil;
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    if ((tableView == self.productos) && (VISITA_PROMOCION_SIN_DESCARGA))
    {
        Promocion *p = [self.entregadosList objectAtIndex:fromIndexPath.row];
        
        [self.entregadosList removeObjectAtIndex:fromIndexPath.row];
        [self.entregadosList insertObject:p atIndex:toIndexPath.row];
        
    } else

    // Productos promocionados
    if ((tableView == self.promociones) || (!VISITA_PROMOCION_SIN_DESCARGA))
    {
        [self promocionesMoveRowAtIndexPath:fromIndexPath toIndexPath:toIndexPath];
    }
    
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((tableView == self.productos) && (VISITA_PROMOCION_SIN_DESCARGA))
       return NO;
   else
       return YES;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.modo == VisitaModoVer)
        return NO;
    else
        return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Muestras entregadas
    if (tableView == self.productos)
    {
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            Promocion *o = [self.entregadosList objectAtIndex:indexPath.row];
            o.muestras = 0;
            
            if ((!o.promocionado) || (!VISITA_PROMOCION_SIN_DESCARGA))
            {
                [self.visita.productos removeObject:o];
            }
          
            [self updateListados];
            [self.productos reloadData];
        } else
            
            if (editingStyle == UITableViewCellEditingStyleInsert)
            {
                // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
            }
    } else
        
    // Productos promocionados
    if (tableView == self.promociones)
    {
        [self promocionesCommitEditingStyle:editingStyle forRowAtIndexPath:indexPath];
    }
}


#pragma mark -
#pragma mark Validaciones

-(BOOL)validarFecha
{
    NSInteger idMedico = 0;
    if (self.modo == VisitaModoAlta)
    {
        idMedico = [self cartera].idCarteraMedico;
    } else
    {
        idMedico = [self.visita.medico.recID integerValue];
    }
    
    // esta validacion la hago por las dudas para no generar inconsistencia, pero nunca deberia de pasar que el id del medico quede en cero MFS.
    if (idMedico == 0)
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Debe seleccionar un médico"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        
        [av show];
        return NO;
    }
    
    // Si estoy modificando y no cambio la fecha entonces no hace falta hacer las validaciones de fechas
    if (self.modo == VisitaModoModificar && [self.visita.fecha compare:self.currentDate] == NSOrderedSame)
    {
        return YES;
    }
    
    // Chequeo si la fecha corresponde a días futuros.
    if ([self.currentDate compare:[NSDate date]] == NSOrderedDescending)
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Fecha superior al día en curso"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        
        [av show];
        
        return NO;
    }
    
    // Chequeo si la fecha está dentro del ciclo actual.
    Ciclo *ciclo = [Ciclo actual];    
    if (([[self.currentDate dateAsDateWithoutTime] compare:[ciclo.desde dateAsDateWithoutTime]] == NSOrderedAscending) ||
        ([[self.currentDate dateAsDateWithoutTime] compare:[ciclo.hasta dateAsDateWithoutTime]] == NSOrderedDescending))
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                     message:[NSString stringWithFormat:@"Fecha incorrecta . Ciclo comprendido entre fecha %@ y %@",
                                                                    [ciclo.desde formattedStringUsingFormat:@"dd/MM/yyyy"],
                                                                    [ciclo.hasta formattedStringUsingFormat:@"dd/MM/yyyy"]]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        
        [av show];
        
        return NO;
    }
    
    
    if (![MTVisita validarVisitaExiste:idMedico
                         fechaVisita:[self.currentDate dateAsDateWithoutTime]
                       fechaOriginal:(self.visita != nil ? [self.visita.fecha dateAsDateWithoutTime] : nil)])
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"El médico ya fue visitado en la fecha seleccionada."
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        
        [av show];
        
        return NO;
    }  
   

    //BOOL cargarConFechaAnterior = NO;
    BOOL *cargaConFechaAnterior = [[ConfigGlobal shareInstance] find:CARGAVISITASCONFECHAANTERIOR];
    if (cargaConFechaAnterior == NO)
    {
        if (![MTVisita validarFechaDeVisista:idMedico fechaVisita:self.currentDate fechaOriginal:self.visita != nil ? self.visita.fecha : nil ])
        {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Hay visitas con fecha posterior en el ciclo."
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            
            [av show];
            
            return NO;
        }        
        
    }
    
    BOOL visitaFinDeSemana = [[ConfigGlobal shareInstance] find:VIS_FINDESEMANA];
    
    // Validar por fines de semana y feriados
    if (visitaFinDeSemana == NO)
    {
        if ([self.currentDate isWeekEnd] || [MTFeriado isFeriado:self.currentDate])
        {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"la fecha seleccionada es fin de semana o feriado."
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
            
            [av show];
            
            return NO;
        }
    }


    return YES;
}



#pragma mark -
#pragma mark Rotations methods

// Notifies when rotation begins, reaches halfway point and ends.
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    self.scrolledView.contentSize = self.view.frame.size;
}


#pragma mark -
#pragma mark Promociones Table methods

- (void) promocionesEditTable
{
    if (self.promociones.editing == NO)
    {
        [self.promociones setEditing:YES animated:YES];
        [self.editar setTitle:@"Finalizar" forState:UIControlStateNormal];
        
        // Deshabilito el segmento de selección de tablas para que no se pueda cambiar de tabla mientras se edita.
        [self.muestrasProductosSegment setUserInteractionEnabled:NO];
    } else
    {
        [self.promociones setEditing:NO animated:YES];
        [self.editar setTitle:@"Editar" forState:UIControlStateNormal];
        
        [self updateListados];
        [self.promociones reloadData];
        
        // Vuevlo a habilitar el segmento de selección de tablas.
        [self.muestrasProductosSegment setUserInteractionEnabled:YES];
    }
}

- (UITableViewCell *)promocionesCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"PromoCellIdentifier";
    
    // Obtain the cell object.
    CustomPromoCell *cell = (CustomPromoCell *)[self.promociones dequeueReusableCellWithIdentifier:MyIdentifier];
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        cell = [[CustomPromoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        [[NSBundle mainBundle] loadNibNamed:@"CustomPromocionCell" owner:self options:nil];
        cell = self.promoCell;
    }
    
    Promocion *p = [self.promocionesList objectAtIndex:indexPath.row];
    
    cell.lblOrden.text = [NSString stringWithFormat:@"%d", p.ordenIngreso];
    cell.lblProducto.text = p.descripcionProducto;
    if (p.muestras > 0)
        cell.lblCantidad.text = [NSString stringWithFormat:@"%d", p.muestras];
    else
        cell.lblCantidad.text = @"-";
    
    cell.lblCodigo.text = [NSString string:p.codigoLote ifNull:@""];
    
    cell.lblObjetivo.text = p.evaluacion;
    
    return cell;
}

- (void)promocionesMoveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    Promocion *p = [self.promocionesList objectAtIndex:fromIndexPath.row];
    
    [self.promocionesList removeObjectAtIndex:fromIndexPath.row];
    [self.promocionesList insertObject:p atIndex:toIndexPath.row];
    
    int i = 1;
    for(Promocion *p1 in self.promocionesList)
    {
        //p1.orden = i;
        p1.ordenIngreso = i;
        i++;
    }
}

- (void)promocionesCommitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        Promocion *o = [self.promocionesList objectAtIndex:indexPath.row];
        o.promocionado = NO;
        
        if (o.muestras == 0)
        {
            [self.visita.productos removeObject:o];
        }
        
        [self updateListados];
        [self.promociones reloadData];
    } else
        if (editingStyle == UITableViewCellEditingStyleInsert)
        {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
}

-(void)updateListados
{
    // Borro el contenido de los listados actuales
    [self.entregadosList removeAllObjects];
    [self.promocionesList removeAllObjects];
    
    // Actualizo los listados
    for (Promocion *p in self.visita.productos)
    {
        if (p.promocionado)
            [self.promocionesList addObject:p];
        
        if (p.muestras > 0)
            [self.entregadosList addObject:p];
    }
    
    // Reordeno las listas
    // Muestras Entregadas
    NSArray *sorted = [self.entregadosList sortedArrayUsingComparator:^NSComparisonResult(id a, id b)
                       {
                           NSComparisonResult *result;
                           
                           NSUInteger first = ((Promocion*)b).promocionado;
                           NSUInteger second = ((Promocion*)a).promocionado;
                           
                           result = [[NSNumber numberWithInt:first] compare:[NSNumber numberWithInt:second]];
                           
                           if (result == NSOrderedSame)
                           {
                               NSUInteger first = ((Promocion*)a).ordenIngreso;
                               NSUInteger second = ((Promocion*)b).ordenIngreso;
                               result = [[NSNumber numberWithInt:first] compare:[NSNumber numberWithInt:second]];
                           }
                           
                           return result;
                       }];
    [self.entregadosList removeAllObjects];
    [self.entregadosList addObjectsFromArray:sorted];
    
    // Productos Promocionados
    NSArray *sorted2 = [self.promocionesList sortedArrayUsingComparator:^NSComparisonResult(id a, id b)
                       {
                           NSUInteger first = ((Promocion*)a).ordenIngreso;
                           NSUInteger second = ((Promocion*)b).ordenIngreso;
                           
                           return [[NSNumber numberWithInt:first] compare:[NSNumber numberWithInt:second]];
                       }];
    
    [self.promocionesList removeAllObjects];
    [self.promocionesList addObjectsFromArray:sorted2];
}

#pragma mark -
#pragma mark TextField methods
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}

@end
