//
//  CustomPromoCell.h
//  APM4
//
//  Created by Laura Busnahe on 7/23/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPromoCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblOrden;
@property (retain, nonatomic) IBOutlet UILabel *lblProducto;
@property (retain, nonatomic) IBOutlet UILabel *lblCodigo;
@property (retain, nonatomic) IBOutlet UILabel *lblCantidad;
@property (retain, nonatomic) IBOutlet UILabel *lblObjetivo;
@end
