//
//  Promocion.m
//  APM4
//
//  Created by Juan Pablo Garcia on 12/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Promocion.h"
#import "Config.h"
#import "DB.h"
#import "Novedad.h"

@implementation Promocion
{
    
}

#pragma mark -
#pragma mark Class methods

+(NSArray *)GetAllByMedico:(NSUInteger) medico visita:(NSUInteger)visita
{
    NSString *sql =
    @"SELECT med_promociones.nodo, "
        "med_promociones.medico, "
        "mtc_lotes.descripcion, "
        "med_promociones.orden, "
        "med_promociones.muestras, "
        "med_promociones.literatura, "
        "med_promociones.obsequio, "
        "med_promociones.producto, "
        "ifnull(med_promociones.evaluacion, ''), "
        "stock, "
        "codigo, "
        "mtc_lotes.lote, "
        "idlote, "
        "fecha_vto, "
        "ifnull(med_apromocionar.objetivos, '') "
    "FROM med_promociones INNER JOIN "
        "mtc_lotes on (mtc_lotes.id = med_promociones.producto AND "
        "               "
        "              mtc_lotes.nodo = med_promociones.nodo) LEFT JOIN "
        "med_apromocionar on (med_apromocionar.producto = med_promociones.producto AND "
        "                     med_apromocionar.medico = med_promociones.medico AND "
        "                     med_apromocionar.nodo = med_promociones.nodo) "
        "WHERE med_promociones.nodo = %@ AND "
        "med_promociones.medico = %d AND "
        "med_promociones.visitador = %@ AND "
        "med_promociones.visita = %d";
    
    
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *query = [NSString stringWithFormat:sql, nodo, medico, nodo, visita];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
#ifdef DEBUG_TRAZABILIDAD
    NSLog(@" - Promocion: GetAllByMedico - ");
    NSLog(@"- Query: %@", query);
#endif
    
    NSMutableArray *promos = [NSMutableArray array];
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            Promocion *p = [[Promocion alloc] init];
            
            // med_promociones.nodo
            p.nodo = sqlite3_column_int(statement, 0);
            
            // medico
            p.medico = sqlite3_column_int(statement, 1);
            
            // mtc_lotes.descripcion
            char *_descripcion = (char *)sqlite3_column_text(statement, 2);
            p.descripcionProducto = [NSString pCharToString:_descripcion];
            
            // orden
            p.orden = sqlite3_column_int(statement, 3);
            p.ordenIngreso = p.orden;
            
            // muestras
            p.muestras = sqlite3_column_int(statement, 4);
            
            // literatura
            p.literatura = sqlite3_column_int(statement, 5);
            
            // obsequio
            p.obsequio = sqlite3_column_int(statement, 6);
            
            // producto
            p.producto = sqlite3_column_int(statement, 7);
            
            // evaluacion
            _descripcion = (char *)sqlite3_column_text(statement, 8);
            p.evaluacion = [NSString pCharToString:_descripcion];
			
            
            // stock
            p.stock = sqlite3_column_int(statement, 9);
            
            
            // codigo
            _descripcion = (char *)sqlite3_column_text(statement, 10);
            p.codigoLote = [NSString pCharToString:_descripcion];
            
            // mtc_lotes.lote
            _descripcion = (char *)sqlite3_column_text(statement, 11);
            p.descripcionLote = [NSString pCharToString:_descripcion];
            
            // idlote
            p.lote = sqlite3_column_int(statement, 12);
            
            // fecha_vto
            double fecha = sqlite3_column_double(statement, 13);
            p.fechaVto = [NSDate dateWithTimeIntervalSince1970:fecha];
            
            // objetivos
            _descripcion = (char *)sqlite3_column_text(statement, 14);
            p.objetivos = [NSString pCharToString:_descripcion];
            
            // promocionado
            p.promocionado = YES;
            if (p.ordenIngreso == 0)
                p.promocionado = NO;
            //p.promocionado = sqlite3_column_int(statement, 14);
            
            p.aPromocionar = NO;
            
            [promos addObject:p];
        }
        
	} else
    {
        NSAssert(0, @"No se pudo ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return promos;
    
}

+(void)deleteForMedico:(NSUInteger)medico visita:(NSUInteger)visita
{
    NSString *nodo = SettingForKey(NODO, @"");
    
    // Borro los registros realmente
    NSString *sql = @"DELETE FROM med_promociones WHERE nodo = %@ AND medico = %d AND visitador = %@ AND visita = %d";
    NSString *query = [NSString stringWithFormat:sql, nodo, medico, nodo, visita];
    
#ifdef DEBUG_TRAZABILIDAD
    NSLog(@" - Promocion: deleteForMedico - ");
    NSLog(@"- Query: %@", query);
#endif
    
    [[DB getInstance] excecuteSQL:query];
}

+(void)updateProductosAPromocionar:(NSUInteger)medico producto:(NSUInteger)producto cantidad:(NSUInteger)cantidad lit:(NSUInteger)lit obsequio:(NSUInteger)obsequio objetivos:(NSString *)objetivos
{
    NSString *nodo = SettingForKey(NODO, @"");
    
    // Borro los registros realmente
    NSString *sql =
    @"UPDATE med_apromocionar "
    "SET cantidad = %d, "
        "literatura = %@, "
        "obsequio = %@, "
        "objetivos = %@ "
    "WHERE nodo = %@ AND medico = %d AND producto = %d";
    
    NSString *query = [NSString stringWithFormat:sql,
                       cantidad,
                       (lit == 0 ? @"0" : @"1"),
                       (obsequio == 0 ? @"0" : @"1"),
                       [NSString stringQuoted:objetivos ifNull:@"''"] ,
                       nodo,
                       medico,
                       producto];
    
    [[DB getInstance] excecuteSQL:query];
}

+(BOOL)existeRegistroAPromocionar:(NSUInteger)medico producto:(NSUInteger)producto lote:(NSUInteger)lote
{
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *sql =
    @"SELECT nodo "
    "FROM med_apromocionar "
    "WHERE nodo = %@ AND medico = %d AND producto = %d";
    //"WHERE nodo = %@ AND medico = %d AND producto = %d and lote = %d";
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       medico,
                       producto,
                       lote];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    
    BOOL result = NO;
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            result = YES;
        }
    }
    sqlite3_finalize(statement);
    
    return result;
}

+(NSString *)getForNovedad:(NSUInteger) medico visita:(NSUInteger)visita orden:(NSUInteger)orden producto:(NSUInteger)producto
{
    NSString *nodo = SettingForKey(NODO, @"");
   
    
    NSString *query = [NSString stringWithFormat:
                       @"SELECT "
                            "nodo || \"|\" || "
                            "medico || \"|\" ||"
                            "visitador|| \"|\" || "
                            "visita || \"|\" || "
                            "ifnull(orden,\"\") || \"|\" || "
                            "ifnull(producto,\"\") || \"|\" || "
                            "ifnull(muestras, \"\") || \"|\" || "
                            "case when literatura IS NULL then '' "
                                 "when literatura is 0 then 'N' "
                                 "when literatura is 1 then 'S' "
                                "else literatura end || \"|\" || "
                            "case when obsequio IS NULL then '' "
                                 "when obsequio is 0 then 'N' "
                                 "when obsequio is 1 then 'S' "
                                "else obsequio end|| \"|\" || "
                            "ifnull(evaluacion,\"\") || \"|\" || "
                            "ifnull(lote,\"\") as novedad "
                       "FROM med_promociones "
                       "WHERE med_promociones.nodo = %@ AND "
                             "med_promociones.medico = %d AND "
                             "med_promociones.visitador = %@ AND "
                             "med_promociones.visita = %d AND "
                             "med_promociones.orden = %d AND "
                             "med_promociones.producto = %d",
                        nodo,
                        medico,
                        nodo,
                        visita,
                        orden,
                        producto];
    
#ifdef DEBUG_TRAZABILIDAD
    NSLog(@" - Promocion: gerForNovedad - ");
    NSLog(@"- query: %@",query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    char *_c;
    NSString *result = @"";
	if ( statement )
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
            _c = (char *)sqlite3_column_text(statement, 0);
            result = [NSString pCharToString:_c];
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
}


#pragma mark -
#pragma mark Object methods

- (id)copy{
    
    Promocion *o = [[Promocion alloc] init];
    
    o.nodo = self.nodo;
    o.medico = self.medico;
    o.visita = self.visita;
    o.visitador = self.visitador;
    o.orden = self.orden;
    o.ordenIngreso = self.ordenIngreso;
    o.producto = self.producto;
    o.muestras = self.muestras;
    o.obsequio = self.obsequio;
    o.literatura = self.literatura;
    o.lote = self.lote;
    o.stock = self.stock;
    
    o.evaluacion = [NSString string:self.evaluacion ifNull:@""];
    
    o.descripcionLote = [NSString string:self.descripcionLote ifNull:@""];
    o.descripcionProducto = [NSString string:self.descripcionProducto ifNull:@""];
    o.codigoLote = [NSString string:self.codigoLote ifNull:@""];
    o.fechaVto = [self.fechaVto copy];
    
    o.promocionado = self.promocionado;
    
    return o;
}

-(void)save
{
    NSString *sql =
       @"INSERT INTO med_promociones "
           "(nodo, "
            "medico, "
            "visitador, "
            "visita, "
            "orden, "
            "producto, "
            "muestras, "
            "literatura, "
            "obsequio, "
            "evaluacion, "
            "lote) "
        "VALUES (%@, %d, %@, %d, %d, %d, %d, %d, %d, %@, %@)";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       self.medico,
                       nodo,
                       self.visita,
                       self.ordenIngreso,
                       self.producto,
                       self.muestras,
                       self.literatura,
                       self.obsequio,
                       [NSString stringQuoted:self.evaluacion ifNull:@"''"],
                       [NSString stringQuoted:[NSString intToStr:self.lote ifZero:@""] ifNull:@"''"]];
    
#ifdef DEBUG_TRAZABILIDAD
    NSLog(@" - Promocion: save - ");
    NSLog(@"- Query: %@", query);
#endif
    
    [[DB getInstance] excecuteSQL:query];
}

- (BOOL)isInArray:(NSArray*)listado
{
    if ([listado count] > 0)
    {
        if (![[listado objectAtIndex:0] isKindOfClass:[Promocion class]])
            return NO;
        
        for (Promocion *p in listado)
        {
            //            @property(nonatomic) NSUInteger nodo;
            //            @property(nonatomic) NSUInteger medico;
            //            @property(nonatomic) NSUInteger visitador;
            //            @property(nonatomic) NSUInteger visita;
            //            @property(nonatomic) NSUInteger orden;
            //            @property(nonatomic) NSUInteger producto;
            //            @property(nonatomic) NSUInteger muestras;
            //            @property(nonatomic) NSUInteger obsequio;
            //            @property(nonatomic) NSUInteger literatura;
            //            @property(nonatomic) NSUInteger lote;
            
            if ((p.nodo == self.nodo) && (p.nodo == self.medico) && (p.nodo == self.visita) &&
                (p.nodo == self.producto) && (p.nodo == self.lote))
                return YES;
        }
        
    }
    
    return NO;
}

- (NSString*)getForNovedad
{
    NSString *result= @"";
    NSString *nodo = SettingForKey(NODO, @"");
    
    result = [NSString stringWithFormat:@"%@|%d|%@|%d|%d|%d|%d|%@|%@|%@|%d",
              nodo,
              self.medico,
              nodo,
              self.visita,
              self.ordenIngreso,
              self.producto,
              self.muestras,
              self.obsequio == 0 ? @"N": @"S",
              self.literatura == 0 ? @"N": @"S",
              [NSString string:self.evaluacion],
              self.lote];
    
    return result;
}

- (NSString*)getForNovedadAPromocionar
{
    NSString *result= @"";
    NSString *nodo = SettingForKey(NODO, @"");
    
    result = [NSString stringWithFormat:@"%@|%d|%d|%d|%d|%@|%@|%@|%d",
              nodo,
              self.medico,
              self.orden,
              self.producto,
              self.muestras,
              (self.obsequio == 1 ? @"S" : @"N"),
              (self.literatura == 1 ? @"S" : @"N"),
              [NSString string:self.objetivos],
              0];
    
    return result;
}

- (void)updateProductosAPromocionar
{
    NSString *nodo = SettingForKey(NODO, @"");

    NSString *sql =
    @"UPDATE med_apromocionar "
    "SET cantidad = %d, "
        "literatura = %@, "
        "obsequio = %@, "
        "objetivos = %@ "
    "WHERE nodo = %@ AND medico = %d AND producto = %d";
    
    NSString *query = [NSString stringWithFormat:sql,
                       self.muestras,
                       (self.literatura == 0 ? @"0" : @"1"),
                       (self.obsequio == 0 ? @"0" : @"1"),
                       [NSString stringQuoted:self.objetivos ifNull:@"''"],
                       nodo,
                       self.medico,
                       self.producto];
    
    [[DB getInstance] excecuteSQL:query];
}


@end
