//
//  AjusteMMViewController.m
//  APM4
//
//  Created by Laura Busnahe on 3/27/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "AjusteMMViewController.h"
#import "customAjusteMMCell.h"
#import "MTMuestrasMedicas.h"
#import "NSDateExtended.h"
#import "popListView.h"
#import "DBExtended.h"

#import "GlobalDefines.h"



#pragma mark -
@implementation AjusteMMViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Ajuste MM";
        
        self.popMenu = nil;
        
        self.headerNIB  = [UINib nibWithNibName:@"customAjusteMMHeader" bundle:nil];
        [self.headerNIB instantiateWithOwner:self options:nil];
    }
    
    [[DB getInstance] getDBVersion];
    
    return self;
}

- (id)initViewDefaultNib
{
    self = [self initWithNibName:@"AjusteMMViewController" bundle:nil];
    
    return self;
}

//- (id)initViewDefaultNibForSelection:(MTProducto*)selProducto
//{
//
//}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //self.table.rowHeight = 44.0;
    self.cellNIB         = [UINib nibWithNibName:@"customAjusteMMCell" bundle:nil];
    
    self.seleccion = nil;
    self.ajusteSel = nil;
    
    [self.popupView setHidden:YES];
    [self.popupShade setHidden:YES];
    
    self.data      = [MTMuestrasMedicas GetAll];
    self.dataFiltered = [[NSMutableArray alloc] initWithArray:self.data];
    
    ajustesArray   = [MTAjustes getAll];
}


#pragma mark -
#pragma mark Life time methods

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)getTitleForHeader{
    return @"Ajuste de Muestras Médicas";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"ficha_48"];
}

-(void)setPopupHiddenAnimated:(BOOL)hide
{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
     
                     animations:^
                    {
                        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                        if (hide)
                        {
                            self.popupShade.alpha=0;
                            self.popupView.alpha=0;
                        } else
                        {
                            self.popupShade.hidden= NO;
                            self.popupShade.alpha= 1;
                            self.popupView.hidden= NO;
                            self.popupView.alpha= 1;
                        }
                    }
     
                     completion:^(BOOL finished)
                    {
                        //NSLog(@"Animation completed");
                        if (hide)
                        {
                            self.popupShade.hidden= YES;
                            self.popupView.hidden= YES;
                        }
                    }
     ];
}


#pragma mark -
#pragma mark Edit View methods
- (void)showEditView:(BOOL)show
{
    if (show)
    {
        // Si se va a mostrar la vista de edición, limpio el contenido de los campos y completo el nombre del producto.
        self.lblDescProducto.text = @"";
        self.lblLote.text = @"Lote:";
        self.lblStock.text = @"";
        self.edtMotivo.text   = @"";
        self.edtCantidad.text = @"";
        if (self.seleccion)
        {
            self.lblDescProducto.text = [NSString  stringWithFormat:@"%@", self.seleccion.descripcion];
            if ([self.seleccion.lote length] > 0)
                self.lblLote.text = [NSString  stringWithFormat:@"Lote: %@", self.seleccion.lote];
            
            self.lblStock.text = [NSString  stringWithFormat:@"Stock actual: %i", self.seleccion.stock];
            
            self.ajusteSel =  nil;
        }
    } else
    {
        //[self.edtCantidad resignFirstResponder];
    }
    
    [self setPopupHiddenAnimated:!show];
}


#pragma mark -
#pragma mark IBAction methods
- (IBAction)btnAceptar:(id)sender
{
    [self tapOther:sender];
    
    // Chequeo que se haya seleccionado un Tipo de Ajuste.
    if (!(self.ajusteSel.recID > 0))
    {
        [self showAlert:@"Debe seleccionar un motivo de ajuste." Titulo:@"Atención"];
        return;
    }
    
    // Si es un ajuste de decremento, chequeo que haya stock.
    if ((self.seleccion.stock == 0) && (self.ajusteSel.operacion < 0))
    {
        [self showAlert:@"No puede realizarse una operación de ajuste negaivo sobre un producto sin stock." Titulo:@"Atención"];
        return;
    }
    
    // Chequeo que se haya completado la Cantidad.
    self.ajusteSel.cantidad = [self.edtCantidad.text integerValue];
    if (self.ajusteSel.cantidad == 0)
    {
        [self showAlert:@"Debe especificar la cantidad del ajuste." Titulo:@"Atención"];
        return;
    }
    
    // Si es un ajuste de decremento, chequeo que la cantidad ingresada no sea mayor que el stock.
    if ((self.ajusteSel.cantidad > self.seleccion.stock) && (self.ajusteSel.operacion < 0))
    {
        [self showAlert:@"La cantidad del ajuste puede ser menor a la cantidad de productos en stock." Titulo:@"Atención"];
        return;
    }
    
    [self showEditView:NO];
    
    
#ifdef DEBUG_AJUSTEMM
    if (DEBUG_AJUSTEMM >= 1)
    {
        NSLog(@"--- Ajuste MM ---");
        NSLog(@"Producto: %@", self.seleccion.descripcion);
        NSLog(@"Motivo: %@", self.ajusteSel.descripcion);
        NSLog(@"Operación: %i", self.ajusteSel.operacion);
        NSLog(@"Cantidad: %i", self.ajusteSel.cantidad);
    }
#endif
    
    
    self.ajusteSel.date = [NSDate date];            // Fecha y hora en que se realizó el ajuste.
    [self.seleccion ajusteStock:self.ajusteSel];    // Guardo el Ajuste de Stock realizado.
    
    [self.table reloadData];                        // Actualizo las celdas de la tabla de productos.
    
}

- (IBAction)btnCancelar:(id)sender
{
    [self tapOther:sender];
    [self showEditView:NO];
}

- (IBAction)tapMotivo:(id)sender
{
    [self tapOther:sender];
    
    // Obtengo la vista desde la cual se llama al popover.
    UIView* popRectView = (UIView*)self.btnMotivo;
    CGSize popOverSize = CGSizeMake(350, 452);
    
    // Creo el popover del menú, si no fue creado.
    if (!self.popMenu)
    {
        self.popMenu = [[MTpopListView alloc] initWithArray:ajustesArray Rect:CGRectMake(0, 0, popOverSize.width, popOverSize.height)];
        self.popMenu.delegate = self;
    }
    [self.popMenu dismissPopoverAnimated:NO];
    
    [self.popMenu setTitleAndParameters:@"Motivo de ajuste" rowData:ajustesArray];
    
    [self.popMenu setSize:popOverSize];
    [self.popMenu useSearch:FALSE];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x,
                               popRectView.frame.origin.y, //+ 64,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    [self.popMenu presentPopoverFromRectWithSender:popSourceRect
                                            Sender:(id)self.edtMotivo
                                            inView:self.popupView
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:YES];
}

- (IBAction)tapOther:(id)sender
{
    [self.edtCantidad resignFirstResponder];
}


#pragma mark-
#pragma mark Table Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of time zone names.
	return [self.dataFiltered count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MyIdentifier = @"AjusteMMdentifier";

    // Obtain the cell object.
	customAjusteMMCell *cell = (customAjusteMMCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	// If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNIB instantiateWithOwner:self options:nil];
		cell = self.tmpCell;
		self.tmpCell = nil;
    }
    
    MTMuestrasMedicas *o = [self.dataFiltered objectAtIndex:indexPath.row];
    
    UIColor *textColor = [UIColor blackColor];
    
    // Chequeo si el producto está vencido.
    if (o.vencido)
        textColor = [UIColor redColor];
    
    cell.lblProducto.text = o.descripcion;
    cell.lblProducto.textColor = textColor;
    cell.lblCantidad.text = [NSString stringWithFormat:@"%d", o.stock];
    cell.lblCantidad.textColor = textColor;
    cell.lblCodigo.text = o.codigo;
    cell.lblCodigo.textColor = textColor;
    cell.lblLote.text = o.lote;
    cell.lblLote.textColor = textColor;
    
    cell.lblFecha.text = [o.fecha_vto formattedStringUsingFormat:@"MM/yyyy"];
    cell.lblFecha.textColor = textColor;
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchBar endEditing:true];
    
    self.seleccion = [self.dataFiltered objectAtIndex:indexPath.row];
    
    [self showEditView:YES];
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Tabla de totales
    if (tableView == self.table)
        return self.tableHeader;
    else
        return nil;
}



#pragma mark-
#pragma mark UIAlertView related methods

- (void)showAlert:(NSString*)mensaje Titulo:(NSString*) titulo
{
    // Llama al AlertView de confirmación antes de eliminar el previsto seleccionado.
   	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titulo
                                                    message:mensaje
                                                   delegate:nil
                                          cancelButtonTitle:@"Aceptar"
                                          otherButtonTitles:nil];
	[alert show];
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // buttonIndex = 0 : No
    
    // buttonIndex = 1 : Si
}

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
- (void)alertViewCancel:(UIAlertView *)alertView
{
    
}



#pragma mark-
#pragma mark popListView related methods

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender didSelectRow:(NSInteger *)MenuIndex selectedText:(NSString *)RowText
{
    UITextField *textField = (UITextField *)sender;
    textField.text = [NSString stringWithFormat:@"%@", RowText];
       
    self.ajusteSel = [ajustesArray objectAtIndex:MenuIndex];
}



#pragma mark -
#pragma mark TextField methods

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField          // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
{
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [self.edtCantidad resignFirstResponder];
}

- (BOOL)textFieldOLD:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string   // return NO to not change text
{
    if ([string length] == 0 && range.length > 0)
    {
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return NO;
    }
    
    if (textField == self.edtCantidad)
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        if ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0)return YES;
        
        return NO;
    } else
        return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    NSString *oldString = [NSString string:textField.text ifNull:@""];
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    if (textField == self.edtCantidad)
    {
        notAllowedSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    }
    
    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
                            stringByTrimmingCharactersInSet:notAllowedSet];
    
    if ([oldString isEqualToString:tempString])
        return NO;
    
    return   YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.edtCantidad endEditing:YES];
    return NO;
}



#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.dataFiltered removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    if ((searchText != nil) && ([searchText length] > 0))
    {
        if (self.data.count > 0)
        {
            // En caso de no ser una entidad, compruebo si responde al selector getDescription
            if ([[self.data objectAtIndex:0] respondsToSelector:@selector(getDescription)])
            {
                for (id item in self.data)
                {
                    NSRange range = [[item getDescription] rangeOfString:searchText
                                                                 options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                    
                    if ( range.length > 0)
                    {
                        [self.dataFiltered addObject:item];
                    }
                }
            } else
            {
                // Para los objetos de tipo NSString.
                for (NSString *item in self.data)
                {
                    NSRange range = [item rangeOfString:searchText
                                                options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                    
                    if ( range.length > 0)
                    {
                        [self.dataFiltered addObject:item];
                    }
                }
            }
        }
    } else
    {
        // Si no hay texto de búsqueda, copio todos los elementos.
        for (id item in self.data)
            [self.dataFiltered addObject:item];
    }
    
    
    [self.table reloadData];
}



#pragma mark -
#pragma mark Search bar delegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)aSearchBar
{
    
}


- (void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar {
    
    // If the user finishes editing text in the search bar by, for example:
    // tapping away rather than selecting from the recents list, then just dismiss the popover
    //
    
    // dismiss the popover, but only if it's confirm UIActionSheet is not open
    //  (UIActionSheets can take away first responder from the search bar when first opened)
    //
    // the popover's main view controller is a UINavigationController; so we need to inspect it's top view controller
    //
    [aSearchBar resignFirstResponder];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    // When the search string changes, filter the recents list accordingly.
    [self filterContentForSearchText:searchText scope:@""];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar endEditing:YES];
    //[searchBar resignFirstResponder];
}

#pragma mark -



@end
