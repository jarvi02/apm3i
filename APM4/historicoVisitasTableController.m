//
//  historicoVisitasTableControllerViewController.m
//  APM4
//
//  Created by Laura Busnahe on 7/15/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "historicoVisitasTableController.h"
#import "historicoVisitaCell.h"
#import "historicoVisitaHeader.h"
#import "MTHistoricoVisitas.h"

@interface historicoVisitasTableController ()

@end

@implementation historicoVisitasTableController

- (id)initWithData:(NSArray*)aData
{
    self = [super init];
    
    if (self)
    {
        self.cellNib    = [UINib nibWithNibName:@"historicoVisitaCell" bundle:nil];
        
        self.headerNib  = [UINib nibWithNibName:@"historicoVisitaHeader" bundle:nil];
        [self.headerNib instantiateWithOwner:self options:nil];
        
        self.data       = aData;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"historicoCelldentifier";
    
    // Obtain the cell object.
    historicoVisitaCell *cell = (historicoVisitaCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
    	cell = self.tmpCell;
    	self.tmpCell = nil;
    }
    
    // Obtengo los datos para la celada.
    MTHistoricoVisitas *o = [self.data objectAtIndex:indexPath.row];
    
    // Celdas de Orden
    cell.txt01.text = [NSString string:o.ciclo01 ifNull:@""];
    cell.txt02.text = [NSString string:o.ciclo02 ifNull:@""];
    cell.txt03.text = [NSString string:o.ciclo03 ifNull:@""];
    cell.txt04.text = [NSString string:o.ciclo04 ifNull:@""];
    cell.txt05.text = [NSString string:o.ciclo05 ifNull:@""];
    cell.txt06.text = [NSString string:o.ciclo06 ifNull:@""];
    cell.txt07.text = [NSString string:o.ciclo07 ifNull:@""];
    cell.txt08.text = [NSString string:o.ciclo08 ifNull:@""];
    cell.txt09.text = [NSString string:o.ciclo09 ifNull:@""];
    cell.txt10.text = [NSString string:o.ciclo10 ifNull:@""];
    cell.txt11.text = [NSString string:o.ciclo11 ifNull:@""];
    cell.txt12.text = [NSString string:o.ciclo12 ifNull:@""];
    
    // Celdas de Producto
    cell.txtTipo.text  = [NSString string:o.tipo ifNull:@""];
    cell.txtTotal.text = [NSString string:o.total ifNull:@""];
    
    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader;
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader.frame.size.height;
    else
        return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
