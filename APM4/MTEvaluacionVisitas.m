//
//  MTEvaluacionVisitas.m
//  APM4
//
//  Created by Laura Busnahe on 9/6/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTEvaluacionVisitas.h"
#import "Config.h"
#import "DB.h"

@implementation MTEvaluacionVisitas

- (id)init
{
    self = [super init];
    if (self)
    {
        self.idVisita = 0;
        self.fecha = [NSDate dateWithTimeIntervalSince1970:0];
        self.evaluacion = @"";
    }
    
    return self;
}
    
+ (NSArray*)getAllForMedico:(NSUInteger)idMedico producto:(NSUInteger)idProducto
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql =
       @"SELECT "
            "med_visitas.id, "
            "med_visitas.fecha, "
            "ifnull(med_promociones.evaluacion, '') as evaluacion "
        "FROM med_promociones "
        "INNER JOIN med_visitas ON (med_visitas.nodo = med_promociones.nodo AND "
                                   "med_visitas.medico = med_promociones.medico AND "
                                   "med_visitas.visitador = med_promociones.visitador AND "
                                   "med_visitas.id = med_promociones.visita) "
        "WHERE med_promociones.nodo = %@ AND "
              "med_promociones.medico = %d AND "
              "med_promociones.visitador = %@ AND "
              "med_promociones.producto = %d "
        "ORDER BY fecha ";
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       idMedico,
                       nodo,
                       idProducto];

#ifdef DEBUG_EVALUACIONPRODUCTOS
    
    NSLog(@"--- MTEvaluacionVisitas -> getAllForMedico:producto: ---");
    NSLog(@" - query: %@", query);
    
#endif
    
    
    char *_c;
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTEvaluacionVisitas *o = [[MTEvaluacionVisitas alloc] init];
            
            // idProducto
            o.idVisita = sqlite3_column_int(statement, 0);
            
            // Fecha
            o.fecha = [[NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 1)] dateAsDateWithoutTime];
            
            // Evaluación
            _c = (char *)sqlite3_column_text(statement, 2);
            o.evaluacion = [NSString pCharToString:_c];
            
            [result addObject:o];
        }
        
	} else
    {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
    
}


@end
