//
//  MTRegionalDictionary.m
//  APM4
//
//  Created by Laura Busnahe on 8/5/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTRegionalDictionary.h"

@implementation MTRegionalDictionary
{
    NSMutableDictionary *dictionary;
}

//static MTRegionalDictionary *_sharedInstance = nil;

+(MTRegionalDictionary *)sharedInstance
{
    static MTRegionalDictionary *instance;
	
	if(instance == nil){
		instance = [MTRegionalDictionary new];
	}
	return instance;
}

-(id) init {
	
    self = [super init];
	if ( self )
    {
        dictionary = [[NSMutableDictionary alloc] init];
        [self load];
	}
	return self;
}

#pragma mark -
#pragma mark Dictionaries methods
- (void)load
{
    [self loadChile];
    [self loadUruguay];
}


#pragma mark Diccionario Chile
- (void)loadChile
{
    NSString *dictionaryName = @"es_CL";
    NSMutableDictionary *tmpDictionary = [[NSMutableDictionary alloc] init];
    
    // Agrego las palabras
    [tmpDictionary setValue:@"RUT"               forKey:@"C.U.I.T."];
    [tmpDictionary setValue:@"RUT"               forKey:@"CUIT"];
    [tmpDictionary setValue:@"RUT"               forKey:@"Matricula Nacional"];
    [tmpDictionary setValue:@"RUT"               forKey:@"M. Nacional"];
    [tmpDictionary setValue:@"Sistema de Salud"  forKey:@"Obra Social"];
    [tmpDictionary setValue:@"Sistemas de Salud" forKey:@"Obras Sociales"];
    [tmpDictionary setValue:@"S. Salud"          forKey:@"O. Social"];
    [tmpDictionary setValue:@"Región"            forKey:@"Provincia"];
    [tmpDictionary setValue:@"Regiones"          forKey:@"Provincias"];
    [tmpDictionary setValue:@"Comuna"            forKey:@"Localidad"];
    [tmpDictionary setValue:@"Comunas"           forKey:@"Localidades"];
    
    [tmpDictionary setValue:@"Debe ingresar al menos un sistema de salud"
                     forKey:@"Debe ingresar al menos una obra social"];
    
    [tmpDictionary setValue:@"Debe seleccionar la región del domicilio"
                     forKey:@"Debe seleccionar la provincia del domicilio"];
    
    [tmpDictionary setValue:@"Debe seleccionar la localidad del domicilio"
                     forKey:@"Debe seleccionar la localidad del domicilio"];
    
    [tmpDictionary setValue:@"Debe ingresar el RUT del médico"
                     forKey:@"Debe ingresar al menos una matrícula del médico"];
    
    [tmpDictionary setValue:@"Ya existe el RUT ingresado en otro médico"
                     forKey:@"Ya existe el N° de Matrícula Nacional en otro médico"];
    
    [dictionary setValue:tmpDictionary forKey:dictionaryName];
}


#pragma mark Diccionario Uruguay
- (void)loadUruguay
{
    NSString *dictionaryName = @"es_UY";
    NSMutableDictionary *tmpDictionary = [[NSMutableDictionary alloc] init];
    
    // Agrego las palabras
    [tmpDictionary setValue:@"RUT"              forKey:@"C.U.I.T."];
    [tmpDictionary setValue:@"RUT"              forKey:@"CUIT"];
    [tmpDictionary setValue:@"Caja Profesional" forKey:@"Matricula Nacional"];
    [tmpDictionary setValue:@"C. Profesional"   forKey:@"M. Nacional"];
    [tmpDictionary setValue:@"Departamento"     forKey:@"Provincia"];
    [tmpDictionary setValue:@"Departamentos"    forKey:@"Provincias"];
    [tmpDictionary setValue:@"Sistema de Salud"  forKey:@"Obra Social"];
    [tmpDictionary setValue:@"Sistemas de Salud" forKey:@"Obras Sociales"];
    [tmpDictionary setValue:@"S. Salud"          forKey:@"O. Social"];
    
    [tmpDictionary setValue:@"Debe ingresar al menos un sistema de salud"
                     forKey:@"Debe ingresar al menos una obra social"];
    
    [tmpDictionary setValue:@"Debe seleccionar el departamento del domicilio"
                     forKey:@"Debe seleccionar la provincia del domicilio"];
    
    [tmpDictionary setValue:@"Debe ingresar la caja profesional del médico"
                     forKey:@"Debe ingresar al menos una matrícula del médico"];
    
    [tmpDictionary setValue:@"Ya existe la caja profesional en otro médico"
                     forKey:@"Ya existe el N° de Matrícula Nacional en otro médico"];
    
    [dictionary setValue:tmpDictionary forKey:dictionaryName];
}



#pragma mark -
#pragma mark Public methods

- (NSString*)valueForText:(NSString*)texto
{
    NSString *result = texto;
    NSString * dictionaryName;
    
    // Obtengo la configuración de región.
    dictionaryName = [[NSLocale currentLocale] localeIdentifier];
    
    // Chequeo si exite el diccionario.
    NSMutableDictionary *currentDictionary = [dictionary valueForKey:dictionaryName];
    if (currentDictionary)
    {
        // Cheque si existe la palabra en el diccionario.
        if ([currentDictionary valueForKey:texto])
        {
            result = [currentDictionary valueForKey:texto];
        }
    }
    
    return result;
}

-(BOOL)hideMatriculaNacional
{
    BOOL result = NO;
    
    if ([[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_CL"])
    {
        return YES;
    }
    
    return result;
}

-(BOOL)hideMatriculaProvincial
{
    BOOL result = NO;
    
    if ([[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_CL"])
    {
        return YES;
    }
    
    if ([[[NSLocale currentLocale] localeIdentifier] isEqualToString:@"es_UY"])
    {
        return YES;
    }
    
    return result;
}

-(BOOL)hideCUIT
{
    BOOL result = NO;
    
    return result;
}

@end
