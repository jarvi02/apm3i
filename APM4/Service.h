//
//  Service.h
//  WebGisMobile
//
//  Created by Develaris on 23/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequestDelegate.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "Localized.h"

//#define URL_BASE      @"http://190.16.128.10:89/scripts/"

//#define URL_BASE      @"http://186.153.135.102:89/scripts/"

//#define URL_BASE      @"http://192.168.0.99/scripts/"

#define URL_BASE      @"http://186.153.5.126/scripts/"

@protocol ServiceDelegate <NSObject>

@required
- (void)requestFinished;
- (void)requestFailed:(NSString*) msg;

@end


@interface Service : NSObject <ASIHTTPRequestDelegate>

@property(nonatomic, assign) id<ServiceDelegate>    delegate;
@property(nonatomic, retain) ASIHTTPRequest         *request;

- (void) logUrl:(NSString*)str;
- (void) logResponse:(NSString*)str;
- (NSString*)textoParaLog;
- (void) cancel;

@end
