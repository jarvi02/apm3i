//
//  MTstdNoVisita.m
//  APM4
//
//  Created by Laura Busnahe on 9/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTstdNoVisita.h"
#import "Config.h"
#import "DB.h"

@implementation MTstdNoVisita

- (id)init
{
    self = [super init];
    if (self)
    {
        self.motivo  = @"";
        self.maniana = 0;
        self.tarde   = 0;
        self.total   = 0;
    }
    
    return self;
}

+ (NSArray*)getAll
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql =
    @"SELECT 0 as _id, "
        "descestado, "
        "SUM(case "
        "       when  actividad = 'M' then 1 else 0 "
        "	 end ) as M, "
        "SUM(case "
        "       when  actividad = 'T' then 1 else 0 "
        "	end) as T, "
        "COUNT(descestado) as total "
    "FROM med_medicos "
    "WHERE med_medicos.b = 0 AND "
        "med_medicos.asignacion = 1 AND "
        "estado > 1 AND "
        "med_medicos.nodo = %@ "
    "GROUP BY descestado";
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql,
                                                nodo];
    
#ifdef DEBUG_STDNOVISITA
    NSLog(@"--- MTstdNoVisita ---");
    NSLog(@" - query: %@", query);
    
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        NSInteger totalManiana = 0;
        NSInteger totalTarde   = 0;
        NSInteger totales      = 0;
        
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTstdNoVisita *o = [[MTstdNoVisita alloc] init];
            
            // Motivo
            _c = (char *)sqlite3_column_text(statement, 1);
            o.motivo = [NSString pCharToString:_c];
            
            // Mañana
            o.maniana = sqlite3_column_int(statement, 2);
            
            // Tarde
            o.tarde = sqlite3_column_int(statement, 3);
            
            // Total
            o.total = sqlite3_column_int(statement, 4);
            
            // Sumo los valores para los totales.
            totalManiana += o.maniana;
            totalTarde   += o.tarde;
            totales      += o.total;
            
            [result addObject:o];
        }

        // Creo un objeto más con id 0 que contiene los totales
        MTstdNoVisita *o = [[MTstdNoVisita alloc] init];
        o.motivo  = @"Totales";
        o.maniana = totalManiana;
        o.tarde   = totalTarde;
        o.total   = totales;
        
        // Agrego los totales al resultaod.
        [result addObject:o];

    } else
    {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}
@end
