//
//  MTGrupos.m
//  APM4
//
//  Created by Laura Busnahe on 6/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTGrupos.h"
#import "Novedad.h"

#import "GlobalDefines.h"
#import "Config.h"
#import "DBExtended.h"

#import "NSStringExtended.h"

@implementation MTGrupos


- (id)init
{
    self = [super init];
    if (self)
    {
        self.recID       = 0;
        self.descripcion = @"";
        self.action      = kMTEANone;
        self.blocked     = NO;
    }
    
    return self;
}

- (MTGrupos*)copyWithZone:(NSZone*)zone
{
    MTGrupos* newCopy;
    
    newCopy = [[MTGrupos alloc] init];
    
//    @property (nonatomic, assign) NSUInteger        recID;
//    @property (nonatomic, strong) NSString          *descripcion;
//    @property (nonatomic, assign) MTEditActionsType action;

    newCopy.recID       = self.recID;
    newCopy.descripcion = [NSString string:self.descripcion ifNull:@""];
    newCopy.action      = self.action;
    
    return newCopy;
}


- (NSString*)getDescription
{
    NSString *result = [NSString string:self.descripcion ifNull:@"-"];
    
    return result;
}


#pragma mark -
#pragma mark DB Grupos methods

- (BOOL)existeGrupo:(NSString*)nombreGrupo
{
    BOOL result = NO;
    
    NSString *sql = @"SELECT id, descripcion "
                     "FROM gst_grupos "
                     "WHERE (nodo = %@ AND lower(descripcion) = lower(%@)) "
                     "ORDER BY descripcion;";
    
    NSString *nodo = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql, nodo, [NSString stringQuoted:nombreGrupo ifNull:@"''"]];

#ifdef DEBUG_GRUPOS
    NSLog(@"- query existeGrupo: %@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if (statement)
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
            result = YES;
	} else
    {
        NSAssert(0, @"No se puede ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}

- (NSString*)isOkToSave
{
    NSString *result = @"";
    
    NSString *_s = [NSString string:self.descripcion ifNull:@""];
    self.descripcion = _s;
    
    if ([_s length] < 2)
    {
        return @"El nombre del grupo debe tener al menos 2 letras.";
    }
    
    if ([self existeGrupo:_s])
    {
        return @"Ya existe un grupo con el nombre ingresado.";
    }
    
    
    return result;
}

- (void)saveAllData
{
    NSString *nodo = SettingForKey(NODO, @"0");
    // Obtengo la nueva ID para el grupo.
    NSInteger newID = [[DB getInstance]
                            getNewIDForTable:@"gst_grupos"
                                 idNameOrNil:@"id"
                                  whereOrNil:[NSString stringWithFormat:@"nodo = %@", nodo]];
    self.recID = newID;
    
    // Inserto el nuevo grupo en la base de datos.
    NSString *sql = @"INSERT INTO gst_grupos (nodo, id, descripcion)"
                     "VALUES (%@, %@, %@);";

    NSString *query = [NSString stringWithFormat:sql,
                            nodo,
                            [NSString intToStr:newID],
                            [NSString stringQuoted:[self.descripcion uppercaseString] ifNull:@"''"]];

#ifdef DEBUG_GRUPOS
    NSLog(@"- query saveGrupo: %@", query);
#endif
    
    [[DB getInstance] excecuteSQL:query];
    
    // Grabo la novedad
    NSString *novedadParam = [NSString stringWithFormat:@"%@|%@|%@",
                              nodo,
                              [NSString intToStr:newID],
                              [self.descripcion uppercaseString]];

#ifdef DEBUG_GRUPOS
    NSLog(@"- novedad saveGrupo: %@", novedadParam);
#endif

    [Novedad insert:@"gst_grupos" novedad:novedadParam tipo:kInsert];
 
    self.action = kMTEANone;
}

- (void)updateAllData
{
    NSString *nodo = SettingForKey(NODO, @"0");
    
    // Inserto el nuevo grupo en la base de datos.
    NSString *sql = @"UPDATE gst_grupos "
                     "SET descripcion = %@ "
                     "WHERE (nodo = %@ AND id = %@)";
    
    NSString *query = [NSString stringWithFormat:sql,
                       [NSString stringQuoted:[self.descripcion uppercaseString] ifNull:@"''"],
                       nodo,
                       [NSString intToStr:self.recID]];
    
#ifdef DEBUG_GRUPOS
    NSLog(@"- query updateGrupo: %@", query);
#endif
    
    [[DB getInstance] excecuteSQL:query];
    
    // Grabo la novedad
    NSString *novedadParam = [NSString stringWithFormat:@"%@|%@|%@",
                              nodo,
                              [NSString intToStr:self.recID],
                              [self.descripcion uppercaseString]];
    
#ifdef DEBUG_GRUPOS
    NSLog(@"- novedad updateGrupo: %@", novedadParam);
#endif
    
    [Novedad insert:@"gst_grupos" novedad:novedadParam tipo:kUpdate];
    
    self.action = kMTEANone;
}

- (void)deleteAllData
{
    // Borro el grupo a todos los médicos asginados.
    [self deleteGrupoForAllMedico];
    
    // Borro el grupo - Novedad
    NSString *nodo = SettingForKey(NODO, @"0");
    
    NSString *novedadParam = [NSString stringWithFormat:@"%@|%@|%@",
                              nodo,
                              [NSString intToStr:self.recID],
                              [self.descripcion uppercaseString]];
    
#ifdef DEBUG_GRUPOS
    NSLog(@"- novedad deleteGrupo: %@", novedadParam);
#endif
    
    [Novedad insert:@"gst_grupos" novedad:novedadParam tipo:kDelete];
    
    // Borro el grupo de la base de datos.
    NSString *sql = @"DELETE FROM gst_grupos "
                     "WHERE (nodo = %@ AND id = %@)";
    
    NSString *query = [NSString stringWithFormat:sql,
                                    nodo,
                                    [NSString intToStr:self.recID]];
    
#ifdef DEBUG_GRUPOS
    NSLog(@"- query deleteGrupo: %@", query);
#endif
    
    [[DB getInstance] excecuteSQL:query];
        
    self.action = kMTEANone;
}

- (BOOL)checkBlocked
{
    if ([[self.descripcion lowercaseString] isEqualToString:@"cartera de medicos"])
        return YES;
    else
        return NO;
}

+ (NSArray*)getAll
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql = @"SELECT id, descripcion "
    "FROM gst_grupos "
    "WHERE (nodo = %@) "
    "ORDER BY descripcion;";
    
    NSString *nodo = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql, nodo];
    
#ifdef DEBUG_GRUPOS
    NSLog(@"- query getAllGrupos: %@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if (statement)
    {
        MTGrupos *o;
        char *_c;
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            
            o = [[MTGrupos alloc] init];
            o.action = kMTEANone;
            
            // id
            o.recID = sqlite3_column_int(statement, 0);
            
            // Descripción.
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0))
            {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            o.blocked = [o checkBlocked];
            
            [result addObject:o];
        }
        
	} else
    {
        NSAssert(0, @"No se puede ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}


#pragma mark -
#pragma mark DB Medicos methods

- (BOOL)existeGrupoForMedico:(NSUInteger)idMedico
{
    BOOL result = NO;
    
    NSString *sql = @"SELECT grupo "
                     "FROM gst_grupomedicos "
                     "WHERE (nodo = %@ AND medico = %d AND grupo = %d);";
    
    NSString *nodo = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql, nodo, idMedico, self.recID];
    
#ifdef DEBUG_GRUPOS
    NSLog(@"- query existeGrupoForMedico: %@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if (statement)
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
            result = YES;
	} else
    {
        NSAssert(0, @"No se puede ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}

- (BOOL)saveGrupoForMedico:(NSUInteger)idMedico
{
    NSString *nodo = SettingForKey(NODO, @"0");
    
    if ([self existeGrupoForMedico:idMedico])
        return NO;
    
    // Inserto el nuevo grupo en la base de datos.
    NSString *sql = @"INSERT INTO gst_grupomedicos (nodo, grupo, medico) "
                    "VALUES (%@, %d, %d);";
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       self.recID,
                       idMedico];

#ifdef DEBUG_GRUPOS
    NSLog(@"- query saveGrupoForMedico: %@", query);
#endif
    
    [[DB getInstance] excecuteSQL:query];
    
    // Grabo la novedad
    NSString *novedadParam = [NSString stringWithFormat:@"%@|%d|%d",
                              nodo,
                              self.recID,
                              idMedico];

#ifdef DEBUG_GRUPOS
    NSLog(@"- novedad saveGrupoForMedico: %@", novedadParam);
#endif

    [Novedad insert:@"gst_grupomedicos" novedad:novedadParam tipo:kInsert];
    
    return YES;
}

- (void)deleteGrupoForMedico:(NSUInteger)idMedico
{
    NSString *nodo = SettingForKey(NODO, @"0");
    NSString *sql;
    
    // Grabo la novedad
    NSString *novedadParam = [NSString stringWithFormat:@"%@|%d|%d",
                              nodo,
                              self.recID,
                              idMedico];
    
#ifdef DEBUG_GRUPOS
    NSLog(@"- novedad deleteGrupoForMedico: %@", novedadParam);
#endif
    
    [Novedad insert:@"gst_grupomedicos" novedad:novedadParam tipo:kDelete];
    
    
    // Borro el médico del grupo en la base de datos.
    sql = @"DELETE FROM gst_grupomedicos "
           "WHERE (nodo = %@ AND grupo = %d AND medico = %d);";
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       self.recID,
                       idMedico];
    
#ifdef DEBUG_GRUPOS
    NSLog(@"- query deleteGrupoForMedico: %@", query);
#endif
    
    [[DB getInstance] excecuteSQL:query];
    
    
}

- (void)deleteGrupoForAllMedico
{
    NSString *nodo = SettingForKey(NODO, @"0");
    NSString *sql;
    NSString *query;
    
    // Grabo la novedad
    NSString *novedadParam;
    
    // Creo una consulta que leerá todos los médicos a los cuales se le asignó este grupo.
    sql = @"SELECT nodo || '|' || "
                   "grupo || '|' || "
                   "medico as novedad "
            "FROM gst_grupomedicos "
            "WHERE nodo = %@ and grupo = %d";
    
    query = [NSString stringWithFormat:sql,
                        nodo,
                        self.recID];
    
     sqlite3_stmt *statement = [[DB getInstance] prepare:query];
     
     if ( statement )
     {
         char *_c;
         // Genero la novedad de delete para cada uno de los médicos encontrados en este grupo
         while (sqlite3_step(statement) == SQLITE_ROW)
         {
             // Novedad
             novedadParam = @"0|0|0";
             _c = (char *)sqlite3_column_text(statement, 1);
             if((_c != nil) && (strlen(_c) > 0))
             {
                 novedadParam = [NSString stringWithUTF8String:_c];
#ifdef DEBUG_GRUPOS
                 NSLog(@"- novedad deleteGrupoForAllMedico: %@", novedadParam);
#endif
                [Novedad insert:@"gst_grupomedicos" novedad:novedadParam tipo:kDelete];
             }
         }
         
     } else {
         NSAssert(0, @"No se puede ejecutar: %@", query);
     }
     sqlite3_finalize(statement);
    
    
    // Elimino los medicos asignados al grupo en la base de datos.
    sql = @"DELETE FROM gst_grupomedicos "
           "WHERE (nodo = %@ AND grupo = %d);";
    
    query = [NSString stringWithFormat:sql,
                       nodo,
                       self.recID];
    
#ifdef DEBUG_GRUPOS
    NSLog(@"- query deleteGrupoForAllMedico: %@", query);
#endif
    
    [[DB getInstance] excecuteSQL:query];
}


+ (NSArray*)getAllForMedico:(NSUInteger)idMedico
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql = @"SELECT id, descripcion "
                     "FROM gst_grupomedicos LEFT JOIN "
                          "gst_grupos ON (gst_grupos.id = gst_grupomedicos.grupo AND "
                                         "gst_grupos.nodo = gst_grupomedicos.nodo) "
                     "WHERE (gst_grupomedicos.nodo = %@ AND gst_grupomedicos.medico = %@) "
                     "ORDER BY descripcion;";
    
    NSString *nodo = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql, nodo, [NSString intToStr:idMedico]];
    
#ifdef DEBUG_GRUPOS
    NSLog(@"- query getGruposForMedico: %@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        MTGrupos *o;
        char *_c;
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            
            o = [[MTGrupos alloc] init];
            o.action = kMTEANone;
            
            // id
            o.recID = sqlite3_column_int(statement, 0);
            
            // Descripción.
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            o.blocked = [o checkBlocked];
            
            [result addObject:o];
        }
        
	} else {
        NSAssert(0, @"No se puede ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}

@end
