//
//  CustomModalViewController.m
//  RnR
//
//  Created by Fabian E. Pezet Vila on 27/11/12.
//  Copyright (c) 2012 Fabian E. Pezet Vila. All rights reserved.
//

#import "CustomModalViewController.h"

@interface CustomModalViewController ()
- (void) configurarBackground;
@end


@implementation CustomModalViewController

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self configurarBackground];
    
}

- (void) configurarBackground{
    UIImageView *bg = [[[UIImageView alloc] initWithImage:[self getBackgroundImage]] autorelease];
    [self.view insertSubview:bg atIndex:0];
    
}

-(UIImage*)getBackgroundImage{
    
    NSAssert(0, @"implementar en hijo");
    return nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) alertTitle:(NSString*) titulo mensaje:(NSString*) mensaje boton:(NSString*) boton{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titulo message:mensaje delegate:nil cancelButtonTitle:boton otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}

@end
