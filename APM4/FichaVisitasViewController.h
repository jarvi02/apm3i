//
//  FichaVisitasViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 19/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "CarteraViewController.h"

@interface FichaVisitasViewController : CustomViewController

@property (nonatomic, strong) IBOutlet UIView *visitaView;
@property (nonatomic, weak) CarteraViewController *carteraViewControllerReference;

@property (nonatomic, strong) IBOutlet UISegmentedControl *btnSegment;
- (IBAction)tapSegments:(id)sender;

#pragma mark -
#pragma mark Visitas view
@property (strong, nonatomic) IBOutlet UITableView *tablaVisitas;
@property (strong, nonatomic) IBOutlet UITableView *tablaOtrosRepresentantes;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;


#pragma mark -
#pragma mark Histórico view
@property (strong, nonatomic) IBOutlet UIScrollView *historicoView;
@property (strong, nonatomic) IBOutlet UITableView  *historicoTable;

@end
