//
//  DateSelectorViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "DateSelectorViewController.h"
#import "AppDelegate.h"
#import "NSDate+extensions.h"

@interface DateSelectorViewController ()

@end

@implementation DateSelectorViewController

#pragma mark -
#pragma mark LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.title = @"Seleccione";
    UIBarButtonItem *barButton = [[[UIBarButtonItem alloc] initWithTitle: @"OK" style:UIBarButtonItemStyleDone target:self action:@selector(dateDone:)] autorelease];
    barButton.tintColor = UIColorFromRGB(defaultButtonColor);
    
    self.navigationItem.rightBarButtonItem = barButton;
    
	[super viewWillAppear:animated];
	if (self.date)
	{
		self.datePicker.date = self.date;
        
	}
	self.date = self.datePicker.date;
}

#pragma mark -
#pragma mark Actions

- (IBAction)dateChanged
{
    self.date = [self.datePicker.date dateAsDateWithoutTime];
}

- (void)dateDone:(id)sender
{
    [Delegate dismissPopOver:YES];
    if (self.delegate != nil)
    {
        [self.delegate DateSelector:self dateSelected:self.date];
    }
}

#pragma mark -
#pragma mark Memory Management


- (void)dealloc
{
    
    [_date release];
    [_datePicker release];
    
    [super dealloc];
}

@end
