//
//  VisitasOtrosRepViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 08/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FichaVisitaOtrosRepCell.h"

@interface VisitasOtrosRepViewController : UITableViewController


@property (nonatomic, retain) NSArray                           *data;
@property (retain, nonatomic) IBOutlet FichaVisitaOtrosRepCell    *tmpCell;
@property (nonatomic, retain) UINib                             *cellNib;

- (id)initWithData:(NSArray*)aData;
- (void)updateData:(NSArray*)aData;


@end
