//
//  MTVisita.m
//  APM4
//
//  Created by Laura Busnahe on 8/15/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTVisita.h"
#import "DBExtended.h"
#import "Config.h"
#import "Novedad.h"
#import "Promocion.h"
#import "ProductoTrazabilidad.h"
#import "EstadoVisita.h"
#import "ParteDiarioData.h"
#import "ParteDiario.h"

@implementation MTVisita

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.oldData = nil;
    }
    
    return self;
}


#pragma mark -
#pragma mark Class methods

+(BOOL)validarVisitaExiste:(NSUInteger)idMedico fechaVisita:(NSDate *)fechaVisita fechaOriginal:(NSDate *)fechaOriginal
{
    
    
    NSString *sql = @"select id "
    "from med_visitas "
    "where nodo = %@ and medico = %d and visitador = %@ and fecha = %.0f and (1 = %@ or fecha <> %.0f) and tipo = 1";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       idMedico,
                       nodo,
                       [fechaVisita timeIntervalSince1970],
                       fechaOriginal != nil ? @"0" : @"1",
                       fechaOriginal != nil ? [[fechaOriginal dateAsDateWithoutTime] timeIntervalSince1970] :
                       [[fechaVisita dateAsDateWithoutTime] timeIntervalSince1970]];
#ifdef DEBUG_VISITA
    NSLog(@"- Visitas: validarVisitaExiste -");
    NSLog(@"- query: %@",query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    BOOL result = YES;
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
            result = NO;
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
    
}

+(BOOL)validarFechaDeVisista:(NSUInteger)idMedico fechaVisita:(NSDate *)fechaVisita fechaOriginal:(NSDate *)fechaOriginal
{
    //NSString *sql = @"select id from med_visitas where nodo = %@ and medico = %d and visitador = %@ and fecha >= %.0f and (1 = %@ or fecha <> %.0f)";
    
    NSString *sql = @"SELECT id "
    "FROM med_visitas "
    "WHERE nodo = %@ AND "
    "medico = %d AND "
    "visitador = %@ AND "
    "fecha > %.0f AND "
    "(1 = %@ OR fecha <> %.0f) AND "
    "tipo = 1";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       idMedico,
                       nodo,
                       [[fechaVisita dateAsDateWithoutTime] timeIntervalSince1970],
                       fechaOriginal != nil ? @"0" : @"1",
                       fechaOriginal != nil ? [[fechaOriginal dateAsDateWithoutTime] timeIntervalSince1970] :
                       [[fechaVisita dateAsDateWithoutTime] timeIntervalSince1970]];
#ifdef DEBUG_VISITA
    NSLog(@"- Visita: validarFechaDeVisista -");
    NSLog(@"%@",query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    BOOL result = YES;
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
            result = NO;
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
}

+(MTVisita*)getByID:(NSUInteger)idVisita idMedico:(NSUInteger)idMedico
{
    /*NSString *sql = @"select a.medico, a.fecha, a.tipo, a.domicilio, a.objetivos, a.evaluacion, b.descripcion, a.id FROM med_visitas a, mtc_tiposvisita b where a.tipo = b.id and a.id = %d and nodo = %@ and medico = %d";*/
    
    
    NSString *sql =
    @"SELECT "
    "a.medico, "
    "a.fecha, "
    "a.tipo, "
    "a.domicilio, "
    "a.objetivos, "
    "a.evaluacion, "
    "b.descripcion, "
    "a.id, "
    "b.estado, "
    "b.contacto "
    "FROM med_visitas a JOIN  "
    "mtc_tiposvisita  b on (a.tipo = b.id) "
    "WHERE a.id = %d and nodo = %@ and medico = %d";
    
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *query = [NSString stringWithFormat:sql, idVisita, nodo, idMedico];
    
#ifdef DEBUG_VISITA
    NSLog(@"- Visitas: getByID -");
    NSLog(@"   query: %@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    MTVisita *v = [[MTVisita alloc] init];
    
    char *_c;
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            NSUInteger _idMedico = sqlite3_column_int(statement, 0);
            v.medico = [MTMedico GetById:_idMedico];
            v.actividad = [NSString string:v.medico.actividad ifNull:@""];
            
            double val = sqlite3_column_double(statement, 1);
            v.fecha = [[NSDate dateWithTimeIntervalSince1970:val] dateAsDateWithoutTime];
            
#ifdef DEBUG_VISITA
            NSLog(@"Fecha en base datos %.0lf  fecha del date %.0lf", val, [v.fecha timeIntervalSince1970]);
#endif
            
            
            v.idTipo = sqlite3_column_int(statement, 2);
            
            v.idDomicilio = sqlite3_column_int(statement, 3);
            
            _c = (char *)sqlite3_column_text(statement, 4);
            v.objetivos = [NSString pCharToString:_c];
			
            _c = (char *)sqlite3_column_text(statement, 5);
            v.evaluacion = [NSString pCharToString:_c];
			
            _c = (char *)sqlite3_column_text(statement, 6);
            v.descripcionTipo = [NSString pCharToString:_c];
            
            v.idVisita = sqlite3_column_int(statement, 7);
            
            // b.estado
            v.idEstado = sqlite3_column_int(statement, 8);
            
            // b.contacto
            v.contacto = sqlite3_column_int(statement, 9);
        }
    }
    
    sqlite3_finalize(statement);
    
    return v;
    
}



#pragma mark -
#pragma mark Public methods

- (void)guardarModificacion
{
    // MODIFICACION
    
    // Si es una visita, Actualizo el parte diario.
    if (self.contacto)
        [self actualizarParteDiarioModificacion];
    
    self.fecha = [self.fecha dateAsDateWithoutTime];
    
    // Modificacion del estado y visitas del medico
    self.medico.idEstado          = [EstadoVisita getByID:self.idTipo].idEstado;
    self.medico.actividad         = [NSString string:self.actividad ifNull:self.medico.actividad];
    [self.medico updateVisitaData];
    
    
    // Actualizacion de la visita
    [self update];
    [Novedad insert:@"med_visitas" novedad:[self getForNovedad] tipo:kUpdate];
    
    // Si es una visita, Guardar Los productos.
    if (self.contacto)
        [self guardarProductos];
}


- (void)iniciarModificacion
{
    self.oldData = [self copy];
}

- (void)cancelarModificacion
{
    self.fecha           = self.oldData.fecha;
    self.descripcionTipo = self.oldData.descripcionTipo;
    self.domicilio       = self.oldData.domicilio;
    self.objetivos       = self.oldData.objetivos;
    self.evaluacion      = self.oldData.evaluacion;
    self.idVisita        = self.oldData.idVisita;
    self.idTipo          = self.oldData.idTipo;
    self.idDomicilio     = self.oldData.idDomicilio;
    self.idEstado        = self.oldData.idEstado;
    self.contacto        = self.oldData.contacto;
    
    self.medico          = self.oldData.medico;
    self.productos       = self.oldData.productos;
    self.actividad       = self.oldData.actividad;
}


#pragma mark -
#pragma mark Private methods

-(void)actualizarParteDiarioModificacion
{
    BOOL parteSinMedicos = [[ConfigGlobal shareInstance] find:PARTE_SIN_MEDICOS];
//    BOOL usaPorcentaje = [[ConfigGlobal shareInstance] find:PORCENTAJESENPARTEDIARIO];
    BOOL fluctuante      = [self.medico isFluctuante];   // Fluctuante => asignacion = 2
    
    // Si no cumple esta condicion no hacer nada con el parte
    if (fluctuante || parteSinMedicos)
        return;
    
//    if (usaPorcentaje)
//        return;
    
    ParteDiarioData *parte   = nil;
    NSDate          *newDate = self.fecha;
    
    
    
    // Verifico si cambio la ACTIVIDAD
    
    self.fecha = self.oldData.fecha;
    
    if (![self.medico.actividad isEqualToString:self.actividad])
    {
        
        // Chequeo si la nueva actividad es MAÑANA
        if ([self.actividad isEqualToString:@"M"])
        {
            parte = [ParteDiario getParteDiario:9999
                                    todasTareas:0
                                      actividad:@"T"
                                    todasFechas:0
                                          fecha:self.fecha.dateAsDateWithoutTime
                                          rango:1
                                          desde:nil
                                          hasta:nil];
            
            // Chequeo si existe la tarea del parte
            if ((parte) && (parte.id_nodotareaspartediario > 0))
            {
                if (parte.cantidad > 0)
                {
                    [ParteDiario restarTarea:parte.id_nodotareaspartediario
                                 parteDiario:parte.partediario
                                   actividad:@"T"
                                       tarea:parte.tarea];
                
                    [Novedad insert:NODO_TAREAS_PARTEDIARIO
                            novedad:[ParteDiario getTareaForNovedad:parte.id_nodotareaspartediario
                                                        parteDiario:parte.partediario]
                               tipo:kUpdate];
                }
            }
        } else
        {
            
            // De lo contrario, la nueva actividad es TARDE
            parte = [ParteDiario getParteDiario:9999
                                    todasTareas:0 actividad:@"M"
                                    todasFechas:0
                                          fecha:self.fecha.dateAsDateWithoutTime
                                          rango:1
                                          desde:nil
                                          hasta:nil];
            
            // Chequeo si existe la tarea del parte
            if ((parte) && (parte.id_nodotareaspartediario > 0) &&
                (parte.cantidad > 0))
            {
                [ParteDiario restarTarea:parte.id_nodotareaspartediario
                             parteDiario:parte.partediario
                               actividad:@"M"
                                   tarea:parte.tarea];
                
                [Novedad insert:NODO_TAREAS_PARTEDIARIO
                        novedad:[ParteDiario getTareaForNovedad:parte.id_nodotareaspartediario
                                                    parteDiario:parte.partediario]
                           tipo:kUpdate];
            }
        }
        
        // TODO: Actualizar la actividad del medico?
        
        [self actualizarParteDiario];
    }
    
    self.fecha = newDate;
    
    
    
    // Verifico si cambio la FECHA
    
    if (![self.fecha.dateAsDateWithoutTime isEqualToDate:self.oldData.fecha.dateAsDateWithoutTime])
    {
        // Se modifico la fecha => Tengo que decrementar la cantidad para la visita original
        parte = [ParteDiario getParteDiario:9999
                                todasTareas:0
                                  actividad:self.actividad
                                todasFechas:0
                                      fecha:self.oldData.fecha.dateAsDateWithoutTime
                                      rango:1
                                      desde:nil
                                      hasta:nil];
        
        // Chequeo si existe alguna la tarea del parte con fecha anterior
        if ((parte) && (parte.id_nodotareaspartediario > 0))
        {
            // Si existe, chequeo que sea mayor a cero y decremento la actividad.
            if (parte.cantidad > 0)
            {
                [ParteDiario restarTarea:parte.id_nodotareaspartediario
                             parteDiario:parte.partediario
                               actividad:self.actividad
                                   tarea:parte.tarea];
                
                [Novedad insert:NODO_TAREAS_PARTEDIARIO
                        novedad:[ParteDiario getTareaForNovedad:parte.id_nodotareaspartediario
                                                    parteDiario:parte.partediario]
                           tipo:kUpdate];
                
                // Actualizo el registro en cache de la tarea del parte.
                parte = [ParteDiario getParteDiario:9999
                                        todasTareas:0
                                          actividad:self.actividad
                                        todasFechas:0
                                              fecha:self.oldData.fecha.dateAsDateWithoutTime
                                              rango:1
                                              desde:nil
                                              hasta:nil];
                
                //parte.cantidad = parte.cantidad -1;
            }
            
            // Chequeo si debo eliminar la tarea del parte.
            if (parte.cantidad <= 0)
            {
                // Elimino la taread del parte diario.
                [Novedad insert:NODO_TAREAS_PARTEDIARIO
                        novedad:[ParteDiario getTareaForNovedad:parte.id_nodotareaspartediario
                                                    parteDiario:parte.partediario]
                           tipo:kDelete];
                
                [ParteDiario deleteTarea:parte.partediario
                               actividad:parte.actividad];
                
            }
        }
        
        // Cheque si el parte posee algún otra tarea, si no es así, lo elimino.
        if (![ParteDiario tieneTareasCargadas:self.oldData.fecha.dateAsDateWithoutTime])
        {
            // Elimino el parte diario
            [Novedad insert:NODO_PARTEDIARIO
                    novedad:[ParteDiario getForNovedad:self.oldData.fecha.dateAsDateWithoutTime]
                       tipo:kDelete];
            
            [ParteDiario remove:self.oldData.fecha.dateAsDateWithoutTime];
        }
        
        
        // Actualizo o creo el nuevo parte diario.
        [self actualizarParteDiario];
    }
    
    [ParteDiario cleanTareasParte];
    [ParteDiario cleanParte];
}

@end
