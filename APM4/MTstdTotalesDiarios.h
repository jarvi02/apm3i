//
//  MTstdTotalesDiarios.h
//  APM4
//
//  Created by Laura Busnahe on 7/29/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTstdTotalesDiarios : NSObject

@property (nonatomic, strong) NSString  *descripcion;
@property (nonatomic) NSInteger         cantidadManiana;
@property (nonatomic) NSInteger         cantidadTarde;
@property (nonatomic) NSInteger         medicosManiana;
@property (nonatomic) NSInteger         medicosTarde;
@property (nonatomic) NSInteger         cantidad;
@property (nonatomic) NSInteger         medicos;

+ (NSArray*)getAll;

@end
