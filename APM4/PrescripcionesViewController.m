//
//  PrescripcionesViewController.m
//  APM4
//
//  Created by Laura Busnahe on 2/27/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "PrescripcionesViewController.h"
#import "FichaVisitasViewController.h"
#import "prescAuditoriaViewController.h"
#import "customTotalesCell.h"
#import "customPrescripcionesCell.h"
#import "customPrescripcionesFooter.h"
#import "MTEditActions.h"
#import "MTPrescripcionesTotales.h"
#import "MTAuditorias.h"
#import "MTMercados.h"

#import "GlobalDefines.h"



@implementation PrescripcionesViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.auditoria = nil;
        self.mercados  = nil;
        
        totalesPrescripciones = nil;
        
        self.totalesHeaderNIB  = [UINib nibWithNibName:@"customTotalesHeader" bundle:nil];
        [self.totalesHeaderNIB instantiateWithOwner:self options:nil];
        self.totalesCellNIB    = [UINib nibWithNibName:@"customTotalesCell" bundle:nil];
//        [self.totalesCellNIB   instantiateWithOwner:self options:nil];
        
        self.prescripcionesHeaderNIB  = [UINib nibWithNibName:@"customPrescripcionesHeader"
                                                       bundle:nil];
        [self.prescripcionesHeaderNIB instantiateWithOwner:self options:nil];
        self.prescripcionesHeader.delegate = self;
        
        self.prescripcionesFooterNIB  = [UINib nibWithNibName:@"customPrescripcionesFooter"
                                                       bundle:nil];
        [self.prescripcionesFooterNIB instantiateWithOwner:self options:nil];
        self.prescripcionesCellNIB    = [UINib nibWithNibName:@"customPrescripcionesCell"
                                                       bundle:nil];
//        [self.prescripcionesCellNIB   instantiateWithOwner:self options:nil];
    }
    return self;
}

// TODO: Seria bueno que todas las vistas implementen este método para que ella misma sepa que XIB le corresponde.
- (id)initViewDefaultNib
{
    self = [self initWithNibName:@"PrescripcionesViewController" bundle:nil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.btnAuditoria.tintColor = UIColorFromRGB(defaultButtonColor);
    
    self.navigationItem.rightBarButtonItem = self.btnAuditoria;
}

- (void)viewDidAppear:(BOOL)animated
{
    // TODO: Antes de mostrar la selección de auditorias, controlar que el medico posea auditorias.
    // Si no posse, mostrar un mensaje y hacer dismiss a la vista de Prescripciones.
    [super viewDidAppear:animated];
    [self navigateToAuditorias];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //    FichaVisitasViewController *o = [[FichaVisitasViewController alloc]
    //                                      initWithNibName:@"FichaVisitasViewController" bundle:nil];
    //    o.carteraViewControllerReference = self.carteraViewControllerReference;
    //    self.carteraViewControllerReference.trasitionStack = o;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma CustomViewController methods

-(NSString *)getTitleForHeader{
    self.title = [NSString stringWithFormat:@"Prescripciones de %@",
                  self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre];
    return self.title;
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"cartera_48"];
}


#pragma mark -
#pragma IBActions
- (IBAction)tapAuditoria:(id)sender
{
    [self navigateToAuditorias];
}


#pragma mark -
#pragma mark Prescripciones - Auditoria View

-(void)navigateToAuditorias;
{
    prescAuditoriaViewController *prescAuditoriaVC = [[prescAuditoriaViewController alloc] initViewDefaultNibForSelection:self.auditoria
                                                                                                            mercadosArray:self.mercados];
    
    prescAuditoriaVC.modalPresentationStyle = UIModalPresentationFormSheet;
    prescAuditoriaVC.modalTransitionStyle   = UIModalTransitionStyleCoverVertical;
    prescAuditoriaVC.delegate = self;
    prescAuditoriaVC.idMedico = self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico;
    
    [self.navigationController presentViewController:prescAuditoriaVC
                                            animated:YES
                                          completion:NULL];
    
}


#pragma mark -
#pragma mark prescAuditoria Delegate methods
- (void)prescAuditoriaView:(prescAuditoriaViewController *)controller
         selectedAuditoria:(MTAuditorias *)auditoria
              mercadosList:(NSArray *)mercados
                    action:(MTEditActionsType)action
{
    if (action == kMTEAChange)
    {
        self.auditoria = auditoria;
        self.mercados  = mercados;
        
        // TODO: Mostrar animación mientras realiza la consulta de prescripciones.
        
        NSInteger idMedico = 0;
        idMedico = self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico;
        
        totalesPrescripciones = [MTPrescripcionesTotales getTotalesMedico:idMedico
                                                                Auditoria:auditoria.RecID
                                                                 Mercados:mercados];
        
        [self.prescripcionesHeader orderBy:kMTFieldDescripcion reset:YES];
        [totalesPrescripciones.prescProductos sortByProperty:@"descripcion" Ascending:YES];
        [self.tblTotales        reloadData];
        [self.tblPrescripciones reloadData];
    }
}


#pragma mark -
#pragma mark Table methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of time zone names.
	//return self.rowData.count;
    
    //    return [self.rowDataFiltered count];
    
    // Tabla de totales
    if (tableView == self.tblTotales)
        return [totalesPrescripciones.prescTotalesMedico count];
    else
        
    // Tabla de Prescripciones
    if (tableView == self.tblPrescripciones)
#ifdef INCLUDE_PRESCRICIONESTABLE
        return [totalesPrescripciones.prescProductos count];
#else
        return 5;
#endif
    else
        return 0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Tabla de totales
    if (tableView == self.tblTotales)
        return self.totalesHeader;
    else
        
    // Tabla de Prescripciones
    if (tableView == self.tblPrescripciones)
        return self.prescripcionesHeader;
    else
        return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    // Tabla de Prescripciones
    if (tableView == self.tblPrescripciones)
    {
        NSInteger totalTrimestre1 = 0;
        NSInteger totalTrimestre2 = 0;
        NSInteger totalTrimestre3 = 0;
        NSInteger totalTrimestre4 = 0;
        NSInteger total           = 0;
        
        // Calculo los totales del footer.
        if ([totalesPrescripciones.prescProductos count] > 0)
        {
            for (MTTotalesProducto *o in totalesPrescripciones.prescProductos)
            {
                totalTrimestre1 = totalTrimestre1 + o.trimestre1;
                totalTrimestre2 = totalTrimestre2 + o.trimestre2;
                totalTrimestre3 = totalTrimestre3 + o.trimestre3;
                totalTrimestre4 = totalTrimestre4 + o.trimestre4;
                total           = total + o.total;
            }
        }
    
        // Completo los totales del footer.
        customPrescripcionesFooter *vista = self.prescripcionesFooter;
        vista.lblTrimestre1.text = [NSString stringWithFormat:@"%i",totalTrimestre1];
        vista.lblTrimestre2.text = [NSString stringWithFormat:@"%i",totalTrimestre2];
        vista.lblTrimestre3.text = [NSString stringWithFormat:@"%i",totalTrimestre3];
        vista.lblTrimestre4.text = [NSString stringWithFormat:@"%i",totalTrimestre4];
        vista.lblTotal.text      = [NSString stringWithFormat:@"%i",total];
        return vista;
    }
    else
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    // Tabla de Prescripciones
    if (tableView == self.tblPrescripciones)
        return 33;
    else
        return 27;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    // Tabla de Prescripciones
    if (tableView == self.tblPrescripciones)
        return 27;
    else
        return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *TotalesCellIdentifier = @"presTotalesCell";
    static NSString *PrescripcionesCellIdentifier = @"presPrescripcionesCell";
    UITableViewCell *cell;
	
    NSString* cellText = @"";
    
    NSString *MyIdentifier;
    
    // Tabla de totales
    if (tableView == self.tblTotales)
    {
        MyIdentifier = [NSString stringWithString:TotalesCellIdentifier];
        customTotalesCell *tempCell = (customTotalesCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (tempCell == nil)
        {
            [self.totalesCellNIB   instantiateWithOwner:self options:nil];
            tempCell = self.totalesCell;
            self.totalesCell = nil;
        }
        
        if (indexPath.row < [totalesPrescripciones.prescTotalesMedico count])
        {
            MTTotalesMedico *o = [totalesPrescripciones.prescTotalesMedico objectAtIndex:indexPath.row];
            
            tempCell.lblDescripcion.text = o.descripcion;
            tempCell.lblUniAcumuladas.text = [NSString stringWithFormat:@"%i", o.unidades];
            tempCell.lblPorcRelativo.text = [NSString stringWithFormat:@"%0.2f %%", o.porcentaje];
            
        } else
        {
            tempCell.lblDescripcion.text = @"";
            tempCell.lblUniAcumuladas.text = @"";
            tempCell.lblPorcRelativo.text = @"";
        }
        
        cell = tempCell;
        
    } else
        
    // Tabla de Prescripciones
    if (tableView == self.tblPrescripciones)
    {
        MyIdentifier = [NSString stringWithString:PrescripcionesCellIdentifier];
        customPrescripcionesCell *tempCell = (customPrescripcionesCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (tempCell == nil)
        {
            [self.prescripcionesCellNIB   instantiateWithOwner:self options:nil];
            tempCell = self.prescripcionesCell;
            self.prescripcionesCell = nil;
        }
        
#ifdef INCLUDE_PRESCRICIONESTABLE
        if (indexPath.row < [totalesPrescripciones.prescProductos count])
        {
            MTTotalesProducto *o;
            o = [totalesPrescripciones.prescProductos objectAtIndex:indexPath.row];
            
            tempCell.lblProducto.text = o.descripcion;
            tempCell.lblTrimestre1.text = [NSString stringWithFormat:@"%i", o.trimestre1];
            tempCell.lblTrimestre2.text = [NSString stringWithFormat:@"%i", o.trimestre2];
            tempCell.lblTrimestre3.text = [NSString stringWithFormat:@"%i", o.trimestre3];
            tempCell.lblTrimestre4.text = [NSString stringWithFormat:@"%i", o.trimestre4];
            tempCell.lblTotal.text      = [NSString stringWithFormat:@"%i", o.total];
            tempCell.lblMercado.text    = o.descMercado;
        } else
        {
            {
                tempCell.lblProducto.text   = @"";
                tempCell.lblTrimestre1.text = @"";
                tempCell.lblTrimestre2.text = @"";
                tempCell.lblTrimestre3.text = @"";
                tempCell.lblTrimestre4.text = @"";
                tempCell.lblTotal.text      = @"";
                tempCell.lblMercado.text    = @"";
            }
        }
#else
                    tempCell.lblProducto.text = @"Productox";
                    tempCell.lblTrimestre1.text = @"1";
                    tempCell.lblTrimestre2.text = @"2";
                    tempCell.lblTrimestre3.text = @"3";
                    tempCell.lblTrimestre4.text = @"4";
                    tempCell.lblTotal.text      = @"10";
                    tempCell.lblMercado.text    = @"";
#endif
        cell = tempCell;
        
    } else
    
    // Otra Tabla
    {
        MyIdentifier = @"MyCell";
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        // If the cell is void, create the cell object.
        if (cell == nil)
        {
            // Use the default cell style.
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
        }
        
        cellText = @"algo";
        
        // Set up the cell content.
        cell.textLabel.text = [NSString stringWithFormat:@"%@", cellText];
    }
    
	return cell;
}

#pragma mark -
#pragma mark MTHeader methods
- (void)customPrescHeader:(customPrescripcionesHeader *)header
            selectedField:(MTHeaderField)field
                    Order:(MTHeaderOrder)order
{
    NSString *sOrdenarPor = @"";
    BOOL ascending        = order == kMTOrderAsc? YES : NO;
    
    switch (field)
    {
        case kMTFieldDescripcion:
            sOrdenarPor = @"descripcion";
            break;
            
        case kMTFieldTrimestre1:
            sOrdenarPor = @"trimestre1";
            break;
            
        case kMTFieldTrimestre2:
            sOrdenarPor = @"trimestre2";
            break;
            
        case kMTFieldTrimestre3:
            sOrdenarPor = @"trimestre3";
            break;
            
        case kMTFieldTrimestre4:
            sOrdenarPor = @"trimestre4";
            break;
            
        case kMTFieldTotal:
            sOrdenarPor = @"total";
            break;
            
        case kMTFieldMercado:
            sOrdenarPor = @"descMercado";
            break;
            
        default:
            sOrdenarPor = @"descripcion";
            break;
    }
    
#ifdef DEBUG_PRESCRIPCIONES
    NSLog(@"Ordenar por %@ (%@)", sOrdenarPor, ascending? @"ASC" : @"DES");
#endif
    
    [totalesPrescripciones.prescProductos sortByProperty:sOrdenarPor Ascending:ascending];
    [self.tblPrescripciones reloadData];
    
}


@end