//
//  VisitasViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 08/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FichaVisitaCell.h"
#import "CarteraViewController.h"

@interface VisitasViewController : UITableViewController


@property (nonatomic, retain) NSMutableArray                       *data;
@property (retain, nonatomic) IBOutlet FichaVisitaCell    *tmpCell;
@property (nonatomic, retain) UINib                         *cellNib;

@property (nonatomic, assign) UINavigationController    *navigationControllerReference;
@property(nonatomic, assign) CarteraViewController *carteraViewControllerReference;

- (id)initWithData:(NSArray*)aData;
- (void)updateData:(NSArray*)aData;


@end
