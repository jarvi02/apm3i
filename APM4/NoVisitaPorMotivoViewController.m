//
//  NoVisitaPorMotivoViewController.m
//  APM4
//
//  Created by Laura Busnahe on 9/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "NoVisitaPorMotivoViewController.h"

@interface NoVisitaPorMotivoViewController ()

@end

@implementation NoVisitaPorMotivoViewController
{
    NSString *descTitulo;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.cellNib    = [UINib nibWithNibName:@"NoVisitaPorMotivoCell" bundle:nil];
        
        self.headerNib  = [UINib nibWithNibName:@"NoVisitaPorMotivoHeader" bundle:nil];
        [self.headerNib instantiateWithOwner:self options:nil];
        
        self.tableHeader.delegate = self;
        
        self.data = nil;
    }
    return self;
}


- (id)initViewDefaultNib
{
    self = [self initWithNibName:@"NoVisitaPorMotivoViewController" bundle:nil];
    
    descTitulo   = @"No Visitas por Motivo";
    
    self.title = [NSString string:descTitulo ifNull:@""];
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = descTitulo;
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Do all the load shit here
    
    [self updateData];
    //[self.tableHeader orderBy:kMTFieldDescripcion sender:self.tableHeader.lblDescripcion reset:YES];
    // ------
    
    [self.loadingView hide];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Overrides

-(BOOL)hasFooter
{
    return YES;
}

-(NSString *)getTitleForHeader{
    return descTitulo;
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"cabecera_principal_32"];
}

#pragma mark -
#pragma mark tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.data count] > 0)
        return [self.data count] -1;
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"NoVisitaPorMotivoCelldentifier";
    
    // Obtain the cell object.
    NoVisitaPorMotivoCell *cell = (NoVisitaPorMotivoCell*)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
    	cell = self.tmpCell;
    	self.tmpCell = nil;
    }
    
    // Obtengo los datos para la celada.
    MTstdNoVisita *o = [self.data objectAtIndex:indexPath.row];
    
    // Celdas de Orden
    cell.lblMotivo.text  = [NSString string:o.motivo];
    cell.lblManiana.text = [NSString intToStr:o.maniana ifZero:@"-"];
    cell.lblTarde.text   = [NSString intToStr:o.tarde ifZero:@"-"];
    cell.lblTotal.text   = [NSString intToStr:o.total ifZero:@"-"];
    
    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
    {
        //[self.tableHeader.lblDescripcion setTitle:descEtiqueta forState:UIControlStateNormal];
        return self.tableHeader;
    }
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader.frame.size.height;
    else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.tableFooter)
    {
        return self.tableFooter.frame.size.height;
    }
    
    return 0;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (self.tableFooter)
    {
        MTstdNoVisita *o = nil;
        
        if ([self.data count] > 0)
            o = [self.data objectAtIndex:[self.data count]-1];
        
        [self.tableFooter.lblManiana setTitle:[NSString intToStr:o.maniana ifZero:@"-"]
                                    forState:UIControlStateNormal];
        
        [self.tableFooter.lblTarde setTitle:[NSString intToStr:o.tarde ifZero:@"-"]
                                   forState:UIControlStateNormal];
        
        [self.tableFooter.lblTotal setTitle:[NSString intToStr:o.total ifZero:@"-"]
                                   forState:UIControlStateNormal];
        
        
        return self.tableFooter;
    }
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark -
#pragma mark Actualización de datos
- (void)updateData
{
    self.data = [NSMutableArray arrayWithArray:[MTstdNoVisita getAll]];
    
    // Actalizo los datos de la tabla.
    [self.tableView reloadData];
    [self.loadingView hide];
}


#pragma mark -
#pragma mark Header methods
- (void)NoVisitaPorMotivoHeader:(NoVisitaPorMotivoHeader*)header
                  selectedField:(tNoVisitaPorMotivoHeaderField)field
                          Order:(NSComparisonResult)order
{
    if ([self.data count] < 1)
        return;
    
    
    // Guardo el total y lo elemino del listado.
    MTstdNoVisita *o = nil;
    NSInteger index = [self.data count]-1;
    
    o = [self.data objectAtIndex:index];
    [self.data removeObject:o];
    
    
    // Busco el string correspondiente a la propiedad.
    NSString *keyName = @"fecha";
    
    if (field == fNoVisitaPorMotivoFieldMotivo)
        keyName = @"motivo";
    
    if (field == fNoVisitaPorMotivoFieldManiana)
        keyName = @"maniana";
    
    if (field == fNoVisitaPorMotivoFieldTarde)
        keyName = @"tarde";
    
    if (field == fNoVisitaPorMotivoFieldTotal)
        keyName = @"total";
    
    BOOL asc = YES;
    if (order == NSOrderedDescending)
        asc = NO;
    
    // Reordeno el listado.
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc]
                                initWithKey:keyName
                                ascending:asc];
    NSArray *sortDescriptors = [NSArray arrayWithObject: sorter];
    [self.data sortUsingDescriptors:sortDescriptors];
    
    // Vuelvo a agregar el total.
    [self.data addObject:o];
    
    [self.tableView reloadData];
}

@end
