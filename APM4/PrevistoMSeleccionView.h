//
//  PrevistoMSeleccion.h
//  APM3i
//
//  Created by Laura Busnahe on 2/7/13.
//  Copyright (c) 2013 Laura Busnahe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "popListView.h"
#import "MTEditActions.h"

@class MTEntidad;
@class PrevistoMensual;
@class MTPrevistoActividad;

@class PrevistoMSeleccionView;



// Este protocolo es requerido para dar aviso al delegate que acción debe llevarse a cabo y con qué datos.
@protocol PrevistoMEdicioDelegate
@required
-(void)pmEditPerformAction:(PrevistoMSeleccionView*)controller
                    Action:(MTEditActionsType)action
                  withData:(MTPrevistoActividad*)activity
               objectIndex:(NSInteger)selectionIndex;
@end


@interface PrevistoMSeleccionView : CustomViewController <MTpopListViewDelegate, UITextFieldDelegate, UIAlertViewDelegate>
{
    UIImage     *imgChecked;
    UIImage     *imgUnchecked;
    
    BOOL        checkedMedico;
    BOOL        checkedIntitucion;
    BOOL        checkedOtros;
}

#pragma mark -
#pragma mark Private members
@property (nonatomic, retain) MTpopListView         *popMenu;

@property (nonatomic, retain) NSArray               *arrayHorarioME;
@property (nonatomic, retain) NSArray               *arrayHorarioMS;
@property (nonatomic, retain) NSArray               *arrayHorarioTE;
@property (nonatomic, retain) NSArray               *arrayHorarioTS;

@property (nonatomic, retain) NSArray               *arrayMedicos;
@property (nonatomic, retain) NSArray               *arrayInstituciones;

@property (nonatomic, retain) NSDate               *Date;
@property                     Boolean              Evening;
@property                     NSInteger            selectionIndex;
@property (nonatomic, retain) MTPrevistoActividad  *Selection;
@property (nonatomic, assign) MTEditActionsType    action;
@property (nonatomic, assign) MTEditActionsType    oldAction;

@property (nonatomic, assign) id <PrevistoMEdicioDelegate> delegate;


#pragma mark -
#pragma mark Título - Outlets
// Título
@property (retain, nonatomic) IBOutlet UILabel *lblFecha;
@property (retain, nonatomic) IBOutlet UILabel *lblTurno;

#pragma mark -
#pragma mark Horario de Entrada - Outlets, IBActions
// Horiario de Entrada.
@property (nonatomic, retain) IBOutlet UITextField    *edtEntrada;
@property (retain, nonatomic) IBOutlet UIButton       *btnEntrada;
- (IBAction)tapEntrada:(id)sender;

#pragma mark -
#pragma mark Horario de Salida - Outlets, IBActions
// Horario de Salida.
@property (nonatomic, retain) IBOutlet UITextField    *edtSalida;
@property (retain, nonatomic) IBOutlet UIButton       *btnSalida;
- (IBAction)tapSalida:(id)sender;

#pragma mark -
#pragma mark Médico - Outlets, IBActions
// Médico.
@property (retain, nonatomic) IBOutlet UIButton       *btnMedico;
@property (retain, nonatomic) IBOutlet UIButton       *chkMedico;
@property (nonatomic, retain) IBOutlet UITextField    *edtMedico;
- (IBAction)tapMedico:(id)sender;
- (IBAction)tapChkMedico:(id)sender;

#pragma mark -
#pragma mark Institución - Outlets, IBActions
// Institución.
@property (nonatomic, retain) IBOutlet UIButton       *btnInstitucion;
@property (nonatomic, retain) IBOutlet UIButton       *chkInstitucion;
@property (nonatomic, retain) IBOutlet UITextField    *edtInstitucion;
- (IBAction)tapInstitucion:(id)sender;
- (IBAction)tapChkInstitucion:(id)sender;

#pragma mark -
#pragma mark Otros - Outlets, IBActions
// Otros.
@property (nonatomic, retain) IBOutlet UIButton       *btnOtros;
@property (nonatomic, retain) IBOutlet UIButton       *chkOtros;
@property (nonatomic, retain) IBOutlet UITextField    *edtOtros;
- (IBAction)tapChkOtros:(id)sender;

#pragma mark -
#pragma mark Footer Buttons - IBActions
// Botones Footer
@property (retain, nonatomic) IBOutlet UIToolbar      *toolBar;
- (IBAction)tapAceptar: (id)sender;
- (IBAction)tapCancelar:(id)sender;
- (IBAction)tapEliminar:(id)sender;


#pragma mark -
#pragma mark Init Methods
//- (id)initViewWithDate:(NSDate*)date Evening:(BOOL)evening;
- (id)initViewWithPrevisto:(PrevistoMensual*)previsto Data:(MTPrevistoActividad*)activity objectIndex:(NSInteger)oIndex;

- (IBAction)tapOtherButton:(id)sender;

@end
