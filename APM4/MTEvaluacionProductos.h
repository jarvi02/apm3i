//
//  MTEvaluacionProductos.h
//  APM4
//
//  Created by Laura Busnahe on 9/2/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTEvaluacionProductos : NSObject

@property(nonatomic)         NSUInteger     idProducto;
@property(nonatomic, strong) NSString       *descripcion;
@property(nonatomic, strong) NSString       *objetivo;

+ (NSArray*)getAllForMedico:(NSUInteger)idMedico;

@end
