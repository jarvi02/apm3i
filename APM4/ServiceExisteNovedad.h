//
//  ServiceExisteNovedad.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Service.h"

@interface ServiceExisteNovedad : Service


-(BOOL) existeNovedad:(NSString*)urlBase;

@end
