//
//  ProgramadosCell.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramadosCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lbOrden;
@property (retain, nonatomic) IBOutlet UILabel *lbProducto;
@property (retain, nonatomic) IBOutlet UILabel *lbCantidad;
@property (retain, nonatomic) IBOutlet UILabel *lbLit;
@property (retain, nonatomic) IBOutlet UILabel *lbObs;

@end
