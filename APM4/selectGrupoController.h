//
//  selectGrupoController.h
//  APM4
//
//  Created by Laura Busnahe on 6/25/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarteraViewController.h"

@interface selectGrupoController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (retain, nonatomic) IBOutlet UITableView      *tblGrupos;
@property (retain, nonatomic) IBOutlet UIBarButtonItem  *btnAceptar;

@property (nonatomic, assign) CarteraViewController     *carteraReference;
@property (nonatomic) BOOL                              agregarAGrupo;

@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, assign) id delegate;

@property (nonatomic, strong) NSMutableArray    *data;
@property (nonatomic, strong) NSMutableArray    *filteredData;

- (IBAction)tapAceptar:(id)sender;


- (id)initWithDefaultNib;

@end
