//
//  EvaluacionProductosCell.h
//  APM4
//
//  Created by Laura Busnahe on 9/4/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EvaluacionProductosCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblProducto;
@property (retain, nonatomic) IBOutlet UILabel *lblObjetivo;

@end
