//
//  Cargo.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Cargo.h"
#import "DB.h"
@implementation Cargo


- (id)init
{
    self = [super init];
    if (self)
    {
        self.idCargo = 0;
        self.descripcion = @"";
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [super dealloc];
    
}

- (NSString*)getDescription
{
    return [NSString stringWithFormat:@"%@", self.descripcion];
}

+(NSArray*) GetAll{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"select id as _id, descripcion from mtc_cargosinstitucionales;";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
        Cargo *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[Cargo alloc] init];
            
            o.idCargo = sqlite3_column_int(statement, 0);
            
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
}


-(id)copy{
    
    Cargo *o = [[Cargo alloc] init];
    o.idCargo = self.idCargo;
    o.descripcion = [NSString stringWithFormat:@"%@", self.descripcion];
    
    return [o autorelease];
}

@end
