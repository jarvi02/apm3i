//
//  EspecialidadViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABMMedicoViewController.h"
#import "TableForComboPopoverViewController.h"

@interface EspecialidadViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, TableForComboPopoverViewControllerDelegate>

@property(nonatomic,assign) ABMMedicoViewController *abmMedicoReference;

@property (retain, nonatomic) IBOutlet UITextField *txtEspecialidad;
- (IBAction)especialidad:(id)sender;

@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UIToolbar *toolBar;

-(void)guardar:(id)sender;
-(void)cancelar:(id)sender;
-(void)editar:(id)sender;


@end
