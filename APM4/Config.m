//
//  Config.m
//  RnR
//
//  Created by Fabian E. Pezet Vila on 10/12/12.
//  Copyright (c) 2012 Fabian E. Pezet Vila. All rights reserved.
//

#import "Config.h"
#import "DB.h"

#import "MTNodo.h"


@interface Config(private)
-(void)load;
-(void)loadNodo;
-(void)updateDB:(NSString*)aKey value:(NSString*)aValue;
@end


@implementation Config

+(Config*)shareInstance{
	static Config *instance;
	
	if(instance == nil){
		instance = [Config new];
	}
	return instance;
}

-(id) init {
	
    self = [super init];
	if ( self ){
        dictionary = [[NSMutableDictionary alloc] initWithCapacity:10];
        [self load];
        [self loadNodo];
	}
	return self;
}

-(void)dealloc{
    
    [dictionary release];
    [super dealloc];
}

- (BOOL)isValidKey:(NSString *)key
{
    return [dictionary valueForKey:key]!=nil;
}


-(void)load{
    
    NSString *sql = @"select ciclo, configuracion.entidad, remote_path, remote_host, remote_port, inicializar, cerrar_ciclo, tranid, tranid_ok, reintentos, pais, visitaspermitidas, app_activada, lab_nodos.descripcion, remote_host_2, remote_port_2  from configuracion left join lab_nodos on (lab_nodos.id = configuracion.entidad);";
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
			
            int _ciclo = sqlite3_column_int(statement, 0);
            NSString *ciclo = [NSString stringWithFormat:@"%d",_ciclo];
            [dictionary setValue:ciclo forKey:CICLO];
           
            int _entidad = sqlite3_column_int(statement, 1);
            NSString *entidad = [NSString stringWithFormat:@"%d",_entidad];
            [dictionary setValue:entidad forKey:ENTIDAD];
        
            char *_remotePath = (char *)sqlite3_column_text(statement, 2);
            NSString *remotePath;
            if((_remotePath != nil) && (strlen(_remotePath) > 0)) {
                remotePath = [NSString stringWithUTF8String:_remotePath];
			}else{
				remotePath = @"";
			}
            [dictionary setValue:remotePath forKey:REMOTE_PATH];
            
            char *_remoteHost = (char *)sqlite3_column_text(statement, 3);
            NSString *remoteHost;
            if((_remoteHost != nil) && (strlen(_remoteHost) > 0)) {
                remoteHost = [NSString stringWithUTF8String:_remoteHost];
			}else{
				remoteHost = @"";
			}
            [dictionary setValue:remoteHost forKey:REMOTE_HOST];
            
            int _remotePort = sqlite3_column_int(statement, 4);
            NSString *remotePort = [NSString stringWithFormat:@"%d",_remotePort];
            [dictionary setValue:remotePort forKey:REMOTE_PORT];
         
            
            int _inicializar = sqlite3_column_int(statement, 5);
            NSString *inicializar = [NSString stringWithFormat:@"%d",_inicializar];
            [dictionary setValue:inicializar forKey:INICIALIZAR];
            
            int _cerrarCiclo = sqlite3_column_int(statement, 6);
            NSString *cerrarCiclo = [NSString stringWithFormat:@"%d",_cerrarCiclo];
            [dictionary setValue:cerrarCiclo forKey:CERRAR_CICLO];
           
            int _tranId = sqlite3_column_int(statement, 7);
            NSString *tranId = [NSString stringWithFormat:@"%d",_tranId];
            [dictionary setValue:tranId forKey:TRANID];
            
            int _tranOK = sqlite3_column_int(statement, 8);
            NSString *tranOK = [NSString stringWithFormat:@"%d",_tranOK];
            [dictionary setValue:tranOK forKey:TRANID_OK];

            int _reintentos = sqlite3_column_int(statement, 9);
            NSString *reintentos = [NSString stringWithFormat:@"%d",_reintentos];
            [dictionary setValue:reintentos forKey:REINTENTOS];
		
            int _pais = sqlite3_column_int(statement, 10);
            NSString *pais = [NSString stringWithFormat:@"%d",_pais];
            [dictionary setValue:pais forKey:PAIS];
            
            int _visitas = sqlite3_column_int(statement, 11);
            NSString *visitas = [NSString stringWithFormat:@"%d",_visitas];
            [dictionary setValue:visitas forKey:VISITAS_PERMITIDAS];
            
            int _activa = sqlite3_column_int(statement, 12);
            NSString *activa = [NSString stringWithFormat:@"%d",_activa];
            [dictionary setValue:activa forKey:APP_ACTIVADA];
        
            char *_nodo = (char *)sqlite3_column_text(statement, 13);
            NSString *nodo;
            if((_nodo != nil) && (strlen(_nodo) > 0)) {
                nodo = [NSString stringWithUTF8String:_nodo];
			}else{
				nodo = @"";
			}
            [dictionary setValue:nodo forKey:DESCRIPCION_NODO];
            
            char *_remoteHost_2 = (char *)sqlite3_column_text(statement, 14);
            NSString *remoteHost_2;
            if((_remoteHost_2 != nil) && (strlen(_remoteHost_2) > 0)) {
                remoteHost_2 = [NSString stringWithUTF8String:_remoteHost_2];
			}else{
				remoteHost_2 = @"";
			}
            [dictionary setValue:remoteHost_2 forKey:REMOTE_HOST_2];
            
            int _remotePort_2 = sqlite3_column_int(statement, 15);
            NSString *remotePort_2 = [NSString stringWithFormat:@"%d",_remotePort_2];
            [dictionary setValue:remotePort_2 forKey:REMOTE_PORT_2];
            
        }
         
        
	} else {
        NSAssert(0, @"No se encontre la tabla configuracion");
	}
	sqlite3_finalize(statement);
}

-(void)loadNodo
{
    //NSString *nodo = [dictionary valueForKey:ENTIDAD];
    
    NSUInteger currentNodoID = 0;
    if ([self isValidKey:NODO])
        currentNodoID = [[dictionary valueForKey:NODO] integerValue];
    
    NSArray* nodosList = [MTNodo getAllforEntidad:[[dictionary valueForKey:ENTIDAD] integerValue]];
    MTNodo *nodo = nil;

    if ([nodosList count] > 0)
    {
        MTNodo *defaultNodo = nil;
        // Busco en el listado de nodos cual corresponde
        for (MTNodo *n in nodosList)
        {
            if (n.recID == currentNodoID)
                nodo = n;
            else
            if (n.recID == n.idEntidad)
                defaultNodo = n;
        }
        
        if (!defaultNodo)
            defaultNodo = [nodosList objectAtIndex:0];
            
        if ((!nodo))
            nodo = defaultNodo;
    }
    
    //[dictionary setValue:nodo forKey:NODO];
    [dictionary setValue:[NSString intToStr:nodo.recID] forKey:NODO];
    [dictionary setValue:[NSString string:nodo.descripcion ifNull:@""] forKey:DESCRIPCION_NODO];
}

-(void) reloadData
{
    NSString *lastNodo = [NSString string:[dictionary valueForKey:NODO] ifNull:@""];
    
    [dictionary removeAllObjects];
    [self load];
    
    [dictionary setValue:lastNodo forKey:NODO];
    [self loadNodo];
    
}

-(void)updateDB:(NSString*)aKey value:(NSString*)aValue{

    NSString *sql;
    
    NSString *query = @"";
    
    sql = @"UPDATE configuracion SET %@ = %@;";
    
    if ([aKey isEqualToString:REMOTE_PATH] || [aKey isEqualToString:REMOTE_HOST] || [aKey isEqualToString:REMOTE_HOST_2])
    {
        query = [NSString stringWithFormat:sql,aKey, [NSString stringQuoted:aValue ifNull:@"NULL"]];
    } else {
        query = [NSString stringWithFormat:sql,aKey, aValue];
    }

    //NSString *query = [NSString stringWithFormat:sql,aKey, aValue];
    [[DB getInstance] excecuteSQL:query];
}


-(NSString*)find:(NSString*)aKey{
    
    NSString *value =[dictionary valueForKey:aKey];
    if (value == nil) {
        NSString *msg = [NSString stringWithFormat:@"No se encontro key en diccionario Config ----> %@ <-----",aKey];
        NSAssert(0, msg);
    }
    return value;
}

-(void)update:(NSString*)aKey value:(NSString*)aValue{
    
    [self updateDB:aKey value:aValue];
    [dictionary removeObjectForKey:aKey];
    [dictionary setValue:aValue forKey:aKey];
    
}

- (void) updateNodo:(MTNodo*)nodo
{
    [dictionary removeObjectForKey:NODO];
    [dictionary setValue:[NSString intToStr:nodo.recID] forKey:NODO];
    
    [self loadNodo];
}

- (NSUInteger)getiOSVersion
{
    NSUInteger result = 0;
    
    // Leo el string con la versión de iOS
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    
//    NSLog(@"APM3i: System Version %@", systemVersion);
    
    NSArray *versionArray = [systemVersion componentsSeparatedByString:@"."];
    
    if ([versionArray count] > 0)
        result = [(NSString*)[versionArray objectAtIndex:0] integerValue];
    
    return result;
}

@end
