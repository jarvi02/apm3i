//
//  MTNodo.m
//  APM4
//
//  Created by Laura Busnahe on 8/27/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTNodo.h"
#import "Config.h"
#import "DBExtended.h"

@implementation MTNodo

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.recID      = 0;
        self.descripcion= @"";
        self.idEntidad  = 0;
    }
    
    return self;
}

- (NSString*) getDescription
{
    NSString* newDescription = [NSString stringWithFormat:@"%d - %@",
                                    self.recID,
                                    [NSString string:self.descripcion ifNull:@"" ]];
    
    return newDescription;
}

- (MTNodo*)copyWithZone:(NSZone*)zone
{
    MTNodo* newCopy; // = [[MTEntidad alloc] init];
    
    newCopy = [[MTNodo alloc] init];
    
    newCopy.recID      = self.recID;
    newCopy.descripcion= [NSString string:self.descripcion ifNull:@""];
    newCopy.idEntidad  = self.idEntidad;
    
    return newCopy;
}

+ (NSArray*)getAllforEntidad:(NSUInteger)idEntidad
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql =
        @"SELECT id, descripcion, entidad "
         "FROM lab_nodos "
         "WHERE entidad = %@ "
         "ORDER BY id";
    
    NSString *query = [NSString stringWithFormat:sql,
                        [NSString intToStr:idEntidad]];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    char *_c;
	
    if ( statement )
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTNodo* o = [[MTNodo alloc] init];
            
            // id
            o.recID = sqlite3_column_int(statement, 0);
            
            // descripcion
            _c = (char*)sqlite3_column_text(statement, 1);
            o.descripcion = [NSString pCharToString:_c];
            
            // entidad
            o.idEntidad = sqlite3_column_int(statement, 2);
            
            [result addObject:o];
        }
    }
    
    sqlite3_finalize(statement);
    
    
    return result;
}

@end
