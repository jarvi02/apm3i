//
//  VisitasPorDiasViewController.m
//  APM4
//
//  Created by Laura Busnahe on 9/11/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "VisitasPorDiasViewController.h"


#import "MTstdVisitasPorDia.h"

@implementation VisitasPorDiasViewController
{
    NSString *descTitulo;
    NSString *descEtiqueta;
    
    MTActivitySelection periodoSeleccionado;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.cellNib    = [UINib nibWithNibName:@"visitasPorDiasCell" bundle:nil];
        
        self.headerNib  = [UINib nibWithNibName:@"visitasPorDiasHeader" bundle:nil];
        [self.headerNib instantiateWithOwner:self options:nil];
        
        self.tableHeader.delegate = self;
        
        self.data = nil;
        
        self.arrayCombo = [NSArray arrayWithObjects:@"Total", @"Mañana", @"Tarde", nil];
        periodoSeleccionado = kMTActivityAll;
    }
    return self;
}


- (id)initViewDefaultNib
{
    self = [self initWithNibName:@"VisitasPorDiasViewController" bundle:nil];
    
    descTitulo   = @"Visitas por día";
    
    self.title = [NSString string:descTitulo ifNull:@""];
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = descTitulo;
    self.lblPeriodo.text = (NSString*)[self.arrayCombo objectAtIndex:0];
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Do all the load shit here
    
    [self updateData];
    //[self.tableHeader orderBy:kMTFieldDescripcion sender:self.tableHeader.lblDescripcion reset:YES];
    // ------
    
    [self.tableHeader orderBy:fVisitasPorDiasFieldFecha sender:self.tableHeader.lblFecha reset:YES];
    [self.loadingView hide];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Overrides

-(BOOL)hasFooter
{
    return YES;
}

-(NSString *)getTitleForHeader{
    return descTitulo;
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"cabecera_principal_32"];
}

#pragma mark -
#pragma mark tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.data count] > 0)
        return [self.data count] -1;
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"visitasPorDiasCelldentifier";
    
    // Obtain the cell object.
    visitasPorDiasCell *cell = (visitasPorDiasCell*)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
    	cell = self.tmpCell;
    	self.tmpCell = nil;
    }
    
    // Obtengo los datos para la celada.
    MTstdVisitasPorDia *o = [self.data objectAtIndex:indexPath.row];
    
    // Celdas de Orden
    cell.lblFecha.text        = [o.fecha formattedStringUsingFormat:@"dd/MM/yyyy"];
    cell.lblFijos.text        = [NSString intToStr:o.fijos ifZero:@"-"];
    cell.lblFluctuantes.text  = [NSString intToStr:o.fluctuantes ifZero:@"-"];
    cell.lblAcompaniadas.text = [NSString intToStr:o.acompaniadas ifZero:@"-"];
    cell.lblTotal.text        = [NSString intToStr:o.total ifZero:@"-"];
    
    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
    {
        //[self.tableHeader.lblDescripcion setTitle:descEtiqueta forState:UIControlStateNormal];
        return self.tableHeader;
    }
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader.frame.size.height;
    else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.tableFooter)
    {
        return self.tableFooter.frame.size.height;
    }
    
    return 0;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (self.tableFooter)
    {
        MTstdVisitasPorDia *o = nil;
        
        if ([self.data count] > 0)
            o = [self.data objectAtIndex:[self.data count]-1];
        
        [self.tableFooter.lblFijos setTitle:[NSString intToStr:o.fijos ifZero:@"-"]
                                       forState:UIControlStateNormal];
        
        [self.tableFooter.lblFluctuantes setTitle:[NSString intToStr:o.fluctuantes ifZero:@"-"]
                                       forState:UIControlStateNormal];
        
        [self.tableFooter.lblAcompaniadas setTitle:[NSString intToStr:o.acompaniadas ifZero:@"-"]
                                       forState:UIControlStateNormal];
        
        [self.tableFooter.lblTotal setTitle:[NSString intToStr:o.total ifZero:@"-"]
                                       forState:UIControlStateNormal];
        
        
        return self.tableFooter;
    }
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark -
#pragma mark Tap methods
- (IBAction)tapBtnPeriodo:(id)sender
{
    UIView *popRectView = (UIView*)sender;
    
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(200, 155);
    
    // Creo el objeto, asigno el título, el listado de datos, y defino que utilizará búsqueda.
    //if (!self.popupMenu)
    //{
        self.popupMenu = [[MTpopListView alloc] initWithRect:rect];
    //}
    [self.popupMenu dismissPopoverAnimated:NO];
    
    self.popupMenu.delegate = self;
    
    [self.popupMenu setTitleAndParameters:@"Período" rowData:self.arrayCombo];
    [self.popupMenu setSize:rect.size];
    
    [self.popupMenu useSearch:NO];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x,
                               popRectView.frame.origin.y, // + 64,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:sender
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionUp
                                            animated:YES];
    
    
}

#pragma mark -
#pragma mark Actualización de datos
- (void)updateData
{
    self.data = [NSMutableArray arrayWithArray:[MTstdVisitasPorDia getAllForActivity:periodoSeleccionado]];
    
    // Actalizo los datos de la tabla.
    [self.tableView reloadData];
    [self.loadingView hide];
}


#pragma mark popListView delegate methods

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender didSelectRow:(NSInteger *)MenuIndex selectedText:(NSString *)RowText
{
    if (sender == self.btnPeriodo)
    {
        periodoSeleccionado = MenuIndex;
        self.lblPeriodo.text = RowText;
        //[self.popupMenu dismissPopoverAnimated:YES];
        
        periodoSeleccionado ++;
        [self updateData];
        [self.tableHeader orderBy:fVisitasPorDiasFieldFecha sender:self.tableHeader.lblFecha reset:YES];
        return;
    }
}

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender willSelectObject:(id)selObject
{
    //if (self.type == _kCoberturaEspecialidad)
    //{
    [self.loadingView show:self];
    //        [self.loadingView.view setNeedsDisplay];
    //}
}

#pragma mark -
#pragma mark Header methods
- (void)VisitasPorDiaHeader:(visitasPorDiasHeader*)header
                    selectedField:(tVisitasPorDiasHeaderField)field
                            Order:(NSComparisonResult)order
{
    if ([self.data count] < 1)
        return;
    
    
    // Guardo el total y lo elemino del listado.
    MTstdVisitasPorDia *o = nil;
    NSInteger index = [self.data count]-1;
    
    o = [self.data objectAtIndex:index];
    [self.data removeObject:o];
    
    
    // Busco el string correspondiente a la propiedad.
    NSString *keyName = @"fecha";
    
    if (field == fVisitasPorDiasFieldFijos)
        keyName = @"fijos";
    
    if (field == fVisitasPorDiasFieldFluctuantes)
        keyName = @"fluctuantes";
    
    if (field == fVisitasPorDiasFieldAcompaniadas)
        keyName = @"acompaniadas";
    
    if (field == fVisitasPorDiasFieldTotal)
        keyName = @"total";
    
    BOOL asc = YES;
    if (order == NSOrderedDescending)
        asc = NO;
    
    // Reordeno el listado.
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc]
                                initWithKey:keyName
                                ascending:asc];
    NSArray *sortDescriptors = [NSArray arrayWithObject: sorter];
    [self.data sortUsingDescriptors:sortDescriptors];
    
    // Vuelvo a agregar el total.
    [self.data addObject:o];
    
    [self.tableView reloadData];
}

@end
