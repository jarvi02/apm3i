//
//  MasterViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 24/01/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MasterViewController.h"
#import "MainMenuViewController.h"
#import "LoginMenuViewController.h"
#import "AppDelegate.h"
#import "Config.h"

//#import "commandMasterVC.h"

@interface MasterViewController ()

-(void)pressedMenu:(id)sender;

@end

@implementation MasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"APM3i";
    }
    return self;
}

-(BOOL)hasHeader
{
    return NO;
}


-(UIImage *)getBackgroundImage
{
    return nil;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Actualizo la cantidad de médicos que muestra en el título.
    UILabel *o = (UILabel*)[self.view viewWithTag:TAG_CICLO];
    o.text = [NSString stringWithFormat:@" Ciclo: %@", [[Config shareInstance] find:CICLO]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey];
    self.lblVersion.text = [NSString stringWithFormat:@"Versión %@   ",version];
    

    // Test para formato regional
#ifdef DEBUG_FORMATOREGIONAL
    NSLocale *localeSetting = [NSLocale currentLocale];
    
    NSLog(@"Locale = %@", [localeSetting localeIdentifier]);
#endif
    
    
    
    // Add Menu button
    UIBarButtonItem *menuButton = [[[UIBarButtonItem alloc] initWithTitle:@"Menú"
                                                                    style:UIBarButtonItemStyleBordered
                                                                   target:self
                                                                   action:@selector(pressedMenu:)]
                                   autorelease];
    menuButton.tintColor = UIColorFromRGB(menuButtonColor);
    
    self.navigationItem.leftBarButtonItem = menuButton;
    
    if ( [[[Config shareInstance] find:APP_ACTIVADA] isEqualToString:@"0"] )
    {
        [self.view viewWithTag:883].hidden = YES;
        [self.view viewWithTag:884].hidden = YES;
        [self.view viewWithTag:885].hidden = YES;
    }
    
#ifdef USE_NEW_MENU
    [[commandMasterView sharedInstance] addToView:self.view andLoadGroup:@"MainGroup"];
    [[commandMasterView sharedInstance] setDelegate:self];
#endif
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // este hack es porque la primera ves que se usa la app no se transmitio entonces se muestra vacio,  pero cuando vuelvo de transmitir ya tengo en nombre del nodo.
    ((UILabel*)[self.view viewWithTag:TAG_DESCRIPCION_NODO]).text = SettingForKey(DESCRIPCION_NODO, @"");
}

#pragma mark -
#pragma mark Private Members
                                              
-(void)pressedMenu:(id)sender
{
#ifdef USE_NEW_MENU
    [[commandMasterView sharedInstance] setHidden:NO];
    [[commandMasterView sharedInstance] showIconsAppBar];
#else

    if (!Delegate.popoverController.popoverVisible)
    {
        // Aca poner la validacion de que menu mostrar
        if ( [[[Config shareInstance] find:APP_ACTIVADA] isEqualToString:@"1"] )
        {
            MainMenuViewController * fmvc = [[[MainMenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil] autorelease];
            
  
            
            fmvc.delegate = self;
        
            UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:fmvc] autorelease];
            

            Delegate.popoverController = [[[UIPopoverController alloc] initWithContentViewController:nav] autorelease];
            [Delegate.popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        else
        {
            LoginMenuViewController * fmvc = [[[LoginMenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil] autorelease];
          
            fmvc.delegate = self;
            
            UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:fmvc] autorelease];
            
            Delegate.popoverController = [[[UIPopoverController alloc] initWithContentViewController:nav] autorelease];
            [Delegate.popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }

    }
#endif
}


#pragma mark -
#pragma mark MenuContainerProtocol

-(void)navigateToController:(UIViewController *)controller
{
    [Delegate dismissPopOver:YES];
    //self.navigationController.navigationBar.tintColor = UIColorFromRGB(defaultButtonColor);
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)showModalController:(UIViewController *)controller
{
    [Delegate dismissPopOver:YES];
    //self.navigationController.navigationBar.tintColor = UIColorFromRGB(defaultButtonColor);
    [self.navigationController presentViewController:controller animated:YES completion:NULL];
}


-(void)performTask:(NSString*) task {
    
    [Delegate dismissPopOver:YES];
    
    if ([task isEqualToString:@"SHOW_LOGIN"]) {
       
            [UIView animateWithDuration:0.2
                         animations:^{self.loginView.alpha = 1.0;}
                         completion:^(BOOL finished){ [self.loginView setHidden:NO];}];
        
    }
    
}

#pragma mark -
#pragma mark Memory Management

-(void)dealloc
{
    
    [_service release];
    
    [_loginView release];
    [_txtUser release];
    [_txtPassword release];
    [_spinner release];
    [_lblVersion release];
    [super dealloc];
}

#pragma mark -
#pragma mark Login Methods
- (IBAction)performAccept:(id)sender {
    
    [self.txtUser resignFirstResponder];
    [self.txtPassword resignFirstResponder];
    
    [self.spinner startAnimating];
    self.service = [[[ServiceLogin alloc] init] autorelease];
    self.service.delegate = self;
    [self.service login:self.txtUser.text andPassword:self.txtPassword.text];
    
}

- (IBAction)performCancel:(id)sender {
    [self.txtUser resignFirstResponder];
    [self.txtPassword resignFirstResponder];
    [self hiddeLogin];
}

-(void)hiddeLogin{
    [UIView animateWithDuration:0.2
                     animations:^{self.loginView.alpha = 0.0;}
                     completion:^(BOOL finished){ [self.loginView setHidden:YES];}];
}

#pragma mark -
#pragma mark Login Service Delegate
- (void)requestFinished {
    
    [self.spinner stopAnimating];
    
    self.txtPassword.text = @"";
    //self.txtUser.text = @"";
    
    [self hiddeLogin];
    
    MainMenuViewController * fmvc = [[[MainMenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil] autorelease];
    fmvc.contentSizeForViewInPopover = [fmvc size];
    fmvc.delegate = self;
    
    /*UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:fmvc] autorelease];
    
    [Delegate.popoverController setContentViewController:nav];
    */
    
    // tube que hacer este hack porque la primera vez que se abre la app no tiene cargados la entidad ni el ciclo.
    ((UITextView*)[self.view viewWithTag:883]).text = [NSString stringWithFormat:@" %@", [[Config shareInstance] find:DESCRIPCION_NODO]];
    ((UITextView*)[self.view viewWithTag:884]).text = [NSString stringWithFormat:@" Ciclo: %@", [[Config shareInstance] find:CICLO]];
    
    [self.view viewWithTag:883].hidden = NO;
    [self.view viewWithTag:884].hidden = NO;
    [self.view viewWithTag:885].hidden = NO;
    
    self.service = nil;
    
}
- (void)requestFailed:(NSString*)msg {
    
    [self.spinner stopAnimating];
    [self hiddeLogin];
    [self alertTitle:@"Alerta" mensaje:msg boton:@"Aceptar"];
    self.service = nil;
}

#pragma mark -
#pragma mark CommandMaster methods

- (void)didSelectMenuListItemAtIndex:(NSInteger)index ForButton:(CommandButton *)selectedButton
{
    //textLabel.text = [NSString stringWithFormat:@"index %i of button titled \"%@\"", index, selectedButton.title];
}

- (void)didSelectButton:(CommandButton *)selectedButton
{
    //textLabel.text = [NSString stringWithFormat:@"button titled \"%@\" was selected", selectedButton.title];
}


#pragma mark -
#pragma mark TextField methods
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}





@end
