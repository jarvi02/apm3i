//
//  GenericSelectorViewController.h
//  APM4
//
//  Created by Juan Pablo Garcia on 27/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GenericSelectorDelegate <NSObject>

-(void)optionSelected:(NSString *)option index:(NSInteger)index;

@end

@interface GenericSelectorViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property(nonatomic, retain) NSString * option;
@property(nonatomic, retain) IBOutlet UIPickerView * picker;
@property(nonatomic, assign) id<GenericSelectorDelegate> delegate;
@property(nonatomic, retain) NSArray *options;

- (IBAction)optionChanged;
- (void)optionDone:(id)sender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil options:(NSArray *)options;

@end
