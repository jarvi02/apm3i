//
//  visitasPorDiasCell.h
//  APM4
//
//  Created by Laura Busnahe on 9/11/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface visitasPorDiasCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblFecha;
@property (retain, nonatomic) IBOutlet UILabel *lblFijos;
@property (retain, nonatomic) IBOutlet UILabel *lblFluctuantes;
@property (retain, nonatomic) IBOutlet UILabel *lblAcompaniadas;
@property (retain, nonatomic) IBOutlet UILabel *lblTotal;

@end
