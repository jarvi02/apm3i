//
//  detalleParteCell.h
//  APM4
//
//  Created by Laura Busnahe on 7/22/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface detalleParteCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblFecha;
@property (retain, nonatomic) IBOutlet UILabel *lblActividad;
@property (retain, nonatomic) IBOutlet UILabel *lblApellidoNombre;
@property (retain, nonatomic) IBOutlet UILabel *lblDescripcion;

@end
