//
//  CustomViewController.m
//  RnR
//
//  Created by Fabian E. Pezet Vila on 27/11/12.
//  Copyright (c) 2012 Fabian E. Pezet Vila. All rights reserved.
//

#import "CustomViewController.h"
#import "Config.h"

#define RED     48.0/255.0
#define GREEN   81.0/255.0
#define BLUE    106.0/255.0
#define ALPHA   1.0


@interface CustomViewController ()
- (void) configurarBarra;
- (void) configiurarHeader;
- (void) configurarBackground;
- (void) configurarPie;
@end

@implementation CustomViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self configurarBarra];
    
    if ([self hasHeader]) {
        [self configiurarHeader];
    }
    
    [self configurarBackground];
    
    if ([self hasFooter])
    {
        [self configurarPie];
    }
    
}

- (void) configurarBarra
{
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"Mediforce_30px"]];
	self.navigationItem.titleView = imageView;
    
    if (iOSVersion < 7)
    {
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:ALPHA];
    }
    [self.navigationController.navigationBar setTranslucent:NO];
    
}

- (void) configiurarHeader{
    
    
    // el header termina en y = 62
    
    CGFloat _red, _green, _blue;
    CGRect frame;
    UIImageView *image;
    UILabel *label;
    
    frame = CGRectMake(0, 0, 768, 20);
    label = [[UILabel alloc] initWithFrame:frame];
    _red    = 207.0/255.0;
    _green  = 219.0/255.0;
    _blue   = 119.0/255.0;
    label.backgroundColor = [UIColor colorWithRed:_red green:_green blue:_blue alpha:1.0];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin ;
    [self.view addSubview:label]; 
   
    
    frame = CGRectMake(6, 2, 95, 17);
    image = [[UIImageView alloc] initWithFrame:frame];
    image.image = [UIImage imageNamed:@"b_mediforce"];
    [self.view addSubview:image];
    
    
    frame = CGRectMake(10, 30, 32, 32);
    image = [[UIImageView alloc] initWithFrame:frame];
    image.image = [self getIconoForHeader];
    [self.view addSubview:image];
    
    frame = CGRectMake(55, 31, 690, 30);
    label = [[UILabel alloc] initWithFrame:frame];
    _red    = 50.0/255.0;
    _green  = 79.0/255.0;
    _blue   = 133.0/255.0;
    label.textColor = [UIColor colorWithRed:_red green:_green blue:_blue alpha:1.0];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:27.0];
    label.tag = TAG_TITULO_HEADER;
    label.text = [self getTitleForHeader];
    [self.view addSubview:label];
    
}

- (void) configurarBackground{
    
    if ([self getBackgroundImage] != nil) {
        UIImageView *bg = [[UIImageView alloc] initWithImage:[self getBackgroundImage]];
        bg.contentMode = UIViewContentModeTop;
        bg.opaque = YES;
        [self.view insertSubview:bg atIndex:0];
    }

}

- (void) configurarPie{
    
    CGFloat _red, _green, _blue;
    _red    = 154.0/255.0;
    _green  = 202.0/255.0;
    _blue   = 250.0/255.0;
    
    UITextField *txtfield;
    CGRect frame;
    
    frame = CGRectMake(0, 932, 260, 30);
    txtfield = [[UITextField alloc] initWithFrame:frame];
    txtfield.borderStyle = UITextBorderStyleLine;
    txtfield.autoresizingMask = 14;
    txtfield.enabled = NO;
    txtfield.tag = TAG_DESCRIPCION_NODO;
    txtfield.text = [NSString stringWithFormat:@" %@", [[Config shareInstance] find:DESCRIPCION_NODO]];
    txtfield.backgroundColor = [UIColor colorWithRed:_red green:_green blue:_blue alpha:1.0];
    
    [self.view addSubview:txtfield];

    frame = CGRectMake(255, 932, 260, 30);
    txtfield = [[UITextField alloc] initWithFrame:frame];
    txtfield.borderStyle = UITextBorderStyleLine;
    txtfield.autoresizingMask = 15;
    txtfield.enabled = NO;
    txtfield.tag = TAG_CICLO;
    txtfield.text = [NSString stringWithFormat:@" Ciclo: %@", [[Config shareInstance] find:CICLO]];
    txtfield.backgroundColor = [UIColor colorWithRed:_red green:_green blue:_blue alpha:1.0];
    
    [self.view addSubview:txtfield];
    
    frame = CGRectMake(511, 932, 260, 30);
    txtfield = [[UITextField alloc] initWithFrame:frame];
    txtfield.borderStyle = UITextBorderStyleLine;
    txtfield.autoresizingMask = 11;
    txtfield.enabled = NO;
    txtfield.tag = TAG_TITULO;
    txtfield.text = [NSString stringWithFormat:@" %@", self.title];
    txtfield.backgroundColor = [UIColor colorWithRed:_red green:_green blue:_blue alpha:1.0];
    
    [self.view addSubview:txtfield];
    
}


-(BOOL)hasHeader{
    //NSAssert(0, @"implementar en hijo  -(BOOL)hasHeader{} ");
    return YES;
}

-(BOOL)hasFooter{
    return YES;
}

-(UIImage*)getBackgroundImage{

    //NSAssert(0, @"implementar en hijo -(UIImage*)getBackgroundImage{} ");
    return [UIImage imageNamed:@"bg_APM3i_claro"];
}

-(UIImage*)getIconoForHeader{

    NSAssert(0, @"implementar en hijo -(UIImage*)getIconoForHeader{} ");
    return nil;

}

-(NSString*)getTitleForHeader{

    NSAssert(0, @"implementar en hijo -(NSString*)getTitleForHeader{} ");
    return nil;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) alertTitle:(NSString*) titulo mensaje:(NSString*) mensaje boton:(NSString*) boton{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titulo message:mensaje delegate:nil cancelButtonTitle:boton otherButtonTitles:nil];
    [alert show];
    
}

@end
