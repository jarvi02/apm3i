//
//  Ciclo.m
//  APM4
//
//  Created by Juan Pablo Garcia on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Ciclo.h"
#import "DB.h"
#import "Config.h"

@implementation Ciclo

+ (NSArray*)GetAll {
    
   
    NSMutableArray *ciclos = [[NSMutableArray alloc] initWithCapacity:10];
    
    NSString *sql = @"select descripcion, desde, hasta, orden, proximo, id from lab_ciclos";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    Ciclo *ciclo = nil;
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            ciclo = [[Ciclo alloc] init];
            
            // Descripcion
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if((_descripcion != nil) && (strlen(_descripcion) > 0)) {
                ciclo.ciclo = [NSString stringWithUTF8String:_descripcion];
			}else{
				ciclo.ciclo = @"";
			}
            
            // desde
            double val = sqlite3_column_double(statement, 1);
            ciclo.desde = [NSDate dateWithTimeIntervalSince1970:val];
            
            // hasta
            val = sqlite3_column_double(statement, 2);
            ciclo.hasta = [NSDate dateWithTimeIntervalSince1970:val];
            
            // orden
            ciclo.orden = sqlite3_column_int(statement, 3);
            
            // proximo
            ciclo.proximo = sqlite3_column_int(statement, 4);
            
            // id
            ciclo.recID = sqlite3_column_int(statement, 5);
        
        
            [ciclos addObject:ciclo];
            [ciclo release];
        
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla lab_ciclo");
	}
	sqlite3_finalize(statement);
    
    return [ciclos autorelease];
    
}

+(Ciclo *)getById:(NSUInteger)orden
{
    Ciclo *ciclo = nil;
    
    NSString *sql = [NSString stringWithFormat:@"select descripcion, desde, hasta, orden, proximo, id from lab_ciclos where orden = %d", orden];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
            
            ciclo = [[Ciclo alloc] init];
            
            // Descripcion
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if((_descripcion != nil) && (strlen(_descripcion) > 0)) {
                ciclo.ciclo = [NSString stringWithUTF8String:_descripcion];
			}else{
				ciclo.ciclo = @"";
			}
            
            // desde
            double val = sqlite3_column_double(statement, 1);
            ciclo.desde = [NSDate dateWithTimeIntervalSince1970:val];
            
            // hasta
            val = sqlite3_column_double(statement, 2);
            ciclo.hasta = [NSDate dateWithTimeIntervalSince1970:val];
            
            // orden
            ciclo.orden = sqlite3_column_int(statement, 3);
            
            // proximo
            ciclo.proximo = sqlite3_column_int(statement, 4);
            
            // id
            ciclo.recID = sqlite3_column_int(statement, 5);
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla lab_ciclo");
	}
	sqlite3_finalize(statement);
    
    return [ciclo autorelease];
}

+(Ciclo *)actual
{
    return [Ciclo getById:[[[Config shareInstance] find:CICLO] intValue]];
}

-(NSString *)description
{
//    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
//    [formatter setDateFormat:@"dd/MM/yyyy"];
//    
//    return [NSString stringWithFormat:@"%@ - %@ - %@", self.ciclo, [formatter stringFromDate:self.desde], [formatter stringFromDate:self.hasta]];

    return [NSString stringWithFormat:@"%@ - %@ - %@",
            self.ciclo,
            [self.desde formattedStringUsingFormat:@"dd/MM/yyyy"],
            [self.hasta formattedStringUsingFormat:@"dd/MM/yyyy"]];
}

-(void)dealloc
{
    [_desde release];
    [_hasta release];
    [_ciclo release];
    
    [super dealloc];
}

@end
