//
//  MTHistoricoVisitas.m
//  APM4
//
//  Created by Laura Busnahe on 7/16/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTHistoricoVisitas.h"
#import "Config.h"
#import "ConfigGlobal.h"
#import "DBExtended.h"

#import "NSStringExtended.h"


@implementation MTHistoricoVisitas

- (id)init
{
    self = [super init];
    if (self)
    {
        self.ciclo01 = @"";
        self.ciclo02 = @"";
        self.ciclo03 = @"";
        self.ciclo04 = @"";
        self.ciclo05 = @"";
        self.ciclo06 = @"";
        self.ciclo07 = @"";
        self.ciclo08 = @"";
        self.ciclo09 = @"";
        self.ciclo10 = @"";
        self.ciclo11 = @"";
        self.ciclo12 = @"";
        
        self.tipo = @"";
        self.total = @"";
    }
    return self;
}

+ (NSArray*)getAllForMedico:(NSUInteger)idMedico
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    // Debo definir estas variables ya que la estadística de la base de datos devuelve mal los totales.
    NSInteger total01 = 0;          NSInteger total02 = 0;
    NSInteger total03 = 0;          NSInteger total04 = 0;
    NSInteger total05 = 0;          NSInteger total06 = 0;
    NSInteger total07 = 0;          NSInteger total08 = 0;
    NSInteger total09 = 0;          NSInteger total10 = 0;
    NSInteger total11 = 0;          NSInteger total12 = 0;
    
    NSInteger totalFila = 0;
    
    
    NSString *sql = @"";
    
    sql = @"SELECT "
                "nodo, "
                "medico, "
                "rec, "
                "cell1, "
                "cell2, "
                "cell3, "
                "cell4, "
                "cell5, "
                "cell6, "
                "cell7, "
                "cell8, "
                "cell9, "
                "cell10, "
                "cell11, "
                "cell12, "
                "total "
        "FROM std_historicovisitas "
        "WHERE nodo = %@ AND medico = %d ORDER BY rec";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql, nodo, idMedico];
    
#ifdef DEBUG_HISTORICO
    NSLog(@"--- Historico visita ---");
    NSLog(@" query: %@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement )
    {
        
        MTHistoricoVisitas *o;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[MTHistoricoVisitas alloc] init];
            totalFila = 0;
            
            char *_d;
            
            // Tipo
            int _n = sqlite3_column_int(statement, 2);
            switch (_n)
            {
                case 1:
                    o.tipo = @"Visita";
                    break;
                case 2:
                    o.tipo = @"Revisita";
                    break;
                case 3:
                    o.tipo = @"Revisita 2";
                    break;
                case 4:
                    o.tipo = @"Revisita 3";
                    break;
                case 5:
                    o.tipo = @"Total";
                    break;
                case 6:
                    o.tipo = @"Acomp.";
                    break;
                    
                default:
                    break;
            }
            NSString *_texto;
            
            // cell1
            _d = (char *)sqlite3_column_text(statement, 3);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total01++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";

                }
            }
            o.ciclo01 = [NSString string:_texto ifNull:@""];
			
            
            // cell2
            _d = (char *)sqlite3_column_text(statement, 4);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total02++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                    
                }
            }
                o.ciclo02 = [NSString string:_texto ifNull:@""];
			
            
            // cell3
            _d = (char *)sqlite3_column_text(statement, 5);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total03++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                    
                }
            }
                o.ciclo03 = [NSString string:_texto ifNull:@""];
			
            
            // cell4
            _d = (char *)sqlite3_column_text(statement, 6);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total04++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                    
                }
            }
                o.ciclo04 = [NSString string:_texto ifNull:@""];
			
            
            // cell5
            _d = (char *)sqlite3_column_text(statement, 7);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total05++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                    
                }
            }
                o.ciclo05 = [NSString string:_texto ifNull:@""];
			
            
            // cell6
            _d = (char *)sqlite3_column_text(statement, 8);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total06++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                    
                }
            }
                o.ciclo06 = [NSString string:_texto ifNull:@""];
			
            
            // cell7
            _d = (char *)sqlite3_column_text(statement, 9);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total07++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                    
                }
            }
                o.ciclo07 = [NSString string:_texto ifNull:@""];
			
            
            // cell8
            _d = (char *)sqlite3_column_text(statement, 10);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total08++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                    
                }
            }
                o.ciclo08 = [NSString string:_texto ifNull:@""];
			
            
            // cell9
            _d = (char *)sqlite3_column_text(statement, 11);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total09++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                    
                }
            }
            o.ciclo09 = [NSString string:_texto ifNull:@""];
			
            
            // cell10
            _d = (char *)sqlite3_column_text(statement, 12);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total10++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                    
                }
            }
            o.ciclo10 = [NSString string:_texto ifNull:@""];
			
            
            // cell11
            _d = (char *)sqlite3_column_text(statement, 13);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total11++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                    
                }
            }
            o.ciclo11 = [NSString string:_texto ifNull:@""];
			
            
            // cell12
            _d = (char *)sqlite3_column_text(statement, 14);
            _texto = @"---";
            if((_d != nil) && (strlen(_d) > 0))
            {
                _texto = [NSString stringWithUTF8String:_d];
                if (_n < 5)
                {
                    if ([_texto integerValue] > 1)
                    {
                        total12++;
                        totalFila++;
                        NSDate *_date  = [[NSDate dateWithTimeIntervalSince1970:[_texto integerValue]] dateAsDateWithoutTime];
                        _texto = [_date formattedStringUsingFormat:@"dd/MM/yyyy"];
                    } else
                        _texto = @"---";
                        
                    
                }
            }
            o.ciclo12 = [NSString string:_texto ifNull:@""];
			
            
            // Total
            _d = (char *)sqlite3_column_text(statement, 15);
            if((_d != nil) && (strlen(_d) > 0))
            {
                //o.total = [NSString stringWithUTF8String:_d];
                o.total = [NSString intToStr:totalFila];
			}
            
            if (_n == 5)
            {
                totalFila = total01 + total02 + total03 + total04 + total05 + total06 +
                            total07 + total08 + total09 + total10 + total11 + total12;
                
                o.ciclo01 = [NSString intToStr:total01];
                o.ciclo02 = [NSString intToStr:total02];
                o.ciclo03 = [NSString intToStr:total03];
                o.ciclo04 = [NSString intToStr:total04];
                o.ciclo05 = [NSString intToStr:total05];
                o.ciclo06 = [NSString intToStr:total06];
                o.ciclo07 = [NSString intToStr:total07];
                o.ciclo08 = [NSString intToStr:total08];
                o.ciclo09 = [NSString intToStr:total09];
                o.ciclo10 = [NSString intToStr:total10];
                o.ciclo11 = [NSString intToStr:total11];
                o.ciclo12 = [NSString intToStr:total12];
                o.total   = [NSString intToStr:totalFila];
                
            }
                
            
            [array addObject:o];
        }
    
        
	} else
    {
        NSAssert(0, @"No se pudo ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return array;
}

@end
