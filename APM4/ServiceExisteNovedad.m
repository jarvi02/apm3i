//
//  ServiceExisteNovedad.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ServiceExisteNovedad.h"
#import "Config.h"

@implementation ServiceExisteNovedad


-(BOOL) existeNovedad:(NSString*)urlBase {
    
    BOOL respuesta = NO;
    @autoreleasepool {
        NSString *urlFormated = [NSString stringWithFormat:@"%@%@?USER=%@&CMD=SVC_EXISTS_FILE&NAME=%@.SWR",
                                 urlBase,
                                 SettingForKey(REMOTE_PATH, @""),
                                 SettingForKey(ENTIDAD, @""),
                                 SettingForKey(TRANID, @"")];
        
        NSURL *url = [NSURL URLWithString:urlFormated];
        
#ifdef DEBUG_TRANSMICION
        NSLog(@" - existeNovedad: %@", urlFormated);
#endif
        
        self.request = [ASIHTTPRequest requestWithURL:url];
        [self.request startSynchronous];
        
        NSError *error = [self.request error];
        if (!error) {
            
            NSString *response = [self.request responseString];
            [self logResponse:response];
            respuesta = ([response isEqualToString:@"1"]) ? YES:NO;
        }
    }
    
    return respuesta;
}

- (NSString*)textoParaLog {
    return @"ServiceExisteNovedad";
}

@end
