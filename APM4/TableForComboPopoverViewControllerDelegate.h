//
//  TableForComboPopoverViewControllerDelegate.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TableForComboPopoverViewControllerDelegate

@required
-(void)selectedIndex:(NSInteger)index caller:(NSString*)caller;  // caller sirve como flag cuando uso esta clases varias veces en el mismo controller y necesito saber quien me llamo

@end
