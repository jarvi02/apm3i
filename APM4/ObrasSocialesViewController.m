//
//  ObrasSocialesViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ObrasSocialesViewController.h"
#import "ObraSocial.h"
#import "EntryForCombo.h"

@interface ObrasSocialesViewController ()
@property(nonatomic, retain) NSArray *obrasSociales;
@property(nonatomic, retain) NSMutableArray *obrasSocialesOriginal;
-(void)crearToolBar;
@end

@implementation ObrasSocialesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Regionalización
    self.lblTitulo.text = [[MTRegionalDictionary sharedInstance] valueForText:@"Obras Sociales"];
    self.lblTable.text  = [[MTRegionalDictionary sharedInstance] valueForText:@"Obras Sociales"];

    self.obrasSociales = [ObraSocial GetAll];
    
    self.obrasSocialesOriginal = [NSMutableArray array];
    for (ObraSocial *o in self.abmMedicoReference.medico.obrasSociales) {
        [self.obrasSocialesOriginal addObject:[o copy]];
    }
    
    [self crearToolBar];
    
        
}

-(void)crearToolBar {
    // flex item used to separate the left groups items and right grouped items
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    // create a special tab bar item with a custom image and title
    UIBarButtonItem *guardarItem = [[UIBarButtonItem alloc] initWithTitle:@"Guardar"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(guardar:)];
    guardarItem.tintColor = UIColorFromRGB(green_iOS7Color);
    guardarItem.width = 70;
    
    UIBarButtonItem *editarItem = [[UIBarButtonItem alloc] initWithTitle:@"Editar"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(editar:)];
    //editarItem.tintColor = [UIColor grayColor];
    editarItem.width = 70;
    
    UIBarButtonItem *cancelarItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar"
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self
                                                                    action:@selector(cancelar:)];
    //cancelarItem.tintColor = [UIColor grayColor];
    cancelarItem.width = 70;
    
    NSArray *items = [NSArray arrayWithObjects: flexItem, guardarItem, editarItem, cancelarItem, flexItem, nil];
    [self.toolBar setItems:items animated:NO];
    
    [flexItem release];
    [guardarItem release];
    [cancelarItem release];
    [editarItem release];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    [_obrasSociales release];
    [_obrasSocialesOriginal release];
    
    [_txtObraSocial release];
    [_tableView release];
    [_toolBar release];
    [_lblTitulo release];
    [_lblTable release];
    [super dealloc];
}
- (IBAction)obraSocial:(id)sender {
    
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    NSMutableArray *entries = [[NSMutableArray alloc] initWithCapacity:0];
    EntryForCombo *e;
    
    for (int i=0; [self.obrasSociales count] > i; i++) {
        
        ObraSocial *o = [self.obrasSociales objectAtIndex:i];
        
        e = [[EntryForCombo alloc] initWithValue:o.descripcion forIndex:i];
        
        [entries addObject:e];
        [e release];
    }
    
    
    CGSize size = CGSizeMake(350, 400);
    TableForComboPopoverViewController *t = [[TableForComboPopoverViewController alloc] initWithData:entries
                                                                titulo:[[MTRegionalDictionary sharedInstance] valueForText:@"Obras Sociales"]
                                                                size:size
                                                                delegate:self
                                                                andCaller:@"OBRASOCIAL"];
    t.aligment = NSTextAlignmentLeft;
    
    [entries release];
    
    Delegate.popoverController = [[[UIPopoverController alloc] initWithContentViewController:t] autorelease];
    [t release];
    
    [Delegate.popoverController presentPopoverFromRect: ((UIButton*)sender).frame inView:self.view permittedArrowDirections:YES animated:YES];
    
}

-(void)guardar:(id)sender{
    
    // agrego la coleccion de la tabla a los medicos
    if ([self.abmMedicoReference.medico.obrasSociales count] >0) {
        ObraSocial *o = [self.abmMedicoReference.medico.obrasSociales objectAtIndex:0];
        NSString *obraSoc = [NSString stringWithFormat:@"%@",o.descripcion];
        self.abmMedicoReference.txtObreSocial.text = obraSoc;
    } else {
        self.abmMedicoReference.txtObreSocial.text = @"";
    }
    
    [self.abmMedicoReference closeModalView];
}
-(void)cancelar:(id)sender{
    
    [self.abmMedicoReference.medico.obrasSociales removeAllObjects];
    
    for (ObraSocial *o in self.obrasSocialesOriginal) {
        [self.abmMedicoReference.medico.obrasSociales addObject:[o copy]];
    }
    
    [self.abmMedicoReference closeModalView];
    
}

-(void)editar:(id)sender{
    
    
    if (self.tableView.editing == NO) {
        [self.tableView setEditing:YES animated:YES];
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:2]).tintColor = [UIColor blueColor];
        
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:1]).enabled =NO;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:3]).enabled =NO;
        
        
    } else {
        [self.tableView setEditing:NO animated:YES];
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:2]).tintColor = [UIColor grayColor];
        
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:1]).enabled =YES;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:3]).enabled =YES;
        
    }
    
    
}

#pragma - mark
#pragma - Combo delegates methods

-(void)selectedIndex:(NSInteger)index caller:(NSString*)caller {
    
    [Delegate dismissPopOver:YES];
    
    if ([caller isEqualToString:@"OBRASOCIAL"]) {
        
        // agrego la obra social si no esta
        ObraSocial *e = [self.obrasSociales objectAtIndex:index];
        BOOL flag = YES;
        for (ObraSocial *o in self.abmMedicoReference.medico.obrasSociales) {
            
            if (e.idObraSocial == o.idObraSocial) {
                flag = NO;
            }
            
        }
        
        if (flag) {
            
            [self.abmMedicoReference.medico.obrasSociales addObject:[e copy]];
            self.txtObraSocial.text = e.descripcion;
            [self.tableView reloadData];
        }
        
        
    }  else {
        // jamas deberia entrar aca
    }
    
}


#pragma - mark
#pragma UItableView Delegate and DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.abmMedicoReference.medico.obrasSociales count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    ObraSocial *e = [self.abmMedicoReference.medico.obrasSociales objectAtIndex:indexPath.row];
    cell.textLabel.text = e.descripcion;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.abmMedicoReference.medico.obrasSociales removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
    
    ObraSocial *ingredient = [self.abmMedicoReference.medico.obrasSociales objectAtIndex:fromIndexPath.row];
    
    ingredient = [ingredient copy];
    
    [self.abmMedicoReference.medico.obrasSociales removeObjectAtIndex:fromIndexPath.row];
    
    [self.abmMedicoReference.medico.obrasSociales insertObject:ingredient atIndex:toIndexPath.row];
    
    
    
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // nada
    
}


@end
