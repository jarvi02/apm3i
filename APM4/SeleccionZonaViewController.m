//
//  SeleccionZonaViewController.m
//  APM4
//
//  Created by Laura Busnahe on 8/27/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "SeleccionZonaViewController.h"
#import "CustomViewController.h"
#import "Config.h"

@interface SeleccionZonaViewController ()

@end

@implementation SeleccionZonaViewController
{
    MTNodo* _selected;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.data          = nil;
        self.filteredData  = nil;
    }
    return self;
}

- (id)initWithDefaultNib
{
    self = [self initWithNibName:@"SeleccionZonaViewController" bundle:nil];
    self.modalPresentationStyle = UIModalPresentationFormSheet;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.btnAtras.tintColor = UIColorFromRGB(defaultButtonColor);
    
    
    self.data         = [NSMutableArray arrayWithArray:[MTNodo getAllforEntidad:[SettingForKey(ENTIDAD, @"0") integerValue]]];
    self.filteredData = [NSMutableArray arrayWithArray:self.data];
    
    _selected = [[MTNodo alloc] init];
    _selected.recID = [SettingForKey(NODO, @"") integerValue];
    
    [self.tblZonas reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark Tap methods

- (IBAction)tapAtras:(id)sender
{
    [self dismiss];
}

#pragma mark -
#pragma mark Table Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of time zone names.
    
	return [self.filteredData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *MyIdentifier = @"grupoCellIdentifier";
	
    // Obtain the cell object.
	UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        // Use the default cell style.
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
    }
    
    NSString *cellText = @"";
    MTNodo *_nodo = nil;
    
    _nodo = [self.filteredData objectAtIndex:indexPath.row];
    cellText = [_nodo getDescription];
 
    
	cell.textLabel.text = [NSString stringWithFormat:@"%@", cellText];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    if (_nodo.recID == _selected.recID)
    {
        cell.imageView.image = [UIImage imageNamed:@"ok_32"];
        //cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:@"ok_32_off"];
        //cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _selected = [self.filteredData objectAtIndex:indexPath.row];
    
    [self cambiarANodo:_selected];
    
    [self.tblZonas reloadData];
    
    // Chequeo si fue llamado o no con un objecto de referencia de la cartera.
}

- (void)refreshTable
{
    [self.tblZonas reloadData];
}



#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredData removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    if ((searchText != nil) && ([searchText length] > 0))
    {
        if (self.data.count > 0)
        {
            // En caso de no ser una entidad, compruebo si responde al selector getDescription
            if ([[self.data objectAtIndex:0] respondsToSelector:@selector(getDescription)])
            {
                for (id item in self.data)
                {
                    NSRange range = [[item getDescription] rangeOfString:searchText
                                                                 options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                    
                    if ( range.length > 0)
                    {
                        [self.filteredData addObject:item];
                    }
                }
            } else
            {
                // Para los objetos de tipo NSString.
                for (NSString *item in self.data)
                {
                    NSRange range = [item rangeOfString:searchText
                                                options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                    
                    if ( range.length > 0)
                    {
                        [self.filteredData addObject:item];
                    }
                }
            }
        }
    } else
    {
        // Si no hay texto de búsqueda, copio todos los elementos.
        for (id item in self.data)
            [self.filteredData addObject:item];
    }
    
    
    [self refreshTable];
}


#pragma mark -
#pragma mark Search bar delegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)aSearchBar
{
    
}


- (void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar
{
    
    // If the user finishes editing text in the search bar by, for example:
    // tapping away rather than selecting from the recents list, then just dismiss the popover
    //
    
    // dismiss the popover, but only if it's confirm UIActionSheet is not open
    //  (UIActionSheets can take away first responder from the search bar when first opened)
    //
    // the popover's main view controller is a UINavigationController; so we need to inspect it's top view controller
    //
    [aSearchBar resignFirstResponder];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    // When the search string changes, filter the recents list accordingly.
    [self filterContentForSearchText:searchText scope:@""];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self.view endEditing:YES];
}

// En vistas que se presentan de forma modal es necesario agregar este override para que oculte el teclado
// con resignFirstResponder
- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

#pragma mark -
#pragma mark Nodo change methods
- (void)cambiarANodo:(MTNodo*)nodo
{
   [[Config shareInstance] updateNodo:nodo];
  
    
    // Actualizo los datos del nodo.
    UIView* v = [self.delegate view];
    ((UILabel*)[v viewWithTag:TAG_DESCRIPCION_NODO]).text = SettingForKey(DESCRIPCION_NODO, @"");
    //((UILabel*)[v viewWithTag:TAG_CICLO]).text = SettingForKey(CICLO, @"");
}

@end
