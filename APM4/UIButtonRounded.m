//
//  UIButtonRounded.m
//  APM4
//
//  Created by Laura Busnahe on 9/23/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "UIButtonRounded.h"

@implementation UIButtonRounded

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void) drawRect:(CGRect)rect
{
    //[super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0);
    [[UIColor lightGrayColor] setStroke];
    [[UIColor whiteColor] setFill];
    
    CGPathRef path = [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:8.0f] CGPath];

    // Usar null en lugar de affineTransform si se usan las lineas comentadas abajo para CGPathRef
    
//    CGContextMoveToPoint(context, rect.origin.x, rect.origin.y + radius);
//    CGContextAddLineToPoint(context, rect.origin.x, rect.origin.y + rect.size.height - radius);
//    CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + rect.size.height - radius,
//                    radius, M_PI, M_PI / 2, 1); //STS fixed
//    CGContextAddLineToPoint(context, rect.origin.x + rect.size.width - radius,
//                            rect.origin.y + rect.size.height);
//    CGContextAddArc(context, rect.origin.x + rect.size.width - radius,
//                    rect.origin.y + rect.size.height - radius, radius, M_PI / 2, 0.0f, 1);
//    CGContextAddLineToPoint(context, rect.origin.x + rect.size.width, rect.origin.y + radius);
//    CGContextAddArc(context, rect.origin.x + rect.size.width - radius, rect.origin.y + radius,
//                    radius, 0.0f, -M_PI / 2, 1);
//    CGContextAddLineToPoint(context, rect.origin.x + radius, rect.origin.y);
//    CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + radius, radius,
//                    -M_PI / 2, M_PI, 1);
//    CGContextFillPath(context);
    
    CGContextAddPath(context, path);
    CGContextDrawPath(context, kCGPathFillStroke);
    
}

@end
