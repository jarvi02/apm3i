//
//  FichaProductosViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 19/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "FichaProductosViewController.h"
#import "PromocionadosViewController.h"
#import "ProgramadosTablaViewController.h"
#import "Promocionados.h"
#import "Programados.h"

#import "historicoTableController.h"
#import "MTHistoricoProductos.h"

#import "NSStringExtended.h"

@interface FichaProductosViewController ()
@property(nonatomic, strong) PromocionadosViewController    *promocionadosTableViewController;
@property(nonatomic, strong) ProgramadosTablaViewController *programadosTableViewController;
@property(nonatomic, strong) historicoTableController       *historicoTableViewController;
@end

@implementation FichaProductosViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Ficha de Productos";
    }
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];


    NSInteger idDoctor = self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico;
    NSArray *array;
    
    array = [Programados GetAllForDoctor:idDoctor];
    
    //self.programadosTableViewController = [[[ProgramadosTablaViewController alloc] initWithData:array] autorelease];
    self.programadosTableViewController     = [[ProgramadosTablaViewController alloc] initWithData:array];
    self.programadosTableView.delegate      = self.programadosTableViewController;
    self.programadosTableView.dataSource    = self.programadosTableViewController;
    
    
    array = [Promocionados GetAllForDoctor:idDoctor];
    //self.promocionadosTableViewController = [[[PromocionadosViewController alloc] initWithData:array] autorelease];
    self.promocionadosTableViewController   = [[PromocionadosViewController alloc] initWithData:array];
    self.promocionadosTableView.delegate    = self.promocionadosTableViewController;
    self.promocionadosTableView.dataSource  = self.promocionadosTableViewController;
    
    ((UILabel*)[self.view viewWithTag:TAG_TITULO_HEADER]).text = [NSString stringWithFormat:@"%@ - %@",self.title, self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre];
    
    ((UILabel*)[self.view viewWithTag:TAG_TITULO]).text = self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre;
    
    array = [MTHistoricoProductos getAllForMedico:idDoctor];
    self.historicoTableViewController   = [[historicoTableController alloc] initWithData:array];
    self.historicoTable.delegate        = self.historicoTableViewController;
    self.historicoTable.dataSource      = self.historicoTableViewController;
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)dealloc
//{
//    [_programadosTableViewController release];
//    [_promocionadosTableViewController release];
//    
//    [_programadosTableView release];
//    [_promocionadosTableView release];
//    [_planificadoView release];
//    [_historicoView release];
//    [_btnSegment release];
//    [_historicoTable release];
//    [_historicoHeaderView release];
//    [super dealloc];
//}

-(NSString *)getTitleForHeader
{
    return @"Ficha de productos";
}

-(UIImage *)getIconoForHeader
{
    return [UIImage imageNamed:@"cabecera_principal_32"];
}

- (void)viewWillAppear:(BOOL)animated    // Called when the view is about to made visible. Default does nothing
{
    [super viewWillAppear:animated];
    
    if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        // Portrait
        self.historicoView.contentSize = self.historicoTable.frame.size;
        
    } else if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        // Landscape
        self.historicoView.contentSize = self.historicoTable.frame.size;
    }
}


#pragma mark -
#pragma mark Rotations methods

// Notifies when rotation begins, reaches halfway point and ends.
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
{
    // Tengo que deshabilitar la interacción con el usuario o al rotarlo descajeta todo.
    [self.historicoView setUserInteractionEnabled:NO];
    [self.historicoTable setUserInteractionEnabled:NO];
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    // Tengo que deshabilitar la interacción con el usuario o al rotarlo descajeta todo.
    [self.historicoView setUserInteractionEnabled:YES];
    [self.historicoTable setUserInteractionEnabled:YES];
    
    if(UIInterfaceOrientationIsPortrait(fromInterfaceOrientation))
    {
        // Landscape
        self.historicoView.contentSize = self.historicoTable.frame.size;
        
    } else if(UIInterfaceOrientationIsLandscape(fromInterfaceOrientation))
    {
        // Portrait
        self.historicoView.contentSize = self.historicoTable.frame.size;
    }
}


#pragma mark -
#pragma mark Tap methods

- (IBAction)tapSegments:(id)sender
{
    if (self.btnSegment.selectedSegmentIndex == 0)
    {
        // Muestro la vista de Planificación/Realizado
        [self.planificadoView setHidden:NO];
        [self.historicoView setHidden:YES];
    } else
    {
        // Muestro la vista de Histórico de productos
        [self.planificadoView setHidden:YES];
        [self.historicoView setHidden:NO];
    }
}


@end
