//
//  ServiceLongitudArchivoNovedad.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 29/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Service.h"

@interface ServiceLongitudArchivoNovedad : Service

-(NSInteger) longitud:(NSString*)urlBase;

@end
