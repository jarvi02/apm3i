//
//  TrazabilidadFamiliaViewController.h
//  APM4
//
//  Created by Lionel Fiszman on 31/7/15.
//  Copyright (c) 2015 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrazabilidadViewController.h"
@interface TrazabilidadFamiliaViewController : UITableViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>
@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;
@property (retain, nonatomic) TrazabilidadViewController* detailVC;
@property NSUInteger medicoId;
@property int ordenCounter;

@property(nonatomic, strong) NSArray        *productosActuales;
@property(nonatomic, strong) NSMutableArray *productos;
@property(nonatomic, strong) NSMutableArray *productosModificados;
@property(nonatomic, strong) NSMutableArray *productosPromocionados;
@property(nonatomic) NSUInteger         idMedico;



@end
