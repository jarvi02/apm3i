//
//  DetalleParteViewController.h
//  APM4
//
//  Created by Laura Busnahe on 7/22/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "detalleParteHeader.h"
#import "detalleParteCell.h"
#import "popListView.h"

@interface DetalleParteViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate, MTpopListViewDelegate>

@property (retain, nonatomic) IBOutlet UITextField  *lblFecha;
@property (retain, nonatomic) IBOutlet UIButton     *btnFecha;

@property (nonatomic, strong) NSArray               *data;
@property (nonatomic, strong) UINib                 *cellNib;
@property (nonatomic, strong) UINib                 *headerNib;

@property (nonatomic, strong) IBOutlet detalleParteCell    *tmpCell;
@property (strong, nonatomic) IBOutlet detalleParteHeader  *tableHeader;
@property (retain, nonatomic) IBOutlet UITableView        *tableView;

@property (strong, nonatomic) NSArray               *arrayCombo;
@property (strong, nonatomic) MTpopListView         *popupMenu;



- (id)initViewDefaultNib;


- (IBAction)tapBtnFecha:(id)sender;

@end
