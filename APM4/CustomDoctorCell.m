//
//  CustomDoctorCell.m
//  CustomCell
//
//  Created by Laura Busnahe on 2/4/13.
//  Copyright (c) 2013 Laura Busnahe. All rights reserved.
//

#import "CustomDoctorCell.h"

@implementation CustomDoctorCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_image release];
    [_lbNombreMedico release];
    [_lbVisto release];
    [_lbLugar release];
    [_lbEspecialidad release];
    [_lbTurno release];
    [_lbDiaHora release];
    [_lblVisRealizadas release];
    [_lblVisProgramadas release];
    [_lblPotencial release];
    [super dealloc];
}
@end
