//
//  CustomProductoHeader.h
//  APM4
//
//  Created by Laura Busnahe on 7/23/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomProductoHeader : UIView

@property (retain, nonatomic) IBOutlet UIButton *lblPromocionado;
@end
