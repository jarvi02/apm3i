//
//  MTVisitaData.h
//  APM4
//
//  Created by Laura Busnahe on 8/15/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTMedico.h"

@interface MTVisitaData : NSObject

@property(nonatomic, strong) NSDate   *fecha;
@property(nonatomic, strong) NSString *descripcionTipo;
@property(nonatomic, strong) NSString *domicilio;
@property(nonatomic, strong) NSString *objetivos;
@property(nonatomic, strong) NSString *evaluacion;

@property(nonatomic)NSUInteger idVisita;
@property(nonatomic)NSUInteger idTipo;
@property(nonatomic)NSUInteger idDomicilio;

@property(nonatomic)NSUInteger idEstado;
@property(nonatomic)NSUInteger contacto;

@property(nonatomic, strong) MTMedico       *medico;
@property(nonatomic, strong) NSMutableArray *productos;
@property(nonatomic, strong) NSString       *actividad;

- (MTVisitaData*)copyWithZone:(NSZone*)zone;

- (NSString *)getForNovedad;

- (void)save;
- (void)update;

- (void)guardarNuevaVisita;
- (void)guardarProductos;
- (void)actualizarParteDiario;

- (NSUInteger)newRecID;


@end
