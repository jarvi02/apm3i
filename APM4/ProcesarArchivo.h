//
//  ProcesarArchivo.h
//  borrar
//
//  Created by Fabian E. Pezet Vila on 24/03/13.
//  Copyright (c) 2013 Fabian E. Pezet Vila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransmitirViewController.h"

@interface ProcesarArchivo : NSObject <NSStreamDelegate> {

}

@property(nonatomic, assign) UILabel *lbStatusTransmisionViewController;
@property(nonatomic, assign) UIProgressView *progressView;

-(BOOL) start:(NSString*)filePath;

@end
