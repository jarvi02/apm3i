//
//  historicoVisitasTableControllerViewController.h
//  APM4
//
//  Created by Laura Busnahe on 7/15/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "historicoVisitaCell.h"
#import "historicoVisitaHeader.h"

@interface historicoVisitasTableController : UITableViewController

@property (nonatomic, strong) NSArray                       *data;
@property (nonatomic, strong) IBOutlet historicoVisitaCell  *tmpCell;
@property (nonatomic, strong) UINib                         *cellNib;
@property (nonatomic, strong) UINib                         *headerNib;

@property (retain, nonatomic) IBOutlet historicoVisitaHeader  *tableHeader;

- (id)initWithData:(NSArray*)aData;


@end
