//
//  DBExtended.h
//  APM4
//
//  Created by Laura Busnahe on 4/8/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "DB.h"

@interface DB (extensions)

- (void)setDBVersion:(NSInteger)versionNumber;      // Establece el número de versión de la base de datos.
- (NSString*)getDBVersion;                          // Obtiene el número de versión de la base de datos.
- (BOOL)updateDB;

- (NSInteger)getNewIDForTable:(NSString*)tableName idNameOrNil:(NSString*)strID whereOrNil:(NSString*)strWhere;   // Obtiene el siguiente ID para hacer un insert en la tabla pasada como parámetro. El parámetro Where se utiliza para filtrar en caso de que el id dependa también de otro campo.

- (NSUInteger)existsRowForTable:(NSString*)tableName condition:(NSString*)strWhere;     // DEvuelve la cantidad de registros que posee la tabla según el parámetro pasado como condition.

- (NSMutableArray*)getTableColumnsList:(NSString*)table;

@end
