//
//  EntryForCombo.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "EntryForCombo.h"

@implementation EntryForCombo

@synthesize description;

-(id)initWithValue:(NSString*) aDescription forIndex:(NSInteger) aIndex{
    
    
    
    self = [super init];
    if (self) {
        self.description = aDescription;
        self.index = aIndex;
        
    }
    return self;
}

-(void)dealloc{

    [self.description release];
    [super dealloc];
    
}

@end
