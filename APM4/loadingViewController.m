//
//  loadingViewController.m
//  APM4
//
//  Created by Laura Busnahe on 7/18/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "loadingViewController.h"
#import "UIView+extensions.h"

@interface loadingViewController ()

@end

@implementation loadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (id)initWithDefaultNib;
{
    self = [self initWithNibName:@"loadingViewController" bundle:nil];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.00f]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)show:(UIViewController*)owner
{
    [self.view setHidden:YES];
    self.view.frame = owner.view.frame;
    [owner.view setUserInteractionEnabled:NO];
    [owner.view addSubview:self.view];
    [self.view setHiddenAnimated:NO];
    
}

- (void)hide
{
    [self.view setHiddenAnimated:YES];
    [self.view.superview setUserInteractionEnabled:YES];
    //[self.view removeFromSuperview];
}

@end
