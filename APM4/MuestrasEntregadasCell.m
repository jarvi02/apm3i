//
//  MuestrasEntregadasCell.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MuestrasEntregadasCell.h"

@implementation MuestrasEntregadasCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lbFecha release];
    [_lbNombreMedico release];
    [_lbProducto release];
    [_lbCodigo release];
    [_lbLote release];
    [_lbMuestras release];
    [_lbFechaVto release];
    [super dealloc];
}
@end
