//
//  ServiceLogin.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 01/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"

@interface ServiceLogin : Service

- (void)login:(NSString*) user andPassword:(NSString*) password;

@end
