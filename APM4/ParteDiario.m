//
//  ParteDiario.m
//  APM4
//
//  Created by Juan Pablo Garcia on 20/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ParteDiario.h"
#import "ParteDiarioData.h"
#import "DB.h"
#import "Config.h"
#import "Novedad.h"

#import "NSDate+extensions.h"
#import "GlobalDefines.h"

@implementation ParteDiario

- (id) init
{
    self = [super init];
    
    if (self)
    {
        self.maniana = nil;
        self.tarde   = nil;
        self.descripcion = @"";
        self.identificador = 0;
        self.fecha = nil;
        self.transmitido = NO;
    }
    
    return self;
}

+(BOOL)tieneTareasCargadas:(NSDate *)date
{
    NSString *sql = [NSString stringWithFormat:
                    @"SELECT id "
                     "FROM nodo_tareaspartediario "
                     "WHERE partediario = %@",
                            [date formattedStringUsingFormat:@"yyyyMMdd"]];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
        
            return YES;
        }
        
	} else {
        NSAssert(0, @"No se puede ejecutar: %@", sql);
	}
	sqlite3_finalize(statement);
    
    return NO;
}

+(NSArray*)tieneTareasCargadasEntreFecha:(NSDate *)date hasta:(NSDate*)dateHasta
{
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *sql = [NSString stringWithFormat:
                     @"SELECT DISTINCT partediario "
                     "FROM nodo_tareaspartediario "
                     "WHERE (partediario >= %@) AND (partediario <= %@) AND nodo = %@",
                     [[date dateAsDateWithoutTime] formattedStringUsingFormat:@"yyyyMMdd"],
                     [[dateHasta dateAsDateWithoutTime] formattedStringUsingFormat:@"yyyyMMdd"],
                     nodo];
    
#ifdef DEBUG_PARTEDIARIO
    NSLog(@" - ParteDiario: %@", sql);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement )
    {
        char* _c;
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            // Obtengo la fecha
            NSDate *dateParte = nil;
            _c = (char*)sqlite3_column_text(statement, 0);
            dateParte = [NSDate stringToTime:[NSString pCharToString:_c] withFormat:@"yyyyMMdd"];
            
            // Agrego la fecha al listaod
            [result addObject:dateParte];
        }
        
	} else
    {
        NSAssert(0, @"No se puede ejecutar: %@", sql);
	}
	sqlite3_finalize(statement);
    
    return result;
}

+(NSString *)getTareaForNovedad:(NSInteger) idTarea parteDiario:(NSInteger)parteDiario
{
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *sql = [NSString stringWithFormat:
                     @"SELECT id || '|' || nodo || '|' || partediario || '|' || actividad || '|' || tarea || '|' || cantidad "
                     "FROM nodo_tareaspartediario "
                     "WHERE id = %d and nodo = %@ and partediario = %d",
                     idTarea,
                     nodo,
                     parteDiario
                    ];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    char *_c;
    NSString *result = @"";
	if ( statement ) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            _c = (char *)sqlite3_column_text(statement, 0);
            if((_c != nil) && (strlen(_c) > 0)) {
                result = [NSString stringWithUTF8String:_c];
            }
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
}

+(void)deleteTarea:(NSUInteger)parteDiario actividad:(NSString *)actividad
{
    // Borro el registro
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *sql = [NSString stringWithFormat:
                     @"DELETE FROM NODO_TAREASPARTEDIARIO "
                      "WHERE tarea = 9999 and nodo = %@ and partediario = %d and actividad = %@",
                                nodo,
                                parteDiario,
                                [NSString stringQuoted:actividad ifNull:@"''"]
                     ];
    
    [[DB getInstance] excecuteSQL:sql];
}

+(void)deleteTareaEx:(ParteDiarioData*)parteDiarioData
{
    // Borro el registro
    NSString *sql = [NSString stringWithFormat:
                     @"DELETE FROM NODO_TAREASPARTEDIARIO "
                     "WHERE tarea = %d and nodo = %d and partediario = %d and actividad = %@",
                     parteDiarioData.tarea,
                     parteDiarioData.nodo,
                     parteDiarioData.partediario,
                     [NSString stringQuoted:parteDiarioData.actividad ifNull:@"''"]
                     ];
    
    [[DB getInstance] excecuteSQL:sql];
}

+(void)eliminarTareas:(NSDate*)date conservarVisitas:(BOOL)conservaVisitas
{
    
    NSArray *arrayParte = [[ParteDiario getAll:date] retain];
    NSMutableArray *tmpPartes = [[NSMutableArray alloc] initWithArray:arrayParte];

    
    // Chequeo si existen partes para la fecha.
    if ([arrayParte count] > 0)
    {
        // Si existen, los borro.
        for (ParteDiario *p in arrayParte)
        {
            
            BOOL marcadoParaBorrar = false;
            
            // Chequeo si debo conservar la visita.
            // Genero la novedad para eliminar las tareas si no es una visita y hay que conservarla.
            
            // Mañana
            if ((p.maniana) && (!((p.maniana.tarea == 9999) && (conservaVisitas))))
            {
                [Novedad insert:NODO_TAREAS_PARTEDIARIO
                        novedad:[ParteDiario getTareaForNovedad:p.maniana.id_nodotareaspartediario
                                                    parteDiario:p.maniana.partediario]
                            tipo:kDelete];
                
                [ParteDiario deleteTareaEx:p.maniana];
                
                marcadoParaBorrar = true;
            }
            
            // Tarde
            if ((p.tarde)  && (!((p.tarde.tarea == 9999) && (conservaVisitas))))
            {
                [Novedad insert:NODO_TAREAS_PARTEDIARIO
                        novedad:[ParteDiario getTareaForNovedad:p.tarde.id_nodotareaspartediario
                                                    parteDiario:p.tarde.partediario]
                            tipo:kDelete];
                    
                [ParteDiario deleteTareaEx:p.tarde];
                
                marcadoParaBorrar = true;
            }
                
            // Borro la tarea del array del parte.
            if ((marcadoParaBorrar) || ((!p.maniana) && (!p.tarde)))
                [tmpPartes removeObject:p];
            
        }

        // Chequeo si quedó alguna tarea, si no hay ninguna, eleimino el parte diario.
        if ([tmpPartes count] == 0)
        {
            NSString *strNoverdad = [ParteDiario getForNovedad:date];
            
            if ([strNoverdad length] > 3)
            {
                // Elimino el parte diario
                [Novedad insert:NODO_PARTEDIARIO
                        novedad:strNoverdad
                           tipo:kDelete];
            
                [ParteDiario remove:date];
            }
        }
    }
    
    [arrayParte release];
    [tmpPartes release];
}

+(NSString *)getForNovedad:(NSDate *) fecha
{
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *FormatoFecha = @"%m/%d/%Y %H:%M";
    
    NSString *sql = [NSString stringWithFormat:
                     @"SELECT id || '|' || nodo || '|' || ifnull(strftime('%@', fecha, 'unixepoch', 'localtime'), \"\") from nodo_partediario where id = %@ and nodo = %@", FormatoFecha, [fecha formattedStringUsingFormat:@"yyyyMMdd"], nodo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    //NSLog(@"- Novedad Parte Diario: %@", sql);
    
    char *_c;
    NSString *result = @"";
	if ( statement ) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            _c = (char *)sqlite3_column_text(statement, 0);
            if((_c != nil) && (strlen(_c) > 0)) {
                result = [NSString stringWithUTF8String:_c];
            }
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
}

+(void)remove:(NSDate *)fecha
{
    // Borro el registro
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *sql = [NSString stringWithFormat:
                     @"DELETE FROM nodo_partediario "
                      "WHERE id = %@ and nodo = %@",
                        [fecha formattedStringUsingFormat:@"yyyyMMdd"],
                        nodo
                     ];
    
    [[DB getInstance] excecuteSQL:sql];
}

+(NSArray *)getAll:(NSDate *)fecha
{
    fecha = [fecha dateAsDateWithoutTime];
    
    NSMutableArray *partes = [NSMutableArray array];
    
    // Cargo todas las tareas
    
    NSString *sql = @"select descripcion, id from mtc_tareas order by descripcion";
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            ParteDiario *parte = [[ParteDiario alloc] init];
            
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if((_descripcion != nil) && (strlen(_descripcion) > 0)) {
                parte.descripcion = [NSString stringWithUTF8String:_descripcion];
			}else{
				parte.descripcion = @"";
			}
           
            parte.identificador = sqlite3_column_int(statement, 1);
            
            parte.fecha = fecha;
            [partes addObject:parte];
            [parte release];
        }
        
	} else {
        NSAssert(0, @"No se puede ejecutar: %@", sql);
	}
	sqlite3_finalize(statement);
    
    // Cargo los partes
    
    sql = @"SELECT DISTINCT "
                "tar.descripcion, "
                "npd.fecha, "
                "ntpd.cantidad, "
                "ntpd.actividad, "
                "ntpd.nodo, "
                "ntpd.id as id_nodotareaspartediario, "
                "ntpd.partediario, "
                "ntpd.tarea, ntpd.id || '|' || ntpd.nodo || '|' || ntpd.partediario || '|' || ntpd.actividad || '|' || ntpd.tarea || '|' || ntpd.cantidad as pipe, "
                "nov.registro "
            "FROM nodo_partediario npd "
                "JOIN nodo_tareaspartediario ntpd ON (npd.id = ntpd.partediario AND npd.nodo = ntpd.nodo) "
                "JOIN mtc_tareas tar ON (ntpd.tarea = tar.id) "
                "LEFT OUTER JOIN log_novedades nov ON (pipe = nov.registro AND nov.tipo = 'I' AND nov.transmitido = 0) "
            "WHERE npd.nodo = %@ AND npd.fecha = %@ "
            "ORDER BY actividad";
    
    NSString *nodo = [[Config shareInstance] find:NODO];
    NSString *fechaStr = [NSString stringWithFormat:@"%d", (int)[fecha timeIntervalSince1970]];
    
    NSString *query = [NSString stringWithFormat:sql, nodo,
                            [NSString stringQuoted:fechaStr ifNull:@"''"]];
    
    statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            // Levanto los datos
            ParteDiarioData *parte = [[ParteDiarioData alloc] init];
            
            // Descripcion
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            NSString *descripcion;
            if((_descripcion != nil) && (strlen(_descripcion) > 0)) {
                descripcion = [NSString stringWithUTF8String:_descripcion];
			}else{
				descripcion = @"";
			}
            parte.descripcion = descripcion;
            
            // Fecha
            double val = sqlite3_column_double(statement, 1);
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:val];
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            [formatter setDateFormat:@"dd/MM/yyyy"];
//            parte.fecha = [formatter stringFromDate:date];
//            [formatter release];
            parte.fecha = [date formattedStringUsingFormat:@"dd/MM/yyyy"];
            
            // Cantidad
            parte.cantidad = sqlite3_column_int(statement, 2);
                    
            // Actividad
            char *_act = (char *)sqlite3_column_text(statement, 3);
            NSString *act;
            if((_act != nil) && (strlen(_act) > 0)) {
                act = [NSString stringWithUTF8String:_act];
            }else{
                act = @"";
            }
            parte.actividad = act;
                    
            // Nodo
            parte.nodo = sqlite3_column_int(statement, 4);
                    
            // Id
            parte.id_nodotareaspartediario = sqlite3_column_int(statement, 5);
                    
            // Parte Diario
            parte.partediario = sqlite3_column_int(statement, 6);
                    
            // Tarea
            parte.tarea = sqlite3_column_int(statement, 7);
                    
            // Pipe
            char *_pipe = (char *)sqlite3_column_text(statement, 8);
            NSString *pipe;
            if((_pipe != nil) && (strlen(_pipe) > 0)) {
                pipe = [NSString stringWithUTF8String:_pipe];
            }else{
                pipe = @"";
            }
            parte.pipe = pipe;
                    
            // Registro
            char *_reg = (char *)sqlite3_column_text(statement, 9);
            NSString *reg;
            if((_reg != nil) && (strlen(_reg) > 0)) {
                reg = [NSString stringWithUTF8String:_reg];
            }else{
                reg = @"";
            }
            parte.registro = reg;
            
            // Lo ingreso a la coleccion principal
            for(ParteDiario *p in partes)
            {
                if ([p.descripcion isEqualToString:parte.descripcion])
                {
                    if ([parte isTarde])
                    {
                        p.tarde = parte;
                        [parte release];
                    }
                    else
                    {
                        p.maniana = parte;
                        [parte release];
                    }
                }
            }
                
        }
        
	} else {
        NSAssert(0, @"No se puede ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    
    return partes;
}

-(NSUInteger)cantidadManiana
{
    if (self.maniana != nil)
        return self.maniana.cantidad;
    else
        return 0;
}

-(NSUInteger)cantidadTarde
{
    if (self.tarde != nil)
        return self.tarde.cantidad;
    else
        return 0;
}

+(void)addCantidad:(NSUInteger)idTarea parteDiario:(NSUInteger)parteDiario actividad:(NSString *)actividad tarea:(NSUInteger)tarea
{
    NSString *nodo = [[Config shareInstance] find:NODO];
    
    NSString *sql = [NSString stringWithFormat:@"UPDATE  NODO_TAREASPARTEDIARIO set CANTIDAD = CANTIDAD + 1 WHERE id = %d and nodo = %@ and partediario = %d and actividad = %@ and tarea = %d", idTarea, nodo, parteDiario,
                     [NSString stringQuoted:actividad ifNull:@"''"],
                      tarea];
    
    [[DB getInstance] excecuteSQL:sql];
    
}

+(void)restarTarea:(NSUInteger)idTarea parteDiario:(NSUInteger)parteDiario actividad:(NSString *)actividad tarea:(NSUInteger)tarea
{
    NSString *nodo = [[Config shareInstance] find:NODO];
    
    NSString *sql = [NSString stringWithFormat:@"UPDATE  NODO_TAREASPARTEDIARIO set CANTIDAD = CANTIDAD - 1 WHERE id = %d and nodo = %@ and partediario = %d and actividad = %@ and tarea = %d", idTarea, nodo, parteDiario,
                     [NSString stringQuoted:actividad ifNull:@"''"],
                     tarea];
    
#ifdef DEBUG_PARTEDIARIO
    NSLog (@" - Query restarcantidad: %@", sql);
#endif
    
    [[DB getInstance] excecuteSQL:sql];

    
}

+(NSUInteger)lastId:(NSUInteger)partediario
{
    NSString *nodo = [[Config shareInstance] find:NODO];
    NSUInteger lastId = 0;
    NSString *sql = @"select case when max(id) is null then 1 else max(id) + 1 end nextid from nodo_tareaspartediario where nodo = %@ and partediario = %d";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:[NSString stringWithFormat:sql, nodo, partediario]];
    
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
            
            lastId = sqlite3_column_int(statement, 0);
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla nodo_tareaspartediario");
	}
	sqlite3_finalize(statement);
    
    return lastId;
}

// TODO : HAcer un método save que cree y borre los registros necesarios y vacios.

+(void)save_old:(ParteDiarioData *)p
{
    if (p.id_nodotareaspartediario == 0 && p.cantidad > 0)
    {
        NSUInteger lastId = [ParteDiario lastId:p.partediario];
        
        // Es un INSERT
        NSString *sql = [NSString stringWithFormat:@"insert into nodo_tareaspartediario (id, nodo, partediario, actividad, tarea, cantidad) values (%d, %d, %d, %@, %d, %d)", lastId, [[[Config shareInstance] find:NODO] intValue], p.partediario,
                         [NSString stringQuoted:p.actividad ifNull:@"''"],
                         p.tarea, p.cantidad];
        
        [[DB getInstance] excecuteSQL:sql];
        
        // Genero la novedad
        [Novedad insert:NODO_TAREAS_PARTEDIARIO novedad:[NSString stringWithFormat:@"%d|%d|%d|%@|%d|%d", lastId, [[[Config shareInstance] find:NODO] intValue], p.partediario, p.actividad, p.tarea, p.cantidad] tipo:kInsert];
    }
    else if (p.id_nodotareaspartediario != 0 && p.cantidad == 0)
    {
        // Es un DELETE
        NSString *sql = [NSString stringWithFormat:@"delete from nodo_tareaspartediario where id=%d and partediario = %d and nodo = %d and actividad = %@ and tarea = %d ", p.id_nodotareaspartediario, p.partediario, [[[Config shareInstance] find:NODO] intValue],
                         [NSString stringQuoted:p.actividad ifNull:@"''"],
                         p.tarea];
        
        [[DB getInstance] excecuteSQL:sql];
        
        // Genero la novedad
        [Novedad insert:NODO_TAREAS_PARTEDIARIO novedad:[NSString stringWithFormat:@"%d|%d|%d|%@|%d|%d", p.id_nodotareaspartediario, [[[Config shareInstance] find:NODO] intValue], p.partediario, p.actividad, p.tarea, p.cantidad] tipo:kDelete];

    }
    else if (p.id_nodotareaspartediario != 0 && p.cantidad > 0)
    {
        NSUInteger cantidadActual = 0;
        
        // Tengo que verificar si es un update realmente para eso tengo que ir a la base
        NSString *sql = [NSString stringWithFormat:@"select cantidad from nodo_tareaspartediario where id=%d and partediario = %d and nodo = %d and actividad = %@ and tarea = %d ", p.id_nodotareaspartediario, p.partediario, [[[Config shareInstance] find:NODO] intValue],
                         [NSString stringQuoted:p.actividad ifNull:@"''"],
                         p.tarea];

        sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
        
        if ( statement )
        {
            
            if (sqlite3_step(statement) == SQLITE_ROW)
            {

                cantidadActual = sqlite3_column_int(statement, 0);
            }
        }
        
        sqlite3_finalize(statement);
        
        if (p.cantidad != cantidadActual)
        {
            // UPDATE
            NSString *sql = [NSString stringWithFormat:@"update nodo_tareaspartediario set cantidad = %d where id=%d and partediario = %d and nodo = %d and actividad = %@ and tarea = %d ", p.cantidad, p.id_nodotareaspartediario, p.partediario, [[[Config shareInstance] find:NODO] intValue],
                             [NSString stringQuoted:p.actividad ifNull:@"''"],
                             p.tarea];
            
            [[DB getInstance] excecuteSQL:sql];
            
            // genero la novedad
            [Novedad insert:NODO_TAREAS_PARTEDIARIO novedad:[NSString stringWithFormat:@"%d|%d|%d|%@|%d|%d", p.id_nodotareaspartediario, [[[Config shareInstance] find:NODO] intValue], p.partediario, p.actividad, p.tarea, p.cantidad] tipo:kUpdate];
        }
    }
    else
    {
        // No deberia suceder este caso
    }
}

+ (void)save:(ParteDiarioData *)p
{
    // Esta función reemplaza a save_old (que ateriormente se llamó save) debido a que en el caso del parte diario desde-hasta
    // puede que se pase un parte sin id, pero cuya tarea ya existe en la base. Por lo que al momento de grabar debe chequear también
    // si el tipo de tarea existe con otro ID y en lugar de un INSERT debo hacer un UPDATE.
    
//    if (p.id_nodotareaspartediario != 0 && p.cantidad == 0)
//    {
//        // Es un DELETE
//        [ParteDiario deleteParte:p];
//    } else
    

    // Busco si ya existe la tarea para el parte.
    ParteDiarioData *oldTareaData = [ParteDiario getParteDiario:p.tarea
                                                    todasTareas:0
                                                      actividad:p.actividad
                                                    todasFechas:0
                                                          fecha:[NSDate stringToTime:p.fecha withFormat:@"dd/MM/yyyy"]
                                                          rango:1
                                                          desde:nil
                                                          hasta:nil];

    
    if (p.cantidad > 0)
    {
        // Chequeo si devolvió o no una tarea;
        if (!oldTareaData.id_nodotareaspartediario)
        {
            // INSERT
            [ParteDiario insertParte:p];
        } else
        if (p.cantidad != oldTareaData.cantidad)
        {
            // UPDATE
            p.id_nodotareaspartediario = oldTareaData.id_nodotareaspartediario;
            [ParteDiario updateParte:p];
        }
    } else
    {
        // Es un DELETE
        if (oldTareaData.id_nodotareaspartediario)
            [ParteDiario deleteParte:p];
    }
}


-(NSUInteger)cantidadTotal
{
    return [self cantidadManiana] + [self cantidadTarde];
}

+(BOOL)existsCabecera:(NSDate *)date
{
    date = [date dateAsDateWithoutTime];
    
    NSString *sql = [NSString stringWithFormat:@"select * from nodo_partediario where id=%@", [date formattedStringUsingFormat:@"yyyyMMdd"]];

    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    BOOL exists = NO;
    if ( statement )
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
            exists = YES;
        }
    }
    
    sqlite3_finalize(statement);
    
    return exists;
}

+(ParteDiarioData *)getParteDiario:(NSUInteger) tarea
                         todasTareas:(NSInteger)todasTareas
                           actividad:(NSString *)actividad
                         todasFechas:(NSInteger)todasFechas
                               fecha:(NSDate *)fecha
                               rango:(NSInteger)rango
                               desde:(NSDate *)desde
                               hasta:(NSDate *)hasta
{
    return [ParteDiario getParteDiarioEx:tarea
                             todasTareas:todasTareas
                               actividad:[NSString stringQuoted:actividad ifNull:@"''"]
                             todasFechas:todasFechas
                                   fecha:fecha
                                   rango:rango
                                   desde:desde
                                   hasta:hasta];
}

+(ParteDiarioData *)getParteDiarioEx:(NSUInteger) tarea
                       todasTareas:(NSInteger)todasTareas
                         actividad:(NSString *)actividad
                       todasFechas:(NSInteger)todasFechas
                             fecha:(NSDate *)fecha
                             rango:(NSInteger)rango
                             desde:(NSDate *)desde
                             hasta:(NSDate *)hasta
{
    NSString *nodo = [[Config shareInstance] find:NODO];
    
    NSString *sql = [NSString stringWithFormat:
                     @"select parte.id as id_parte, "
                             "tarea.id as id_tarea, "
                             "tarea.cantidad as cantidad, "
                             "tarea.actividad as actividad, "
                             "parte.fecha as fecha, "
                             "tarea.tarea as tarea "
                     "from nodo_partediario parte left outer join "
                          "nodo_tareaspartediario tarea on parte.nodo = tarea.nodo and "
                                                          "parte.id = tarea.partediario and "
                                                          "(1 = %d or tarea.tarea = %d) and "
                                                          "tarea.actividad in (%@) "
                     "where   parte.nodo = %@ and "
                            "(1 = %d or parte.fecha = %.0f) and "
                            "(1 = %d or (parte.fecha >= %.0f and parte.fecha <= %.0f)) "
                     "order by parte.id, tarea.actividad",
                     todasTareas,
                     tarea,
                     actividad,
                     nodo,
                     todasFechas,
                     [fecha timeIntervalSince1970],
                     rango,
                     desde != nil ? [desde timeIntervalSince1970] : -1,
                     hasta != nil ? [hasta timeIntervalSince1970] : -1];
        
    //NSLog(@"%@",sql);
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    ParteDiarioData *parte = nil;
    if ( statement )
    {
        
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
            
            parte = [[ParteDiarioData alloc] init];
            
            // Parte Diario
            parte.partediario = sqlite3_column_int(statement, 0);
            // Tarea
            parte.id_nodotareaspartediario = sqlite3_column_int(statement, 1);
            // Cantidad
            parte.cantidad = sqlite3_column_int(statement, 2);
            
            // Actividad
            char *_act = (char *)sqlite3_column_text(statement, 3);
            NSString *act;
            if((_act != nil) && (strlen(_act) > 0)) {
                act = [NSString stringWithUTF8String:_act];
            }else{
                act = @"";
            }
            parte.actividad = act;
            
            // Fecha
            double val = sqlite3_column_double(statement, 4);
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:val];
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            [formatter setDateFormat:@"dd/MM/yyyy"];
//            parte.fecha = [formatter stringFromDate:date];
//            [formatter release];
            parte.fecha = [date formattedStringUsingFormat:@"dd/MM/yyyy"];
            
            
            parte.tarea = sqlite3_column_int(statement, 5);

        }
    }
    
    sqlite3_finalize(statement);
    
    return [parte autorelease];
}

+(NSArray *)getAllParteDiario:(NSDate *)fecha
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = [NSString stringWithFormat:
                     @"SELECT "
                            "parte.id as id_parte, "
                            "tarea.id as id_tarea, "
                            "tarea.cantidad as cantidad, "
                            "tarea.actividad as actividad, "
                            "parte.fecha as fecha, "
                            "tarea.tarea as tarea "
                     "FROM nodo_partediario parte left outer join "
                          "nodo_tareaspartediario tarea on ("
                                "parte.nodo = tarea.nodo and "
                                "parte.id = tarea.partediario and "
                                "(1 = %d or tarea.tarea = %d) and "
                                "tarea.actividad in ( '%@' )) "
                     "WHERE parte.nodo = %@ and "
                            "(1 = %d or parte.fecha = %.0f) and "
                            "(1 = %d or (parte.fecha >= %d and parte.fecha <= %d)) "
                     "ORDER BY "
                            "parte.id, "
                            "tarea.actividad",
                     1, 0, @"M','T", SettingForKey(NODO, @""), 0, [fecha timeIntervalSince1970], 1, -1, -1];
    
    //NSLog(@"- getAllParteDiario query: %@",sql);
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    ParteDiarioData *parte = nil;
    if ( statement ) {
        
        //if (sqlite3_step(statement) == SQLITE_ROW) {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            parte = [[ParteDiarioData alloc] init];
            
            // Parte Diario
            parte.partediario = sqlite3_column_int(statement, 0);
            // Tarea
            parte.id_nodotareaspartediario = sqlite3_column_int(statement, 1);
            // Cantidad
            parte.cantidad = sqlite3_column_int(statement, 2);
            
            // Actividad
            char *_act = (char *)sqlite3_column_text(statement, 3);
            NSString *act;
            if((_act != nil) && (strlen(_act) > 0)) {
                act = [NSString stringWithUTF8String:_act];
            }else{
                act = @"";
            }
            parte.actividad = act;
            
            // Fecha
            double val = sqlite3_column_double(statement, 4);
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:val];
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            [formatter setDateFormat:@"dd/MM/yyyy"];
//            parte.fecha = [formatter stringFromDate:date];
//            [formatter release];
            parte.fecha = [date formattedStringUsingFormat:@"dd/MM/yyyy"];
            
            parte.tarea = sqlite3_column_int(statement, 5);
            
            [array addObject:parte];
            [parte release];
        }
    }
    
    sqlite3_finalize(statement);
    
    return [array autorelease];
    
}


+(void)insertCabecera:(NSDate *)date
{
    date = [date dateAsDateWithoutTime];
    
    NSString *sql = [NSString stringWithFormat:@"insert into nodo_partediario (id, nodo, fecha) values (%@, %d, %d)", [date formattedStringUsingFormat:@"yyyyMMdd"], [[[Config shareInstance] find:NODO] intValue], (int)[date timeIntervalSince1970]];
    
    
    [[DB getInstance] excecuteSQL:sql];

}

+(void)insertTarea:(NSInteger) idTarea actividad:(NSString *)actividad cantidad:(NSInteger)cantidad parteDiario:(NSInteger)parteDiario tarea:(NSInteger)tarea
{
    
    NSString *nodo = [[Config shareInstance] find:NODO];
    
    NSString *sql = [NSString stringWithFormat:
                     @"insert into NODO_TAREASPARTEDIARIO "
                     "(id, nodo, partediario, actividad, tarea, cantidad) "
                     "values (%d, %@, %d, %@, %d, %d)",
                        idTarea,
                        nodo,
                        parteDiario,
                        [NSString stringQuoted:actividad ifNull:@"''"],
                        tarea,
                        cantidad];
    
    [[DB getInstance] excecuteSQL:sql];
}

-(void)setCantidadForManiana:(NSUInteger)maniana
{
    if (self.maniana != nil)
    {
        self.maniana.cantidad = maniana;
    }
    else
    {
        self.maniana = [[[ParteDiarioData alloc] init] autorelease];
        
        self.maniana.partediario = [[self.fecha formattedStringUsingFormat:@"yyyyMMdd"] integerValue];
        self.maniana.cantidad = maniana;
        self.maniana.tarea = self.identificador;
        self.maniana.actividad = @"M";
    }
}

-(void)setCantidadForTarde:(NSUInteger)tarde
{
    if (self.tarde != nil)
    {
        self.tarde.cantidad = tarde;
    }
    else
    {
        self.tarde = [[[ParteDiarioData alloc] init] autorelease];
        
        self.tarde.cantidad = tarde;
        self.tarde.partediario = [[self.fecha formattedStringUsingFormat:@"yyyyMMdd"] integerValue];
        self.tarde.tarea = self.identificador;
        self.tarde.actividad = @"T";
    }
}

#pragma mark -
#pragma mark Memory Management

-(void)dealloc
{
    [_maniana release];
    [_tarde release];
    [_fecha release];
    [_descripcion release];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Privated methods

+(void)insertParte:(ParteDiarioData *)p
{
    // Obtengo el ID para el neuvo registro.
    NSUInteger lastId = [ParteDiario lastId:p.partediario];
    
    // INSERT
    NSString *sql = [NSString stringWithFormat:
                     @"INSERT INTO nodo_tareaspartediario (id, nodo, partediario, actividad, tarea, cantidad) "
                      "VALUES (%d, %d, %d, %@, %d, %d)",
                     lastId, [[[Config shareInstance] find:NODO] intValue], p.partediario,
                     [NSString stringQuoted:p.actividad ifNull:@"''"],
                     p.tarea, p.cantidad];
    
    [[DB getInstance] excecuteSQL:sql];
    
    // Genero la novedad
    [Novedad insert:NODO_TAREAS_PARTEDIARIO novedad:[NSString stringWithFormat:@"%d|%d|%d|%@|%d|%d",
                                                     lastId,
                                                     [[[Config shareInstance] find:NODO] intValue],
                                                     p.partediario,
                                                     p.actividad,
                                                     p.tarea,
                                                     p.cantidad] tipo:kInsert];
}

+(void)updateParte:(ParteDiarioData *)p
{
    // UPDATE
    NSString *sql = [NSString stringWithFormat:
                     @"UPDATE nodo_tareaspartediario "
                      "SET cantidad = %d "
                      "WHERE id=%d and partediario = %d and nodo = %d and actividad = %@ and tarea = %d ",
                     p.cantidad,
                     p.id_nodotareaspartediario,
                     p.partediario,
                     [[[Config shareInstance] find:NODO] intValue],
                     [NSString stringQuoted:p.actividad ifNull:@"''"],
                     p.tarea];
    
    [[DB getInstance] excecuteSQL:sql];
    
    // genero la novedad
    [Novedad insert:NODO_TAREAS_PARTEDIARIO novedad:[NSString stringWithFormat:@"%d|%d|%d|%@|%d|%d",
                                                     p.id_nodotareaspartediario,
                                                     [[[Config shareInstance] find:NODO] intValue],
                                                     p.partediario,
                                                     p.actividad,
                                                     p.tarea,
                                                     p.cantidad] tipo:kUpdate];
}

+(void)deleteParte:(ParteDiarioData *)p
{
    NSString *sql = [NSString stringWithFormat:
                     @"DELETE FROM nodo_tareaspartediario "
                      "WHERE id=%d and partediario = %d and nodo = %d and actividad = %@ and tarea = %d ",
                     p.id_nodotareaspartediario,
                     p.partediario,
                     [[[Config shareInstance] find:NODO] intValue],
                     [NSString stringQuoted:p.actividad ifNull:@"''"],
                     p.tarea];
    
    [[DB getInstance] excecuteSQL:sql];
    
    // Genero la novedad
    [Novedad insert:NODO_TAREAS_PARTEDIARIO novedad:[NSString stringWithFormat:@"%d|%d|%d|%@|%d|%d",
                                                     p.id_nodotareaspartediario,
                                                     [[[Config shareInstance] find:NODO] intValue],
                                                     p.partediario,
                                                     p.actividad,
                                                     p.tarea,
                                                     p.cantidad] tipo:kDelete];
}



+(NSInteger)getCountWithoutDoctorsForDate:(NSDate *)fecha
{
    fecha = [fecha dateAsDateWithoutTime];
    
    NSInteger cantidad = 0;
    
    NSString *sql =
        @"SELECT SUM(ntpd.cantidad)"
        "FROM nodo_partediario npd "
        "JOIN nodo_tareaspartediario ntpd ON (npd.id = ntpd.partediario AND npd.nodo = ntpd.nodo) "
        "WHERE npd.nodo = %@ AND npd.fecha = %@ AND tarea != 9999 "
        "ORDER BY actividad";
    
    NSString *nodo = [[Config shareInstance] find:NODO];
    NSString *fechaStr = [NSString stringWithFormat:@"%d", (int)[fecha timeIntervalSince1970]];
    
    NSString *query = [NSString stringWithFormat:sql,
                            nodo,
                            [NSString string:fechaStr]
                       ];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if (statement)
    {
		if (sqlite3_step(statement) == SQLITE_ROW)
        {
            cantidad = sqlite3_column_int(statement, 0);
        }
        
	} else
    {
        NSAssert(0, @"No se puede ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return cantidad;
}


+ (void)cleanTareasParte
{
    // Este método borra todas la tareas que tengan como cantidad 0.
    NSString* sql =
    @"SELECT * FROM ( "
        "SELECT npd.id, npd.partediario, sum(cantidad) as cantidad, npd.nodo, np.fecha "
        "FROM nodo_tareaspartediario npd "
            "LEFT JOIN nodo_partediario np ON (np.nodo = npd.nodo AND np.id = npd.partediario) "
        "WHERE npd.nodo = %@ "
        "GROUP BY npd.partediario) "
    "WHERE cantidad = 0 ";
    
    NSString* query = [NSString stringWithFormat:sql,
                       SettingForKey(NODO, @"0")];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if (statement)
    {
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            
            NSDate* date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 4)];
            NSArray* partes = [ParteDiario getAll:date];
            
            // Si hay tarea de parte diario, la borro.
            if ([partes count] > 0)
            {
                for (ParteDiario* p in partes)
                {
                    if (p.maniana)
                    {
                        NSString* strNovedad = [ParteDiario getTareaForNovedad:p.maniana.id_nodotareaspartediario
                                                                   parteDiario:p.maniana.partediario];
                        [Novedad insert:NODO_TAREAS_PARTEDIARIO
                                novedad:strNovedad
                                   tipo:kDelete];
                        
                        [ParteDiario deleteTareaEx:p.maniana];
                    }
                    
                    if (p.tarde)
                    {
                        NSString* strNovedad = [ParteDiario getTareaForNovedad:p.tarde.id_nodotareaspartediario
                                                                   parteDiario:p.tarde.partediario];
                        [Novedad insert:NODO_TAREAS_PARTEDIARIO
                                novedad:strNovedad
                                   tipo:kDelete];
                        
                        [ParteDiario deleteTareaEx:p.tarde];
                    }
                }
            }
        }
        
	} else
    {
        NSAssert(0, @"No se puede ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
}

+ (void)cleanParte
{
    // Este método borra los partes diarios sin actividades.
    
    NSString* sql = @"SELECT DISTINCT "
                        "npd.id, ntpd.partediario, ntpd.id, cantidad, npd.fecha "
                    "FROM nodo_partediario npd "
                        "LEFT JOIN nodo_tareaspartediario ntpd ON (npd.id = ntpd.partediario AND npd.nodo = ntpd.nodo) "
                    "WHERE npd.nodo = %@ and (cantidad is null) "
                    "ORDER BY npd.fecha ";

    NSString* query = [NSString stringWithFormat:sql,
                                    SettingForKey(NODO, @"0")];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if (statement)
    {
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            NSDate* date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 4)];
            NSString *strNoverdad = [ParteDiario getForNovedad:date];
            
            if ([strNoverdad length] > 3)
            {
                // Elimino el parte diario
                [Novedad insert:NODO_PARTEDIARIO
                        novedad:strNoverdad
                           tipo:kDelete];
                
                [ParteDiario remove:date];
            }

        }
        
	} else
    {
        NSAssert(0, @"No se puede ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
}


@end
