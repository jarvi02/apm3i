//
//  StockMM.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "StockMM.h"
#import "DB.h"
#import "Config.h"

#import "NSDateExtended.h"
#import "NSStringExtended.h"

@implementation StockMM


- (id)init
{
    self = [super init];
    if (self) {

        self.descripcion = @"";
        self.codigo = @"";
        self.lote =@"";
        self.vencido = NO;

    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [_codigo release];
    [_lote release];
    
    [super dealloc];
}

- (void)checkProducto
{
    //self.blocked = NO;
    self.vencido = NO;
    
    if ([[[NSDate dateWithTimeIntervalSince1970:self.fecha_vto] dateAsDateWithoutTime] timeIntervalSince1970] <= [[NSDate dateWithoutTime] timeIntervalSince1970])
    {
        self.vencido = YES;
    }
}

+(NSArray*)GetAll {
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    NSString *sql = @"SELECT descripcion, "
                            "stock, "
                            "codigo, "
                            "lote, "
                            "fecha_vto "
                            //"strftime('%@', fecha_vto, 'unixepoch', 'localtime') fecha_vto_display "
                     "FROM mtc_lotes "
                     "WHERE mtc_lotes.stock > 0 and mtc_lotes.nodo = %@ "
                     "ORDER BY descripcion, mtc_lotes.codigo, mtc_lotes.fecha_vto, mtc_lotes.lote;";
    
    NSString *nodo = SettingForKey(NODO, @"0");
    //NSString *query = [NSString stringWithFormat:sql, @"%s", nodo];
    NSString *query = [NSString stringWithFormat:sql, nodo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        StockMM *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[StockMM alloc] init];
            
            _c = (char *)sqlite3_column_text(statement, 0);
            o.descripcion = [NSString pCharToString:_c];
            
            o.stock = sqlite3_column_int(statement, 1);
            
            
            _c = (char *)sqlite3_column_text(statement, 2);
            o.codigo = [NSString pCharToString:_c];
            
            _c = (char *)sqlite3_column_text(statement, 3);
            o.lote = [NSString pCharToString:_c];
            
            o.fecha_vto = sqlite3_column_int(statement, 4);
            
            [o checkProducto];
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];

}

@end
