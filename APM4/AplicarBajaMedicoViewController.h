//
//  AplicarBajaMedicoViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomViewController.h"
#import "CarteraViewController.h"
#import "DateSelectorViewController.h"
#import "GenericSelectorViewController.h"

@class MotivoBaja;

@interface AplicarBajaMedicoViewController : CustomViewController <DateSelectorDelegate, MTpopListViewDelegate>

@property(nonatomic, weak) CarteraViewController *carteraViewControllerReference;
@property(nonatomic, strong) NSString *titulo;

@property (strong, nonatomic) MTpopListView         *popupMenu;

@property(nonatomic, strong) IBOutlet UITextField *fecha;
@property(nonatomic, strong) IBOutlet UITextField *motivo;

@property(nonatomic, strong) IBOutlet UIButton *selectDateButton;
@property(nonatomic, strong) IBOutlet UIButton *selectMotivoButton;

@property(nonatomic, strong) NSDate *currentDate;
@property(nonatomic, strong) MotivoBaja *currentMotivo;

-(IBAction)pressedGuardar:(id)sender;
-(IBAction)pressednCancelar:(id)sender;
-(IBAction)selectMotivo:(id)sender;
-(IBAction)selectFecha:(id)sender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil cartera:(CarteraViewController *)cartera;

@end
