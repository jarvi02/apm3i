//
//  Novedad.h
//  APM4
//
//  Created by Juan Pablo Garcia on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NovedadTipo) {
    kInsert,
    kUpdate,
    kDelete
} TipoNovedad;

#define NODO_PARTEDIARIO                @"nodo_partediario"
#define NODO_TAREAS_PARTEDIARIO         @"nodo_tareaspartediario"
#define MED_MEDICOS                     @"med_medicos"

@interface Novedad : NSObject

+(NSUInteger)lastId;
+(void)insert:(NSString *)tabla novedad:(NSString *)novedad tipo:(NovedadTipo)tipo;

@end
