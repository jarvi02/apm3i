//
//  MuestrasEntregadasCell.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MuestrasEntregadasCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lbFecha;
@property (retain, nonatomic) IBOutlet UILabel *lbNombreMedico;
@property (retain, nonatomic) IBOutlet UILabel *lbProducto;
@property (retain, nonatomic) IBOutlet UILabel *lbCodigo;
@property (retain, nonatomic) IBOutlet UILabel *lbLote;
@property (retain, nonatomic) IBOutlet UILabel *lbMuestras;
@property (retain, nonatomic) IBOutlet UILabel *lbFechaVto;

@end
