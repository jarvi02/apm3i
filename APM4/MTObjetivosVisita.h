//
//  MTObjetivosVisita.h
//  APM4
//
//  Created by Laura Busnahe on 8/8/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTObjetivosVisita : NSObject

@property(nonatomic, strong) NSDate     *fecha;
@property(nonatomic, strong) NSString   *objetivos;
@property(nonatomic, strong) NSString   *evaluacion;

+ (NSArray*)getAllForMedico:(NSInteger)idMedico;

@end
