//
//  ServiceEnviarNovedad.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ServiceEnviarNovedad.h"
#import "Config.h"
#import "ConfigGlobal.h"

#import "GlobalDefines.h"

@implementation ServiceEnviarNovedad


-(BOOL*) enviar:(NSString*)urlBase fileName:(NSString*)aFile tamanio:(NSInteger)tamanio
{
    BOOL result = YES;
    
    @autoreleasepool
    {
        
        NSString *urlFormated = [NSString stringWithFormat:@"%@%@?USER=%@&CMD=RTX&NAME=%@&SIZE=%d&DATA=BINARIO",
                                 urlBase,
                                 SettingForKey(REMOTE_PATH, @""),
                                 SettingForKey(ENTIDAD, @""),
                                 SettingForKey(TRANID, @""),
                                 tamanio];
        
        NSURL *url = [NSURL URLWithString:urlFormated];
       
  
        self.request = [ASIHTTPRequest requestWithURL:url];
        
#ifdef DEBUG_TRANSMICION
        NSLog(@" - enviar:filename: %@", urlFormated);
#endif

        [self.request appendPostDataFromFile:aFile];
        [self.request startSynchronous];
        
        NSError *error = [self.request error];


        NSString *response = @"0";
        if (!error)
        {
            response = [self.request responseString];
            [self logResponse:response];
            
        } else
        {
#ifdef DEBUG_TRANSMICION
            NSLog(@"  errorDescription: %@", error.localizedDescription);
            NSLog(@"  errorReason: %@", error.localizedFailureReason);
            NSLog(@"  errorSuggetion: %@", error.localizedRecoverySuggestion);
            NSLog(@"  errorButtons: %d", [error.localizedRecoveryOptions count]);
#endif
            result = NO;
        }
    }
    return result;
}

- (NSString*)textoParaLog {
    return @"ServiceEnviarNovedad";
}

@end
