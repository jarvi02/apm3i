//
//  FichaVisitaCell.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 09/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FichaVisitaCell : UITableViewCell


@property (retain, nonatomic) IBOutlet UILabel *lbFecha;
@property (retain, nonatomic) IBOutlet UILabel *lbVisita;


@end
