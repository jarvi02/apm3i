//
//  MTstdCobertuaCommon.h
//  APM4
//
//  Created by Laura Busnahe on 7/18/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTstdCobertuaCommon : NSObject

@property (nonatomic, strong)NSString   *descripcion;
@property (nonatomic, assign)NSInteger  programados;
@property (nonatomic, assign)NSInteger  fluctuantes;
@property (nonatomic, assign)NSInteger  vistos;
@property (nonatomic, assign)NSInteger  revisitas;
@property (nonatomic, assign)double     porcentaje;
@property (nonatomic, assign)NSInteger  total;

+ (NSArray*)getAll:(NSString*)query;

@end
