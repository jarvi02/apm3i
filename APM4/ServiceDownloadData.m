//
//  ServiceDownloadData.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 01/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ServiceDownloadData.h"
#import "Config.h"
#import "ASIDownloadCache.h"


@interface ServiceDownloadData()

@end

@implementation ServiceDownloadData

- (NSString*) donwload:(NSString*)urlBase
{
    NSString *file = nil;
    @autoreleasepool {
        
        NSString *cachesFolder = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
        file = [[cachesFolder stringByAppendingPathComponent:@"apm3i.d"] retain];
        
        if (DEBUG_TRANSMICIONX) NSLog(@" - Archivo de datos: %@", file);
        
        NSString *urlFormated = [NSString stringWithFormat:@"%@%@?USER=%@&CMD=SWR_DATA&NAME=%@",
                                 urlBase,
                                 SettingForKey(REMOTE_PATH, @""),
                                 SettingForKey(ENTIDAD, @""),
                                 SettingForKey(TRANID, @"")];
        
        NSURL *url = [NSURL URLWithString:urlFormated];
        
        self.request = [ASIHTTPRequest requestWithURL:url];
        
        [self.request setDefaultResponseEncoding:NSISOLatin1StringEncoding];
        [self.request setResponseEncoding:NSISOLatin1StringEncoding];
        [self.request addRequestHeader:@"Accept" value:@"text/plain"];
        [self.request addRequestHeader:@"Content-Type" value:@"text/plain;charset=iso-8859-1;"];
        
        [self.request setDownloadProgressDelegate:self.progressView];
        [self.progressView setProgress:0.0];

        
        [self.request setDownloadCache:[ASIDownloadCache sharedCache]];
        [self.request setDownloadDestinationPath:file];
        [self.request setDelegate:self];
        [self.request startSynchronous];
        
        NSError *error = [self.request error];
        if (!error) {
            
            NSString *response = [self.request responseString];
            [self logResponse:response];
        }
    }

    return [file autorelease];
}

- (NSString*)textoParaLog {
    return @"ServiceDownloadData";
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    self.dataLenght = [request contentLength];
    
#ifdef DEBUG_TRANSMICION
    NSLog(@"Tamaño del archivo: %llu Bytes", self.dataLenght);
#endif
}

@end
