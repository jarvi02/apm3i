//
//  EspecialidadViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "EspecialidadViewController.h"
#import "Especialidad.h"
#import "EntryForCombo.h"

@interface EspecialidadViewController ()
@property(nonatomic, retain) NSArray *especialidades;
@property(nonatomic, retain) NSMutableArray *especialidadesOriginal;
-(void)crearToolBar;
@end

@implementation EspecialidadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
    [_txtEspecialidad release];
    [_tableView release];
    [_toolBar release];
    [_especialidades release];
    [_especialidadesOriginal release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.especialidades = [Especialidad GetAll];
    
    self.especialidadesOriginal = [NSMutableArray array];
    for (Especialidad *o in self.abmMedicoReference.medico.especialidades) {
        [self.especialidadesOriginal addObject:[o copy]];
    }
    
    [self crearToolBar];    

}

-(void)crearToolBar {
    // flex item used to separate the left groups items and right grouped items
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    // create a special tab bar item with a custom image and title
    UIBarButtonItem *guardarItem = [[UIBarButtonItem alloc] initWithTitle:@"Guardar"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(guardar:)];
    //guardarItem.tintColor = [UIColor grayColor];
    guardarItem.width = 70;
    
    UIBarButtonItem *editarItem = [[UIBarButtonItem alloc] initWithTitle:@"Editar"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(editar:)];
    //editarItem.tintColor = [UIColor grayColor];
    editarItem.width = 70;
    
    UIBarButtonItem *cancelarItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar"
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self
                                                                    action:@selector(cancelar:)];
    //cancelarItem.tintColor = [UIColor grayColor];
    cancelarItem.width = 70;
    
    NSArray *items = [NSArray arrayWithObjects: flexItem, guardarItem, editarItem, cancelarItem, flexItem, nil];
    [self.toolBar setItems:items animated:NO];
    
    [flexItem release];
    [guardarItem release];
    [cancelarItem release];
    [editarItem release];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)especialidad:(id)sender {
    
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    NSMutableArray *entries = [[NSMutableArray alloc] initWithCapacity:0];
    EntryForCombo *e;
    
    for (int i=0; [self.especialidades count] > i; i++) {
        
        Especialidad *o = [self.especialidades objectAtIndex:i];
        
        e = [[EntryForCombo alloc] initWithValue:o.descripcion forIndex:i];
        
        [entries addObject:e];
        [e release];
    }

    
    CGSize size = CGSizeMake(350, 300);
    TableForComboPopoverViewController *t = [[TableForComboPopoverViewController alloc] initWithData:entries titulo:@"Especialidad" size:size delegate:self andCaller:@"ESPECIALIDAD"];
    t.aligment = NSTextAlignmentLeft;
    
    [entries release];
    
    Delegate.popoverController = [[[UIPopoverController alloc] initWithContentViewController:t] autorelease];
    [t release];
    
    [Delegate.popoverController presentPopoverFromRect: ((UIButton*)sender).frame inView:self.view permittedArrowDirections:YES animated:YES];

}

-(void)guardar:(id)sender{
    
    if ([self.abmMedicoReference.medico.especialidades count] >0) {
        Especialidad *o = [self.abmMedicoReference.medico.especialidades objectAtIndex:0];
        self.abmMedicoReference.txtEspecialidad.text = [NSString stringWithFormat:@"%@ - %@", o.sigla, o.descripcion];
    } else {
        self.abmMedicoReference.txtEspecialidad.text = @"";
    }
    
    [self.abmMedicoReference closeModalView];
}
-(void)cancelar:(id)sender{
    
    [self.abmMedicoReference.medico.especialidades removeAllObjects];
    
    [self.abmMedicoReference.medico.especialidades addObjectsFromArray:self.especialidadesOriginal];
    
    [self.abmMedicoReference closeModalView];
    
}

-(void)editar:(id)sender{
 
    
    if (self.tableView.editing == NO) {
        [self.tableView setEditing:YES animated:YES];
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:2]).tintColor = [UIColor blueColor];
        
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:1]).enabled =NO;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:3]).enabled =NO;
        
        
    } else {
        [self.tableView setEditing:NO animated:YES];
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:2]).tintColor = [UIColor grayColor];
        
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:1]).enabled =YES;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:3]).enabled =YES;

    }
    
    
}

#pragma - mark
#pragma - Combo delegates methods

-(void)selectedIndex:(NSInteger)index caller:(NSString*)caller {
    
    [Delegate dismissPopOver:YES];
    
    if ([caller isEqualToString:@"ESPECIALIDAD"]) {
        
        // agrego la especialidad si no esta
        Especialidad *e = [self.especialidades objectAtIndex:index];
        BOOL flag = YES;
        for (Especialidad *o in self.abmMedicoReference.medico.especialidades) {
            
            if ([e.sigla isEqualToString:o.sigla]) {
                flag = NO;
                break;
            }
        }
        
        if (flag) {
            
            [self.abmMedicoReference.medico.especialidades addObject:[e copy]];
            self.txtEspecialidad.text = e.descripcion;
            [self.tableView reloadData];
        }
       
        
    }  else {
        // jamas deberia entrar aca
    }
    
}


#pragma - mark
#pragma UItableView Delegate and DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.abmMedicoReference.medico.especialidades count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    Especialidad *e = [self.abmMedicoReference.medico.especialidades objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", e.sigla, e.descripcion];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.abmMedicoReference.medico.especialidades removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 
     Especialidad *o = [self.abmMedicoReference.medico.especialidades objectAtIndex:fromIndexPath.row];
     
     o = [o copy];
     
     [self.abmMedicoReference.medico.especialidades removeObjectAtIndex:fromIndexPath.row];
     
     [self.abmMedicoReference.medico.especialidades insertObject:o atIndex:toIndexPath.row];
    
 }

 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
     return YES;
 }


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    // nada
    
}


@end
