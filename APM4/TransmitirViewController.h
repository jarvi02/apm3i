//
//  TransmitirViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "Reachability.h"

@interface TransmitirViewController : CustomViewController {
    Reachability* internetReach;
    
    /*
     0 - no hay ninguno
     1 - primer host
     2 - segundo host
     */
    NSInteger  serverAvailable;
}

@property (weak, nonatomic) IBOutlet UIImageView *checkInternetAvailable;
@property (weak, nonatomic) IBOutlet UIImageView *checkServerAvailable;
@property (weak, nonatomic) IBOutlet UIImageView *checkServer2Available;

@property (weak, nonatomic) IBOutlet UILabel *lbInternetAvailable;
@property (weak, nonatomic) IBOutlet UILabel *lbServerAvailable;
@property (weak, nonatomic) IBOutlet UILabel *lbServer2Available;

@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UILabel *lbestado;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

- (IBAction)Transmitir:(id)sender;


@end
 