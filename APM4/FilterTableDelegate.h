//
//  FilterTableDelegate.h
//  APM4
//
//  Created by Juan Pablo Garcia on 25/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kProgramados,
    kFluctuante,
    kManiana,
    kTarde,
    kVisitados,
    kNovisitados,
    kAltas,
    kBajas,
    kParcialmenteVisitados
} kFilters;;

@protocol FilterProtocol <NSObject>

-(void)filterSelected:(kFilters)filter selected:(BOOL)selected;

@end

@interface FilterTableDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, assign) id<FilterProtocol> delegate;
@property(nonatomic, retain) NSMutableArray *currentFilters;

@end
