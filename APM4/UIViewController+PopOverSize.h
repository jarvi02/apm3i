//
//  UIViewController+PopOverSize.h
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PopOverSize)

- (void) forcePopoverSize;

@end
