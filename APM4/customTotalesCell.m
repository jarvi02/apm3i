//
//  customTotalesCell.m
//  APM4
//
//  Created by Laura Busnahe on 3/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "customTotalesCell.h"

@implementation customTotalesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)dealloc {
//    [_lblDescripcion release];
//    [_lblUniAcumuladas release];
//    [_lblPorcRelativo release];
//    [super dealloc];
//}
@end
