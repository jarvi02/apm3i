//
//  MTpopTextView.h
//  APM3i
//
//  Created by Ezequiel Manacorda on 2/6/13.
//  Copyright (c) 2013 Ezequiel Manacorda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "popListViewController.h"

@class MTpopListView;

@protocol MTpopListViewDelegate <NSObject>
@optional
// Sent when the user selects a row in the menu list.
- (void)popListView:(MTpopListView *)controller senderControl:(id)sender didSelectRow:(NSInteger *)MenuIndex selectedText:(NSString *)RowText;
- (void)popListView:(MTpopListView *)controller senderControl:(id)sender selObject:(id)selObject;
- (void)popListView:(MTpopListView *)controller senderControl:(id)sender willSelectObject:(id)selObject;
@end

@interface MTpopListView : NSObject <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
{
    //NSArray               *rowData;     // Variable interna que contiene los datos de todas
                                        // las columnas sin filtrar.
}

@property (nonatomic, assign) id                         sender;    // Qué objecto llamó al popOver.
@property (nonatomic, assign) id <MTpopListViewDelegate> delegate;  // Delegate del popOver


@property (nonatomic, retain) UIPopoverController     *popOver;               // Objeto Popover
@property (nonatomic, retain) popListViewController   *ViewController;        // View Controller que manejará la vista del Popover

@property (nonatomic, strong)NSArray                  *rowData;     // Variable interna que contiene los datos de todas
                                                                 // las columnas sin filtrar.
@property (nonatomic, strong) NSMutableArray          *rowDataFiltered;
@property (nonatomic) BOOL                            withSearch;

@property (nonatomic, retain) NSString                *popTitle;         // Título del popOver.


// Método de inicialización del popover.
- (id) initWithRect:(CGRect) rect;                        // Inicializa el popover con el tamaño pasado como parametro.
- (id) initWithArray:(NSArray*)list Rect:(CGRect)rect;    // Inicializa el popover con una array


// Métodos para parametrización del popover.
- (void) setTitle:(NSString*)title;
- (void) setTitleAndParameters:(NSString*) title rowData:(NSArray*) data;
- (void) setSize:(CGSize)size;
- (void) useSearch:(BOOL) search;        // Define si utiliza o no el campo de búsqueda (NO MODIFICAR LA PROPIEDAD withSearch)


// Métodos que muestra el popover desde un CGRect especifico.
- (void)presentPopoverFromRect:(CGRect)rect
                        inView:(UIView *)view
      permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
                      animated:(BOOL)animated;

- (void)presentPopoverFromSender:(id)sender
                          inView:(UIView *)view
        permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
                        animated:(BOOL)animated;

- (void)presentPopoverFromRectWithSender:(CGRect)rect
                                  Sender:(id)sender
                                  inView:(UIView *)view
                permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
                                animated:(BOOL)animated;


// Método que oculta el popover.
- (void)dismissPopoverAnimated:(BOOL)animated;


- (void)viewDidUnload;
- (void)viewWillAppear;
- (void)viewDidDisappear:(BOOL)animated;

@end
