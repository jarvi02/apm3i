//
//  Medico.h
//  APM4
//
//  Created by Ezequiel Manacorda on 2/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTEntidad.h"
#import "Potencial.h"
#import "Tratamiento.h"
#import "Domicilio.h"

#import "MTEditActions.h"

//typedef NS_ENUM(NSInteger, ABMMedicoModo) {
//    ABMMedicoModoAlta,
//    ABMMedicoModoModificar
//};

@interface MTMedico : MTEntidad <NSCopying>

/* Propiedades heredadas de MTEntidad:
@property(nonatomic, retain) NSString*  recID;
@property(nonatomic, retain) NSString*  descripcion;
@property(nonatomic, retain) NSString*  direccion;
@property(nonatomic, assign) NSUInteger altura;
@property(nonatomic, retain) NSString*  localidad;
*/

//@property(nonatomic, assign) ABMMedicoModo  modo;
@property(nonatomic, assign) MTEditActionsType  modo;

@property(nonatomic, strong) NSString       *apellido;
@property(nonatomic, strong) NSString       *nombre;
@property(nonatomic, strong) NSString       *actividad;       // M/T
@property(nonatomic, strong) Potencial      *potencial;
@property(nonatomic, strong) NSMutableArray *especialidades;
@property(nonatomic, strong) NSMutableArray *domicilios;
@property(nonatomic, strong) NSMutableArray *obrasSociales;
@property(nonatomic, strong) NSString       *sexo;            //M/F
@property(nonatomic, strong) Tratamiento    *tratamiento;
@property(nonatomic, strong) NSDate         *fechaNacimiento;
@property(nonatomic, strong) NSString       *anioEgreso;
@property(nonatomic, strong) NSString       *CUIT;
@property(nonatomic, strong) NSString       *email;
@property(nonatomic, strong) NSString       *codigoInterno;
@property(nonatomic, strong) NSString       *revisita;
@property(nonatomic, strong) NSString       *matriculaNacional;
@property(nonatomic, strong) NSString       *matriculaProvincial;
@property(nonatomic, strong) NSString       *lugar;
@property(nonatomic, strong) NSString       *descEstado;
@property(nonatomic, strong) NSString       *descABM;
@property(nonatomic, strong) NSString       *descPotencial;
@property(nonatomic, strong) NSString       *descTratamiento;
@property(nonatomic, strong) NSString       *descMarcado;

@property (nonatomic, strong) NSDate        *fechaBaja;
@property (nonatomic, strong) NSDate        *fechaAlta;
@property (nonatomic, strong) NSString      *objetivos;
@property (nonatomic, strong) NSString      *marca;
@property (nonatomic, strong) NSString      *cp;
@property (nonatomic, strong) NSString      *dpto;
@property (nonatomic, strong) NSString      *especialidad1;
@property (nonatomic, strong) NSString      *especialidad2;
@property (nonatomic, strong) NSString      *comentarios;
@property (nonatomic) NSUInteger idMotivoBaja;
@property (nonatomic) NSInteger  asignacion;
@property (nonatomic) NSUInteger idPool;
@property (nonatomic) BOOL       A;
@property (nonatomic) BOOL       B;
@property (nonatomic) BOOL       M;
@property (nonatomic) NSInteger  piso;
@property (nonatomic) NSInteger  desde;
@property (nonatomic) NSInteger  hasta;
@property (nonatomic) BOOL       lu;
@property (nonatomic) BOOL       ma;
@property (nonatomic) BOOL       mi;
@property (nonatomic) BOOL       ju;
@property (nonatomic) BOOL       vi;
@property (nonatomic) BOOL       penAlta;
@property (nonatomic) BOOL       penBaja;

//@property(nonatomic) NSUInteger idMedico;
@property(nonatomic) NSUInteger idEstado;
@property(nonatomic) NSUInteger visitasRealizadas;
@property(nonatomic) NSUInteger visitasProgramadas;

// ValorInicial de cfg_ultimo devuelto por el método cfgUltimoRead.
@property(nonatomic) NSUInteger id_valorinicial;

- (id)init;
- (MTMedico*)copyWithZone:(NSZone*)zone;

-(BOOL)isFluctuante;


// Médicos fluctuantes
- (NSString*)isOkForSaveFluctuante;

// Médicos
- (void)performEditAction: (MTEditActionsType)accion;

- (void)saveAllData;
- (NSString*)isOkForSave;
- (void)updateAllData;


+ (MTMedico*) Medico;

- (NSUInteger)getNewID;
+ (NSArray*) GetAll;
+ (MTMedico*) GetById:(NSUInteger) idMedico;

-(void)updateVisitaData;

@end
