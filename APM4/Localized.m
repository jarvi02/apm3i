//
//  Localized.m
//  RnR
//
//  Created by Fabian E. Pezet Vila on 10/12/12.
//  Copyright (c) 2012 Fabian E. Pezet Vila. All rights reserved.
//

#import "Localized.h"
#import "DB.h"
#import "Config.h"

@interface Localized(private)
-(void)load;
@end

@implementation Localized

@synthesize localize;

+(Localized*)shareInstance{
	static Localized *instance;
	
	if(instance == nil){
		instance = [Localized new];
	}
	return instance;
}

-(id) init {
	
    self = [super init];
	if ( self ){
        dictionary = [[NSMutableDictionary alloc] initWithCapacity:10];
        [self load];
	}
	return self;
}

-(void)dealloc{
    
    [dictionary release];
    [localize release];
    [super dealloc];
}

-(NSString*)find:(NSString*)aKey{
    
    return [dictionary valueForKey:aKey];
}

- (BOOL)isValidKey:(NSString *)key
{
    return [dictionary valueForKey:key]!=nil;
}

-(void)load{
    
    NSString *sqlCount = @"SELECT COUNT(1) FROM IDIOMA WHERE IDIOMA = %@ GROUP BY IDIOMA;";
    NSString *sqlIdioma = @"SELECT key,value FROM IDIOMA WHERE IDIOMA = %@;";
    
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    self.localize = [languages objectAtIndex:0];
    
    NSString *query;
    sqlite3_stmt *statement;
    int count =0;
    
    query = [NSString stringWithFormat:sqlCount, [NSString stringQuoted:self.localize ifNull:@"''"]];

    statement = [[DB getInstance] prepare:query];
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
            count = sqlite3_column_int(statement, 0);
		}
        
	} else {
        NSAssert(0, @"No se encontre la tabla Idioma");
	}
	sqlite3_finalize(statement);

    if (count == 0) {  // si no hay un diccionario para el idima que tiene el equipo, cargo el idima por defecto
        self.localize = [[Config shareInstance] find:@"idioma.defecto"];
    }    
    query = [NSString stringWithFormat:sqlIdioma, [NSString stringQuoted:self.localize ifNull:@"''"]];
    
    
    statement = [[DB getInstance] prepare:query];
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
			
            char *k= (char *)sqlite3_column_text(statement, 0);
            NSString *key = [NSString stringWithUTF8String:k];
            
			char *v= (char *)sqlite3_column_text(statement, 1);
            NSString *value = @"";
            if ((v != nil) && (strlen(v) > 0)) {
                value= [NSString stringWithUTF8String:v];
            }
            
			[dictionary setValue:value forKey:key];
		}
        
	} else {
        NSAssert(0, @"No se encontre la tabla Config");
	}
	sqlite3_finalize(statement);

}


-(void) printAllKeyFor:(NSString*) idioma {
    
    NSString *query;
    NSString *sql = @"select key,value from idioma where idioma = %@;";
    sqlite3_stmt *statement;
    
    query = [NSString stringWithFormat:sql, [NSString stringQuoted:idioma ifNull:@"''"]];
    
    statement = [[DB getInstance] prepare:query];
	if ( statement ) {
        
        int i = 0;
		while (sqlite3_step(statement) == SQLITE_ROW) {

            
            char *k= (char *)sqlite3_column_text(statement, 0);
            NSString *key = [NSString stringWithUTF8String:k];
            
			char *v= (char *)sqlite3_column_text(statement, 1);
            NSString *value = @"";
            if ((v != nil) && (strlen(v) > 0)) {
                value= [NSString stringWithUTF8String:v];
            }
            
            printf("%s\n",[[NSString stringWithFormat:@"%d    %@   =   %@", i++,key, value] UTF8String]);
            
		}
        
	}
    
	sqlite3_finalize(statement);
    
}

@end
