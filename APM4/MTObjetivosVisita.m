//
//  MTObjetivosVisita.m
//  APM4
//
//  Created by Laura Busnahe on 8/8/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTObjetivosVisita.h"
#import "Config.h"
#import "DBExtended.h"

@implementation MTObjetivosVisita

-(id)init
{
    self = [super init];
    
    
    return self;
}

+ (NSArray*)getAllForMedico:(NSInteger)idMedico
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql =
        @"SELECT 0 as _id, fecha, "
            "objetivos, evaluacion "
        "FROM med_visitas "
        "WHERE nodo = %@ AND "
            "medico = %@ "
        "ORDER BY fecha DESC";
    
    NSString *nodo  = SettingForKey(NODO, @"0");    
    NSString *query = [NSString stringWithFormat:sql,
                                nodo,
                                [NSString intToStr:idMedico]];
    
#ifdef DEBUG_OBJETIVOS
    
    NSLog(@"--- MTObjetivos -> getAllForMedico ---");
    NSLog(@" - query: %@", query);
    
#endif
    
    
    char *_c;
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTObjetivosVisita *o = [[MTObjetivosVisita alloc] init];

            // Fecha
            _c = (char *)sqlite3_column_text(statement, 1);
            o.fecha = [[NSDate datetimeFromDB:[NSString pCharToString:_c]] dateAsDateWithoutTime];
            
            // Objetivos
            _c = (char *)sqlite3_column_text(statement, 2);
            o.objetivos = [NSString pCharToString:_c];
            
            // Evaluacion
            _c = (char *)sqlite3_column_text(statement, 3);
            o.evaluacion = [NSString pCharToString:_c];
            
            [result addObject:o];
        }
        
	} else
    {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}

@end
