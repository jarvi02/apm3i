//
//  prescMasterViewController.h
//  APM4
//
//  Created by Laura Busnahe on 2/28/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "prescDetailViewController.h"


@interface prescMasterViewController : UIViewController

@property (strong, nonatomic) prescDetailViewController *detailViewController;

@end
