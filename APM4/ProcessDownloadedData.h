//
//  ProcessDownloadedData.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 01/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ProcessDownloadedDataDelegate <NSObject>

@required
- (void)processDataFinished;
- (void)processDataFailed;

@end

@interface ProcessDownloadedData : NSObject

@property(nonatomic, assign) id<ProcessDownloadedDataDelegate> delegate;

-(void)start;

@end
