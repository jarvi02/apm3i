//
//  DateSelectorViewController.h
//  APM4
//
//  Created by Juan Pablo Garcia on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DateSelectorViewController;


@protocol DateSelectorDelegate <NSObject>

-(void)DateSelector:(DateSelectorViewController*)dateSelector dateSelected:(NSDate *)date;

@end


@interface DateSelectorViewController : UIViewController

@property(nonatomic, retain) NSDate * date;
@property(nonatomic, retain) IBOutlet UIDatePicker * datePicker;
@property(nonatomic, assign) id<DateSelectorDelegate> delegate;

- (IBAction)dateChanged;
- (void)dateDone:(id)sender;

@end
