//
//  MTstdCoberturaEspecialidad.m
//  APM4
//
//  Created by Laura Busnahe on 7/18/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTstdCoberturaEspecialidad.h"
#import "Config.h"
#import "DB.h"

@implementation MTstdCoberturaEspecialidad

+ (NSArray*)getAllWhere:(NSString*)where
{
    NSArray *result = nil;
    
    NSString *sql =
    @"SELECT 0 as _id, desp.descripcion, "
        "SUM(case when asignacion = 1 then  1 else 0 end) as programados, "
        "SUM(case when asignacion != 1 then  1 else 0 end) as fluctuantes, "
        "SUM(case when asignacion = 1 and visitasrealizadas > 0 then 1 else 0 end ) as vistos, "
        "SUM(case when asignacion = 1 and visitasrealizadas > 0 then visitasrealizadas -1 else 0 end ) as revisitas, "
            "ROUND(CAST ( "
                "CAST (SUM(case when asignacion = 1 and visitasrealizadas > 0 then 1 else 0 end)  as float) * 100 "
                " / "
                "CAST( SUM(case when asignacion = 1 then  1 else 0 end) as float ) "
            "as float ),2) as Porcentaje, "
        "SUM(visitasrealizadas) as total "
    "FROM med_medicos INNER JOIN "
         "med_especialidades AS esp ON (esp.nodo = med_medicos.nodo AND "
                                       "esp.medico = med_medicos.id AND "
                                       "esp.orden = 1) INNER JOIN "
         "mtc_especialidades as desp ON (desp.id = esp.especialidad) "
    "WHERE B = 0 AND med_medicos.nodo =  %@ %@ "
    "GROUP BY especialidad1 "
    
    "union all "
    
    "SELECT 1 as _id, 'Totales', "
        "SUM(case when asignacion = 1 then  1 else 0 end) as programados, "
        "SUM(case when asignacion != 1 then  1 else 0 end) as fluctuantes, "
        "SUM(case when asignacion = 1 and visitasrealizadas > 0 then 1 else 0 end ) as vistos,  "
        "SUM(case when asignacion = 1 and visitasrealizadas > 0 then visitasrealizadas -1 else 0 end ) as revisitas, "
            "ROUND(CAST ( "
                "CAST (SUM(case when asignacion = 1 and visitasrealizadas > 0 then 1 else 0 end)  as float) * 100 "
                " / "
                "CAST( SUM(case when asignacion = 1 then  1 else 0 end) as float ) "
            "as float ),2) as Porcentaje, "
        "SUM(visitasrealizadas) as total "
    
    "FROM med_medicos WHERE B = 0 AND nodo = %@ %@ "
    "ORDER BY _id, desp.descripcion";
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql,
                                                    nodo,
                                                    [NSString string:where ifNull:@""],
                                                    nodo,
                                                    [NSString string:where ifNull:@""]];
    
#ifdef DEBUG_COBERTURA
    NSLog(@"--- Cobertura Especialidad ---");
    NSLog(@" - query: %@", query);
    
#endif
    
    result = [self getAll:query];
    
    return result;
}


@end
