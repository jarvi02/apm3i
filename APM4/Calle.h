//
//  Calle.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 16/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calle : NSObject

@property(nonatomic, assign) NSInteger idCalle;
@property(nonatomic, retain) NSString *descripcion;

+(NSArray*) GetAllBy:(NSInteger)pais provincia:(NSInteger)provincia localidad:(NSInteger)localidad;


@end
