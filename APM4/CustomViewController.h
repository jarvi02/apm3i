//
//  CustomViewController.h
//  RnR
//
//  Created by Fabian E. Pezet Vila on 27/11/12.
//  Copyright (c) 2012 Fabian E. Pezet Vila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "loadingViewController.h"

#define TAG_TITULO_HEADER       882
#define TAG_DESCRIPCION_NODO    883
#define TAG_CICLO               884
#define TAG_TITULO              885

@interface CustomViewController : UIViewController

@property (nonatomic, strong)loadingViewController       *loadingView;

-(UIImage*)getBackgroundImage;
-(BOOL)hasHeader;
-(BOOL)hasFooter;
-(UIImage*)getIconoForHeader;
-(NSString*)getTitleForHeader;

-(void) alertTitle:(NSString*) titulo mensaje:(NSString*) mensaje boton:(NSString*) boton;

@end
