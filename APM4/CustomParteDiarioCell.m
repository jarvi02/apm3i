//
//  CustomParteDiarioCell.m
//  APM4
//
//  Created by Juan Pablo Garcia on 20/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomParteDiarioCell.h"

@implementation CustomParteDiarioCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setChecked:(BOOL)checked
{
    if (checked)
    {
        //self.imageView.image = [UIImage imageNamed:@"ok_32.png"];
        self.checkedImage.image = [UIImage imageNamed:@"ok_32.png"];
    }
    else
    {
        //self.imageView.image = [UIImage imageNamed:@"ok_32_ff.png"];
        self.checkedImage.image = [UIImage imageNamed:@"ok_32_off.png"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
    [_descripcionLbl release];
    [_manianaLbl release];
    [_tardeLbl release];
    [_totalLbl release];
    [_checkedImage release];
    [super dealloc];
}

@end
