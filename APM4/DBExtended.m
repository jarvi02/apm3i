//
//  DBExtended.m
//  APM4
//
//  Created by Laura Busnahe on 4/8/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "DBExtended.h"
#import "GlobalDefines.h"

@implementation DB (extensions)


- (NSInteger)getNewIDForTable:(NSString*)tableName idNameOrNil:(NSString*)strID whereOrNil:(NSString*)strWhere
{
    NSInteger result = 0;
    
    NSString *sql = @"select max(%@) from %@ %@";
    NSString *where = @"";
    NSString *idRow = @"id";
    
    if ((strID) && ([strID length] > 0))
        idRow = [NSString stringWithFormat:@"%@", strID];
    
    if ((strWhere) && ([strWhere length] > 0))
        where = [NSString stringWithFormat:@" WHERE (%@)", strWhere];
    
    NSString *query = [NSString stringWithFormat:sql, idRow, tableName, where];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement )
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            // lastID.
            result = sqlite3_column_int(statement, 0) +1;
        }
        
	} else 
    {
        NSAssert(0, @"No se pudo ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}

- (NSUInteger)existsRowForTable:(NSString*)tableName condition:(NSString*)strWhere
{
    NSUInteger result = 0;
    
    NSString *sql = @"SELECT COUNT () FROM %@ %@";
    NSString *where = @"";
    
    if ((strWhere) && ([strWhere length] > 0))
        where = [NSString stringWithFormat:@" WHERE (%@)", strWhere];
    
    NSString *query = [NSString stringWithFormat:sql, tableName, where];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
	if (statement)
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
            // Count
            result = sqlite3_column_int(statement, 0);
        }
	} else
    {
        NSAssert(0, @"No se pudo ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    
    return result;
}


#pragma mark -
#pragma mark Update methods

- (void)setDBVersion:(NSInteger)versionNumber
{
    // Envío la nueva versión a la base de datos.
    NSString *sql = @"";
    sql = [NSString stringWithFormat:@"PRAGMA user_version = %i;", versionNumber];
    
    [self excecuteSQL:sql];
}

- (NSString*)getDBVersion
{
    NSString *result = @"0";
    
    //Pido la versión de usuario de la base de datos.
    NSString *sql = @"PRAGMA user_version;";
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    if ( statement )
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            // Leo la versión de usuario devuelta por la base de datos.
            result = [NSString stringWithFormat:@"%i", sqlite3_column_int(statement, 0) ];
        }
        
	}
#ifdef DEBUG_DBEXTENDED
    NSLog(@"DB Version: %@", result);
#endif
    
    return result;
}


- (BOOL)updateDB
{
    BOOL result = NO;
    
    NSInteger dbVersion = [[self getDBVersion] integerValue];
    
    // Updates de versión 1 -> 2
    if (dbVersion < 2)
    {
        //gst_ajustes
        [self update_cfg_tablastransmision1a2];     // Actualizo la tabla cfg_tablasmision
        
        [self setDBVersion:2];                      // Establezco la nueva versión de la base de datos.
        result = YES;
    }
    
//    // Updates de versión 2 -> 3
//    if (dbVersion < 3)
//    {
//        //gst_ajustes
//        [self update_cfg_tablastransmision2a3];     // Actualizo la tabla cfg_tablasmision
//        
//        [self setDBVersion:3];                      // Establezco la nueva versión de la base de datos.
//        result = YES;
//    }
    
    
#ifdef DEBUG_DBEXTENDED
    if (result)
        NSLog(@"- Base de datos actualizada.");
    else
        NSLog(@"- No es necesario actualizar la Base de datos.");
#endif
    
    return result;
}

- (BOOL)update_cfg_tablastransmision1a2
{
    BOOL result = NO;
    NSInteger newID = 0;
    NSInteger newOrden = 0;
    
    
    // gst_ajustes
    
    // Busco el nuevo ID de la tabla.
    newID = [self getNewIDForTable:@"cfg_tablastransmision"
                       idNameOrNil:@"_id"
                        whereOrNil:nil];
    
    // Busco el nuevo orden para el tipo S.
    newOrden = [self getNewIDForTable:@"cfg_tablastransmision"
                          idNameOrNil:@"orden"
                           whereOrNil:@"tipo = 's'"];
    
    NSString *sql = @"INSERT INTO cfg_tablastransmision "
    "(_id, tabla, tipo, orden) "
    "VALUES(%d, 'gst_ajustes' ,'s', %d)";
    NSString *query;
    
    // Inserto la tabla gst_ajustes dentro de cfg_tablastransmision
    query = [NSString stringWithFormat:sql, newID, newOrden];
    [self excecuteSQL:query];
    
    return result;
}

//- (BOOL)update_cfg_tablastransmision2a3
//{
//    BOOL result = YES;
//  
//    // med_promociones
//    // Agrego la columna "promocionado"
//    NSString *sql = @"ALTER TABLE med_promociones "
//                     "ADD promocionado boolean"
//                     "DEFAULT 'S'";
//   
//    // Ejecuto el SQL
//    [self excecuteSQL:sql];
//    
//    return result;
//}

- (NSMutableArray*)getTableColumnsList:(NSString*)table
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *query = [NSString stringWithFormat:@"PRAGMA table_info(%@);", table];
    
    sqlite3_stmt *statement = [self prepare:query];
    char *_c;
	
    if ( statement )
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            // name
            _c = (char*)sqlite3_column_text(statement, 1);
            NSString *columnName = [NSString pCharToString:_c];
            
            
            [result addObject:columnName];
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
}

@end
