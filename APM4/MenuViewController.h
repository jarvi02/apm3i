//
//  MenuViewController.h
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@protocol MenuContainerProtocol

-(void)navigateToController:(UIViewController *)controller;
-(void)showModalController:(UIViewController *)controller;
-(void)performTask:(NSString*) task;

@end

@interface MenuViewController : UITableViewController
{

}

@property (nonatomic) BOOL forceResize;
@property (nonatomic, assign) id<MenuContainerProtocol> delegate;
@property (nonatomic, retain) MenuViewController *parent;

-(NSArray *)options;
-(void)selectedOptionsAtIndex:(NSUInteger)index;
-(NSString *)title;
-(CGSize)size;
-(BOOL)hasSubMenus;
-(BOOL)hasIcons;
-(void)navigateToSubMenu:(MenuViewController *)subMenu;
-(NSString *)imageNameForIndexPath:(NSIndexPath *)indexPath;

-(void)navigateToController:(UIViewController *)controller;
-(void)showModalController:(UIViewController *)controller;

-(void)showNotImplemented;

-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index;
-(float)getEstimatedHeight;

@end
