//
//  Potencial.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Potencial.h"
#import "DB.h"

@implementation Potencial

- (id)init
{
    self = [super init];
    if (self)
    {
        self.recID = 0;
        self.descripcion = @"";
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [super dealloc];
}

- (NSString*)getDescription
{
    return [NSString stringWithFormat:@"%@", self.descripcion];
}

+(NSArray*) GetAll{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"select 1 as orden, descripcion, id from mtc_potenciales order by orden;";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
        Potencial *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[Potencial alloc] init];
            
            // Orden
            o.orden = sqlite3_column_int(statement, 0);
            
            // Descripción
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
                
            // id
            o.recID = sqlite3_column_int(statement, 2);
			}
                        
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
}

@end
