//
//  CarteraViewController.h
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuViewController.h"
#import "CustomViewController.h"
#import "CustomDoctorCell.h"
#import "CarteraMedico.h"
#import "FilterTableDelegate.h"
#import "listViewController.h"
#import "Domicilio.h"
#import "DateSelectorViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "popListExtended.h"
#import "MTGrupos.h"
#import "MTDescripciones.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface CarteraViewController : CustomViewController <MenuContainerProtocol, UITableViewDataSource, UITableViewDelegate,
                                                         UIActionSheetDelegate, FilterProtocol, UISearchBarDelegate,
                                                         listViewDelegate, DateSelectorDelegate, MTpopListViewDelegate,
                                                         UITextFieldDelegate,MFMailComposeViewControllerDelegate>


@property (retain, nonatomic) IBOutlet UILabel *etiquetaProvincia;
@property (retain, nonatomic) IBOutlet UILabel *etiquetaLocalidad;


@property (nonatomic, retain) NSArray                   *allCartera;
@property (nonatomic, retain) NSArray                   *allData;
@property (nonatomic, retain) NSMutableArray            *data;
@property (nonatomic, retain) NSMutableArray            *filteredData;

@property (retain, nonatomic) IBOutlet UITableView      *table;
@property (retain, nonatomic) IBOutlet CustomDoctorCell *tmpCell;
@property (nonatomic, retain) UINib                     *cellNib;
@property(nonatomic, retain) CarteraMedico              *itemCarteraMedicoSelected;
@property (nonatomic, retain) UIViewController          *trasitionStack;
@property (nonatomic, retain) MTGrupos                  *selectedGrupo;

@property (nonatomic, retain) NSArray                   *descripcionesEstado;
@property (nonatomic, retain) NSArray                   *descripcionesPotencial;


// Busqueda y filtro.
@property (retain, nonatomic) IBOutlet UISearchBar      *searchBar;
@property (retain, nonatomic) IBOutlet UIBarButtonItem  *btnAvanzada;
@property (retain, nonatomic) IBOutlet UIBarButtonItem  *btnFiltros;
-(IBAction)tapAvanzada:(id)sender;
-(IBAction)tapFiltros:(id)sender;


// Filtros
@property (retain, nonatomic) IBOutlet UIButton *lblFiltro;
@property (nonatomic, retain) IBOutlet UIView       *filtrosView;
@property (nonatomic, retain) IBOutlet UITableView  *filtrosTableView;
@property (nonatomic, retain) FilterTableDelegate   *filtrosDelegate;
@property (nonatomic, retain) NSMutableArray        *currentFilters;
@property (nonatomic, retain) IBOutlet UIView                       *mask;
@property (nonatomic, retain) IBOutlet TPKeyboardAvoidingScrollView *kbMask;
@property (retain, nonatomic) IBOutlet UIButton *btnFiltrosAceptar;
@property (retain, nonatomic) IBOutlet UIButton *btnFiltrosLimpiar;
-(IBAction)pressedAceptarFiltros:(id)sender;
-(IBAction)pressedQuitarFiltros:(id)sender;

// Busqueda Avanzada
@property (retain, nonatomic) IBOutlet UIView   *avanzadaView;
@property (retain, nonatomic) IBOutlet UIButton *lblBusquedaAvanzada;
@property (retain, nonatomic) MTpopListView     *popupMenu;
@property (retain, nonatomic) NSDate         *filtroFecha;
@property (retain, nonatomic) Domicilio      *filtroDomicilio;
@property (retain, nonatomic) NSMutableArray *filtroPotencial;
@property (retain, nonatomic) NSMutableArray *filtroEspecialidades;
@property (retain, nonatomic) IBOutlet UIButton     *chkFecha;
@property (retain, nonatomic) IBOutlet UILabel      *lblFecha;
@property (retain, nonatomic) IBOutlet UIButton     *btnFecha;
@property (retain, nonatomic) IBOutlet UIButton     *chkPotencial;
@property (retain, nonatomic) IBOutlet UILabel      *lblPotencial;
@property (retain, nonatomic) IBOutlet UIButton     *btnPotencial;
@property (retain, nonatomic) IBOutlet UIButton     *chkEspecialidad;
@property (retain, nonatomic) IBOutlet UILabel      *lblEspecialidad;
@property (retain, nonatomic) IBOutlet UIButton     *btnEspecialidad;
@property (retain, nonatomic) IBOutlet UIButton     *chkDomicilio;
@property (retain, nonatomic) IBOutlet UITextField  *lblProvincia;
@property (retain, nonatomic) IBOutlet UIButton     *btnProvincia;
@property (retain, nonatomic) IBOutlet UITextField  *lblLocalidad;
@property (retain, nonatomic) IBOutlet UIButton     *btnLocalidad;
@property (retain, nonatomic) IBOutlet UITextField  *lblCalle;
@property (retain, nonatomic) IBOutlet UIButton *btnAvanzadaAceptar;
@property (retain, nonatomic) IBOutlet UIButton *btnAvanzadaLimpiar;
- (IBAction)txtEditingEnd:(id)sender;
- (IBAction)tapChkFecha:(id)sender;
- (IBAction)tapChkPotencial:(id)sender;
- (IBAction)tapChkEspecialidad:(id)sender;
- (IBAction)tapChkDomicilio:(id)sender;
- (IBAction)tapBtnFecha:(id)sender;
- (IBAction)tapBtnPotencial:(id)sender;
- (IBAction)tapBtnEspecialidad:(id)sender;
- (IBAction)tapBtnProvincia:(id)sender;
- (IBAction)tapBtnLocalidad:(id)sender;
- (IBAction)tapAvanzadaAceptar:(id)sender;
- (IBAction)tapAvanzadaCancelar:(id)sender;

- (BOOL)isFiltered;
- (void)addSelectionToGroup:(MTGrupos*)grupo;

@end
