//
//  ConfiguracionViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 14/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ConfiguracionViewController.h"
#import "DB.h"
#import "Config.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"
#import "NSString+Validaciones.h"

@interface ConfiguracionViewController ()

-(void)showForm;
-(void)hideForm;
-(void)load;
-(void)showInvalidPass;

@end

@implementation ConfiguracionViewController

#pragma mark -
#pragma mark LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Configuración";
    }
    return self;
}


-(void) viewDidLoad
{
    [super viewDidLoad];
    
    [self hideForm];
    [self load];
    
    modificar = NO;
    inicializar = NO;
    inicializarChanged = NO;
    
    NSInteger iInicialiar = [[[Config shareInstance] find:INICIALIZAR] integerValue];
    if (iInicialiar > 0)
    {
        [self.inicializarButton setImage:[UIImage imageNamed:@"ok_32"] forState:UIControlStateNormal];
        inicializar = YES;
    } else
    {
        [self.inicializarButton setImage:[UIImage imageNamed:@"ok_32_off"] forState:UIControlStateNormal];
    }
    
    [self.modificarButton setImage:[UIImage imageNamed:@"ok_32_off"] forState:UIControlStateNormal];

}


#pragma mark -
#pragma mark Overrides

-(BOOL)hasFooter
{
    return YES;
}

-(NSString *)getTitleForHeader{
    return @"Configuración del sistema";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"config"];
}

/*-(UIImage*)getBackgroundImage{
    
    return nil;
}*/

#pragma mark -
#pragma mark Actions

-(IBAction)pressedInicializar:(id)sender
{
    NSInteger tranid_ok = [[[Config shareInstance] find:TRANID_OK] integerValue];
    
    if (tranid_ok == 0)
    {
        // Hay una transaccion erronea
        self.inicializarButton.imageView.image = [UIImage imageNamed:@"ok_32_off"];
        
        UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Operación no disponible, Existe una transmisión pendiente" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        
        [av show];
        inicializarChanged = NO;
    }
    else
    {
        inicializarChanged = YES;
        if (inicializar == NO)
        {
            inicializar = YES;
            [self.inicializarButton setImage:[UIImage imageNamed:@"ok_32"] forState:UIControlStateNormal];
        }
        else
        {
            inicializar = NO;
             [self.inicializarButton setImage:[UIImage imageNamed:@"ok_32_off"] forState:UIControlStateNormal];
        }
    }
}

-(IBAction)pressedModificar:(id)sender
{
    modificar = !modificar;
    
    BOOL estaSeleccionado = modificar;
    BOOL usaClaveConfig = ![self.claveConfig isEqualToString:@"N"];
    
    if (estaSeleccionado && usaClaveConfig)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Configuración" message:@"Ingrese la clave" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancelar", nil];
        
        alertView.alertViewStyle = UIAlertViewStyleSecureTextInput;
        alertView.tag = 1;
        [alertView show];
        [alertView release];
    }
    else
    {
        [self showForm];
        self.modificarButton.enabled = NO;
        self.btnModificarOverall.enabled = NO;
        [self.modificarButton setImage:[UIImage imageNamed:@"ok_32"] forState:UIControlStateNormal];
        modificar = YES;

    }
}

-(IBAction)pressedGuardar:(id)sender
{
    if (self.servidor.hidden == NO)
    {
//        if (([self.servidor.text length] == 0) || ![self.servidor.text isValidIP])
//        {
//            [self alertTitle:@"Alerta" mensaje:@"El valor de IP en server 1 no es valido" boton:@"Aceptar"];
//            return;
//        }
//        if (([self.servidor2.text length] == 0) || ![self.servidor2.text isValidIP])
//        {
//            [self alertTitle:@"Alerta" mensaje:@"El valor de IP en server 2 no es valido" boton:@"Aceptar"];
//            return;
//        }
        if (([self.puerto.text length] == 0) || ![self.puerto.text isNumber])
        {
            [self alertTitle:@"Alerta" mensaje:@"El valor del Puerto en server 1 no es valido" boton:@"Aceptar"];
            return;
        }
        if (([self.puerto2.text length] == 0) || ![self.puerto2.text isNumber])
        {
            [self alertTitle:@"Alerta" mensaje:@"El valor del Puerto en server 2 no es valido" boton:@"Aceptar"];
            return;
        }
        if (([self.entidad.text length] == 0) || ![self.entidad.text isNumber])
        {
            [self alertTitle:@"Alerta" mensaje:@"el calor de la entidad no es vaido" boton:@"Aceptar"];
            return;
        }
        if (([self.ciclo.text length] == 0) || ![self.ciclo.text isNumber])
        {
            [self alertTitle:@"Alerta" mensaje:@"El valor del Ciclo no es valido" boton:@"Aceptar"];
            return;
        }
        if (([self.pais.text length] == 0) || ![self.pais.text isNumber])
        {
            [self alertTitle:@"Alerta" mensaje:@"El valor del Pais no es valido" boton:@"Aceptar"];
            return;
        }
        
        [[Config shareInstance] update:REMOTE_HOST value:self.servidor.text];
        [[Config shareInstance] update:REMOTE_PORT value:self.puerto.text];
        [[Config shareInstance] update:REMOTE_HOST_2 value:self.servidor2.text];
        [[Config shareInstance] update:REMOTE_PORT_2 value:self.puerto2.text];
        [[Config shareInstance] update:ENTIDAD value:self.entidad.text];
        [[Config shareInstance] update:CICLO value:self.ciclo.text];
        [[Config shareInstance] update:PAIS value:self.pais.text];
    }
    
    if(inicializarChanged)
        [[Config shareInstance] update:INICIALIZAR value:[NSString stringWithFormat:@"%@", inicializar ? @"1" : @"0"]];
    
    // Mostrar un mensaje y hacer un pop
    RIButtonItem *aceptarItem = [RIButtonItem item];
    aceptarItem.label = @"OK";
    aceptarItem.action = ^ {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    };
    
    NSString *msg = @"Los datos se han actualizado corectamente";
    
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Configuración"
                                                        message:msg
                                               cancelButtonItem:aceptarItem
                                               otherButtonItems:nil];
    [alertView show];
    [alertView release];

}

-(IBAction)pressedCancelar:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UITextFieldDelegate

-(BOOL) textFieldShouldBeginEditing:(UITextField*)textField {
    //activeField = textField;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return NO;
}

- (BOOL)textFieldOLD:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string length] == 0 && range.length > 0)
    {
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return NO;
    } else
    
    if (textField.tag == 1)
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        if ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0)return YES;
    
        return NO;
    } else
        
    if ((textField == self.servidor) || (textField == self.servidor2))
    {
        return YES;
    } else
    
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
        if ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0)return YES;
        
        return NO;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    NSString *oldString = [NSString string:textField.text ifNull:@""];
    NSCharacterSet *notAllowedSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
    if ((textField == self.servidor) || (textField == self.servidor2))
    {
        notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"|$#~"];
    }
    
    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
                            stringByTrimmingCharactersInSet:notAllowedSet];
    
    if ([oldString isEqualToString:tempString])
        return NO;
    
    return   YES;
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 1)
        {
            self.modificarButton.enabled = YES;
            self.btnModificarOverall.enabled = YES;
            [self.modificarButton setImage:[UIImage imageNamed:@"ok_32_off"] forState:UIControlStateNormal];
            modificar = NO;
        }
        else
        {
            NSString *pass = [alertView textFieldAtIndex:0].text;
            
            if (([pass isEqualToString:self.claveConfig]) || (!self.claveConfig)) {
                [self showForm];
                self.modificarButton.enabled = NO;
                self.btnModificarOverall.enabled = NO;
                [self.modificarButton setImage:[UIImage imageNamed:@"ok_32"] forState:UIControlStateNormal];
                modificar = YES;
            } else {
                self.modificarButton.enabled = YES;
                self.btnModificarOverall.enabled = YES;
                [self.modificarButton setImage:[UIImage imageNamed:@"ok_32_off"] forState:UIControlStateNormal];
                modificar = NO;
                
                [self performSelectorOnMainThread:@selector(showInvalidPass) withObject:nil waitUntilDone:NO];
            }
        }
    }
}

#pragma mark -
#pragma mark Private

-(void)showInvalidPass
{
    UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"Configuración" message:@"Clave inválida." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [av show];
}

-(void)showForm
{
    self.servidor.hidden = NO;
    self.servidorlbl.hidden = NO;
    self.servidor2.hidden = NO;
    self.servidor2lb.hidden = NO;
    self.puerto.hidden = NO;
    self.puertolbl.hidden = NO;
    self.puerto2.hidden = NO;
    self.puerto2lb.hidden = NO;
    self.entidad.hidden = NO;
    self.entidadlbl.hidden = NO;
    self.pais.hidden = NO;
    self.paislbl.hidden = NO;
    self.ciclo.hidden = NO;
    self.ciclolbl.hidden = NO;
    self.container.hidden = NO;
    
    self.servidor.text      = SettingForKey(REMOTE_HOST, @"");
    self.puerto.text        = SettingForKey(REMOTE_PORT, @"0");
    self.servidor2.text     = SettingForKey(REMOTE_HOST_2, @"");
    self.puerto2.text       = SettingForKey(REMOTE_PORT_2, @"0");
    self.entidad.text       = SettingForKey(ENTIDAD, @"0");
    self.pais.text          = SettingForKey(PAIS, @"");
    self.ciclo.text         = SettingForKey(CICLO, @"");
    
}

-(void)hideForm
{
    self.servidor.hidden = YES;
    self.servidorlbl.hidden = YES;
    self.puerto.hidden = YES;
    self.puertolbl.hidden = YES;
    self.entidad.hidden = YES;
    self.entidadlbl.hidden = YES;
    self.pais.hidden = YES;
    self.paislbl.hidden = YES;
    self.ciclo.hidden = YES;
    self.ciclolbl.hidden = YES;
    self.container.hidden = YES;
    self.servidor2.hidden = YES;
    self.servidor2lb.hidden = YES;
    self.puerto2.hidden = YES;
    self.puerto2lb.hidden = YES;
}

-(void)load
{

    self.claveConfig = @"N";
    
    NSString *sql = @"select valor from cfg_global where propiedad = 'USACLAVECONFIGURACION'";
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW)
        {
		    char *value = (char *)sqlite3_column_text(statement, 0);
            self.claveConfig = [NSString pCharToString:value];
		}
        
        
	} else
    {
        NSAssert(0, @"No se encontre la tabla cfg_global");
	}
	sqlite3_finalize(statement);
}


#pragma mark -
#pragma mark Memory Management

-(void)dealloc
{
    [_inicializarButton release];
    [_modificarButton release];
    [_puerto release];
    [_servidor release];
    [_pais release];
    [_ciclo release];
    [_entidad release];
    [_puertolbl release];
    [_servidorlbl release];
    [_paislbl release];
    [_ciclolbl release];
    [_entidadlbl release];
    [_claveConfig release];
    [_scrollView release];
    [_container release];
    [_servidor2lb release];
    [_puerto2lb release];
    [_servidor2 release];
    [_puerto2 release];
    
    [_btnModificarOverall release];
    [_btnInicializarOverall release];
    [super dealloc];
}

@end
