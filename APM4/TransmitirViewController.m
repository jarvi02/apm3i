//
//  TransmitirViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "TransmitirViewController.h"
#import "ProcesoTransmicion.h"
#import "ProcesarArchivo.h"

#import "Ciclo.h"
#import "MTFeriado.h"
#import "ParteDiario.h"

#import "Config.h"

@interface TransmitirViewController ()

@end

@implementation TransmitirViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Transmitir";
        serverAvailable = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    self.container.hidden = YES;
    [self.progressView setHidden:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];

    NSString *urlAsString;
    NSURL *url;
    NSURLRequest *urlRequest;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    //urlAsString = [NSString stringWithFormat:@"http://%@",SettingForKey(REMOTE_HOST, @"")];
    urlAsString = [NSString stringWithFormat:@"http://%@:%@/scripts/webmgr.dll?CMD=SVC_PING",
                            SettingForKey(REMOTE_HOST, @""),
                            SettingForKey(REMOTE_PORT, @"")];
    url         = [NSURL URLWithString:urlAsString];
    urlRequest  = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         
         if ([data length] >0 && error == nil)
         {
             self.checkServerAvailable.image = [UIImage imageNamed:@"ok_32.png"];
             self.lbServerAvailable.text = @"Servidor 1: Disponible";
             serverAvailable = 1;
         }
         else
         {
             self.lbServerAvailable.text = @"Servidor 1: No Disponible";
             self.checkServerAvailable.image = [UIImage imageNamed:@"ok_32_off.png"];
         }
         
     }];
    
    
    //urlAsString = [NSString stringWithFormat:@"http://%@",SettingForKey(REMOTE_HOST_2, @"")];
    urlAsString = [NSString stringWithFormat:@"http://%@:%@/scripts/webmgr.dll?CMD=SVC_PING",
                            SettingForKey(REMOTE_HOST_2, @""),
                            SettingForKey(REMOTE_PORT_2, @"")];
    url         = [NSURL URLWithString:urlAsString];
    urlRequest  = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection
     sendAsynchronousRequest:urlRequest
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         
         if ([data length] >0 && error == nil)
         {
             self.checkServer2Available.image = [UIImage imageNamed:@"ok_32.png"];
             self.lbServer2Available.text = @"Servidor 2: Disponible";
             serverAvailable = 2;
         }
         else
         {
             self.checkServer2Available.image = [UIImage imageNamed:@"ok_32_off.png"];
             self.lbServer2Available.text = @"Servidor 2: No Disponible";
         }
         
     }];
    

    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifier];
	[self updateInterfaceWithReachability: internetReach];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)getTitleForHeader{
    return @"Transmitir";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"synchronize_128"];
}


- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{

    if(curReach == internetReach)
	{
		
        NetworkStatus netStatus = [curReach currentReachabilityStatus];
        
        if (netStatus == NotReachable) {
            self.lbInternetAvailable.text = @"Internet: Su dispositivo no se encuentra conectado a una red";
            self.checkInternetAvailable.image = [UIImage imageNamed:@"ok_32_off.png"];
            self.checkServerAvailable.image = [UIImage imageNamed:@"ok_32_off.png"];
            self.checkServer2Available.image = [UIImage imageNamed:@"ok_32_off.png"];
            
            self.lbServerAvailable.text = @"Servidor 1: No Disponible";
            self.lbServer2Available.text = @"Servidor 2: No Disponible";
            
            serverAvailable = 0;
        } else {
            self.lbInternetAvailable.text = @"Internet: Su dispositivo se encuentra conectado a una red";
            self.checkInternetAvailable.image = [UIImage imageNamed:@"ok_32.png"];
        }
        
	}
	
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateInterfaceWithReachability: curReach];
}

- (IBAction)Transmitir:(id)sender {
    
    if ([self isOkToTransmit])
    {
        
        // Muestra la vista de estado de transmisión.
        self.container.hidden = NO;
        [self.spinner startAnimating];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        ((UIButton*)[self.view viewWithTag:666]).enabled = NO;

        
        // Oculto el botón back de la navigation bar.
        self.navigationItem.hidesBackButton = YES;
        
        
        [[ UIApplication sharedApplication ] setIdleTimerDisabled: YES ];
        
    
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
            ProcesoTransmicion *o = [[ProcesoTransmicion alloc] init];
            o.serverAvailable = serverAvailable;
            o.lbStatusTransmisionViewController = self.lbestado;
            o.progressView = self.progressView;
            [o start];

            dispatch_async( dispatch_get_main_queue(), ^{
                
                ((UIButton*)[self.view viewWithTag:666]).enabled = YES;      
                
                self.container.hidden = YES;
                self.navigationItem.hidesBackButton = NO;
                
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [self.spinner stopAnimating];
                
                [[ UIApplication sharedApplication ] setIdleTimerDisabled: NO ];
                
                
                // Cargo nuevamente la Configuración Global.
                [[ConfigGlobal shareInstance] reloadData];
                
                // Tengo que hacer este hack para volver a cargar los datos de configuracion, porque ahi esta el nombre del nodo
                [[Config shareInstance] reloadData];
                ((UILabel*)[self.view viewWithTag:TAG_DESCRIPCION_NODO]).text = SettingForKey(DESCRIPCION_NODO, @"");
                UILabel *cl = (UILabel*)[self.view viewWithTag:TAG_CICLO];
                cl.text = [NSString stringWithFormat:@" Ciclo: %@", [[Config shareInstance] find:CICLO]];
                
            });
        });
        
    
    }
//    else
//    {
//        [self alertTitle:@"Alerta" mensaje:@"No hay ningun servidor disponible" boton:@"Aceptar"];
//        
//        // Oculto nuevamente la View del estado de la transmisión.
//        ((UIButton*)[self.view viewWithTag:666]).enabled = YES;
//        
//        self.container.hidden = YES;
//        self.navigationItem.hidesBackButton = NO;
//        
//        
//        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//        [self.spinner stopAnimating];
//        
//        [[ UIApplication sharedApplication ] setIdleTimerDisabled: NO ];
//    }
    
    
}

- (BOOL) isOkToTransmit
{
 
    BOOL verificaParte = [[ConfigGlobal shareInstance] find:VERIFICAPARTEAYER];
    BOOL inicializar   = [SettingForKey(INICIALIZAR, @"0") isEqualToString:@"1"]? YES: NO;
    
    // Controlo si hay algún servidor esté disponible.
    if (serverAvailable < 1)
    {
        [self alertTitle:@"Alerta" mensaje:@"No hay ningun servidor disponible" boton:@"Aceptar"];
        return NO;
    }
    
    // Chequea si debo verificar el parte diario
    if ((verificaParte) && (!inicializar))
    {
        // Obtengo la fecha de ayer y las fechas entre las que se comprende el ciclo actual.
        Ciclo* cicloActual = [Ciclo actual];
        NSDate* ayer = [[[NSDate date] substractDays:1] dateAsDateWithoutTime];
        cicloActual.desde = [cicloActual.desde dateAsDateWithoutTime];
        cicloActual.hasta = [cicloActual.hasta dateAsDateWithoutTime];
        
        
        // Si la fecha de ayer es anterior al ciclo de inicio   -> Permitir transmitir
        // Si la fecha es posterior al final del ciclo          -> Chequear hasta el fin de ciclo
        // Si la fecha esta dentro del ciclo                    -> Chequear desde el inicio hasta ayer
        
        // Chequeo que la fecha de ayer no sea menos al inicio del ciclo.
        if (!([ayer timeIntervalSince1970] < [cicloActual.desde timeIntervalSince1970]))
        {
            BOOL parteCompleto = YES;
            if ([ayer timeIntervalSince1970] > [cicloActual.hasta timeIntervalSince1970])
                ayer = cicloActual.hasta;
            
            NSArray* dias = [self listadoDiasDesde:cicloActual.desde hasta:ayer];
            NSMutableArray* diasIncompletos = [[NSMutableArray alloc] init];
            // Controlo que haya parte diario cargado para cada uno de los días.
            
            if (DEBUG_VERIFICA_PARTE_AYER)
            {
                NSLog(@"-- Verifica Parte --------");
                NSLog(@"Ayer: %@", [ayer formattedStringUsingFormat:@"dd/MM/yyyy"]);
                NSLog(@"Ciclo desde: %@", [cicloActual.desde formattedStringUsingFormat:@"dd/MM/yyyy"]);
                NSLog(@"Ciclo hasta: %@", [cicloActual.hasta formattedStringUsingFormat:@"dd/MM/yyyy"]);
                //NSLog(@"Listado de días: \n%@", [dias componentsJoinmedByString:@"\n"]);
            }
            
            for (NSDate* d in dias)
            {
                NSInteger cantidadTareas = [ParteDiario getCountWithoutDoctorsForDate:d];
                if ((cantidadTareas < 1))
                {
                    parteCompleto = NO;
                    [diasIncompletos addObject:[d formattedStringUsingFormat:@"dd/MM/yyyy"]];
                }
            }
            
            if (!parteCompleto)
            {
//                [self alertTitle:@"Alerta"
//                         mensaje:[NSString  stringWithFormat:@"El parte diario entre las fechas %@ y %@ no está completo.",
//                                            [cicloActual.desde formattedStringUsingFormat:@"dd/MM/yyyy"],
//                                            [ayer formattedStringUsingFormat:@"dd/MM/yyyy"]]
//                           boton:@"Aceptar"];
                [self alertTitle:@"Alerta"
                         mensaje:[NSString  stringWithFormat:@"No es posible transmitir hasta que el parte diario de las siguiente fechas esté completo:\n%@",
                                  [diasIncompletos componentsJoinedByString:@"\n"]]
                           boton:@"Aceptar"];
                return NO;
            }
        }
        
        
        
    }
    
    return YES;
}

-(NSArray *)listadoDiasDesde:(NSDate *)from hasta:(NSDate *)to
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    // Obtengo todo el listado de feriados.
    NSArray *feriadosList = [MTFeriado getAll];
    
    
    while ([from compare:to] != NSOrderedDescending)
    {
        BOOL visitasFeriadoFinde = TRUE;
        
        // Busca la configuración para saber si el APM hace visitas los fines de semana.
        if ([[ConfigGlobal shareInstance] isValidKey:VIS_FINDESEMANA])
            visitasFeriadoFinde = [[ConfigGlobal shareInstance] find:VIS_FINDESEMANA];
        
        BOOL esSabadoDomingo = NO;
        //        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //        [dateFormatter setDateFormat:@"e"];
        
        NSString* strWeekDay = [from formattedStringUsingFormat:@"e"];
        
        if (([strWeekDay isEqual:@"1"]) ||         // Domingo
            ([strWeekDay isEqual:@"7"]))           // Sábado
            esSabadoDomingo = YES;
        
        // Chequeo que el día no sea feriado ni fin de semana y agrego la fecha al listado.
        if ((![MTFeriado isDate:from intoArray:feriadosList]) &&
            (!esSabadoDomingo))
        {
            [result addObject:[from dateAsDateWithoutTime]];
        } else
            // Chequeo si el APM visita los fines de semana y feriados, si es así también agrego el día al listado.
            if (visitasFeriadoFinde)
            {
                [result addObject:[from dateAsDateWithoutTime]];
            }
        from = [from addDays:1];
    }
    
    return result;
}

@end
