//
//  CloseModalViewControllerDelegate.h
//  RnR
//
//  Created by Fabian E. Pezet Vila on 28/11/12.
//  Copyright (c) 2012 Fabian E. Pezet Vila. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol CloseModalViewControllerDelegate <NSObject>

@required
-(void) closeModalView;

@end
