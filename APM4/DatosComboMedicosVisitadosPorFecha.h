//
//  DatosComboMedicosVisitadosPorFecha.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatosComboMedicosVisitadosPorFecha : NSObject

@property(nonatomic, assign) NSInteger  idDatosComboMedicosVisitadosPorFecha;
@property(nonatomic, retain) NSDate     *fecha;

+(NSArray*)GetAll;

@end
