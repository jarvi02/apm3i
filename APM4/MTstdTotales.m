//
//  MTstdTotales.m
//  APM4
//
//  Created by Laura Busnahe on 7/16/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTstdTotales.h"
#import "Config.h"
#import "DBExtended.h"

@implementation MTstdTotales

- (id)init
{
    self = [super init];
    if (self)
    {
        
        sTotalesValores s;
        s.maniana = 0;
        s.tarde = 0;
        s.total = 0;
        
        sfTotalesValores sf;
        sf.maniana = 0;
        sf.tarde = 0;
        sf.total = 0;
        
        sTotalesFijosRevisitas sFijosRevisita;
        sFijosRevisita.programados  = s;
        sFijosRevisita.vistos       = s;
        sFijosRevisita.porcenataje  = sf;
        sFijosRevisita.media        = sf;
        
        sTotalesFluctuantes sFluctuantes;
        sFluctuantes.visitados      = s;
        sFluctuantes.revisitas      = s;
        
        sTotalesTotal sTotal;
        sTotal.visitados            = s;
        sTotal.media                = sf;
        
        
        // Periodos trabajados
        self.periodosTrabajados  = s;
        
        // Fijos
        self.fijos               = sFijosRevisita;
        
        // Revisitas
        self.revisitas           = sFijosRevisita;
        
        // Fluctuantes
        self.fluctuantes         = sFluctuantes;
        
        // Total
        self.total               = sTotal;
    }
    return self;
}

+ (MTstdTotales*)getTotales
{
    MTstdTotales *result = nil;
    
    NSString *sql;
    sql = @"SELECT "
    "sum(case  when asignacion = 1 then "
    "case when actividad = 'M' then 1 else 0 end "
    "else 0 end) as prog_m, "
    
    "sum(case  when asignacion = 1 then "
    "case when actividad = 'T' then 1 else 0 end "
    "else 0 end) as prog_t, "
    
    "sum(case when asignacion = 1   then 1 else 0 end) as prog_total, "
    
    "sum(case  when asignacion = 1 then "
    "case when actividad = 'M' then "
    "case when estado = 1 then 1 else 0 end "
    "else 0 end "
    "else 0 end) as prog_vis_m, "
    
    "sum(case  when asignacion = 1 then "
    "case when actividad = 'T' then "
    "case when estado = 1 then 1 else 0 end "
    "else 0 end "
    "else 0 end) as prog_vis_t, "
    
    "sum(case when asignacion = 1  then "
    "case when estado = 1 then 1 else 0 end "
    "else 0 end) as prog_vis_total, "
    
    "sum(case  when asignacion = 1 then "
    "case when actividad = 'M' then visitasprogramadas-1 else 0 end "
    "else 0 end) as revisprog_m, "
    
    "sum(case  when asignacion = 1 then "
    "case when actividad = 'T' then visitasprogramadas-1 else 0 end "
    "else 0 end) as revisprog_t,  "
    
    "sum(case when asignacion = 1   then visitasprogramadas-1 else 0 end) as revisprog_total, "
    
    "sum(case  when asignacion = 1 then "
    "case when actividad = 'M' then "
    "case when visitasrealizadas > 0 then visitasrealizadas-1 else 0 end "
    "else 0 end "
    "else 0 end) as revis_m, "
    
    "sum(case  when asignacion = 1 then "
    "case when actividad = 'T' then  "
    "case when visitasrealizadas > 0 then visitasrealizadas-1 else 0 end "
    "else 0 end  "
    "else 0 end) as revis_t, "
    
    "sum(case when asignacion = 1  then "
    "case when visitasrealizadas > 0 then visitasrealizadas-1 else 0 end "
    "else 0 end) as revis_total, "
    
    "sum(case  when asignacion = 2 then "
    "case when actividad = 'M' then 1 else 0 end "
    "else 0 end) as fluc_visto_m, "
    
    "sum(case  when asignacion = 2 then "
    "case when actividad = 'T' then 1 else 0 end "
    "else 0 end) as fluc_visto_t, "
    
    "sum(case when asignacion = 2 then 1 else 0 end) as fluc_visto_total, "
    
    "sum(case  when asignacion = 2 then "
    "case when actividad = 'M' then "
    "case when visitasrealizadas > 0 then visitasrealizadas-1 else 0 end "
    "else 0 end "
    "else 0 end) as fluc_revis_m, "
    
    "sum(case  when asignacion = 2 then "
    "case when actividad = 'T' then "
    "case when visitasrealizadas > 0 then visitasrealizadas-1 else 0 end "
    "else 0 end "
    "else 0 end) as fluc_revis_t, "
    
    "sum(case when asignacion = 2  then "
    "case when visitasrealizadas > 0 then visitasrealizadas-1 else 0 end "
    "else 0 end) as fluc_revis_total, "
    
    "sum(case when actividad = 'M' then visitasrealizadas else 0 end ) as total_visitas_m, "
    
    "sum(case when actividad = 'T' then visitasrealizadas else 0 end) as total_visitas_t, "
    
    "sum(visitasrealizadas) as total_visitas "
    
    "FROM med_medicos " 
    "WHERE B = 0 and nodo = %@";
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql, nodo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        
        result = [[MTstdTotales alloc] init];

		if (sqlite3_step(statement) == SQLITE_ROW)
        {
            sTotalesValores s;
            s.maniana = 0;
            s.tarde = 0;
            s.total = 0;
            
            sfTotalesValores sf;
            sf.maniana = 0;
            sf.tarde = 0;
            sf.total = 0;
            
            sTotalesFijosRevisitas sFijosRevisita;
            sFijosRevisita.programados  = s;
            sFijosRevisita.vistos       = s;
            sFijosRevisita.porcenataje  = sf;
            sFijosRevisita.media        = sf;
            
            sTotalesFluctuantes sFluctuantes;
            sFluctuantes.visitados      = s;
            sFluctuantes.revisitas      = s;
            
            sTotalesTotal sTotal;
            sTotal.visitados            = s;
            sTotal.media                = sf;
            
            sFijosRevisita = result.fijos;
            // prog_m, prog_vis_t, prog_vis_total
            s.maniana = sqlite3_column_int(statement, 0);
            s.tarde = sqlite3_column_int(statement, 1);
            s.total = sqlite3_column_int(statement, 2);
            sFijosRevisita.programados = s;
            
            // prog_vis_m, prog_vis_t, prog_vis_total
            s.maniana = sqlite3_column_int(statement, 3);
            s.tarde = sqlite3_column_int(statement, 4);
            s.total = sqlite3_column_int(statement, 5);
            sFijosRevisita.vistos = s;
            
            result.fijos = sFijosRevisita;
            
            
            sFijosRevisita = result.revisitas;
            // revisprog_m, revisprog_t, revisprog_total
            s.maniana = sqlite3_column_int(statement, 6);
            s.tarde = sqlite3_column_int(statement, 7);
            s.total = sqlite3_column_int(statement, 8);
            sFijosRevisita.programados = s;
            
            // revis_m, revis_t, revis_total
            s.maniana = sqlite3_column_int(statement, 9);
            s.tarde = sqlite3_column_int(statement, 10);
            s.total = sqlite3_column_int(statement, 11);
            sFijosRevisita.vistos = s;
            
            result.revisitas = sFijosRevisita;
            
            sFluctuantes = result.fluctuantes;
            // fluc_visto_m, fluc_visto_t, fluc_visto_total
            s.maniana = sqlite3_column_int(statement, 12);
            s.tarde = sqlite3_column_int(statement, 13);
            s.total = sqlite3_column_int(statement, 14);
            sFluctuantes.visitados = s;
            
            // fluc_revis_m, fluc_revis_t, fluc_revis_total
            s.maniana = sqlite3_column_int(statement, 15);
            s.tarde = sqlite3_column_int(statement, 16);
            s.total = sqlite3_column_int(statement, 17);
            sFluctuantes.revisitas = s;
            
            sTotal = result.total;
            // total_visitas_m, total_visitas_t, total_visitas
            s.maniana = sqlite3_column_int(statement, 18);
            s.tarde = sqlite3_column_int(statement, 19);
            s.total = sqlite3_column_int(statement, 20);
            sTotal.visitados = s;
            
            result.total = sTotal;
            
            [result updateValues];
        }
        
	} else {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);

    
    return result;
}

- (void)updateValues
{
    sTotalesFijosRevisitas sFR;
    sTotalesTotal          sT;
    
    // Fijos
    sFR = self.fijos;
    sFR.porcenataje = [self getPorcentaje:self.fijos.programados visitas:self.fijos.vistos];
    sFR.media       = [self getMedia:self.fijos.vistos];
    self.fijos = sFR;
    
    // Revisitas
    sFR = self.revisitas;
    sFR.porcenataje = [self getPorcentaje:self.revisitas.programados visitas:self.revisitas.vistos];
    sFR.media       = [self getMedia:self.revisitas.vistos];
    self.revisitas = sFR;
    
    // Total;
    sT = self.total;
    sT.media = [self getMedia:self.total.visitados];
    self.total = sT;
}

- (sfTotalesValores)getPorcentaje:(sTotalesValores)programados visitas:(sTotalesValores)visitas
{
    sfTotalesValores sfZ;
    sfZ.maniana = 0.0f;
    sfZ.tarde = 0.0f;
    sfZ.total = 0.0f;
    
    sfTotalesValores result = sfZ;
    
    if (programados.maniana > 0)
        result.maniana = (visitas.maniana * 100.0f) / (double)(programados.maniana);
    
    if (programados.tarde > 0)
        result.tarde = (visitas.tarde * 100.0f) / (double)(programados.tarde);
    
    if (programados.total > 0)
        result.total = (visitas.total * 100.0f) / (double)(programados.total);
    
    return result;
}

- (sfTotalesValores)getMedia:(sTotalesValores)visitas
{
    sfTotalesValores sfZ;
    sfZ.maniana = 0.0f;
    sfZ.tarde = 0.0f;
    sfZ.total = 0.0f;
    
    sfTotalesValores result = sfZ;
    
    if (self.periodosTrabajados.maniana > 0)
        result.maniana = (double)(visitas.maniana) / (double)(self.periodosTrabajados.maniana);
    
    if (self.periodosTrabajados.tarde > 0)
        result.tarde = (double)(visitas.tarde) / (double)(self.periodosTrabajados.tarde);
    
    if (self.periodosTrabajados.total > 0)
        result.total = (double)(visitas.total) / (double)(self.periodosTrabajados.total);
    
    
    return result;
}


@end
