//
//  NoVisitaPorMotivoViewController.h
//  APM4
//
//  Created by Laura Busnahe on 9/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "CustomViewController.h"
#import "NoVisitaPorMotivoCell.h"
#import "NoVisitaPorMotivoHeader.h"
#import "NoVisitaPorMotivoFooter.h"

#import "MTstdNoVisita.h"

@interface NoVisitaPorMotivoViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate,
                                                                   MTNoVisitaPorMotivoHeaderDelegate>

@property (retain, nonatomic) IBOutlet UITextField  *lblPeriodo;
@property (retain, nonatomic) IBOutlet UIButton     *btnPeriodo;

@property (nonatomic, strong) NSMutableArray                 *data;
@property (nonatomic, strong) IBOutlet NoVisitaPorMotivoCell *tmpCell;
@property (nonatomic, strong) UINib                          *cellNib;
@property (nonatomic, strong) UINib                          *headerNib;

@property (strong, nonatomic) IBOutlet NoVisitaPorMotivoHeader *tableHeader;
@property (strong, nonatomic) IBOutlet NoVisitaPorMotivoFooter *tableFooter;
@property (strong, nonatomic) IBOutlet UITableView          *tableView;

- (id)initViewDefaultNib;

@end
