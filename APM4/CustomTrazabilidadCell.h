//
//  CustomTrazabilidadCell.h
//  APM4
//
//  Created by Juan Pablo Garcia on 05/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTrazabilidadCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UIImageView *selectedImage;
@property(nonatomic, strong) IBOutlet UILabel *producto;
@property(nonatomic, strong) IBOutlet UILabel *stock;
@property(nonatomic, strong) IBOutlet UILabel *lote;
@property(nonatomic, strong) IBOutlet UILabel *cantidad;
@property(nonatomic, strong) IBOutlet UILabel *codigo;
@property(nonatomic, strong) IBOutlet UILabel *vencimiento;
@property(nonatomic, strong) IBOutlet UILabel *orden;
@property(nonatomic, strong) IBOutlet UILabel *literatura;
@property(nonatomic, strong) IBOutlet UILabel *obsequio;
@property(nonatomic, strong) IBOutlet UILabel *promocionado;
@property(nonatomic, strong) IBOutlet UILabel *objetivos;
@property(nonatomic, strong) IBOutlet UILabel *orden2;

@property (nonatomic, strong) IBOutlet UILabel *lblPromocionado;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@end
