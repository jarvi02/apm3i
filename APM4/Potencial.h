//
//  Potencial.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Potencial : NSObject

@property(nonatomic) NSUInteger        recID;
@property(nonatomic, assign) NSInteger orden;
@property(nonatomic, retain) NSString *descripcion;

- (NSString*)getDescription;

+(NSArray*) GetAll;

@end
