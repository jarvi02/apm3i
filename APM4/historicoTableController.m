//
//  historicoTableController.m
//  APM4
//
//  Created by Laura Busnahe on 7/12/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "historicoTableController.h"
#import "MTHistoricoProductos.h"

#import "NSStringExtended.h"

@interface historicoTableController ()

@end

@implementation historicoTableController

- (id)initWithData:(NSArray*)aData
{
    self = [super init];
    
    if (self)
    {
        self.cellNib    = [UINib nibWithNibName:@"historicoCell" bundle:nil];
        
        self.headerNib  = [UINib nibWithNibName:@"historicoHeader" bundle:nil];
        [self.headerNib instantiateWithOwner:self options:nil];
        
        self.data       = aData;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"historicoCelldentifier";
    
    // Obtain the cell object.
    historicoCell *cell = (historicoCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
    	cell = self.tmpCell;
    	self.tmpCell = nil;
    }
    
    // Obtengo los datos para la celada.
    MTHistoricoProductos *o = [self.data objectAtIndex:indexPath.row];
    
    // Celdas de Orden
    cell.txtOrden01.text = [NSString string:o.orden01 ifNull:@""];
    cell.txtOrden02.text = [NSString string:o.orden02 ifNull:@""];
    cell.txtOrden03.text = [NSString string:o.orden03 ifNull:@""];
    cell.txtOrden04.text = [NSString string:o.orden04 ifNull:@""];
    cell.txtOrden05.text = [NSString string:o.orden05 ifNull:@""];
    cell.txtOrden06.text = [NSString string:o.orden06 ifNull:@""];
    cell.txtOrden07.text = [NSString string:o.orden07 ifNull:@""];
    cell.txtOrden08.text = [NSString string:o.orden08 ifNull:@""];
    cell.txtOrden09.text = [NSString string:o.orden09 ifNull:@""];
    cell.txtOrden10.text = [NSString string:o.orden10 ifNull:@""];
    cell.txtOrden11.text = [NSString string:o.orden11 ifNull:@""];
    cell.txtOrden12.text = [NSString string:o.orden12 ifNull:@""];
    
    // Celdas de Producto
    cell.txtProducto01.text = [NSString string:o.descripcion01 ifNull:@""];
    cell.txtProducto02.text = [NSString string:o.descripcion02 ifNull:@""];
    cell.txtProducto03.text = [NSString string:o.descripcion03 ifNull:@""];
    cell.txtProducto04.text = [NSString string:o.descripcion04 ifNull:@""];
    cell.txtProducto05.text = [NSString string:o.descripcion05 ifNull:@""];
    cell.txtProducto06.text = [NSString string:o.descripcion06 ifNull:@""];
    cell.txtProducto07.text = [NSString string:o.descripcion07 ifNull:@""];
    cell.txtProducto08.text = [NSString string:o.descripcion08 ifNull:@""];
    cell.txtProducto09.text = [NSString string:o.descripcion09 ifNull:@""];
    cell.txtProducto10.text = [NSString string:o.descripcion10 ifNull:@""];
    cell.txtProducto11.text = [NSString string:o.descripcion11 ifNull:@""];
    cell.txtProducto12.text = [NSString string:o.descripcion12 ifNull:@""];
    

    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader;
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader.frame.size.height;
    else
        return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
