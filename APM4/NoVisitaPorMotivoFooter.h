//
//  NoVisitaPorMotivoFooter.h
//  APM4
//
//  Created by Laura Busnahe on 9/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoVisitaPorMotivoFooter : UIView

@property (retain, nonatomic) IBOutlet UIButton *lblMotivo;
@property (retain, nonatomic) IBOutlet UIButton *lblManiana;
@property (retain, nonatomic) IBOutlet UIButton *lblTarde;
@property (retain, nonatomic) IBOutlet UIButton *lblTotal;

@end
