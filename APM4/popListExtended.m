//
//  popListExtended.m
//  APM4
//
//  Created by Laura Busnahe on 6/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "popListExtended.h"
#import "Provincia.h"
#import "Localidad.h"
#import "Especialidad.h"
#import "MTTiposDomicilios.h"
#import "Tratamiento.h"
#import "MotivoBaja.h"


@implementation MTpopListView (extensions)

- (id)initForProvinciasWithDelegate:(id)idDelegate
{
    NSArray *list = [[NSArray alloc] init];
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(350, 500);
    
    // Cargo el listado de provincias.
    list = [Provincia GetAll];
    
    // Creo el objeto, asigno el título, el listado de datos, y defino que utilizará búsqueda.
    if (self)
    {
        self = [self initWithRect:rect];
    }
    self.delegate = idDelegate;
    [self dismissPopoverAnimated:NO];
    
    [self setTitleAndParameters:[[MTRegionalDictionary sharedInstance] valueForText:@"Provincias"] rowData:list];
    [self setSize:rect.size];
    [self useSearch:YES];
    
    return self;
}

- (id)initForLocalidadesForProvincia:(NSUInteger)idProvincia WithDelegate:(id)idDelegate
{
    NSArray *list = [[NSArray alloc] init];
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(350, 500);
    
    // Cargo el listado de provincias.
    list = [Localidad GetAllByProvincia:idProvincia];
    
    // Creo el objeto, asigno el título, el listado de datos, y defino que utilizará búsqueda.
    if (self)
    {
        self = [self initWithRect:rect];
    }
    self.delegate = idDelegate;
    [self dismissPopoverAnimated:NO];
    
    [self setTitleAndParameters:[[MTRegionalDictionary sharedInstance] valueForText:@"Localidades"] rowData:list];
    [self setSize:rect.size];
    [self useSearch:YES];
    
    return self;
}

- (id)initForEspecialidadesWithDelegate:(id)idDelegate
{
    NSArray *list = [[NSArray alloc] init];
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(350, 500);
    
    // Cargo el listado de especialidades.
    list = [Especialidad GetAll];
    
    // Creo el objeto, asigno el título, el listado de datos, y defino que utilizará búsqueda.
    if (self)
    {
        self = [self initWithRect:rect];
    }
    self.delegate = idDelegate;
    [self dismissPopoverAnimated:NO];
    
    [self setTitleAndParameters:[[MTRegionalDictionary sharedInstance] valueForText:@"Especialidades"] rowData:list];
    [self setSize:rect.size];
    [self useSearch:YES];
    
    return self;
}

- (id)initForTiposDomiciliosWithDelegate:(id)idDelegate
{
    NSArray *list = [[NSArray alloc] init];
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(350, 500);
    
    // Cargo el listado de tipos de domicilios.
    list = [MTTiposDomicilios getAll];
    
    // Creo el objeto, asigno el título, el listado de datos, y defino que utilizará búsqueda.
    if (self)
    {
        self = [self initWithRect:rect];
    }
    self.delegate = idDelegate;
    [self dismissPopoverAnimated:NO];
    
    [self setTitleAndParameters:[[MTRegionalDictionary sharedInstance] valueForText:@"Tipos de domicilios"] rowData:list];
    [self setSize:rect.size];
    [self useSearch:YES];
    
    return self;
}

- (id)initForTratamientosWithDelegate:(id)idDelegate
{
    NSArray *list = [[NSArray alloc] init];
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(350, 500);
    
    // Cargo el listado de tratamientos.
    list = [Tratamiento GetAll];
    
    // Creo el objeto, asigno el título, el listado de datos, y defino que utilizará búsqueda.
    if (self)
    {
        self = [self initWithRect:rect];
    }
    
    self.delegate = idDelegate;
    [self dismissPopoverAnimated:NO];
    
    [self setTitleAndParameters:[[MTRegionalDictionary sharedInstance] valueForText:@"Tratamientos"] rowData:list];
    [self setSize:rect.size];
    [self useSearch:YES];
    
    return self;
}

- (id)initForMotivoBajaWithDelegate:(id)idDelegate
{
    NSArray *list = [[NSArray alloc] init];
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(350, 500);
    
    // Cargo el listado de especialidades.
    list = [MotivoBaja GetAll];
    
    // Creo el objeto, asigno el título, el listado de datos, y defino que utilizará búsqueda.
    if (self)
    {
        self = [self initWithRect:rect];
    }
    self.delegate = idDelegate;
    [self dismissPopoverAnimated:NO];
    
    [self setTitleAndParameters:[[MTRegionalDictionary sharedInstance] valueForText:@"Motivo de baja"] rowData:list];
    [self setSize:rect.size];
    [self useSearch:YES];
    
    return self;
}

@end
