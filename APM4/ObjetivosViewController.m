//
//  ObjetivosViewController.m
//  APM4
//
//  Created by Laura Busnahe on 8/8/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ObjetivosViewController.h"
#import "MTObjetivosVisita.h"

@interface ObjetivosViewController ()

@end

@implementation ObjetivosViewController
{
    NSString *descTitulo;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.data = nil;
        
        self.cellNib    = [UINib nibWithNibName:@"customObjetivosCell" bundle:nil];
        self.headerNib  = [UINib nibWithNibName:@"customObjetivosHeader" bundle:nil];
        [self.headerNib instantiateWithOwner:self options:nil];
        
        descTitulo = @"Objetivos";
        self.title = descTitulo;
    }
    return self;
}

- (id)initWithDefaultNib
{
    self = [self initWithNibName:@"ObjetivosViewController" bundle:nil];
    
    return self;
}

-(NSString *)getTitleForHeader
{
    NSString *nombreMedico = @"";
    if (self.carteraViewControllerReference)
        nombreMedico = [NSString string:self.carteraViewControllerReference.itemCarteraMedicoSelected.apellidoNombre ifNull:@"?"];
    
    descTitulo = [NSString stringWithFormat:@"%@ - %@",
                  descTitulo , nombreMedico];
    
    return descTitulo;
}

-(UIImage *)getIconoForHeader
{
    return [UIImage imageNamed:@"cartera_48"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = descTitulo;
    
    // Objetivo próxima visita
    self.lblObjetivo.text = [NSString string:self.carteraViewControllerReference.itemCarteraMedicoSelected.objetivos ifNull:@""];
    
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.data = [MTObjetivosVisita getAllForMedico:self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico];
    
    [self.tableView reloadData];
    [self.loadingView hide];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -
#pragma mark Table methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"customObjetivosCelldentifier";
    
    // Obtain the cell object.
    customObjetivosCell *cell = (customObjetivosCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
    	cell = self.tableCell;
    	self.tableCell = nil;
    }
    
    // Obtengo los datos para la celada.
    MTObjetivosVisita *o = [self.data objectAtIndex:indexPath.row];
    
    // Datos de las celdas
    cell.lblFecha.text      = [o.fecha formattedStringUsingFormat:@"dd/MM/yyyy"];
    cell.lblObjetivo.text   = [NSString string:o.objetivos ifNull:@"(Sin objetivo)"];
    cell.lblEvaluacion.text = [NSString string:o.evaluacion ifNull:@""];
    
    
    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader;
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader.frame.size.height;
    else
        return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
