//
//  EvaluacionPromocionesViewController.h
//  APM4
//
//  Created by Laura Busnahe on 9/2/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "CarteraViewController.h"
#import "EvaluacionProductosHeader.h"
#import "EvaluacionProductosCell.h"
#import "EvaluacionVisitasCell.h"
#import "EvaluacionVisitasHeader.h"

#import "MTEvaluacionProductos.h"
#import "MTEvaluacionVisitas.h"

@interface EvaluacionPromocionesViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)IBOutlet EvaluacionProductosCell       *productosCell;
@property (nonatomic, strong)IBOutlet EvaluacionProductosHeader     *productosHeader;
@property (nonatomic, strong)IBOutlet EvaluacionVisitasCell         *visitasCell;
@property (nonatomic, strong)IBOutlet EvaluacionVisitasHeader       *visitasHeader;
@property (nonatomic, strong)IBOutlet UITableView                   *productosTableView;
@property (nonatomic, strong)IBOutlet UITableView                   *visitasTableView;

@property (retain, nonatomic) IBOutlet UIButton *lblProductos;
@property (retain, nonatomic) IBOutlet UIButton *lblVisitas;


@property (nonatomic, strong) NSArray                        *dataProductos;
@property (nonatomic, strong) NSArray                        *dataVisitas;

@property (nonatomic, strong) UINib                          *cellProductosNib;
@property (nonatomic, strong) UINib                          *headerProductosNib;
@property (nonatomic, strong) UINib                          *cellVisitasNib;
@property (nonatomic, strong) UINib                          *headerVisitasNib;


@property(nonatomic, assign) CarteraViewController *carteraViewControllerReference;

- (id)initWithDefaultNib;



@end
