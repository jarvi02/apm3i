//
//  DistribucionCantidadCell.h
//  APM4
//
//  Created by Laura Busnahe on 9/9/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DistribucionCantidadCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblDescripcion;
@property (retain, nonatomic) IBOutlet UILabel *lblManiana;
@property (retain, nonatomic) IBOutlet UILabel *lblPorcManiana;
@property (retain, nonatomic) IBOutlet UILabel *lblTarde;
@property (retain, nonatomic) IBOutlet UILabel *lblPorcTarde;
@property (retain, nonatomic) IBOutlet UILabel *lblTotal;
@property (retain, nonatomic) IBOutlet UILabel *lblPorcTotal;

@end
