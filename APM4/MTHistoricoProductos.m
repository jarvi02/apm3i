//
//  MTHistoricoProductos.m
//  APM4
//
//  Created by Laura Busnahe on 7/12/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTHistoricoProductos.h"
#import "Config.h"
#import "DBExtended.h"

#import "GlobalDefines.h"
#import "NSStringExtended.h"

@implementation MTHistoricoProductos

- (id)init
{
    self = [super init];
    if (self)
    {
        self.orden01 = @"";
        self.orden02 = @"";
        self.orden03 = @"";
        self.orden04 = @"";
        self.orden05 = @"";
        self.orden06 = @"";
        self.orden07 = @"";
        self.orden08 = @"";
        self.orden09 = @"";
        self.orden10 = @"";
        self.orden11 = @"";
        self.orden12 = @"";
        
        self.descripcion01 = @"";
        self.descripcion02 = @"";
        self.descripcion03 = @"";
        self.descripcion04 = @"";
        self.descripcion05 = @"";
        self.descripcion06 = @"";
        self.descripcion07 = @"";
        self.descripcion08 = @"";
        self.descripcion09 = @"";
        self.descripcion10 = @"";
        self.descripcion11 = @"";
        self.descripcion12 = @"";
    }
    return self;
}

+ (NSArray*)getAllForMedico:(NSUInteger)idMedico
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    NSString *sql = @"";
    
    sql = @"SELECT "
                "orden1, descprod1, "
                "orden2, descprod2, "
                "orden3, descprod3, "
                "orden4, descprod4, "
                "orden5, descprod5, "
                "orden6, descprod6, "
                "orden7, descprod7, "
                "orden8, descprod8, "
                "orden9, descprod9, "
                "orden10, descprod10, "
                "orden11, descprod11, "
                "orden12, descprod12 "
            "FROM std_ordenpromocion_ext "
            "WHERE nodo = %@ and medico = %d order by secuencia";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql, nodo, idMedico];
    
#ifdef DEBUG_HISTORICO
    NSLog(@"--- Historico producto ---");
    NSLog(@" query: %@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement )
    {
        
        MTHistoricoProductos *o;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[MTHistoricoProductos alloc] init];
            
            char *_d;
            
            // Ciclo 1
            _d = (char *)sqlite3_column_text(statement, 0);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden01 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 1);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion01 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 2
            _d = (char *)sqlite3_column_text(statement, 2);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden02 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 3);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion02 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 3
            _d = (char *)sqlite3_column_text(statement, 4);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden03 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 5);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion03 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 4
            _d = (char *)sqlite3_column_text(statement, 6);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden04 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 7);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion04 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 5
            _d = (char *)sqlite3_column_text(statement, 8);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden05 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 9);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion05 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 6
            _d = (char *)sqlite3_column_text(statement, 10);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden06 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 11);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion06 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 7
            _d = (char *)sqlite3_column_text(statement, 12);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden07 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 13);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion07 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 8
            _d = (char *)sqlite3_column_text(statement, 14);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden08 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 15);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion08 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 9
            _d = (char *)sqlite3_column_text(statement, 16);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden09 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 17);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion09 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 10
            _d = (char *)sqlite3_column_text(statement, 18);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden10 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 19);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion10 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 11
            _d = (char *)sqlite3_column_text(statement, 20);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden11 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 21);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion11 = [NSString stringWithUTF8String:_d];
			}
            
            // Ciclo 12
            _d = (char *)sqlite3_column_text(statement, 22);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.orden12 = [NSString stringWithUTF8String:_d];
			}
            
            _d = (char *)sqlite3_column_text(statement, 23);
            if((_d != nil) && (strlen(_d) > 0))
            {
                o.descripcion12 = [NSString stringWithUTF8String:_d];
			}

            [array addObject:o];
        }
        
	} else
    {
        NSAssert(0, @"No se pudo ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return array;
}

@end
