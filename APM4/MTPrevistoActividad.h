//
//  MTPrevistoActividad.h
//  APM4
//
//  Created by Laura Busnahe on 2/14/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTEntidad.h"

// Constantes de tipo de Entidad para la base de datos.
#define TEDB_OTROS        1
#define TEDB_MEDICO       2
#define TEDB_INSTITUCION  3

typedef enum {
    kTEUnknown,
    kTEMedico,
    kTEInstitucion
} MTEntityType;


@interface MTPrevistoActividad : NSObject <NSCopying>

@property (nonatomic, retain) MTEntidad*    entidad;
@property (nonatomic, assign) MTEntityType  tipoEntidad;
@property (nonatomic, retain) NSDate*       entrada;
@property (nonatomic, retain) NSDate*       salida;


- (NSString*)getDescription;            // Devuelve la descripción de la actividad con los datos del horario y entidad.
- (NSString*)getDireccion;              // Devuelve la dirección de la activadad si es que tiene definada alguna.
- (NSUInteger)getDbEntityType;          // Devuelve el número de tipo para la Base de Datos correspondiente a la entided.
- (void)setEntityTypeFromDBvalue:(NSUInteger)dbEntityType;       // Devuelve el tipo de Entidad segun el número leido desde la Base de Datos.

- (MTPrevistoActividad*)copyWithZone:(NSZone*)zone;
@end
