//
//  Institucion.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Institucion : NSObject

@property(nonatomic, retain) NSString *descripcion;
@property(nonatomic, assign) NSInteger pais;
@property(nonatomic, assign) NSInteger provincia;
@property(nonatomic, retain) NSString *calle;
@property(nonatomic, assign) NSInteger altura;
@property(nonatomic, assign) NSInteger tipoDomicilio;
@property(nonatomic, retain) NSString *localidad;
@property(nonatomic, retain) NSString *codigoPosta;
@property(nonatomic, assign) NSInteger recID;

+(NSArray*)GetAllByTipo:(NSInteger)tipo;

@end
