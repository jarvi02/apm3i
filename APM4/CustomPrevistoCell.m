//
//  PrevistoCustomCell.m
//  APM4
//
//  Created by Laura Busnahe on 2/26/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomPrevistoCell.h"

@implementation CustomPrevistoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end