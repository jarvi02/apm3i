//
//  MTMuestrasMedicas.h
//  APM4
//
//  Created by Laura Busnahe on 3/28/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTAjustes.h"

@interface MTMuestrasMedicas : NSObject

@property(nonatomic, assign) NSInteger  recID;
@property(nonatomic, strong) NSString   *descripcion;
@property(nonatomic, assign) NSInteger  stock;
@property(nonatomic, strong) NSString   *codigo;
@property(nonatomic, strong) NSString   *lote;
@property(nonatomic, strong) NSDate     *fecha_vto;
@property(nonatomic, assign) NSInteger  idNodo;
@property(nonatomic, strong) NSString   *idLote;
@property(nonatomic, assign) BOOL       vencido;
@property(nonatomic, assign) BOOL       blocked;

- (void)checkProducto;

- (NSString*)getDescription;
- (void)ajusteStock:(MTAjustes*)ajuste;


+(NSArray*)GetAll;

@end
