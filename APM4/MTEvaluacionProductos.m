//
//  MTEvaluacionProductos.m
//  APM4
//
//  Created by Laura Busnahe on 9/2/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTEvaluacionProductos.h"
#import "Config.h"
#import "DB.h"

@implementation MTEvaluacionProductos
-(id)init
{
    self = [super init];
    
    if (self)
    {
        self.idProducto  = 0;
        self.descripcion = @"";
        self.objetivo    = @"";
    }
    
    return self;
}

+ (NSArray*)getAllForMedico:(NSUInteger)idMedico
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql =
    @"SELECT DISTINCT "
        "mtc_productos.id as idProducto, "
        "mtc_productos.descripcion as descProducto, "
        "ifnull(objetivos, '') as objetivo "
    "FROM med_apromocionar "
    "INNER JOIN mtc_productos ON (mtc_productos.id = med_apromocionar.producto) "
    "WHERE (nodo = %@ AND medico = %d)  "
    "  "
    "UNION "
    " "
    "SELECT DISTINCT "
        "mtc_productos.id as idProducto, "
        "mtc_productos.descripcion as descProducto, "
        "'' as objetivo "
    "FROM med_promociones "
    "INNER JOIN mtc_productos ON (mtc_productos.id = med_promociones.producto) "
    "WHERE (length(med_promociones.evaluacion) > 0 AND nodo = %@ AND medico = %d AND "
           "NOT (producto IN (SELECT producto "
                             "FROM med_apromocionar "
                             "WHERE nodo = %@ AND medico = %d))) "
    " "
    "ORDER BY descProducto ";
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       idMedico,
                       nodo,
                       idMedico,
                       nodo,
                       idMedico];
    
#ifdef DEBUG_EVALUACIONPRODUCTOS
    
    NSLog(@"--- MTEvaluacionProductos -> getAllForMedico ---");
    NSLog(@" - query: %@", query);
    
#endif
    
    
    char *_c;
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTEvaluacionProductos *o = [[MTEvaluacionProductos alloc] init];
            
            // idProducto
            o.idProducto = sqlite3_column_int(statement, 0);
            
            // descProducto
            _c = (char *)sqlite3_column_text(statement, 1);
            o.descripcion = [NSString pCharToString:_c];
            
            // Objetivo
            _c = (char *)sqlite3_column_text(statement, 2);
            o.objetivo = [NSString pCharToString:_c];
            
            [result addObject:o];
        }
        
	} else
    {
        NSAssert(0, @"No se pudo ejecutar %@", query);
	}
	sqlite3_finalize(statement);
    
    return result;
}
@end
