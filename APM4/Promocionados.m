//
//  Promocionados.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 25/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Promocionados.h"
#import "Config.h"
#import "DB.h"

@implementation Promocionados

- (id)init
{
    self = [super init];
    if (self) {
        self.descripcion = @"";
        self.literatura = @"";
        self.obsequio = @"";
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [_literatura release];
    [_obsequio release];
    [super dealloc];
}

+(NSArray*)GetAllForDoctor:(NSInteger)idDoctor {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    NSString *sql = @"select prod.descripcion, prom.orden, prom.muestras, case when prom.literatura = 0 then 'No' else 'Si' end as literatura, case when prom.obsequio = 0 then 'No' else 'Si' end as obsequio, vis.fecha as fechaDisplay from med_promociones prom inner join mtc_productos prod on prod.id = prom.producto left outer join med_visitas vis on vis.nodo = prom.nodo and vis.visitador = prom.nodo and vis.medico = prom.medico and vis.id = prom.visita where prom.nodo = %@ and prom.medico = %d AND (prom.orden > 0) order by orden;";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql, nodo, idDoctor];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        Promocionados *o;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[Promocionados alloc] init];
            
            
            char *_d = (char *)sqlite3_column_text(statement, 0);
            if((_d != nil) && (strlen(_d) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_d];
			}
            
            o.orden = sqlite3_column_int(statement, 1);
            o.muestras = sqlite3_column_int(statement, 2);
            
            char *_an = (char *)sqlite3_column_text(statement, 3);
            if((_an != nil) && (strlen(_an) > 0)) {
                o.literatura = [NSString stringWithUTF8String:_an];
			}
            
            char *_obs = (char *)sqlite3_column_text(statement, 4);
            if((_obs != nil) && (strlen(_obs) > 0)) {
                o.obsequio = [NSString stringWithUTF8String:_obs];
			}
            
            
            o.fechaDisplay = sqlite3_column_int(statement, 5);
			
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla configuracion");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
}

                          
                          

@end
