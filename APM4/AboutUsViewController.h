//
//  AboutUsViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 08/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"


@interface AboutUsViewController : CustomViewController

@property (retain, nonatomic) IBOutlet UIImageView *image;
@end
