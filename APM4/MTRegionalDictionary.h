//
//  MTRegionalDictionary.h
//  APM4
//
//  Created by Laura Busnahe on 8/5/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTRegionalDictionary : NSObject



+(MTRegionalDictionary *)sharedInstance;

// Devuelve el texto traducido (si existe en el diccionario) o el mismo que fue pasado como parámetro.
- (NSString*)valueForText:(NSString*)texto;

-(BOOL)hideMatriculaNacional;
-(BOOL)hideMatriculaProvincial;
-(BOOL)hideCUIT;

@end
