//
//  CusromMedicosVisitadosPorFechaCell.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CusromMedicosVisitadosPorFechaCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *fecha;
@property (retain, nonatomic) IBOutlet UILabel *nombre;
@property (retain, nonatomic) IBOutlet UILabel *tipoVisita;
@property (retain, nonatomic) IBOutlet UILabel *actividad;
@end
