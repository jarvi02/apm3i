//
//  stdTotalesViewController.h
//  APM4
//
//  Created by Laura Busnahe on 7/16/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"

@interface stdTotalesViewController : CustomViewController <UITextFieldDelegate>
- (id)initViewDefaultNib;

#pragma mark -
#pragma mark Períodos trabajados
@property (retain, nonatomic) IBOutlet UILabel *txtPeriodoM;
@property (retain, nonatomic) IBOutlet UILabel *txtPeriodoT;
@property (retain, nonatomic) IBOutlet UILabel *txtPeriodoTotal;
@property (retain, nonatomic) IBOutlet UITextField *edtPeriodoM;
@property (retain, nonatomic) IBOutlet UITextField *edtPeriodoT;
@property (retain, nonatomic) IBOutlet UITextField *edtPeriodoTotal;
- (IBAction)edtPeriodoChanged:(id)sender;


#pragma mark -
#pragma mark Fijos
@property (retain, nonatomic) IBOutlet UILabel *txtFijosProgM;
@property (retain, nonatomic) IBOutlet UILabel *txtFijosProgT;
@property (retain, nonatomic) IBOutlet UILabel *txtFijosProgTotal;

@property (retain, nonatomic) IBOutlet UILabel *txtFijosVisitM;
@property (retain, nonatomic) IBOutlet UILabel *txtFijosVisitT;
@property (retain, nonatomic) IBOutlet UILabel *txtFijosVisitTotal;

@property (retain, nonatomic) IBOutlet UILabel *txtFijosPorcM;
@property (retain, nonatomic) IBOutlet UILabel *txtFijosPorcT;
@property (retain, nonatomic) IBOutlet UILabel *txtFijosPorcTotal;

@property (retain, nonatomic) IBOutlet UILabel *txtFijosMedM;
@property (retain, nonatomic) IBOutlet UILabel *txtFijosMedT;
@property (retain, nonatomic) IBOutlet UILabel *txtFijosMedTotal;


#pragma mark -
#pragma mark Revisitas
@property (retain, nonatomic) IBOutlet UILabel *txtRevisProgM;
@property (retain, nonatomic) IBOutlet UILabel *txtRevisProgT;
@property (retain, nonatomic) IBOutlet UILabel *txtRevisProgTotal;

@property (retain, nonatomic) IBOutlet UILabel *txtRevisVisitM;
@property (retain, nonatomic) IBOutlet UILabel *txtRevisVisitT;
@property (retain, nonatomic) IBOutlet UILabel *txtRevisVisitTotal;

@property (retain, nonatomic) IBOutlet UILabel *txtRevisPorcM;
@property (retain, nonatomic) IBOutlet UILabel *txtRevisPorcT;
@property (retain, nonatomic) IBOutlet UILabel *txtRevisPorcTotal;

@property (retain, nonatomic) IBOutlet UILabel *txtRevisMedM;
@property (retain, nonatomic) IBOutlet UILabel *txtRevisMedT;
@property (retain, nonatomic) IBOutlet UILabel *txtRevisMedTotal;


#pragma mark -
#pragma mark Fluctuantes
@property (retain, nonatomic) IBOutlet UILabel *txtFlucVisitM;
@property (retain, nonatomic) IBOutlet UILabel *txtFlucVisitT;
@property (retain, nonatomic) IBOutlet UILabel *txtFlucVisitTotal;

@property (retain, nonatomic) IBOutlet UILabel *txtFlucRevisM;
@property (retain, nonatomic) IBOutlet UILabel *txtFlucRevisT;
@property (retain, nonatomic) IBOutlet UILabel *txtFlucRevisTotal;


#pragma mark -
#pragma mark Total
@property (retain, nonatomic) IBOutlet UILabel *txtTotalVisitM;
@property (retain, nonatomic) IBOutlet UILabel *txtTotalVisitT;
@property (retain, nonatomic) IBOutlet UILabel *txtTotalVisitTotal;

@property (retain, nonatomic) IBOutlet UILabel *txtTotalMedM;
@property (retain, nonatomic) IBOutlet UILabel *txtTotalMedT;
@property (retain, nonatomic) IBOutlet UILabel *txtTotalMedTotal;


@end
