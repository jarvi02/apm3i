//
//  ConfigGlobal.h
//  APM4
//
//  Created by Juan Pablo Garcia on 20/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>


//#define SettingForKey(key, stringdefault) ([[Config shareInstance] isValidKey:key] ? [[Config shareInstance] find:key] : stringdefault)
#define VISITA_PROMOCION_SIN_DESCARGA ([[ConfigGlobal shareInstance] getVISITA_PROMOCION_SIN_DESCARGA])



#define HISTORICOVISITA                     @"HISTORICOVISITA"
#define ORDENPROMOCION                      @"ORDENPROMOCION"
#define VISITADPM                           @"VISITADPM"
#define USAAGENDAMENSUAL                    @"USAAGENDAMENSUAL"
#define INTENTOS                            @"INTENTOS"
#define USAAUTORIZACION                     @"USAAUTORIZACION"
#define USADUPLICACIONMED                   @"USADUPLICACIONMED"
#define USAMODMEDICOS                       @"USAMODMEDICOS"
#define USAMEDINACTIVO                      @"USAMEDINACTIVO"
#define CRT_EGRESO                          @"CRT_EGRESO"
#define CRT_OBRASOCIAL                      @"CRT_OBRASOCIAL"
#define USAHORASALIDAM                      @"USAHORASALIDAM"
#define USAPARTEUNIFICADO                   @"USAPARTEUNIFICADO"
#define USAPRESCRIPCIONESNUEVAS             @"USAPRESCRIPCIONESNUEVAS"
#define SIN_PROM_OBLIG_PROD                 @"SIN_PROM_OBLIG_PROD"
#define CARGAVISITASCONFECHAANTERIOR        @"CARGAVISITASCONFECHAANTERIOR"
#define USA_REVERSION_BAJA                  @"USA_REVERSION_BAJA"
#define VISITARAPIDA                        @"VISITARAPIDA"
#define OCULTAFLUCTUANTE                    @"OCULTAFLUCTUANTE"
#define USALOGACTIVIDAD                     @"USALOGACTIVIDAD"
#define USACOBAJUSTADA                      @"USACOBAJUSTADA"
#define USAESTPORAPM                        @"USAESTPORAPM"
#define USALOTES                            @"USALOTES"
#define VIS_FINDESEMANA                     @"VIS_FINDESEMANA"
#define VIS_REVISITA                        @"VIS_REVISITA"
#define PORCENTAJESENPARTEDIARIO            @"PORCENTAJESENPARTEDIARIO"
#define VERIFICAPARTEAYER                   @"VERIFICAPARTEAYER"
#define USAHORASALIDAT                      @"USAHORASALIDAT"
#define PARTE_OBLIGATORIO                   @"PARTE_OBLIGATORIO"
#define PARTE_SIN_MEDICOS                   @"PARTE_SIN_MEDICOS"
#define CARGAMUESTRASENTREGADAS             @"CARGAMUESTRASENTREGADAS"
#define TRZ_CANT_MAXIMA                     @"TRZ_CANT_MAXIMA"
#define USAAUTORIZACION                     @"USAAUTORIZACION"
#define FRECUENCIAVISITAS                   @"FRECUENCIAVISITAS"
#define PROMOCIONSINDESCARGA                @"PROMOCIONSINDESCARGA"

@interface ConfigGlobal : NSObject{
    
@private
    NSMutableDictionary *dictionary;
}

+(ConfigGlobal*)shareInstance;
-(void) reloadData;
-(BOOL)isValidKey:(NSString*)key;
-(BOOL)find:(NSString*)aKey;
-(NSString *)findValue:(NSString*)aKey;
-(void)updateDB:(NSString*)aKey value:(BOOL)aValue;
- (BOOL)getVISITA_PROMOCION_SIN_DESCARGA;
@end
