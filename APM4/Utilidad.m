//
//  Utilidad.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 06/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Utilidad.h"
#import "DB.h"

@implementation Utilidad


- (id)init
{
    self = [super init];
    if (self) {
        self.descripcion = @"";
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [super dealloc];
    
}

- (NSString*)getDescription
{
    return [NSString stringWithFormat:@"%@", self.descripcion];
}

+(NSArray*) GetAll{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"select id, descripcion from mtc_utilidadesdomicilio order by id;";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
        Utilidad *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[Utilidad alloc] init];
            
            o.idUtilidad = sqlite3_column_int(statement, 0);
            
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
}


-(id)copy{
    
    Utilidad *o = [[Utilidad alloc] init];
    o.idUtilidad = self.idUtilidad;
    o.descripcion = [NSString stringWithFormat:@"%@", self.descripcion];
    
    return [o autorelease];
}

@end
