//
//  VisitasOtrosRepViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 08/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "VisitasOtrosRepViewController.h"
#import "FichaVisitaOtrosRep.h"
#import "NSDate+extensions.h"

@interface VisitasOtrosRepViewController ()

@end


@implementation VisitasOtrosRepViewController


- (void)dealloc
{
    [_data release];
    
    [_tmpCell release];
	[_cellNib release];
    
    [super dealloc];
}

- (id)initWithData:(NSArray*)aData
{
    self = [super init];
    if (self) {
        self.cellNib    = [UINib nibWithNibName:@"FichaVisitaOtrosRepCell" bundle:nil];
        self.data       = aData;
    }
    return self;
}

- (void)updateData:(NSArray*)aData
{
    self.data = aData;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.data count] == 0) {
        return 1;
    }
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"FichaVisitaOtrosCelldentifier";
	
    // Obtain the cell object.
	FichaVisitaOtrosRepCell *cell = (FichaVisitaOtrosRepCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	// If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
		cell = self.tmpCell;
		self.tmpCell = nil;
        
    }
    
    if ([self.data count] == 0) {
        
        // si es 0 meto uno a la fuerza
        cell.lbFecha.text = @"Sin Visita";
        return cell;
    }
    
    FichaVisitaOtrosRep *o = [self.data objectAtIndex:indexPath.row];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[o.fecha intValue]];
    cell.lbFecha.text       = [date formattedStringUsingFormat:@"dd/MM/yyyy"];
    cell.lbVisita.text      = o.visitador;
    
	return cell;

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UILabel *label = [[[UILabel alloc] init] autorelease];
    label.text = @"   Visitas otros Representante";
    label.font = [UIFont systemFontOfSize:14.0f];
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.backgroundColor = UIColorFromRGB(defaultHeaderCellColor);
    
    return label;

}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
