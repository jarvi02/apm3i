//
//  FichaMenuViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 07/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "FichaMenuViewController.h"
#import "PrescripcionesViewController.h"
#import "FichaVisitasViewController.h"
#import "FichaProductosViewController.h"
#import "ObjetivosViewController.h"
#import "EvaluacionPromocionesViewController.h"
#import "AppDelegate.h"

#import "GlobalDefines.h"

@interface FichaMenuViewController ()
-(void)navigateToPrescripciones;
-(void)navigateToFichaVisitadas;
-(void)navigateToFichaProductos;
-(void)alert:(NSString*)msg;
@end

@implementation FichaMenuViewController

-(NSArray *)options
{
    return [NSArray arrayWithObjects:
            @"Prescripciones",
            @"Ficha de visita",
            @"Ficha de productos",
            @"Objetivos",
            @"Evaluación Promociones",
            nil];
}

-(NSString *)title
{
    return @"Ficha";
}

-(CGSize)size
{
    return CGSizeMake(300.0f, 220.0f);
}

-(void)selectedOptionsAtIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
            // Prescripciones
            
            if (self.carteraViewControllerReference.itemCarteraMedicoSelected != nil) {
                [self navigateToPrescripciones];
            } else {
                [self alert:@"Debe seleccionar un médico antes de ir a ver sus prescripciones"];
            }
            break;
        case 1:
            // Ficha de Visita
            if (self.carteraViewControllerReference.itemCarteraMedicoSelected != nil) {
                [self navigateToFichaVisitadas];
            } else {
                [self alert:@"Debe seleccionar un médico antes de ir a ver su ficha de visitas"];
            }
            break;
        case 2:
            // Ficha de Productos
            if (self.carteraViewControllerReference.itemCarteraMedicoSelected != nil) {
                [self navigateToFichaProductos];
            } else {
                [self alert:@"Debe seleccionar un médico antes de ir a ver su ficha de productos"];
            }
            break;
        case 3:
            // Objetivos
            if (self.carteraViewControllerReference.itemCarteraMedicoSelected != nil) {
                [self navigateToObjetivos];
            } else {
                [self alert:@"Debe seleccionar un médico para poder ver sus objetivos"];
            }
            break;
        case 4:
            // Evaluación Promociones
            [self navigateToEvaluacionProductos];
            break;
            
        default:
            [self showNotImplemented];
    }
}

-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index {
    // Medico seleccionado
    CarteraMedico *c = nil;
    if (self.carteraViewControllerReference.itemCarteraMedicoSelected != nil)
    {
        c = self.carteraViewControllerReference.itemCarteraMedicoSelected;
    }
    
    switch (index)
    {
        case 0:
            // Prescripciones
#ifdef INCLUDE_PRESCRICIONES
            return [self estaSeleccionadoNoFluctuante:c];
#else
            return NO;
#endif
            break;
        case 1:
            // Ficha de Visita
            return (c != nil ? TRUE : FALSE);
            break;
        case 2:
            // Ficha de Productos
            return (c != nil ? TRUE : FALSE);;
            break;
        case 3:
            // Objetivos
            return ([self estaSeleccionadoNoFluctuante:c]);
            break;
        case 4:
            // Evaluación Promociones
            return ([self estaSeleccionadoNoFluctuante:c]);
            break;
        default:
            return NO;
    }
}

#pragma mark -
#pragma Private Members

- (BOOL)estaSeleccionadoNoFluctuante:(CarteraMedico*)c
{
    if ((c) && (![c esFluctuante]))
        return YES;
    else
        return NO;
}

- (BOOL)estaSeleccionadoFluctuante:(CarteraMedico*)c
{
    if ((c) && ([c esFluctuante]))
        return YES;
    else
        return NO;
}


-(void)navigateToPrescripciones{
    
    PrescripcionesViewController *o = [[[PrescripcionesViewController alloc] initWithNibName:@"PrescripcionesViewController" bundle:nil] autorelease];
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    [self navigateToController:o];
}
-(void)navigateToFichaVisitadas{
    
    FichaVisitasViewController *o = [[[FichaVisitasViewController alloc] initWithNibName:@"FichaVisitasViewController" bundle:nil] autorelease];
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    [self navigateToController:o];
    
}
-(void)navigateToFichaProductos{
    
    FichaProductosViewController *o = [[[FichaProductosViewController alloc] initWithNibName:@"FichaProductosViewController" bundle:nil] autorelease];
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    [self navigateToController:o];
    
}

-(void)navigateToObjetivos{
    
    ObjetivosViewController *o = [[[ObjetivosViewController alloc] initWithDefaultNib] autorelease];
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    [self navigateToController:o];
}

-(void)navigateToEvaluacionProductos
{
    EvaluacionPromocionesViewController *o = [[[EvaluacionPromocionesViewController alloc] initWithDefaultNib] autorelease];
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    [self navigateToController:o];
}



-(void)alert:(NSString*)msg{
    
    [Delegate dismissPopOver:YES];
    
    UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Alerta" message:msg delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
    [a show];
    [a release];
}

@end
