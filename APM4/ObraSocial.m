//
//  ObraSocial.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 04/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ObraSocial.h"
#import "Novedad.h"
#import "Config.h"
#import "DBExtended.h"
#import "NSStringExtended.h"

@implementation ObraSocial


- (id)init
{
    self = [super init];
    if (self) {
        self.descripcion = @"";
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [super dealloc];
}

- (NSString*)getDescription
{
    return [NSString stringWithFormat:@"%@", self.descripcion];
}

+(NSArray*) GetAll{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"SELECT id, descripcion "
                     "FROM mtc_obrassociales "
                     "ORDER BY descripcion;";
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
	if ( statement ) {
        
        ObraSocial *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[ObraSocial alloc] init];
            
            // id
            o.idObraSocial = sqlite3_column_int(statement, 0);
            
            // Descripción
            _c = (char *)sqlite3_column_text(statement, 1);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
}

+(void)deleteAllForMedico:(NSInteger)idMedico
{
    NSString *sqlMedicos = @"select nodo || '|' || medico || '|' || obrasocial as registro " 
    "from med_obrassociales where nodo = %@ and medico = %@";
    
    NSString *queryMedico = [NSString stringWithFormat:sqlMedicos,
                             SettingForKey(NODO, @"0"),
                             [NSString intToStr:idMedico]];
    
    // Genero un array con los strings para luego agregar el registro en la log_novedades.
    sqlite3_stmt *statement = [[DB getInstance] prepare:queryMedico];
    NSMutableArray *arrayNovedad = [[NSMutableArray alloc] init];
    [arrayNovedad removeAllObjects];
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            NSString *o = @"";
            
            // Param. log_novedades
            char *_descripcion = (char *)sqlite3_column_text(statement, 0);
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o = [NSString stringWithUTF8String:_descripcion];
            
            [arrayNovedad addObject:[NSString stringWithFormat:@"%@",o]];
        }
        
	} else
    {
        NSAssert(0, @"ERROR: No se pudo ejecutar: %@", queryMedico);
	}
	sqlite3_finalize(statement);
    
    // Chequeo que haya algún dato para borrar.
    if ([arrayNovedad count] == 0)
        return;
    
    // Borro los registros de la base de datos
    sqlMedicos = @"DELETE FROM med_obrassociales WHERE (nodo=%@) AND (medico=%@)";
    queryMedico = [NSString stringWithFormat:sqlMedicos,
                   SettingForKey(NODO, @"0"),
                   [NSString intToStr:idMedico]];
#ifdef DEBUG_ABMMEDICO
    NSLog(@"- query D med_obrassociales: %@", queryMedico);
#endif
    [[DB getInstance] excecuteSQL:queryMedico];
    
    
    // LOG_NOVEDADES
    for (NSString *str in arrayNovedad)
    {
#ifdef DEBUG_ABMMEDICO
        NSLog(@"- Novedad D med_obrassociales: %@", str);
#endif
        [Novedad insert:@"med_obrassociales" novedad:str tipo:kDelete];
    }
}

-(id)copy
{
   
    ObraSocial *o = [[ObraSocial alloc] init];
    o.idObraSocial = self.idObraSocial;
    o.descripcion = [NSString stringWithFormat:@"%@", self.descripcion];
    
    return [o autorelease];
}

@end
