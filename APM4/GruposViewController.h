//
//  GruposViewController.h
//  APM4
//
//  Created by Laura Busnahe on 6/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GruposViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UITextField      *txtNombreGrupo;
@property (retain, nonatomic) IBOutlet UIButton         *btnAdd;
@property (retain, nonatomic) IBOutlet UIButton         *btnOk;
@property (retain, nonatomic) IBOutlet UITableView      *tblGrupos;
@property (retain, nonatomic) IBOutlet UIBarButtonItem  *btnEditar;
@property (retain, nonatomic) IBOutlet UIBarButtonItem  *btnAceptar;

@property (nonatomic, strong) NSMutableArray    *data;
@property (nonatomic, strong) NSMutableArray    *oldData;

- (IBAction)tapBtnAdd:(id)sender;
- (IBAction)tapBtnEditar:(id)sender;
- (IBAction)tapBtnAceptar:(id)sender;

- (id)initWithDefaultNib;

@end
