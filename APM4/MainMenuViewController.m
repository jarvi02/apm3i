//
//  MainMenuViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MainMenuViewController.h"
#import "FileMenuViewController.h"
#import "StatisticsMenuViewController.h"
#import "ToolsMenuViewController.h"
#import "Config.h"
@interface MainMenuViewController ()

-(void)pressedGoToFile;
-(void)pressedGoToStatistics;
-(void)pressedGoToTools;

@end

@implementation MainMenuViewController

-(NSArray *)options
{
    return [NSArray arrayWithObjects:
            @"Archivo",
            @"Estadísticas",
            @"Herramientas",
            nil];
}




-(NSString *)title
{
    return @"Menú";
}

-(BOOL)hasSubMenus
{
    return YES;
}

-(BOOL)hasIcons
{
    return YES;
}


-(void)selectedOptionsAtIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
            [self pressedGoToFile];
            break;
            
        case 1:
            [self pressedGoToStatistics];
            break;
            
        case 2:
            [self pressedGoToTools];
            break;
            
        default:
            break;
    }
}

-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index {
    
    switch (index) {
        case 0:
            return YES;
            break;
            
        case 1:
            return YES;
            break;
            
        case 2:
            return YES;
            break;
            
        default:
            return NO;
    }
}

-(NSString *)imageNameForIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return @"cartera_48.png";
            
        case 1:
            return @"estadistica.png";
            
        case 2:
            return @"config.png";
            
        default:
            break;
    }

    return @"";
}

#pragma mark -
#pragma mark Public Methods

-(void)pressedGoToFile
{
    FileMenuViewController * mvc = [[[FileMenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil] autorelease];
   
   
    [self navigateToSubMenu:mvc];
}

-(void)pressedGoToStatistics
{
    StatisticsMenuViewController * mvc = [[[StatisticsMenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil] autorelease];
   
    [self navigateToSubMenu:mvc];
}

-(void)pressedGoToTools
{
    ToolsMenuViewController * mvc = [[[ToolsMenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil] autorelease];
   
    [self navigateToSubMenu:mvc];
}

@end

