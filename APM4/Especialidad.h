//
//  Especialidad.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Especialidad : NSObject

@property(nonatomic) NSUInteger      recID;
@property(nonatomic,retain) NSString *descripcion;
@property(nonatomic,retain) NSString *sigla;
@property(nonatomic) NSInteger orden;

- (NSString*)getDescription;

+(NSArray*) GetAll;
+(void)deleteAllForMedico:(NSInteger)idMedico;

@end
