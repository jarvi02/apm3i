//
//  MTstdCoberturaLugarVisita.m
//  APM4
//
//  Created by Laura Busnahe on 7/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTstdCoberturaLugarVisita.h"
#import "Config.h"
#import "DB.h"

@implementation MTstdCoberturaLugarVisita

+ (NSArray*)getAllWhere:(NSString*)where
{
    NSArray *result = nil;
    
    NSString *sql =
    @"SELECT 0 as _id, lugar, "
    "SUM(case when asignacion = 1 then  1 else 0 end) as programados, "
    "SUM(case when asignacion != 1 then  1 else 0 end) as fluctuantes, "
    "SUM(case when asignacion = 1 and visitasrealizadas > 0 then 1 else 0 "
    "          end ) as vistos, "
    "SUM(case when asignacion = 1 and visitasrealizadas > 0 then visitasrealizadas -1 else 0 "
    "         end ) as revisitas, "
    " ROUND(CAST ( "
    " CAST (SUM(case when asignacion = 1 and visitasrealizadas > 0 then 1 else 0 "
    "          end)  as float) * 100 "
    " / "
    "CAST( SUM(case when asignacion = 1 then  1 else 0 end) as float ) "
    " as float ),2) "
    "as Porcentaje, "
    "SUM(visitasrealizadas) as total "
    "FROM med_medicos WHERE B = 0 AND "
    "nodo =  %@ %@ "
    "GROUP BY lugar "
    
    "union all "
    
    "SELECT 1 as _id, 'Totales', "
    "SUM(case when asignacion = 1 then  1 else 0 end) as programados, "
    "SUM(case when asignacion != 1 then  1 else 0 end) as fluctuantes, "
    "SUM(case when asignacion = 1 and visitasrealizadas > 0 then 1 else 0 "
    "    end ) as vistos,  "
    "SUM(case when asignacion = 1 and visitasrealizadas > 0 then visitasrealizadas -1 else 0 "
    "    end ) as revisitas, "
    "ROUND(CAST ( "
    "  		  CAST (SUM(case when asignacion = 1 and visitasrealizadas > 0 then 1 else 0 "
    "			           end)  as float) * 100 "
    "			  / "
    "			 CAST( SUM(case when asignacion = 1 then  1 else 0 end) as float ) "
    "			  as float ),2) "
    "			 as Porcentaje, "
    "SUM(visitasrealizadas) as total "
    "FROM med_medicos WHERE B = 0 AND "
    "nodo = %@ %@ "
    "ORDER BY _id, lugar ";
    
    NSString *nodo  = SettingForKey(NODO, @"0");
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       [NSString string:where ifNull:@""],
                       nodo,
                       [NSString string:where ifNull:@""]];
    
#ifdef DEBUG_COBERTURA
    NSLog(@"--- Cobertura Lugar Visita ---");
    NSLog(@" - query: %@", query);
    
#endif
    
    result = [self getAll:query];
    
    return result;
}

@end
