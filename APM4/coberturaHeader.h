//
//  coberturaHeader.h
//  APM4
//
//  Created by Laura Busnahe on 7/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
@class coberturaHeader;


typedef enum
{
    kMTFieldDescripcion,
    kMTFieldProgramados,
    kMTFieldVisitados,
    kMTFieldPorcentaje,
    kMTFieldRevisitas,
    kMTFieldFluctuantes,
    kMTFieldTotal
} MTCoberturaHeaderField;

typedef enum
{
    kMTOrderAsc,
    kMTOrderDesc
} MTHeaderOrder;

@protocol MTCoberturaHeaderDelegate <NSObject>
@required
// Sent when the user tap a field.
- (void)customCoberturaHeader:(coberturaHeader *)header
                selectedField:(MTCoberturaHeaderField)field
                        Order:(MTHeaderOrder)order;
@end


@interface coberturaHeader : UIView

@property (nonatomic, assign) id <MTCoberturaHeaderDelegate>        delegate;


@property (retain, nonatomic) IBOutlet UIButton *lblDescripcion;
@property (retain, nonatomic) IBOutlet UIButton *lblProgramados;
@property (retain, nonatomic) IBOutlet UIButton *lblVisitados;
@property (retain, nonatomic) IBOutlet UIButton *lblPorcentaje;
@property (retain, nonatomic) IBOutlet UIButton *lblRevisitas;
@property (retain, nonatomic) IBOutlet UIButton *lblFluctuantes;
@property (retain, nonatomic) IBOutlet UIButton *lblTotal;

- (void)orderBy:(MTCoberturaHeaderField)field sender:(id)sender reset:(BOOL)reset;

- (IBAction)tapLabel:(id)sender;

@end
