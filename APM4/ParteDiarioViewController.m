//
//  ParteDiarioViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 19/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ParteDiarioViewController.h"
#import "DB.h"
#import "Config.h"
#import "ConfigGlobal.h"
#import "ParteDiario.h"
#import "ParteDiarioData.h"
#import "NSDate+extensions.h"
#import "UIView+extensions.h"
#import "AppDelegate.h"
#import "MTFeriado.h"
#import "Ciclo.h"
#import "Novedad.h"


// TODO: Agregar rutinas para parte diario con porcentajes.

@interface ParteDiarioViewController ()

@property (nonatomic, assign) BOOL yaTransmitido;

@end

@implementation ParteDiarioViewController
#pragma mark -
#pragma mark LyfeCicle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"Partes Diarios";
        self.viewControllerMode = ParteDiarioSimple;
        
        self.dateDesde = nil;
        self.dateHasta = nil;
        
        self.txtPorcManiana.text = @"0 %";
        self.txtPorcTarde.text   = @"0 %";
        
        self.arrayComboManiana  = [[NSMutableArray alloc] init];
        self.arrayComboTarde    = [[NSMutableArray alloc] init];
        
        self.yaTransmitido = NO;
    }
    return self;
}

-(void)viewDidLoad
{
    // Configuracion
    visitaFinDeSemana = [[ConfigGlobal shareInstance] find:VIS_FINDESEMANA];
    parteObligatorio  = [[ConfigGlobal shareInstance] find:PARTE_OBLIGATORIO];
    usaPorcentaje     = [[ConfigGlobal shareInstance] find:PORCENTAJESENPARTEDIARIO];
    
    
    if (FORCE_PORCENTAJESENPARTEDIARIO)
        usaPorcentaje = TRUE;
    
    // Ciclo Actual y Proximo
    self.cicloActual = [Ciclo getById:[[[Config shareInstance] find:CICLO] intValue]];
    self.cicloProximo = [Ciclo getById:self.cicloActual.proximo];
    
    // Chequeo en qué modo se abrió la vista.
    if (self.viewControllerMode == ParteDiarioSimple)
    {
        // Parte Diario.
        [self setSimpleMode];
    } else
    {
        // Parte Diario Desde-Hasta.
        [self setDesdeHastaMode];
    }
    
    self.inserted = [NSMutableArray array];
    self.updated = [NSMutableArray array];
    self.deleted = [NSMutableArray array];
    
    huboModificaciones = NO;
    
    [super viewDidLoad];
    
    // Oculto el frame negro y el popup para modificar cantidades.
    [self.popup setHidden:YES];             // Popup Cantidades
    [self.popupPorcetaje setHidden:YES];    // Popup Porcentajes
    [self.frameView setHidden:YES];
}

#pragma mark -
#pragma mark Private 

-(void) setSimpleMode
{
    [self refresh:[NSDate dateWithoutTime]];
    
    // Muestro los objetos correspondientes a la vista simple.
    [self.lblFecha          setHidden:NO];
    [self.dateTextField     setHidden:NO];
    [self.selectDateButton  setHidden:NO];
    
    // Oculto los objetos correspondientes a la vista Desde-Hasta.
    // Fecha desde
    [self.lblFechaDesde setHidden:YES];
    [self.txtFechaDesde setHidden:YES];
    [self.btnFechaDesde setHidden:YES];
    // Fecha Hasta
    [self.lblFechaHasta setHidden:YES];
    [self.txtFechaHasta setHidden:YES];
    [self.btnFechaHasta setHidden:YES];
    
    // Selecciono la fecha actual
    
}

-(void) setDesdeHastaMode
{
    //self.dateDesde = [NSDate dateWithoutTime];
    //self.dateHasta = [NSDate dateWithoutTime];
    
    [self refreshDesde:[NSDate dateWithoutTime]];
    [self refreshHasta:[NSDate dateWithoutTime]];
    
    // Muestro los objetos correspondientes a la vista Desde-Hasta.
    // Fecha desde
    [self.lblFechaDesde setHidden:NO];
    [self.txtFechaDesde setHidden:NO];
    [self.btnFechaDesde setHidden:NO];
    // Fecha Hasta
    [self.lblFechaHasta setHidden:NO];
    [self.txtFechaHasta setHidden:NO];
    [self.btnFechaHasta setHidden:NO];
    
    // Oculto los objetos correspondientes a la vista simple.
    [self.lblFecha          setHidden:YES];
    [self.dateTextField     setHidden:YES];
    [self.selectDateButton  setHidden:YES];
    
    // Cargo un parte diario en blanco. Para esto, utilizo distantFuture asegurádome que el parte que busque
    // en la abse de datos está vacio.
    self.partes = [ParteDiario getAll:[NSDate distantFuture]];
    
    // Si utiliza porcentajes, elimino del listado, la tarea "MEDICOS"
    if (usaPorcentaje)
    {
        for (ParteDiario *p in self.partes)
        {
            if (p.identificador == 9999)
            {
                NSMutableArray *newPartes = [[NSMutableArray alloc] initWithArray:self.partes];
                [newPartes removeObject:p];
                
                //if (self.partes != nil)
                //    [self.partes dealloc];
                self.partes = [[NSArray alloc] initWithArray:newPartes];
                //[self.partes initWithArray:newPartes];
            }
        }
    }
    
    [self calcularTotales];
    
    [self.tableView reloadData];
}



-(BOOL)fueraDeCiclo:(NSDate*)date
{
//    if ([self.currentDate compare:self.cicloActual.desde] == NSOrderedAscending)
//        return YES;
//    
//    if ([self.cicloProximo.hasta compare:self.currentDate] == NSOrderedAscending)
//        return  YES;
    
    if ([date compare:self.cicloActual.desde] == NSOrderedAscending)
        return YES;
    
    if ([self.cicloProximo.hasta compare:date] == NSOrderedAscending)
        return  YES;
    
    return NO;
}

- (BOOL)esFechaValida:(NSDate*)date sender:(id)sender
{
    NSDate* hoy = [[NSDate date] dateAsDateWithoutTime];
    date = [date dateAsDateWithoutTime];
    
    // Chequeo que la fecha no sea a futuro
    if ([date timeIntervalSince1970] > [hoy timeIntervalSince1970])
    {
        UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"La fecha seleccionada no puede ser mayor a la fecha actual." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        
        [av show];
        return NO;
    }
    
    // Fecha modo simple
    if (sender == self.selectDateButton)
    {
        
    } else
    
    // Fecha Desde
    if (sender == self.btnFechaDesde)
    {
        NSDate* hasta = [self.dateHasta dateAsDateWithoutTime];
        
        // Chequeo que la fecha Hasta no sea menor a la fecha Desde.
        if ([date timeIntervalSince1970] > [hasta timeIntervalSince1970])
        {
            UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"La fecha seleccionada no puede ser mayor a la fecha hasta." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
            
            [av show];
            return NO;
        }
    } else
    
    // Fecha Hasta
    if (sender == self.btnFechaHasta)
    {
        NSDate* desde = [self.dateDesde dateAsDateWithoutTime];
        
        // Chequeo que la fecha Hasta no sea menor a la fecha Desde.
        if ([date timeIntervalSince1970] < [desde timeIntervalSince1970])
        {
            UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"La fecha seleccionada no puede ser menor a la fecha desde." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
            
            [av show];
            return NO;
        }
    }
    
    // Chequeo si la fecha seleccionada está fuera de ciclo.
    if ([self fueraDeCiclo:date])
    {
        UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"La fecha seleccionada se encuentra fuera del ciclo actual y próximo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        
        [av show];
        
        if (sender != self.selectDateButton)
            return NO;
            
        // Devuelve YES porque puede ver los detalles del ciclo, pero no modificarlos en el caso del Parte diario Simple.
        return YES;
    }
    
    return YES;
}

-(NSArray *)listadoDiasDesde:(NSDate *)from hasta:(NSDate *)to
{
    NSMutableArray *result = [[NSMutableArray alloc] init];

    // Obtengo todo el listado de feriados.
    NSArray *feriadosList = [MTFeriado getAll];
    
    while ([from compare:to] != NSOrderedDescending)
    {
        BOOL visitasFeriadoFinde = TRUE;
        
        // Busca la configuración para saber si el APM hace visitas los fines de semana.
        if ([[ConfigGlobal shareInstance] isValidKey:VIS_FINDESEMANA])
            visitasFeriadoFinde = [[ConfigGlobal shareInstance] find:VIS_FINDESEMANA];
        
        BOOL esSabadoDomingo = NO;
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"e"];
        
        NSString* strWeekDay = [from formattedStringUsingFormat:@"e"];
        
        if (([strWeekDay isEqual:@"1"]) ||         // Domingo
            ([strWeekDay isEqual:@"7"]))           // Sábado
            esSabadoDomingo = YES;
        
        // Chequeo que el día no sea feriado ni fin de semana y agrego la fecha al listado.
        if ((![MTFeriado isDate:from intoArray:feriadosList]) &&
            (!esSabadoDomingo))
        {
            [result addObject:[from copy]];
        } else
        // Chequeo si el APM visita los fines de semana y feriados, si es así también agrego el día al listado.
        if (visitasFeriadoFinde)
            [result addObject:[from copy]];
        
        from = [from addDays:1];
    }
    
    return result;
}

-(void)calcularTotales
{
    self.total = 0;
    self.totalTarde = 0;
    self.totalManiana = 0;
    
    for(ParteDiario *p in self.partes)
    {
        self.totalTarde += [p cantidadTarde];
        self.totalManiana += [p cantidadManiana];
        self.total = self.totalTarde + self.totalManiana;
        if (usaPorcentaje)
            self.total = self.total / 2;
    }
}

-(void)refresh:(NSDate *)date
{
    self.currentDate = [date dateAsDateWithoutTime];
    
    self.partes = [ParteDiario getAll:self.currentDate];
    
    // Si utiliza porcentajes, elimino del listado, la tarea "MEDICOS"
    if (usaPorcentaje)
    {
        for (ParteDiario *p in self.partes)
        {
          if (p.identificador == 9999)
          {
              NSMutableArray *newPartes = [[NSMutableArray alloc] initWithArray:self.partes];
              [newPartes removeObject:p];
              
              //if (self.partes != nil)
              //    [self.partes dealloc];
              self.partes = [[NSArray alloc] initWithArray:newPartes];
              //[self.partes initWithArray:newPartes];
          }
        }
    }
    
    [self calcularTotales];
    
    // Muestro la fecha
    self.dateTextField.text = [self.currentDate formattedStringUsingFormat:@"dd/MM/yyyy"];
    
    // Refresco la tabla
    [self.tableView reloadData];
    
    // Verificamos si el parte diario tiene tareas cargadas
    tieneTareasCargadas = [ParteDiario tieneTareasCargadas:self.currentDate];

    // Verifico si el parte diario ya fue transmitido
    self.yaTransmitido = [self fueTransmitido];
    self.lblYaTransmitido.hidden =  !(self.yaTransmitido);
    
}

-(void)refreshDesde:(NSDate *)date
{
    self.dateDesde = [date dateAsDateWithoutTime];
    
    //self.partes = [ParteDiario getAllFrom:self.dateDesde to:self.dateHasta];
    
    // Muestro la fecha
    self.txtFechaDesde.text = [self.dateDesde formattedStringUsingFormat:@"dd/MM/yyyy"];
}

-(void)refreshHasta:(NSDate *)date
{
    self.dateHasta = [date dateAsDateWithoutTime];
    
    //self.partes = [ParteDiario getAllFrom:self.dateDesde to:self.dateHasta];
    
    // Muestro la fecha
    self.txtFechaHasta.text = [self.dateHasta formattedStringUsingFormat:@"dd/MM/yyyy"];
}

- (BOOL)saveParteDiarioSimple:(NSDate*)date
{
    // Este método devuelve YES si se pudo guardar el parte diario para la fecha ingresada.
    
    // Creacion de la cabecera del parte diario
    if (![ParteDiario existsCabecera:date])
    {
        // La creo
        [ParteDiario insertCabecera:date];
        
        // Novedad
        [Novedad insert:NODO_PARTEDIARIO
                novedad:[NSString stringWithFormat:@"%@|%d|%@",
                         [date.dateAsDateWithoutTime formattedStringUsingFormat:@"yyyyMMdd"],
                         [[[Config shareInstance] find:NODO] intValue],
                         [date formattedStringUsingFormat:@"MM/dd/yyyy '00:00'"]]
                   tipo:kInsert];
    }
    
    // Registro de todas las tareas
    for (ParteDiario *p in self.partes)
    {
        // Cuando utilizo parte diario desde hasta, no están definidas todas las propiedades del parte por lo
        // que tengo que cargarlas nuevamente.
        p.fecha = date;
        
        // Chequeo si existen datos de la actividad para la mañana.
        if (p.maniana)
        {
            [self completarParteData:p.maniana parte:p];
            [ParteDiario save:p.maniana];
        }
        
        // Chequeo si existen datos de la actidad para la tarde.
        if (p.tarde)
        {
            [self completarParteData:p.tarde parte:p];
            [ParteDiario save:p.tarde];
        }
    }
    
    // Limpieza de cabeceras sin tareas
    if (![ParteDiario tieneTareasCargadas:date])
    {
        // Lo elimino
        [ParteDiario remove:date];
        
        // Novedad
        [Novedad insert:NODO_PARTEDIARIO
                novedad:[NSString stringWithFormat:@"%@|%d|%@",
                         [date.dateAsDateWithoutTime formattedStringUsingFormat:@"yyyyMMdd"],
                         [[[Config shareInstance] find:NODO] intValue],
                         [date formattedStringUsingFormat:@"MM/dd/yyyy '00:00'"]]
                   tipo:kDelete];
    }
    
    return YES;
}

- (void)completarParteData:(ParteDiarioData*)actividad parte:(ParteDiario*)parte
{
    NSString *sDate = [parte.fecha formattedStringUsingFormat:@"yyyyMMdd"];
    NSInteger iParteDiario = [sDate integerValue];
    
    actividad.descripcion = [NSString string:parte.descripcion];
    actividad.fecha       = [parte.fecha formattedStringUsingFormat:@"dd/MM/yyyy"];
    actividad.partediario = iParteDiario;
}

- (BOOL)checkParteDiarioDesdeHasta
{
    // Este método devuelve YES si se pudo guardar los partes diario para el rango de fechas.
 
    // Chequeo que se haya ingresado al manos una actividad.
    [self calcularTotales];
    if ((self.totalManiana == 0) && (self.totalTarde == 0))
    {
        // Muestro la alerta preguntando al usuario si desea o no modificar los datos existentes del parte diario.
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                       message:@"Debe seleccionar al menos una tarea."
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
        [aler show];
        [aler release];
        
        return NO;
    }
    
    // Controlo que las fechas seleccionada sean válidas.
    if (![self esFechaValida:self.dateDesde sender:self.btnFechaDesde])
        return NO;
    
    if (![self esFechaValida:self.dateHasta sender:self.btnFechaHasta])
        return NO;
    
    // Chequeo si existen partes diarios entre las fechas Desde y Hasta.
    NSArray *listadoPartesExistentes = [ParteDiario tieneTareasCargadasEntreFecha:self.dateDesde hasta:self.dateHasta];
    if ([listadoPartesExistentes count] > 0)
    {
        NSString *sFecha = @"";
        
        // Chequeo y genero el texto según sea para una solo fecho o varias.
        if ([listadoPartesExistentes count] == 1)
        {
            // Una sola fecha
            sFecha = [NSString stringWithFormat:@"la fecha %@",
                [(NSDate*)[listadoPartesExistentes objectAtIndex:0] formattedStringUsingFormat:@"dd/MM/yyyy"] ];
        } else
        {
            // Más de una fecha.
            NSString *sDates = @"";
            
            // Genero el string separado por comas de las fechas en el listado.
            for (NSDate *d in listadoPartesExistentes)
            {
                if ([sDates length] != 0)
                    sDates = [NSString stringWithFormat:@"%@, ", sDates];
                
                sDates = [NSString stringWithFormat:@"%@%@", sDates,
                          [d formattedStringUsingFormat:@"dd/MM/yyyy"]];
            }
            
            sFecha = [NSString stringWithFormat:@"las fechas %@", sDates];
        }
        
        // Muestro la alerta preguntando al usuario si desea o no modificar los datos existentes del parte diario.
        
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                       message:[NSString stringWithFormat:
                                                                @"Se eliminarán los datos existentes para %@.\n\n¿Desea continuar de todas maneras?",
                                                                sFecha]
                                                      delegate:self
                                             cancelButtonTitle:@"No"
                                             otherButtonTitles:@"Si",nil];
        [aler show];
        [aler release];
        
        return NO;
    }
    
    
    
    return YES;
}

- (void)saveParteDiarioDesdeHasta
{
    // Obtengo el listado de días hábiles para el APM.
    NSArray *dias = [self listadoDiasDesde:self.dateDesde hasta:self.dateHasta];
    
    // Guardo el parte diario para los días del listado.
    for (NSDate *d in dias)
    {
        // Borrar el parte diario anterior para la fecha dada.
        [ParteDiario eliminarTareas:[d dateAsDateWithoutTime] conservarVisitas:YES];
        
        // guardo el parte diario actual
        [self saveParteDiarioSimple:d];
    }
    
    // Mostrar un mensaje y hacer un pop
    NSString *msg = @"Los datos se han actualizado corectamente";
    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    
    [alertView show];
    
}

#pragma mark -
#pragma mark Overrides

-(BOOL)hasFooter
{
    return YES;
}

-(NSString *)getTitleForHeader{
    return @"Partes Diarios";
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"parte_32"];
}


#pragma mark - 
#pragma mark TableView delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of time zone names.
	return [self.partes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MyIdentifier = @"ParteCellIdentifier";
	
    // Obtain the cell object.
	CustomParteDiarioCell *cell = (CustomParteDiarioCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	// If the cell is void, create the cell object.
    if (cell == nil)
    {
        cell = [[[CustomParteDiarioCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier] autorelease];
        [[NSBundle mainBundle] loadNibNamed:@"CustomParteDiarioCell" owner:self options:nil];
        cell = self.tmpCell;
    }
    
    ParteDiario *p = [self.partes objectAtIndex:indexPath.row];
    
    cell.descripcionLbl.text = p.descripcion;
    if (usaPorcentaje)
    {
        cell.manianaLbl.text = [NSString stringWithFormat:@"%d %%", [p cantidadManiana]];
        cell.tardeLbl.text   = [NSString stringWithFormat:@"%d %%", [p cantidadTarde]];
        cell.totalLbl.text   = [NSString stringWithFormat:@"%d %%", [p cantidadTotal]];
    
    } else
    {
        cell.manianaLbl.text = [NSString intToStr:[p cantidadManiana] ifZero:@"-"];
        cell.tardeLbl.text   = [NSString intToStr:[p cantidadTarde] ifZero:@"-"];
        cell.totalLbl.text   = [NSString intToStr:[p cantidadTotal] ifZero:@"-"];
    }
    
    if (!(p.identificador == 9999))
    {
        cell.descripcionLbl.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
    } else
        cell.descripcionLbl.textColor = UIColorFromRGB(defaultCellFontColor);
    
    [cell setChecked:[p cantidadTotal] > 0];
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
    currentIndex = [indexPath row];
    ParteDiario *p = [self.partes objectAtIndex:currentIndex];
    
    // Si el parte ya fue transmitido, no se puede enviar.
    if (self.yaTransmitido)
//    for (ParteDiario* pAux in self.partes)
    {
//        if (pAux.identificador != 9999)
//        if ((pAux.maniana.id_nodotareaspartediario != 0) || (pAux.tarde.id_nodotareaspartediario != 0))
//            if (!([pAux.maniana.registro length] > 0) || !([pAux.tarde.registro length] > 0))
//            {
                UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"No se puede modificar un parte dario ya transmitido." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
                
                [av show];
                return;
//            }
    }
    
    // Si se selecciono una fecha posterior a la fecha actual no se puede modificar
    if ([self.currentDate compare:[NSDate date]] == NSOrderedDescending)
    {
        UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"La fecha seleccionada es mayor a la actual. No puede realizar modificaciones" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        
        [av show];
        return;
    }
    
    // Ver si se selecciono la tarea MEDICOS
    if (p.identificador == 9999)
    {
        UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"No es posible modificar el parte de visitas. (dado que dicha tarea se completa cuando se realiza una visita y no desde este proceso)" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        
        [av show];
        return;
    }
    
    // Validar por fines de semana y feriados
    if (visitaFinDeSemana == NO)
    {
        if ([self.currentDate isWeekEnd] || [MTFeriado isFeriado:self.currentDate])
        {
            UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"La fecha seleccionada es fin de semana o feriado." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
            
            [av show];
            return;
        }
    }
    
    
    // Chequeo si debo mostrar el popup de Porcentajes o Cantidad
    if (usaPorcentaje)
    {
        // Chequeo si el 100% de las tareas de la mañana y de la tarde estan completos cuando el parte diario es obligatorio.
        if ((parteObligatorio) &&
            (p.cantidadManiana  == 0) &&
            (p.cantidadTarde    == 0) &&
            (self.totalManiana  == 100) &&
            (self.totalTarde    == 100))
        {
            // Ya se reportó el 100% del parte
            UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"Ya se reportó el 100% del parte." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
            
            [av show];
            return;
        }
        
        // Porcentajes
        self.txtPorcManiana.text = [NSString stringWithFormat:@"%d %%", [p cantidadManiana]];
        self.txtPorcTarde.text   = [NSString stringWithFormat:@"%d %%", [p cantidadTarde]];
        
        
    } else
    {
        // Cantidad
        self.manianaText.text = [NSString stringWithFormat:@"%d", [p cantidadManiana]];
        self.tardeText.text = [NSString stringWithFormat:@"%d", [p cantidadTarde]];
        
    }
    
    nuevaCantidadManiana = p.cantidadManiana;
    nuevaCantidadTarde = p.cantidadTarde;
    
    [self showPopupWithTittle:[NSString stringWithFormat:@"   %@", p.descripcion]];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    [self calcularTotales];
    
    CGRect frame = CGRectMake(0, 0, 844, 22);
    UILabel *background = [[UILabel alloc] initWithFrame:frame];
    background.autoresizingMask = 34;
    background.backgroundColor = UIColorFromRGB(defaultHeaderCellColor);
    
    UILabel *label;
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(5, 0, 102, 20);
    label.autoresizingMask = 36;
    label.text = @"Totales";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont boldSystemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    // Valor total Mañana
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(500, 0, 80, 20);
    label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    if (usaPorcentaje)
        label.text = [NSString stringWithFormat:@"%d %%", self.totalManiana];
    else
        label.text = [NSString stringWithFormat:@"%d", self.totalManiana];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont boldSystemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    // Valor total Tarde
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(651, 0, 42, 20);
    label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    if (usaPorcentaje)
        label.text = [NSString stringWithFormat:@"%d %%", self.totalTarde];
    else
        label.text = [NSString stringWithFormat:@"%d", self.totalTarde];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont boldSystemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(775, 0, 57, 20);
    label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    if (usaPorcentaje)
        label.text = [NSString stringWithFormat:@"%g %%", self.total];
    else
        label.text = [NSString stringWithFormat:@"%g", self.total];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont boldSystemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    return [background autorelease];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGRect frame = CGRectMake(0, 0, 844, 22);
    UILabel *background = [[UILabel alloc] initWithFrame:frame];
    background.autoresizingMask = 34;
    background.backgroundColor = UIColorFromRGB(defaultHeaderCellColor);
    
    UILabel *label;
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(5, 0, 102, 20);
    label.autoresizingMask = 36;
    label.text = @"Motivos";
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(500, 0, 80, 20);
    label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    label.text = @"Mañana";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(651, 0, 42, 20);
    label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    label.text = @"Tarde";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(782, 0, 42, 20);
    label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    label.text = @"Total";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = UIColorFromRGB(defaultHeaderCellFontColor);
    label.font = [UIFont systemFontOfSize:14.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [background addSubview:label];
    
    return [background autorelease];
}

#pragma mark-
#pragma mark UIAlertView delegate methods

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // El UIAlert solo es llamado con delegate por el método de guardar un parte diario entre fechas,
    // por lo que no necesito saber quién lo llamó.
    
    // buttonIndex = 0 : No
    
    // buttonIndex = 1 : Si
    if (buttonIndex == 1)
    {
        [self saveParteDiarioDesdeHasta];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark -
#pragma mark TextField delegate methods

-(BOOL) textFieldShouldBeginEditing:(UITextField*)textField {
    self.current = textField;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return NO;
}

- (BOOL)textFieldOLD:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (range.location >= 6)
        return NO;
    
    if ([string length] == 0 && range.length > 0)
    {
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return NO;
    }
    
    NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    if ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0)return YES;
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    if (range.location >= 6)
        return NO;
    
    NSString *oldString = [NSString string:textField.text ifNull:@""];
    NSCharacterSet *notAllowedSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
                            stringByTrimmingCharactersInSet:notAllowedSet];
    
    if ([oldString isEqualToString:tempString])
        return NO;
    
    return   YES;
}


#pragma mark -
#pragma mark Popup Managemnet

- (void)showPopupWithTittle:(NSString*)tittle
{
    
    [self.tableView setUserInteractionEnabled:NO];
    [self.frameView setHiddenAnimated:NO];
    
    // Chequeo si debo mostrar el popup de Porcentajes o Cantidad
    if (usaPorcentaje)
    {
        // Chequeo si debo mostrar todas las opciones de porcentaje
        [self.arrayComboManiana removeAllObjects];
        [self.arrayComboManiana addObjectsFromArray:
                                    [[[NSArray alloc] initWithObjects:@"0", @"25", @"50", @"75", @"100", nil] autorelease]
                                ];
        [self.arrayComboTarde   removeAllObjects];
        [self.arrayComboTarde   addObjectsFromArray:
                                    [[[NSArray alloc] initWithObjects:@"0", @"25", @"50", @"75", @"100", nil] autorelease]
                                ];

#ifdef DEBUG_PARTEDIARIO
        NSLog(@"----------------------------------------");
        NSLog(@" Total Mañana: %d", self.totalManiana);
        NSLog(@" Diferencia Mañana: %d", 100 - self.totalManiana);
        NSLog(@" Total Tarde: %d", self.totalTarde);
        NSLog(@" Diferencia Tarde: %d", 100 - self.totalTarde);
#endif
        
        // Mañana
        if ((parteObligatorio) &&
            (self.totalManiana    >= 100) &&
            (nuevaCantidadManiana == 0))
        {
            [self.arrayComboManiana removeAllObjects];
            [self.arrayComboManiana addObject:@"0"];
            [self.btnPorcManiana setEnabled:true];

        } else
        {
            int diferencia = 100 - self.totalManiana + nuevaCantidadManiana;
            if    (diferencia < 100)
            {
                    [self.arrayComboManiana removeAllObjects];
                    [self.arrayComboManiana addObject:@"0"];
                    if (diferencia >= 25) [self.arrayComboManiana addObject:@"25"];
                    if (diferencia >= 50) [self.arrayComboManiana addObject:@"50"];
                    if (diferencia >= 75) [self.arrayComboManiana addObject:@"75"];
            }
        }
        
        // Tarde
        if ((parteObligatorio) &&
            (self.totalTarde    >= 100) &&
            (nuevaCantidadTarde == 0))
        {
            [self.arrayComboTarde removeAllObjects];
            [self.arrayComboTarde addObject:@"0"];
            [self.btnPorcTarde setEnabled:true];
            
        } else
        {
            int diferencia = 100 - self.totalTarde + nuevaCantidadTarde;
            if    (diferencia < 100)
            {
               
                [self.arrayComboTarde removeAllObjects];
                [self.arrayComboTarde addObject:@"0"];
                if (diferencia >= 25) [self.arrayComboTarde addObject:@"25"];
                if (diferencia >= 50) [self.arrayComboTarde addObject:@"50"];
                if (diferencia >= 75) [self.arrayComboTarde addObject:@"75"];
            }
        }
        
        // Porcentajes
        self.lblPorcTitulo.text = tittle;
        [self.popupPorcetaje setHiddenAnimated:NO];
        
    } else
    {
        // Cantidad
        self.lblTitulo.text = tittle;
        [self.popup setHiddenAnimated:NO];
        
    }
    
    
    //[self.popup setHiddenAnimated:NO];
    
}

- (void)hidePopup
{
    [self.popup setHiddenAnimated:YES];
    [self.popupPorcetaje setHiddenAnimated:YES];
    [self.frameView setHiddenAnimated:YES];
    [self.tableView setUserInteractionEnabled:YES];
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

-(IBAction)pressedAceptarPop:(id)sender
{
    // Quito el teclado
    [self.current resignFirstResponder];
    
    // Obtengo el parte diario actual
    ParteDiario *p = [self.partes objectAtIndex:currentIndex];
    
    // Obtengo los valores ingresados
    //NSUInteger nuevaCantidadManiana = [self.manianaText.text intValue];
    //NSUInteger nuevaCantidadTarde = [self.tardeText.text intValue];
    if (!usaPorcentaje)
    {
        nuevaCantidadManiana = [self.manianaText.text intValue];
        nuevaCantidadTarde = [self.tardeText.text intValue];
    }
    
    // Actualizo las cantidades
    [p setCantidadForManiana:nuevaCantidadManiana];
    [p setCantidadForTarde:nuevaCantidadTarde];
    
    // Oculto el pop
    [self hidePopup];
    
    // Refresco
    [self.tableView reloadData];
    
    // Marca de que se modificaron datos del parte
    huboModificaciones = YES;
}

-(IBAction)pressedCancelarPop:(id)sender
{
    [self.current resignFirstResponder];
    [self hidePopup];
}



#pragma mark popListView delegate methods

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender didSelectRow:(NSInteger *)MenuIndex selectedText:(NSString *)RowText
{
    if (sender == self.btnPorcManiana)
    {
        nuevaCantidadManiana = [RowText intValue];
        self.txtPorcManiana.text = [NSString stringWithFormat:@"%@ %%", RowText];
        
        return;
    } else
    
    if (sender == self.btnPorcTarde)
    {
        nuevaCantidadTarde = [RowText intValue];
        self.txtPorcTarde.text = [NSString stringWithFormat:@"%@ %%", RowText];;
        
        return;
    }
}

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender willSelectObject:(id)selObject
{
    [self.loadingView show:self];
}


#pragma mark -
#pragma mark Tap methods

-(IBAction)selectDate:(id)sender
{
    // Si hubo modificaciones obligarle a guardar los datos antes de continuar
    if ((huboModificaciones) && (self.viewControllerMode == ParteDiarioSimple))
    {
        UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@"" message:@"Por favor presione Guardar antes de cambiar de fecha para modificar los datos ingresados" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        [av show];
        
        return;
    }
    
    if (!Delegate.popoverController.popoverVisible)
    {
        // Indico quién lo está llamando
        datePickerCaller = sender;
        NSDate *pickerDate = self.currentDate;
        
        DateSelectorViewController *dvc = [[[DateSelectorViewController alloc] initWithNibName:@"DateSelectorViewController" bundle:nil] autorelease];
        
        // Por algua razón tengo que definir así el frame porque si lo hago casteando el sender no funciona.
        // Fecha Desde
        CGRect frame = self.selectDateButton.frame;
        if (sender == self.btnFechaDesde)
        {
            frame = self.btnFechaDesde.frame;
            pickerDate = self.dateDesde;
        }
        else
        
        // Fecha Hasta
        if (sender == self.btnFechaHasta)
        {
            frame = self.btnFechaHasta.frame;
            pickerDate = self.dateHasta;
        }
        
        
        dvc.contentSizeForViewInPopover = CGSizeMake(320.0f, 216.0f);
        dvc.date = pickerDate;
        dvc.delegate = self;
        
        
        UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:dvc] autorelease];
        
        Delegate.popoverController = [[[UIPopoverController alloc] initWithContentViewController:nav] autorelease];
//        [Delegate.popoverController presentPopoverFromRect:self.selectDateButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        [Delegate.popoverController presentPopoverFromRect:frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

-(IBAction)pressedGuardar:(id)sender
{
    
    
        // Chequeo si se trata de un parte diario por porcentaje y si es obligatorio
        if (usaPorcentaje && parteObligatorio)
        {
            if (!((self.totalManiana == 0) && (self.totalTarde == 0)))
            {
                // Chequeo que esté completo el parte de la mañana.
                if ((self.totalManiana != 100))
                {
                    UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@""
                                                                  message:@"Debe ingresar el 100% de las tareas de la mañana."
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil]
                                       autorelease];
                    
                    [av show];
                    return;
                }
                
                // Chequeo que esté completo el parte de la tarde.
                if ((self.totalTarde != 100))
                {
                    UIAlertView *av = [[[UIAlertView alloc] initWithTitle:@""
                                                                  message:@"Debe ingresar el 100% de las tareas de la tarde."
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil]
                                       autorelease];
                    
                    [av show];
                    return;
                }
            }
        }
    
    // Parte diario Simple
    if (self.viewControllerMode == ParteDiarioSimple)
    {
        [self saveParteDiarioSimple:self.currentDate];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    } else
    
    // Parte diario Desde Hasta
    if (self.viewControllerMode == ParteDiarioDesdeHasta)
    {
        // Chequea si hay datos en alguno de los partes comprendidos entre las fechas Desde y Hasta.
        // Si existen, el método devuelve NO y llama una Alert para que el usuario confirme si desea o no
        // modificar los datos. En este caso, el método delegate del UIAlert determina si se grabarán los datos o no.
        if ([self checkParteDiarioDesdeHasta])
        {
            [self saveParteDiarioDesdeHasta];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    
    // Limpia todos los partes de la base
    [ParteDiario cleanTareasParte];
    [ParteDiario cleanParte];
}

-(IBAction)pressedCancelar:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark DateSelectorDelegate

//-(void)dateSelected:(NSDate *)date
-(void)DateSelector:(DateSelectorViewController*)dateSelector dateSelected:(NSDate *)date
{
    if (![self esFechaValida:date sender:datePickerCaller])
    {
        // Si no es una fecha válida, no actualizo la selección.
        return;
    }
    
    // Fecha - Modo Simple
    if (datePickerCaller == self.selectDateButton)
        [self refresh:date];
    
    // Fecha desde
    if (datePickerCaller == self.btnFechaDesde)
        [self refreshDesde:date];
    else
        
    // Fecha hasta
    if (datePickerCaller == self.btnFechaHasta)
        [self refreshHasta:date];
    
    //[self refresh:date];
}

#pragma mark -
#pragma mark Memory Management

-(void)dealloc
{
    [_currentDate release];
    [_dateTextField release];
    [_selectDateButton release];
    [_tableView release];
    [_partes release];
    [_popup release];
    [_tardeText release];
    [_manianaText release];
    [_current release];
    [_cicloActual release];
    [_cicloProximo release];
    [_inserted release];
    [_updated release];
    [_deleted release];
    
    [_lblFecha release];
    [_lblFechaDesde release];
    [_txtFechaDesde release];
    [_btnFechaDesde release];
    [_lblFechaHasta release];
    [_txtFechaHasta release];
    [_btnFechaHasta release];
    [_dateDesde release];
    [_dateHasta release];
    
    [_frameView release];
    [_lblTitulo release];
    [_popupPorcetaje release];
    [_txtPorcManiana release];
    [_txtPorcTarde release];
    [_btnPorcTarde release];
    [_btnPorcManiana release];
    
    [_popupMenu release];
    
    [_arrayComboManiana release];
    [_arrayComboTarde   release];
    
    
    [_lblPorcTitulo release];
    [_lblYaTransmitido release];
    [super dealloc];
}

- (IBAction)pressedPorcManiana:(id)sender
{
    UIView *popRectView = (UIView*)sender;
    
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(150, 240);
    
    // Creo el objeto, asigno el título, el listado de datos, y defino que utilizará búsqueda.
    //if (!self.popupMenu)
    //{
    self.popupMenu = [[MTpopListView alloc] initWithRect:rect];
    //}
    [self.popupMenu dismissPopoverAnimated:NO];
    
    self.popupMenu.delegate = self;
    
    [self.popupMenu setTitleAndParameters:@"Porcentaje" rowData:self.arrayComboManiana];
    [self.popupMenu setSize:rect.size];
    
    [self.popupMenu useSearch:NO];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x + self.popupPorcetaje.frame.origin.x,
                               popRectView.frame.origin.y + self.popupPorcetaje.frame.origin.y, // + 64,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:sender
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionLeft
                                            animated:YES];

}

- (IBAction)pressedPorcTarde:(id)sender
{
    UIView *popRectView = (UIView*)sender;
    
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(150, 240);
    
    // Creo el objeto, asigno el título, el listado de datos, y defino que utilizará búsqueda.
    //if (!self.popupMenu)
    //{
    self.popupMenu = [[MTpopListView alloc] initWithRect:rect];
    //}
    [self.popupMenu dismissPopoverAnimated:NO];
    
    self.popupMenu.delegate = self;
    
    [self.popupMenu setTitleAndParameters:@"Porcentaje" rowData:self.arrayComboTarde];
    [self.popupMenu setSize:rect.size];
    
    [self.popupMenu useSearch:NO];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x + self.popupPorcetaje.frame.origin.x,
                               popRectView.frame.origin.y + self.popupPorcetaje.frame.origin.y, // + 64,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:sender
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionLeft
                                            animated:YES];
}

- (BOOL) fueTransmitido
{
    // Si el parte ya fue transmitido, no se puede enviar.
    for (ParteDiario* pAux in self.partes)
    {
        if (pAux.identificador != 9999)
        {
            if ((pAux.maniana.id_nodotareaspartediario != 0))
                if (!([pAux.maniana.registro length] > 0))
                {
                    return YES;
                }
            
            if ((pAux.tarde.id_nodotareaspartediario != 0))
                if (!([pAux.tarde.registro length] > 0))
                {
                    return YES;
                }
        }
    }
    
    return NO;
}

@end
