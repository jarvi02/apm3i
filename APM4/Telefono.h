//
//  Telefono.h
//  APM4
//
//  Created by Laura Busnahe on 4/18/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Telefono : NSObject

@property(nonatomic, retain) NSString* numero;
@property(nonatomic, assign) NSInteger pais;
@property(nonatomic, assign) NSInteger area;

-(NSString*) descripcion;
-(id)copy;

-(void)saveForMedico:(NSInteger)idMedico Domicilio:(NSInteger)idDomicilio;

+(void)deleteAllForMedico:(NSInteger)idMedico;
+(NSArray*)getAllByMedico:(NSUInteger)idMedico Domicilio:(NSUInteger)idDomicilio;

@end
