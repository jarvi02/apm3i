//
//  MTstdCoberturaPotencial.h
//  APM4
//
//  Created by Laura Busnahe on 7/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTstdCobertuaCommon.h"

@interface MTstdCoberturaPotencial : MTstdCobertuaCommon

+ (NSArray*)getAllWhere:(NSString*)where;

@end
