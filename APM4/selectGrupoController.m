//
//  selectGrupoController.m
//  APM4
//
//  Created by Laura Busnahe on 6/25/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "selectGrupoController.h"
#import "CarteraViewController.h"
#import "MTGrupos.h"

#import "GlobalDefines.h"
#import "NSStringExtended.h"

@interface selectGrupoController ()

@end

@implementation selectGrupoController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.data          = nil;
        self.filteredData  = nil;
        self.carteraReference = nil;
        self.agregarAGrupo = NO;
    }
    return self;
}

- (id)initWithDefaultNib
{
    self = [self initWithNibName:@"selectGrupoController" bundle:nil];
    self.modalPresentationStyle = UIModalPresentationFormSheet;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.btnAceptar.tintColor = UIColorFromRGB(defaultButtonColor);
    
    self.data         = [NSMutableArray arrayWithArray:[MTGrupos getAll]];
    self.filteredData = [NSMutableArray arrayWithArray:self.data];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)navigateToController:(UIViewController *)controller
{
    [self dismiss];
    //[Delegate.navigationController pushViewController:controller animated:YES];
    [self.delegate navigateToController:controller];
    
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark Tap methods

- (IBAction)tapAceptar:(id)sender
{
    [self dismiss];
}

#pragma mark -
#pragma mark Table Delegate and Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// There is only one section.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of time zone names.
    
	return [self.filteredData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MyIdentifier = @"grupoCellIdentifier";
	
    // Obtain the cell object.
	UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        // Use the default cell style.
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    
    NSString *cellText = @"";
    MTGrupos *_grp = nil;
    _grp = [self.filteredData objectAtIndex:indexPath.row];
    cellText = [_grp getDescription];
    
    // Si el grupo está bloqueado, lo muestro en gris (si no puede modificarse).
    if (_grp.blocked)
    {
        cell.textLabel.textColor = UIColorFromRGB(defaultFixedInteractiveCellFontColor);
        //cell.textLabel.textColor = [UIColor grayColor];
    }
    else
    {
        cell.textLabel.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
        //cell.textLabel.textColor = [UIColor blackColor];
    }
    
	cell.textLabel.text = [NSString stringWithFormat:@"%@", cellText];
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MTGrupos *selectedGroup;
    selectedGroup = [self.filteredData objectAtIndex:indexPath.row];
    
    // Chequeo si fue llamado o no con un objecto de referencia de la cartera.
    if (!self.carteraReference)
    {
        // Si no tiene como referencia la cartera, entonces abro la cartera con el grupo seleccionado.
        CarteraViewController *cvc = [[CarteraViewController alloc] initWithNibName:@"CarteraViewController" bundle:nil];
        cvc.selectedGrupo = selectedGroup;
    
        [self navigateToController:cvc];
    } else
    {
        // Si fue abierto con un objeto de la cartera, chequeo si es nuevamente selección de grupo, o se
        // están agregando médicos al grupo.
        if (self.agregarAGrupo)
        {
            [self dismiss];
            [self.carteraReference addSelectionToGroup:selectedGroup];
        } else
        {
            self.carteraReference.selectedGrupo = selectedGroup;
            [self dismiss];
            [self.carteraReference performTask:@"aplicargrupo"];
        }
    }
}

- (void)refreshTable
{
    [self.tblGrupos reloadData];
}



#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredData removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    if ((searchText != nil) && ([searchText length] > 0))
    {
        if (self.data.count > 0)
        {
                // En caso de no ser una entidad, compruebo si responde al selector getDescription
                if ([[self.data objectAtIndex:0] respondsToSelector:@selector(getDescription)])
                {
                    for (id item in self.data)
                    {
                        NSRange range = [[item getDescription] rangeOfString:searchText
                                                                     options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                        
                        if ( range.length > 0)
                        {
                            [self.filteredData addObject:item];
                        }
                    }
                } else
                {
                    // Para los objetos de tipo NSString.
                    for (NSString *item in self.data)
                    {
                        NSRange range = [item rangeOfString:searchText
                                                    options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                        
                        if ( range.length > 0)
                        {
                            [self.filteredData addObject:item];
                        }
                    }
                }
        }
    } else
    {
        // Si no hay texto de búsqueda, copio todos los elementos.
        for (id item in self.data)
            [self.filteredData addObject:item];
    }
    
    
    [self refreshTable];
}


#pragma mark -
#pragma mark Search bar delegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)aSearchBar
{
    
}


- (void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar {
    
    // If the user finishes editing text in the search bar by, for example:
    // tapping away rather than selecting from the recents list, then just dismiss the popover
    //
    
    // dismiss the popover, but only if it's confirm UIActionSheet is not open
    //  (UIActionSheets can take away first responder from the search bar when first opened)
    //
    // the popover's main view controller is a UINavigationController; so we need to inspect it's top view controller
    //
    [aSearchBar resignFirstResponder];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    // When the search string changes, filter the recents list accordingly.
    [self filterContentForSearchText:searchText scope:@""];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self.view endEditing:YES];
}

// En vistas que se presentan de forma modal es necesario agregar este override para que oculte el teclado
// con resignFirstResponder
- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

@end
