//
//  Localized.h
//  RnR
//
//  Created by Fabian E. Pezet Vila on 10/12/12.
//  Copyright (c) 2012 Fabian E. Pezet Vila. All rights reserved.
//

#import <Foundation/Foundation.h>


#define LocalizedKey(key, stringdefault) ([[Localized shareInstance] isValidKey:key] ? [[Localized shareInstance] find:key] : stringdefault)


@interface Localized : NSObject {
    
@private
    NSMutableDictionary *dictionary;
}

@property(nonatomic, retain) NSString *localize;

+(Localized*)shareInstance;
-(NSString*)find:(NSString*)aKey;
-(void) printAllKeyFor:(NSString*) idioma;
-(BOOL)isValidKey:(NSString*)key;

@end
