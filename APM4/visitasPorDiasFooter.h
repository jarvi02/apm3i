//
//  visitasPorDiasFooter.h
//  APM4
//
//  Created by Laura Busnahe on 9/12/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface visitasPorDiasFooter : UIView

@property (retain, nonatomic) IBOutlet UIButton *lblFecha;
@property (retain, nonatomic) IBOutlet UIButton *lblFijos;
@property (retain, nonatomic) IBOutlet UIButton *lblFluctuantes;
@property (retain, nonatomic) IBOutlet UIButton *lblAcompaniadas;
@property (retain, nonatomic) IBOutlet UIButton *lblTotal;


@end
