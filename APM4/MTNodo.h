//
//  MTNodo.h
//  APM4
//
//  Created by Laura Busnahe on 8/27/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTNodo : NSObject <NSCopying>

@property(nonatomic) NSUInteger         recID;
@property(nonatomic, strong) NSString*  descripcion;
@property(nonatomic) NSUInteger         idEntidad;


- (NSString*)getDescription;
- (MTNodo*)copyWithZone:(NSZone*)zone;


+ (NSArray*)getAllforEntidad:(NSUInteger)idEntidad;

@end
