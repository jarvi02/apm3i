//
//  CarteraMedico.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 14/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarteraMedico : NSObject


@property(nonatomic, assign) NSInteger idCarteraMedico;
@property(nonatomic, retain) NSString *apellidoNombre;
@property(nonatomic, retain) NSString *lugar;
@property(nonatomic, retain) NSString *especialidad;
@property(nonatomic, retain) NSString *descripcionEspecialidad;
@property(nonatomic, retain) NSString *dias;
@property(nonatomic, retain) NSString *actividad;
@property(nonatomic, retain) NSString *descripcionActividad;
@property(nonatomic, retain) NSString *direccion;
@property(nonatomic, assign) NSInteger estado;
@property(nonatomic, retain) NSString *descripcionEstado;
@property(nonatomic, assign) NSInteger a;
@property(nonatomic, assign) NSInteger b;
@property(nonatomic, assign) NSInteger m;
@property(nonatomic, assign) NSInteger visitasProgramadas;
@property(nonatomic, assign) NSInteger visitasRealizadas;
@property(nonatomic, retain) NSString *objetivos;
@property(nonatomic, assign) NSInteger asignacion;
@property(nonatomic, assign) NSInteger penalta;
@property(nonatomic, assign) NSInteger penbaja;
@property(nonatomic, retain) NSString *descripcionABM;
@property(nonatomic, retain) NSString *descripcionInstitucion;
@property(nonatomic, assign) NSInteger visitasProgramadas_2;
@property(nonatomic, assign) NSInteger visitasRealizadas_2;
@property(nonatomic, assign) NSInteger idEspecialidad;
@property(nonatomic, assign) NSInteger idPotencial;
@property(nonatomic, assign) NSInteger idProvincia;
@property(nonatomic, retain) NSString *localidad;
@property(nonatomic, retain) NSString *calle;
@property(nonatomic, retain) NSMutableArray *grupos;
@property(nonatomic, retain) NSString *email;

+(NSArray*) GetAll;
-(void)darDeBaja:(double)fecha motivo:(NSInteger)motivo descr:(NSString *)descr pendiente:(BOOL)pendiente baja:(BOOL)baja;
-(NSString *)novedad;

-(UIImage*)imagenMedico;
-(UIColor*)colorTexto;
-(NSString*)getDescripcionABM;

- (BOOL)esFluctuante;
- (BOOL)esBaja;
- (BOOL)necesitaAutorizacion;

- (NSString*)getDescripcionDomicilio;
- (NSString*)getDescripcionVisitasRealizadas;
- (NSString*)getDescripcionVisitasProgramadas;
@end
