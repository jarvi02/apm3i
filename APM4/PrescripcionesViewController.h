//
//  PrescripcionesViewController.h
//  APM4
//
//  Created by Laura Busnahe on 2/27/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "CarteraViewController.h"
#import "prescAuditoriaViewController.h"
#import "customTotalesCell.h"
#import "customPrescripcionesCell.h"
#import "customPrescripcionesHeader.h"
#import "customPrescripcionesFooter.h"

#import "GlobalDefines.h"

@class MTAuditorias;
@class MTMercados;
@class MTPrescripcionesTotales;

@class prescMasterViewController;

@interface PrescripcionesViewController : CustomViewController <UITableViewDelegate, UITableViewDataSource,
                                                                prescAuditoriaDelegate, MTHeaderDelegate>
{
    MTPrescripcionesTotales *totalesPrescripciones;
}

@property (strong, nonatomic) MTAuditorias  *auditoria;
@property (strong, nonatomic) NSArray       *mercados;


@property(nonatomic, assign) CarteraViewController *carteraViewControllerReference;

// Tabla de Prescripciones
@property (retain, nonatomic) IBOutlet UITableView                *tblPrescripciones;
@property (retain, nonatomic) IBOutlet customPrescripcionesHeader *prescripcionesHeader;
@property (retain, nonatomic) IBOutlet customPrescripcionesFooter *prescripcionesFooter;
@property (retain, nonatomic) IBOutlet customPrescripcionesCell   *prescripcionesCell;
@property (nonatomic, retain) UINib       *prescripcionesHeaderNIB;
@property (nonatomic, retain) UINib       *prescripcionesFooterNIB;
@property (nonatomic, retain) UINib       *prescripcionesCellNIB;

// Tabla de Totales del médico
@property (retain, nonatomic) IBOutlet UITableView        *tblTotales;
@property (retain, nonatomic) IBOutlet UIView             *totalesHeader;
@property (retain, nonatomic) IBOutlet customTotalesCell  *totalesCell;
@property (nonatomic, retain) UINib       *totalesHeaderNIB;
@property (nonatomic, retain) UINib       *totalesCellNIB;


// Outlets
@property (retain, nonatomic) IBOutlet UIBarButtonItem  *btnAuditoria;


// Methods
- (id)initViewDefaultNib;

- (IBAction)tapAuditoria:(id)sender;

@end