//
//  NSString+Validaciones.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 14/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "NSString+Validaciones.h"

@implementation NSString (Validaciones)


-(BOOL)isValidIP {
    
    NSString *urlRegEx = @"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    NSPredicate *ipTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [ipTest evaluateWithObject:self];

}

-(BOOL)isNumber{
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return ([self rangeOfCharacterFromSet:notDigits].location == NSNotFound);
}

@end
