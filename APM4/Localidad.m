//
//  Localidad.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Localidad.h"
#import "DB.h"

@implementation Localidad



- (id)init
{
    self = [super init];
    if (self) {
        self.descripcion = @"";
    }
    return self;
}

- (void)dealloc
{
    [_descripcion release];
    [super dealloc];
    
}

- (NSString*)getDescription
{
    return [NSString stringWithFormat:@"%@", self.descripcion];
}

+(NSArray*)GetAllByProvincia:(NSInteger)idProvincia{
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *sql = @"select pais, provincia, descripcion, calles, id from geo_localidades where provincia = %d order by descripcion;";
    
    NSString *query = [NSString stringWithFormat:sql,idProvincia];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        Localidad *o;
        char *_c;
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            o = [[Localidad alloc] init];
            
            o.pais = sqlite3_column_int(statement, 0);
            o.provincia = sqlite3_column_int(statement, 1);
            
            _c = (char *)sqlite3_column_text(statement, 2);
            if((_c != nil) && (strlen(_c) > 0)) {
                o.descripcion = [NSString stringWithUTF8String:_c];
			}
            
            o.calles = sqlite3_column_int(statement, 3);
            
            o.idLocalidad = sqlite3_column_int(statement, 4);
            
            [array addObject:o];
            [o release];
            
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
}


-(id)copy{
    
    Localidad *o = [[Localidad alloc] init];
    o.pais = self.pais;
    o.provincia = self.provincia;
    o.calles = self.calles;
    o.descripcion = [NSString stringWithFormat:@"%@", self.descripcion];
    o.idLocalidad = self.idLocalidad;
    
    return [o autorelease];
}

@end
