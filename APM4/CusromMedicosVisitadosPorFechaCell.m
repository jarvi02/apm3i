//
//  CusromMedicosVisitadosPorFechaCell.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CusromMedicosVisitadosPorFechaCell.h"

@implementation CusromMedicosVisitadosPorFechaCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_fecha release];
    [_nombre release];
    [_tipoVisita release];
    [_actividad release];
    [super dealloc];
}
@end
