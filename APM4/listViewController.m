//
//  listViewController.m
//  APM4
//
//  Created by Laura Busnahe on 6/11/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "listViewController.h"
#import "Potencial.h"
#import "Especialidad.h"
#import "ObraSocial.h"

#import "NSStringExtended.h"



@interface listViewController ()

@end

@implementation listViewController
{
    NSMutableArray *newList;
}

#pragma mark - 
#pragma mark Init methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.allObjects     = nil;
        self.listContent    = nil;
        self.popMenu        = nil;
        newList             = nil;
        
        self.delegate  = nil;
        self.reference = nil;
        self.mainKey    = @"";
    }
    
    return self;
}

- (id)initViewDefaultNibWithReference:(id)reference
{
    self = [self initWithNibName:@"listViewController" bundle:nil];
    self.modalPresentationStyle = UIModalPresentationFormSheet;
    self.reference = reference;
    
    return self;
}


- (id)initViewAsFechaUltimaVisita:(id)reference
{
    //where_fecha = fecha + " = (select mv1.fecha from med_visitas mv1 where mv1.id = mv.id and mv1.nodo = mv.nodo and mv1.medico = mv.medico) ";
    
    // " *mFecha_seleccionada* " = (select substr(strftime('%m/%d/%Y', max(mv1.fecha), 'unixepoch', 'localtime'),1,10)
    //                              from med_visitas mv1
    //                              where mv1.id = mv.id and mv1.nodo = mv.nodo and mv1.medico = mv.medico) ";
    
    self = [self initViewDefaultNibWithReference:reference];
    self.title = @"Fecha de última visita";
    
    self.allObjects = [[NSMutableArray alloc] init];
    
    
    return self;
}

- (id)initViewAsEspecialidadesWithReference:(id)reference
{
    self = [self initViewDefaultNibWithReference:reference];
    self.title = @"Especialidades";
    self.mainKey = @"recID";
    
    self.allObjects = [Especialidad GetAll];
    
    return self;
}

- (id)initViewAsPotencialesWithReference:(id)reference
{
    self = [self initViewDefaultNibWithReference:reference];
    self.title = @"Potencial";
    self.mainKey = @"recID";

    self.allObjects = [Potencial GetAll];
    
    return self;
}

- (id)initViewAsObrasSocialesWithReference:(id)reference
{
    self = [self initViewDefaultNibWithReference:reference];
    self.title = [[MTRegionalDictionary sharedInstance] valueForText:@"Obra Social"];
    self.mainKey = @"idObraSocial";
    
    self.allObjects = [ObraSocial GetAll];
    
    return self;
}


#pragma mark -
#pragma mark View methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.lblTitulo.text = self.title;

    // Asigno los colores por defecto de los botones
    self.btnAdd.tintColor       = UIColorFromRGB(agregarButtonColor);
    self.btnAceptar.tintColor   = UIColorFromRGB(aceptarButtonColor);
    self.btnEdit.tintColor      = UIColorFromRGB(editarButtonColor);
    self.btnCancelar.tintColor  = UIColorFromRGB(cancelarButtonColor);
    
    
    newList = [NSMutableArray arrayWithArray:self.listContent];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissView
{
    [self respondToDelegate];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    
    self.lblTitulo.text = title;
}


#pragma mark -
#pragma mark ObjectList methods

-(void)setObjectsList:(NSMutableArray*)oArray
{
    self.listContent = oArray;
}

-(void)showAddCombo:(id)sender
{
    // Obtengo la vista desde la cual se llama al popover.
    CGSize  popOverSize = CGSizeMake(350, 500);
    //self.btnAdd.
    
    // Creo el popover del menú.
    if (!self.popMenu)
    {
        self.popMenu = [[MTpopListView alloc] initWithArray:self.allObjects Rect:CGRectMake(0, 0, popOverSize.width, popOverSize.height)];
        self.popMenu.delegate = self;
    }
    [self.popMenu dismissPopoverAnimated:NO];
    
    [self.popMenu setTitleAndParameters:@"Seleccione un ítem" rowData:self.allObjects];
    [self.popMenu setSize:popOverSize];
    [self.popMenu useSearch:TRUE];
    
    CGRect popSourceRect;
    //CGRect buttonFrame = [self barButtonFrame:self.btnAdd];
    CGRect buttonFrame = self.btnAdd.frame;
    
    popSourceRect = CGRectMake(buttonFrame.origin.x,
                               buttonFrame.origin.y,
                               buttonFrame.size.width,
                               buttonFrame.size.height);
    [self.popMenu presentPopoverFromRectWithSender:popSourceRect
                                            Sender:(id)self.tableView
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionUp
                                          animated:YES];
}

- (CGRect) barButtonFrame: (UIBarButtonItem *) aButton
{
    UIView *view = [aButton valueForKey:@"view"];
    CGRect frame = CGRectZero;
    
    if (view)
    {
        frame = [view frame];
        frame.origin.x = self.toolbar.viewForBaselineLayout.frame.origin.x + frame.origin.x;
        frame.origin.y = self.toolbar.viewForBaselineLayout.frame.origin.y + frame.origin.y;
    }

    return frame;
}

- (BOOL)existeObjecto:(id)o
{
    BOOL result = NO;
    
    if ([self.mainKey length] > 0)
    {
        // obtengo la propiedad mainKey del objeto.
        id idOKey = [o valueForKey:self.mainKey];
//        NSString  *sOKey = idOKey;
        NSObject  *iOKey = idOKey;
        
        for (id p in newList)
        {
            id idPKey = [p valueForKey:self.mainKey];
//            NSString  *sPKey = idPKey;
            NSObject  *iPKey = idPKey;
           
//            if ([idOKey isKindOfClass:[NSString class]])
//            {
//                if ([sOKey isEqualToString:sPKey])
//                   return YES;
//            } else
//            {
                if ([iOKey.description isEqual:iPKey.description])
                    return YES;
//            }
        }
    } else
    {
        NSObject *oObject = o;
        for (NSObject *pObject in newList)
        {
            if ([oObject isEqual:pObject])
                return YES;
        }
        
        if ([newList indexOfObject:o] != NSNotFound)
            return YES;
    }
    
    
    return result;
}


#pragma mark -
#pragma mark Buttons methods

- (IBAction)tapAceptar:(id)sender
{
    // Si se tocó aceptar, agrego los objetos de la nueva lista.
    //self.listContent = newList;
    [self.listContent removeAllObjects];
    
    for (id o in newList)
         [self.listContent addObject:o];
    
    [self dismissView];
}

- (IBAction)tapCancelar:(id)sender
{
    // Si se tocó cancelar, devuelvo la misma lista con la que se inicializó.
    [self dismissView];
}

- (IBAction)tapEdit:(id)sender
{
    if (self.tableView.editing == NO)
    {
        [self.tableView setEditing:YES animated:YES];
        self.btnEdit.tintColor = UIColorFromRGB(defaultEditModeButtonColor);
        
        self.btnAceptar.enabled  = NO;
        self.btnCancelar.enabled = NO;
        self.btnAdd.enabled      = NO;
        
    } else
    {
        [self.tableView setEditing:NO animated:YES];
        self.btnEdit.tintColor = UIColorFromRGB(editarButtonColor);
        
        self.btnAceptar.enabled  = YES;
        self.btnCancelar.enabled = YES;
        self.btnAdd.enabled      = YES;
    }
}

- (IBAction)tapAdd:(id)sender
{
    [self showAddCombo:sender];
}

#pragma mark -
#pragma mark Table delegate and data source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [newList count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    id o = [newList objectAtIndex:indexPath.row];
    cell.textLabel.text = @"";
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    if ([o respondsToSelector:@selector(getDescription)])
        cell.textLabel.text = [NSString string:[o getDescription] ifNull:@""];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [newList removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
    id o = [newList objectAtIndex:fromIndexPath.row];
    
    [newList removeObjectAtIndex:fromIndexPath.row];
    [newList insertObject:o atIndex:toIndexPath.row];
    
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // nada
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark -
#pragma mark popListView delegate methods

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender didSelectRow:(NSInteger *)MenuIndex selectedText:(NSString *)RowText
{
//    id o = [self.allObjects objectAtIndex:MenuIndex];
//    
//    // Antes de agregar el objeto a la lista, chequeo que no exista.
//    if ([newList indexOfObject:o] == NSNotFound)
//    {
//        [newList addObject:[self.allObjects objectAtIndex:MenuIndex]];
//        [self.tableView reloadData];
//    }
}

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender selObject:(id)selObject
{
    // Antes de agregar el objeto a la lista, chequeo que no exista.
    //if ([newList indexOfObject:selObject] == NSNotFound)
    if (![self existeObjecto:selObject])
    {
        [newList addObject:selObject];
        [self.tableView reloadData];
    }
}



#pragma mark -
#pragma mark Protocol method
- (void)respondToDelegate
{
    // Chequeo si el delegado responde al selector del protocolo. Si es así, lo llamo.
    if ([self.delegate respondsToSelector:@selector(listViewController:referenceControl:selectedObjects:)])
        [self.delegate listViewController:self referenceControl:self.reference selectedObjects:self.listContent];
    
}


// En vistas que se presentan de forma modal es necesario agregar este override para que oculte el teclado
// con resignFirstResponder
- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

@end
