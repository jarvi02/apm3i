//
//  Visita.m
//  APM4
//
//  Created by Juan Pablo Garcia on 11/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Visita.h"
#import "DB.h"
#import "Config.h"
#import "NSDate+extensions.h"

@implementation Visita

+(NSUInteger)lastId:(NSUInteger)idMedico;
{
    NSString *sql = @"select max(id) as id from med_visitas where nodo = %@ and medico = %d and visitador = %@";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql, nodo, idMedico, nodo];
    
    NSUInteger last = 0;
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
            last = sqlite3_column_int(statement, 0);
        }
    }
    
    sqlite3_finalize(statement);

    return last + 1;
}

+(BOOL)validarVisitaExiste:(NSUInteger)idMedico fechaVisita:(NSDate *)fechaVisita fechaOriginal:(NSDate *)fechaOriginal
{
    
    
    NSString *sql = @"select id "
                     "from med_visitas "
                     "where nodo = %@ and medico = %d and visitador = %@ and fecha = %.0f and (1 = %@ or fecha <> %.0f) and tipo = 1";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       idMedico,
                       nodo,
                       [fechaVisita timeIntervalSince1970],
                       fechaOriginal != nil ? @"0" : @"1",
                       fechaOriginal != nil ? [[fechaOriginal dateAsDateWithoutTime] timeIntervalSince1970] :
                                              [[fechaVisita dateAsDateWithoutTime] timeIntervalSince1970]];
#ifdef DEBUG_VISITA
    NSLog(@"- Visitas: validarVisitaExiste -");
    NSLog(@"- query: %@",query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    BOOL result = YES;
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
            result = NO;
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
    
}

+(BOOL)validarFechaDeVisista:(NSUInteger)idMedico fechaVisita:(NSDate *)fechaVisita fechaOriginal:(NSDate *)fechaOriginal
{
    //NSString *sql = @"select id from med_visitas where nodo = %@ and medico = %d and visitador = %@ and fecha >= %.0f and (1 = %@ or fecha <> %.0f)";
    
    NSString *sql = @"SELECT id "
                "FROM med_visitas "
                "WHERE nodo = %@ AND "
                    "medico = %d AND "
                    "visitador = %@ AND "
                    "fecha > %.0f AND "
                    "(1 = %@ OR fecha <> %.0f) AND "
                    "tipo = 1";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql,
                       nodo,
                       idMedico,
                       nodo,
                       [[fechaVisita dateAsDateWithoutTime] timeIntervalSince1970],
                       fechaOriginal != nil ? @"0" : @"1",
                       fechaOriginal != nil ? [[fechaOriginal dateAsDateWithoutTime] timeIntervalSince1970] :
                                              [[fechaVisita dateAsDateWithoutTime] timeIntervalSince1970]];
#ifdef DEBUG_VISITA
    NSLog(@"- Visita: validarFechaDeVisista -");
    NSLog(@"%@",query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    BOOL result = YES;
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
            result = NO;
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
}

+(NSString *)getForNovedad:(NSUInteger)visita medico:(NSUInteger)medico
{
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *FormatoFecha = @"%m/%d/%Y %H:%M";
    
    NSString *sql = @"select nodo || '|' || ifnull(medico,\"\") || '|' || ifnull(visitador,\"\") || '|' || id || '|' ||ifnull(strftime('%@', fecha,'unixepoch','localtime'), \"\") || '|' || ifnull(tipo,\"\") || '|' || ifnull(domicilio,\"\") || '|' || ifnull(objetivos, \"\") || '|' || ifnull(evaluacion, \"\")  from med_visitas where nodo  = %@ and id = %d and medico = %d";
    
         
    NSString *query = [NSString stringWithFormat:sql, FormatoFecha, nodo, visita, medico];
    
#ifdef DEBUG_VISITA
    NSLog(@"- Visita: getForNovedad -");
    NSLog(@"%@",query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    char *_c;
    NSString *result = @"";
	if ( statement ) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            _c = (char *)sqlite3_column_text(statement, 0);
            if((_c != nil) && (strlen(_c) > 0)) {
                result = [NSString stringWithUTF8String:_c];
            }
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;

}

+(void)save:(Visita *)visita
{
    
    NSString *sql = @"insert into med_visitas "
        "(nodo, medico, visitador, id, fecha, tipo, domicilio, objetivos, evaluacion) "
        "values (%@, %d, %@, %d, %.0f, %d, %d, %@, %@)";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql,
                            nodo,
                            visita.idMedico,
                            nodo,
                            visita.idVisita,
                            [[visita.fecha dateAsDateWithoutTime] timeIntervalSince1970 ] ,
                            visita.idTipo,
                            visita.idDomicilio,
                            [NSString stringQuoted:visita.objetivos ifNull:@"''"],
                            [NSString stringQuoted:visita.evaluacion ifNull:@"''"]];
#ifdef DEBUG_VISITA
    NSLog(@"- Visitas: save -");
    NSLog(@"%@",query);
#endif
    
    [[DB getInstance] excecuteSQL:query];
    
}

+(void)update:(Visita *)visita
{
    
    NSString *sql = @"UPDATE med_visitas "
                     "SET fecha = %.0f, "
                         "tipo = %d, "
                         "objetivos = \"%@\", "
                         "evaluacion = \"%@\" "
                     "WHERE nodo = %@ and medico = %d and visitador = %@ and id = %d";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql,
                        [[visita.fecha dateAsDateWithoutTime] timeIntervalSince1970],
                        visita.idTipo,
                        [NSString string:visita.objetivos ifNull:@"" ],
                        [NSString string:visita.evaluacion ifNull:@"" ],
                        nodo,
                        visita.idMedico,
                        nodo,
                        visita.idVisita];
    
    [[DB getInstance] excecuteSQL:query];
    
}

+(Visita *)getByID:(NSUInteger)idVisita idMedico:(NSUInteger)idMedico
{
    /*NSString *sql = @"select a.medico, a.fecha, a.tipo, a.domicilio, a.objetivos, a.evaluacion, b.descripcion, a.id FROM med_visitas a, mtc_tiposvisita b where a.tipo = b.id and a.id = %d and nodo = %@ and medico = %d";*/
   
    
    NSString *sql =
    @"SELECT "
        "a.medico, "
        "a.fecha, "
        "a.tipo, "
        "a.domicilio, "
        "a.objetivos, "
        "a.evaluacion, "
        "b.descripcion, "
        "a.id, "
        "b.estado, "
        "b.contacto "
    "FROM med_visitas a JOIN  "
         "mtc_tiposvisita  b on (a.tipo = b.id) "
    "WHERE a.id = %d and nodo = %@ and medico = %d";
    
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *query = [NSString stringWithFormat:sql, idVisita, nodo, idMedico];
    
#ifdef DEBUG_VISITA
    NSLog(@"- Visitas: getByID -");
    NSLog(@"   query: %@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    Visita *v = [[Visita alloc] init];
    
    char *_c;

	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW) {
            
            v.idMedico = sqlite3_column_int(statement, 0);
            
            double val = sqlite3_column_double(statement, 1);
            v.fecha = [[NSDate dateWithTimeIntervalSince1970:val] dateAsDateWithoutTime];

#ifdef DEBUG_VISITA
            NSLog(@"Fecha en base datos %.0lf  fecha del date %.0lf", val, [v.fecha timeIntervalSince1970]);
#endif
            
            
            v.idTipo = sqlite3_column_int(statement, 2);
            
            v.idDomicilio = sqlite3_column_int(statement, 3);
            
            _c = (char *)sqlite3_column_text(statement, 4);
            v.objetivos = [NSString pCharToString:_c];
			
            _c = (char *)sqlite3_column_text(statement, 5);
            v.evaluacion = [NSString pCharToString:_c];
			
            _c = (char *)sqlite3_column_text(statement, 6);
            v.descripcionTipo = [NSString pCharToString:_c];
			         
            v.idVisita = sqlite3_column_int(statement, 7);
            
            // b.estado
            v.idEstado = sqlite3_column_int(statement, 8);
            
            // b.contacto
            v.contacto = sqlite3_column_int(statement, 9);
        }
    }
    
    sqlite3_finalize(statement);
    
    return [v autorelease];

}

-(void)update
{
    NSString *sql = @"update med_visitas set fecha = %.0f, tipo = %d, domicilio = %d, objetivos = %@, evaluacion = %@ where id = %d";

    
    NSString *query = [NSString stringWithFormat:sql, [[self.fecha dateAsDateWithoutTime] timeIntervalSince1970], self.idTipo, self.idDomicilio, self.objetivos, self.evaluacion, self.idVisita];
    
    [[DB getInstance] excecuteSQL:query];

}

-(void)dealloc
{
    [_fecha release];
    [_descripcionTipo release];
    [_domicilio release];
    [_objetivos release];
    [_evaluacion release];
    
    [super dealloc];
}


+(void)addVisitaFamilia:(int) familiaId Visita:(int)visitaId nodoId:(int)nodoId medicoId:(int)medicoId andOrden:(int)orden{
   
    NSString *sql = @"insert into med_visita_familia  "
    "(nodo_id, medico_id, visita_id, familia_id, orden) "
    "values (%d, %d, %d, %d, %d)";
     
    NSString *query = [NSString stringWithFormat:sql,
                       nodoId,
                       medicoId,
                       visitaId,
                       familiaId,
                       orden];
#ifdef DEBUG_VISITA
    NSLog(@"- Visitas-Familia-Orden: save -");
    NSLog(@"%@",query);
#endif
    
    [[DB getInstance] excecuteSQL:query];

}

@end
