//
//  MedicosVisitadosPorFecha.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MedicosVisitadosPorFecha.h"
#import "Config.h"
#import "DB.h"
#import "NSDate+extensions.h"

@implementation MedicosVisitadosPorFecha

-(id)init{
    
    self = [super init];
    if (self) {
        self.apellidoNombre = @"";
        self.tipoVisita = @"";
        self.actividad = @"";
    }
    return self;
}


-(void)dealloc{
    
    [_apellidoNombre release];
    [_tipoVisita release];
    [_actividad release];
    
    [super dealloc];
}

+(NSArray*) GetAllWithFilter:(NSInteger)filtro {
  
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    // para solucionar el tema de los repetidos, que se debian a que las fechas estaban con horas, fuerzo a que me traiga todo, poniendo 1=1 y filtro cuando cargo el array si filtro es distinto de 1
    NSString *sql =
    @"SELECT "
        "1 as _id, "
        "vis.nodo, "
        "vis.medico, "
        "vis.fecha as fechadisplay, "
        "med.apellido || ', ' || med.nombre as apenom, "
        "tipo.descripcion as tipovis, "
        "case when med.actividad = 'M' then 'Mañana' "
            "else 'Tarde' end "
            "as actividad "
    "FROM med_visitas vis "
        "INNER JOIN med_medicos med ON (vis.nodo=med.nodo AND vis.medico=med.id) "
        "INNER JOIN mtc_tiposvisita tipo ON (vis.tipo = tipo.id) "
    "WHERE ((1=%d or fechadisplay = '%d')  and tipo.contacto = 1 AND vis.nodo = %@) "
    "ORDER BY vis.fecha, actividad, apenom;";
    
    NSString *nodo = [[Config shareInstance] find:NODO];
    
    NSString *query = [NSString stringWithFormat:sql, 1, 1, nodo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
	if ( statement ) {
        
        MedicosVisitadosPorFecha *o;
        BOOL flag;
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
            
        
            flag = YES;
            if (filtro != 1)
            {
                flag = YES;
                NSDate *filtroDate= [[NSDate dateWithTimeIntervalSince1970:filtro] dateAsDateWithoutTime];
                NSDate *registroDate = [[NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 3)] dateAsDateWithoutTime];
                if ( [filtroDate isEqualToDate:registroDate] ) {
                    flag = YES;
                }
                else
                {
                    flag = NO;
                }
                
            }
            
            if ((filtro == 1) || flag)
            {
                o = [[MedicosVisitadosPorFecha alloc] init];
                
                o.idMedicosVisitadosPorFecha = sqlite3_column_int(statement, 0);
                o.nodo = sqlite3_column_int(statement, 1);
                o.medico = sqlite3_column_int(statement, 2);
                o.fechaDisplay = sqlite3_column_int(statement, 3);
                
                char *_an = (char *)sqlite3_column_text(statement, 4);
                o.apellidoNombre = [NSString pCharToString:_an];
                
                char *_tv = (char *)sqlite3_column_text(statement, 5);
                o.tipoVisita = [NSString pCharToString:_tv];
                
                char *_a = (char *)sqlite3_column_text(statement, 6);
                o.actividad = [NSString pCharToString:_a];
                
                [array addObject:o];
                [o release];
            }
            
        }
        
	}
    else
    {
        NSAssert(0, @"No se puede ejecutar: %@", query);
	}
	sqlite3_finalize(statement);
    
    return [array autorelease];
    
    
}
@end
