//
//  DomicilioViewController.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "DomicilioViewController.h"
#import "HorarioViewController.h"
#import "TelefonoViewController.h"
#import "Tipo.h"
#import "Institucion.h"
#import "Cargo.h"
#import "Provincia.h"
#import "Localidad.h"
#import "Utilidad.h"
#import "Domicilio.h"
#import "Calle.h"
#import "MTEditActions.h"
#import "Config.h"
#import "DB.h"
#import "Telefono.h"
#import "Horario.h"
#import "popListExtended.h"

#import "NSStringExtended.h"


// Tags de los controles de la vista
#define TAG_btnTIPO         1001
#define TAG_btnINSTITUCION  1002
#define TAG_btnCARGO        1003
#define TAG_btnPROVINCIA    1004
#define TAG_btnLOCALIDAD    1005
#define TAG_btnCALLE        666
#define TAG_btnUTILIDAD     1006
#define TAG_txtCALLE        668
#define TAG_txtNUMERO       667
#define TAG_txtPISO         669
#define TAG_txtDPTO         670



@interface DomicilioViewController ()
@property(nonatomic, retain) NSArray *tipos;
@property(nonatomic, retain) NSArray *instituciones;
@property(nonatomic, retain) NSArray *cargos;
@property(nonatomic, retain) NSArray *provincias;
@property(nonatomic, retain) NSArray *localidades;
@property(nonatomic, retain) NSArray *calles;
@property(nonatomic, retain) NSArray *utilidades;

@property(nonatomic, retain) NSMutableArray *domiciliosOriginal;
@property(nonatomic, retain) Domicilio  *domicilioAux;

-(void)crearToolBar;
-(NSString*)buscarCP;

@end

@implementation DomicilioViewController


- (void)dealloc {
    
    [_domiciliosOriginal release];
    [_domicilioAux release];
    
    [_tipos release];
    [_instituciones release];
    [_cargos release];
    [_provincias release];
    [_localidades release];
    [_calles release];
    [_utilidades release];
    
    [_txtTipo release];
    [_txtInstitucional release];
    [_txtCargo release];
    [_txtProvincia release];
    [_txtLocalidad release];
    [_txtCalle release];
    [_txtNumero release];
    [_txtCP release];
    [_txtPiso release];
    [_txtDto release];
    [_txtUtilidad release];
    [_txtTelefono release];
    [_txtHorario release];
    [_tableView release];
    [_toolBar release];
    [_scrollView release];
    [_btnConsulrotioExterno release];
    [_lblProvincia release];
    [_lblLocalidad release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andMode:(MTEditActionsType) modo
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if (modo == kMTEAAdd) {
            self.title = @"Alta";
        } else {
            self.title = @"Modificación";
        }
        
    }
    return self;
}

-(NSString *)getTitleForHeader{
    
    return self.title;
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"user_add"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self crearToolBar];

    // Regionalización
    self.lblLocalidad.text = [[MTRegionalDictionary sharedInstance] valueForText:@"Localidad"];
    self.lblProvincia.text = [[MTRegionalDictionary sharedInstance] valueForText:@"Provincia"];
    
    
    //self.domicilioAux = [[[Domicilio alloc] init] autorelease];
    
    self.domiciliosOriginal = [NSMutableArray array];
    for (Domicilio *o in self.abmMedicoReference.medico.domicilios) {
        [self.domiciliosOriginal addObject:[o copy]];
    }
    
    self.tipos = [Tipo GetAll];
    self.cargos = [Cargo GetAll];
    self.provincias = [Provincia GetAll];
    
    [self limpiar:nil];
    
//    Importante : Si es el primer domicilio que se carga, el campo UTILIDAD es siempre de VISITA y no se puede modificar.
//    En los posteriores se puede modificar pero no se puede elegir visita
    if ([self.abmMedicoReference.medico.domicilios count] == 0) {
        NSArray *arr =[Utilidad GetAll] ;
        Utilidad *u = [arr objectAtIndex:0];
        self.utilidades = [NSArray arrayWithObjects:u,nil];
        self.domicilioAux.utilidad = [u copy];
        self.txtUtilidad.text = [NSString stringWithFormat:@"%@", u.descripcion];
    } else {
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Utilidad GetAll]];
        [arr removeObjectAtIndex:0];
        self.utilidades = arr;
    }
        
}

-(void)crearToolBar {
    
    // flex item used to separate the left groups items and right grouped items
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    // create a special tab bar item with a custom image and title
    UIBarButtonItem *guardarItem = [[UIBarButtonItem alloc] initWithTitle:@"Guardar"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(guardar:)];
    //guardarItem.tintColor = [UIColor grayColor];
    guardarItem.width = 70;
    
    UIBarButtonItem *editarItem = [[UIBarButtonItem alloc] initWithTitle:@"Editar"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(editar:)];
    //editarItem.tintColor = [UIColor grayColor];
    editarItem.width = 70;
    
    UIBarButtonItem *cancelarItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar"
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self
                                                                    action:@selector(cancelar:)];
    //cancelarItem.tintColor = [UIColor grayColor];
    cancelarItem.width = 70;
    
    UIBarButtonItem *agregarItem = [[UIBarButtonItem alloc] initWithTitle:@"Agregar"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(agregar:)];
    // agregarItem.tintColor = [UIColor grayColor];
    // orchid = d29Ce2
    //agregarItem.tintColor = [UIColor colorWithRed:0xd2/255.0f green:0x9c/255.0f blue:0xe2/255.0f alpha:1];
    agregarItem.width = 70;
    
    
    UIBarButtonItem *limpiarItem = [[UIBarButtonItem alloc] initWithTitle:@"Limpiar"
                                                                    style:UIBarButtonItemStyleBordered
                                                                   target:self
                                                                   action:@selector(limpiar:)];
    //limpiarItem.tintColor = [UIColor lightGrayColor];
    limpiarItem.width = 70;
    
    
    NSArray *items = [NSArray arrayWithObjects: guardarItem, editarItem, cancelarItem, flexItem, limpiarItem, agregarItem, nil];
    [self.toolBar setItems:items animated:NO];
    
    
    [flexItem release];
    [guardarItem release];
    [cancelarItem release];
    [editarItem release];
    [agregarItem release];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) closeModalView {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark Tap methods
-(void)guardar:(id)sender{
    
    BOOL pierdeDatosIngresados = NO;
    
    // Chequeo si el domicilio seleccionado al presionar guardar contiene datos válidos y si existe un domicilio de visita.
    NSInteger cantDomiciliosMedico = [self.abmMedicoReference.medico.domicilios count];
    if (cantDomiciliosMedico == 0)
    {
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                       message:@"Debe definir un domicilio de visita"
                                                      delegate:nil
                                             cancelButtonTitle:@"Aceptar"
                                             otherButtonTitles:nil];
        [aler show];
        [aler release];
        self.abmMedicoReference.txtDomicilio.text = @"";
        self.abmMedicoReference.txtHorario.text = @"";
        return;
        
    } else
    {
        // Domicilio Seleccionado.
        // Busco si el domicilio seleccionado está en la lista de domicilios.
        if (self.domicilioAux.tipo.idTipo > 0)
            pierdeDatosIngresados = YES;
        for (NSInteger dCount = 0; dCount < cantDomiciliosMedico; dCount++)
        {
            Domicilio *d = [self.abmMedicoReference.medico.domicilios objectAtIndex:dCount];
            // Si el domicilio seleccionado es igual a alguno de los domicilios del listado, chequeo que los datos sean correctos.
            if (d == self.domicilioAux)
            {
                pierdeDatosIngresados = NO;
                if (!([self checkDomicilioData:d]))
                {
                    return;
                }
            } else
            {
                
            }
        }
        
        // Chequeo si los datos ingresados se perderán.
        if (pierdeDatosIngresados)
        {
            UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                           message:@"Si continúa perderá los datos del domicilio que no agregó.\nPara mantener los cambios realizados seleccione No y luego toque el botón Agregar.\n\n¿Desea continuar de todas formas?"
                                                          delegate:self
                                                 cancelButtonTitle:@"No"
                                                 otherButtonTitles:@"Si",nil];
            [aler show];
            [aler release];
        } else
            [self finishGuardar];
    
    } 
    
}

-(void)finishGuardar
{
    // Dimicilio de Visita.
    // Chequeo si se definió un domicilio de Visita.
    Domicilio *o = [self buscarDomicilioPrincial];
    
    // Si no hay definodo un domicilio prinpal, tengo que obligar a que se defina.
    if (o)
    {
        // Si existe el domicilio principal, chequeo que los datos sean correctos.
        if (!([self checkDomicilioData:o]))
            return;
        
        self.abmMedicoReference.txtDomicilio.text = o.getDescripcion;
    } else
    {
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                       message:@"Debe definir un domicilio de visita"
                                                      delegate:nil
                                             cancelButtonTitle:@"Aceptar"
                                             otherButtonTitles:nil];
        [aler show];
        [aler release];
        return;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)cancelar:(id)sender{
    
    [self.abmMedicoReference.medico.domicilios removeAllObjects];
    
    [self.abmMedicoReference.medico.domicilios addObjectsFromArray:self.domiciliosOriginal];
    
    Domicilio *o = [self buscarDomicilioPrincial];
    if (!o)
    {
        self.abmMedicoReference.txtDomicilio.text = @"";
        self.abmMedicoReference.txtHorario.text = @"";
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)editar:(id)sender{
    
    
    if (self.tableView.editing == NO) {
        [self.tableView setEditing:YES animated:YES];
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:1]).tintColor = UIColorFromRGB(defaultEditModeButtonColor);
        
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:0]).enabled =NO;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:2]).enabled =NO;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:4]).enabled =NO;
        
        
    } else {
        [self.tableView setEditing:NO animated:YES];
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:1]).tintColor = ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:2]).tintColor;
        
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:0]).enabled =YES;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:2]).enabled =YES;
        ((UIBarButtonItem *)[self.toolBar.items objectAtIndex:4]).enabled =YES;
        
    }
    
    
}

-(void)agregar:(id)sender
{
    
    Domicilio *dom = self.domicilioAux;
    
    // Chequeo que no exista domicilio de visita si se esta ingresando uno.
    Domicilio *o = [self buscarDomicilioPrincial];
    if ((o) && (dom.utilidad.idUtilidad == 1))
    {
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                       message:@"Ya existe un domicilio de visita"
                                                      delegate:nil
                                             cancelButtonTitle:@"Aceptar"
                                             otherButtonTitles:nil];
        [aler show];
        [aler release];
        return;
    }
    
    if (![self checkDomicilioData:dom])
    {
        //[dom release];
        return;
    }
    
    dom = [self.domicilioAux copy];
    
     [self.abmMedicoReference.medico.domicilios addObject:dom];
    

    
    [self limpiar:sender];
    
//     self.domicilioAux = [[[Domicilio alloc] init] autorelease];
//    
//    ((UIButton*)[self.view viewWithTag:TAG_btnINSTITUCION]).enabled = NO;
//    ((UIButton*)[self.view viewWithTag:TAG_btnCARGO]).enabled       = NO;
//    ((UIButton*)[self.view viewWithTag:TAG_btnPROVINCIA]).enabled   = NO;
//    ((UIButton*)[self.view viewWithTag:TAG_btnLOCALIDAD]).enabled   = NO;
//    ((UIButton*)[self.view viewWithTag:TAG_btnCALLE]).enabled       = NO;
//    if ([self.abmMedicoReference.medico.domicilios count] > 0)
//        ((UIButton*)[self.view viewWithTag:TAG_btnUTILIDAD]).enabled = YES;
//    else
//        ((UIButton*)[self.view viewWithTag:TAG_btnUTILIDAD]).enabled = NO;
//    
//    
//    self.txtTipo.text = @"";
//    self.txtInstitucional.text = @"";
//    self.txtCargo.text = @"";
//    self.txtProvincia.text = @"";
//    self.txtLocalidad.text = @"";
//    self.txtUtilidad.text = @"";
//    self.txtCalle.text = @"";
//    self.txtNumero.text = @"";
//    self.txtPiso.text = @"";
//    self.txtDto.text = @"";
//    self.txtCP.text = @"";
//    self.txtTelefono.text = @"";
//    self.txtHorario.text = @"";
//    
//    self.txtCalle.enabled = YES;
//    self.txtNumero.enabled = YES;
//    self.txtPiso.enabled = YES;
//    self.txtDto.enabled = YES;
//    
//    [self.tableView reloadData];
}

-(void)limpiar:(id)sender{
    
    self.domicilioAux = [[[Domicilio alloc] init] autorelease];
    
    
    ((UIButton*)[self.view viewWithTag:TAG_btnINSTITUCION]).enabled = NO;
    ((UIButton*)[self.view viewWithTag:TAG_btnCARGO]).enabled       = NO;
    ((UIButton*)[self.view viewWithTag:TAG_btnPROVINCIA]).enabled   = NO;
    ((UIButton*)[self.view viewWithTag:TAG_btnLOCALIDAD]).enabled   = NO;
    ((UIButton*)[self.view viewWithTag:TAG_btnCALLE]).enabled       = NO;
    
    self.txtTipo.text = @"";
    self.txtInstitucional.text = @"";
    self.txtCargo.text = @"";
    self.txtProvincia.text = @"";
    self.txtLocalidad.text = @"";
    self.txtUtilidad.text = @"";
    self.txtCalle.text = @"";
    self.txtNumero.text = @"";
    self.txtPiso.text = @"";
    self.txtDto.text = @"";
    self.txtCP.text = @"";
    self.txtTelefono.text = @"";
    self.txtHorario.text = @"";
    
    self.txtCalle.enabled = YES;
    self.txtNumero.enabled = YES;
    self.txtPiso.enabled = YES;
    self.txtDto.enabled = YES;
    
    if ([self.abmMedicoReference.medico.domicilios count] > 0)
        ((UIButton*)[self.view viewWithTag:TAG_btnUTILIDAD]).enabled = YES;
    else
    {
        ((UIButton*)[self.view viewWithTag:TAG_btnUTILIDAD]).enabled = NO;
        Utilidad *util = [self.utilidades objectAtIndex:0];
        self.domicilioAux.utilidad = util;
        self.txtUtilidad.text = [NSString stringWithFormat:@"%@", util.descripcion];
    }
    
    [self.btnConsulrotioExterno setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
    self.btnConsulrotioExterno.tag = 0;
    
    [self.tableView reloadData];
}


#define TIPO @"Tipo"
- (IBAction)tipo:(id)sender {
    
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    [self showPop:sender withData:self.tipos withTitle:TIPO withSearch:NO];
    
}

#define INSTITUCIONAL @"Institucional"
- (IBAction)institucional:(id)sender {
    
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    
    if ([self.txtTipo.text length] > 0 ) {
        
        NSInteger i = 0;
        for (Tipo *o in self.tipos) {
            
            if ([o.descripcion isEqualToString:self.txtTipo.text]) {
                i = o.idTipo;
            }
        }
        
        self.instituciones = [Institucion GetAllByTipo:i];
        
    }
     
    [self showPop:sender withData:self.instituciones withTitle:INSTITUCIONAL withSearch:YES];
}

#define CARGO @"Cargo"
- (IBAction)cargo:(id)sender {
    
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    [self showPop:sender withData:self.cargos withTitle:CARGO withSearch:YES];
}

#define PROCINCIA [[MTRegionalDictionary sharedInstance] valueForText:@"Provincia"]
- (IBAction)provincia:(id)sender {
    
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
     
    [self showPop:sender withData:self.provincias withTitle:PROCINCIA withSearch:YES];
    
}

#define LOCALIDAD [[MTRegionalDictionary sharedInstance] valueForText:@"Localidad"]
- (IBAction)localidad:(id)sender {
    
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    
    if ([self.txtProvincia.text length] > 0 ) {
        
        NSInteger i = 0;
        for (Provincia *o in self.provincias) {
            
            if ([o.descripcion isEqualToString:self.txtProvincia.text]) {
                i = o.idProvincia;
            }
        }
        
        self.localidades = [Localidad GetAllByProvincia:i];
        
    }
       
    [self showPop:sender withData:self.localidades withTitle:LOCALIDAD withSearch:YES];
}



#define UTILIDAD @"Utilidad"
- (IBAction)utilidad:(id)sender {
    
    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    
    if ([self.abmMedicoReference.medico.domicilios count] > 0) {
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[Utilidad GetAll]];
        [arr removeObjectAtIndex:0];
        self.utilidades = arr;
    }
    
    [self showPop:sender withData:self.utilidades withTitle:UTILIDAD withSearch:NO];
    
}

#define CALLE @"Calle"
- (IBAction)calles:(id)sender {

    if (Delegate.popoverController.popoverVisible)
        [Delegate dismissPopOver:NO];
    
    NSInteger pais = [SettingForKey(PAIS, @"54") integerValue];
    
    self.calles = [Calle GetAllBy:pais provincia:self.domicilioAux.provincia.idProvincia localidad:self.domicilioAux.localidad.idLocalidad];
    
    [self showPop:sender withData:self.calles withTitle:CALLE withSearch:YES];
}


- (IBAction)consultorioExterno:(id)sender {
   
    if (self.btnConsulrotioExterno.tag) {
    
        self.domicilioAux.consultorioExterno = NO;
        [self.btnConsulrotioExterno setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
        self.btnConsulrotioExterno.tag = 0;
        
    } else {
        
        self.domicilioAux.consultorioExterno = YES;
        [self.btnConsulrotioExterno setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
        self.btnConsulrotioExterno.tag = 1;
    }

}

- (IBAction)telefono:(id)sender {
    
    TelefonoViewController *o = [[TelefonoViewController alloc] initWithNibName:@"TelefonoViewController" bundle:nil];
    o.modalPresentationStyle = UIModalPresentationFormSheet;
    o.domicilioViewControllerReference = self;
    o.domicilioReference = self.domicilioAux;
    o.delegate = self;
    [self presentViewController:o animated:YES completion:nil];
    
    [o release];
    
}

- (IBAction)horario:(id)sender {
    
    
    HorarioViewController *o = [[HorarioViewController alloc] initWithNibName:@"HorarioViewController" bundle:nil];
    o.modalPresentationStyle = UIModalPresentationFormSheet;
    o.abmMedicoReference = self.abmMedicoReference;
    o.domicilioViewControllerReference = self;
    o.domicilioReference = self.domicilioAux;
    o.delegate = self;
    [self presentViewController:o animated:YES completion:nil];
    
    [o release];
    
}



#pragma mark -
#pragma mark Combo delegates methods

-(void)selectedIndex:(NSInteger)index caller:(NSString*)caller {
    
    [Delegate dismissPopOver:YES];
    
    if ([caller isEqualToString:TIPO])
    {
        
        Tipo *o = [self.tipos objectAtIndex:index];
        self.domicilioAux.tipo = [o copy];
        self.txtTipo.text = [o descripcion];
        
        ((UIButton*)[self.view viewWithTag:1002]).enabled = YES;
        ((UIButton*)[self.view viewWithTag:1003]).enabled = YES;
        ((UIButton*)[self.view viewWithTag:1005]).enabled = YES;
        ((UIButton*)[self.view viewWithTag:1004]).enabled = YES;
        if ([self.abmMedicoReference.medico.domicilios count] > 0)
            ((UIButton*)[self.view viewWithTag:1006]).enabled = YES;
        else
            ((UIButton*)[self.view viewWithTag:1006]).enabled = NO;
        ((UIButton*)[self.view viewWithTag:666]).enabled = YES;
        
        self.domicilioAux.institucion = nil;
        self.domicilioAux.cargo = nil;
        self.domicilioAux.provincia = nil;
        self.domicilioAux.localidad = nil;
        
        if (self.domicilioAux.utilidad.idUtilidad != 1)
            self.domicilioAux.utilidad = nil;
        
        self.domicilioAux.calle = nil;
        self.domicilioAux.numero = 0;
        self.domicilioAux.piso = 0;
        self.domicilioAux.depto = nil;
        self.domicilioAux.codigoPostal = nil;
        
        self.txtInstitucional.text = @"";
        self.txtCargo.text = @"";
        self.txtProvincia.text = @"";
        self.txtLocalidad.text = @"";
        self.txtUtilidad.text = @"";
        self.txtCalle.text = @"";
        self.txtNumero.text = @"";
        self.txtPiso.text = @"";
        self.txtDto.text = @"";
        self.txtCP.text = @"";
        
        self.txtCalle.enabled = YES;
        self.txtNumero.enabled = YES;
        self.txtPiso.enabled = YES;
        self.txtDto.enabled = YES;
        
        /*
         1 - consultorio
         Si se selecciona “consultorio” se deshabilitan los desplegables de Institucion , Cargo y de Utilidad
         Si se selecciona otra opciones diferente se habilitan los desplegables de institución , Cargo y Utilidad.
         
         */
        if (self.domicilioAux.tipo.idTipo == 1)
        {
            ((UIButton*)[self.view viewWithTag:1002]).enabled = NO;
            ((UIButton*)[self.view viewWithTag:1003]).enabled = NO;
            //((UIButton*)[self.view viewWithTag:1006]).enabled = NO;
            
        }
        
        
        
        if (self.domicilioAux.utilidad.idUtilidad == 1)
        {
            self.domicilioAux.utilidad.descripcion = @"VISITA";
            //self.domicilioAux.utilidad = [[self.utilidades objectAtIndex:0] copy];
            self.txtUtilidad.text = self.domicilioAux.utilidad.descripcion;
            ((UIButton*)[self.view viewWithTag:1006]).enabled = NO;
        }

        
    } else if ( [caller isEqualToString:INSTITUCIONAL] ) {
        
        Institucion *o = [self.instituciones objectAtIndex:index];
        self.domicilioAux.institucion = [o copy];
        
        self.txtInstitucional.text = [o descripcion];
        
//        deshabilitando los desplegables de Provincia, Localidad, Calle, Numero, CP, Piso, Dto.  (el CP siempre esta deshabilitado)
        ((UIButton*)[self.view viewWithTag:1004]).enabled = NO;
        ((UIButton*)[self.view viewWithTag:1005]).enabled = NO;
        ((UIButton*)[self.view viewWithTag:666]).enabled = NO;
        self.txtCalle.enabled = NO;
        self.txtNumero.enabled = NO;
        self.txtPiso.enabled = NO;
        self.txtDto.enabled = NO;

//         Se deja habilitado el desplegable de CARGO.
        ((UIButton*)[self.view viewWithTag:1003]).enabled = YES;
        
//         Cuando se seleccione la institución , se toman los datos de Provincia, Localidad, Calle, Numero , Piso, dpto. que se traen mediante el query y se completan dichos campos en pantalla,

        // Provincia.
        for (Provincia *o in self.provincias)
        {
            
            if (o.idProvincia == self.domicilioAux.institucion.provincia) {
                self.domicilioAux.provincia = [o copy];
            }
        }
        self.txtProvincia.text = [NSString string:self.domicilioAux.provincia.descripcion ifNull:@""];
        
        // Localidad.
        self.localidades = [Localidad GetAllByProvincia:self.domicilioAux.institucion.provincia];
        for (Localidad *o in self.localidades) {
    
            if ( [o.descripcion isEqualToString:self.domicilioAux.institucion.localidad] ) {
                self.domicilioAux.localidad = [o copy];
            }
        }
        self.txtLocalidad.text = [NSString string:self.domicilioAux.localidad.descripcion ifNull:@""];
        
        // Calle.
        Calle *calle = [[[Calle alloc] init] autorelease];
        calle.descripcion = self.domicilioAux.institucion.calle;
        self.domicilioAux.calle = calle;
        self.txtCalle.text = [NSString string:self.domicilioAux.calle.descripcion ifNull:@""];
        
        
        
        self.domicilioAux.numero = self.domicilioAux.institucion.altura;
        self.txtNumero.text = [NSString stringWithFormat:@"%d", self.domicilioAux.numero];
        // el piso y depto no me la devuelve la query
        self.txtPiso.text = @"";
        self.txtDto.text = @"";
        
        self.domicilioAux.codigoPostal = self.domicilioAux.institucion.codigoPosta;
        self.txtCP.text = [NSString string:[self.domicilioAux getCpDescripcion] ifNull:@""];
        
        
    } else if ( [caller isEqualToString:CARGO] ) {
        
        Cargo *o = [self.cargos objectAtIndex:index];
        self.domicilioAux.cargo = [o copy];
        self.txtCargo.text = [o descripcion];
        
    } else if ( [caller isEqualToString:PROCINCIA] ) {
        
        Provincia *o = [self.provincias objectAtIndex:index];
        self.domicilioAux.provincia = [o copy];
        self.txtProvincia.text = [o descripcion];
        
        // Borro el resto de los datos dependientes de este.
        self.domicilioAux.localidad = nil;
        self.domicilioAux.calle = nil;
        self.domicilioAux.numero = 0;
        self.domicilioAux.piso = 0;
        self.domicilioAux.depto = @"";
        self.domicilioAux.codigoPostal = @"";
        self.txtLocalidad.text = @"";
        self.txtCalle.text = @"";
        self.txtNumero.text = @"";
        self.txtPiso.text = @"";
        self.txtDto.text = @"";
        self.txtCP.text = @"";
        
    } else if ( [caller isEqualToString:LOCALIDAD] )
    {
        
        Localidad *o = [self.localidades objectAtIndex:index];
        self.domicilioAux.localidad = [o copy];
        self.txtLocalidad.text = [o descripcion];
        
        // Borro el resto de los datos dependientes de este.
        self.domicilioAux.calle = nil;
        self.domicilioAux.numero = 0;
        self.domicilioAux.piso = 0;
        self.domicilioAux.depto = @"";
        self.domicilioAux.codigoPostal = @"";
        self.txtCalle.text = @"";
        self.txtNumero.text = @"";
        self.txtPiso.text = @"";
        self.txtDto.text = @"";
        self.txtCP.text = @"";
        
//        Cuando se selecciona una localidad , nos fijamos el campo calles que nos trajimos en el query a la geo_localidades y se evalua :
//        Si dicho campo tiene valor 1, callesestra un desplegable con las calles para esa provincia y localidad sino se muestra un campo de edición para que el usuario complete.
        if (self.domicilioAux.localidad.calles == 1)
        {
            
            self.txtCalle.enabled = NO;
            self.txtCalle.backgroundColor = [UIColor lightGrayColor];
            ((UIButton*)[self.view viewWithTag:666]).enabled = YES;
            
        } else
        {
            self.txtCalle.enabled = YES;
             self.txtCalle.backgroundColor = [UIColor whiteColor];
            ((UIButton*)[self.view viewWithTag:666]).enabled = NO;
            
            // Como la localidad no tiene calles, busco el código postal de la localidad.
            if ([self buscarCP] != nil)
            {
                self.txtCP.text = [self buscarCP];
                // Comento esta línea, porque en la rutina de búsqueda de código postal ya llena la propiedad con el valor real.
                //self.domicilioAux.codigoPostal = [self buscarCP];
            }
            
        }
        
        
                
    } else if ( [caller isEqualToString:UTILIDAD] ) {
        
        Utilidad *o = [self.utilidades objectAtIndex:index];
        
        /*
        SI PRIMER DOMICILIO
        CAMPO UTILIDAD = VISITA (no se peude tocar)
        SINO
        CAMPO UTILIDAD seleccionable menos la VISITA
        */
        
        // lo anterior lo puedo cambiar por lo siguiente,  si el domicilio que se queire cargar tiene campo utilidad = 1 osea es una VISITA, entonces no se lo cambio.
        
        if (self.domicilioAux.utilidad.idUtilidad != 1)
        {
            self.domicilioAux.utilidad = [o copy];
            self.txtUtilidad.text = [o descripcion];
        }
        
    } else if ( [caller isEqualToString:CALLE] ) {
        
        Calle *o = [self.calles objectAtIndex:index];
        self.txtCalle.text = o.descripcion;
        self.domicilioAux.calle = o;
        
        //blanqueo la altura cuando cambio la calle desde el combo
        self.domicilioAux.numero = 0;
        self.domicilioAux.piso = 0;
        self.domicilioAux.depto = nil;
        self.domicilioAux.codigoPostal = nil;
        
        self.txtNumero.text = @"";
        self.txtPiso.text = @"";
        self.txtDto.text = @"";
        self.txtCP.text = @"";
    
    }
    
    else {
        // jamas deberia entrar aca
    }
    
}


- (IBAction)editTxtNumero:(id)sender
{
//    if (([self.txtNumero.text integerValue] > 0) && (self.domicilioAux.localidad.calles == 1))
//    {
//    // Si la calle se selecciona de la lista, busco la el CP para la calle
//        NSString *sCP = nil;
//        sCP = [self buscarCPforCalle:self.domicilioAux.calle.idCalle Numero:[self.txtNumero.text integerValue]];
//        if (sCP!= nil)
//        {
//            self.txtCP.text = sCP;
//            self.domicilioAux.codigoPostal = sCP;
//        } else
//        {
//            // En el caso de no existir un código postal, borro el que aparecía.
//            self.txtCP.text = @"";
//            self.domicilioAux.codigoPostal = @"";
//            
//        }
//    }
}


#pragma - mark
#pragma UItableView Delegate and DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.abmMedicoReference.medico.domicilios count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    NSString *str = @"";
    
    if ([self.abmMedicoReference.medico.domicilios count] >= (indexPath.row+1))
    {
        
        Domicilio *o = [self.abmMedicoReference.medico.domicilios objectAtIndex:indexPath.row];
    
        str = o.getDescripcion;
    }
    
    cell.textLabel.text = str;
    cell.textLabel.textColor = UIColorFromRGB(defaultInteractiveCellFontColor);
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        // Chequeo si se trata del domicilio de visita. Si es así. No se puede eleiminar.
        Domicilio *d = [self.abmMedicoReference.medico.domicilios objectAtIndex:indexPath.row];
        if (d.utilidad.idUtilidad == 1)
        {
            UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                           message:@"No se puede eliminar el domicilio de visita"
                                                          delegate:nil
                                                 cancelButtonTitle:@"Aceptar"
                                                 otherButtonTitles:nil];
            [aler show];
            [aler release];
            return;
        } else
        {
            [self.abmMedicoReference.medico.domicilios removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
    Domicilio *d = nil;
    
    d = [self.abmMedicoReference.medico.domicilios objectAtIndex:fromIndexPath.row];
    d = [d copy];
    [self.abmMedicoReference.medico.domicilios removeObjectAtIndex:fromIndexPath.row];
    [self.abmMedicoReference.medico.domicilios insertObject:d atIndex:toIndexPath.row];
    
    
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row == 0)
//        return NO;
//    else
        return YES;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.domicilioAux = [self.abmMedicoReference.medico.domicilios objectAtIndex:indexPath.row];
    
    self.txtTipo.text = self.domicilioAux.tipo.descripcion;
    self.txtInstitucional.text  = self.domicilioAux.institucion.descripcion;
    self.txtCargo.text = self.domicilioAux.cargo.descripcion;
    self.txtProvincia.text = self.domicilioAux.provincia.descripcion;
    self.txtLocalidad.text = self.domicilioAux.localidad.descripcion;
    self.txtCalle.text = self.domicilioAux.calle.descripcion;
    self.txtNumero.text = [NSString stringWithFormat:@"%d", self.domicilioAux.numero];
    self.txtPiso.text = self.domicilioAux.piso >= 1 ? [NSString intToStr:self.domicilioAux.piso]: @"";
    self.txtDto.text = self.domicilioAux.depto;
    self.txtCP.text = [self.domicilioAux getCpDescripcion];
    self.txtUtilidad.text = self.domicilioAux.utilidad.descripcion;
    
    if (!self.domicilioAux.consultorioExterno)
    {
        
        [self.btnConsulrotioExterno setBackgroundImage:[UIImage imageNamed:@"ok_32_off"]  forState:UIControlStateNormal];
        self.btnConsulrotioExterno.tag = 0;
        
    } else
    {
    
        [self.btnConsulrotioExterno setBackgroundImage:[UIImage imageNamed:@"ok_32"]  forState:UIControlStateNormal];
        self.btnConsulrotioExterno.tag = 1;
    }
    
    self.txtTelefono.text = @"";
    if ([self.domicilioAux.telefonos count] > 0)
    {
        Telefono *tel = [self.domicilioAux.telefonos objectAtIndex:0];
        self.txtTelefono.text = [tel descripcion];
    }
    
    self.txtHorario.text = @"";
    if ([self.domicilioAux.horarios count] > 0)
    {
        Horario *hor = [self.domicilioAux.horarios objectAtIndex:0];
        self.txtHorario.text = [hor descripcion];
    }
    
    
    // Habilito los combos y textFields según corresponda al tipo de domicilio.
    self.txtCalle.enabled  = YES;
    self.txtNumero.enabled = YES;
    self.txtPiso.enabled   = YES;
    self.txtDto.enabled    = YES;
    ((UIButton*)[self.view viewWithTag:TAG_btnINSTITUCION]).enabled = YES;
    ((UIButton*)[self.view viewWithTag:TAG_btnCARGO]).enabled       = YES;
    ((UIButton*)[self.view viewWithTag:TAG_btnPROVINCIA]).enabled   = YES;
    ((UIButton*)[self.view viewWithTag:TAG_btnLOCALIDAD]).enabled   = YES;
    ((UIButton*)[self.view viewWithTag:TAG_btnCALLE]).enabled       = NO;
    ((UIButton*)[self.view viewWithTag:TAG_btnUTILIDAD]).enabled    = YES;
    
    // CONSULTORIO - Deshabilito los combos para el tipo Consultorio.
    if (self.domicilioAux.tipo.idTipo == 1)
    {
        ((UIButton*)[self.view viewWithTag:TAG_btnINSTITUCION]).enabled = NO;
        ((UIButton*)[self.view viewWithTag:TAG_btnCARGO]).enabled       = NO;
    }
    
    // LOCALIDAD - Habilito el combo de calles si la localidad tiene calles y cargo el listado.
    if (self.domicilioAux.localidad.calles > 0)
    {
        ((UIButton*)[self.view viewWithTag:TAG_btnCALLE]).enabled       = YES;
        self.txtCalle.enabled  = NO;
        
        // Cargo el listado de calles para la localidad cuando se toca el botón de Calle.
        
    }
    
    // INSTITUCION - Chequeo si el tipo de domicilio es Institual y de ser así, si tiene alguna institucion asignada.
    if ((self.domicilioAux.tipo.idTipo >= 2) && (self.domicilioAux.institucion.recID > 0))
    {
        //        deshabilitando los desplegables de Provincia, Localidad, Calle, Numero, CP, Piso, Dto.  (el CP siempre esta deshabilitado)
        ((UIButton*)[self.view viewWithTag:TAG_btnPROVINCIA]).enabled = NO;
        ((UIButton*)[self.view viewWithTag:TAG_btnLOCALIDAD]).enabled = NO;
        ((UIButton*)[self.view viewWithTag:TAG_btnCALLE]).enabled     = NO;
        self.txtCalle.enabled  = NO;
        self.txtNumero.enabled = NO;
        self.txtPiso.enabled   = NO;
        self.txtDto.enabled    = NO;
    }
    
    // VISITA Deshabilito el combo de Utilidad para el domicilio de Visita.
    if (self.domicilioAux.utilidad.idUtilidad == 1)
    {
        ((UIButton*)[self.view viewWithTag:TAG_btnUTILIDAD]).enabled    = NO;
    }

}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGRect frame = CGRectMake(0, 0, 768, 22);
    UILabel *background = [[UILabel alloc] initWithFrame:frame];
    background.autoresizingMask = 34;
    background.backgroundColor = UIColorFromRGB(defaultTitlesColor);
    
    UILabel *label;
    
    label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(6, 0, 100, 20);
    label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    label.text = @"Domicilios";
    label.textColor = UIColorFromRGB(defaultTitlesFontColor);
    label.font = [UIFont boldSystemFontOfSize:14.0f];
    label.backgroundColor = UIColorFromRGB(defaultTitlesColor);
    
    [background addSubview:label];
    
    
    return [background autorelease];
}

#pragma mark-
#pragma mark UIAlertView related methods

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // buttonIndex = 0 : No
    
    // buttonIndex = 1 : Si
    if (buttonIndex == 1)
    {
        [self finishGuardar];
    }
}

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
- (void)alertViewCancel:(UIAlertView *)alertView
{
    
}

#pragma mark -
#pragma mark TextField methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (textField.tag == TAG_txtNUMERO)
    {
        
//        if ([self buscarCP] != nil) {
//            self.txtCP.text = [self buscarCP];
//            self.domicilioAux.codigoPostal = [self buscarCP];
//        }
        if (([self.txtNumero.text integerValue] > 0) && (self.domicilioAux.localidad.calles == 1))
        {
            // Si la calle se selecciona de la lista, busco la el CP para la calle
            NSString *sCP = nil;
            sCP = [self buscarCPforCalle:self.domicilioAux.calle.idCalle Numero:[self.txtNumero.text integerValue]];
            if (sCP!= nil)
            {
                self.txtCP.text = sCP;
                // El método buscarCPforCalle ya compleata la propiedad codigoPostal;
                //self.domicilioAux.codigoPostal = sCP;
            } else
            {
                // En el caso de no existir un código postal, borro el que aparecía.
                self.txtCP.text = @"";
                self.domicilioAux.codigoPostal = @"";
                
            }
        }
        
    }
    else if (textField.tag == 668)
    {
        Calle *calle = [[[Calle alloc] init] autorelease];
        calle.idCalle = -1;
        calle.descripcion = self.txtCalle.text;
        self.domicilioAux.calle = calle;
    }
    else if (textField.tag == 669)
    {
        self.domicilioAux.piso = [self.txtPiso.text integerValue];
    }
    else if (textField.tag == 670)
    {
        self.domicilioAux.depto = self.txtDto.text;
    }
    
}

- (BOOL)textFieldOLD:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //BOOL _isAllowed = YES;
    
    // return NO to not change text
    
    if ([string length] == 0 && range.length > 0)
    {
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return NO;
    }
    
    if ((textField == self.txtNumero) ||
        (textField == self.txtPiso))
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        if ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0)
            return YES;
        
        return NO;
    }
    
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    if ([string stringByTrimmingCharactersInSet:notAllowedSet].length > 0)
        return YES;
    
    return NO;
    
//    NSString *oldString = [NSString string:textField.text ifNull:@""];
//    
//    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string] stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
//    
//    
//    if ([oldString isEqualToString:tempString])
//    {
//        _isAllowed =  NO;
//    }
//    
//    return   _isAllowed;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    NSString *oldString = [NSString string:textField.text ifNull:@""];
    NSCharacterSet *notAllowedSet = [NSCharacterSet characterSetWithCharactersInString:@"\"\\/|$#~"];
    
    if ((textField == self.txtNumero) ||
        (textField == self.txtPiso))
    {
        notAllowedSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    }
    
    NSString *tempString = [[textField.text stringByReplacingCharactersInRange:range withString:string]
                            stringByTrimmingCharactersInSet:notAllowedSet];
    
    if ([oldString isEqualToString:tempString])
        return NO;
    
    return   YES;
}


-(NSString*)buscarCP
{
    // busco CP
    /*
     
     Se obtiene mediante el query :
     
     select cp
     from geo_codigospostales
     where pais = %d and provincia = %d and localidad = %d and calle = %d and desde <= %d and hasta >= %d (los valores que se le pasan a desde y hasta son la altura ingresada)
     
     En caso de no usar el desplegable es decir cuando se ingresa a mano la calle, el CP se obtiene de la siguiente forma:
     */
    
    NSString *cp = nil;
    
    NSString *sql = @"select cp from geo_codigospostales where pais = %@ and provincia = %d and localidad = %d";
    
    NSString *query = [NSString stringWithFormat:sql,
                       SettingForKey(PAIS, @"54"),
                       self.domicilioAux.provincia.idProvincia,
                       self.domicilioAux.localidad.idLocalidad];
     
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
            char *_c = (char *)sqlite3_column_text(statement, 0);
            if((_c != nil) && (strlen(_c) > 0))
            {
                cp = [NSString stringWithUTF8String:_c];
            }
        }
        
    }
    else
    {
        NSAssert(0, @"No se encontre la tabla");
    }
    sqlite3_finalize(statement);
    
    self.domicilioAux.codigoPostal = cp;
    
    cp = [self.domicilioAux getCpDescripcion];
    return cp;
}

-(NSString*)buscarCPforCalle:(NSInteger)idCalle Numero:(NSInteger)numero
{
    // busco CP
    
    NSString *cp = nil;
    
    NSString *sql = @"select cp "
                "from geo_codigospostales "
                "where pais = %@ and provincia = %@ and localidad = %@ and calle = %@ and desde <= %@ and hasta >= %@";
    
    NSString *query = [NSString stringWithFormat:sql,
                       SettingForKey(PAIS, @"54"),
                      [NSString intToStr:self.domicilioAux.provincia.idProvincia],
                      [NSString intToStr:self.domicilioAux.localidad.idLocalidad],
                      [NSString intToStr:idCalle],
                      [NSString intToStr:numero],
                      [NSString intToStr:numero]];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    if ( statement )
    {
        if (sqlite3_step(statement) == SQLITE_ROW)
        {
            char *_c = (char *)sqlite3_column_text(statement, 0);
            if((_c != nil) && (strlen(_c) > 0))
            {
                cp = [NSString stringWithUTF8String:_c];
            }
        }
        
    }
    else
    {
        NSAssert(0, @"No se encontre la tabla");
    }
    sqlite3_finalize(statement);
    
    self.domicilioAux.codigoPostal = cp;
    cp = [self.domicilioAux getCpDescripcion];
    
    return cp;
}


-(Domicilio*)buscarDomicilioPrincial
{
    if ([self.abmMedicoReference.medico.domicilios count] > 0)
    {
        for (NSInteger oCount = 0; oCount < [self.abmMedicoReference.medico.domicilios count]; oCount++)
        {
            Domicilio* d = [self.abmMedicoReference.medico.domicilios objectAtIndex:oCount];
            if (d.utilidad.idUtilidad == 1)
                return d;
        }
    }
    
    return nil;
}


- (BOOL)checkDomicilioData:(Domicilio*)d
{
//    // Chequeo que no exista domicilio de visita si se esta ingresando uno.
//    Domicilio *o = [self buscarDomicilioPrincial];
//    if ((o) && (!(o==d) && (d.utilidad.idUtilidad == 1)))
//    {
//        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"Alerta"
//                                                       message:@"Ya existe un domicilio de visita"
//                                                      delegate:nil
//                                             cancelButtonTitle:@"Aceptar"
//                                             otherButtonTitles:nil];
//        [aler show];
//        [aler release];
//        return FALSE;
//    }
    
    // Para saber si la calle debe ser ingresada manualmente chequear si la localidad del domicilio tiene la propiedad calles = 0;
    if (d.localidad.calles == 0)
    {
        // Si no creó el objeto de la calle.
        //if ((!d.calle) || ([d.calle.description length] < 1))
        if (!d.calle)
        {
            // Calle.
            d.calle = [[Calle alloc] init];
        }
    }
        // Chequeo si el domicilio de domiciliosAux es el mismo que el pasado como parámetro
        if (d == self.domicilioAux)
        {
            d.calle.descripcion = [NSString string:self.txtCalle.text ifNull:@""];
            
            // Número (Altura de la calle)
            d.numero = [self.txtNumero.text integerValue];
            
            // Piso
            d.piso = [self.txtPiso.text integerValue];
            
            // Dpto
            d.depto = [NSString string:self.txtDto.text ifNull:@""];
        }
    //}
    
    NSString *msg = [[d isOkForSave] retain];
    if ([msg isEqualToString:@"OK"])
    {
        [msg release];
        return TRUE;
    }
    else
    {
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"Alerta"
                                                       message:msg
                                                      delegate:nil
                                             cancelButtonTitle:@"Aceptar"
                                             otherButtonTitles:nil];
        [aler show];
        [aler release];
        [msg release];
        return FALSE;
    }
    
    return TRUE;
}


#pragma mark -
#pragma mark popListView methods

- (void)showPop:(id)sender withData:(NSArray*)data withTitle:(NSString*)title withSearch:(BOOL)search
{
    CGRect rect =  CGRectZero;
    rect.origin = CGPointMake(0, 0);
    rect.size   = CGSizeMake(350, 500);
    
    UIView *popRectView = (UIView*)sender;
    
    //if (!self.popupMenu)
        self.popupMenu = [MTpopListView alloc];
    
    //self.popupMenu = [self.popupMenu initForLocalidadesForProvincia:self.filtroDomicilio.provincia.idProvincia
    //                                                   WithDelegate:self];
    self.popupMenu = [[MTpopListView alloc] initWithRect:rect];
    self.popupMenu.delegate = self;
    [self.popupMenu dismissPopoverAnimated:NO];
    
    [self.popupMenu setTitleAndParameters:title rowData:data];
    [self.popupMenu setSize:rect.size];
    [self.popupMenu useSearch:search];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x + 16,
                               popRectView.frame.origin.y + 100,// + 64,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    
    [self.popupMenu presentPopoverFromRectWithSender:popSourceRect
                                              Sender:title
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionRight
                                            animated:YES];
}

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender selObject:(id)selObject
{
    NSInteger indice = -1;
    indice = [self.popupMenu.rowData indexOfObject:selObject];
    
    if (indice == NSNotFound)
        return;
    
    [self selectedIndex:indice caller:sender];
}
@end
