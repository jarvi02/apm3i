//
//  coberturaFooter.h
//  APM4
//
//  Created by Laura Busnahe on 8/6/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface coberturaFooter : UIView

@property (retain, nonatomic) IBOutlet UIButton *lblDescripcion;
@property (retain, nonatomic) IBOutlet UIButton *lblProgramados;
@property (retain, nonatomic) IBOutlet UIButton *lblVisitados;
@property (retain, nonatomic) IBOutlet UIButton *lblPorcetajes;
@property (retain, nonatomic) IBOutlet UIButton *lblRevisitas;
@property (retain, nonatomic) IBOutlet UIButton *lblFluctuantes;
@property (retain, nonatomic) IBOutlet UIButton *lblTotal;
@end
