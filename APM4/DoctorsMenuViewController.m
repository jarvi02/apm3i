//
//  DoctorsMenuViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "DoctorsMenuViewController.h"
#import "AppDelegate.h"

@interface DoctorsMenuViewController ()

-(void)pressedFiltros;

@end

@implementation DoctorsMenuViewController

-(NSArray *)options
{
    return [NSArray arrayWithObjects:
            @"Búsqueda Avanzada",
            @"Vista Rápida",
            @"Filtro",
            @"Quitar Todos los Filtros",
            @"Cambiar de Grupo",
            nil];
}

-(NSString *)title
{
    return @"Cartera";
}

-(CGSize)size
{
    //return CGSizeMake(300.0f, 176.0f);
    return CGSizeMake(300.0f, 220.0f);
}

-(void)selectedOptionsAtIndex:(NSUInteger)index
{
    switch (index) {
            
        case 2:
            [self pressedFiltros];
            break;
            
        case 0:
            // Busqueda
            [self pressedBusqueda];
            break;
            
        case 3:
            // Quitar Filtro
            [self pressedQuitarFiltro];
            break;
            
        case 4:
            // Quitar Filtro
            [self pressedCambiarGrupo];
            break;
            
        case 1:
            // Vista Rapida
            
        default:
            [self showNotImplemented];
    }
}

-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index {
    
    switch (index) {
        case 0:
            // Busqueda
            return YES;
            break;
        case 1:
            // Vista Rapida
            return NO;
            break;
        case 2:
            // Filtro
            return YES;
            break;
        case 3:
            // Quitar Filtro
            return YES;
            break;
        case 4:
            // Cambiar Grupo
            return YES;
            break;
        default:
            return NO;
    }
    
}

-(void)pressedFiltros
{
    [Delegate dismissPopOver:YES];
    if (self.delegate != nil)
        [self.delegate performTask:@"filtros"];
}

-(void)pressedBusqueda
{
    [Delegate dismissPopOver:YES];
    if (self.delegate != nil)
        [self.delegate performTask:@"busqueda"];
}

-(void)pressedQuitarFiltro
{
    [Delegate dismissPopOver:YES];
    if (self.delegate != nil)
        [self.delegate performTask:@"quitarfiltros"];
}

-(void)pressedCambiarGrupo
{
    [Delegate dismissPopOver:YES];
    if (self.delegate != nil)
        [self.delegate performTask:@"cambiargrupo"];
}

@end
