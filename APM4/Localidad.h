//
//  Localidad.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 05/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Localidad : NSObject

@property(nonatomic, assign)NSInteger pais;
@property(nonatomic, assign)NSInteger provincia;
@property(nonatomic, retain)NSString *descripcion;
@property(nonatomic, assign)NSInteger calles;
@property(nonatomic, assign)NSInteger idLocalidad;

- (NSString*)getDescription;

+(NSArray*)GetAllByProvincia:(NSInteger)idProvincia;

@end
