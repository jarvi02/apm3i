//
//  prescAuditoriaViewController.h
//  APM4
//
//  Created by Laura Busnahe on 3/4/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "popListView.h"
#import "MTEditActions.h"

@class MTAuditorias;
@class MTMercados;
@class prescAuditoriaViewController;


@protocol prescAuditoriaDelegate <NSObject>
@required
// Sent when the user tap Selección or Todos button.
- (void)prescAuditoriaView:(prescAuditoriaViewController *)controller
         selectedAuditoria:(MTAuditorias *)auditoria
              mercadosList:(NSArray *)mercados
                    action:(MTEditActionsType)action;
@end


@interface prescAuditoriaViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MTpopListViewDelegate, UITextFieldDelegate>

// Properties
@property (nonatomic) NSInteger                 idMedico;
@property (nonatomic, strong) NSArray           *arrayAuditorias;
@property (nonatomic, strong) NSArray           *arrayMercados;
@property (nonatomic,strong) MTAuditorias       *auditoriaSelection;

@property (nonatomic, retain) MTpopListView                 *popMenu;
@property (nonatomic, strong) id <prescAuditoriaDelegate>   delegate;

// Outlets
@property (retain, nonatomic) IBOutlet UITableView *tblMercados;
@property (retain, nonatomic) IBOutlet UITextField *edtAuditoria;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *oBtnTodos;

// Methods
- (id)initViewDefaultNib;
- (id)initViewDefaultNibForSelection:(MTAuditorias*)selAuditoria mercadosArray:(NSArray*)mercados;

- (void)updateMercadosTable;

- (IBAction)btnTodos:(id)sender;
- (IBAction)btnSeleccion:(id)sender;
- (IBAction)btnCancelar:(id)sender;

- (IBAction)tapLimpiar:(id)sender;
- (IBAction)tapAuditoria:(id)sender;

@end
