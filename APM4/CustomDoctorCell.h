//
//  CustomDoctorCell.h
//  CustomCell
//
//  Created by Laura Busnahe on 2/4/13.
//  Copyright (c) 2013 Laura Busnahe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomDoctorCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *image;
@property (retain, nonatomic) IBOutlet UILabel *lbNombreMedico;
@property (retain, nonatomic) IBOutlet UILabel *lbVisto;
@property (retain, nonatomic) IBOutlet UILabel *lbLugar;
@property (retain, nonatomic) IBOutlet UILabel *lbEspecialidad;
@property (retain, nonatomic) IBOutlet UILabel *lbTurno;
@property (retain, nonatomic) IBOutlet UILabel *lbDiaHora;
@property (retain, nonatomic) IBOutlet UILabel *lblPotencial;

@property (retain, nonatomic) IBOutlet UILabel *lblVisRealizadas;
@property (retain, nonatomic) IBOutlet UILabel *lblVisProgramadas;
@end
