//
//  customAjusteMMCell.m
//  APM4
//
//  Created by Laura Busnahe on 4/3/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "customAjusteMMCell.h"

@implementation customAjusteMMCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
