//
//  UIButtonRounded.h
//  APM4
//
//  Created by Laura Busnahe on 9/23/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButtonRounded : UIButton

@end
