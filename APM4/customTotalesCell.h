//
//  customTotalesCell.h
//  APM4
//
//  Created by Laura Busnahe on 3/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customTotalesCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *lblDescripcion;
@property (retain, nonatomic) IBOutlet UILabel *lblUniAcumuladas;
@property (retain, nonatomic) IBOutlet UILabel *lblPorcRelativo;
@end
