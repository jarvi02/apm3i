//
//  PromocionadosCell.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 24/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "PromocionadosCell.h"

@implementation PromocionadosCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lbFecha release];
    [_lbOrden release];
    [_lbProducto release];
    [_lbCantidad release];
    [_lbLit release];
    [_lbObs release];
    [super dealloc];
}
@end
