//
//  CarteraMedico.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 14/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CarteraMedico.h"


#import "DB.h"
#import "Config.h"
#import "MTGrupos.h"

#import "NSDateExtended.h"
#import "NSStringExtended.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//#define colorASC     0xBBE872
//#define colorDESC    0xFA7372
//#define colorNormal  0xFFFFFF

@interface CarteraMedico()
+(NSString*)filtrarCaracteresRaros:(NSString*) str;
@end

@implementation CarteraMedico


#pragma mark -
#pragma mark Class Methods

+(NSArray*) GetAll{
    
    /*
     SELECT med_medicos.id as _id, apellido || "," || nombre as apenom, lugar, especialidad1 || ", " || especialidad2 as esp, mtc_especialidades.descripcion as desc_esp, case lu when 1 then "Lu " else "" end || case Ma when 1 then "Ma " else "" end || case Mi when 1 then "Mi " else "" end || case Ju when 1 then "Ju " else "" end || case Vi when 1 then "Vi " else "" end || desde || " a " || hasta as dias, actividad, case actividad when "T" then "Tarde" else "Mañana" end as desc_act, case when mtc_instituciones.descripcion is null then case when med_medicos.calle is null then '' else med_medicos.calle end || ", " || case when med_medicos.altura is null then '' else med_medicos.altura end || ", " || case when med_medicos.piso is null then '' else med_medicos.piso end || ", " || case when med_medicos.dpto is null then '' else med_medicos.dpto end || "," || case when med_medicos.localidad is null then '' else med_medicos.localidad end else mtc_instituciones.descripcion end as direccion, estado, descestado, a, b, m, visitasprogramadas, visitasrealizadas, objetivos, asignacion, penalta, penbaja, descABM, mtc_instituciones.descripcion as desc_institucion, visitasprogramadas, visitasrealizadas FROM med_medicos inner join mtc_especialidades on med_medicos.especialidad1= mtc_especialidades.sigla and med_medicos.nodo  = 207 left join med_domicilios on med_medicos.id = med_domicilios.medico and med_domicilios.nodo = 207 and med_domicilios.utilidad = 1 left join mtc_instituciones on med_domicilios.institucion = mtc_instituciones.id;
     */
    
    NSMutableArray  *array = [[NSMutableArray alloc] initWithCapacity:40];
    
    NSString *sql =
            @"SELECT med_medicos.id as _id, "
                    "apellido || \", \" || nombre as apenom, "
                    "lugar, "
                    "especialidad1 || \", \" || especialidad2 as esp, "
                    "mtc_especialidades.descripcion as desc_esp, "
                        "case lu when 1 then \"Lu \" else \"\" end || "
                        "case Ma when 1 then \"Ma \" else \"\" end || "
                        "case Mi when 1 then \"Mi \" else \"\" end || "
                        "case Ju when 1 then \"Ju \" else \"\" end || "
                        "case Vi when 1 then \"Vi \" else \"\" end || "
                        "desde || \" a \" || hasta as dias, "
                    "actividad, "
                    "case actividad when \"T\" then \"Tarde\" else \"Mañana\" end as desc_act, "
                    "case when mtc_instituciones.descripcion is null then "
                        "case when med_medicos.calle is null then \'\' else med_medicos.calle end || \", \" || "
                        "case when med_medicos.altura is null then \'\' else med_medicos.altura end || \", \" || "
                        "case when med_medicos.piso is null then \'\' else med_medicos.piso end || \", \" || "
                        "case when med_medicos.dpto is null then \'\' else med_medicos.dpto end || \",\" || "
                        "case when med_medicos.localidad is null then \'\' else med_medicos.localidad end "
                    "else mtc_instituciones.descripcion end as direccion, "
                    "estado, "
                    "descestado, "
                    "a, b, m, "
                    "visitasprogramadas, "
                    "visitasrealizadas, "
                    "objetivos, "
                    "asignacion, "
                    "penalta, "
                    "penbaja, "
                    "descABM, "
                    "mtc_instituciones.descripcion as desc_institucion, "
                    "visitasprogramadas, "
                    "visitasrealizadas, "
                    "mtc_especialidades.id as id_especialidad, "
                    "potencial as id_potencial, "
                    "med_domicilios.provincia as id_provincia, "
                    "ifnull(med_domicilios.localidad, \"\") as localidad, "
                    "ifnull(med_domicilios.calle, \"\") as calle, "
                    "lu, "
                    "ma, "
                    "mi, "
                    "ju, "
                    "vi, "
                    "email "
            "FROM med_medicos inner join "
                 "mtc_especialidades on med_medicos.especialidad1= mtc_especialidades.sigla and med_medicos.nodo  = %@ left join "
                 "med_domicilios on med_medicos.id = med_domicilios.medico and med_domicilios.nodo = %@ and med_domicilios.utilidad = 1 left join "
                 "mtc_instituciones on med_domicilios.institucion = mtc_instituciones.id "
            "ORDER BY apenom";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql, nodo, nodo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];

	if ( statement ) {
        
        CarteraMedico *o;
        
  
        
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
		
            o = [[CarteraMedico alloc] init];
            
            o.idCarteraMedico = sqlite3_column_int(statement, 0);
            
            char *_an = (char *)sqlite3_column_text(statement, 1);
            if((_an != nil) && (strlen(_an) > 0))
            {
                o.apellidoNombre = [NSString stringWithUTF8String:_an];
			}
            
            
            char *_l = (char *)sqlite3_column_text(statement, 2);
            if((_l != nil) && (strlen(_l) > 0))
            {
               o.lugar = [NSString stringWithUTF8String:_l];
			}
            
            
            char *_e = (char *)sqlite3_column_text(statement, 3);
            if((_e != nil) && (strlen(_e) > 0))
            {
              o.especialidad  = [NSString stringWithUTF8String:_e];
			}
            
            
            char *_de = (char *)sqlite3_column_text(statement, 4);
            if((_de != nil) && (strlen(_de) > 0))
            {
              o.descripcionEspecialidad  = [NSString stringWithUTF8String:_de];
			}
            
            
            char *_di = (char *)sqlite3_column_text(statement, 5);
            if((_di != nil) && (strlen(_di) > 0))
            {
              o.dias  = [NSString stringWithUTF8String:_di];
			}
            
            
            char *_ac = (char *)sqlite3_column_text(statement, 6);
            if((_ac != nil) && (strlen(_ac) > 0))
            {
               o.actividad = [NSString stringWithUTF8String:_ac];
			}
            
            
            char *_da = (char *)sqlite3_column_text(statement, 7);
            if((_da != nil) && (strlen(_da) > 0))
            {
               o.descripcionActividad = [CarteraMedico filtrarCaracteresRaros:[NSString stringWithUTF8String:_da]];
			}
            
            
            char *_dn = (char *)sqlite3_column_text(statement, 8);
            if((_dn != nil) && (strlen(_dn) > 0))
            {
               o.direccion = [NSString stringWithUTF8String:_dn];
			}
            
            
            o.estado = sqlite3_column_int(statement, 9);
            
            char *_dt = (char *)sqlite3_column_text(statement, 10);
            if((_dt != nil) && (strlen(_dt) > 0))
            {
               o.descripcionEstado = [NSString stringWithUTF8String:_dt];
			}
            
            
            o.a = sqlite3_column_int(statement, 11);
            
            o.b = sqlite3_column_int(statement, 12);
            
            o.m = sqlite3_column_int(statement, 13);
            
            o.visitasProgramadas = sqlite3_column_int(statement, 14);
            
            o.visitasRealizadas = sqlite3_column_int(statement, 15);
            
            char *_o = (char *)sqlite3_column_text(statement, 16);
            if((_o != nil) && (strlen(_o) > 0))
            {
               o.objetivos = [NSString stringWithUTF8String:_o];
			}
            
            
            o.asignacion = sqlite3_column_int(statement, 17);
            
            o.penalta = sqlite3_column_int(statement, 18);
            
            o.penbaja = sqlite3_column_int(statement, 19);
            
            char *_abm = (char *)sqlite3_column_text(statement, 20);
            if((_abm != nil) && (strlen(_abm) > 0))
            {
               o.descripcionABM = [NSString stringWithUTF8String:_abm];
			}
            
            
            char *_den = (char *)sqlite3_column_text(statement, 21);
            if((_den != nil) && (strlen(_den) > 0))
            {
               o.descripcionInstitucion = [NSString stringWithUTF8String:_den];
			}

            
            o.visitasProgramadas_2 = sqlite3_column_int(statement, 22);
            
            o.visitasRealizadas_2 = sqlite3_column_int(statement, 23);
            
            o.idEspecialidad = sqlite3_column_int(statement, 24);
            
            o.idPotencial = sqlite3_column_int(statement, 25);
            
            o.idProvincia = sqlite3_column_int(statement, 26);

            char *_localidad = (char *)sqlite3_column_text(statement, 27);
            if((_localidad != nil) && (strlen(_localidad) > 0))
            {
                o.localidad = [NSString stringWithUTF8String:_localidad];
			}

            char *_calle = (char *)sqlite3_column_text(statement, 28);
            if((_calle != nil) && (strlen(_calle) > 0))
            {
                o.calle = [NSString stringWithUTF8String:_calle];
			}
            
            o.grupos = [NSMutableArray arrayWithArray:[MTGrupos getAllForMedico:o.idCarteraMedico]];
            
            
            char *_email = (char *)sqlite3_column_text(statement, 34);
            if((_email != nil) && (strlen(_email) > 0))
            {
                o.email = [NSString stringWithUTF8String:_email];
            }
        
            [array addObject:o];
            [o release];
            
        }
        
        
	} else {
        NSAssert(0, @"No se encontre la tabla configuracion");
	}
	sqlite3_finalize(statement);
    
    
    return [array autorelease];
}

+(NSString*)filtrarCaracteresRaros:(NSString*) str {
    
    
    return str;
}

-(void)darDeBaja:(double)fecha motivo:(NSInteger)motivo descr:(NSString *)descr pendiente:(BOOL)pendiente baja:(BOOL)baja
{
    
    NSString *sql = [NSString stringWithFormat:@"update MED_MEDICOS set fechabaja = %f , motivobaja = %d, B = %d, Descabm = \"%@\", Penbaja=%d where nodo = %@ and id=%d", fecha , motivo, baja?1:0, descr, pendiente?1:0, SettingForKey(NODO, @""), self.idCarteraMedico];

#ifdef DEBUG_ABMMEDICO
    NSLog(@" - CarteraMedico: darDeBaja - ");
    NSLog(@"- query: %@", sql);
#endif
    
    [[DB getInstance] excecuteSQL:sql];
}

#pragma mark -
#pragma mark Instance Methods

-(id)init{
    
    self = [super init];
    if (self) {
        
        self.apellidoNombre = @"";
        self.lugar = @"";
        self.especialidad = @"";
        self.descripcionEspecialidad = @"";
        self.dias = @"";
        self.actividad = @"";
        self.descripcionActividad = @"";
        self.direccion = @"";
        self.descripcionEstado = @"";
        self.objetivos = @"";
        self.descripcionABM = @"";
        self.descripcionInstitucion = @"";
        self.idEspecialidad = 0;
        self.idPotencial = 0;
        self.idProvincia = 0;
        self.localidad  = @"";
        self.calle = @"";
        self.grupos = nil;
        
    }
    return self;
}

-(NSString *)novedad
{
    NSString *FormatoFecha = @"%m/%d/%Y %H:%M";
    
   
    NSString *sql = [NSString stringWithFormat:@"select nodo || \"|\" || id || \"|\" || ifnull(apellido, \"\") || \"|\" || ifnull(nombre,\"\") || \"|\" || ifnull(descmarcado,\"\") || \"|\" || sexo || \"|\" || ifnull(nacimiento, \"\") || \"|\" || ifnull(cuit, \"\") || \"|\" || ifnull(matriculanacional,\"\") || \"|\" || ifnull(matriculaprovincial,\"\") || \"|\" || ifnull(egreso,\"\") || \"|\" || ifnull(email, \"\") || \"|\" || codigointerno || \"|\" || ifnull(asignacion,\"\") || \"|\" || actividad || \"|\" || potencial || \"|\" || ifnull(tratamiento,\"\") || \"|\" || visitasprogramadas || \"|\" || visitasrealizadas || \"|\" || ifnull(estado,\"\") || \"|\" || ifnull(strftime('%@', fechabaja, 'unixepoch', 'localtime'), \"\") || \"|\" || ifnull(motivobaja, \"\") || \"|\" || ifnull(idpool,\"\") || \"|\" || strftime('%@', fechaalta, 'unixepoch', 'localtime') || \"|\" || ifnull(objetivos, \"\") || \"|\" || ifnull(marca, \"\") || \"|\" ||case when A IS 0 then 'N' else 'S' end || \"|\" || case when B IS 0 then 'N' else 'S' end || \"|\" || case when M IS 0 then 'N' else 'S' end || \"|\" || case when penalta IS NULL then '' when penalta is 0 then 'N' when penalta is 1 then 'S' else penalta end || \"|\" || case when penbaja IS NULL then '' when penbaja is 0 then 'N' when penbaja is 1 then 'S' else penbaja end as novedad from med_medicos where id = %d and nodo = %@", FormatoFecha, FormatoFecha, self.idCarteraMedico, SettingForKey(NODO, @"")];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:sql];
    
    NSString *novedad = @"";
	if ( statement ) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            char *_ac = (char *)sqlite3_column_text(statement, 0);
            if((_ac != nil) && (strlen(_ac) > 0)) {
                novedad = [NSString stringWithUTF8String:_ac];
            }
        }
    } else {
        NSAssert(0, @"No se encontre la tabla med_medicos");
	}
	sqlite3_finalize(statement);
    
    return novedad;
}

-(void)dealloc{
    
    
    [_apellidoNombre release];
    [_lugar release];
    [_especialidad release];
    [_descripcionEspecialidad release];
    [_dias release];
    [_actividad release];
    [_descripcionActividad release];
    [_direccion release];
    [_descripcionEstado release];
    [_objetivos release];
    [_descripcionABM release];
    [_descripcionInstitucion release];
    [_grupos release];
    
    [super dealloc];
    
}

-(UIImage*)imagenMedico {
    
    UIImage *image = nil;
    
    int mVisitasRealizadas = self.visitasRealizadas;
    int mVisitasProgramadas = self.visitasProgramadas;
    
    if (mVisitasRealizadas==0) {
        image = [UIImage imageNamed:@"userred"];// imgEstado.setImageResource(R.drawable.userred);
    } else {
        if (mVisitasRealizadas>=mVisitasProgramadas) {
           image = [UIImage imageNamed:@"usergreen"];// imgEstado.setImageResource(R.drawable.usergreen);
        } else {
           image = [UIImage imageNamed:@"useryellow"];// imgEstado.setImageResource(R.drawable.useryellow);
        }
    }

    return image;
    
}

-(UIColor*)colorTexto {
    
    //UIColor *c = nil;
    /*
     Orden Predecencia
     Azul		=	Cuando se modifico cualquier dato del registro del medico
     Verde		=	cuando di de alta un medico
     Rojo		=	cuando di de baja a un medico
     Negro      =	color por defecto
     */
    
    // Baja
    if ((self.b) || (self.penbaja))
    {
        return UIColorFromRGB(0xC80000);
        //return [UIColor redColor];
    }
    
    // Alta
    if ((self.a) || (self.penalta))
    {
        //return [UIColor greenColor];
        return UIColorFromRGB(0x00B800);
    }
    
    // Modificación
    if (self.m)
    {
        //return [UIColor blueColor];
        return UIColorFromRGB(0x0000B8);
    }
    
    
    
    return [UIColor blackColor];
}

#define _descAlta           @"Alta"
#define _descBaja           @"Baja"
#define _descModificacion   @"Modificación"
#define _descPendienteAlta  @"Pendiente de Alta"
#define _descPendienteBaja  @"Pendiente de Baja"
-(NSString*)getDescripcionABM
{
    NSString *result = @"";
    NSString *descABM = @"";
    
    if (self.esFluctuante)
        result = @"Fluctuante";
    
    // Pendiente de Baja
    if (self.penbaja)
        descABM = _descPendienteBaja;
    else
    // Pendiente de Alta
    if (self.penalta)
        descABM = _descPendienteAlta;
    else
    // Baja
    if (self.b)
        descABM = _descBaja;
    else
    // Alta
    if (self.a)
        descABM = _descAlta;
    else
    // Modificación
    if (self.m)
        descABM = _descModificacion;
    
    // Si es fluctuante y tiene descripción
    if (([descABM length] > 0) && ([result length] > 0))
        result = [NSString stringWithFormat:@"%@ - %@", result, descABM];
    else
    if ([descABM length] > 0)
        result = [NSString string:descABM];
        
    return result;
}


- (BOOL)esFluctuante
{
    if ((self) && (self.asignacion == 2))
        return YES;
    else
        return NO;
}

- (BOOL)esBaja
{
    if ((self) && ((self.b) || (self.penbaja)))
        return YES;
    else
        return NO;
}

- (BOOL)necesitaAutorizacion
{
    if ((self) &&
        ((self.penalta) || (self.penbaja)))
        return YES;
    else
        return NO;
}

- (NSString*)getDescripcionDomicilio
{
    NSString *result = @"";
    
    if ([self esFluctuante])
    {
        
    } else
    {
        
    }
    
    return result;
}

- (NSString*)getDescripcionVisitasRealizadas
{
    NSString *result = @"";
    
    NSInteger mVisitasRealizadas = self.visitasRealizadas;
    
    result = [NSString stringWithFormat:@"Visitas Realizadas: %d", mVisitasRealizadas];
    
//    // Chequeo y genero el texto dependiendo si hay 1 o más visitas programadas.
//    NSString *textoProgramadas = @"visita programada";
//    NSString *textoVisita = @"visita";
//    
//    if (mVisitasProgramadas > 1)
//    {
//        textoProgramadas = @"visitas programadas";
//        textoVisita = @"visitas";
//    }
//    
//    // Genero el texto dependiendo de la cantidad de visitas realizadas.
//    NSString *textoRealizadas = @"realizada";
//    if (mVisitasRealizadas > 1)
//    {
//        textoRealizadas = @"realizadas";
//    }
//    
//    if (mVisitasRealizadas == 0)
//    {
//        result = [NSString stringWithFormat:@"%d %@", mVisitasProgramadas, textoProgramadas];
//    } else
//    {
//        result = [NSString stringWithFormat:@"%d de %d %@ %@", mVisitasRealizadas, mVisitasProgramadas,
//                                                               textoVisita, textoRealizadas];
//    }
    
    return result;
}

- (NSString*)getDescripcionVisitasProgramadas
{
    NSString *result = @"";

    NSInteger mVisitasProgramadas = self.visitasProgramadas;
    
    result = [NSString stringWithFormat:@"Visitas Programadas: %d", mVisitasProgramadas];
    
    return result;
}

@end
