//
//  MTHistoricoVisitas.h
//  APM4
//
//  Created by Laura Busnahe on 7/16/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTHistoricoVisitas : NSObject

@property (nonatomic, strong)NSString *ciclo01;
@property (nonatomic, strong)NSString *ciclo02;
@property (nonatomic, strong)NSString *ciclo03;
@property (nonatomic, strong)NSString *ciclo04;
@property (nonatomic, strong)NSString *ciclo05;
@property (nonatomic, strong)NSString *ciclo06;
@property (nonatomic, strong)NSString *ciclo07;
@property (nonatomic, strong)NSString *ciclo08;
@property (nonatomic, strong)NSString *ciclo09;
@property (nonatomic, strong)NSString *ciclo10;
@property (nonatomic, strong)NSString *ciclo11;
@property (nonatomic, strong)NSString *ciclo12;

@property (nonatomic, strong)NSString *tipo;
@property (nonatomic, strong)NSString *total;


+(NSArray*)getAllForMedico:(NSUInteger)idMedico;


@end
