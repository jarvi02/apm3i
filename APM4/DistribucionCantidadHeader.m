//
//  DistribucionCantidadHeader.m
//  APM4
//
//  Created by Laura Busnahe on 9/9/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "DistribucionCantidadHeader.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define colorASC     darkHeaderAscColor
#define colorDESC    darkHeaderDescColor
#define colorNormal  0x000000

#pragma mark -
@implementation DistribucionCantidadHeader
{
    tDistribucionCantidadHeaderField orderedBy;
    NSComparisonResult               order;
}

#pragma mark Private methods
- (id)init
{
    self = [super init];
    if (self)
    {
        self.delegate = nil;
        orderedBy = fDistribucionCantidadFieldDescripcion;
        order     = NSOrderedAscending;
    }
    
    return self;
}

- (void)orderBy:(tDistribucionCantidadHeaderField)field sender:(id)sender reset:(BOOL)reset
{
    // Chequeo si el delegado fue asignado.
    if (!self.delegate)
        return;
    
    // Chequeo si se trata del mismo campo por el que ya estaba ordenado.
    if (orderedBy != field)
    {
        // Si no es igual, asigno el nuevo campo y cambio el orden a ascendete.
        orderedBy = field;
        order     = NSOrderedAscending;
    } else
    {
        // Si es el mismo campo, cambio el orden.
        if (order == NSOrderedAscending)
            order = NSOrderedDescending;
        else
            order = NSOrderedAscending;
    }
    
    if (reset)
        order = NSOrderedAscending;
    
    // Coloreo el label seleccionado.
    [self colorLabels:sender];
    
    // Llamo el método del delegate.
    if ([self.delegate respondsToSelector:@selector(DistribucionCantidadHeader:selectedField:Order:)])
        [self.delegate DistribucionCantidadHeader:self selectedField:orderedBy Order:order];
}

- (void)colorLabels:(id)sender
{
    NSUInteger newColor = order == NSOrderedAscending ? colorASC : colorDESC;
    
    [self.btnDescripcion  setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.btnManiana      setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.btnPorcManiana  setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.btnTarde        setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.btnPorcTarde    setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    
    UIButton *o = (UIButton*)sender;
    if ([o respondsToSelector:@selector(setTitleColor:forState:)])
        [o setTitleColor:UIColorFromRGB(newColor) forState:UIControlStateNormal];
    
}

#pragma mark -
#pragma mark IBAction methods
- (IBAction)tapLabel:(id)sender
{
    tDistribucionCantidadHeaderField field = fDistribucionCantidadFieldDescripcion;
    
    if (sender == self.btnDescripcion)
        field = fDistribucionCantidadFieldDescripcion;
    
    if (sender == self.btnManiana)
        field = fDistribucionCantidadFieldManiana;
    
    if (sender == self.btnPorcManiana)
        field = fDistribucionCantidadFieldPorcManiana;
    
    if (sender == self.btnTarde)
        field = fDistribucionCantidadFieldTarde;
    
    if (sender == self.btnPorcTarde)
        field = fDistribucionCantidadFieldPorcTarde;
    
    if (sender == self.btnTotal)
        field = fDistribucionCantidadFieldTotal;
    
    if (sender == self.btnPorcTotal)
        field = fDistribucionCantidadFieldPorcTotal;
    
    [self orderBy:field sender:sender reset:NO];
}
#pragma mark -

@end