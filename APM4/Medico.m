//
//  Medico.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 03/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Medico.h"
#import "Config.h"
#import "DB.h"
#import "EstadoVisita.h"

@implementation Medico


- (id)init
{
    self = [super init];
    if (self) {
       
        self.apellido = @"";
        self.nombre = @"";
        self.actividad = @"";
        self.potencial = nil;
        self.sexo = @"";
        self.tratamiento = nil;
        self.fechaNacimiento = nil;
        self.anioEgreso = @"";
        self.CUIT = @"";
        self.email = @"";
        self.revisita = @"";
        self.matriculaNacional = @"";
        self.matriculaProvincial = @"";
    }
    return self;
}

- (void)dealloc
{
    [_apellido release];
    [_nombre release];
    [_actividad release];
    [_potencial release];
    [_especialidades release];
    [_domicilios release];
    [_obrasSociales release];
    [_sexo release];
    [_tratamiento release];
    [_fechaNacimiento release];
    [_anioEgreso release];
    [_CUIT release];
    [_email release];
    [_revisita release];
    [_matriculaNacional release];
    [_matriculaProvincial release];

    [super dealloc];
}

+ (Medico*) Medico {

    Medico *medico = [[Medico alloc] init];
    
    medico.modo = ABMMedicoModoAlta;
    medico.especialidades = [NSMutableArray array];
    medico.domicilios = [NSMutableArray array];
    medico.obrasSociales = [NSMutableArray array];
    
    return [medico autorelease];
}

+(Medico*) GetById:(NSUInteger) idMedico
{
    Medico *m = [[Medico alloc] init];
    m.modo = ABMMedicoModoModificar;
    
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *sql = @"SELECT id, "
                        "estado, "
                        "visitasprogramadas, "
                        "visitasrealizadas, "
                        "apellido, "
                        "nombre, "
                        "actividad "
                     "FROM med_medicos "
                     "WHERE id=%d and nodo = %@";
    NSString *query = [NSString stringWithFormat:sql, idMedico, nodo];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    char *_c;
	if ( statement ) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
        
            
            m.idMedico = sqlite3_column_int(statement, 0);
            m.idEstado = sqlite3_column_int(statement, 1);
            m.visitasProgramadas = sqlite3_column_int(statement, 2);
            m.visitasRealizadas = sqlite3_column_int(statement, 3);
            
            _c = (char *)sqlite3_column_text(statement, 4);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.apellido = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 5);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.nombre = [NSString stringWithUTF8String:_c];
			}
            
            _c = (char *)sqlite3_column_text(statement, 6);
            if((_c != nil) && (strlen(_c) > 0)) {
                m.actividad = [NSString stringWithUTF8String:_c];
			}
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla");
	}
	sqlite3_finalize(statement);
    
    return [m autorelease];
}

+(NSString *)getForNovedad:(NSUInteger) idMedico
{
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *FormatoFecha = @"%m/%d/%Y";
    
    NSString *query = [NSString stringWithFormat:
                       @"select nodo || \"|\" || "
                                "id || \"|\" || "
                                "ifnull(apellido, \"\") || \"|\" || "
                                "ifnull(nombre,\"\") || \"|\" || "
                                "ifnull(descmarcado,\"\") || \"|\" || "
                                "sexo || \"|\" || "
                                "ifnull(nacimiento, \"\") || \"|\" || "
                                "ifnull(cuit, \"\") || \"|\" || "
                                "ifnull(matriculanacional, \"\") || \"|\" || "
                                "ifnull(matriculaprovincial, \"\") || \"|\" || "
                                "ifnull(egreso,\"\") || \"|\" || "
                                "ifnull(email, \"\") || \"|\" || "
                                "codigointerno || \"|\" || "
                                "ifnull(asignacion,\"\") || \"|\" || "
                                "actividad || \"|\" || "
                                "potencial || \"|\" || "
                                "ifnull(tratamiento,\"\") || \"|\" || "
                                "visitasprogramadas || \"|\" || "
                                "visitasrealizadas || \"|\" || "
                                "ifnull(estado,\"\") || \"|\" || "
                                "ifnull(strftime('%@', fechabaja, 'unixepoch', 'localtime'), \"\") || \"|\" || "
                                "ifnull(motivobaja, \"\") || \"|\" || "
                                "ifnull(idpool,\"\") || \"|\" || "
                                "strftime('%@', fechaalta, 'unixepoch', 'localtime') || \"|\" || "
                                "ifnull(objetivos, \"\") || \"|\" || "
                                "ifnull(marca, \"\") || \"|\" || "
                                "case "
                                    "when A IS 0 then 'N' "
                                    "else 'S' end || \"|\" || "
                                "case "
                                    "when B IS 0 then 'N' "
                                    "else 'S' end || \"|\" || "
                                "case "
                                    "when M IS 0 then 'N' "
                                    "else 'S' end || \"|\" || "
                                "case "
                                    "when penalta IS NULL then '' "
                                    "when penalta is 0 then 'N' "
                                    "when penalta is 1 then 'S' "
                                    "else penalta end || \"|\" || "
                                "case "
                                    "when penbaja IS NULL then '' "
                                    "when penbaja is 0 then 'N' "
                                    "when penbaja is 1 then 'S' "
                                    "else penbaja end "
                                "as novedad "
                       "from med_medicos "
                       "where id = %d and nodo = %@",
                                                    FormatoFecha,
                                                    FormatoFecha,
                                                    idMedico,
                                                    nodo];
    
    
    //NSLog(@"%@",query);
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    char *_c;
    NSString *result = @"";
	if ( statement ) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
        _c = (char *)sqlite3_column_text(statement, 0);
        if((_c != nil) && (strlen(_c) > 0)) {
            result = [NSString stringWithUTF8String:_c];
        }
        }
    }
    
    sqlite3_finalize(statement);
    
    return result;
}

-(void)save:(NSString *)objetivos
{
    EstadoVisita *estado = [EstadoVisita getByID:self.idEstado];
    
    NSString *nodo = SettingForKey(NODO, @"");
    NSString *sql = @"update med_medicos set estado = %d, visitasprogramadas = %d, visitasrealizadas = %d, actividad = %@, descestado = %@, objetivos = %@ where id=%d and nodo = %@";
    
    NSString *query = [NSString stringWithFormat:sql, self.idEstado, self.visitasProgramadas, self.visitasRealizadas,
                       [NSString stringQuoted:self.actividad ifNull:@"''"],
                       [NSString stringQuoted:estado.descripcion ifNull:@"''"],
                       [NSString stringQuoted:objetivos ifNull:@"''"],
                       self.idMedico, nodo];
    
    [[DB getInstance] excecuteSQL:query];
    
}

-(NSString*)isOkForSave
{
    /*
     
     Los datos obligatorios a ingresar son : Domicilio, horario, Apellido, Nombre, Una de las 2 matriculas, actividad, sexo, potencial, año de egreso
     
     Si domicilio no seleccionado
     Mensaje : Debe ingresar un domicilio de visita.
     
     Si Apellido en blanco
     Mensaje : Debe ingresar el apellido del médico.
     
     Si Nombre en blanco
     Mensaje : Debe ingresar el nombre del médico.
     
     Si sexo no seleccionado
     Debe seleccionar el sexo del médico.
     
     Se debe verificar que la/s matricula/s ingresada/s no exista/n :
     
     Para buscar matricula nacional :
     "select matriculanacional from med_medicos where id <> %d and matriculanacional = %d
     
     Para buscar matricula provincial :
     
     select matriculaprovincial from med_medicos med
     left join med_domicilios dom
     on med.nodo = dom.nodo and med.id = dom.medico and dom.utilidad = 1
     where med.id <> %d and matriculaprovincial = %d and provincia = %d
     
     Si matricula nacional  es diferente de cero y existe
     MENSAJE : Ya existe el N° de Matrícula Nacional en otro médico.
     Si matricula provincial es diferente de cero y  existe :
     MENSAJE : Ya existe el N° de Matrícula Provincial en otro médico.
     
     Si actividad no seleccionada
     MENSAJE :  Debe seleccionar la actividad.
     
     Si no se selecciona tratamiento
     MENSAJE : Debe seleccionar el tratamiento del médico
     
     Si no se selecciona potencial
     MENSAJE : Debe seleccionar el potencial del médico.
     
     Consultar el valor de la columna “VALOR” en la cfg_global por campo propiedad “CRT_EGRESO”.
     
     Si CRT_EGRESO = ‘S’
     Si anio egreso = 0 o a vacio
     MENSAJE : Debe ingresar el año de egreso.
     Sino
     Puede ingresar 0 o vacio.
     
     Si ingresa un valor
     Si Año de egreso es menor a 1900 o mayor o igual al año actual mostrar mensaje MENSAJE : Año de egreso inválido.
     
     Consultar el valor de la columna “VALOR” en la cfg_global por campo propiedad “CRT_OBRASOCIAL”.
     
     Si CRT_OBRASOCIAL = ‘S’
     Si no cargo ninguna obra social para el medico
     MENSAJE : Debe ingresar al menos una obra social. 
     
     Si no selecciono ninguna especialidad
     MENSAJE: Debe ingresar al menos una especialidad.
     
     */
    
    return @"OK";
}
-(void)saveAllData
{
        
    /*
     
     Se deberá:
     * Insertar el médico med_medicos
     * Insertar las especialidades med_especialidades
     * Insertar las obras sociales med_obrassociales
     * Insertar los domicilios med_domicilios
     * Por cada domicilio:
	 *    Insertar horarios med_horarios
	 *    Insertar teléfonos med_telefonos
     
     *en todos los insert de la log_novedades hay que grabar en el campo secuencia el max(secuencia) + 1
     
     Insert a la MED_MEDICOS :
     
     "nodo" = nodo variable global
     "id" = max id de med medico por nodo
     "apellido" = apellido ingresado
     "nombre"= nombre ingresado
     "sexo" = sexo seleccionado (Masculino =’M’ Femenino ‘F’)
     "nacimiento" = null
     
     Si cuit vacio
     Cuit = null
     Sino
     cuit = cuit ingresado
     
     "matriculanacional” = matricula nacional ingresada
     "matriculaprovincial" = matricula provincial ingresada
     Si Egreso es vacio , grabar cero sino valor ingresado
     
     Si email vacio
     email = null
     Sino
     email = email ingresado
     
     
     "codigointerno" = null
     “asignacion" = 1
     "actividad” = ‘M’ mañana ; ‘T’ tarde
     "tratamiento" = id de tratamiento seleccionado
     "visitasprogramadas" = 1 + valor ingresado en revisitas.
     "visitasrealizadas" = 0
     “estado" = 2
     "fechabaja” = null
     "motivobaja" = null
     "idpool" = null
     "fechaalta", fecha del dia
     "objetivos" = null;
     "marca" = 0
     "A"= 1
     "B"= 0
     "M"= 0
     "cp" = código postal del domicilio seleccionado
     "localidad" = descripción de la localidad del domicilio seleccionado
     "calle" = descripción de la calle del domicilio seleccionado
     "altura" = altura del domicilio seleccionado
     "piso" = si no se ingreso piso = null sino valor ingresado
     "dpto" = si no se ingreso dpto = null sino valor ingresado
     "especialidad1" = campo sigla de la mtc_especialidad seleccionada
     especialidad2" =
     si selecciono mas de una especialidad = de la mtc_especialidad campo sigla de la 2da posición de las especialidades asignadas
     sino
     grabar null
     
     "desde", hora_desde del horario seleccionado
     "hasta", hora_hasta del horario seleccionado
     "comentarios" = si hay comentarios hechos en el horario seleccionado
     Sino
     "comentarios" = null
     "lu"  = si lunes seleccionado 1 sino 0
     "ma"  = si martes seleccionado 1 sino 0
     "mi"  = si miercoles seleccionado 1 sino 0
     "ju"  = si jueves seleccionado 1 sino 0
     "vi"  = si viernes seleccionado 1 sino 0
     
     "lugar" = descripción de tipo de domicilio seleccionado
     "descestado" = “NO VISTO”
     "descABM" = “ALTA”
     potencial" =  id potencial seleccionado
     descpotencial =  descripción de potencial seleccionado;
     "desctratamiento" = descripción de tratamiento seleccionado
     "descmarcado" = null;
     
     Consultar el valor de la columna “VALOR” en la cfg_global por campo propiedad “USAAUTORIZACION”.
     
     Si USAAUTORIZACION = ‘S’
     “penalta” = 1
     Sino
     “penalta” = 0
     
     “penbaja" = 0
     
     Grabar novedad en la log_novedades para la MED_MEDICOS
     Tabla : MED_MEDICOS
     Tipo : ‘I’
     Registro : nodo|id|apellido|nombre|descmarcado|sexo|(nacimiento==-1?"":nacimiento (mm/dd/yyyy)|cuit|
     matriculanacional|matriculaprovincial|egreso==-1?"":egreso|email|codigointerno|asignacion|actividad|
     potencial|tratamiento|visitasprogramadas|visitasrealizadas|estado|fechabaja==-1?"":fechabaja|
     fechabaja==-1?"":motivobaja|idpool==-1?"":idpool|fechaalta|objetivos|marca|(A==0?"N":"S")|
     (B==0?"N":"S")|(M==0?"N":"S")|(penalta==0?"N":"S"|(penbaja==0?"N":"S")
     TranId : 0
     Transmitido : 0
     
     
     
     Insert a la GST_GRUPOMEDICOS:
     
     Grabar registro en la gst_grupoMedicos :
     
     "nodo" = nodo variable global
     "grupo" = 1.
     "medico" = id de medico
     
     Grabar novedad en la log_novedades para la GST_GRUPOMEDICOS
     Tabla : GST_GRUPOMEDICOS
     Tipo : ‘I’
     Registro : Nodo|grupo|medico (es decir los valores grabados separados por pipes
     TranId : 0
     Transmitido : 0
     
     Si estoy trabajando en otro grupo que no sea el 1 que es la cartera de médicos se graba también otro registro en la gst_grupomedicos.
     (Breve reseña del uso de selección de grupo.
     Archivo – selección de grupo : Se muestra los médicos de ese grupo, lease que la opción de cartera de médicos es como un acceso directo seleccionando el grupo 1 )
     
     Si idGrupo > 1 entonces
     
     "nodo" = nodo variable global
     "grupo" = idGrupo
     "medico" = id de medico
     
     
     Grabar novedad en la log_novedades para la GST_GRUPOMEDICOS
     Tabla : GST_GRUPOMEDICOS
     Tipo : ‘I’
     Registro : Nodo|grupo|medico (es decir los valores grabados separados por pipes
     TranId : 0
     Transmitido : 0
     
     Fin si
     
     
     
     
     Insert a la MED_ESPECIALIDADES: (tabla relación nodo, medico, especialidad)
     
     Iterar entre las especialidades cargadas
     
     "nodo"   = nodo variable global
     "medico" = id de medico
     "orden"  = Orden en el que se ubico la especialidad
     "especialidad" = idEspecialidad de la especialidad seleccionada obtenido de la mtc_especialidades
     
     
     Grabar novedad en la log_novedades para la GST_GRUPOMEDICOS
     Tabla : MED_ESPECIALIDADES
     Tipo : ‘I’
     Registro : Nodo|medico|orden|especialidad (es decir los valores grabados separados por pipes)
     TranId : 0
     Transmitido : 0
     
     
     Insert a la MED_OBRASSOCIALES (tabla relación nodo, medico, obra social)
     
     Iterar entre las obras sociales seleccionadas :
     
     "nodo"   = nodo variable global
     "medico" = id de medico
     "obrasocial" = idObraSocial de la obra social seleccionada obtenido de la mtc_obrassociales
     
     Grabar novedad en la log_novedades para la MED_OBRASSOCIALES
     Tabla : MED_OBRASSOCIALES
     Tipo : ‘I’
     Registro : Nodo|medico|orden|obrasocial (es decir los valores grabados separados por pipes)
     TranId : 0
     Transmitido : 0
     
     Insert a la MED_DOMICILIOS (tabla relación nodo, medico, domicilios)
     
     Iterar entre las direcciones cargadas :
     
     "nodo" = nodo variable global);
     "medico", id de medico
     "id",  = max id +1 para nodo y medico en la tabla med_domicilios
     "pais", país = id de país variable global
     "cp", = código postal cargado en la pantalla de domicilios
     "provincia" = provincia = id de provincia cargado en el domicilio
     "localidad" = descripción de la localidad seleccionada en carga domicilio
     "calle", = descripción de la calle cargada en la pantalla de domicilios
     "altura” =  altura de la calle cargada en la pantalla de domicilios
     
     Si piso <1
     "piso” = 0
     Sino
     "piso” = piso del depto cargado en la pantalla de domicilios
     
     
     Si departamento = vacio
     Departamento = null
     Sino
     Departamento = departamento cargado en la pantalla de domicilios
     
     "tipo” = id del tipo de domicilio seleccionado tomado de la mtc_tiposdomicilio
     
     "utilidad” = Id de la utilidad seleccionado tomado de la mtc_utilidadesdomicilio
     
     Si no se selecciono {
     "institucion" = null
     sino
     "institucion", id institución tomado de la mtc_instituciones
     }
     Si no se selecciono CARGO
     cargo= null
     sino
     "cargo" = id de cargo seleccionado
     
     Si tilde EXTERNO seleccionado
     "externo" =  1
     Sino
     “externo" = 0
     
     
     Grabar novedad en la log_novedades para la MED_DOMICILIOS
     Tabla : MED_DOMICILIOS
     Tipo : ‘I’
     Registro : nodo|medico|id|pais|cp.trim()|provincia|localidad.trim()|calle.trim()|altura|
     (piso<1?"0":piso)|departamento.trim()|tipo|utilidad|(institucion==-1?"":institucion)|
     (cargo==-1||cargo==0?"":cargo)|externo==1?"S":"N"
     TranId : 0
     Transmitido : 0
     
     
     
     Insert a la MED_HORARIOS (tabla relación nodo, medico, horario)
     
     Iterar sobre los horarios cargados
     
     "nodo" = nodo variable global);
     "medico", id de medico
     "domicilio" =  iddomicilio (asignado en el paso anterior)
     "id" = max id +1 para nodo, medico, domicilio en la tabla med_horarios
     "desde" =  desde (hora seleccionada en el horario)
     "hasta" = hasta  (hora seleccionada en el horario)
     Si comentarios del horario = vacio
     Comentarios = null
     Sino
     Comentarios = Comentarios hechos al horario
     "lu"  = si lunes seleccionado 1 sino 0
     "ma"  = si martes seleccionado 1 sino 0
     "mi"  = si miercoles seleccionado 1 sino 0
     "ju"  = si jueves seleccionado 1 sino 0
     "vi"  = si viernes seleccionado 1 sino 0
     
     
     Grabar novedad en la log_novedades para la MED_HORARIOS
     Tabla : MED_HORARIOS
     Tipo : ‘I’
     Registro : nodo|medico|domicilio|id|desde|hasta|comentarios.trim()|(lunes==1?"S":"N")|
     (martes==1?"S":"N")|(miercoles==1?"S":"N")|(jueves==1?"S":"N")|(viernes==1?"S":"N")
     TranId : 0
     Transmitido : 0
     
     
     Insert a la MED_TELEFONOS (tabla relación nodo, medico, telefonos)
     
     
     "nodo" = nodo variable global);
     "medico", id de medico
     "domicilio" =  iddomicilio (asignado en el paso anterior)
     "numero” = numero de teléfono ingresado
     "pais", país = id de país variable global
     "area", area = Nro de area ingresada
     
     Grabar novedad en la log_novedades para la MED_TELEFONOS
     
     Tabla : MED_TELEFONOS
     Tipo : ‘I’
     Registro : nodo|medico|domicilio|pais|area|numero
     TranId : 0
     Transmitido : 0
     
     //este caso podría obviarse según como se haga, en android lo hacemos para el siguiente caso : el usuario ingresa un domicilio, le asigna un horario todo dentro del alta ed domicilio.
     Luego vuelve a la pantalla ppal de alta de medico y desde esta asigna otro horario. El caso del insert este seria para dar de alta el horario sin modificar la med_domicilios.
     
     
     Insert a la MED_HORARIOS (tabla relación nodo, medico, horario)
     
     Iterar sobre los horarios cargados
     
     "nodo" = nodo variable global);
     "medico", id de medico
     "domicilio" =  iddomicilio (asignado en el paso anterior)
     "id" = max id +1 para nodo, medico, domicilio en la tabla med_horarios
     "desde" =  desde (hora seleccionada en el horario)
     "hasta" = hasta  (hora seleccionada en el horario)
     Si comentarios del horario = vacio
     Comentarios = null
     Sino
     Comentarios = Comentarios hechos al horario
     "lu"  = si lunes seleccionado 1 sino 0
     "ma"  = si martes seleccionado 1 sino 0
     "mi"  = si miercoles seleccionado 1 sino 0
     "ju"  = si jueves seleccionado 1 sino 0
     "vi"  = si viernes seleccionado 1 sino 0
     
     
     Grabar novedad en la log_novedades para la MED_HORARIOS
     Tabla : MED_HORARIOS
     Tipo : ‘I’
     Registro : nodo|medico|domicilio|id|desde|hasta|comentarios.trim()|(lunes==1?"S":"N")|
     (martes==1?"S":"N")|(miercoles==1?"S":"N")|(jueves==1?"S":"N")|(viernes==1?"S":"N")
     TranId : 0
     Transmitido : 0 
     
     
     Se guarda todo y se retorna a la cartera 
     
     
     
     
     */
    
    
}



-(void)updateAllData
{
    
    /*
     
     UPDATE A LA MED_MEDICOS DE TODO EL REGISTRO
     WHERE NODO = NODO VARIABLE GLOBAL  AND ID = ID MEDICO SELECCIONADO
     
     Grabar novedad en la log_novedades para la MED_MEDICOS
     
     Tabla : MED_MEDICOS
     Tipo : ‘U’
     Registro : nodo|id|apellido|nombre|descmarcado|sexo|(nacimiento==-1?"":nacimiento (mm/dd/yyyy)|cuit|
     matriculanacional|matriculaprovincial|egreso==-1?"":egreso|email|codigointerno|asignacion|actividad|
     potencial|tratamiento|visitasprogramadas|visitasrealizadas|estado|fechabaja==-1?"":fechabaja|
     fechabaja==-1?"":motivobaja|idpool==-1?"":idpool|fechaalta|objetivos|marca|(A==0?"N":"S")|
     (B==0?"N":"S")|(M==0?"N":"S")|(penalta==0?"N":"S"|(penbaja==0?"N":"S")
     TranId : 0
     Transmitido : 0
     
     
     Lo que hace es borrar los registros y volver a grabar:
     (Conviene traerse el registro de novedad antes de borrar)
     
     Grabar novedad en la log_novedades para la MED_ESPECIALIDADES
     
     Tabla : MED_ESPECIALIDADES
     Tipo : ‘D’
     Registro : Nodo|medico|orden|especialidad
     TranId : 0
     Transmitido : 0
     
     
     DELETE FROM MED_ESPECIALIDADES WHERE NODO = NODO VARIABLE GLOBAL  AND ID = ID MEDICO SELECCIONADO
     
     
     
     
     Grabar novedad en la log_novedades para la MED_OBRASSOCIALES
     
     Tabla : MED_OBRASSOCIALES
     Tipo : ‘D’
     Registro : Nodo|medico|orden|obrasocial
     TranId : 0
     Transmitido : 0
     
     DELETE FROM MED_OBRASSOCIALES WHERE NODO = NODO VARIABLE GLOBAL  AND MEDICO = ID MEDICO SELECCIONADO
     
     
     Grabar novedad en la log_novedades para la MED_HORARIOS
     
     Tabla : MED_HORARIOS
     Tipo : ‘D’
     Registro : nodo|medico|domicilio|id|desde|hasta|comentarios.trim()|(lunes==1?"S":"N")|
     (martes==1?"S":"N")|(miercoles==1?"S":"N")|(jueves==1?"S":"N")|(viernes==1?"S":"N")
     TranId : 0
     Transmitido : 0
     
     DELETE FROM MED_HORARIOS WHERE NODO = NODO VARIABLE GLOBAL  AND MEDICO = ID MEDICO SELECCIONADO AND DOMICILIO = ID TIPO DOMICILIO
     
     
     
     Grabar novedad en la log_novedades para la MED_TELEFONOS
     
     Tabla : MED_TELEFONOS
     Tipo : ‘D’
     Registro : nodo|medico|domicilio|pais|area|numero
     TranId : 0
     Transmitido : 0
     
     
     DELETE FROM MED_TELEFONOS WHERE NODO = NODO VARIABLE GLOBAL  AND MEDICO = ID MEDICO SELECCIONADO AND DOMICILIO = ID TIPO DOMICILIO
     
     
     
     
     
     DELETE DE LA MED_DOMICILIOS
     
     Grabar novedad en la log_novedades para la MED_DOMICILIOS
     Tabla : MED_DOMICILIOS
     Tipo : ‘D’
     Registro : nodo|medico|id|pais|cp.trim()|provincia|localidad.trim()|calle.trim()|altura|
     (piso<1?"0":piso)|departamento.trim()|tipo|utilidad|(institucion==-1?"":institucion)|
     (cargo==-1||cargo==0?"":cargo)|externo==1?"S":"N"
     TranId : 0
     Transmitido : 0
     
     DELETE FROM MED_DOMICILIOS WHERE NODO = NODO VARIABLE GLOBAL  AND MEDICO = ID MEDICO SELECCIONADO
     
     
     
     Terminados los delete, se hacen los insert.
     
     Es exactamente al alta de medico
     
     Insert a la MED_ESPECIALIDADES: (tabla relación nodo, medico, especialidad)
     
     Iterar entre las especialidades cargadas
     
     "nodo"   = nodo variable global
     "medico" = id de medico
     "orden"  = Orden en el que se ubico la especialidad
     "especialidad" = idEspecialidad de la especialidad seleccionada obtenido de la mtc_especialidades
     
     
     Grabar novedad en la log_novedades para la GST_GRUPOMEDICOS
     Tabla : MED_ESPECIALIDADES
     Tipo : ‘I’
     Registro : Nodo|medico|orden|especialidad (es decir los valores grabados separados por pipes)
     TranId : 0
     Transmitido : 0
     
     
     Insert a la MED_OBRASSOCIALES (tabla relación nodo, medico, obra social)
     
     Iterar entre las obras sociales seleccionadas :
     
     "nodo"   = nodo variable global
     "medico" = id de medico
     "obrasocial" = idObraSocial de la obra social seleccionada obtenido de la mtc_obrassociales
     
     Grabar novedad en la log_novedades para la MED_OBRASSOCIALES
     Tabla : MED_OBRASSOCIALES
     Tipo : ‘I’
     Registro : Nodo|medico|orden|obrasocial (es decir los valores grabados separados por pipes)
     TranId : 0
     Transmitido : 0
     
     Insert a la MED_DOMICILIOS (tabla relación nodo, medico, domicilios)
     
     Iterar entre las direcciones cargadas :
     
     "nodo" = nodo variable global);
     "medico", id de medico
     "id",  = max id +1 para nodo y medico en la tabla med_domicilios
     "pais", país = id de país variable global
     "cp", = código postal cargado en la pantalla de domicilios
     "provincia" = provincia = id de provincia cargado en el domicilio
     "localidad" = descripción de la localidad seleccionada en carga domicilio
     "calle", = descripción de la calle cargada en la pantalla de domicilios
     "altura” =  altura de la calle cargada en la pantalla de domicilios
     
     Si piso <1
     "piso” = 0
     Sino
     "piso” = piso del depto cargado en la pantalla de domicilios
     
     
     Si departamento = vacio
     Departamento = null
     Sino
     Departamento = departamento cargado en la pantalla de domicilios
     
     "tipo” = id del tipo de domicilio seleccionado tomado de la mtc_tiposdomicilio
     
     "utilidad” = Id de la utilidad seleccionado tomado de la mtc_utilidadesdomicilio
     
     Si no se selecciono {
     "institucion" = null
     sino
     "institucion", id institución tomado de la mtc_instituciones
     }
     Si no se selecciono CARGO
     cargo= null
     sino
     "cargo" = id de cargo seleccionado
     
     Si tilde EXTERNO seleccionado
     "externo" =  1
     Sino
     “externo" = 0
     
     
     Grabar novedad en la log_novedades para la MED_DOMICILIOS
     Tabla : MED_DOMICILIOS
     Tipo : ‘I’
     Registro : nodo|medico|id|pais|cp.trim()|provincia|localidad.trim()|calle.trim()|altura|
     (piso<1?"0":piso)|departamento.trim()|tipo|utilidad|(institucion==-1?"":institucion)|
     (cargo==-1||cargo==0?"":cargo)|externo==1?"S":"N"
     TranId : 0
     Transmitido : 0
     
     
     
     Insert a la MED_HORARIOS (tabla relación nodo, medico, horario)
     
     Iterar sobre los horarios cargados
     
     "nodo" = nodo variable global);
     "medico", id de medico
     "domicilio" =  iddomicilio (asignado en el paso anterior)
     "id" = max id +1 para nodo, medico, domicilio en la tabla med_horarios
     "desde" =  desde (hora seleccionada en el horario)
     "hasta" = hasta  (hora seleccionada en el horario)
     Si comentarios del horario = vacio
     Comentarios = null
     Sino
     Comentarios = Comentarios hechos al horario
     "lu"  = si lunes seleccionado 1 sino 0
     "ma"  = si martes seleccionado 1 sino 0
     "mi"  = si miercoles seleccionado 1 sino 0
     "ju"  = si jueves seleccionado 1 sino 0
     "vi"  = si viernes seleccionado 1 sino 0
     
     
     Grabar novedad en la log_novedades para la MED_HORARIOS
     Tabla : MED_HORARIOS
     Tipo : ‘I’
     Registro : nodo|medico|domicilio|id|desde|hasta|comentarios.trim()|(lunes==1?"S":"N")|
     (martes==1?"S":"N")|(miercoles==1?"S":"N")|(jueves==1?"S":"N")|(viernes==1?"S":"N")
     TranId : 0
     Transmitido : 0
     
     
     Insert a la MED_TELEFONOS (tabla relación nodo, medico, telefonos)
     
     
     "nodo" = nodo variable global);
     "medico", id de medico
     "domicilio" =  iddomicilio (asignado en el paso anterior)
     "numero” = numero de teléfono ingresado
     "pais", país = id de país variable global
     "area", area = Nro de area ingresada
     
     Grabar novedad en la log_novedades para la MED_TELEFONOS
     
     Tabla : MED_TELEFONOS
     Tipo : ‘I’
     Registro : nodo|medico|domicilio|pais|area|numero
     TranId : 0
     Transmitido : 0
     
     //este caso podría obviarse según como se haga, en android lo hacemos para el siguiente caso : el usuario ingresa un domicilio, le asigna un horario todo dentro del alta ed domicilio.
     Luego vuelve a la pantalla ppal de alta de medico y desde esta asigna otro horario. El caso del insert este seria para dar de alta el horario sin modificar la med_domicilios.
     
     
     Insert a la MED_HORARIOS (tabla relación nodo, medico, horario)
     
     Iterar sobre los horarios cargados
     
     "nodo" = nodo variable global);
     "medico", id de medico
     "domicilio" =  iddomicilio (asignado en el paso anterior)
     "id" = max id +1 para nodo, medico, domicilio en la tabla med_horarios
     "desde" =  desde (hora seleccionada en el horario)
     "hasta" = hasta  (hora seleccionada en el horario)
     Si comentarios del horario = vacio
     Comentarios = null
     Sino
     Comentarios = Comentarios hechos al horario
     "lu"  = si lunes seleccionado 1 sino 0
     "ma"  = si martes seleccionado 1 sino 0
     "mi"  = si miercoles seleccionado 1 sino 0
     "ju"  = si jueves seleccionado 1 sino 0
     "vi"  = si viernes seleccionado 1 sino 0
     
     
     Grabar novedad en la log_novedades para la MED_HORARIOS
     Tabla : MED_HORARIOS
     Tipo : ‘I’
     Registro : nodo|medico|domicilio|id|desde|hasta|comentarios.trim()|(lunes==1?"S":"N")|
     (martes==1?"S":"N")|(miercoles==1?"S":"N")|(jueves==1?"S":"N")|(viernes==1?"S":"N")
     TranId : 0
     Transmitido : 0 
     
     
     Se guarda todo y se retorna a la cartera
     
     
    
     */
    
}

@end
