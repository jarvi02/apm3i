//
//  MTstdVisitasPorDia.h
//  APM4
//
//  Created by Laura Busnahe on 9/11/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum {
    kMTActivityNone,
    kMTActivityAll,
    kMTActivityMorning,
    kMTActivityAfternon    
    } MTActivitySelection;

@interface MTstdVisitasPorDia : NSObject

@property (nonatomic, assign)NSUInteger idVisita;
@property (nonatomic, strong)NSDate     *fecha;
@property (nonatomic, assign)NSInteger  fijos;
@property (nonatomic, assign)NSInteger  fluctuantes;
@property (nonatomic, assign)NSInteger  acompaniadas;
@property (nonatomic, assign)NSInteger  total;


+ (NSArray*)getAllForActivity:(MTActivitySelection) actividad;

@end
