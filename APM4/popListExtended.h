//
//  popListExtended.h
//  APM4
//
//  Created by Laura Busnahe on 6/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "popListView.h"

@interface MTpopListView (extensions)

- (id)initForProvinciasWithDelegate:(id)idDelegate;
- (id)initForLocalidadesForProvincia:(NSUInteger)idProvincia WithDelegate:(id)idDelegate;
- (id)initForEspecialidadesWithDelegate:(id)idDelegate;
- (id)initForTiposDomiciliosWithDelegate:(id)idDelegate;
- (id)initForTratamientosWithDelegate:(id)idDelegate;
- (id)initForMotivoBajaWithDelegate:(id)idDelegate;

@end
