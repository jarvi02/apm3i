//
//  MTstdDistribucionCantidad.h
//  APM4
//
//  Created by Laura Busnahe on 9/9/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTstdDistribucionCantidad : NSObject

@property (nonatomic, strong)NSString   *descripcion;
@property (nonatomic, assign)NSInteger  maniana;
@property (nonatomic, assign)NSInteger  tarde;
@property (nonatomic, assign)NSInteger  total;
@property (nonatomic, assign)double     porcManiana;
@property (nonatomic, assign)double     porcTarde;
@property (nonatomic, assign)double     porcTotal;

+ (NSArray*)getAll;

@end
