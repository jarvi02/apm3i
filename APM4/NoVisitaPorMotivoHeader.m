//
//  NoVisitaPorMotivoHeader.m
//  APM4
//
//  Created by Laura Busnahe on 9/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "NoVisitaPorMotivoHeader.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define colorASC     0xBBE872
#define colorDESC    0xFA7372
#define colorNormal  0xFFFFFF

#pragma mark -
@implementation NoVisitaPorMotivoHeader
{
    tNoVisitaPorMotivoHeaderField  orderedBy;
    NSComparisonResult             order;
}

#pragma mark Private methods
- (id)init
{
    self = [super init];
    if (self)
    {
        self.delegate = nil;
        orderedBy = fNoVisitaPorMotivoFieldMotivo;
        order     = NSOrderedAscending;
    }
    
    return self;
}

- (void)orderBy:(tNoVisitaPorMotivoHeaderField)field sender:(id)sender reset:(BOOL)reset
{
    // Chequeo si el delegado fue asignado.
    if (!self.delegate)
        return;
    
    // Chequeo si se trata del mismo campo por el que ya estaba ordenado.
    if (orderedBy != field)
    {
        // Si no es igual, asigno el nuevo campo y cambio el orden a ascendete.
        orderedBy = field;
        order     = NSOrderedAscending;
    } else
    {
        // Si es el mismo campo, cambio el orden.
        if (order == NSOrderedAscending)
            order = NSOrderedDescending;
        else
            order = NSOrderedAscending;
    }
    
    if (reset)
        order = NSOrderedAscending;
    
    // Coloreo el label seleccionado.
    [self colorLabels:sender];
    
    // Llamo el método del delegate.
    if ([self.delegate respondsToSelector:@selector(NoVisitaPorMotivoHeader:selectedField:Order:)])
        [self.delegate NoVisitaPorMotivoHeader:self selectedField:orderedBy Order:order];
}

- (void)colorLabels:(id)sender
{
    NSUInteger newColor = order == NSOrderedAscending ? colorASC : colorDESC;
    
    [self.lblMotivo  setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblManiana setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblTarde   setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    [self.lblTotal   setTitleColor:UIColorFromRGB(colorNormal) forState:UIControlStateNormal];
    
    UIButton *o = (UIButton*)sender;
    if ([o respondsToSelector:@selector(setTitleColor:forState:)])
        [o setTitleColor:UIColorFromRGB(newColor) forState:UIControlStateNormal];
    
}

#pragma mark -
#pragma mark IBAction methods
- (IBAction)tapLabel:(id)sender
{
    tNoVisitaPorMotivoHeaderField field = fNoVisitaPorMotivoFieldMotivo;
    
    if (sender == self.lblMotivo)
        field = fNoVisitaPorMotivoFieldMotivo;
    
    if (sender == self.lblManiana)
        field = fNoVisitaPorMotivoFieldManiana;
    
    if (sender == self.lblTarde)
        field = fNoVisitaPorMotivoFieldTarde;
    
    if (sender == self.lblTotal)
        field = fNoVisitaPorMotivoFieldTotal;
    
    [self orderBy:field sender:sender reset:NO];
}

#pragma mark -
@end
