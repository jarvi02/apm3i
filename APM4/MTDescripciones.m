//
//  MTDescripciones.m
//  APM4
//
//  Created by Laura Busnahe on 9/16/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTDescripciones.h"
#import "Config.h"
#import "DBExtended.h"

@implementation MTDescripciones
- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.recID      = 0;
        self.descripcion= @"";
    }
    
    return self;
}

- (NSString*) getDescription
{
    NSString* newDescription = [NSString stringWithFormat:@"%d - %@",
                                    self.recID,
                                    [NSString string:self.descripcion]];
    
    return newDescription;
}

- (MTDescripciones*)copyWithZone:(NSZone*)zone
{
    MTDescripciones* newCopy; // = [[MTEntidad alloc] init];
    
    newCopy = [[MTDescripciones alloc] init];
    
    newCopy.recID      = self.recID;
    newCopy.descripcion= [NSString string:self.descripcion ifNull:@""];
    
    return newCopy;
}

+ (NSArray*)getAllVisita
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql =
    @"SELECT id, descripcion "
     "FROM mtc_estadosvisita "
     "ORDER BY descripcion";
    
    NSString *query = [NSString string:sql];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    char *_c;
	
    if ( statement )
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTDescripciones* o = [[MTDescripciones alloc] init];
            
            // id
            o.recID = sqlite3_column_int(statement, 0);
            
            // descripcion
            _c = (char*)sqlite3_column_text(statement, 1);
            o.descripcion = [NSString pCharToString:_c];
            
            [result addObject:o];
        }
    }
    
    sqlite3_finalize(statement);
    
    
    return result;
}

+ (NSArray*)getAllPotencial
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSString *sql =
    @"SELECT id, descripcion "
    "FROM mtc_potenciales "
    "ORDER BY descripcion";
    
    NSString *query = [NSString string:sql];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    char *_c;
	
    if ( statement )
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            MTDescripciones* o = [[MTDescripciones alloc] init];
            
            // id
            o.recID = sqlite3_column_int(statement, 0);
            
            // descripcion
            _c = (char*)sqlite3_column_text(statement, 1);
            o.descripcion = [NSString pCharToString:_c];
            
            [result addObject:o];
        }
    }
    
    sqlite3_finalize(statement);
    
    
    return result;
}

+ (NSString*)getDescriptionById:(NSUInteger)descripcion fromArray:(NSArray*)array
{
    NSString *result = @"";
    
    for (MTDescripciones *o in array)
    {
        if (o.recID == descripcion)
            return o.descripcion;
    }
    
    
    return result;
}

@end
