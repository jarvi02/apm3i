//
//  AjusteMMViewController.h
//  APM4
//
//  Created by Laura Busnahe on 3/27/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "customAjusteMMCell.h"
#import "customAjusteMMHeader.h"
#import "MTMuestrasMedicas.h"
#import "MTAjustes.h"
#import "popListView.h"


@interface AjusteMMViewController : CustomViewController <MTpopListViewDelegate,
                                                          UITableViewDataSource, UITableViewDelegate,
                                                          UISearchBarDelegate,
                                                          UIAlertViewDelegate,
                                                          UITextFieldDelegate>
{
    NSArray     *ajustesArray;
}

@property (nonatomic, strong) NSArray               *data;              // Datos en crudo de la TableView.
@property (nonatomic, strong) NSMutableArray        *dataFiltered;      // Datos filtrados de la TableView.

@property (nonatomic, retain) MTpopListView                 *popMenu;       // Popup con el listado de motivos.
@property (nonatomic, strong) IBOutlet UIView               *popupView;     // "Modal" view con la vista de edición de la muestra.
@property (nonatomic, strong) IBOutlet UIView               *popupShade;    // View oscura de fondo utilizada para sombrear cuando aparece el
                                                                            // popupView.
@property (retain, nonatomic) IBOutlet UISearchBar          *searchBar;
@property (nonatomic, strong) IBOutlet UITableView          *table;
@property (nonatomic, strong) IBOutlet customAjusteMMCell   *tmpCell;
@property (retain, nonatomic) IBOutlet customAjusteMMHeader *tableHeader;


@property (nonatomic, strong) UINib             *cellNIB;
@property (nonatomic, retain) UINib             *headerNIB;
@property (nonatomic, strong) MTMuestrasMedicas *seleccion;
@property (nonatomic, strong) MTAjustes         *ajusteSel;

// Outlets Edición.
@property (retain, nonatomic) IBOutlet UIButton     *btnMotivo;
@property (nonatomic, strong) IBOutlet UITextField  *edtProducto;
@property (nonatomic, strong) IBOutlet UITextField  *edtMotivo;
@property (nonatomic, strong) IBOutlet UITextField  *edtCantidad;
@property (retain, nonatomic) IBOutlet UILabel      *lblDescProducto;
@property (retain, nonatomic) IBOutlet UILabel      *lblLote;
@property (retain, nonatomic) IBOutlet UILabel      *lblStock;


// Methods
- (id)initViewDefaultNib;
//- (id)initViewDefaultNibForSelection:(MTProducto*)selProducto;
- (void)showEditView:(BOOL)show;


// IBActions
- (IBAction)btnAceptar: (id)sender;
- (IBAction)btnCancelar:(id)sender;
- (IBAction)tapMotivo:  (id)sender;

- (IBAction)tapOther:  (id)sender;

@end
