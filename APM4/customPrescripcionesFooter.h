//
//  customPrescripcionesFooter.h
//  APM4
//
//  Created by Ezequiel on 3/20/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customPrescripcionesFooter : UIView

@property (retain, nonatomic) IBOutlet UILabel *lblTrimestre1;
@property (retain, nonatomic) IBOutlet UILabel *lblTrimestre2;
@property (retain, nonatomic) IBOutlet UILabel *lblTrimestre3;
@property (retain, nonatomic) IBOutlet UILabel *lblTrimestre4;
@property (retain, nonatomic) IBOutlet UILabel *lblTotal;
@end
