//
//  MTMercados.h
//  APM4
//
//  Created by Ezequiel Manacorda on 2/28/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTMercados : NSObject

@property                    NSInteger  auditoria;
@property                    NSInteger  mercado;
@property(nonatomic, retain) NSString*  descripcion;
@property                    NSInteger  total;
@property                    NSInteger  totalLaboratorio;

@property                    BOOL       selected;


- (NSString*)getDescription;


+ (NSArray*)getByAuditoria:(NSInteger) idAuditoria;
+ (NSArray*)getForMedico:(NSInteger)idMedico Auditoria:(NSInteger)idAuditoria;
+ (NSString*)getSelectedIdsString:(NSArray*)arrayMercados;

@end
