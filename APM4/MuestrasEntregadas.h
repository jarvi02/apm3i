//
//  MuestrasEntregadas.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 28/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MuestrasEntregadas : NSObject

@property(nonatomic,assign) NSInteger fecha;
@property(nonatomic,retain) NSString  *apellidoNombre;
@property(nonatomic,retain) NSString  *descripcion;
@property(nonatomic,retain) NSString  *codigo;
@property(nonatomic,retain) NSString  *lote;
@property(nonatomic,assign) NSInteger  muestras;
@property(nonatomic,assign) NSInteger  fecha_vto;

+(NSArray*)GetAll;


@end
