//
//  UIViewController+PopOverSize.m
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "UIViewController+PopOverSize.h"

@implementation UIViewController (PopOverSize)

- (void) forcePopoverSize {
    CGSize currentSetSizeForPopover = self.contentSizeForViewInPopover;
    CGSize fakeMomentarySize = CGSizeMake(currentSetSizeForPopover.width - 1.0f, currentSetSizeForPopover.height - 1.0f);
    self.contentSizeForViewInPopover = fakeMomentarySize;
    self.contentSizeForViewInPopover = currentSetSizeForPopover;
}

@end
