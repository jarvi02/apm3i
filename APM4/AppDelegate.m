//
//  AppDelegate.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 24/01/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "AppDelegate.h"
#import "MasterViewController.h"
#import "CarteraViewController.h"
#import "TransmitirViewController.h"
#import "Config.h"
#import "DBExtended.h"
#import "NSString+Validaciones.h"

#import "ProcesoTransmicion.h"
#import "commandMasterView.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>



@implementation AppDelegate

@synthesize popoverController;

- (void)dealloc
{
    [_window release];
    [popoverController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Fabric with:@[CrashlyticsKit]];


#ifdef USE_NEW_MENU
    [[commandMasterView sharedInstance] addButtons:@[[CommandButton createButtonWithImage:[UIImage imageNamed:@"cartera_48"]
                                                                               andTitle:@"Archivo"
                                                                       andMenuListItems:@[
                                                    @"Cartera de Médicos",
                                                    @"Selección de Grupos",
                                                    @"Grupos",
                                                    @"Seleccionar Zona",
                                                    @"Previsto Mensual",
                                                    @"Parte Diario"]],
                                                    [CommandButton createButtonWithImage:[UIImage imageNamed:@"estadistica"]
                                                                                andTitle:@"Estadísticas"
                                                                        andMenuListItems:@[
                                                     @"Médicos visitados por Fecha",
                                                     @"Muestras entregadas",
                                                     @"Stock MM"]],
                                                    [CommandButton createButtonWithImage:[UIImage imageNamed:@"config"]
                                                                                andTitle:@"Herramientas"
                                                                        andMenuListItems:@[
                                                     @"Transmisión",
                                                     @"Aplicar cierre de ciclo",
                                                     @"Revertir cierre de ciclo",
                                                     @"Configuración",
                                                     @"Ajuste MM",
                                                     @"Acerca Nuestro"]]]
                                        forGroup:@"MainGroup"];
#endif
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    
    
    
    MasterViewController *masterViewController = [[[MasterViewController alloc] initWithNibName:@"MasterViewController" bundle:nil] autorelease];
    
    self.navigationController = [[[UINavigationController alloc] initWithRootViewController:masterViewController] autorelease];
    
    
    
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    
    // Actualizo la base de datos si es necesario.
    [[DB getInstance] updateDB];

    
    return YES;

}

- (void)applicationWillResignActive:(UIApplication *)application
{
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
 
   // [[Config shareInstance] update:APP_ACTIVADA value:@"0"];
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
   // [[Config shareInstance] update:APP_ACTIVADA value:@"0"];
    
    [[DB getInstance] close];
    
}

#pragma mark -
#pragma mark Menu Support Methods

-(void)dismissPopOver:(BOOL)animated
{
    if (self.popoverController != nil)
    {
        [self.popoverController dismissPopoverAnimated:animated];
    }
}


@end
