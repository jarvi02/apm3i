//
//  CustomParteDiarioCell.h
//  APM4
//
//  Created by Juan Pablo Garcia on 20/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomParteDiarioCell : UITableViewCell

@property(nonatomic, retain) IBOutlet UILabel *descripcionLbl;
@property(nonatomic, retain) IBOutlet UILabel *manianaLbl;
@property(nonatomic, retain) IBOutlet UILabel *tardeLbl;
@property(nonatomic, retain) IBOutlet UILabel *totalLbl;
@property(nonatomic, retain) IBOutlet UIImageView *checkedImage;

-(void)setChecked:(BOOL)checked;

@end
