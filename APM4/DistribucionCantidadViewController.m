//
//  DistribucionCantidadViewController.m
//  APM4
//
//  Created by Laura Busnahe on 9/9/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "DistribucionCantidadViewController.h"
#import "MTstdDistribucionCantidad.h"

@interface DistribucionCantidadViewController ()

@end

@implementation DistribucionCantidadViewController
{
    NSString *descTitulo;
    NSString *descEtiqueta;
    
    NSInteger periodoSeleccionado;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.cellNib    = [UINib nibWithNibName:@"DistribucionCantidadCell" bundle:nil];
        
        self.headerNib  = [UINib nibWithNibName:@"DistribucionCantidadHeader" bundle:nil];
        [self.headerNib instantiateWithOwner:self options:nil];
     
        self.tableHeader.delegate = self;
        
        self.data = nil;
    }
    return self;
}


- (id)initViewDefaultNib
{
    self = [self initWithNibName:@"DistribucionCantidadViewController" bundle:nil];

    descTitulo   = @"Distribución de médicos por cantidad de visitas";
    
    self.title = [NSString string:descTitulo ifNull:@""];
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = descTitulo;
    
    self.loadingView = [[loadingViewController alloc] initWithDefaultNib];
    [self.loadingView show:self];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Do all the load shit here
    
    [self updateData];
    // ------
    
    [self.tableHeader orderBy:fDistribucionCantidadFieldDescripcion sender:self.tableHeader.btnDescripcion reset:YES];
    [self.loadingView hide];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Overrides

-(BOOL)hasFooter
{
    return YES;
}

-(NSString *)getTitleForHeader{
    return descTitulo;
}

-(UIImage *)getIconoForHeader{
    return [UIImage imageNamed:@"cabecera_principal_32"];
}


#pragma mark -
#pragma mark tableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.data count] > 0)
        return [self.data count] -1;
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"coberturaCelldentifier";
    
    // Obtain the cell object.
    DistribucionCantidadCell *cell = (DistribucionCantidadCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    // If the cell is void, create the cell object.
    if (cell == nil)
    {
        [self.cellNib instantiateWithOwner:self options:nil];
    	cell = self.tmpCell;
    	self.tmpCell = nil;
    }
    
    // Obtengo los datos para la celada.
    MTstdDistribucionCantidad *o = [self.data objectAtIndex:indexPath.row];
    
    // Celdas de Orden
    cell.lblDescripcion.text = [NSString string:o.descripcion ifNull:@""];
    cell.lblManiana.text     = [NSString intToStr:o.maniana ifZero:@"-"];
    cell.lblPorcManiana.text = [NSString stringWithFormat:@"%@%%", [NSString floatToStr:o.porcManiana]];
    cell.lblTarde.text       = [NSString intToStr:o.tarde ifZero:@"-"];
    cell.lblPorcTarde.text   = [NSString stringWithFormat:@"%@%%", [NSString floatToStr:o.porcTarde]];
    cell.lblTotal.text       = [NSString intToStr:o.total ifZero:@"-"];
    cell.lblPorcTotal.text   = [NSString stringWithFormat:@"%@%%", [NSString floatToStr:o.porcTotal]];
    
    return cell;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
    {
        return self.tableHeader;
    }
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.tableHeader)
        return self.tableHeader.frame.size.height;
    else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.tableFooter)
    {
        return self.tableFooter.frame.size.height;
    }
    
    return 0;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (self.tableFooter)
    {
        MTstdDistribucionCantidad *o = nil;
        
        if ([self.data count] > 0)
            o = [self.data objectAtIndex:[self.data count]-1];
        
        // Mañana
        self.tableFooter.lblManiana.text = [NSString intToStr:o.maniana ifZero:@"-"];
        self.tableFooter.lblPorcManiana.text = [NSString stringWithFormat:@"%@%%", [NSString floatToStr:o.porcManiana]];
        
        // Tarde
        self.tableFooter.lblTarde.text = [NSString intToStr:o.tarde ifZero:@"-"];
        self.tableFooter.lblPorcTarde.text = [NSString stringWithFormat:@"%@%%", [NSString floatToStr:o.porcTarde]];
        
        // Total
        self.tableFooter.lblTotal.text = [NSString intToStr:o.total ifZero:@"-"];
        self.tableFooter.lblPorcTotal.text = [NSString stringWithFormat:@"%@%%", [NSString floatToStr:o.porcTotal]];
        
        return self.tableFooter;
    }
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

#pragma mark -
#pragma mark Private methods

- (void)updateData
{
    self.data = [NSMutableArray arrayWithArray:[MTstdDistribucionCantidad getAll]];
    
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark Header methods
- (void)DistribucionCantidadHeader:(DistribucionCantidadHeader*)header
                     selectedField:(tDistribucionCantidadHeaderField)field
                             Order:(NSComparisonResult)order
{
    if ([self.data count] < 1)
        return;
    
    
    // Guardo el total y lo elemino del listado.
    MTstdDistribucionCantidad *o = nil;
    NSInteger index = [self.data count]-1;
    
    o = [self.data objectAtIndex:index];
    [self.data removeObject:o];
    
//    self.descripcion = @"";
//    self.maniana     = 0;
//    self.tarde       = 0;
//    self.total       = 0;
//    self.porcManiana = 0.0f;
//    self.porcTarde   = 0.0f;
//    self.porcTotal   = 0.0f;
    
    // Busco el string correspondiente a la propiedad.
    NSString *keyName = @"descripcion";
    
    if (field == fDistribucionCantidadFieldManiana)
        keyName = @"maniana";
    
    if (field == fDistribucionCantidadFieldPorcManiana)
        keyName = @"porcManiana";
    
    if (field == fDistribucionCantidadFieldTarde)
        keyName = @"tarde";
    
    if (field == fDistribucionCantidadFieldPorcTarde)
        keyName = @"porcTarde";
    
    if (field == fDistribucionCantidadFieldTotal)
        keyName = @"total";

    if (field == fDistribucionCantidadFieldPorcTotal)
        keyName = @"porcTotal";

    
    BOOL asc = YES;
    if (order == NSOrderedDescending)
        asc = NO;
    
    // Reordeno el listado.
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc]
                                initWithKey:keyName
                                ascending:asc];
    NSArray *sortDescriptors = [NSArray arrayWithObject: sorter];
    [self.data sortUsingDescriptors:sortDescriptors];
    
    // Vuelvo a agregar el total.
    [self.data addObject:o];
    
    [self.tableView reloadData];
}

@end
