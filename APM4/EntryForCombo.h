//
//  EntryForCombo.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EntryForCombo : NSObject

@property(nonatomic, assign) NSInteger index;
@property(nonatomic, retain) NSString *description;

-(id)initWithValue:(NSString*) aDescription forIndex:(NSInteger) aIndex;

@end
