//
//  MedicosVisitadosPorFecha.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 18/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MedicosVisitadosPorFecha : NSObject

@property(nonatomic, assign) NSInteger idMedicosVisitadosPorFecha;
@property(nonatomic, assign) NSInteger nodo;
@property(nonatomic, assign) NSInteger medico;
@property(nonatomic, assign) NSInteger fechaDisplay;
@property(nonatomic, retain) NSString *apellidoNombre;
@property(nonatomic, retain) NSString *tipoVisita;
@property(nonatomic, retain) NSString *actividad;

+(NSArray*) GetAllWithFilter:(NSInteger)filtro;

@end
