//
//  MTTiposDomicilios.h
//  APM4
//
//  Created by Laura Busnahe on 8/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTTiposDomicilios : NSObject

@property(nonatomic) NSUInteger      recID;
@property(nonatomic,retain) NSString *descripcion;

- (NSString*)getDescription;

+ (NSArray*)getAll;

@end
