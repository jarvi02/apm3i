//
//  LoginMenuViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "LoginMenuViewController.h"
#import "AboutUsViewController.h"

@interface LoginMenuViewController ()

-(void)pressedGoToLogin;
-(void)pressedGoToAbout;

@end

@implementation LoginMenuViewController

-(NSArray *)options
{
    return [NSArray arrayWithObjects:
            @"Login",
            @"Acerca Nuestro",
            nil];
}

-(NSString *)title
{
    return @"Login";
}

-(CGSize)size
{
    return CGSizeMake(200.0f, 88.0f);
}

-(void)selectedOptionsAtIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
            [self pressedGoToLogin];
            break;
        case 1:
            [self pressedGoToAbout];
            break;
        default:
            [self showNotImplemented];
    }
}

-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index {

    switch (index) {
        case 0:
            return YES;
            break;
        case 1:
            return YES;
            break;
        default:
            return NO;
    }
    
}

#pragma mark -
#pragma mark Public Methods

-(void)pressedGoToLogin
{
    if (self.delegate != nil)
    {
        [self.delegate performTask:@"SHOW_LOGIN"];
    }
    
}

-(void)pressedGoToAbout
{
    
    AboutUsViewController *o = [[[AboutUsViewController alloc] initWithNibName:@"AboutUsViewController" bundle:nil] autorelease];
    
    [self navigateToController:o];
    
}

@end

