//
//  Visita.h
//  APM4
//
//  Created by Juan Pablo Garcia on 11/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Visita : NSObject

+(BOOL)validarFechaDeVisista:(NSUInteger)idMedico fechaVisita:(NSDate *)fechaVisita fechaOriginal:(NSDate *)fechaOriginal;

+(BOOL)validarVisitaExiste:(NSUInteger)idMedico fechaVisita:(NSDate *)fechaVisita fechaOriginal:(NSDate *)fechaOriginal;

@property(nonatomic, retain) NSDate *fecha;
@property(nonatomic, retain) NSString *descripcionTipo;
@property(nonatomic, retain) NSString *domicilio;
@property(nonatomic, retain) NSString *objetivos;
@property(nonatomic, retain) NSString *evaluacion;

@property(nonatomic)NSUInteger idVisita;
@property(nonatomic)NSUInteger idMedico;
@property(nonatomic)NSUInteger idTipo;
@property(nonatomic)NSUInteger idDomicilio;

@property(nonatomic)NSUInteger idEstado;
@property(nonatomic)NSUInteger contacto;

+(NSUInteger)lastId:(NSUInteger)idMedico;
+(Visita *)getByID:(NSUInteger)idVisita idMedico:(NSUInteger)idMedico;
+(void)save:(Visita *)visita;
+(void)update:(Visita *)visita;
+(NSString *)getForNovedad:(NSUInteger)visita medico:(NSUInteger)medico;
+(void)addVisitaFamilia:(int) familiaId Visita:(int)visitaId nodoId:(int)nodoId medicoId:(int)medicoId andOrden:(int)orden;

@end
