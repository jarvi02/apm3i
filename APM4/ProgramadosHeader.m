//
//  ProgramadosHeader.m
//  APM4
//
//  Created by Laura Busnahe on 9/24/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ProgramadosHeader.h"

@implementation ProgramadosHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
