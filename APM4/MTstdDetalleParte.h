//
//  MTstdDetalleParte.h
//  APM4
//
//  Created by Laura Busnahe on 7/19/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTstdDetalleParte : NSObject

@property (nonatomic, strong)NSString   *fecha;
@property (nonatomic, strong)NSString   *actividad;
@property (nonatomic, strong)NSString   *nombre;
@property (nonatomic, strong)NSString   *descripcion;

+ (NSArray*)getFechasParte;
+ (NSArray*)getForFechaOrNil:(NSDate*)fecha;

@end
