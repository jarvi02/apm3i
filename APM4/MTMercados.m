//
//  MTMercados.m
//  APM4
//
//  Created by Ezequiel Manacorda on 2/28/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "MTMercados.h"
#import "DB.h"

#define DEBUG_MERCADOS



@implementation MTMercados

- (NSString*)getDescription
{
    NSString* newDescription = [NSString stringWithFormat:@"%@",self.descripcion];
    
    return newDescription;
}



+ (NSArray*)getByAuditoria:(NSInteger) idAuditoria
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    
    NSString *sql = @"select auditoria, mercado, descripcion, total, totallaboratorio "
                     "from dpm_mercados "
                     "where auditoria = %i order by descripcion";
    NSString *query = [NSString stringWithFormat:sql, idAuditoria];
    
    
#ifdef DEBUG_MERCADOS
    NSLog(@"--- Mercado query ---");
    NSLog(@"%@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    MTMercados *o;
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
			
            o = [[MTMercados alloc] init];
            
          
            // id Auditoría
            int _auditoria = sqlite3_column_int(statement, 0);
            o.auditoria = _auditoria;
            
            // id Mercado
            int _mercado = sqlite3_column_int(statement, 1);
            o.mercado = _mercado;
            
            // Descripción del mercado
            char *_descripcion = (char *)sqlite3_column_text(statement, 2);
            o.descripcion = @"";
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o.descripcion = [NSString stringWithUTF8String:_descripcion];
            
            // Total
            int _total = sqlite3_column_int(statement, 3);
            o.total = _total;
            
            // Total del laboratorio
            int _totalLaboratio = sqlite3_column_int(statement, 4);
            o.totalLaboratorio = _totalLaboratio;
            
            o.selected = false;
            
            [array addObject:o];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla dpm_mercados");
	}
	sqlite3_finalize(statement);
    
    
    return array;
}

+ (NSArray*)getForMedico:(NSInteger)idMedico Auditoria:(NSInteger)idAuditoria
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    
    NSString *sql = @"SELECT DISTINCT dpm_mercados.auditoria, "
    "dpm_mercados.mercado, "
    "dpm_mercados.descripcion, "
    "dpm_mercados.total, "
    "dpm_mercados.totallaboratorio "
    "FROM dpm_mercados "
    "INNER JOIN dpm_prescripciones ON (dpm_mercados.mercado = dpm_prescripciones.mercado AND dpm_mercados.auditoria = dpm_prescripciones.auditoria) "
    "WHERE dpm_prescripciones.medico = %i AND "
    "dpm_prescripciones.auditoria = %i "
    "ORDER BY dpm_mercados.descripcion";
    NSString *query = [NSString stringWithFormat:sql, idMedico, idAuditoria];
    
    
#ifdef DEBUG_MERCADOS
    NSLog(@"--- Mercados query ---");
    NSLog(@"%@", query);
#endif
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    MTMercados *o;
    
	if ( statement ) {
        
		while (sqlite3_step(statement) == SQLITE_ROW)
        {
			
            o = [[MTMercados alloc] init];
            
            
            // id Auditoría
            int _auditoria = sqlite3_column_int(statement, 0);
            o.auditoria = _auditoria;
            
            // id Mercado
            int _mercado = sqlite3_column_int(statement, 1);
            o.mercado = _mercado;
            
            // Descripción del mercado
            char *_descripcion = (char *)sqlite3_column_text(statement, 2);
            o.descripcion = @"";
            if ((_descripcion != nil) && (strlen(_descripcion) > 0))
                o.descripcion = [NSString stringWithUTF8String:_descripcion];
            
            // Total
            int _total = sqlite3_column_int(statement, 3);
            o.total = _total;
            
            // Total del laboratorio
            int _totalLaboratio = sqlite3_column_int(statement, 4);
            o.totalLaboratorio = _totalLaboratio;
            
            o.selected = false;
            
            [array addObject:o];
        }
        
	} else {
        NSAssert(0, @"No se encontre la tabla dpm_mercados");
	}
	sqlite3_finalize(statement);
    
    
    return array;
}


+ (NSString*)getSelectedIdsString:(NSArray*)arrayMercados
{
    NSString *result = nil;
    
    // Chequeo que el array tenga datos y no esté vacio.
    if ((arrayMercados == nil) || ([arrayMercados count] == 0))
        return result;
    
    // Chequeo que el tipo de objetos del array sean MTMercados
    if (![[arrayMercados objectAtIndex:0] isKindOfClass:[MTMercados class]])
        return result;
    
    NSInteger oCount = 0;
    for (MTMercados *o in arrayMercados)
    {
        if (o.selected)
        {
            NSString *sConcat;
            if (oCount == 0)
                result = @"";
            sConcat = [NSString stringWithFormat:@"%@%i", oCount==0?@" ":@", ", o.mercado];
            result = [NSString stringWithFormat:@"%@%@",result, sConcat];
            oCount++;
        }
    }
    
    return result;
}


@end
