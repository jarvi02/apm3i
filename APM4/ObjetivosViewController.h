//
//  ObjetivosViewController.h
//  APM4
//
//  Created by Laura Busnahe on 8/8/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomViewController.h"
#import "CarteraViewController.h"
#import "customObjetivosCell.h"
#import "customObjetivosHeader.h"

@interface ObjetivosViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray                        *data;
@property (nonatomic, strong) UINib                          *cellNib;
@property (nonatomic, strong) UINib                          *headerNib;
@property (nonatomic, strong) IBOutlet customObjetivosCell   *tableCell;
@property (strong, nonatomic) IBOutlet customObjetivosHeader *tableHeader;

@property (retain, nonatomic) IBOutlet UILabel *lblObjetivo;
@property (retain, nonatomic) IBOutlet UITableView *tableView;







@property(nonatomic, assign) CarteraViewController *carteraViewControllerReference;


- (id)initWithDefaultNib;

@end
