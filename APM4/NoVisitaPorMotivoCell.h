//
//  NoVisitaPorMotivoCell.h
//  APM4
//
//  Created by Laura Busnahe on 9/13/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoVisitaPorMotivoCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblMotivo;
@property (nonatomic, strong) IBOutlet UILabel *lblManiana;
@property (nonatomic, strong) IBOutlet UILabel *lblTarde;
@property (nonatomic, strong) IBOutlet UILabel *lblTotal;

@end
