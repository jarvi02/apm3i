//
//  Promocion.h
//  APM4
//
//  Created by Juan Pablo Garcia on 12/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Promocion : NSObject

@property(nonatomic) NSUInteger nodo;
@property(nonatomic) NSUInteger medico;
@property(nonatomic) NSUInteger visitador;
@property(nonatomic) NSUInteger visita;
@property(nonatomic) NSUInteger orden;
@property(nonatomic) NSUInteger producto;
@property(nonatomic) NSUInteger muestras;
@property(nonatomic) NSUInteger obsequio;
@property(nonatomic) NSUInteger literatura;
@property(nonatomic) NSUInteger lote;
@property(nonatomic) NSUInteger stock;

@property(nonatomic, retain) NSString *evaluacion;
@property(nonatomic, retain) NSString *objetivos;

@property(nonatomic, retain) NSString *descripcionLote;
@property(nonatomic, retain) NSString *descripcionProducto;
@property(nonatomic, retain) NSString *codigoLote;
@property(nonatomic, retain) NSDate *fechaVto;
@property(nonatomic) NSUInteger ordenIngreso;

@property(nonatomic) NSInteger  promocionado;

@property(nonatomic) BOOL aPromocionar;


+ (NSArray *)GetAllByMedico:(NSUInteger) medico visita:(NSUInteger)visita;
+ (void)deleteForMedico:(NSUInteger)medico visita:(NSUInteger)visita;
+ (BOOL)existeRegistroAPromocionar:(NSUInteger)medico producto:(NSUInteger)producto lote:(NSUInteger)lote;
+ (void)updateProductosAPromocionar:(NSUInteger)medico producto:(NSUInteger)producto cantidad:(NSUInteger)cantidad lit:(NSUInteger)lit obsequio:(NSUInteger)obsequio objetivos:(NSString *)objetivos;
+ (NSString *)getForNovedad:(NSUInteger) medico visita:(NSUInteger)visita orden:(NSUInteger)orden producto:(NSUInteger)producto;


- (void)save;
- (BOOL)isInArray:(NSArray*)listado;

- (void)updateProductosAPromocionar;

- (NSString*)getForNovedad;
- (NSString*)getForNovedadAPromocionar;

@end
