//
//  TasksMenuViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 07/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "TasksMenuViewController.h"
#import "VisitaViewController.h"
#import "NoVisitaViewController.h"
#import "ABMMedicoViewController.h"
#import "AplicarBajaMedicoViewController.h"
#import "Config.h"
#import "ConfigGlobal.h"
#import "CarteraMedico.h"
#import "MTMedico.h"
#import "selectGrupoController.h"
#import "AltaFluctuanteViewController.h"

#import "GlobalDefines.h"

@interface TasksMenuViewController ()

-(void)alert:(NSString*)msg;

-(void)navigateToVisita;
-(void)navigateToNoVisita;
-(void)navigateToAlta;
-(void)navigateToModificar;
-(void)navigateToAplicarBaja;

@end

// TODO: Cartera - Si el médico es fuctunte, no mostrar el horario y en la descripción de la dirección mostrar Lugar, Localidad y Provincia.
// TODO: Deseleccionar médico al cambiar de grupo.

@implementation TasksMenuViewController

-(NSArray *)options
{
    return [NSArray arrayWithObjects:
            @"Visita",
            @"No visita",
            @"Alta fluctuante",
            @"Convertir fluctuante",
            @"Alta",
            @"Modificar",
            @"Aplicar baja",
            @"Revertir baja",
            @"Agregar a grupo",
            @"Quitar de grupo",
            nil];
}

-(NSString *)title
{
    return @"Tareas";
}

-(CGSize)size
{
    return CGSizeMake(300.0f, 440.0f);
}

- (BOOL)estaSeleccionadoNoFluctuante:(CarteraMedico*)c
{
    if ((c) && (![c esFluctuante]))
        return YES;
    else
        return NO;
}

- (BOOL)estaSeleccionadoFluctuante:(CarteraMedico*)c
{
    if ((c) && ([c esFluctuante]))
        return YES;
    else
        return NO;
}

-(void)selectedOptionsAtIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
        case 1:
            // Visita
            
            if (self.carteraViewControllerReference.itemCarteraMedicoSelected != nil) {
                
                NSString *cerrarCiclo = [[Config shareInstance] find:CERRAR_CICLO];
                
                if ([cerrarCiclo isEqualToString:@"1"])
                {
                    [self alert:@"Cierre de ciclo pendiente de transmisión, no puede operar con los médicos"];
                    break;
                }
                else
                {
                    //BOOL visRevisita = [[ConfigGlobal shareInstance] find:VIS_REVISITA];
                    
                    CarteraMedico *c = self.carteraViewControllerReference.itemCarteraMedicoSelected;
                    
                    BOOL usaAutorizacion = [[ConfigGlobal shareInstance] find:USAAUTORIZACION];
                
                    // Pendiente de Alta
                    if (usaAutorizacion && c.penalta == 1)
                    {
                        [self alert:@"No se puede hacer ningún tipo de operación sobre un médico pendiente de alta."];
                        break;
                    }
                
                    // Pendiete de baja
                    if (usaAutorizacion && c.penbaja == 1)
                    {
                        [self alert:@"No se puede hacer ningún tipo de operación sobre un médico pendiente de baja."];
                        break;
                    }
                
                    // Baja
                    if (c.b == 1)
                    {
                        [self alert:@"No se puede hacer ningún tipo de operación sobre un médico dado de baja."];
                        break;
                    }
                
                    // Médico fluctuante.
                    if ([c esFluctuante])
                    {
                        [self alert:@"No es posible revisitar un médico fluctuante"];
                        break;
                    }
                    
                    if ((c.visitasProgramadas - c.visitasRealizadas < 1) && (index == 0))
                    {
                        [self alert:@"Máximo de visitas alcanzado para este médico."];
                        break;
                    }

                    // Chequeo si es Visita o No Visita
                    if (index == 0)
                        [self navigateToVisita];
                    else
                        [self navigateToNoVisita];
                
                }
            } else {
                [self alert:@"Debe seleccionar un medico antes de ir a visita"];
                break;
            }            
            break;
//        case 1:
//            // No Visita
//            if (self.carteraViewControllerReference.itemCarteraMedicoSelected != nil) {
//                [self navigateToNoVisita];
//            } else {
//                [self alert:@"Debe seleccionar un medico antes de ir a no visita"];
//            }
//
//            break;
        case 2:
            // Alta fluctuante
            [self navigateToAltaFluctuante];
            break;
        case 4:
            // alta
            [self navigateToAlta];
            break;
            
        case 3:
            // Convertir Fluctuante
        case 5:
            // Modificar
            if (self.carteraViewControllerReference.itemCarteraMedicoSelected != nil)
            {
                // Chequeo queel médico no esté pendiente de baja o en baja.
                CarteraMedico *itemCartera = self.carteraViewControllerReference.itemCarteraMedicoSelected;
                
//                // Médico fluctuante.
//                if ([itemCartera esFluctuante])
//                {
//                    [self alert:@"No es posible modificar un médico fluctuante"];
//                    break;
//                }
                
                if (itemCartera.penbaja > 0)
                {
                    [self alert:@"No se puede modificar un médico pendiente de baja"];
                } else
                if (itemCartera.b > 0)
                {
                    [self alert:@"No se puede modificar un médico que fue dado de baja"];
                } else
                    [self navigateToModificar];
            } else {
                [self alert:@"Debe seleccionar un medico antes de ir a modificar"];
            }
            break;
        case 6:
            // Aplicar Baja
            if (self.carteraViewControllerReference.itemCarteraMedicoSelected != nil)
            {
                CarteraMedico *c = self.carteraViewControllerReference.itemCarteraMedicoSelected;
                
                // Médico fluctuante.
                if ([c esFluctuante])
                {
                    [self alert:@"No es posible dar de baja un médico fluctuante"];
                    break;
                }
                
                // a un medico ya dado de BAJA o PENDIENTE DE BAJA, si campo B o pendbaja = 1 no dejar entrar a la baja
                
                if ((c.b == 1) || (c.penbaja == 1))
                {
                    [self alert:@"Este medico se a dado de baja o esta pendiente de baja"];
                }
                else
                {
                    [self navigateToAplicarBaja];
                }
                
            } else {
                [self alert:@"Debe seleccionar un medico antes de aplicar la baja"];
            }
            break;
            
        case 8:
            // Agregar a Grupo
            if ((self.carteraViewControllerReference.itemCarteraMedicoSelected != nil) ||
               ((self.carteraViewControllerReference.isFiltered) && ([self.carteraViewControllerReference.filteredData count] > 0)))
            {
                // Chequeo queel médico no esté pendiente de baja o en baja.
                CarteraMedico *itemCartera = self.carteraViewControllerReference.itemCarteraMedicoSelected;
                if (itemCartera.penbaja > 0)
                {
                    [self alert:@"No se puede modificar un médico pendiente de baja"];
                } else
                if (itemCartera.b > 0)
                {
                    [self alert:@"No se puede modificar un médico que fue dado de baja"];
                } else
                    [self navigateToAgregarAGrupo];
            } else
                [self alert:@"Debe seleccionar un medico o filtrar la cartera para agregar a un grupo"];
            break;
            
        case 9:
            // Quitar de Grupo
            if ((self.carteraViewControllerReference.itemCarteraMedicoSelected != nil) ||
                ((self.carteraViewControllerReference.isFiltered) && ([self.carteraViewControllerReference.filteredData count] > 0)))
            {
                // Chequeo que el médico no esté pendiente de baja o en baja.
                CarteraMedico *itemCartera = self.carteraViewControllerReference.itemCarteraMedicoSelected;
                if (itemCartera.penbaja > 0)
                {
                    [self alert:@"No se puede modificar un médico pendiente de baja"];
                } else
                    if (itemCartera.b > 0)
                    {
                        [self alert:@"No se puede modificar un médico que fue dado de baja"];
                    } else
                        [self navigateToQuitarGrupo];
            } else
                [self alert:@"Debe seleccionar un medico o filtrar la cartera para agregar a un grupo"];
            break;
        
        case 7:
            // Revertir Baja
            
        default:
            [self showNotImplemented];
    }
}

-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index
{
    // Medico seleccionado
    CarteraMedico *c = nil;
    if (self.carteraViewControllerReference.itemCarteraMedicoSelected != nil)
    {
        c = self.carteraViewControllerReference.itemCarteraMedicoSelected;
    }
            
            
    switch (index)
    {
        case 0:
            // Visita
            return (([self estaSeleccionadoNoFluctuante:c]) &&
                    (![c necesitaAutorizacion]));
            break;
            
        case 1:
            // No Visita
            return (([self estaSeleccionadoNoFluctuante:c]) &&
                    (![c necesitaAutorizacion]));
            break;
            
        case 2:
            // Alta fluctuante
            return YES;
            break;
            
        case 3:
            // Convertir Fluctuante
            return ([self estaSeleccionadoFluctuante:c]);
            break;
            
        case 4:
            // alta
            return YES;
            break;
            
        case 5:
            // Modificar
            return (([self estaSeleccionadoNoFluctuante:c]) &&
                    (![c esBaja]));
            break;
            
        case 6:
            // Aplicar Baja
            return (([self estaSeleccionadoNoFluctuante:c] ) &&
                    (![c esBaja]) &&
                    (![c necesitaAutorizacion]));
            break;
            
        case 7:
            // Revertir Baja
            return NO;
            break;
            
        case 8:
            // Agregar a Grupo
            if (((c) && (![c necesitaAutorizacion]))  ||
                ((self.carteraViewControllerReference.isFiltered) && ([self.carteraViewControllerReference.filteredData count] > 0)))
                return YES;
            else
                return NO;
            
            break;
            
        case 9:
            // Quitar de Grupo
            // Chequeo si la cartera tiene seleccionado algún grupo y que no esté bloqueado.
            if ((self.carteraViewControllerReference.selectedGrupo) &&
                (!self.carteraViewControllerReference.selectedGrupo.blocked) &&
                ((c) && (![c esBaja]) && (![c necesitaAutorizacion])))
                return  YES;
            else
                return NO;
            break;
            
        default:
            return NO;
    }
}

#pragma mark -
#pragma Private Members

-(void)alert:(NSString*)msg{
    
    [Delegate dismissPopOver:YES];
    
    UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Alerta" message:msg delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
    [a show];
    [a release];
}

-(void)navigateToVisita
{
    VisitaViewController *o = [[[VisitaViewController alloc] initWithNibName:@"VisitaViewController" bundle:nil] autorelease];
    o.modo = VisitaModoAlta; 
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    
    
    [self navigateToController:o];
}

-(void)navigateToNoVisita {
    
    NoVisitaViewController *o = [[[NoVisitaViewController alloc] initWithDefaultNib] autorelease];
    o.modo = NoVisitaModoAlta;
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    
    [self navigateToController:o];

}

-(void)navigateToAlta{
    ABMMedicoViewController *o = [[[ABMMedicoViewController alloc] initWithNibName:@"ABMMedicoViewController"
                                                                            bundle:nil
                                                                          idMedico:0
                                                                           andMode:kMTEAAdd] autorelease];
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    [self navigateToController:o];
    
}

-(void)navigateToModificar{
    NSInteger idMedico = self.carteraViewControllerReference.itemCarteraMedicoSelected.idCarteraMedico;
    ABMMedicoViewController *o = [[[ABMMedicoViewController alloc] initWithNibName:@"ABMMedicoViewController"
                                                                            bundle:nil
                                                                          idMedico:idMedico
                                                                           andMode:kMTEANone] autorelease];
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    [self navigateToController:o];
    
}

-(void)navigateToAplicarBaja{
    AplicarBajaMedicoViewController *o = [[[AplicarBajaMedicoViewController alloc] initWithNibName:@"AplicarBajaMedicoViewController" bundle:nil cartera:self.carteraViewControllerReference] autorelease];
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    [self navigateToController:o];
}

-(void)navigateToAgregarAGrupo
{
    selectGrupoController *gvc = [[[selectGrupoController alloc] initWithDefaultNib] autorelease];
    gvc.carteraReference = self.carteraViewControllerReference;
    gvc.agregarAGrupo = YES;
    
    
        
    [Delegate dismissPopOver:YES];
    [self.carteraViewControllerReference presentViewController:gvc animated:YES completion:nil];
}

-(void)navigateToQuitarGrupo
{
    [Delegate dismissPopOver:YES];
    [self.carteraViewControllerReference performTask:@"quitardegrupo"];
}

-(void)navigateToAltaFluctuante
{
    AltaFluctuanteViewController *o = [[[AltaFluctuanteViewController alloc] initWithDefaultNib:0 andMode:kMTEAAdd] autorelease];
    o.carteraViewControllerReference = self.carteraViewControllerReference;
    [self navigateToController:o];
}

@end
