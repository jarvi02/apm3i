//
//  Domicilio.m
//  APM4
//
//  Created by Juan Pablo Garcia on 11/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "Domicilio.h"
#import "Config.h"
#import "DB.h"
#import "NSStringExtended.h"

#import "GlobalDefines.h"

@implementation Domicilio



- (id)init
{
    self = [super init];
    if (self)
    {
        self.telefonos = [NSMutableArray array];
        self.horarios =[NSMutableArray array];
    }
    return self;
}

-(NSString*)getDescripcion
{
    
    return [NSString stringWithFormat:@"%@ - %@%@%@%@, %@, %@, %@ - %@",
           [NSString string:self.utilidad.descripcion ifNull:@""],
           [NSString string:self.calle.descripcion ifNull:@""],
            self.numero>0? [NSString stringWithFormat:@" %@",[NSString intToStr:self.numero]] : @"",
            self.piso>0? [NSString stringWithFormat:@" %@",[NSString intToStr:self.piso]] : @"",
           [[NSString string:self.depto ifNull:@""] length] > 0 ? [NSString stringWithFormat:@" %@", self.depto] : @"",
           [NSString string:[self getCpDescripcion] ifNull:@""],
           [NSString string:self.localidad.descripcion ifNull:@""],
           [NSString string:self.provincia.descripcion ifNull:@""],
           [NSString string:self.tipo.descripcion ifNull:@""]];
    
}

-(NSString*)getCpDescripcion
{
    NSString *result = @"";
    
    if ([self.codigoPostal length] < 4)
        return result;
    
    
    
    if ([SettingForKey(PAIS, @"54") isEqualToString:@"54"])
        result = [self.codigoPostal substringToIndex:4];
    else
        result = [NSString stringWithFormat:@"%@", self.codigoPostal];
    
    return result;
}

- (void)dealloc
{
 
    [_tipo release];
    [_institucion release];;
    [_cargo release];
    [_provincia release];
    [_localidad release];
    [_calle release];
    [_depto release];
    [_codigoPostal release];
    [_utilidad release];
    [_horarios release];
    [_telefonos release];
    
    [super dealloc];
}


+(Domicilio *)getByMedico:(NSUInteger)idMedico
{
    NSString *sql = @"SELECT id "
                     "FROM med_domicilios "
                     "WHERE utilidad = 1 AND "
                           "nodo = %@ AND "
                           "medico = %d";
    
    NSString *nodo = SettingForKey(NODO, @"");
    
    NSString *query = [NSString stringWithFormat:sql, nodo, idMedico];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
    
    Domicilio *d = [[Domicilio alloc] init];
    
	if ( statement ) {
        
		if (sqlite3_step(statement) == SQLITE_ROW) {
            
            d.idDomicilio = sqlite3_column_int(statement, 0);
            
        }
        else
            return nil;
    }
    
    sqlite3_finalize(statement);
    
    return [d autorelease];
}


-(id)copy{

    Domicilio *dom = [[Domicilio alloc] init];
    
    dom.idDomicilio = self.idDomicilio;
    dom.tipo = [self.tipo copy];
    dom.institucion = [self.institucion copy];
    dom.cargo = [self.cargo copy];
    dom.provincia = [self.provincia copy];
    dom.localidad = [self.localidad copy];
    dom.calle = [self.calle copy];
    dom.numero = self.numero;
    dom.piso = self.piso;
    dom.depto = [NSString stringWithString:((self.depto!=nil) ? self.depto:@"")];
    dom.codigoPostal = [NSString stringWithString:((self.codigoPostal!=nil) ? self.codigoPostal:@"")];
    dom.utilidad = [self.utilidad copy];
    dom.consultorioExterno = self.consultorioExterno;
    dom.horarios = [NSMutableArray arrayWithArray:self.horarios];
    dom.telefonos = [NSMutableArray arrayWithArray:self.telefonos];
    
    return  [dom autorelease];
}


-(NSString*)isOkForSave
{
    
    
    // realmente con esto no queda muy claro que validaciones hay que hacer sobre el domicilio. 
    
    /*De este pantalla si se selecciona “consultorio” los campos institución, cargo , piso, dpto. y teléfonos no son obligatorios.
     
    Si se selecciona institución los campos piso, dpto. y teléfonos no son obligatorios.
    
    Importante : Si es el primer domicilio que se carga, el campo UTILIDAD es siempre de VISITA y no se puede modificar.
    En los posteriores se puede modificar pero no se puede elegir visita
    
    SI PRIMER DOMICILIO
    CAMPO UTILIDAD = VISITA (no se peude tocar)
    SINO
    CAMPO UTILIDAD seleccionable menos la VISITA
    */
    
    // Tipo
    if ((!self.tipo) || (self.tipo.idTipo < 1) || ([self.tipo.descripcion length] < 1))
        return @"Debe seleccionar un tipo de domicilio";
    
    // Provincia
    if ((!self.provincia) || (self.provincia.idProvincia < 1) || ([self.provincia.descripcion length] < 1))
        return [[MTRegionalDictionary sharedInstance] valueForText:@"Debe seleccionar la provincia del domicilio"];
    
    // Localidad
    //if ((!self.localidad) || (self.localidad.idLocalidad < 1))
    if ((!self.localidad) || ([self.localidad.descripcion length] < 1))
        return [[MTRegionalDictionary sharedInstance] valueForText:@"Debe seleccionar la localidad del domicilio"];
    
    // Calle
    if ((!self.calle) || ([self.calle.descripcion length] < 1))
        return @"Debe ingresar una calle para el domicilio";
    
    // Numero
    if ((!self.numero) || (self.numero < 1))
        return @"Debe ingresar el número de la calle";
    
    // Código Postal
    //          Cuando la calle es seleccionada desde un el combo de calles, el código postal es seleccionado desde la base de datos
    //          según el número (altura) de la calle dada. Puede ocurrir que el número ingresado no contenga un código postal por estar
    //          el mismo fuera de rango de altura. Para esto debe chequearse que esté definido el CP.
    if ((!self.codigoPostal) || ([self.codigoPostal length] == 0))
    {
        // TODO: Buscar qué código postal es válido para la calle y localidad del domicilio.
        return @"El código postal no es válido.\n"
                "Controle que la calle y número seleccionados sean los correctos o vuelva a ingresarlos.";
    }
    
    // Utilidad
    if ((!self.utilidad) || (self.utilidad.idUtilidad < 1) || ([self.utilidad.descripcion length] < 1))
        return @"Debe ingresar la utilidad del domicilio";
    
    // Horario
    if ((!self.horarios) || ([self.horarios count] < 1))
        return @"Debe ingresar al menos un horario para el domicilio";
    
    return @"OK";
    
}


- (void)fillDomicilioByCP
{
    
#ifdef DEBUG_ABMMEDICO
    
#endif
    
    // Busco el la Localidad y Calle según el Código Postal.
    if ([self getCalleLocalidadByCP])
        return;
    
    // Si no encontré la dirección por CP, busco si la calle y altura possen CP.
    if ([self getCPByCalleLocalidad])
        return;
    
    // Busco el código postal para la localidad y provincia si no existia para la calle.
    if ([self getCPByProvinciaLocalidad])
        return;
    
}


- (BOOL)getCalleLocalidadByCP
{
    
    NSString *sql = @"";
    NSString *query = @"";
    Calle *c = [[Calle alloc] init];
    Localidad *l = [[Localidad alloc] init];
    
    // Busco primero la calle según el código postal seleccionado.
    sql = @"SELECT g_codpost.provincia as idprovincia, "
                "g_codpost.localidad as idlocalidad, "
                "g_local.descripcion as localidad, "
                "g_local.calles as bcalles, "
                "g_codpost.calle as idcalle, "
                "g_calles.descripcion as calle "
    "FROM geo_codigospostales g_codpost "
    "INNER JOIN  geo_calles g_calles on ((g_calles.id = idcalle ) AND "
                "(g_calles.localidad = g_codpost.localidad) AND "
                "(g_calles.provincia = g_codpost.provincia) AND "
                "(g_calles.pais = g_codpost.pais)) "
    "INNER JOIN geo_localidades g_local on ((g_local.id = idlocalidad) AND "
                "(g_local.provincia = g_codpost.provincia) AND "
                "(g_local.pais = g_codpost.pais)) "
    "WHERE ((g_codpost.pais = %@) AND (g_codpost.cp = %@));";
    
    l.pais =  [SettingForKey(PAIS, @"54") integerValue];
    
    query = [NSString stringWithFormat:sql,
             [NSString intToStr:l.pais],
             [NSString stringQuoted:self.codigoPostal ifNull:@"NULL"]];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
	if (statement)
    {
		if (sqlite3_step(statement) == SQLITE_ROW) {
            
            // id Provincia
            l.provincia = sqlite3_column_int(statement, 0);
            
            // id Localidad
            l.idLocalidad = sqlite3_column_int(statement, 1);
            
            // Localidad
            l.descripcion = [NSString pCharToString:(char *)sqlite3_column_text(statement, 2)];
            
            // Posee Calles
            l.calles = sqlite3_column_int(statement, 3);
            
            // id Calle
            c.idCalle = sqlite3_column_int(statement, 4);
            
            // Calle
            c.descripcion = [NSString pCharToString:(char *)sqlite3_column_text(statement, 5)];
            
            if (l.calles > 0)
                self.calle  = c;
            
            self.localidad = l;
            
#ifdef DEBUG_ABMMEDICO
            NSLog(@"--- getCalleLocalidadByCP ---");
            NSLog(@"Query: %@", query);
#endif
            sqlite3_finalize(statement);
            return YES;
        }
    }
    sqlite3_finalize(statement);
    
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"--- getCalleLocalidadByCP ---");
    NSLog(@"Query: %@", query);
    NSLog(@" No encontrado");
#endif
    
    return NO;
}


- (BOOL)getCPByCalleLocalidad
{
    
    NSString *sql = @"";
    NSString *query = @"";
    Calle *c = [[Calle alloc] init];
    Localidad *l = [[Localidad alloc] init];
    
    // Busco primero la calle según el código postal seleccionado.
    sql = @"SELECT g_codpost.localidad as idlocalidad, "
                    "g_local.calles as bcalles, "
                    "g_codpost.calle as idcalle, "
                    "g_codpost.cp as codpostal "
    "FROM geo_codigospostales g_codpost "
    "INNER JOIN  geo_calles g_calles on ((g_calles.id = idcalle ) AND "
                    "(g_calles.localidad = g_codpost.localidad) AND "
                    "(g_calles.provincia = g_codpost.provincia) AND "
                    "(g_calles.pais = g_codpost.pais)) "
    "INNER JOIN geo_localidades g_local on ((g_local.id = idlocalidad) AND "
                    "(g_local.provincia = g_codpost.provincia) AND "
                    "(g_local.pais = g_codpost.pais)) "
    "WHERE ((g_local.pais = %@) AND (g_local.descripcion = %@) AND (g_calles.descripcion = %@) AND "
                    "((g_codpost.desde <= %@) AND (g_codpost.hasta >= %@)));";
    
    l.pais =  [SettingForKey(PAIS, @"54") integerValue];
    
    query = [NSString stringWithFormat:sql,
             [NSString intToStr:l.pais],
             [NSString stringQuoted:self.localidad.descripcion ifNull:@"''"],
             [NSString stringQuoted:self.calle.descripcion ifNull:@"''"],
             [NSString intToStr:self.numero],
             [NSString intToStr:self.numero]];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
	if (statement)
    {
		if (sqlite3_step(statement) == SQLITE_ROW) {
            
            // id Provincia
            l.provincia = self.provincia.idProvincia;
            
            // id Localidad
            l.idLocalidad = sqlite3_column_int(statement, 0);
            
            // Localidad
            l.descripcion = self.localidad.descripcion;
            
            // Posee Calles
            l.calles = sqlite3_column_int(statement, 1);
            
            // id Calle
            c.idCalle = sqlite3_column_int(statement, 2);
            
            // Calle
            c.descripcion = self.calle.descripcion;
            
            // Código Postal
            self.codigoPostal = [NSString pCharToString:(char*)sqlite3_column_text(statement, 3)];
            
            if (l.calles > 0)
                self.calle  = c;
            self.localidad = l;
            
#ifdef DEBUG_ABMMEDICO
            NSLog(@"--- getCPByCalleLocalidad ---");
            NSLog(@"Query: %@", query);
            NSLog(@"CP: %@", self.codigoPostal);
#endif
            sqlite3_finalize(statement);
            return YES;
        }
    }
    sqlite3_finalize(statement);
    
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"--- getCPByCalleLocalidad ---");
    NSLog(@"Query: %@", query);
    NSLog(@" No encontrado");
#endif
    
    return NO;
}


- (BOOL)getCPByProvinciaLocalidad
{
    
    NSString *sql = @"";
    NSString *query = @"";
    Calle *c = [[Calle alloc] init];
    Localidad *l = [[Localidad alloc] init];
    
    // Busco primero la calle según el código postal seleccionado.
    sql = @"SELECT "
                "g_codpost.localidad as idlocalidad, "
                "g_local.calles as bcalles, "
                "g_codpost.cp as cp "
           "FROM geo_codigospostales g_codpost "
           "INNER JOIN geo_localidades g_local on "
                "((g_local.id = idlocalidad) AND "
                 "(g_local.provincia = g_codpost.provincia) AND "
                 "(g_local.pais = g_codpost.pais)) "
        "WHERE ((g_codpost.pais = %@) AND (g_local.descripcion = %@));";
    
    l.pais =  [SettingForKey(PAIS, @"54") integerValue];
    
    query = [NSString stringWithFormat:sql,
             [NSString intToStr:l.pais],
             [NSString stringQuoted:self.localidad.descripcion  ifNull:@"''"]];
    
    sqlite3_stmt *statement = [[DB getInstance] prepare:query];
	if (statement)
    {
		if (sqlite3_step(statement) == SQLITE_ROW) {
            
            // id Provincia
            l.provincia = self.provincia.idProvincia;
            
            // id Localidad
            l.idLocalidad = sqlite3_column_int(statement, 0);
            
            // Localidad
            l.descripcion = self.localidad.descripcion;
            
            // Posee Calles
            l.calles = sqlite3_column_int(statement, 1);
            
            // id Calle
            c.idCalle = 0;
            
            // Calle
            c.descripcion = self.calle.descripcion;
            
            // Código Postal
            self.codigoPostal = [NSString pCharToString:(char*)sqlite3_column_text(statement, 2)];
            
            if (l.calles > 0)
                self.calle  = c;
            self.localidad = l;
            
#ifdef DEBUG_ABMMEDICO
            NSLog(@"--- getCPByProvinciaLocalidad ---");
            NSLog(@"Query: %@", query);
            NSLog(@"CP: %@", self.codigoPostal);
#endif
            sqlite3_finalize(statement);
            return YES;
        }
    }
    sqlite3_finalize(statement);
    
    
#ifdef DEBUG_ABMMEDICO
    NSLog(@"--- getCPByProvinciaLocalidad ---");
    NSLog(@"Query: %@", query);
    NSLog(@" No encontrado");
#endif
    
    return NO;
}



@end
