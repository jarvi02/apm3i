//
//  ParteDiarioData.m
//  APM4
//
//  Created by Juan Pablo Garcia on 20/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ParteDiarioData.h"

@implementation ParteDiarioData

-(BOOL)isTarde
{
    return [self.actividad isEqualToString:@"T"];
}

#pragma mark -
#pragma mark Memory Management

-(void)dealloc
{
    [_descripcion release];
    [_pipe release];
    [_registro release];
    [_fecha release];
    [_actividad release];
    
    [super dealloc];
}

@end
