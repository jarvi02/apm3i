//
//  DB.m
//  CRUD
//
//  Created by Fabian Eduardo Pezet Vila on 12/06/12.
//  Copyright (c) 2012 Southopia. All rights reserved.
//

#import "DB.h"

/*
 * Esta clase implemeta un singleton, hacer un getInstance y usar 
 *
 * En el metodo applicationWillTerminate del Delegate colocar  [[Database getInstance] close];
 */

@implementation DB

+(DB *)getInstance{
	static DB *instance; 
	
	if(instance == nil){
		instance = [DB new];
	}
	return instance;	
}

-(void) dealloc{
	
	if (dbh){
		sqlite3_close(dbh);
	}
	
	[super dealloc];
}

-(id) init {
	
    self = [super init];
	if ( self ){
                
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *rwpath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"App.sqlite"];
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        if (![fileManager fileExistsAtPath:rwpath]) {
            
            NSString *dbpath = [[NSBundle mainBundle] pathForResource:@"App" ofType:@"sqlite"];
            [[NSFileManager defaultManager] copyItemAtPath:dbpath toPath:rwpath error:nil];
            
        }
        [fileManager release];
        sqlite3_open_v2([rwpath UTF8String], &dbh, SQLITE_OPEN_READWRITE, nil); 

	}
	return self;
}


 

#pragma mark -
#pragma mark Public Methods

-(sqlite3_stmt *) prepare:(NSString *) sql{
	
	const char *utfsql = [sql UTF8String];
	sqlite3_stmt *statement;
	
	if (sqlite3_prepare([self dbh], utfsql, -1, &statement, NULL) == SQLITE_OK){
		return statement;
	}else{
		return 0;
	}
}


-(void) excecuteSQL:(NSString*)_sql{
    
    char *err;
    if (sqlite3_exec([self dbh], [_sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
        NSAssert(0, _sql);
    }
    
}

- (int)getLastInsertedId{
    return sqlite3_last_insert_rowid([self dbh]);
}


-(void) close{
	
	if (dbh){
		sqlite3_close(dbh);
	}
}

-(sqlite3 *) dbh{
	
	return dbh;
}


@end
