//
//  PrevistoMSeleccion.m
//  APM3i
//
//  Created by Laura Busnahe on 2/7/13.
//  Copyright (c) 2013 Laura Busnahe. All rights reserved.
//

#import "PrevistoMSeleccionView.h"
#import "popListView.h"
#import "Config.h"
#import "DB.h"
#import "MTMedico.h"
#import "MTInstitucion.h"
#import "PrevistoMensual.h"
#import "MTPrevistoActividad.h"

#import "GlobalDefines.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define tagEntrada      1
#define tagSalida       2
#define tagMedico       3
#define tagInstitucion  4
#define tagOtros        5

//#define offsetY         64
#define offsetY         114

@interface PrevistoMSeleccionView ()

@end

@implementation PrevistoMSeleccionView


#pragma mark -
#pragma mark LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    // Esta función carga la vista con el NIB pasado como parámetro.
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.title = @"Previsto mensual";
        
        // Creo los array con los horarios Mañana.
        // Entrada.
        self.arrayHorarioME = [NSArray arrayWithObjects:
                               @"06:00", @"06:30", @"07:00", @"07:30", @"08:00", @"08:30", @"09:00", @"09:30",
                               @"10:00", @"10:30", @"11:00", @"11:30", nil
                               ];
        // Salida.
        self.arrayHorarioMS = [NSArray arrayWithObjects:
                               @"06:30", @"07:00", @"07:30", @"08:00", @"08:30", @"09:00", @"09:30", @"10:00",
                               @"10:30", @"11:00", @"11:30", @"12:00", nil
                               ];
        
        // Creo los array con los horarios Tarde.
        // Entrada.
        self.arrayHorarioTE = [NSArray arrayWithObjects:
                               @"12:00", @"12:30", @"13:00", @"13:30", @"14:00", @"14:30", @"15:00", @"15:30",
                               @"16:00", @"16:30", @"17:00", @"17:30", @"18:00", @"18:30", @"19:00", @"19:30", @"20:00",
                               @"20:30", nil
                               ];
        // Salida.
        self.arrayHorarioTS = [NSArray arrayWithObjects:
                               @"12:30", @"13:00", @"13:30", @"14:00", @"14:30", @"15:00", @"15:30", @"16:00",
                               @"16:30", @"17:00", @"17:30", @"18:00", @"18:30", @"19:00", @"19:30", @"20:00", @"20:30",
                               @"21:00", nil
                               ];
        
        // Cargo las imágenes para los chkButtons.
        imgChecked   = [UIImage imageNamed:@"ok_32.png"];
        imgUnchecked = [UIImage imageNamed:@"ok_32_off.png"];
       
        // Inicializo las variables para los chkButtons.
        checkedMedico     = TRUE;
        checkedIntitucion = FALSE;
        checkedOtros      = FALSE;

        // Médicos: Cargo los datos desde la base de datos.
        self.arrayMedicos = [MTMedico GetAll];
        
        // Instituciones: Cargo los datos desde la base de datos.
        self.arrayInstituciones = [MTInstitucion GetAll];

        self.popMenu = nil;
        
        self.oldAction = kMTEANone;
    }
    return self;
}

- (id)initViewWithDate:(NSDate*)date Evening:(BOOL)evening
{
    // Esta función inicia y muestra la vista con una fecha y turno (mañana o tarde).
    
    // Inicializo la vsta con el XIB "PrevistoMSeleccionView".
    self = [self initWithNibName:@"PrevistoMSeleccionView" bundle:nil];
    if (self)
    {
        self.Date      = date;      // Establezco la fecha para la vista.
        self.Evening   = evening;   // Establezco el turno para la vista.
        self.action    = kMTEAAdd;  // Como no se inicializa con datos, defino la acción de agrgar.
        
        // Inicializo el objeto de selección.
        self.Selection = [[MTPrevistoActividad alloc] init];
    }
    
    // TODO: Implementar el código de abajo que chequea si es obligatoria el horario de salida de la mañana y/o tarde.
    // Propiedades en Config Global. Valor (str) S o N
    // USAHORASALIDAM
    // USAHORASALIDAT
//    COPIADO DEL CODIGO DE ANDROID
//    try {
//        mHorSalidaMObligatorio = Apm4Parametrizacion.activo(Apm4Parametrizacion.USAHORASALIDAM);
//        mHorSalidaTObligatorio = Apm4Parametrizacion.activo(Apm4Parametrizacion.USAHORASALIDAT);
//    } catch (Exception e) {
//        mHorSalidaMObligatorio = false;
//        mHorSalidaTObligatorio = false;
//    }
    
    return self;
}

- (id)initViewWithPrevisto:(PrevistoMensual*)previsto Data:(MTPrevistoActividad*)activity objectIndex:(NSInteger)oIndex
{
    // Inicializo la vista con la fecha y turno del previsto pasado como parámetro.
    self = [self initViewWithDate:previsto.date Evening:previsto.type == kMorning ? FALSE: TRUE];
    
    if (self)
    {
        self.selectionIndex = oIndex;
        
        // Chequeo si como parámetro se pasó una actividad ya existente.
        if (activity != nil)
        {
            // Guardo el indice en la lista de objectos de quien llamó.
            self.selectionIndex = oIndex;
            
            self.Selection = [activity copy];
            self.action = kMTEAChange;          // Defino el modo como de Edición ya que se suministraron los datos de la activida.
        
        } else
        {
            self.Selection = [[MTPrevistoActividad alloc] init];
        }
        
        self.oldAction = previsto.action;
    }
    
#ifdef DEBUG_PREVISTOM
    // Solo DEBUG.
    // ---------------------------------------------------------
    NSLog(@"--- PrevistoMSeleccion.initViewWithPrevisto ---");
    NSLog(@"action: %@",
          self.action == kMTEAAdd? @"Add":
          self.action == kMTEAChange? @"Modify":
          self.action == kMTEADelete? @"Delete":
                                      @"None");
    if (previsto.action == kMTEADelete)
        NSLog(@"*Debería ser Modify*");
    // ---------------------------------------------------------
#endif
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Oculto el botón eliminar y solo lo muestro si se trata de una modificación
    
    [self crearToolBar];
    
    
    // Asigno los tags a los botones.
    
    
    
    // Muestro el turno en la parte superior izquierda de la vista.
    self.lblTurno.text = self.Evening ? @"TARDE": @"MAÑANA";

    // Muestro la fecha en la parte superior derecha de la vista
    NSDateFormatter* formatedDate = [[NSDateFormatter alloc] init];
    [formatedDate setDateFormat:@"EEEE, "];
    NSString *sDate = [[formatedDate stringFromDate:self.Date] capitalizedString];
    [formatedDate setDateFormat:@"d 'de' "];
    sDate = [sDate stringByAppendingString:[formatedDate stringFromDate:self.Date]];
    [formatedDate setDateFormat:@"MMMM "];
    sDate = [sDate stringByAppendingString:[[formatedDate stringFromDate:self.Date] capitalizedString]];
    [formatedDate setDateFormat:@"'de' yyyy"];
    sDate = [sDate stringByAppendingString:[formatedDate stringFromDate:self.Date]];
    self.lblFecha.text = sDate;
    
    
    // Chequeo si se pasó alguna entidad
    [formatedDate setDateFormat:@"HH:mm"];      // Defino el formato de exportación de la hora.
    self.edtEntrada.text = [formatedDate stringFromDate:self.Selection.entrada];  // Asigno el horario de entrada de la actividad.
    self.edtSalida.text  = [formatedDate stringFromDate:self.Selection.salida];   // Asigno el horario de salida de la actividad.
    
    // Chequo si se agrega o edita la actividad.
    if (self.action != kMTEAAdd)
    {
        switch (self.Selection.tipoEntidad)
        {
            // Médico
            case kTEMedico:
                self.edtMedico.text = [self.Selection.entidad getDescription];          // Completo el nombre del médico.
                [self enableChkMedico:true];                                            // Activo el switch de Médico.
                break;
            
            // Institución
            case kTEInstitucion:
                self.edtInstitucion.text = [self.Selection.entidad getDescription];     // Completo el nombre de la institución.
                [self enableChkInstitucion:true];                                       // Activo el switch de Instición.
                break;
            
            // Otros
            default:
                self.edtOtros.text = [self.Selection.entidad getDescription];           // Completo el detalle de Otros.
                [self enableChkOtros:true];                                             // Activo el switch Otros.
                break;
        }
    }
}



#pragma mark -
#pragma mark Overrides

-(BOOL)hasFooter
{
    return NO;
}

-(NSString *)getTitleForHeader{
    return @"Previsto";
}

-(UIImage *)getIconoForHeader{
//    return [UIImage imageNamed:@"user_add"];
    return [UIImage imageNamed:@"ficha_48"];
}


#pragma mark -
#pragma mark Privated methods
-(void)crearToolBar {
    // flex item used to separate the left groups items and right grouped items
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:nil
                                                                              action:nil];
    
    // create a special tab bar item with a custom image and title
    UIBarButtonItem *eliminarItem = [[UIBarButtonItem alloc] initWithTitle:@"Eliminar"
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self
                                                                    action:@selector(tapEliminar:)];
    eliminarItem.width = 70;
    eliminarItem.tintColor = UIColorFromRGB(eliminarButtonColor);
    
    UIBarButtonItem *aceptarItem = [[UIBarButtonItem alloc] initWithTitle:@"Aceptar"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(tapAceptar:)];
    aceptarItem.width = 70;
    
    UIBarButtonItem *cancelarItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar"
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self
                                                                    action:@selector(tapCancelar:)];
    cancelarItem.width = 70;
    
    NSArray *items;
    if (self.action == kMTEAAdd)
        items = [NSArray arrayWithObjects: flexItem, aceptarItem, cancelarItem, flexItem, nil];
    else
        items = [NSArray arrayWithObjects: eliminarItem, flexItem, aceptarItem, cancelarItem, flexItem, nil];
    
    [self.toolBar setItems:items animated:NO];
}


#pragma mark -
#pragma mark chkButtons Privated methods
- (void)updateChkImages
{
    [self.chkMedico      setImage:checkedMedico? imgChecked: imgUnchecked
                         forState:UIControlStateNormal];
//    [self.chkMedico      setImage:checkedMedico? imgChecked: imgUnchecked
//                         forState:UIControlStateHighlighted];
    
    [self.chkInstitucion setImage:checkedIntitucion? imgChecked: imgUnchecked
                         forState:UIControlStateNormal];
//    [self.chkInstitucion setImage:checkedIntitucion? imgChecked: imgUnchecked
//                         forState:UIControlStateHighlighted];
    
    [self.chkOtros       setImage:checkedOtros? imgChecked: imgUnchecked
                         forState:UIControlStateNormal];
//    [self.chkOtros       setImage:checkedOtros? imgChecked: imgUnchecked
//                         forState:UIControlStateHighlighted];
}

- (void)enableChkMedico:(BOOL)enable
{
    // Chequeo si el switch de Médico debe ser activado.
    if (enable)
    {
        // Si es así, desactivo el de instituciones y otros.
        checkedMedico     = true;
        checkedIntitucion = false;
        checkedOtros      = false;
        
        
        self.edtInstitucion.text = @"";                     // Borro el texto de la selección de instituciones.
        self.edtOtros.text = @"";                           // Borro el texto de otros, y deshabilito el textField.
        self.edtOtros.enabled = FALSE;
        self.edtOtros.backgroundColor = self.edtMedico.backgroundColor;
    } else
        checkedMedico = false;
    
    [self updateChkImages];
}

- (void)enableChkInstitucion:(BOOL)enable
{
    // Chequeo si el switch de Instituciones debe ser activado.
    if (enable)
    {
        // Si es así, desactivo el de instituciones y otros.
        [self enableChkMedico:false];
        checkedIntitucion = true;
        [self enableChkOtros:false];
        
        self.edtMedico.text = @"";                          // Borro el texto de la selección de medicos.
        self.edtOtros.text = @"";                           // Borro el texto de otros, y deshabilito el textField.
        self.edtOtros.enabled = FALSE;
        self.edtOtros.backgroundColor = self.edtMedico.backgroundColor;
    } else
        checkedIntitucion = false;
    
    [self updateChkImages];
}

- (void)enableChkOtros:(BOOL)enable
{
    // Chequeo si el switch de Otros debe ser activado.
    if (enable)
    {
        // Si es así, desactivo el de instituciones y otros.
        [self enableChkMedico:false];
        [self enableChkInstitucion:false];
        checkedOtros      = true;
        
        // Chequeo si el tipo de entidad ya era Unknown y que haya sido creada previamente.
        if ((self.Selection.tipoEntidad != kTEUnknown) ||
            (!self.Selection.entidad))
        {
            // Si no cumple esta condición, creo la entidad y defino el tipo.
            MTEntidad* newEntity;
            newEntity = [[MTEntidad alloc] init];
            
            self.Selection.entidad = newEntity;
            self.Selection.tipoEntidad = kTEUnknown;
        }
        // Asigno la ID de registro nula ya que no corresponde ni a un médico ni a una institución.
        self.Selection.entidad.recID = NULL;
        
        // Borro el texto de médicos e instituciones y habilito el campo Otros.
        self.edtMedico.text = @"";
        self.edtInstitucion.text = @"";
        self.edtOtros.enabled = TRUE;
        self.edtOtros.backgroundColor = [UIColor whiteColor];
        
    } else
    {
        checkedOtros = false;
        // Deshabilito el campo otros.
        self.edtOtros.enabled = FALSE;
        self.edtOtros.backgroundColor = self.edtMedico.backgroundColor;
    }
    
    [self updateChkImages];
}



#pragma mark-
#pragma mark IBActions

- (IBAction)tapEntrada:(id)sender
{
    // Obtengo la vista desde la cual se llama al popover.
    UIView* popRectView = (UIView*)sender;
    CGSize popOverSize = CGSizeMake(163, 500);
    
    // Creo el popover del menú, si no fue creado.
    if (!self.popMenu)
    {
        self.popMenu = [[MTpopListView alloc] initWithArray:self.arrayHorarioME Rect:CGRectMake(0, 0, popOverSize.width, popOverSize.height)];
        self.popMenu.delegate = self;
    }
    [self.popMenu dismissPopoverAnimated:NO];
    
    [self.popMenu setTitleAndParameters:@"Entrada" rowData: self.Evening? self.arrayHorarioTE: self.arrayHorarioME];
    
    [self.popMenu setSize:popOverSize];
    [self.popMenu useSearch:FALSE];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x,
                               popRectView.frame.origin.y + offsetY,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    [self.popMenu presentPopoverFromRectWithSender:popSourceRect
                                            Sender:(id)self.edtEntrada
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:YES];
}

- (IBAction)tapSalida:(id)sender
{
    // Obtengo la vista desde la cual se llama al popover.
    UIView* popRectView = (UIView*)sender;
    CGSize popOverSize = CGSizeMake(163, 500);
    
    // Creo el popover del menú.
    if (!self.popMenu)
    {
        self.popMenu = [[MTpopListView alloc] initWithArray:self.arrayHorarioME Rect:CGRectMake(0, 0, popOverSize.width, popOverSize.height)];
        self.popMenu.delegate = self;
    }
    [self.popMenu dismissPopoverAnimated:NO];
    
    
    [self.popMenu setTitleAndParameters:@"Salida" rowData:self.Evening? self.arrayHorarioTS: self.arrayHorarioMS];
    
    [self.popMenu setSize:popOverSize];
    [self.popMenu useSearch:FALSE];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x,
                               popRectView.frame.origin.y + offsetY,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    [self.popMenu presentPopoverFromRectWithSender:popSourceRect
                                            Sender:(id)self.edtSalida
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:YES];
}

- (IBAction)tapMedico:(id)sender
{
    // Obtengo la vista desde la cual se llama al popover.
    UIView* popRectView = (UIView*)sender;
    CGSize popOverSize = CGSizeMake(350, 500);
    
    // Creo el popover del menú.
    if (!self.popMenu)
    {
        self.popMenu = [[MTpopListView alloc] initWithArray:self.arrayHorarioME Rect:CGRectMake(0, 0, popOverSize.width, popOverSize.height)];
        self.popMenu.delegate = self;
    }
    [self.popMenu dismissPopoverAnimated:NO];
    
    [self.popMenu setTitleAndParameters:@"Médicos" rowData:self.arrayMedicos];
    [self.popMenu setSize:popOverSize];
    [self.popMenu useSearch:TRUE];

    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x,
                               popRectView.frame.origin.y + offsetY,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    [self.popMenu presentPopoverFromRectWithSender:popSourceRect
                                            Sender:(id)self.edtMedico
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:YES];
}

- (IBAction)tapInstitucion:(id)sender
{
    // Obtengo la vista desde la cual se llama al popover.
    UIView* popRectView = (UIView*)sender;
    CGSize popOverSize = CGSizeMake(450, 500);
    
    // Creo el popover del menú.
    if (!self.popMenu)
    {
        self.popMenu = [[MTpopListView alloc] initWithArray:self.arrayHorarioME Rect:CGRectMake(0, 0, popOverSize.width, popOverSize.height)];
        self.popMenu.delegate = self;
    }
    [self.popMenu dismissPopoverAnimated:NO];
    
    [self.popMenu setTitleAndParameters:@"Instituciones" rowData:self.arrayInstituciones];
    [self.popMenu setSize:popOverSize];
    [self.popMenu useSearch:TRUE];
    
    CGRect popSourceRect;
    popSourceRect = CGRectMake(popRectView.frame.origin.x,
                               popRectView.frame.origin.y + offsetY,
                               popRectView.frame.size.width,
                               popRectView.frame.size.height);
    [self.popMenu presentPopoverFromRectWithSender:popSourceRect
                                            Sender:(id)self.edtInstitucion
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:YES];
}

- (IBAction)tapChkMedico:(id)sender
{
    [self enableChkMedico:!checkedMedico];
}

- (IBAction)tapChkInstitucion:(id)sender
{
    [self enableChkInstitucion:!checkedIntitucion];
}

- (IBAction)tapChkOtros:(id)sender
{
    [self enableChkOtros:!checkedOtros];
}


#pragma mark-
#pragma mark toolBar IBActions methods
- (IBAction)tapAceptar:(id)sender
{
    // Chequeo si se estaba editando el textField Otros y si es así termino la edición.
    if ([self.edtOtros isEditing])
    {
        [self textFieldDidEndEditing:self.edtOtros];    // Actualizo el texto en la selección.
        [self.edtOtros endEditing:TRUE];                // Finalizo la edición del campo.
    }
    
    
    // Chequeo si la vista tiene delegado.
    if (!self.delegate)
        return;
    
    // Chequeo los horarios seleccionados.
    if (!self.Evening)
    {
        // MAÑANA
        // Chequeo que tenga horario de entrada y de salida.
        if ((self.Selection.entrada == nil) || (self.Selection.salida == nil))
        {
            [self showAlert:@"Debe completar el horario de entrada y salida del previsto."
                     Titulo:@"Datos incompletos"];
            return;
        }
        
        // Comparo si la hora de entrada es menor que la de salida.
        if ([self.Selection.entrada compare:self.Selection.salida] != NSOrderedAscending)
        {
            [self showAlert:@"El horario de salida debe ser mayor al de entrada."
                     Titulo:@"Error en datos"];
            return;
        }
    } else
    {
        // TARDE
        // Chequeo que tenga horario de entrada y de salida.
        if (self.Selection.entrada == nil)
        {
            [self showAlert:@"Al menos debe completar el horario de entrada del previsto."
                     Titulo:@"Datos incompletos"];
            return;
        }
        
        // Comparo si se seleccionó horario de salida y que la hora de entrada es menor que la de salida.
        if (self.Selection.salida != nil)
            if ([self.Selection.entrada compare:self.Selection.salida] != NSOrderedAscending)
            {
                [self showAlert:@"El horario de salida debe ser mayor al de entrada."
                         Titulo:@"Error en datos"];
                return;
            }
    }
    
    // Chequeo que alguno de los 3 swtichs esté seleccionado.
    if ((!checkedMedico) &&
        (!checkedIntitucion) &&
        (!checkedOtros))
    {
        [self showAlert:@"No se seleccionó ningún médico, intitución, u otro motivo para el previsto."
                 Titulo:@"Datos incompletos"];
        return;
    }
    
    // Chequeo que el el medico, institición u otro correspondan al tipo seleccionado y no estén vacios.
    if (!(((self.Selection.entidad != nil) && ([self.Selection.entidad.descripcion length] >0)) &&
          (
           ((self.Selection.tipoEntidad == kTEMedico)      && (checkedMedico)     && ([self.edtMedico.text length] >0)) ||
           ((self.Selection.tipoEntidad == kTEInstitucion) && (checkedIntitucion) && ([self.edtInstitucion.text length] >0)) ||
           ((self.Selection.tipoEntidad == kTEUnknown)     && (checkedOtros)      && ([self.edtOtros.text length] >0))
           )
          ))
    {
        // Chequeo cuál de los switches está activado.
        if (checkedMedico)
            [self showAlert:@"Debe seleccionar un médico del listado."
                     Titulo:@"Datos incompletos"];
        
        if (checkedIntitucion)
            [self showAlert:@"Debe seleccionar una institución del listado."
                     Titulo:@"Datos incompletos"];
        
        if (checkedOtros)
            [self showAlert:@"Debe ingresar una descipción para el previsto."
                     Titulo:@"Datos incompletos"];
        return;
    }

    if ((self.oldAction == kMTEADelete) && (self.action == kMTEAAdd))
        self.action = kMTEAChange;

    
    // Llamo al método del delegate que procesa la acción realizada en esta vista.
    [self.delegate pmEditPerformAction:self
                                Action:self.action
                              withData:self.Selection
                           objectIndex:self.selectionIndex];
    
    // Cierro la vista.
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)tapCancelar:(id)sender;
{
    if (self.delegate)
    {
        // Si el delegate está asignado, llamo el método que procesa la acción realizada.
        [self.delegate pmEditPerformAction:self
                                    Action:kMTEANone
                                  withData:nil
                               objectIndex:self.selectionIndex];
    }
    
    // Cierro la vista.
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)tapEliminar:(id)sender
{
    // Llama al AlertView de confirmación antes de eliminar el previsto seleccionado.
   	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Eliminar previsto"
                                                    message:@"¿Está seguro que desea eliminar esta actividad?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Si", nil];
	[alert show];
//	[alert release];
}


- (IBAction)tapOtherButton:(id)sender
{
    if ([self.edtOtros isEditing])
        [self.edtOtros endEditing:TRUE];
}


#pragma mark-
#pragma mark UIAlertView related methods

- (void)showAlert:(NSString*)mensaje Titulo:(NSString*) titulo
{
    // Llama al AlertView de confirmación antes de eliminar el previsto seleccionado.
   	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titulo
                                                    message:mensaje
                                                   delegate:nil
                                          cancelButtonTitle:@"Aceptar"
                                          otherButtonTitles:nil];
	[alert show];
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // buttonIndex = 0 : No
    
    // buttonIndex = 1 : Si
    if (buttonIndex == 1)
    {
        if (self.delegate)
        {
            // Si el delegate está asignado, llamo el método que procesa la acción realizada.
           [self.delegate pmEditPerformAction:self
                                       Action:kMTEADelete
                                     withData:nil
                                  objectIndex:self.selectionIndex];
        }
        
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
- (void)alertViewCancel:(UIAlertView *)alertView
{
    
}


#pragma mark-
#pragma mark popListView related methods

- (void)popListView:(MTpopListView *)controller senderControl:(id)sender didSelectRow:(NSInteger *)MenuIndex selectedText:(NSString *)RowText
{
    UITextField *textField = (UITextField *)sender;
    textField.text = [NSString stringWithFormat:@"%@", RowText];
    
//    NSDateFormatter* formatedDate = [[NSDateFormatter alloc] init];
    
        
    // Chequeo quién llamó el popUp de sleección
    switch (textField.tag) {
        
        // Horario de Entrada
        case tagEntrada:
//            [formatedDate setDateFormat:@"HH:mm"];
            self.Selection.entrada = [[NSDate alloc] initWithTimeString:[controller.rowDataFiltered objectAtIndex:MenuIndex]];
            break;
            
        // Horario de Salida
        case tagSalida:
//            [formatedDate setDateFormat:@"HH:mm"];
            self.Selection.salida = [[NSDate alloc] initWithTimeString:RowText];
            break;
            
        // Médico
        case tagMedico:
            self.Selection.entidad = [controller.rowDataFiltered objectAtIndex:MenuIndex];
            self.Selection.tipoEntidad = kTEMedico;
            [self enableChkMedico:true];
            break;
            
        // Institución
        case tagInstitucion:
            self.Selection.entidad = [controller.rowDataFiltered objectAtIndex:MenuIndex];
            self.Selection.tipoEntidad = kTEInstitucion;
            [self enableChkInstitucion:true];
            break;
        
        default:
            break;
    }
    
}

#pragma mark-
#pragma mark textField related methods

// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Elimina los espacios al inicio y al final del texto
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([textField.text length] > 62)
        textField.text = [textField.text substringToIndex:62];
    
    self.Selection.entidad.descripcion = textField.text;
}

// called when clear button pressed. return NO to ignore (no notifications)
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

// called when 'return' key pressed. return NO to ignore.
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}



#pragma mark -
#pragma mark Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
