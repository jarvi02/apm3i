//
//  Programados.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 25/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Programados : NSObject

@property(nonatomic, assign) NSInteger orden;
@property(nonatomic, assign) NSInteger producto;
@property(nonatomic, retain) NSString *descripcion;
@property(nonatomic, assign) NSInteger cantidad;
@property(nonatomic, retain) NSString *literatura;
@property(nonatomic, retain) NSString *obsequio;
@property(nonatomic, retain) NSString *objetivos;


+(NSArray*)GetAllForDoctor:(NSInteger)idDoctor;

@end
