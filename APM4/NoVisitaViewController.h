//
//  NoVisitaViewController.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 26/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "CustomViewController.h"
#import "CarteraViewController.h"
#import "MotivosNoVisita.h"
#import "TableForComboPopoverViewControllerDelegate.h"
#import "DateSelectorViewController.h"
#import "FichaVisita.h"

#import "MTVisita.h"
#import "MTMedico.h"

@class MTNoVisita;

typedef NS_ENUM(NSInteger, NoVisitaModo)
{
    NoVisitaModoAlta,
    NoVisitaModoModificar,
    NoVisitaModoVer
};

@interface NoVisitaViewController : CustomViewController<TableForComboPopoverViewControllerDelegate, DateSelectorDelegate>


- (id)initWithDefaultNib;

@property (nonatomic, assign) CarteraViewController *carteraViewControllerReference;
@property (nonatomic, strong) IBOutlet UILabel      *lblNoVisitaTransmitida;
@property(nonatomic, strong) IBOutlet UITextField   *txtFecha;
@property (nonatomic, strong) IBOutlet UIButton     *btnFecha;
@property(nonatomic, strong) IBOutlet UITextField   *txtTipo;
@property (nonatomic, strong) IBOutlet UIButton     *btnTipo;
@property (nonatomic, strong) IBOutlet UIButton     *btnManiana;
@property (nonatomic, strong) IBOutlet UIButton     *btnTarde;
@property (nonatomic, strong) IBOutlet UIButton     *btnManianaFrame;
@property (nonatomic, strong) IBOutlet UIButton     *btnTardeFrame;
@property (nonatomic, strong) IBOutlet UIButton     *btnGuardar;
@property (nonatomic, strong) IBOutlet UIButton     *btnCancelar;


@property(nonatomic, assign) NoVisitaModo   modo;
@property(nonatomic, strong) MTVisita     *noVisita;
@property(nonatomic, assign) FichaVisita    *ficha;

- (IBAction)selectManiana:(id)sender;
- (IBAction)selectTarde:(id)sender;
- (IBAction)selectFecha:(id)sender;
- (IBAction)selectTipo:(id)sender;


- (IBAction)guardar:(id)sender;
- (IBAction)cancelar:(id)sender;

@end
