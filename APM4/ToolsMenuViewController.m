//
//  ToolsMenuViewController.m
//  APM4
//
//  Created by Juan Pablo Garcia on 05/02/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ToolsMenuViewController.h"
#import "TransmitirViewController.h"
#import "ConfiguracionViewController.h"
#import "AboutUsViewController.h"
#import "AjusteMMViewController.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"
#import "Config.h"

#import "GlobalDefines.h"

@interface ToolsMenuViewController ()

-(void)navigateToTransmision;
-(void)navigateToConfiguracion;
-(void)navigateToAcerdaNuestro;

-(void)aplicarCierreciclo;
-(void)revertirCierreciclo;

@end

@implementation ToolsMenuViewController

-(NSArray *)options
{
    return [NSArray arrayWithObjects:
            @"Transmisión",
            @"Aplicar cierre de ciclo",
            @"Revertir cierre de ciclo",
            @"Configuración",
            @"Ajuste MM",
            @"Acerca Nuestro",
            nil];
}

-(NSString *)title
{
    return @"Herramientas";
}


- (void)selectedOptionsAtIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
            // Transmision
            [self navigateToTransmision];
            break;
        case 1:
            // Aplicar cierre de ciclo
            [self aplicarCierreciclo];
            break;
        case 2:
            // Revertir cierre de ciclo
            [self revertirCierreciclo];
            break;
        case 3:
            // Configuracion
            [self navigateToConfiguracion];
            break;
        case 4:
            // Ajuste MM
            [self navigateToAjusteMM];
            break;
        case 5:
            // Acerca de
            [self navigateToAcerdaNuestro];
            break;
        default:
            [self showNotImplemented];
    }
}   

-(BOOL)isEnableOptionsAtIndex:(NSUInteger)index {

    switch (index) {
        case 0:
            // Transmision
            return YES;
            break;
        case 1:
            // Aplicar cierre de ciclo
            return YES;
            break;
        case 2:
            // Revertir cierre de ciclo
            return YES;
            break;
        case 3:
            // Configuración
            return YES;
            break;
        case 4:
            // Ajuste MM
#ifdef INCLUDE_AJUSTEMM
            return YES;
#else
            return NO;
#endif
            break;
        case 5:
            // Acerda de
            return YES;
            break;
        default:
            return NO;
    }
    
}

#pragma mark -
#pragma mark Menu

-(void)navigateToConfiguracion
{
    ConfiguracionViewController *tvc = [[[ConfiguracionViewController alloc] initWithNibName:@"ConfiguracionViewController" bundle:nil] autorelease];
    [self navigateToController:tvc];
}

-(void)navigateToTransmision
{
    TransmitirViewController *tvc = [[[TransmitirViewController alloc] initWithNibName:@"TransmitirViewController" bundle:nil] autorelease];
    [self navigateToController:tvc];
}


-(void)navigateToAcerdaNuestro {
    
    
    AboutUsViewController *o = [[[AboutUsViewController alloc] initWithNibName:@"AboutUsViewController" bundle:nil] autorelease];
    [self navigateToController:o];
}

-(void)navigateToAjusteMM {
    
    
    AjusteMMViewController *o = [[[AjusteMMViewController alloc] initViewDefaultNib] autorelease];
    [self navigateToController:o];
}


-(void)aplicarCierreciclo {
 
    
    if ( [[[Config shareInstance] find:TRANID_OK] isEqualToString:@"1"]) {
        
        if ([[[Config shareInstance] find:CERRAR_CICLO] isEqualToString:@"0"]) {
            
            RIButtonItem *cancelItem = [RIButtonItem item];
            cancelItem.label = @"NO";
            cancelItem.action = ^{
                // NO hago nada
            };
            
            RIButtonItem *aceptarItem = [RIButtonItem item];
            aceptarItem.label = @"SI";
            aceptarItem.action = ^ {
                
                [[Config shareInstance] update:CERRAR_CICLO value:@"1"];
                
            };
            
            NSString *msg = [NSString stringWithFormat:@"¿ Confirma el cierre del ciclo %@ ?", [[Config shareInstance] find:CICLO]];
            
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cierre de ciclo"
                                                                message:msg
                                                       cancelButtonItem:cancelItem
                                                       otherButtonItems:aceptarItem, nil];
            [alertView show];
            [alertView release];
            
            
        } else {
        
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cierre de ciclo" message:@"Ciclo ya cerrado" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
            [alert show];
            [alert release];
            
        }
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cierre de ciclo" message:@"Existe una transmisión pendiente" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }

    
}

-(void)revertirCierreciclo {
    

    if ([[[Config shareInstance] find:TRANID_OK] isEqualToString:@"1"]) {
        
        if ([[[Config shareInstance] find:CERRAR_CICLO] isEqualToString:@"0"]) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Revertir cierre de ciclo" message:@"Ciclo pendiente de cierre" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
            [alert show];
            [alert release];

            
        } else {
            
            RIButtonItem *cancelItem = [RIButtonItem item];
            cancelItem.label = @"NO";
            cancelItem.action = ^{
                // NO hago nada
            };
            
            RIButtonItem *aceptarItem = [RIButtonItem item];
            aceptarItem.label = @"SI";
            aceptarItem.action = ^ {
                
                [[Config shareInstance] update:CERRAR_CICLO value:@"0"];
                
            };
            
            
            NSString *msg = [NSString stringWithFormat:@"¿ Confirma revertir el cierre del ciclo %@ ?", [[Config shareInstance] find:CICLO]];
            
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Revertir cierre de ciclo"
                                                        message:msg
                                                        cancelButtonItem:cancelItem
                                                        otherButtonItems:aceptarItem, nil];
            [alertView show];
            [alertView release];

        }
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Revertir cierre de ciclo" message:@"Existe una transmisión pendiente" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    

    
}

@end

