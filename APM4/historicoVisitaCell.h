//
//  historicoVisitaCell.h
//  APM4
//
//  Created by Laura Busnahe on 7/15/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface historicoVisitaCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *txtTipo;
@property (retain, nonatomic) IBOutlet UILabel *txt01;
@property (retain, nonatomic) IBOutlet UILabel *txt02;
@property (retain, nonatomic) IBOutlet UILabel *txt03;
@property (retain, nonatomic) IBOutlet UILabel *txt04;
@property (retain, nonatomic) IBOutlet UILabel *txt05;
@property (retain, nonatomic) IBOutlet UILabel *txt06;
@property (retain, nonatomic) IBOutlet UILabel *txt07;
@property (retain, nonatomic) IBOutlet UILabel *txt08;
@property (retain, nonatomic) IBOutlet UILabel *txt09;
@property (retain, nonatomic) IBOutlet UILabel *txt10;
@property (retain, nonatomic) IBOutlet UILabel *txt11;
@property (retain, nonatomic) IBOutlet UILabel *txt12;
@property (retain, nonatomic) IBOutlet UILabel *txtTotal;


@end
