//
//  Horario.h
//  APM4
//
//  Created by Fabian E. Pezet Vila on 04/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Horario : NSObject

@property(nonatomic, assign) BOOL lunes;
@property(nonatomic, assign) BOOL martes;
@property(nonatomic, assign) BOOL miercoles;
@property(nonatomic, assign) BOOL jueves;
@property(nonatomic, assign) BOOL viernes;

@property(nonatomic, retain) NSString *horaDesde;
@property(nonatomic, retain) NSString *horaHasta;

@property (nonatomic, assign) NSInteger iHrDesde;
@property (nonatomic, assign) NSInteger iMinDesde;
@property (nonatomic, assign) NSInteger iHrHasta;
@property (nonatomic, assign) NSInteger iMinHasta;


@property(nonatomic, retain) NSString *comentarios;
@property(nonatomic) NSInteger recID;

-(NSString*) descripcion;
-(id)copy;

-(void)saveForMedico:(NSInteger)idMedico Domicilio:(NSInteger)idDomicilio;

+(void)deleteAllForMedico:(NSInteger)idMedico;
+(NSArray*)getAllByMedico:(NSUInteger)idMedico Domicilio:(NSUInteger)idDomicilio;

-(NSString*)isOkForSave;

//- (void)showAlert:(NSString*)mensaje Titulo:(NSString*) titulo;

@end
