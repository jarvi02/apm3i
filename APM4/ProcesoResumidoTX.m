//
//  ProcesoResumidoTX.m
//  APM4
//
//  Created by Fabian E. Pezet Vila on 21/03/13.
//  Copyright (c) 2013 Meditec. All rights reserved.
//

#import "ProcesoResumidoTX.h"
#import "Config.h"
#import "DB.h"
#import "ServiceVerificarExisteNovedad.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface ProcesoResumidoTX()
-(NSData*) generarHeader:(ACCIONTX) accion;
-(NSData*) generarCuerpo:(ACCIONTX) accion;
-(NSData*) generarPie:(ACCIONTX) accion;
-(void) enviarNovedad:(NSData*)novedad;
-(BOOL) verificarNovedadEnServidor;
@end

@implementation ProcesoResumidoTX


-(void)start {
    
    NSInteger tranid = [[[Config shareInstance] find:TRANID] integerValue]+1;
    NSString *strnaid = [NSString stringWithFormat:@"%d", tranid];
    [[Config shareInstance] update:TRANID value:strnaid];
    [[Config shareInstance] update:TRANID_OK value:@"0"];
    
    ACCIONTX accion;
    if ([SettingForKey(CERRAR_CICLO, @"0") isEqualToString:@"1"]) {
        accion = CIERRE_CICLO;
    } else if ([SettingForKey(INICIALIZAR, @"0") isEqualToString:@"1"]) {
        accion = INICIALIZACION;
    } else {
        accion = NOVEDADES;
    }
    
    NSMutableData *data = [[NSMutableData alloc] initWithCapacity:0];
    
    [data appendData:[self generarHeader:accion]];
    [data appendData:[self generarCuerpo:accion]];
    [data appendData:[self generarPie:accion]];
    
    [self enviarNovedad:data];
    
    [data release];
    
    if ([self verificarNovedadEnServidor]) {
        
        NSString *sqlBorrarLogNovedades = @"delete from log_novedades;";
        
        [[DB getInstance] excecuteSQL:sqlBorrarLogNovedades];
        
        [[Config shareInstance] update:INICIALIZAR value:@"0"];
        [[Config shareInstance] update:TRANID_OK value:@"1"];
        
    } else {
        
        NSInteger tranid = [[[Config shareInstance] find:TRANID] integerValue]-1;
        NSString *strnaid = [NSString stringWithFormat:@"%d", tranid];
        [[Config shareInstance] update:TRANID value:strnaid];
        
        NSString *msg = [NSString stringWithFormat:@"TX0008 (sec. %@) Nunca se genero la transacción anterior en el servidor.", SettingForKey(TRANID, @"")];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alerta" message:msg delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        
    }
    
}


-(NSData*) generarHeader:(ACCIONTX) accion {
    
    
    NSMutableData *data = [[NSMutableData alloc] initWithCapacity:0];
    @autoreleasepool {
        
        NSString *str = @"apm3i.i\r\n<HEADER>\r\nENTIDAD=%@\r\nCICLO=%@\r\nEXEVERSION=1.0.33.46\r\nANDROIDCODENAME=REL\r\nANDROIDINCREMENTAL=UBKL2\r\nANDROIDRELEASE=3.2\r\nANDROIDSDK_INT=13\r\nAPM4VERSIONCODE=46\r\nAPM4VERSIONNAME=1.0.33\r\nTRANSACTIONID=%@\r\n<FIN>\r\n<REQUERIMIENTOS>\r\n";
        str = [NSString stringWithFormat:str,SettingForKey(ENTIDAD, @""),SettingForKey(CICLO, @""),SettingForKey(TRANID, @"")];
        [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        if (accion == INICIALIZACION) {
            
            str = @"cfg_ultimo\r\nINICIALIZACION\r\ncfg_global\r\nmed_rechazados\r\nstd_visitasexternas\r\nstd_movimientofichero\r\n";
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
        } else if (accion == CIERRE_CICLO) {
            str = @"cfg_ultimo\r\nCERRARCICLO\r\ncfg_global\r\nmail_recibidos\r\nmed_rechazados\r\nstd_visitasexternas\r\nstd_movimientofichero\r\nmail_secuencia\r\n";
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
        } else if (accion == NOVEDADES) {
            
            str = @"cfg_ultimo\r\ncfg_global\r\nmed_rechazados\r\nstd_visitasexternas\r\nstd_movimientofichero\r\nmtc_productos\r\nmtc_lotes\r\nmtc_feriados\r\n";
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
        } else {
            str = @"REQUERIMIENTO NO ENTENDIDO\r\n";
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        
        str = @"<FIN>\r\n";
        [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    return [data autorelease];
    
}
-(NSData*) generarCuerpo:(ACCIONTX) accion {
    
    NSMutableData *data = [[NSMutableData alloc] initWithCapacity:0];
    @autoreleasepool {
        NSString *str;
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:10];
        
        NSString *QUERY_NOVEDADES = @"select nov.tabla, nov.secuencia || \"|\" || nov.tipo || \"|\" || nov.registro as registro, nov.tranid, nov.transmitido from log_novedades nov inner join cfg_tablastransmision cfg on nov.tabla = cfg.tabla where nov.transmitido = 0 order by cfg.orden, nov.secuencia;";
        
        sqlite3_stmt *statement = [[DB getInstance] prepare:QUERY_NOVEDADES];
        if ( statement ) {
            
            char *_c;
            NSString *tabla;
            NSString *registro;
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                _c = (char *)sqlite3_column_text(statement, 0);
                tabla = [NSString stringWithUTF8String:_c];
                
                _c = (char *)sqlite3_column_text(statement, 1);
                registro = [NSString stringWithUTF8String:_c];
                
                
                if ( [dictionary valueForKey:tabla]!=nil ) {
                    
                    // ya existe la entrada pido el array
                    NSMutableArray *arr = [dictionary valueForKey:tabla];
                    
                    //agego registro
                    [arr addObject:registro];
                    
                } else {
                    
                    // creo array con registro
                    NSMutableArray *arr = [[NSMutableArray alloc] init];
                    [arr addObject:registro];
                    
                    // agrego entrada con array
                    [dictionary setValue:arr forKey:tabla];
                    [arr release];
                }
                
                
            }
            
        } else {
            NSAssert(0, @"No se encontre la tabla");
        }
        sqlite3_finalize(statement);
        
        
        NSArray *allKeys = [[dictionary allKeys] retain];
        
        for (NSString *tabla in allKeys) {
            
            str = @"<DATASET>\r\n";
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
            str = [NSString stringWithFormat:@"NOMBRE=%@\r\n",tabla];
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSArray *arr = [[dictionary valueForKey:tabla] retain];
            
            int cnn = 0;
            for (NSString *registro in arr) {
                cnn = cnn + [registro length] + 1;  //el +1 se lo pongo por el delimitadorque se agregar adelante  ~
            }
            str = [NSString stringWithFormat:@"BLOCKSIZE=%d\r\n",cnn];
            [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
            
            for (NSString *registro in arr) {
                
                str = [NSString stringWithFormat:@"%@~",registro];
                [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
                
            }
            [arr release];
        }
        
        [data appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [allKeys release];
        [dictionary release];
        
    }
    
    return [data autorelease];
    
}
-(NSData*) generarPie:(ACCIONTX) accion {
    
    NSMutableData *data = [[NSMutableData alloc] initWithCapacity:0];
    @autoreleasepool {
        
        NSString *str = @"=%@\r\nCMD=RTX\r\nNAME=%@\r\nSIZE=999\r\n";
        str = [NSString stringWithFormat:str,SettingForKey(ENTIDAD, @""),SettingForKey(TRANID, @"") ];
        [data appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    return [data autorelease];
    
}

-(void) enviarNovedad:(NSData*)novedad {
    
    // generar archivo txt - apm3i.i
    NSString *cachesFolder = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    
    NSString *file = [[cachesFolder stringByAppendingPathComponent:@"apm3i.i"] retain];
    
    [novedad writeToFile:file options:NSDataWritingAtomic error:nil];
    
    NSString *ip    = [[Config shareInstance] find:REMOTE_HOST];
    NSString *puerto= [[Config shareInstance] find:REMOTE_PORT];
    NSString *script= [[Config shareInstance] find:REMOTE_PATH];
    NSString *user  = [[Config shareInstance] find:ENTIDAD];
    NSString *secuencia = [[Config shareInstance] find:TRANID];
    
    NSInteger tamanio = [novedad length];
    
    NSString *urlFormated = [NSString stringWithFormat:@"http://%@:%@%@?USER=%@&CMD=RTX&NAME=%@&SIZE=%d&DATA=BINARIO",ip,puerto,script,user,secuencia,tamanio];
    
    NSURL *url = [NSURL URLWithString:urlFormated];
    
    ASIHTTPRequest *request = [[ASIHTTPRequest requestWithURL:url] retain];
    [request appendPostDataFromFile:file];
    [request startSynchronous];
    
    NSError *error = [request error];
    NSString *response = @"0";
    if (!error) {
        
        response = [request responseString];
        if (DEBUG_TRANSMICIONX) NSLog(@"%@", response);
        
    }
    
    [request release];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    [fileMgr removeItemAtPath:file error:&error];
    
    [file release];
    
    
    
}


-(BOOL) verificarNovedadEnServidor {
    
    
    NSString *ip    = [[Config shareInstance] find:REMOTE_HOST];
    NSString *puerto= [[Config shareInstance] find:REMOTE_PORT];
    NSString *script= [[Config shareInstance] find:REMOTE_PATH];
    NSString *user  = [[Config shareInstance] find:ENTIDAD];
    NSString *secuencia = [[Config shareInstance] find:TRANID];
    
    NSString *urlFormated = [NSString stringWithFormat:@"http://%@:%@%@?USER=%@&CMD=SVC_EXISTS_RPR&ID=%@",ip,puerto,script,user,secuencia];
    NSURL *url = [NSURL URLWithString:urlFormated];
    ASIHTTPRequest *request = [[ASIHTTPRequest requestWithURL:url] retain];
    [request startSynchronous];
    
    NSError *error = [request error];
    NSString *response = @"0";
    if (!error) {
        
        response = [request responseString];
        
    }
    
    [request release];
    return ([response isEqualToString:@"1"]) ? YES:NO;
    
}



@end
